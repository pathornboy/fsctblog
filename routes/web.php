<?php


use Illuminate\Support\Facades\Input;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $data = Input::all();

    Session::put('fullname', $data['fullname']);
    Session::put('position', $data['position']);
    Session::put('brcode', $data['brcode']);
    Session::put('emp_code', $data['emp_code']);
    Session::put('id_position',$data['id_position']);
    Session::put('level_emp', $data['level_emp']);
    //
    // Session::put('idcompany', '1');
    // Session::put('brcode', '1001');
    // Session::put('emp_code', '1001');
    // Session::put('id_position', '1');//1
    // Session::put('level_emp', '1');//1
    // Session::put('fullname', 'Boss');
    // Session::put('position', 'Boss');

    return view('welcome');
});
Route::get('/mainmenu', function () {
    return view('tempmenu');
});

Route::get('/mainmenuhead', function () {
    return view('headmenu');
});

Route::get('/mainmenufooter', function () {
    return view('footer');
});

Route::get('/listsform','ListsController@form');

Route::get('/reserve_money','Reserve_moneyController@index');

Route::get('/!expenses','not_expensesController@index');

Route::get('/ภงด3','AlltaxController@index');

Route::get('/gen_tax3','AlltaxController@gen_tax3');

Route::get('/approve_prlist','Approve_prlistController@approve_prlist');

Route::get('/approve_prlist_ac','Approve_polistController@approve_polist');


Route::get('/pettycash','PettycashController@index');

Route::get('/getpettycash','PettycashController@get');

Route::post('/insertpettycash','PettycashController@create');

Route::any('/deletepettycash','PettycashController@delete');

Route::post('/editpettycash','PettycashController@edit');

Route::get('/companyinformation','CompanyController@companyinformation');

Route::get('/companyinformationbyid/{id}','CompanyController@companyinformationbyid');

Route::post('/companyinsert','CompanyController@companyinsert');

Route::get('/oilbill','OilController@Index');

Route::post('/deletecompany','CompanyController@deletecompany');

Route::get('/customercenter','CustomerController@customercenter');

Route::get('/configaccounttax','ConfigaccountController@taxfig');

Route::post('/configtaxinsertandupdate','ConfigaccountController@configtaxinsertandupdate');

Route::get('/getdataconfigtax','ConfigaccountController@getdataconfigtax');

Route::get('/configcodeaccount','ConfigaccountController@configcodeaccount');

Route::post('/configaccoutcodeinsertandupdate','ConfigaccountController@configaccoutcodeinsertandupdate');

Route::get('/getdataconfigaccountcode','ConfigaccountController@getdataconfigaccountcode');

Route::get('/configtypepay','ConfigaccountController@configtypepay');

Route::post('/configtypepayinsertandupdate','ConfigaccountController@configtypepayinsertandupdate');

Route::get('/getdataconfigtypepay','ConfigaccountController@getdataconfigtypepay');

Route::get('/configtypebuy','ConfigaccountController@configtypebuy');

Route::post('/configtypebuyinsertandupdate','ConfigaccountController@configtypebuyinsertandupdate');

Route::get('/getdataconfigtypebuy','ConfigaccountController@getdataconfigtypebuy');

Route::get('/configterms','ConfigaccountController@configterms');

Route::post('/configtermsinsertandupdate','ConfigaccountController@configtermsinsertandupdate');

Route::get('/getdataconfigterms','ConfigaccountController@getdataconfigterms');

Route::get('/configconfiggroupsupp','ConfigaccountController@configconfiggroupsupp');

Route::post('/configconfiggroupsuppinsertandupdate','ConfigaccountController@configconfiggroupsuppinsertandupdate');

Route::get('/getdataconfigconfiggroupsupp','ConfigaccountController@getdataconfigconfiggroupsupp');

Route::get('/configinitial','ConfigaccountController@configinitial');

Route::post('/configinitialinsertandupdate','ConfigaccountController@configinitialinsertandupdate');

Route::get('/getdataconfiginitial','ConfigaccountController@getdataconfiginitial');

Route::get('/printpr/{id}', function ($id) {
    $data = [
        'id'=>$id
    ];
    $pdf = PDF::loadView('printppr', $data);
    return @$pdf->stream();

//    return view('invoice');
});

Route::get('/configstatusaccount','ConfigaccountController@configstatusaccounts');

Route::post('/configstatusaccinsertandupdate','ConfigaccountController@configstatusaccinsertandupdate');

Route::get('/getdataconfigstatusacc','ConfigaccountController@getdataconfigstatusacc');

Route::get('/configwithhold','ConfigaccountController@configwithhold');

Route::post('/configwithholdinsertandupdate','ConfigaccountController@configwithholdinsertandupdate');

Route::get('/getdataconfigwithhold','ConfigaccountController@getdataconfigwithhold');

Route::post('/inform_po_get_id','POController@inform_po_get_id');

Route::post('/inform_po_edit','POController@inform_po_edit');

Route::get('geteditreservemoney', 'MoneyController@geteditreservemoney');

//Route::get('/configtransfer','ConfigaccountController@configtransfer');
////
//Route::post('/configtransferinsertandupdate','ConfigaccountController@configtransferinsertandupdate');
//
//Route::get('/getdataconfigtransfer','ConfigaccountController@getdataconfigtransfer');

Route::post('/printpaymentvoucher','VendorController@printpaymentvoucher');

Route::get('/printpo/{id}', function ($id) {
    $data = [
        'id'=>$id
    ];
    $pdf = PDF::loadView('printpo', $data);
    return @$pdf->stream();

});



Route::get('/printchoosesupplier/{id}', function ($id) {
    $data = [
        'id'=>$id
    ];
    $pdf = PDF::loadView('printchoosesupplier', $data);
    return @$pdf->stream();

});

Route::get('/taxwithholdsupplier', 'TaxwithholdsupplierController@index');

Route::get('/print_taxwithholdsupplier/{id}', function ($id) {
    $data = [
        'id'=> $id//$id
    ];
    $pdf = PDF::loadView('taxwithhold', $data)->setPaper('a3', 'portrait');
    // dd($pdf['Dompdf']);
    // dd($pdf);
    return @$pdf->stream();

});

Route::any('/findpohead', 'TaxwithholdsupplierController@findpohead');


Route::get('/exportexcel','TestController@exportexcel');

Route::get('/printreceipt/{billno}/{poid}', function ($billno,$poid) {
    $data = [
        'billno'=>$billno,
        'poid'=>$poid,
    ];
    $pdf = PDF::loadView('printreceipt', $data);
    return @$pdf->stream();

});




//GOOD CRUD

	//GOOD GROUP

Route::get('/configgoodgroup','ConfigGoodController@configgoodgroup');

Route::post('/configgoodgroupinsertandupdate','ConfigGoodController@configgoodgroupinsertandupdate');

Route::get('/getdataconfiggoodgroup','ConfigGoodController@getdataconfiggoodgroup');

	//GOOD type

Route::get('/configgoodtype','ConfigGoodController@configgoodtype');

Route::post('/configgoodtypeinsertandupdate','ConfigGoodController@configgoodtypeinsertandupdate');

Route::get('/getdataconfiggoodtype','ConfigGoodController@getdataconfiggoodtype');

	//GOOD

Route::get('/configgood','ConfigGoodController@configgood');

Route::post('/configgoodinsertandupdate','ConfigGoodController@configgoodinsertandupdate');

Route::get('/getdataconfiggood','ConfigGoodController@getdataconfiggood');

    //Supplier

Route::post('/updatevendorstatus','VendorController@updatevendorstatus');

Route::get('/vendorcenter','VendorController@vendorcenter');

Route::post('/vendorcenterinsertandupdate','VendorController@vendorcenterinsertandupdate');

Route::get('/getdatavendorcenter','VendorController@getdatavendorcenter');

Route::get('/vendoraddbill','VendorController@vendoraddbill');

Route::post('/insertheadppr','VendorController@insertheadppr');

Route::post('/insertheadpoall','VendorController@insertheadpoall');

Route::post('/insertdetailppr','VendorController@insertdetailppr');

Route::get('/getdatavendordetail','VendorController@getdatavendordetail');

Route::get('/getdatavendorpprdetail','VendorController@getdatavendorpprdetail');

Route::get('/getdatapoupdate','VendorController@getdatapoupdate');

Route::get('/getdatapodetailupdate','VendorController@getdatapodetailupdate');

Route::post('/insertbillmoney','VendorController@insertbillmoney');

Route::post('/insertlogbillrunstock','VendorController@insertlogbillrunstock');

Route::get('/getpobillrunmoney','VendorController@getpobillrunmoney');

Route::get('/getdataaccounttype','VendorController@getdataaccounttype');

Route::get('/getidpr','VendorController@getidpr');

Route::get('/addpo','VendorController@addpo');

Route::get('/addpoall','VendorController@addpoall');

Route::get('/detail_choosesupplier','VendorController@detail_choosesupplier');

Route::get('/getidpo','VendorController@getidpo');

Route::post('/getdataprbycodepr','VendorController@getdataprbycodepr');

Route::post('/getdataprbyweekall','VendorController@getdataprbyweekall');

Route::get('/getdatacompany','VendorController@getdatacompany');

Route::post('/addpoinsertupdate','VendorController@addpoinsertupdate');

Route::post('/insertdetailpo','VendorController@insertdetailpo');

Route::get('/transferpo','VendorController@transferpo');

Route::post('/inserttransfertostockacc','VendorController@inserttransfertostockacc');

Route::post('/getprrefmaterialid','VendorController@getprrefmaterialid');

Route::post('/insertporefpr','VendorController@insertporefpr');

Route::post('/insertpopremony','VendorController@insertpopremony');

Route::post('/getcheckprpoduplicate','VendorController@getcheckprpoduplicate');

//Supplier Credit

Route::post('/updatevendorstatuscredit','VendorCreditController@updatevendorstatus');

Route::get('/vendorcentercredit','VendorCreditController@vendorcenter');

Route::post('/vendorcenterinsertandupdatecredit','VendorCreditController@vendorcenterinsertandupdate');

Route::get('/getdatavendorcentercredit','VendorCreditController@getdatavendorcenter');

Route::get('/vendoraddbillcredit','VendorCreditController@vendoraddbill');

Route::post('/insertheadpprcredit','VendorCreditController@insertheadppr');

Route::post('/insertheadpoallcredit','VendorCreditController@insertheadpoall');

Route::post('/insertdetailpprcredit','VendorCreditController@insertdetailppr');

Route::get('/getdatavendordetailcredit','VendorCreditController@getdatavendordetail');

Route::get('/getdatavendorpprdetailcredit','VendorCreditController@getdatavendorpprdetail');

Route::get('/getdatapoupdatecredit','VendorCreditController@getdatapoupdate');

Route::get('/getdatapodetailupdatecredit','VendorCreditController@getdatapodetailupdate');

Route::post('/insertbillmoneycredit','VendorCreditController@insertbillmoney');

Route::post('/insertlogbillrunstockcredit','VendorCreditController@insertlogbillrunstock');

Route::get('/getpobillrunmoneycredit','VendorCreditController@getpobillrunmoney');

Route::get('/getdataaccounttypecredit','VendorCreditController@getdataaccounttype');

Route::get('/getidprcredit','VendorCreditController@getidpr');

Route::get('/addpocredit','VendorCreditController@addpo');

Route::get('/addpoallcredit','VendorCreditController@addpoall');

Route::get('/detail_choosesuppliercredit','VendorCreditController@detail_choosesupplier');

Route::get('/getidpocredit','VendorCreditController@getidpo');

Route::post('/getdataprbycodeprcredit','VendorCreditController@getdataprbycodepr');

Route::post('/getdataprbyweekallcredit','VendorCreditController@getdataprbyweekall');

Route::get('/getdatacompanycredit','VendorCreditController@getdatacompany');

Route::post('/addpoinsertupdatecredit','VendorCreditController@addpoinsertupdate');

Route::post('/insertdetailpocredit','VendorCreditController@insertdetailpo');

Route::get('/transferpocredit','VendorCreditController@transferpo');

Route::post('/inserttransfertostockacccredit','VendorCreditController@inserttransfertostockacc');

Route::post('/getprrefmaterialidcredit','VendorCreditController@getprrefmaterialid');

Route::post('/insertporefprcredit','VendorCreditController@insertporefpr');

Route::post('/insertpopremonycredit','VendorCreditController@insertpopremony');


// เลขบัญชีธนาคารแต่ละสาขา
Route::get('/bankcash','BankcashController@index');

Route::get('/getbankcash','BankcashController@get');

Route::post('/insertbankcash','BankcashController@create');

Route::any('/deletebankcash','BankcashController@delete');

Route::post('/editbankcash','BankcashController@edit');


//   FsctmainController
Route::get('/getmainmaterialall','FsctmainController@getmainmaterialall');

Route::get('/getgoodassetall','FsctmainController@getgoodassetall');

Route::get('/getotherall','FsctmainController@getotherall');


Route::get('/pocenter','POController@pocenter');

Route::get('/podetailbydate','POController@podetailbydate');

Route::get('/deleteinform/{id}','POController@deleteinform');



//ระบบเงินสด

Route::get('/reportcashdailynew','ReportController@reportcashdailynew'); //รายงานเงิดสดย่อยรายวัน

Route::post('/searchreportcashdailynow','ReportController@searchreportcashdailynow');

Route::post('/configreportcashdailynow','ReportController@configreportcashdailynow');

Route::get('/cashcheckbank','ReportController@cashcheckbank');

Route::post('/searchcashcheckbank','ReportController@searchcashcheckbank');

Route::post('/configgetcashchechbank','ReportController@configgetcashchechbank');//กรณีเมื่อคลิก ฝากสลิป

Route::post('/configgetcashchech','ReportController@configgetcashchech');//กรณีเมื่อ ออดิททำการตรวจเช็ค

Route::get('/excelreportcashdailynew','CashExcelController@excelreportcashdailynew');//Excel รายงานเงินสดย่อยรายวัน

Route::get('/getdatacashedit','ReportController@getdatacashedit'); //จัดการข้อมูลเงินตั้งต้นรายวัน

Route::post('/searchgetdatacashedit','ReportController@searchgetdatacashedit'); //จัดการข้อมูลเงินตั้งต้นรายวัน

Route::post('/saveeditgetdatacashedit','ReportController@saveeditgetdatacashedit'); //จัดการข้อมูลเงินตั้งต้นรายวัน

Route::post('insertdatacash', 'ReportController@insertdatacash');

Route::post('savecash', 'ReportController@savecash');




Route::get('/reportcashdaily','ReportController@reportcashdaily'); //รายงานเงิดสดย่อยรายวัน (อันเก่า)

Route::post('/searchreportcashdaily','ReportController@searchreportcashdaily');

Route::post('/configreportcashdaily','ReportController@configreportcashdaily'); //รายงานเงิดสดย่อยรายวัน (อันเก่า)

Route::get('/reporttotalcashdaily','ReportController@reporttotalcashdaily'); //รายงานภาพรวมเงินสดย่อย

Route::post('/searchreporttotalcashdaily','ReportController@searchreporttotalcashdaily'); //รายงานภาพรวมเงินสดย่อย

Route::get('/getcashedit','RentController@getcashedit'); //จัดการข้อมูลเงินสด

Route::post('/searchgetcashedit','RentController@searchgetcashedit');

Route::get('/getcash','RentController@getcash'); //จัดการข้อมูลเงินสด (อันใหม่)

Route::post('/configgetcash','RentController@configgetcash');

Route::get('/getdataconfigcash','RentController@getdataconfigcash');

Route::post('/configcashinsertandupdate','RentController@configcashinsertandupdate'); //จัดการข้อมูลเงินสด

Route::get('/configaccount','ConfigaccounttingController@configaccount'); //ตั้งค่าข้อมูลบัญชี

Route::post('/configaccountinsertandupdate','ConfigaccounttingController@configaccountinsertandupdate');

Route::get('/getdataconfigaccount','ConfigaccounttingController@getdataconfigaccount'); //ตั้งค่าข้อมูลบัญชี

Route::get('/polist','POController@polist'); //

Route::get('/polistshow','POController@polistshow'); //

Route::post('/saveapprovedpo','POController@saveapprovedpo');

Route::post('/searchprlist','POController@searchprlist');

Route::post('/searchprlistac','POController@searchprlistac');

Route::post('/approvedtopu','POController@approvedtopu');

Route::post('/approvedtoac','POController@approvedtoac');


Route::get('/cashrent','CashrentController@index');

Route::post('/getcashrent','CashrentController@getcashrent');

Route::post('/savecashrent','CashrentController@savecashrent');

Route::post('insertbillcash', 'CashrentController@insertbillcash');

Route::post('/searchbc','POController@searchbc');

Route::post('/searchbcshow','POController@searchbcshow');


Route::post('/deletepprdetail','POController@deletepprdetail');

Route::get('/reportpopayin','ReportcashpayinController@reportpopayin');

Route::post('/serachreportpopayin','ReportcashpayinController@serachreportpopayin');

Route::post('/serachpoforpayin','POController@serachpoforpayin');

Route::post('/serachpodetailforpayin','POController@serachpodetailforpayin');

Route::post('/savechangesmoneypo','POController@savechangesmoneypo');

Route::post('/deletepohead','POController@deletepohead');

Route::post('/createwhd','POController@createwhd');

Route::get('/reporttaxbuy','ReportController@reporttaxbuy'); //รายงานภาษีซื้อ

Route::post('/serachreporttaxbuy','ReportController@serachreporttaxbuy');

Route::get('/excelreporttaxbuy','ExcelController@excelreporttaxbuy'); //รายงานภาษีซื้อ

Route::get('/reportexpenses','ReportController@reportexpenses'); //รายงานค่าใช้จ่าย

Route::post('/serachreportexpenses','ReportController@serachreportexpenses'); //รายงานค่าใช้จ่าย

Route::get('/reporttaxsell','ReportController@reporttaxsell'); //รายงานภาษีขาย

Route::post('/serachreporttaxsell','ReportController@serachreporttaxsell'); //รายงานภาษีขาย

Route::get('/reporttaxwht','ReportController@reporttaxwht'); //รายงานภาษีหัก ณ ที่จ่าย

Route::post('/serachreporttaxwht','ReportController@serachreporttaxwht'); //รายงานภาษีหัก ณ ที่จ่าย

Route::get('/reporttaxsellinsurance','ReportController@reporttaxsellinsurance'); //รายงานภาษีขาย (เงินประกัน)

Route::post('/serachreporttaxsellinsurance','ReportController@serachreporttaxsellinsurance'); //รายงานภาษีขาย (เงินประกัน)




//----------------- PDF รายงานภาษีซื้อ -------------------------
Route::get('/printreporttaxbuy', function () { //รายงานภาษีซื้อ
    $data = Input::all();
	// print_r($data);

	$pdf = PDF::loadView('printreporttaxbuy', $data);
    return @$pdf->stream();
});
//----------------- PDF รายงานภาษีซื้อ -------------------------


//----------------- PDF รายงานค่าใช้จ่าย -------------------------
Route::get('/printreportexpenses', function () { //รายงานภาษีซื้อ
    $data = Input::all();
	// print_r($data);

	$pdf = PDF::loadView('printreportexpenses', $data);
    return @$pdf->stream();
});
//----------------- PDF รายงานค่าใช้จ่าย -------------------------


//----------------- PDF รายงานภาษีขาย -------------------------
Route::get('/printreporttaxsell', function () { //รายงานภาษีขาย
    $data = Input::all();
	// print_r($data);

	$pdf = PDF::loadView('printreporttaxsell', $data);
    return @$pdf->stream();
});
//----------------- PDF รายงานภาษีขาย -------------------------


//----------------- PDF รายงานภาษีหัก ณ ที่จ่าย -------------------------
Route::get('/printreporttaxwht', function () { //รายงานภาษีหัก ณ ที่จ่าย
    $data = Input::all();
	// print_r($data);

	$pdf = PDF::loadView('printreporttaxwht', $data);
    return @$pdf->stream();
});
//----------------- PDF รายงานภาษีหัก ณ ที่จ่าย -------------------------


//----------------- PDF รายงานภาษีขาย (เงินประกัน) -------------------------
Route::get('/printreporttaxsellinsurance', function () { //รายงานภาษีขาย (เงินประกัน)
    $data = Input::all();
	// print_r($data);

	$pdf = PDF::loadView('printreporttaxsellinsurance', $data);
    return @$pdf->stream();
});
//----------------- PDF รายงานภาษีขาย -------------------------




Route::get('/poprint', function () {
    $data = Input::all();
	// print_r($data);

	$pdf = PDF::loadView('poprint', $data);
    return @$pdf->stream();
});

Route::get('/prcenter','PRController@prcenter');

Route::get('/prprint', function () {
    $data = Input::all();
	// print_r($data);

	$pdf = PDF::loadView('prprint', $data);
    return @$pdf->stream();
});

Route::get('downloadExcelPO', 'POExcelController@downloadExcelPO');
Route::get('downloadExcelPR', 'PRExcelController@downloadExcelPR');
Route::get('importExport', 'POExcelController@importExport');
Route::post('importExcel', 'POExcelController@importExcel');

Route::get('/inform_po','POController@inform_po');
Route::post('/inform_po_insert','POController@inform_po_insert');

Route::post('/searchpodetailbc','POController@searchpodetailbc');


Route::get('/withdraw_money','MoneyController@withdraw_money');




//-------------------- Report Saraly ----------------------------

Route::get('/reportpayinandsalary','ReportController@reportpayinandsalary');



//-------------------- PrintTextController ------------------------------------

Route::get('/printwithholdtaxpo','PrintTextController@printwithholdtaxpo');

Route::post('/serachprintwithholdtaxpo','PrintTextController@serachprintwithholdtaxpo');


Route::post('saveatmcash', 'VendorController@saveatmcash');

Route::post('insertpaypre', 'MoneyController@insertpaypre');

Route::post('insert_expenses', 'not_expensesController@insert_expenses');

Route::post('insertporef', 'MoneyController@insertporef');


Route::get('/boxcover','ReportController@boxcover');

Route::post('getboxcover', 'ReportController@getboxcover');



Route::get('/printboxcover', function () {

	$pdf = PDF::loadView('boxcoverpdf')->setPaper('A4','landscape');
    return @$pdf->stream();
});



Route::get('/logout', function () {
      Session::flush();
      return redirect('http://www.fsctonline.com/fscthr/auth/default/index');
});
