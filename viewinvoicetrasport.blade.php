<?php
use  App\Api\Connectdb;
use  App\Api\Rent;
$level_emp = Session::get('level_emp');
$emp_code = Session::get('emp_code');
$position = Session::get('id_position');
?>
@include('headmenu')




<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>
<link rel="stylesheet" type="text/css" href="css/ui/jquery-ui.css">

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>

<!-- <script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap2.min.js'></script> -->

<script type="text/javascript" src = 'js/rent/viewinvoicermandcn.js'></script>
<script src="bower_components/select2/dist/js/select2.full.min.js"></script>
<link rel="stylesheet" href="bower_components/select2/dist/css/select2.min.css">

<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


<style>
    .ui-autocomplete-input {
        border: none;
        font-size: 14px;
        width: 225px;
        height: 24px;
        margin-bottom: 5px;
        padding-top: 2px;
        border: 1px solid #DDD !important;
        padding-top: 0px !important;
        z-index: 1511;
        position: relative;
    }
    .ui-menu .ui-menu-item a {
        font-size: 12px;
    }
    .ui-autocomplete {
        position: absolute;
        top: 0;
        left: 0;
        z-index: 1510 !important;
        float: left;
        display: none;
        min-width: 160px;
        width: 160px;
        padding: 4px 0;
        margin: 2px 0 0 0;
        list-style: none;
        background-color: #ffffff;
        border-color: #ccc;
        border-color: rgba(0, 0, 0, 0.2);
        border-style: solid;
        border-width: 1px;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding;
        background-clip: padding-box;
        *border-right-width: 2px;
        *border-bottom-width: 2px;
    }
    .ui-menu-item > a.ui-corner-all {
        display: block;
        padding: 3px 15px;
        clear: both;
        font-weight: normal;
        line-height: 18px;
        color: #555555;
        white-space: nowrap;
        text-decoration: none;
    }
    .ui-state-hover, .ui-state-active {
        color: #ffffff;
        text-decoration: none;
        background-color: #0088cc;
        border-radius: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        background-image: none;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                  <li>
                      <i class="ace-icon fa fa-cog home-icon"></i>
                      <a href="#">ระบบเช่า</a>
                  </li>
                  <li class="active">ใบเสร็จรับเงินค่าขนส่ง</li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-10">

                </div>
                <div class="col-md-1">

                </div>
                <div class="col-md-1">
                    <a href="#" title="เพิ่มข้อมูล" data-toggle="modal" data-target="#myModal" onclick="insertnew()" ><img src="images/global/add.png">เพิ่มข้อมูล</a>
                </div>
            </div>
            <div class="box-body" class="row">

              <form action="approvededitbilltransport" method="post">
                 <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="row" style="overflow-x:auto;">
              <input type="hidden" id="datetimeset" value="<?php echo date('Y-m-d H:i:s') ?>">
                      <table id="example" class="table table-striped table-bordered">
                          <thead class="thead-inverse">
                          <tr>
                              <td>#</td>
                              <td>วันที่</td>
                              <td>บิลเลขท่ี</td>
                              <td>บิลเลขท่ีเช่า</td>
                              <td>สาขา</td>
                              <td>สถานะ</td>
                              <td>การจัดการ</td>
                              <td>พิมพ์เอกสาร</td>
                              <td>รายละเอียด</td>
                              <td>เหตุผลที่ขอ</td>
                              <td>อนุมัติโดยซุป</td>
                              <td>อนุมัติโดยออดิด</td>
                              <td>อนุมัติโดยบอส</td>
                              <td>ประเภท</td>


                          </tr>
                          </thead>

                          <tbody>

                            <?php
                                $i =1;
                                $db = Connectdb::Databaseall();
                                $datemonth = date('Y-m-d');
                                $timeremove = strtotime('-7 day',strtotime($datemonth));
                                $timeremove = date('Y-m-d 23:59:59',$timeremove);
                                $brcode = Session::get('brcode');

                                 if($level_emp==1){
                                   $data = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.taxinvoice_special_abb WHERE  date_req > "'.$timeremove.'" ORDER BY `id` DESC');
                                 }else{
                                   $data = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.taxinvoice_special_abb WHERE branch_id = "'.$brcode.'" AND date_req > "'.$timeremove.'" ORDER BY `id` DESC');

                                 }
                                // print_r($data);
                                // exit;
                            ?>
                            <?php foreach ($data as $key => $value) {?>
                              <tr>
                                  <td><?php echo $i; ?></td>
                                  <td><?php echo $value->date_req ; ?></td>
                                  <td>
                                    <?php
                                    echo $idbillrent =  $value->bill_rent ;
                                    // $billdata = Rent::getdatabillrent($idbillrent);
                                    // print_r($billdata[0]->bill_rent);
                                    ?>
                                  </td>
                                  <td><?php echo $value->number_taxinvoice ; ?></td>
                                  <td><?php echo $value->branch_id ; ?></td>
                                  <td><?php if($value->status==1){
                                              echo "<font color='green'>ใช้งาน</font>";
                                            } else {
                                              echo "<font color='red'>ยกเลิก</font>";
                                        } ?>
                                  </td>

                                  <td>
                                    <a href="#" title="อนุมัติข้อมูล" data-toggle="modal" data-target="#myModal" onclick="getdata({{$value->id}},0)">
                                      <img src="images/global/edit-icon.png">


                                  </td>
                                  <td align="center">
                                        <?php if($value->statusboss==1  OR $value->statussup==1 OR $value->statusauth==1){ ?>
                                            <a href="<?php echo url("/printtaxinvoicespecialabb/$value->id");?>" target="_blank"><img src="images/global/printer.png"></a>
                                        <?php } ?>
                                  </td>
                                  <td align="center">
                                            <a href="<?php echo url("/printtaxinvoicespecialabbexmple/$value->id");?>" target="_blank"><img src="images/global/printer.png"></a>
                                  </td>
                                  <td align="center">
                                      <?php echo $value->note;?>
                                  </td>
                                  <td>
                                      <input type="checkbox" name="approvedsup1[]" value="<?php echo $value->id;?>" <?php if($value->statussup==1){ echo "checked"; }?> <?php if($level_emp!=7){ echo "disabled"; } ?>> อนุมัติซุป
                                  </td>
                                  <td>
                                      <input type="checkbox" name="approvedauth1[]" value="<?php echo $value->id;?>"  <?php if($value->statusauth==1){ echo "checked"; }?>  <?php if($position ==17 OR $position ==26 ){ echo "enabled"; }else{ echo "disabled";} ?>> อนุมัติออดิด
                                  </td>
                                  <td>
                                      <input type="checkbox" name="approvedboss1[]" value="<?php echo $value->id;?>" <?php if($value->statusboss==1){ echo "checked"; }?>  <?php if($level_emp ==1 OR $level_emp ==2 ){ echo "enabled"; }else{ echo "disabled";} ?>> อนุมัติบอส
                                  </td>
                                  <td>ใบเสร็จรับเงิน</td>

                              </tr>
                            <?php  $i++; }?>


                          </tbody>
                      </table>


                    </div>
                    <div class="row">
                      <div class="col-md-12">
                          <input type="submit" class="btn btn-success pull-right" value="บันทึก">
                      </div>
                    </div>
              </form>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')


<!-- Modal -->

<!-- Modal -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">ข้อมูลคลังสินค้า</h5>
            </div>

            <div class="modal-body">
                <!-- The form is placed inside the body of modal -->
                <form id="formStockhome" onsubmit="return getdatesubmit();" data-toggle="validator" method="post" class="form-horizontal">
                    <input value="{{ null }}" type="hidden" id="id" name="id" />
                    <input type="hidden" name="year" id="year" value="<?php echo date('Y')?>" >
                    <input type="hidden" name="company_id" id="company_id" value="<?php echo $idcompany = Session::get('idcompany'); ?>" >
                    <input type="hidden" name="branch_id" id="branch_id" value="<?php echo $idcompany = Session::get('brcode'); ?>" >

                    <div class="form-group">
                        <label class="col-xs-3 control-label">วันที่<i><span style="color: red">*</span></i></label>
                        <div class="col-xs-5">
                            <input type="text" id="date_req" value="<?php echo date('Y-m-d H:i:s')?>" class="form-control" name="date_req" readonly  required/>
                        </div>
                        <div class="col-xs-4">

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">ชื่อลูกค้า</label>
                        <div class="col-xs-5">
                            <input type="text" name="customer_name" id="customer_name" onkeyup="autocompletecusdata()"  class="form-control" />
                            <input type="hidden" name="customer_id" id="customer_id" class="form-control" />
                        </div>
                        <div class="col-xs-4">

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-3 control-label">รหัสพนักงาน<i><span style="color: red">*</span></i></label>
                        <div class="col-xs-5">
                            <input type="text" name="codeemp" id="codeemp" class="form-control" required/>
                        </div>
                        <div class="col-xs-4">

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">บิลที่อ้างอิง</label>
                        <div class="col-xs-5">
                            <?php
                              $brcode = Session::get('brcode');
                              $db = Connectdb::Databaseall();
                              $sqlref = 'SELECT * FROM '.$db['fsctmain'].'.bill_rent WHERE status != "99" AND branch_id = "'.$brcode.'" ';
                              $dataref = DB::connection('mysql')->select($sqlref);

                              $sqlengine = 'SELECT * FROM '.$db['fsctmain'].'.billengine_rent WHERE status != "99" AND branch_id = "'.$brcode.'" ';
                              $dataengine = DB::connection('mysql')->select($sqlengine);
                            ?>
                            <select name="bill_rent" id="bill_rent" class="form-control select2">
                                <option value="">เลือกบิล</option>
                                <?php foreach ($dataref as $key => $value) { ?>
                                  <option value="<?php echo $value->bill_rent ?>"><?php echo $value->bill_rent ?></option>
                                <?php } ?>
                                <?php foreach ($dataengine as $key2 => $value2) { ?>
                                  <option value="<?php echo $value2->billengine_rent ?>"><?php echo $value2->billengine_rent ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-xs-4">

                        </div>
                    </div>

                    <div class="row ">
                        <div class="col-md-12">
                            <div class="pull-right">
                                <a href="#" title="เพิ่มรายการ" id="addrow" ><img src="images/global/add.png">เพิ่มรายการ</a>
                            </div>
                        </div>
                    </div>
                    <div class="row showdetail">
                        <div class="col-md-12">
                            <table class="table" id="thdetail">
                                <thead>
                                <tr>
                                    <td align="center">รายการ</td>
                                    <td align="center" >ราคา</td>


                                    <td align="center">ลบ</td>
                                </tr>
                                </thead>
                                <tbody id="tdbody">
                                    <td align="center">
                                        <input type="text" name="name[]" id="name0"  class="form-control" >
                                    </td>
                                    <td align="center">
                                        <input type="text" name="total[]" onblur="calculatenew()" id="total0" class="form-control">
                                    </td>
                                    <td align="center"></td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                              <label class="col-xs-5 control-label">หัก ณ ที่จ่าย<i><span style="color: red">*</span></i></label>
                              <div class="col-xs-4">
                                  <input type="text" name="withhold" id="withhold" onblur="calculatenew()"  class="form-control" placeholder="%" value="0">
                              </div>
                              <label class="col-xs-1 control-label">%</label>

                          </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-5 control-label">ภาษี<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-4">

                                    <select name="vat" id="vat" class="form-control" onchange="calculatenew()">
                                        <option value="0" >ไม่คิดภาษี</option>
                                        <?php
                                        $db = Connectdb::Databaseall();
                                        $datavat = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.tax_config WHERE status ="1"');
                                        ?>
                                        @foreach ($datavat as $valuevat)
                                            <option value="{{$valuevat->tax}}">{{$valuevat->tax}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label class="col-xs-1 control-label">%</label>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-6">

                        </div>
                        <div class="col-md-6">
                          <div class="form-group">

                              <div class="col-xs-4">
                                  <input type="hidden" name="subtotal" id="subtotal"  class="form-control" placeholder="%" value="0">
                                  <input type="hidden" name="vatmoney" id="vatmoney"  class="form-control" placeholder="%" value="0">
                                  <input type="hidden" name="withholdmoney" id="withholdmoney"  class="form-control" placeholder="%" value="0">
                              </div>


                          </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                      <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-5 control-label">เหตุผลในการขอ</label>
                                <div class="col-xs-4">
                                    <textarea id="note" cols="40" rows="6"></textarea>
                                </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label class="col-xs-5 control-label">รวมสุทธิ</label>
                                  <div class="col-xs-4">
                                      <U><span id="showtotal">0</span>
                                        <input type="hidden" name="grandtotal" id="grandtotal" value="0">
                                      </U>
                                      บาท
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <br>
                      </div>
                      <div class="row">
                          <div class="col-md-4">
                          </div>
                          <div class="col-md-4">
                              เลือกประเภทการขาย
                              <select id="type" name="type" class="form-control">
                                    <option value="2" select="select">ใบเสร็จรับเงิน( พิเศษ )</option>
                              </select>
                          </div>
                          <div class="col-md-4">
                          </div>
                      </div>
                      <div class="row">
                          <br>
                      </div>
                      <div class="row">
                          <div class="col-md-4">
                          </div>
                          <div class="col-md-4">
                              เลือกช่องการจ่ายเงิน
                              <select id="getvaluepay" onchange="selectgetvaluern()"  name="getvaluepay" class="form-control">
                                    <option value="0" select="select">เลือกการโอนเงิน</option>
                                    <option value="1" >เงินสด</option>
                                    <option value="2" >เงินโอน</option>
                              </select>
                          </div>
                          <div class="col-md-4">
                          </div>
                      </div>
                      <div class="row">
                          <br>
                      </div>
                      <div class="row">
                          <div class="col-md-4">
                          </div>
                          <div class="col-md-4">
                              <input type="hidden" id="status" name="status" value="0">
                          </div>
                          <div class="col-md-4">
                          </div>
                      </div>
                      <div class="row">
                          <br>
                      </div>
                    <div class="form-group">
                        <div class="col-xs-5 col-xs-offset-5">
                            <button type="submit" style="display:none;" id="Btn_save" class="btn btn-primary" >บันทึก</button>
                              <?php if($level_emp==1 || $level_emp ==2 || $level_emp==7){?>
                            <button type="button" style="display:none" id="Btn_approved" onclick="saveapproved()" class="btn btn-success">อนุมัติ</button>
                              <?php } ?>

                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
