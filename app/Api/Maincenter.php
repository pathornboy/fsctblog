<?php
/**
 * Created by PhpStorm.
 * User: pacharapol
 * Date: 1/14/2018 AD
 * Time: 9:59 PM
 */

namespace App\Api;
use  App\Api\Connectdb;
use DB;


class Maincenter
{
    public static function databranchbycode($code_branch){

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[hr_base].branch.*
                FROM $db[hr_base].branch
                WHERE $db[hr_base].branch.code_branch = '$code_branch'";

        $dataquery = DB::select($sql);

        return $dataquery;
    }

    public static function datacompany($id){

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[hr_base].working_company.*
                FROM $db[hr_base].working_company
                WHERE $db[hr_base].working_company.id = '$id'";

        $dataquery = DB::select($sql);

        return $dataquery;
    }

    public static  function getdatacompemp($codeemp){
        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[hr_base].emp_data.*
                FROM $db[hr_base].emp_data
                WHERE $db[hr_base].emp_data.code_emp_old = '$codeemp'";

        $dataquery = DB::select($sql);

        return $dataquery;
    }

  	public static function yearCorverttoBE ($date){

        $year=substr($date,0,4);
  			$month=substr($date,5,2);
  			$day=substr($date,8,2);
  			$yearBE = $year+543;
  			$dateBE=$yearBE."-".$month."-".$day;

        return $dateBE;
      }

      public static function getdatacustomer($customerid){

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctmain].customers.*,
                       $db[fsctaccount].initial.per
                FROM $db[fsctmain].customers

                INNER JOIN $db[fsctaccount].initial
                  ON $db[fsctmain].customers.initial = $db[fsctaccount].initial.id

                WHERE $db[fsctmain].customers.customerid LIKE '%$customerid%'
                ";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;
      }

      public static function getbillrent($idbillrent,$iddate){

        // echo "<pre>";
        // print_r($iddatetime);
        // echo "<br>";
        // print_r($idbranch_id);
        // exit;
        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctmain].bill_rent.*,
                       $db[fsctmain].bill_return_head.*
                FROM $db[fsctmain].bill_rent
                INNER JOIN $db[fsctmain].bill_return_head
                   ON $db[fsctmain].bill_rent.id = $db[fsctmain].bill_return_head.bill_rent
                WHERE $db[fsctmain].bill_return_head.bill_rent = '$idbillrent'
                AND $db[fsctmain].bill_return_head.time LIKE '%$iddate%'
                ";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;
      }

      public static function getbillrentengine($idbillrent,$iddate){

        // echo "<pre>";
        // print_r($idbillrent);
        // echo "<br>";
        // print_r($iddate);
        // exit;
        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctmain].billengine_rent.*,
                       $db[fsctmain].billengine_return_head.*
                FROM $db[fsctmain].billengine_rent
                INNER JOIN $db[fsctmain].billengine_return_head
                   ON $db[fsctmain].billengine_rent.id = $db[fsctmain].billengine_return_head.bill_rent
                WHERE $db[fsctmain].billengine_return_head.bill_rent = '$idbillrent'
                AND $db[fsctmain].billengine_return_head.time LIKE '%$iddate%'
                ";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;
      }

      public static function getdatacustomercash($customerid){

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctmain].customers.*
                FROM $db[fsctmain].customers
                WHERE $db[fsctmain].customers.customerid LIKE '%$customerid%'";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;
      }

      public static function getdatapodetail($poid){

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].po_detail.*
                FROM $db[fsctaccount].po_detail
                WHERE $db[fsctaccount].po_detail.po_headid LIKE '%$poid%'
                AND $db[fsctaccount].po_detail.statususe NOT IN (99)
                ";

        $dataquery = DB::connection('mysql')->select($sql);
        // echo "<pre>";
        // print_r($dataquery);
        // exit;
        return $dataquery;
      }

      public static function getdatara($ra){

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].taxinvoice_abb.*
                FROM $db[fsctaccount].taxinvoice_abb
                WHERE $db[fsctaccount].taxinvoice_abb.id = '$ra'
                ";

        $dataquery = DB::connection('mysql')->select($sql);
        // echo "<pre>";
        // print_r($dataquery);
        // exit;
        return $dataquery;
      }

      public static function getdatarn($rn){

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].taxinvoice_more_abb.*
                FROM $db[fsctaccount].taxinvoice_more_abb
                WHERE $db[fsctaccount].taxinvoice_more_abb.id = '$rn'
                ";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;
      }

      public static function getdatarl($rl){

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].taxinvoice_loss_abb.*
                FROM $db[fsctaccount].taxinvoice_loss_abb
                WHERE $db[fsctaccount].taxinvoice_loss_abb.id = '$rl'
                ";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;
      }

      public static function getdatacn($cn){

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].taxinvoice_creditnote.*
                FROM $db[fsctaccount].taxinvoice_creditnote
                WHERE $db[fsctaccount].taxinvoice_creditnote.id = '$cn'
                ";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;
      }

      public static function getdatati($ti){

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].taxinvoice_insurance.*
                FROM $db[fsctaccount].taxinvoice_insurance
                WHERE $db[fsctaccount].taxinvoice_insurance.id = '$ti'
                ";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;
      }

      public static function getdataci($ci){

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].taxinvoice_insurance_creditnote.*
                FROM $db[fsctaccount].taxinvoice_insurance_creditnote
                WHERE $db[fsctaccount].taxinvoice_insurance_creditnote.bill_rent = '$ci'
                AND $db[fsctaccount].taxinvoice_insurance_creditnote.status NOT IN (99)
                ";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;
      }

      public static function getdatars($rs){

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].taxinvoice_special_abb.*
                FROM $db[fsctaccount].taxinvoice_special_abb
                WHERE $db[fsctaccount].taxinvoice_special_abb.id = '$rs'
                ";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;
      }

      public static function getdatacs($cs){

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].taxinvoice_creditnote_special_abb.*
                FROM $db[fsctaccount].taxinvoice_creditnote_special_abb
                WHERE $db[fsctaccount].taxinvoice_creditnote_special_abb.id = '$cs'
                ";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;
      }

      public static function getdataro($ro){

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].taxinvoice_partial_abb.*
                FROM $db[fsctaccount].taxinvoice_partial_abb
                WHERE $db[fsctaccount].taxinvoice_partial_abb.id = '$ro'
                ";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;
      }

      public static function getdatass($ss){

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctmain].stock_sell_head.*
                FROM $db[fsctmain].stock_sell_head
                WHERE $db[fsctmain].stock_sell_head.id = '$ss'
                ";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;
      }

      public static function getdatasupplierpo($po){

        $db = Connectdb::Databaseall();

        $po_id = explode(",",trim(($po)));
        // $po_id[0];


        $sql = "SELECT $db[fsctaccount].po_head.*
                FROM $db[fsctaccount].po_head
                WHERE $db[fsctaccount].po_head.id = '$po_id[0]'";

        $data = DB::connection('mysql')->select($sql);
        // echo "<pre>";
        // print_r($data);
        // exit;

        if($data[0]->supplier_id){
          $sqlsuplier = "SELECT $db[fsctaccount].supplier.*
                         FROM $db[fsctaccount].supplier
                         WHERE $db[fsctaccount].supplier.id = '".$data[0]->supplier_id."'
                        ";

          $dataquery = DB::connection('mysql')->select($sqlsuplier);
        }
        // echo "<pre>";
        // print_r($dataquery);
        // exit;

        return $dataquery;
      }

      public static function getdatasupplierponame($po){

        $db = Connectdb::Databaseall();

        // $po_id = explode(",",trim(($po)));
        // $po_id[0];


        $sql = "SELECT $db[fsctaccount].po_head.*
                FROM $db[fsctaccount].po_head
                WHERE $db[fsctaccount].po_head.id = '$po_id'";

        $data = DB::connection('mysql')->select($sql);
        // echo "<pre>";
        // print_r($data);
        // exit;

        if($data[0]->supplier_id){
          $sqlsuplier = "SELECT $db[fsctaccount].supplier.*
                         FROM $db[fsctaccount].supplier
                         WHERE $db[fsctaccount].supplier.id = '".$data[0]->supplier_id."'
                        ";

          $dataquery = DB::connection('mysql')->select($sqlsuplier);
        }
        // echo "<pre>";
        // print_r($dataquery);
        // exit;

        return $dataquery;
      }

      public static function getdatapo($po){

        $db = Connectdb::Databaseall();

        $sql = "SELECT $db[fsctaccount].po_head.*
                FROM $db[fsctaccount].po_head
                WHERE $db[fsctaccount].po_head.id = '$po'";

        $dataquery = DB::connection('mysql')->select($sql);
        // echo "<pre>";
        // print_r($data);
        // exit;

        return $dataquery;
      }

      public static function getdatawth($po){

        $db = Connectdb::Databaseall();

        $sql = "SELECT $db[fsctaccount].withholdtaxpo.*
                FROM $db[fsctaccount].withholdtaxpo
                WHERE $db[fsctaccount].withholdtaxpo.id_po = '$po'";

        $dataquery = DB::connection('mysql')->select($sql);
        // echo "<pre>";
        // print_r($data);
        // exit;

        return $dataquery;
      }

      public static function gettaxinvoice_all($bill_rent,$type,$ref_tax_type){

        $db = Connectdb::Databaseall();

        // echo "<pre>";
        // print_r($bill_rent);
        // print_r($type);
        // print_r($ref_tax_type);
        // exit;

         // เช่านั่งร้าน
        if($type == 0){

          if($ref_tax_type == 0){ //เช็ค อย่างย่อ / อย่างเต็ม

            $sql = "SELECT $db[fsctaccount].taxinvoice_abb.*
                    FROM $db[fsctaccount].taxinvoice_abb
                    WHERE $db[fsctaccount].taxinvoice_abb.bill_rent = '$bill_rent'
                    AND status IN (1,98)
                    AND type = '0'
                    ";

            $dataquery = DB::connection('mysql')->select($sql);
            // echo "<pre>";
            // print_r($dataquery);
            // exit;

            if($dataquery){
              if($dataquery[0]->status == 98){

                $sql = "SELECT $db[fsctaccount].taxinvoice.*
                        FROM $db[fsctaccount].taxinvoice
                        WHERE $db[fsctaccount].taxinvoice.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '0'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);

              }else if($dataquery[0]->status == 1){

                $sql = "SELECT $db[fsctaccount].taxinvoice_abb.*
                        FROM $db[fsctaccount].taxinvoice_abb
                        WHERE $db[fsctaccount].taxinvoice_abb.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '0'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);
              }
            }

          }else if($ref_tax_type == 1){ //ของหาย

            $sql = "SELECT $db[fsctaccount].taxinvoice_loss_abb.*
                    FROM $db[fsctaccount].taxinvoice_loss_abb
                    WHERE $db[fsctaccount].taxinvoice_loss_abb.bill_rent = '$bill_rent'
                    AND status IN (1,98)
                    AND type = '0'
                    ";

            $dataquery = DB::connection('mysql')->select($sql);
            // echo "<pre>";
            // print_r($dataquery);
            // exit;
            if($dataquery){
              if($dataquery[0]->status == 98){

                $sql = "SELECT $db[fsctaccount].taxinvoice_loss.*
                        FROM $db[fsctaccount].taxinvoice_loss
                        WHERE $db[fsctaccount].taxinvoice_loss.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '0'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);

              }else if($dataquery[0]->status == 1){

                $sql = "SELECT $db[fsctaccount].taxinvoice_loss_abb.*
                        FROM $db[fsctaccount].taxinvoice_loss_abb
                        WHERE $db[fsctaccount].taxinvoice_loss_abb.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '0'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);
              }
            }

          }else if($ref_tax_type == 2){ //เพิ่มเติม

            $sql = "SELECT $db[fsctaccount].taxinvoice_more_abb.*
                    FROM $db[fsctaccount].taxinvoice_more_abb
                    WHERE $db[fsctaccount].taxinvoice_more_abb.bill_rent = '$bill_rent'
                    AND status IN (1,98)
                    AND type = '0'
                    ";

            $dataquery = DB::connection('mysql')->select($sql);
            // echo "<pre>";
            // print_r($dataquery);
            // exit;
            if($dataquery){
              if($dataquery[0]->status == 98){

                $sql = "SELECT $db[fsctaccount].taxinvoice_more.*
                        FROM $db[fsctaccount].taxinvoice_more
                        WHERE $db[fsctaccount].taxinvoice_more.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '0'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);

              }else if($dataquery[0]->status == 1){

                $sql = "SELECT $db[fsctaccount].taxinvoice_more_abb.*
                        FROM $db[fsctaccount].taxinvoice_more_abb
                        WHERE $db[fsctaccount].taxinvoice_more_abb.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '0'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);
              }
            }

          }else if($ref_tax_type == 4){ //แบบย่อย

            $sql = "SELECT $db[fsctaccount].taxinvoice_partial_abb.*
                    FROM $db[fsctaccount].taxinvoice_partial_abb
                    WHERE $db[fsctaccount].taxinvoice_partial_abb.bill_rent = '$bill_rent'
                    AND status IN (1,98)
                    AND type = '0'
                    ";

            $dataquery = DB::connection('mysql')->select($sql);
            // echo "<pre>";
            // print_r($dataquery);
            // exit;
            if($dataquery){
              if($dataquery[0]->status == 98){

                $sql = "SELECT $db[fsctaccount].taxinvoice_partial.*
                        FROM $db[fsctaccount].taxinvoice_partial
                        WHERE $db[fsctaccount].taxinvoice_partial.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '0'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);

              }else if($dataquery[0]->status == 1){

                $sql = "SELECT $db[fsctaccount].taxinvoice_partial_abb.*
                        FROM $db[fsctaccount].taxinvoice_partial_abb
                        WHERE $db[fsctaccount].taxinvoice_partial_abb.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '0'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);
              }
            }

          }


        //เช่าเครื่องยนต์
        }else if($type == 2) {

          if($ref_tax_type == 0){ //อย่างย่อ

            $sql = "SELECT $db[fsctaccount].taxinvoice_abb.*
                    FROM $db[fsctaccount].taxinvoice_abb
                    WHERE $db[fsctaccount].taxinvoice_abb.bill_rent = '$bill_rent'
                    AND status IN (1,98)
                    AND type = '2'
                    ";

            $dataquery = DB::connection('mysql')->select($sql);
            // echo "<pre>";
            // print_r($dataquery);
            // exit;
            if($dataquery){
              if($dataquery[0]->status == 98){

                $sql = "SELECT $db[fsctaccount].taxinvoice.*
                        FROM $db[fsctaccount].taxinvoice
                        WHERE $db[fsctaccount].taxinvoice.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '2'
                        ";

                // return 1;
                $dataquery = DB::connection('mysql')->select($sql);

              }else if($dataquery[0]->status == 1){

                $sql = "SELECT $db[fsctaccount].taxinvoice_abb.*
                        FROM $db[fsctaccount].taxinvoice_abb
                        WHERE $db[fsctaccount].taxinvoice_abb.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '2'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);
              }
            }

          }else if($ref_tax_type == 1){ //ของหาย

            $sql = "SELECT $db[fsctaccount].taxinvoice_loss_abb.*
                    FROM $db[fsctaccount].taxinvoice_loss_abb
                    WHERE $db[fsctaccount].taxinvoice_loss_abb.bill_rent = '$bill_rent'
                    AND status IN (1,98)
                    AND type = '2'
                    ";

            $dataquery = DB::connection('mysql')->select($sql);
            // echo "<pre>";
            // print_r($dataquery);
            // exit;

            if($dataquery){
              if($dataquery[0]->status == 98){

                $sql = "SELECT $db[fsctaccount].taxinvoice_loss.*
                        FROM $db[fsctaccount].taxinvoice_loss
                        WHERE $db[fsctaccount].taxinvoice_loss.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '2'
                        ";

                // return 1;
                $dataquery = DB::connection('mysql')->select($sql);

              }else if($dataquery[0]->status == 1){

                $sql = "SELECT $db[fsctaccount].taxinvoice_loss_abb.*
                        FROM $db[fsctaccount].taxinvoice_loss_abb
                        WHERE $db[fsctaccount].taxinvoice_loss_abb.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '2'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);
              }
            }

          }else if($ref_tax_type == 2){ //เพิ่มเติม

            $sql = "SELECT $db[fsctaccount].taxinvoice_more_abb.*
                    FROM $db[fsctaccount].taxinvoice_more_abb
                    WHERE $db[fsctaccount].taxinvoice_more_abb.bill_rent = '$bill_rent'
                    AND status IN (1,98)
                    AND type = '2'
                    ";

            $dataquery = DB::connection('mysql')->select($sql);
            // echo "<pre>";
            // print_r($dataquery);
            // exit;
            if($dataquery){
              if($dataquery[0]->status == 98){

                $sql = "SELECT $db[fsctaccount].taxinvoice_more.*
                        FROM $db[fsctaccount].taxinvoice_more
                        WHERE $db[fsctaccount].taxinvoice_more.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '2'
                        ";

                // return 1;
                $dataquery = DB::connection('mysql')->select($sql);

              }else if($dataquery[0]->status == 1){

                $sql = "SELECT $db[fsctaccount].taxinvoice_more_abb.*
                        FROM $db[fsctaccount].taxinvoice_more_abb
                        WHERE $db[fsctaccount].taxinvoice_more_abb.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '2'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);
              }
            }

          }else if($ref_tax_type == 4){ //แบบย่อย

            $sql = "SELECT $db[fsctaccount].taxinvoice_partial_abb.*
                    FROM $db[fsctaccount].taxinvoice_partial_abb
                    WHERE $db[fsctaccount].taxinvoice_partial_abb.bill_rent = '$bill_rent'
                    AND status IN (1,98)
                    AND type = '2'
                    ";

            $dataquery = DB::connection('mysql')->select($sql);
            // echo "<pre>";
            // print_r($dataquery);
            // exit;
            if($dataquery){
              if($dataquery[0]->status == 98){

                $sql = "SELECT $db[fsctaccount].taxinvoice_partial.*
                        FROM $db[fsctaccount].taxinvoice_partial
                        WHERE $db[fsctaccount].taxinvoice_partial.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '2'
                        ";

                // return 1;
                $dataquery = DB::connection('mysql')->select($sql);

              }else if($dataquery[0]->status == 1){

                $sql = "SELECT $db[fsctaccount].taxinvoice_partial_abb.*
                        FROM $db[fsctaccount].taxinvoice_partial_abb
                        WHERE $db[fsctaccount].taxinvoice_partial_abb.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '2'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);
              }
            }

          }

        }

        // echo "<pre>";
        // print_r($dataquery);
        // exit;

        // return 1;

        return $dataquery;
      }


      public static function getdatapre($pre){

        $db = Connectdb::Databaseall();

        $sql = "SELECT $db[fsctaccount].initial.*
                FROM $db[fsctaccount].initial
                WHERE $db[fsctaccount].initial.per = '$pre'";

        $dataquery = DB::connection('mysql')->select($sql);
        // echo "<pre>";
        // print_r($dataquery);
        // exit;

        return $dataquery;
      }


      public static function getdatatf($tf){

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].taxinvoice.number_taxinvoice
                FROM $db[fsctaccount].taxinvoice
                WHERE $db[fsctaccount].taxinvoice.bill_rent = '$tf'
                ";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;
      }


      public static function getdatatk($tk){

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].taxinvoice_loss.number_taxinvoice
                FROM $db[fsctaccount].taxinvoice_loss
                WHERE $db[fsctaccount].taxinvoice_loss.bill_rent = '$tk'
                ";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;
      }


      public static function getdatatm($tm){

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].taxinvoice_more.number_taxinvoice
                FROM $db[fsctaccount].taxinvoice_more
                WHERE $db[fsctaccount].taxinvoice_more.bill_rent = '$tm'
                ";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;
      }


      public static function getdatatp($tp){

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].taxinvoice_partial.number_taxinvoice
                FROM $db[fsctaccount].taxinvoice_partial
                WHERE $db[fsctaccount].taxinvoice_partial.bill_rent = '$tp'
                ";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;
      }


      public static function getdata_cn($bill_rent,$type,$ref_tax_type){

        $db = Connectdb::Databaseall();

        // echo "<pre>";
        // print_r($bill_rent);
        // print_r($type);
        // print_r($ref_tax_type);
        // exit;

         // เช่านั่งร้าน
        if($type == 0){

          if($ref_tax_type == 0){ //เช็ค อย่างย่อ / อย่างเต็ม

            $sql = "SELECT $db[fsctaccount].taxinvoice_abb.*
                    FROM $db[fsctaccount].taxinvoice_abb
                    WHERE $db[fsctaccount].taxinvoice_abb.bill_rent = '$bill_rent'
                    AND status IN (1,98)
                    AND type = '0'
                    ";

            $dataquery = DB::connection('mysql')->select($sql);
            // echo "<pre>";
            // print_r($dataquery);
            // exit;

            if($dataquery){
              if($dataquery[0]->status == 98){

                $sql = "SELECT $db[fsctaccount].taxinvoice.*
                        FROM $db[fsctaccount].taxinvoice
                        WHERE $db[fsctaccount].taxinvoice.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '0'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);

              }else if($dataquery[0]->status == 1){

                $sql = "SELECT $db[fsctaccount].taxinvoice_abb.*
                        FROM $db[fsctaccount].taxinvoice_abb
                        WHERE $db[fsctaccount].taxinvoice_abb.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '0'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);
              }
            }

          }else if($ref_tax_type == 1){ //ของหาย

            $sql = "SELECT $db[fsctaccount].taxinvoice_loss_abb.*
                    FROM $db[fsctaccount].taxinvoice_loss_abb
                    WHERE $db[fsctaccount].taxinvoice_loss_abb.bill_rent = '$bill_rent'
                    AND status IN (1,98)
                    AND type = '0'
                    ";

            $dataquery = DB::connection('mysql')->select($sql);
            // echo "<pre>";
            // print_r($dataquery);
            // exit;
            if($dataquery){
              if($dataquery[0]->status == 98){

                $sql = "SELECT $db[fsctaccount].taxinvoice_loss.*
                        FROM $db[fsctaccount].taxinvoice_loss
                        WHERE $db[fsctaccount].taxinvoice_loss.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '0'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);

              }else if($dataquery[0]->status == 1){

                $sql = "SELECT $db[fsctaccount].taxinvoice_loss_abb.*
                        FROM $db[fsctaccount].taxinvoice_loss_abb
                        WHERE $db[fsctaccount].taxinvoice_loss_abb.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '0'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);
              }
            }

          }else if($ref_tax_type == 2){ //เพิ่มเติม

            $sql = "SELECT $db[fsctaccount].taxinvoice_more_abb.*
                    FROM $db[fsctaccount].taxinvoice_more_abb
                    WHERE $db[fsctaccount].taxinvoice_more_abb.bill_rent = '$bill_rent'
                    AND status IN (1,98)
                    AND type = '0'
                    ";

            $dataquery = DB::connection('mysql')->select($sql);
            // echo "<pre>";
            // print_r($dataquery);
            // exit;
            if($dataquery){
              if($dataquery[0]->status == 98){

                $sql = "SELECT $db[fsctaccount].taxinvoice_more.*
                        FROM $db[fsctaccount].taxinvoice_more
                        WHERE $db[fsctaccount].taxinvoice_more.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '0'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);

              }else if($dataquery[0]->status == 1){

                $sql = "SELECT $db[fsctaccount].taxinvoice_more_abb.*
                        FROM $db[fsctaccount].taxinvoice_more_abb
                        WHERE $db[fsctaccount].taxinvoice_more_abb.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '0'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);
              }
            }

          }else if($ref_tax_type == 4){ //แบบย่อย

            $sql = "SELECT $db[fsctaccount].taxinvoice_partial_abb.*
                    FROM $db[fsctaccount].taxinvoice_partial_abb
                    WHERE $db[fsctaccount].taxinvoice_partial_abb.bill_rent = '$bill_rent'
                    AND status IN (1,98)
                    AND type = '0'
                    ";

            $dataquery = DB::connection('mysql')->select($sql);
            // echo "<pre>";
            // print_r($dataquery);
            // exit;
            if($dataquery){
              if($dataquery[0]->status == 98){

                $sql = "SELECT $db[fsctaccount].taxinvoice_partial.*
                        FROM $db[fsctaccount].taxinvoice_partial
                        WHERE $db[fsctaccount].taxinvoice_partial.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '0'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);

              }else if($dataquery[0]->status == 1){

                $sql = "SELECT $db[fsctaccount].taxinvoice_partial_abb.*
                        FROM $db[fsctaccount].taxinvoice_partial_abb
                        WHERE $db[fsctaccount].taxinvoice_partial_abb.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '0'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);
              }
            }

          }


        //เช่าเครื่องยนต์
        }else if($type == 2) {

          if($ref_tax_type == 0){ //อย่างย่อ

            $sql = "SELECT $db[fsctaccount].taxinvoice_abb.*
                    FROM $db[fsctaccount].taxinvoice_abb
                    WHERE $db[fsctaccount].taxinvoice_abb.bill_rent = '$bill_rent'
                    AND status IN (1,98)
                    AND type = '2'
                    ";

            $dataquery = DB::connection('mysql')->select($sql);
            // echo "<pre>";
            // print_r($dataquery);
            // exit;
            if($dataquery){
              if($dataquery[0]->status == 98){

                $sql = "SELECT $db[fsctaccount].taxinvoice.*
                        FROM $db[fsctaccount].taxinvoice
                        WHERE $db[fsctaccount].taxinvoice.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '2'
                        ";

                // return 1;
                $dataquery = DB::connection('mysql')->select($sql);

              }else if($dataquery[0]->status == 1){

                $sql = "SELECT $db[fsctaccount].taxinvoice_abb.*
                        FROM $db[fsctaccount].taxinvoice_abb
                        WHERE $db[fsctaccount].taxinvoice_abb.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '2'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);
              }
            }

          }else if($ref_tax_type == 1){ //ของหาย

            $sql = "SELECT $db[fsctaccount].taxinvoice_loss_abb.*
                    FROM $db[fsctaccount].taxinvoice_loss_abb
                    WHERE $db[fsctaccount].taxinvoice_loss_abb.bill_rent = '$bill_rent'
                    AND status IN (1,98)
                    AND type = '2'
                    ";

            $dataquery = DB::connection('mysql')->select($sql);
            // echo "<pre>";
            // print_r($dataquery);
            // exit;

            if($dataquery){
              if($dataquery[0]->status == 98){

                $sql = "SELECT $db[fsctaccount].taxinvoice_loss.*
                        FROM $db[fsctaccount].taxinvoice_loss
                        WHERE $db[fsctaccount].taxinvoice_loss.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '2'
                        ";

                // return 1;
                $dataquery = DB::connection('mysql')->select($sql);

              }else if($dataquery[0]->status == 1){

                $sql = "SELECT $db[fsctaccount].taxinvoice_loss_abb.*
                        FROM $db[fsctaccount].taxinvoice_loss_abb
                        WHERE $db[fsctaccount].taxinvoice_loss_abb.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '2'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);
              }
            }

          }else if($ref_tax_type == 2){ //เพิ่มเติม

            $sql = "SELECT $db[fsctaccount].taxinvoice_more_abb.*
                    FROM $db[fsctaccount].taxinvoice_more_abb
                    WHERE $db[fsctaccount].taxinvoice_more_abb.bill_rent = '$bill_rent'
                    AND status IN (1,98)
                    AND type = '2'
                    ";

            $dataquery = DB::connection('mysql')->select($sql);
            // echo "<pre>";
            // print_r($dataquery);
            // exit;
            if($dataquery){
              if($dataquery[0]->status == 98){

                $sql = "SELECT $db[fsctaccount].taxinvoice_more.*
                        FROM $db[fsctaccount].taxinvoice_more
                        WHERE $db[fsctaccount].taxinvoice_more.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '2'
                        ";

                // return 1;
                $dataquery = DB::connection('mysql')->select($sql);

              }else if($dataquery[0]->status == 1){

                $sql = "SELECT $db[fsctaccount].taxinvoice_more_abb.*
                        FROM $db[fsctaccount].taxinvoice_more_abb
                        WHERE $db[fsctaccount].taxinvoice_more_abb.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '2'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);
              }
            }

          }else if($ref_tax_type == 4){ //แบบย่อย

            $sql = "SELECT $db[fsctaccount].taxinvoice_partial_abb.*
                    FROM $db[fsctaccount].taxinvoice_partial_abb
                    WHERE $db[fsctaccount].taxinvoice_partial_abb.bill_rent = '$bill_rent'
                    AND status IN (1,98)
                    AND type = '2'
                    ";

            $dataquery = DB::connection('mysql')->select($sql);
            // echo "<pre>";
            // print_r($dataquery);
            // exit;
            if($dataquery){
              if($dataquery[0]->status == 98){

                $sql = "SELECT $db[fsctaccount].taxinvoice_partial.*
                        FROM $db[fsctaccount].taxinvoice_partial
                        WHERE $db[fsctaccount].taxinvoice_partial.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '2'
                        ";

                // return 1;
                $dataquery = DB::connection('mysql')->select($sql);

              }else if($dataquery[0]->status == 1){

                $sql = "SELECT $db[fsctaccount].taxinvoice_partial_abb.*
                        FROM $db[fsctaccount].taxinvoice_partial_abb
                        WHERE $db[fsctaccount].taxinvoice_partial_abb.bill_rent = '$bill_rent'
                        AND status = '1'
                        AND type = '2'
                        ";

                $dataquery = DB::connection('mysql')->select($sql);
              }
            }

          }

        }

        // echo "<pre>";
        // print_r($dataquery);
        // exit;

        // return 1;

        return $dataquery;
      }


























}
