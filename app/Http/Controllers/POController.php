<?php
/**
 * Created by PhpStorm.
 * User: pacharapol
 * Date: 1/14/2018 AD
 * Time: 7:06 PM
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Api\Connectdb;

use App\Pprdetail;
use DB;
use Illuminate\Support\Facades\Input;
use phpDocumentor\Reflection\Types\Null_;
use Session;
use App\Api\Datetime;
use Redirect;


class POController extends  Controller
{
    public function pocenter(){
        return view('pocenter');
    }

    public function printpo(){
		return view('poprint');
	}


    public function polist(){
        return view('polist');
    }

    public function polistshow(){

        return view('polistshow');
    }

    public function saveapprovedpo(){

      $data = Input::all();

      $emp_code = Session::get('emp_code');

      DB::beginTransaction();
      try {
      $db = Connectdb::Databaseall();

      if(!empty($data['tranfermoneyidpo'])){
          $tranfermoneyidpo = $data['tranfermoneyidpo'];
          foreach ($tranfermoneyidpo as $key => $value) {

            $time = date('Y-m-d H:i:s');
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_head')->where('id',$value)->update(['status_head'=>2,'po_date_ap'=>$time]);


            $sql = "SELECT $db[fsctaccount].po_head.*
                        FROM $db[fsctaccount].po_head
                        WHERE $db[fsctaccount].po_head.id = '$value'";

           $dataquery = DB::connection('mysql')->select($sql);
           // echo "<pre>";
           // print_r($dataquery);
           //
           // exit;



           $newarrinsertmoney = [];

           $arrInsertCash = [];
             foreach ($dataquery as $key3 => $value3) {
                $branch_id = $value3->branch_id;
                $newarrinsertmoney[$key3]['id']='';
                $newarrinsertmoney[$key3]['payreal']=$value3->totolsumall;
                $newarrinsertmoney[$key3]['status']='0';
                $newarrinsertmoney[$key3]['createat']= date('Y-m-d H:i:s');
                $newarrinsertmoney[$key3]['statususe']= '1';
                $newarrinsertmoney[$key3]['po_headid']= $value;
                $newarrinsertmoney[$key3]['updateat']= date('Y-m-d H:i:s');




                $arrInsertCash[$key3]['id']='';
                $arrInsertCash[$key3]['datetimeinsert']=date('Y-m-d H:i:s');
                $arrInsertCash[$key3]['money']=$value3->totolsumall;
                $arrInsertCash[$key3]['typedoc']=12;
                $arrInsertCash[$key3]['emp_code']=$emp_code;
                $arrInsertCash[$key3]['branch']=$branch_id;
                $arrInsertCash[$key3]['status']=1;
                $arrInsertCash[$key3]['typetranfer']=2;
                $arrInsertCash[$key3]['log']='MD/BOSS โอนเงินจากใบ '.$value3->po_number;
                $arrInsertCash[$key3]['ref']=$value3->id;
                $arrInsertCash[$key3]['typereftax']=$value3->id;


             }

            $tranfermodel = DB::connection('mysql')->table($db['fsctaccount'].'.'.'moneytrasnfer')->insert($newarrinsertmoney);

            $InsertCash = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($arrInsertCash);

          }
      }


      if(!empty($data['approvedidpo'])){
        $approvedidpo = $data['approvedidpo'];
          foreach ($approvedidpo as $key2 => $value2) {

            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_head')->where('id',$value2)->update(['status_head'=>1]);

          }

      }

      if(!empty($data['returnstatus'])){
        $approvedidpo = $data['returnstatus'];
          foreach ($approvedidpo as $key2 => $value2) {

            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_head')->where('id',$value2)->update(['status_head'=>0]);

          }

      }

      if(!empty($data['cancelstatus'])){
        $approvedidpo = $data['cancelstatus'];
          foreach ($approvedidpo as $key2 => $value2) {

            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_head')->where('id',$value2)->update(['status_head'=>99]);

          }

      }

      DB::commit();

        return redirect('polist');

    }catch (\Exception $e) {
                DB::rollback();

    			  return $e;
    			// print_r($e);
    			//return 0;
                // something went wrong
      }


    }

    public function searchprlist(){

        $data = Input::all();
        $db = Connectdb::Databaseall();
        $branch = $data['branch'];
        return view('approve_prlist',['branch'=>$branch,'data'=>true]);

    }

    public function searchprlistac(){

        $data = Input::all();
        // echo "<pre>";
        // print_r($data);

        $datepicker = $data['datepicker'];
        $dateranger = Datetime::FormatDateFromCalendarRange($datepicker);
        $start_date = $dateranger['start_date'];
        $end_date = $dateranger['end_date'];


        $db = Connectdb::Databaseall();
        $branch = $data['branch'];

        return view('approve_prlist_view',['branch'=>$branch,'data'=>true,'start_date'=>$start_date,'end_date'=>$end_date,'datepicker'=>$datepicker]);

    }

    public function approvedtopu(){
        $data = Input::all();
        // print_r($data);
        // exit;
        $idapp = $data['idapp'];
        $branch = $data['branch'];
        $db = Connectdb::Databaseall();
        foreach ($idapp as $key => $value) {

            // print_r($dataquery);
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'ppr_detail')->where('id',$value)->update(['approvedstatus'=>1]);


        }

        return view('approve_polist',['branch'=>$branch,'data'=>true]);

    }

    public function approvedtoac(){
        $data = Input::all();
        // print_r($data);
        // exit;
        $idapp = $data['idapp'];
        $branch = $data['branch'];
        $start_date = $data['start_date'];
        $end_date = $data['end_date'];
        $db = Connectdb::Databaseall();
        // DB::beginTransaction();
        // try {
            foreach ($idapp as $key => $value) {
              $sql = "SELECT $db[fsctaccount].ppr_detail.ppr_headid
                          FROM $db[fsctaccount].ppr_detail
                          WHERE $db[fsctaccount].ppr_detail.id = '$value'";

             $dataquery = DB::connection('mysql')->select($sql);
             $idhead = $dataquery[0]->ppr_headid;
              $modelhead = DB::connection('mysql')->table($db['fsctaccount'].'.'.'ppr_head')->where('id',$idhead)->update(['status'=>3]);
                $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'ppr_detail')->where('id',$value)->update(['approvedstatusmd'=>1,'approvedstatus'=>1]);
            }
          // }
          // DB::rollback();

        return view('approve_prlist_view',['branch'=>$branch,'data'=>true,'start_date'=>$start_date,'end_date'=>$end_date]);

    }


    public function searchbc(){

        $data = Input::all();

        $datepicker = Datetime::FormatDateFromCalendarRange($data['datepicker']);
        // print_r($datepicker);
        $start_date = $datepicker['start_date'];
        $end_date = $datepicker['end_date'];

        $branch = $data['branchselect'];
        $status_head = $data['status_head'];

        return view('polist',['branchselect'=>$branch,
                              'querybr'=>true,
                              'datepicker'=>$data['datepicker'],
                              'start_date'=>$start_date,
                              'end_date'=>$end_date,
                              'status_head'=>$status_head]);
    }

    public function searchbcshow(){

        $data = Input::all();
        // echo "<pre>";
        // print_r($data);
        // exit;
        $branch = $data['branchselect'];

        return view('polistshow',['branchselect'=>$branch,'querybr'=>true]);

    }




    public function deletepprdetail(){
        $data = Input::all();

        $db = Connectdb::Databaseall();
        $id = $data['id'];
        $branch = $data['branch'];

        $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'ppr_detail')->where('id',$id)->update(['approvedstatusmd'=>98]);


        return 1;


    }


    public function inform_po(){
        $db = Connectdb::Databaseall();
        $branch = Session::get('brcode');
        $data_po = DB::table('inform_po')
                          ->select('inform_po.*')
                          ->join('po_head', 'po_head.id', '=', 'inform_po.id_po')
                          ->where([
                                  ['inform_po.status','=',1],
                                  ['po_head.branch_id','=',$branch]
                                ])
                          ->get();
        $vat_po = DB::table('tax_config')->get();
        $wht_po = DB::table('withhold')->get();
        return view('inform_po',
        [
            'data_po' => $data_po,
            'vat_po' => $vat_po,
            'wht_po'=>$wht_po,
            ]);
    }

    public function inform_po_insert(){
        $data = Input::all();
        $db = Connectdb::Databaseall();
        DB::beginTransaction();
          try{
                if(Input::hasFile('inform_po_picture')){

                  // echo "<pre>";
                  // print_r($data);
                  // exit;


        			$file = Input::file('inform_po_picture');
                    $file->move('uploads_po', $data['po_ref'].'.jpg');
                    $name = Input::file('inform_po_picture')->getClientOriginalName(); //get ชื่อfie จริงๆ

                            DB::table('inform_po')->insert(
                            ['po_ref' => $data['po_ref'],
                             'id_po' => $data['id_po'],
                             'bill_no'=>$data['bill_no'],
                            'vat_percent' => $data['vat_percent'],
                            'vat_price' => $data['vat_price'],
                            'wht_percent'=>$data['wht_percent'],
                            'wht'=>$data['wht'],
                            'type_pay' => $data['type_pay'],
                            'payout' => $data['payout'],
                            'change_pay' => $data['change_pay'],
                            'inform_po_picture' =>  $data['po_ref'],
                            'datebill'=>$data['datebill']
                            ]);


                      $emp_code = Session::get('emp_code');
                      $brcode = Session::get('brcode');



                            DB::table('insertcashrent')->insert(
                            ['id' => '',
                              'datetimeinsert' => date('Y-m-d H:i:s'),
                              'money' => $data['payout'],
                              'typedoc'=>10,
                              'emp_code'=>$emp_code,
                              'branch' => $brcode,
                              'status' => '1',
                              'typetranfer' => $data['type_pay'],
                              'log' => 'จ่ายเงินให้กับใบ  '.$data['po_ref'],
                              'ref' => $data['id_po'],
                              'typereftax' =>  $data['po_ref']
                            ]);



                            $arrvalue['id']= '';
                            $arrvalue['po_headid']= $data['id_po'];
                            $arrvalue['datebill']= date('Y-m-d');
                            $arrvalue['bill_no']= $data['bill_no'];
                            $arrvalue['pay_real']= $data['payout'];
                            $arrvalue['returnperbill']= $data['change_pay'];
                            $arrvalue['status']= 1;
                            $arrvalue['code_emp']= $emp_code;
                            $arrvalue['createby']= $emp_code;
                            $arrvalue['createdate']= date('Y-m-d H:i:s');


                            DB::table('po_bill_run_money')->insert(
                            ['id' => '',
                              'po_headid' => $data['id_po'],
                              'datebill' =>date('Y-m-d'),
                              'bill_no'=>$data['bill_no'],
                              'pay_real'=>$data['payout'],
                              'returnperbill' => $data['change_pay'],
                              'status' => '1',
                              'code_emp' => $emp_code,
                              'createby' => $emp_code,
                              'createdate' => date('Y-m-d H:i:s')
                            ]);



                        // echo "string";
                        // exit;

                      if($data['totolsumall']==$data['payout']){
                          //// จ่ายครบ
                            $modelupdate = DB::table('po_head')->where('id',$data['id_po'])->update(['status_head'=>3]);

                      }else if($data['totolsumall'] > $data['payout']){
                          //// มีถอน
                            $modelupdate = DB::table('po_head')->where('id',$data['id_po'])->update(['status_head'=>4]);


                      }else if($data['totolsumall'] < $data['payout']){
                          //// ขาด
                            $modelupdate = DB::table('po_head')->where('id',$data['id_po'])->update(['status_head'=>5]);


                      }


                      $arrStock = [];

                      foreach ($data['quantity_get'] as $key => $value) {
                          $arrStock[$key]['id'] = '';
                          $arrStock[$key]['dateinput'] = date('Y-m-d');
                          $arrStock[$key]['bill_no'] = $data['bill_no'];
                          $arrStock[$key]['id_po'] = $data['id_po'];
                          $arrStock[$key]['id_detail'] = $data['id_detail'][$key];
                          $arrStock[$key]['quantity_get'] = $value;
                          $arrStock[$key]['code_emp'] = $emp_code;

                      }


                        $model2 = DB::connection('mysql')->table($db['fsctaccount'].'.savestocklog')->insert($arrStock);



                            //////  การจ่ายเงิน //////

                            $statusbill = 1;



                        DB::commit();

                        return redirect()->back()->with('alert', 'แจ้งการจ่ายเงินสำเร็จ');

            		}else{
                        DB::rollback();


                }


        }catch (\Exception $e) {
            DB::rollback();
            throw new \Exception($e);
        }



    }

    public function serachpoforpayin(){
        $data = Input::all();
        // echo "<pre>";
        // print_r($data);
        $po_ref = $data['po_ref'];

        $db = Connectdb::Databaseall();

        $sql = "SELECT $db[fsctaccount].po_head.*
                    FROM $db[fsctaccount].po_head
                    WHERE $db[fsctaccount].po_head.po_number LIKE '$po_ref'
                    AND  status_head = '2'";

       $dataquery = DB::connection('mysql')->select($sql);

       if(!empty($dataquery)){
         return $dataquery;
       }else{
         return 0;
       }

    }



    public function serachpodetailforpayin(){
          $data = Input::all();
          // echo "<pre>";
          // print_r($data);
          $po_ref = $data['po_ref'];

          $db = Connectdb::Databaseall();

          $sql = "SELECT $db[fsctaccount].po_detail.*
                      FROM $db[fsctaccount].po_head
                      INNER JOIN $db[fsctaccount].po_detail
                      ON  $db[fsctaccount].po_head.id = $db[fsctaccount].po_detail.po_headid
                      WHERE $db[fsctaccount].po_head.po_number LIKE '$po_ref'
                      AND  $db[fsctaccount].po_head.status_head = '2'
                      AND $db[fsctaccount].po_detail.statususe = '1' ";

         $dataquery = DB::connection('mysql')->select($sql);

         if(!empty($dataquery)){
           return $dataquery;
         }else{
           return 0;
         }


    }


    public function savechangesmoneypo(){
            $data = Input::all();
            $db = Connectdb::Databaseall();
            // echo "<pre>";
            // print_r($data);
            // exit;
            $arrdetailid = $data['arrdetailid'];
            $idpo = $data['idpo'];
            $totolsumall = $data['totolsumall'];
              DB::beginTransaction();
              try{
                  foreach ($arrdetailid as $key => $value) {
                    $total = $data['arramount'][$key]*$data['arrprice'][$key];
                    $amount = $data['arramount'][$key];
                    $price = $data['arrprice'][$key];
                      // echo $value;
                      // echo "amount=".$amount;
                      // echo "price=".$price;
                      // echo "<br>";
                      $model = DB::connection('mysql')->table($db['fsctaccount'].'.po_detail')
                                                      ->where('id',$value)
                                                      ->update(['amount'=>$amount,
                                                                'price'=>$price,
                                                                'total'=>$total
                                                                ]);
                    }


                      $modelhead = DB::connection('mysql')->table($db['fsctaccount'].'.po_head')
                                                      ->where('id',$idpo)
                                                      ->update(['totolsumall'=>$totolsumall]);

                    DB::commit();

                    return 1;

              }catch (\Exception $e){
                    DB::rollback();
                    return 0;

              }







    }



   public function deletepohead()
    {
          $data = Input::all();
          $id = $data['id'];
          $db = Connectdb::Databaseall();
          $modelhead = DB::connection('mysql')->table($db['fsctaccount'].'.po_head')
                                          ->where('id',$id)
                                          ->update(['status_head'=>99]);
          return 1;


    }


    public function createwhd(){

        $data = Input::all();
        $db = Connectdb::Databaseall();

        $idpo = $data['id'];
        $sql = "SELECT $db[fsctaccount].withholdtaxpo.*
                    FROM $db[fsctaccount].withholdtaxpo
                    WHERE $db[fsctaccount].withholdtaxpo.id_po = '$idpo'
                    AND $db[fsctaccount].withholdtaxpo.status = '1' ";

        $model = DB::connection('mysql')->select($sql);

       if(!empty($model)){
          ////////   มีแล้ว

          return $model[0]->id;

       }else{
         ////////  สร้างใหม่
          $date = date('Y-m-d');
          $sqlpo = "SELECT $db[fsctaccount].po_head.whd,
                           $db[fsctaccount].po_head.branch_id,
                           $db[fsctaccount].po_detail.amount,
                           $db[fsctaccount].po_detail.price,
                           $db[fsctaccount].po_detail.total
                      FROM $db[fsctaccount].po_head
                      INNER JOIN $db[fsctaccount].po_detail
                        ON $db[fsctaccount].po_head.id =  $db[fsctaccount].po_detail.po_headid
                      WHERE $db[fsctaccount].po_head.id = '$idpo'
                      AND $db[fsctaccount].po_detail.statususe = '1' ";

          $modelpo = DB::connection('mysql')->select($sqlpo);

          // echo "<pre>";
          $arrSum = 0;
          // print_r($modelpo);
          foreach ($modelpo as $key => $value) {
              $arrSum = $arrSum + ($value->amount * $value->price);
                 // print_r($value);
          }
          $whd_percent = $modelpo[0]->whd;
          $branch_id = $modelpo[0]->branch_id;
          $arrSum =round($arrSum,2);
          $beforevat = $arrSum;
          $arrSum = $arrSum*($whd_percent/100);
          $emp_code = Session::get('emp_code');
          // exit;

          $yearnow = date('Y');
          $month = date('m');



           $sqlnumrun = "SELECT  COUNT(id) as idgen
                      FROM $db[fsctaccount].withholdtaxpo
                      WHERE $db[fsctaccount].withholdtaxpo.branch = '$branch_id'
                      AND YEAR($db[fsctaccount].withholdtaxpo.date) = '$yearnow'
                      AND MONTH($db[fsctaccount].withholdtaxpo.date) = '$month'
                      AND $db[fsctaccount].withholdtaxpo.status = '1'
                      GROUP  BY YEAR($db[fsctaccount].withholdtaxpo.date), MONTH($db[fsctaccount].withholdtaxpo.date) ";



          $bbr_no_branch = DB::connection('mysql')->select($sqlnumrun);
          $numberrun = 0;

          // print_r($bbr_no_branch);
          // exit;

          if(!empty($bbr_no_branch[0]->idgen) && $bbr_no_branch[0]->idgen >= 1){
            // echo "22222";
             $onset = $bbr_no_branch[0]->idgen;
                 $numberrun = (int)($onset)+1;
          }else{

              $numberrun = 1;
              // echo "string";

          }


          $arrInsert = ['id'=>'',
                        'idhead'=>$month,
                        'idrun'=>$numberrun,
                        'id_po'=>$idpo,
                        'date'=>$date,
                        'beforevat'=>$beforevat,
                        'whd_percent'=>$whd_percent,
                        'total'=>$arrSum,
                        'branch'=>$branch_id,
                        'code_emp'=>$emp_code,
                        'status'=>1];

          $tranfermodel = DB::connection('mysql')->table($db['fsctaccount'].'.withholdtaxpo')->insertGetId($arrInsert);


          return $tranfermodel;




       }






    }

	public function podetailbydate(){

			return view('podetailbydate');
	}


	public function searchpodetailbc(){


        $data = Input::all();
        //echo "<pre>";
		//print_r($data);
		//exit;
        $datepicker = Datetime::FormatDateFromCalendarRange($data['datepicker']);
        // print_r($datepicker);
        $start_date = $datepicker['start_date'];
        $end_date = $datepicker['end_date'];

        $branch = $data['branchselect'];

        return view('podetailbydate',['branchselect'=>$branch,
                              'querybr'=>true,
                              'datepicker'=>$datepicker,
                              'start_date'=>$start_date,
                              'end_date'=>$end_date]);
	}

  public function deleteinform($id){
        $data = Input::all();
        $db = Connectdb::Databaseall();

        $sqlinform_po = "SELECT $db[fsctaccount].inform_po.*
                    FROM $db[fsctaccount].inform_po
                    WHERE  $db[fsctaccount].inform_po.status = '1'
                    AND $db[fsctaccount].inform_po.id = '$id' ";

       $dataqueryinform_po = DB::connection('mysql')->select($sqlinform_po);




        $modelupdateinform_po = DB::connection('mysql')->table($db['fsctaccount'].'.inform_po')
                                        ->where('id',$id)
                                        ->update(['status'=>99]);
        $id_po =$dataqueryinform_po[0]->id_po ;

        $modelupdatepo_head = DB::connection('mysql')->table($db['fsctaccount'].'.po_head')
                                        ->where('id',$id_po)
                                        ->update(['status_head'=>2]);

        $modelupdate = DB::connection('mysql')->table($db['fsctaccount'].'.po_bill_run_money')
                                        ->where('po_headid',$id_po)
                                        ->update(['status'=>99]);

        $modelupdatecash = DB::connection('mysql')->table($db['fsctaccount'].'.insertcashrent')
                                        ->where('ref',$id_po)
                                        ->where('typedoc','=','10')
                                        ->update(['status'=>99]);

        $po_ref = $dataqueryinform_po[0]->po_ref;

        $sql = "SELECT $db[fsctaccount].po_detail.*
                    FROM $db[fsctaccount].po_head
                    INNER JOIN $db[fsctaccount].po_detail
                    ON  $db[fsctaccount].po_head.id = $db[fsctaccount].po_detail.po_headid
                    WHERE $db[fsctaccount].po_head.po_number LIKE '$po_ref'
                    AND  $db[fsctaccount].po_head.status_head = '2'
                    AND $db[fsctaccount].po_detail.statususe = '1' ";

       $dataquery = DB::connection('mysql')->select($sql);



       $branch = Session::get('brcode');
       $data_po = DB::table('inform_po')
                         ->select('inform_po.*')
                         ->join('po_head', 'po_head.id', '=', 'inform_po.id_po')
                         ->where([
                                 ['inform_po.status','=',1],
                                 ['po_head.branch_id','=',$branch]
                               ])
                         ->get();
        $vat_po = DB::table('tax_config')->get();
        $wht_po = DB::table('withhold')->get();

         return Redirect::back()->with([
             'data_po' => $data_po,
             'vat_po' => $vat_po,
             'wht_po'=>$wht_po,
             ]);

  }





}
