<?php
/**
 * Created by PhpStorm.
 * User: pacharapol
 * Date: 1/14/2018 AD
 * Time: 7:06 PM
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Api\Connectdb;
use App\Api\Datetime;
use App\Pprdetail;
use DB;
use Illuminate\Support\Facades\Input;
use phpDocumentor\Reflection\Types\Null_;
use Session;
use PDF;
use Redirect;


class VendorController extends  Controller
{

    public function __construct(){

        $empcode = $this->cartId = session()->get('emp_code');

        if(!isset($empcode)){
          Redirect::to('/logout')->send();
        }

        //Redirect::to('/logout')->send();

      }

    public function vendorcenter(){
        return view('vendorcenter');
    }

    public function vendorcenterinsertandupdate(){
        $data= Input::all();
        $db = Connectdb::Databaseall();
        $datainsert = json_decode($data['data']);
        $arrInsert = [];

        foreach ($datainsert as $v){
            $arrInsert [$v->name] = $v->value;
        }
       // print_r($arrInsert);

        $data= Input::all();
        //        print_r($data);
        //        exit;
         $yearnow = date('Y');


        $brcode = Session::get('brcode');
        $db = Connectdb::Databaseall();
        $sql = "SELECT COUNT(id) as idgen FROM $db[fsctaccount].supplier";
        $bbr_no_branch = DB::connection('mysql')->select($sql);
        $numberrun = 0;

        $onset =  $bbr_no_branch[0]->idgen;

        if($onset == 0){
            $numberrun = 1;
        }else{
            $numberrun = (int)($onset)+1;
        }

        $yearth = $yearnow + 543;
        $yearth =  substr($yearth,2,2);
        $idgen = 'PS'.$brcode.$yearth.date('m').str_pad($numberrun, 3, "0", STR_PAD_LEFT);
        // $datagen =[ 'idgen'=>$idgen,
        //     'idnumber'=>$numberrun
        // ] ;

        $arrInsert['ps_number'] = $idgen;
        // return $arrInsert;

        if($arrInsert['id'] == ""){
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'supplier')->insert($arrInsert);
        }else{
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'supplier')->where('id',$arrInsert['id'])->update($arrInsert);
        }

        return 1;
    }

    public function getdatavendorcenter(){
        $data = Input::all();

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].supplier.*,$db[fsctaccount].supplier_terms.name as suppliertermsname
                     FROM $db[fsctaccount].supplier
                     INNER JOIN $db[fsctaccount].supplier_terms
                     ON $db[fsctaccount].supplier.terms_id = $db[fsctaccount].supplier_terms.id
                     WHERE $db[fsctaccount].supplier.id = '$data[id]'";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;

    }
    public function vendoraddbill(){
        return view('vendocreatebillnew');
    }


    public function insertheadppr(){
        $data= Input::all();



        $db = Connectdb::Databaseall();


        if($data['data']['id'] == ""){


            $yearnow = date('Y');
            $idcompany = $data['data']['id_company'];


            $brcode = Session::get('brcode');
            $db = Connectdb::Databaseall();
            $month = date('m');


           $sql = "SELECT COUNT(id) as idgen
                     FROM $db[fsctaccount].ppr_head
                     WHERE branch_id = '$brcode'
                     AND id_company = '$idcompany'
                     AND year = '$yearnow'
                     AND MONTH($db[fsctaccount].ppr_head.date) = '$month'
                     GROUP  BY YEAR($db[fsctaccount].ppr_head.date), MONTH($db[fsctaccount].ppr_head.date) ";

            // print_r($bbr_no_branch);

            $bbr_no_branch = DB::connection('mysql')->select($sql);
            $numberrun = 0;
            // print_r($bbr_no_branch);
            // exit('100');
            if(!empty($bbr_no_branch[0]->idgen)){
                  $onset =  $bbr_no_branch[0]->idgen;

            }else{
                    $onset =  0;

            }



            if($onset == 0){
               $numberrun = 1;
            }else{
               $numberrun = (int)($onset)+1;
            }

            $yearth = $yearnow + 543;
            $yearth =  substr($yearth,2,2);
            $idgen = 'PR'.$brcode.$yearth.date('m').str_pad($numberrun, 3, "0", STR_PAD_LEFT);

             $data['data']['number_ppr'] = $idgen;
             $data['data']['bbr_no_branch'] = $numberrun;



            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'ppr_head')->insert($data['data']);
            return $lastid = DB::getPdo()->lastInsertId();

        }else{


            $emp_code = Session::get('emp_code');
            $datainsertapproved = [ 'id'=>Null,
                'id_approvedby'=>$emp_code,
                'id_prhead'=>$data['data']['id'],
                'status'=>1,
                'status_approved'=>$data['data']['status'],
                'createby'=>$emp_code,
                'createdate'=>date('Y-m-d H:i:s'),
                'updateby'=>$emp_code,
                'updatedate'=>date('Y-m-d H:i:s')];

            $sqlapproved = 'SELECT * FROM '.$db['fsctaccount'].'.approved_pr WHERE id_prhead ='.$data['data']['id'].' ';

            $modelapproved = DB::connection('mysql')->select($sqlapproved);

            if($modelapproved){

                $updateapproved = DB::connection('mysql')->table($db['fsctaccount'].'.'.'approved_pr')->where('id_prhead',$data['data']['id'])->update(['status'=>99]);
                $insertapproved = DB::connection('mysql')->table($db['fsctaccount'].'.'.'approved_pr')->insert($datainsertapproved);
            }else{
//
                $insertapproved = DB::connection('mysql')->table($db['fsctaccount'].'.'.'approved_pr')->insert($datainsertapproved);

            }

            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'ppr_head')->where('id',$data['data']['id'])->update($data['data']);

            return $data['data']['id'];

        }





    }

    public function insertheadpoall(){
      $db = Connectdb::Databaseall();
      $data= Input::all();
      DB::beginTransaction();
      try {

          function getidpo($id_company, $i){
              $brcode = Session::get('brcode');
              $db = Connectdb::Databaseall();
              $sql = "SELECT COUNT(id) as idgen FROM $db[fsctaccount].po_head WHERE branch_id = '$brcode' AND id_company = '$id_company'";
              $bbr_no_branch = DB::connection('mysql')->select($sql);
              $numberrun = 0;

              $onset =  $bbr_no_branch[0]->idgen;

              if($onset == 0){
                  $numberrun = 1;
              }else{
                  $numberrun = (int)($onset)+$i;
              }
              $yearnow = date('Y');
              $yearth = $yearnow + 543;
              $yearth =  substr($yearth,2,2);
              $idgen = 'PO'.$brcode.$yearth.date('m').str_pad($numberrun, 3, "0", STR_PAD_LEFT);
              // $datagen =[ 'idgen'=>$idgen,
              //     'idnumber'=>$numberrun
              // ] ;
              $datagen = $idgen;
              return $datagen;
          }

          function po_no_branch_gen($brcode, $id_company){
            $db = Connectdb::Databaseall();
            $sql = "SELECT count(id) AS count_po_no FROM $db[fsctaccount].po_head WHERE branch_id = '$brcode' AND id_company = '$id_company'";
            $result = DB::connection('mysql')->select($sql);
            return $result[0]->count_po_no;
          }

          // getidpo();
          $no_po_now = po_no_branch_gen($data['branch_id'], $data['id_company']);
          $data['urgent_status'] = 2;
          $data['code_sup'] = NULL;
          $data['ckaddbranch'] = 1;

          $db = Connectdb::Databaseall();
          $brcode = Session::get('brcode');
          // return $brcode;
          // DB::connection('mysql')->select("set names 'utf8'");
          $sql = "SELECT address FROM $db[hr_base].branch WHERE code_branch = '$brcode' AND branch.status = '1'";
          $result_address = DB::connection('mysql')->select($sql);


          $data['address_send'] = $result_address[0]->address;
          $data['emp_code'] = Session::get('emp_code');
          $data['code_emp_approve'] = 0;
          $data['totolsumall'] = 0;
          $data['payreal'] = NULL;
          $data['diffpay'] = NULL;
          $data['status_head'] = 0;
          $data['id_pv'] = 0;
          $data['status_pv'] = 0;

          foreach (  $data['vendor'] as $key => $value) {
            $sql = "SELECT terms_id FROM $db[fsctaccount].supplier INNER JOIN $db[fsctaccount].supplier_terms ON(supplier.terms_id = supplier_terms.id)
                    WHERE supplier.id = '$value' AND supplier.status = '1'";
            $result_address = DB::connection('mysql')->select($sql);
            $data['terms_show'][] = $result_address[0]->terms_id;
          }

          foreach ($data['vendor'] as $key => $value) {
            $data_insert_head[] = [
              'week' => $data['week'],
              'date' => $data['datenow'],
              'year' => $data['year'],
              'year_th' => $data['year_th'],
              'year_th' => $data['year_th'],
              'branch_id' => $data['branch_id'],
              'po_number' => getidpo($data['id_company'], ($key + 1)),
              'po_no_branch' => ++$no_po_now,
              'supplier_id' => $data['vendor'][$key],
              'type_buy' => $data['type_buy'][$key],
              'type_pay' => $data['type_pay'][$key],
              'in_house' => $data['in_house'][$key],
              'in_budget' => $data['in_budget'][$key],
              'urgent_status' => $data['urgent_status'],
              'code_sup' => NULL,
              'id_company' => $data['id_company'],
              'id_company' => $data['id_company'],
              'ckaddbranch' => 1,
              'address_send' => $data['address_send'],
              'transfer_config_id' => $data['type_transfer'][$key],
              'transfer_config_id' => $data['type_transfer'][$key],
              'emp_code_po' => $data['emp_code'],
              'code_emp_approve' => 0,
              'terms' => $data['terms_show'][$key],
              'terms' => $data['terms_show'][$key],
              'vat' => 7,
              'totolsumall' => 0,
              'totolsumall' => 0,
              'payreal' => NULL,
              'status_head' => 0,
            ];
          }
          // echo "<pre>";
          // print_r($data_insert_head);
          // exit();
          $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_head')->insert($data_insert_head); //open now


          $sql = "SELECT sum(total) AS total, sum(withhold) AS withhold, sum(amount) AS amount, config_group_supp_id, materialid, list, type_amount, price, id_supplier
                  FROM $db[fsctaccount].ppr_head INNER JOIN $db[fsctaccount].ppr_detail ON (ppr_head.id = ppr_detail.ppr_headid)
                  WHERE ppr_head.branch_id = '$brcode'
                        AND ppr_head.week = '$data[week]'
                        AND ppr_detail.approvedstatus = '1'
                        AND ppr_detail.status = '1'
                        AND ppr_detail.approvedstatusmd = '1'
                        GROUP BY materialid
                        ";
          $result_po_detail = DB::connection('mysql')->select($sql); //group each pr
          // echo "<pre>";
          // print_r($result_po_detail);
          // exit();


          // $result_prdetail[0]->materialid = 447;
          // return count($data_insert_head);
          // var_dump($data_insert_head);
          foreach ($data_insert_head as $key => $value) {
            // echo $value['po_number']."<br>";
            $sql = "SELECT id AS po_id, supplier_id FROM $db[fsctaccount].po_head
                    WHERE po_number = '$value[po_number]'";
            $result_find_supplier_id = DB::connection('mysql')->select($sql);
            if($result_find_supplier_id){ // step 1 check po_number find supplier_id
              // echo "<pre>";
              // print_r($result_find_supplier_id);
              // exit();
              foreach ($result_po_detail as $key => $value) { // step 2 loop po_detail
                // code...
                if($value->id_supplier == $result_find_supplier_id[0]->supplier_id){ //step 3 check po_detail match result query
                  $result_po_detail[$key]->po_headid = $result_find_supplier_id[0]->po_id;
                  $result_po_detail[$key]->quantity_get = 0;
                  $result_po_detail[$key]->quantity_loss = 0;
                  $result_po_detail[$key]->status = 1;
                  $result_po_detail[$key]->statususe = 1;
                  // unset($result_po_detail[$key]->id_supplier);
                }
              }
            }
            // echo "<pre>";
            // print_r($result_address);
            // exit();
          }


          foreach ($result_po_detail as $key => $value) { //obj to array
            $result_po_detail[$key] = (array)$result_po_detail[$key];
            unset($result_po_detail[$key]['id_supplier']); //remove
          }

          $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_detail')->insert($result_po_detail);

          // echo "<pre>";
          // print_r($data);
          // exit();

          $sql_pr_ = "SELECT ppr_head.id AS pr_id, ppr_detail.materialid AS materialid, number_ppr AS pr_number  FROM $db[fsctaccount].ppr_head
                 INNER JOIN $db[fsctaccount].ppr_detail ON $db[fsctaccount].ppr_head.id = $db[fsctaccount].ppr_detail.ppr_headid
                 WHERE week ='$data[week]'
                 AND id_company  ='$data[id_company]'
                 AND vat = '$data[vat]'
                 AND branch_id = '$data[branch_id]'";
                 // return $sqlhead;
                 //GROUP BY $db[fsctaccount].supplier.id
         $resuit_sql_pr_ = DB::connection('mysql')->select($sql_pr_);

         $sql_po_ = "SELECT po_head.id AS po_id, po_detail.materialid AS materialid, po_number  FROM $db[fsctaccount].po_head
                INNER JOIN $db[fsctaccount].po_detail ON $db[fsctaccount].po_head.id = $db[fsctaccount].po_detail.po_headid
                WHERE week ='$data[week]'
                AND id_company  ='$data[id_company]'
                AND vat = '$data[vat]'
                AND branch_id = '$data[branch_id]'";
                // return $sqlhead;
                //GROUP BY $db[fsctaccount].supplier.id
         $resuit_sql_po_ = DB::connection('mysql')->select($sql_po_);

          foreach ($resuit_sql_po_ as $k_po => $v_po) {
            // echo $value->materialid."<br>";
            foreach ($resuit_sql_pr_ as $k_pr => $v_pr) {
              if($v_pr->materialid == $v_po->materialid){
                $data_po_ref_pr[] = [
                  'po_id' => $v_po->po_id,
                  'ponumber' => $v_po->po_number,
                  'pr_id' => $v_pr->pr_id,
                  'pr_number' => $v_pr->pr_number,
                  'config_group_supp_id' => 1,
                  'material_id' => $v_po->materialid,
                  'createby' => $data['emp_code'],
                  'createdate' => date('Y-m-d H:m:s'),
                  'status' => 1
                ];
              }
            }
          }

         // echo "<pre>";
         // print_r($data_po_ref_pr);
         // exit();
          unset($data);
          $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_ref_pr')->insert($data_po_ref_pr);
          // if($model) return 1;
          // return $lastid = DB::getPdo()->lastInsertId();
          DB::commit();
          if($model) return 1;

      } catch (\Exception $e) {
          DB::rollback();

      }




    }

    public function insertdetailppr(){

        $data= Input::all();



        unset($data['data']['transfer_money']); //ลบเงินโอน
        $db = Connectdb::Databaseall();
        $idhead = $data['data']['idhead'];
        $sql ='SELECT * FROM '.$db['fsctaccount'].'.ppr_detail WHERE ppr_headid = "'.$idhead.'" AND status ="1"';
        $model = DB::connection('mysql')->select($sql);
        $newarr = $data['data'];



        if(!$model){
            //print_r($data);

            $arrvalue = [];


            foreach ($newarr['config_group_supp_id'] as $k =>$v){
                $arrvalue[$k]['config_group_supp_id']= $v;
                $arrvalue[$k]['approvedstatus']= $newarr['arrcheckapproved'][$k];
                $arrvalue[$k]['approvedstatusmd']= $newarr['arrcheckapprovedmd'][$k];
                $arrvalue[$k]['id_supplier']= $newarr['arridsupplier'][$k];
                $arrvalue[$k]['materialid']= $newarr['arrmaterialid'][$k];
                $arrvalue[$k]['list']= $newarr['arrlist'][$k];
                $arrvalue[$k]['amount']= $newarr['arramount'][$k];
                $arrvalue[$k]['type_amount']= $newarr['arrtype_amount'][$k];
                $arrvalue[$k]['price']= $newarr['arrprice'][$k];
                $arrvalue[$k]['vat']= $newarr['arrvat'][$k];
                $arrvalue[$k]['withhold']= $newarr['arrwithhold'][$k];
                $arrvalue[$k]['total']= $newarr['arrtotal'][$k];
                $arrvalue[$k]['daterecentpurchases']= $newarr['arrdaterecentpurchases'][$k];
                $arrvalue[$k]['avg']= $newarr['arravg'][$k];
                $arrvalue[$k]['note']= $newarr['arrnote'][$k];
                $arrvalue[$k]['status']= 1;
                $arrvalue[$k]['ppr_headid']= $newarr['idhead'];
            }

            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'ppr_detail')->insert($arrvalue);


        }else{

            foreach ($newarr['config_group_supp_id'] as $k =>$v){
                $arrvalue[$k]['config_group_supp_id']= $v;
                $arrvalue[$k]['approvedstatus']= $newarr['arrcheckapproved'][$k];
                $arrvalue[$k]['approvedstatusmd']= $newarr['arrcheckapprovedmd'][$k];
                $arrvalue[$k]['id_supplier']= $newarr['arridsupplier'][$k];
                $arrvalue[$k]['materialid']= $newarr['arrmaterialid'][$k];
                $arrvalue[$k]['list']= $newarr['arrlist'][$k];
                $arrvalue[$k]['amount']= $newarr['arramount'][$k];
                $arrvalue[$k]['type_amount']= $newarr['arrtype_amount'][$k];
                $arrvalue[$k]['price']= $newarr['arrprice'][$k];
                $arrvalue[$k]['vat']= $newarr['arrvat'][$k];
                $arrvalue[$k]['withhold']= $newarr['arrwithhold'][$k];
                $arrvalue[$k]['total']= $newarr['arrtotal'][$k];
                $arrvalue[$k]['daterecentpurchases']= $newarr['arrdaterecentpurchases'][$k];
                $arrvalue[$k]['avg']= $newarr['arravg'][$k];
                $arrvalue[$k]['note']= $newarr['arrnote'][$k];
                $arrvalue[$k]['status']= 1;
                $arrvalue[$k]['ppr_headid']= $newarr['idhead'];
            }

            $modelupdate = DB::connection('mysql')->table($db['fsctaccount'].'.'.'ppr_detail')->where('ppr_headid',$arrvalue[0]['ppr_headid'])->update(['status'=>99]);

            $model2 = DB::connection('mysql')->table($db['fsctaccount'].'.'.'ppr_detail')->insert($arrvalue);


        }

        return 1;

    }



    public function getdatavendordetail(){
        $data= Input::all();
        $db = Connectdb::Databaseall();
        $idhead = $data['id'];
        $sql ='SELECT * FROM '.$db['fsctaccount'].'.ppr_head WHERE id = "'.$idhead.'" ';
        $model = DB::connection('mysql')->select($sql);


        return $model;
    }

    public function getdatavendorpprdetail(){
        $data= Input::all();
        $db = Connectdb::Databaseall();
        $idhead = $data['id'];
        $sql ='SELECT * FROM '.$db['fsctaccount'].'.ppr_detail WHERE ppr_headid = "'.$idhead.'"   AND status = "1" ';
        $model = DB::connection('mysql')->select($sql);

        return $model;
    }


    public function getidpr(){
        $data= Input::all();
//        print_r($data);
//        exit;
         $yearnow = date('Y');


        $brcode = Session::get('brcode');
        $db = Connectdb::Databaseall();
        $sql = "SELECT COUNT(id) as idgen FROM $db[fsctaccount].ppr_head WHERE branch_id = '$brcode' AND id_company = '$data[id]' AND year = '$yearnow'";
        $bbr_no_branch = DB::connection('mysql')->select($sql);
        $numberrun = 0;

        $onset =  $bbr_no_branch[0]->idgen;

        if($onset == 0){
            $numberrun = 1;
        }else{
            $numberrun = (int)($onset)+1;
        }

        $yearth = $yearnow + 543;
        $yearth =  substr($yearth,2,2);
        $idgen = 'PR'.$brcode.$yearth.date('m').str_pad($numberrun, 3, "0", STR_PAD_LEFT);
        $datagen =[ 'idgen'=>$idgen,
            'idnumber'=>$numberrun
        ] ;


        return json_encode($datagen);
    }



    public function addpo(){
        return view('createpo');
    }

    public function addpoall(){
        return view('createpoall');
    }

    public function detail_choosesupplier(){
        return view('detail_choosesupplier');
    }

    public function getidpo(){
        $data= Input::all();
//        print_r($data);
//        exit;


        $brcode = Session::get('brcode');
        $db = Connectdb::Databaseall();
        $sql = "SELECT COUNT(id) as idgen FROM $db[fsctaccount].po_head WHERE branch_id = '$brcode' AND id_company = '$data[id]'";
        $bbr_no_branch = DB::connection('mysql')->select($sql);
        $numberrun = 0;

        $onset =  $bbr_no_branch[0]->idgen;

        if($onset == 0){
            $numberrun = 1;
        }else{
            $numberrun = (int)($onset)+1;
        }
        $yearnow = date('Y');
        $yearth = $yearnow + 543;
        $yearth =  substr($yearth,2,2);
        $idgen = 'PO'.$brcode.$yearth.date('m').str_pad($numberrun, 3, "0", STR_PAD_LEFT);
        $datagen =[ 'idgen'=>$idgen,
            'idnumber'=>$numberrun
        ];

        return json_encode($datagen);
    }


    public function getdataprbycodepr(){

        $data= Input::all();

        $db = Connectdb::Databaseall();
        $brcode = Session::get('brcode');

        // exit("SELECT id FROM $db[fsctaccount].ppr_head
        //         WHERE week ='$data[week]'
        //         AND id_company  ='$data[idcompany]'");

         $sqlhead = "SELECT id FROM $db[fsctaccount].ppr_head
                WHERE week ='$data[week]'
                AND id_company  ='$data[idcompany]'
                AND branch_id = '$brcode'
                AND type_ppr = '0' ";

        $modelhead = DB::connection('mysql')->select($sqlhead);

        $countitem = count($modelhead);

        if($countitem>1){
            $arridhead = [];
            foreach ($modelhead as $value) {
                  $arridhead[] = $value->id;
            }
            $arrid = implode(",",$arridhead);
        }else{
            // print_r($modelhead);
            // exit();
            $arrid = $modelhead[0]->id;

        }


           $sqldetail =  "SELECT amount ,
                    price,
                    vat,
                    withhold,
                    materialid,
                    list,
                    type_amount,
                    config_group_supp_id,
                    note
                FROM $db[fsctaccount].ppr_detail
                WHERE ppr_headid IN ($arrid)
                AND status  ='1'
                AND approvedstatusmd = '1'
                AND id_supplier = '$data[supplier]'
                AND vat = '$data[vat]'
                AND approvedstatus = '1'";




        $modeldetail = DB::connection('mysql')->select($sqldetail);
        // print_r($modeldetail);
        // exit();
        return $modeldetail;
    }

    public function getdataprbyweekall(){

        $data= Input::all();
        // print_r($data);
        // exit;
        $db = Connectdb::Databaseall();

        // exit("SELECT id FROM $db[fsctaccount].ppr_head
        //         WHERE week ='$data[week]'
        //         AND id_company  ='$data[idcompany]'");

         $sqlhead = "SELECT CONCAT(pre, name_supplier) AS supplier_name, supplier.id AS supplier_id FROM $db[fsctaccount].ppr_head
                INNER JOIN $db[fsctaccount].ppr_detail ON $db[fsctaccount].ppr_head.id = $db[fsctaccount].ppr_detail.ppr_headid
                INNER JOIN $db[fsctaccount].supplier ON $db[fsctaccount].ppr_detail.id_supplier = $db[fsctaccount].supplier.id
                INNER JOIN $db[fsctaccount].supplier_terms ON $db[fsctaccount].supplier.terms_id = $db[fsctaccount].supplier_terms.id
                WHERE week ='$data[week]'
                AND supplier_terms.status = '1'
                AND id_company  ='$data[idcompany]'
                AND vat = '$data[vat]'
                AND branch_id = '$data[branch_id]'
                GROUP BY $db[fsctaccount].supplier.id";
                // return $sqlhead;
                //GROUP BY $db[fsctaccount].supplier.id
        $modelhead = DB::connection('mysql')->select($sqlhead);

        // print_r($modelhead);
        // exit;
        if($modelhead) return response()->json($modelhead);
        else return 0;
    }


    public function getdatacompany(){
        $data= Input::all();

        $db = Connectdb::Databaseall();
        $sql = "SELECT * FROM $db[hr_base].working_company WHERE  id = '$data[id]'";
        $model = DB::connection('mysql')->select($sql);

        return $model;
    }

    public function addpoinsertupdate(){
        $data= Input::all();


        $db = Connectdb::Databaseall();

        $transfer_money = $data['data']['transfer_money'];
        unset($data['data']['transfer_money']);

        // echo "string";
        // dd($data);
        // exit;

        if($data['data']['id'] == ""){

            $data= Input::all();

            // echo "<pre>";
            // print_r()



          $brcode = Session::get('brcode');
          $idcompany = $data['data']['id_company'];
          $db = Connectdb::Databaseall();
          $yearnow = date('Y');
          $month = date('m');

            $sql = "SELECT COUNT(id) as idgen
                      FROM $db[fsctaccount].po_head
                      WHERE branch_id = '$brcode'
                      AND id_company = '$idcompany'
                      AND year = '$yearnow'
                      AND MONTH($db[fsctaccount].po_head.date) = '$month'
                      GROUP  BY YEAR($db[fsctaccount].po_head.date), MONTH($db[fsctaccount].po_head.date) ";



          $bbr_no_branch = DB::connection('mysql')->select($sql);
          $numberrun = 0;

          if(!empty($bbr_no_branch[0]->idgen)){
              $onset =  $bbr_no_branch[0]->idgen;
          }else{
              $onset = 0;
          }


            if($onset == 0){
                $numberrun = 1;
            }else{
                $numberrun = (int)($onset)+1;
            }
          $yearnow = date('Y');
          $yearth = $yearnow + 543;
          $yearth =  substr($yearth,2,2);
          $idgen = 'PO'.$brcode.$yearth.date('m').str_pad($numberrun, 3, "0", STR_PAD_LEFT);
          $datagen =[ 'idgen'=>$idgen,
              'idnumber'=>$numberrun
          ] ;


             $data['data']['po_number'] = $idgen;
             $data['data']['po_no_branch'] = $numberrun;
             $transfer_money = $data['data']['transfer_money'];
             unset($data['data']['transfer_money']);






            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_head')->insertGetId($data['data']);
            $lastid_poheadid = $model;
            // foreach ($transfer_money as $key => $value) {
            //   $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'moneytrasnfer')->insert(
            //     ['payreal' => $value,
            //       'status' => 0,
            //       'createat' => date("Y-m-d H:i:s"),
            //       'statususe' => 1,
            //       'updateat' => date("Y-m-d H:i:s"),
            //       'po_headid' => $lastid_poheadid
            //     ]
            //   );
            // }


            // print_r($data);
            $totolsumall = $data['data']['totolsumall'];
            $totolsumreal = $data['data']['totolsumreal'];

            $diffmoney = $totolsumall-$totolsumreal;
            if($diffmoney>0){
                //// ขาด

                $arrvatdiff = ['id'=>'',
                               'id_po'=>$lastid_poheadid,
                               'vatdiff'=>$diffmoney,
                               'status'=>1,
                               'type'=>1];

                $modelhead = DB::connection('mysql')->table($db['fsctaccount'].'.'.'vat_diff')->insert($arrvatdiff);

            }else{
              //// เกิน

                $arrvatdiff = ['id'=>'',
                               'id_po'=>$lastid_poheadid,
                               'vatdiff'=>$diffmoney,
                               'status'=>1,
                               'type'=>2];

                $modelhead = DB::connection('mysql')->table($db['fsctaccount'].'.'.'vat_diff')->insert($arrvatdiff);

            }


                return $lastid_poheadid;
            // return $lastid = DB::getPdo()->lastInsertId();
        }else{
            // $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_head')->where('id',$data['data']['id'])->update($data['data']);
            //
            // foreach ($transfer_money as $key => $value) {
            //   $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'moneytrasnfer')->where('id',$data['data']['id'])->update(
            //     ['payreal' => $value,
            //       'status' => 0,
            //       'createat' => date("Y-m-d H:i:s"),
            //       'statususe' => 1,
            //       'updateat' => date("Y-m-d H:i:s"),
            //       'po_headid' => $data['data']['id']
            //     ]
            //   );
            // }
            //
            // return $data['data']['id'];
        }

    }



    public function insertdetailpo(){

        $data= Input::all();
        // echo "<pre>";
        // print_r($data);
        // exit();

        $db = Connectdb::Databaseall();
        $idhead = $data['data']['idhead'];
        $sql ='SELECT * FROM '.$db['fsctaccount'].'.po_detail WHERE po_headid = "'.$idhead.'" ';
        $model = DB::connection('mysql')->select($sql);
        $newarr = $data['data'];

        if(!$model){
            $arrvalue = [];


            foreach ($newarr['config_group_supp_id'] as $k =>$v){
                $arrvalue[$k]['config_group_supp_id']= $v;
                $arrvalue[$k]['materialid']= $newarr['arrmaterialid'][$k];
                $arrvalue[$k]['list']= $newarr['arrlist'][$k];
                $arrvalue[$k]['amount']= $newarr['arramount'][$k];
                $arrvalue[$k]['type_amount']= $newarr['arrtype_amount'][$k];
                $arrvalue[$k]['price']= $newarr['arrprice'][$k];
                $arrvalue[$k]['note']= $newarr['arrnote'][$k];
                //$arrvalue[$k]['vat']= $newarr['arrvat'][$k];
                // $arrvalue[$k]['withhold']= $newarr['arrwithhold'][$k];
                $arrvalue[$k]['total']= $newarr['arrtotal'][$k];
                $arrvalue[$k]['quantity_get']= $newarr['quantity_get'][$k];
                $arrvalue[$k]['quantity_loss']= $newarr['quantity_loss'][$k];
                $arrvalue[$k]['status']= 1;
                $arrvalue[$k]['statususe']= 1;
                $arrvalue[$k]['po_headid']= $newarr['idhead'];
            }

            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_detail')->insert($arrvalue);


        }else{

            foreach ($newarr['config_group_supp_id'] as $k =>$v){
                $arrvalue[$k]['config_group_supp_id']= $v;
                $arrvalue[$k]['materialid']= $newarr['arrmaterialid'][$k];
                $arrvalue[$k]['list']= $newarr['arrlist'][$k];
                $arrvalue[$k]['amount']= $newarr['arramount'][$k];
                $arrvalue[$k]['type_amount']= $newarr['arrtype_amount'][$k];
                $arrvalue[$k]['price']= $newarr['arrprice'][$k];
                //$arrvalue[$k]['vat']= $newarr['arrvat'][$k];
                // $arrvalue[$k]['withhold']= $newarr['arrwithhold'][$k];
                $arrvalue[$k]['total']= $newarr['arrtotal'][$k];
                $arrvalue[$k]['quantity_get']= $newarr['quantity_get'][$k];
                $arrvalue[$k]['quantity_loss']= $newarr['quantity_loss'][$k];
                $statusresult =1;
                if($newarr['arramount'][$k] != $newarr['quantity_get'][$k]){
                    $arrvalue[$k]['status']= $statusresult;
                }else if($newarr['arramount'][$k] == $newarr['quantity_get'][$k]){
                    $arrvalue[$k]['status']= 2;
                }

                $arrvalue[$k]['statususe']= 1;
                $arrvalue[$k]['po_headid']= $newarr['idhead'];
            }

            $modelupdate = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_detail')->where('po_headid',$arrvalue[0]['po_headid'])->update(['statususe'=>99]);

            $model2 = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_detail')->insert($arrvalue);

        }

        $sqlupdatehead1 ='SELECT * FROM '.$db['fsctaccount'].'.po_detail WHERE po_headid = "'.$idhead.'" AND statususe = "1"  AND status = "1"';

        $modelupdatehead1 = DB::connection('mysql')->select($sqlupdatehead1);

        if(count($modelupdatehead1)==0){
            $modelupdateheadpo = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_head')->where('id',$idhead)->update(['status_head'=>3]);
        }
        return 1;


    }

    public function getdatapoupdate(){
        $data= Input::all();



        $db = Connectdb::Databaseall();
         $sql ='SELECT '.$db['fsctaccount'].'.po_head.* ,
                 CONCAT('.$db['fsctaccount'].'.supplier.pre,\' \','.$db['fsctaccount'].'.supplier.name_supplier) as Fullnameauto,
                 '.$db['fsctaccount'].'.type_buy.name_buy as type_buy_name,
                 '.$db['fsctaccount'].'.type_pay.name_pay as type_pay_name,
                 '.$db['fsctaccount'].'.supplier_terms.name as supplier_termsname
               FROM '.$db['fsctaccount'].'.po_head
               INNER JOIN '.$db['fsctaccount'].'.supplier ON  '.$db['fsctaccount'].'.supplier.id = '.$db['fsctaccount'].'.po_head.supplier_id
               INNER JOIN '.$db['fsctaccount'].'.type_buy ON  '.$db['fsctaccount'].'.type_buy.id = '.$db['fsctaccount'].'.po_head.type_buy
               INNER JOIN '.$db['fsctaccount'].'.type_pay ON  '.$db['fsctaccount'].'.type_pay.id = '.$db['fsctaccount'].'.po_head.type_pay
               INNER JOIN '.$db['fsctaccount'].'.supplier_terms ON  '.$db['fsctaccount'].'.supplier_terms.id = '.$db['fsctaccount'].'.supplier.terms_id
               WHERE '.$db['fsctaccount'].'.po_head.id = "'.$data['id'].'" ';

        $model = DB::connection('mysql')->select($sql);

        return $model;

    }

    public function getdatapodetailupdate(){
        $data = Input::all();

//        print_r($data);

        $data= Input::all();
        $db = Connectdb::Databaseall();
        $idhead = $data['id'];
        $sql ='SELECT * FROM '.$db['fsctaccount'].'.po_detail WHERE po_headid = "'.$data['id'].'"   AND statususe = "1" ';
        $model = DB::connection('mysql')->select($sql);

        return $model;
    }



    public function insertlogbillrunstock(){
        $data = Input::all();

        $db = Connectdb::Databaseall();
        $emp_code = Session::get('emp_code');
        $idhead = $data['data']['idhead'];
        $sql ='SELECT * FROM '.$db['fsctaccount'].'.po_bill_run_stock_log WHERE id_po_ref = "'.$idhead.'" ';
        $model = DB::connection('mysql')->select($sql);
        $newarr = $data['data'];

       // print_r($newarr);
       // exit;

        $arrvalue = [];

//        $count = count($newarr);
//

        if(!$model){
            foreach ($newarr['config_group_supp_id'] as $k =>$v){
                $arrvalue[$k]['bill_no']= end($newarr['arrbillref']);
                $arrvalue[$k]['id_material']= $newarr['arrmaterialid'][$k];
                $arrvalue[$k]['id_po_ref']= $newarr['idhead'];
                $arrvalue[$k]['list']= $newarr['arrlist'][$k];
                $arrvalue[$k]['amount']= $newarr['arramount'][$k];
                $arrvalue[$k]['quantity_get']= $newarr['quantity_get'][$k];
                $arrvalue[$k]['quantity_get_now']= $newarr['quantity_get'][$k];
                $arrvalue[$k]['quantity_get_dif']= $newarr['quantity_get'][$k]-0;
                $arrvalue[$k]['quantity_loss']= $newarr['quantity_loss'][$k];
                $arrvalue[$k]['code_emp']= $emp_code;
            }
             // print_r($arrvalue);

            $modelupdate = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_bill_run_stock_log')->insert($arrvalue);

            return end($newarr['arrbillref']);

        }else{

            foreach ($newarr['config_group_supp_id'] as $k =>$v){
                $listname = $newarr['arrlist'][$k];
                $materialid = $newarr['arrmaterialid'][$k];


                $sqllast = 'SELECT quantity_get FROM '.$db['fsctaccount'].'.po_bill_run_stock_log WHERE id IN (SELECT MAX(id) as id
                FROM '.$db['fsctaccount'].'.po_bill_run_stock_log
                WHERE id_po_ref = "'.$idhead.'" AND id_material = "'.$materialid.'")';
                $qtylast = DB::connection('mysql')->select($sqllast);
                 // print_r($qtylast);
                 // exit;
                $arrvalue[$k]['bill_no']= end($newarr['arrbillref']);
                  $arrvalue[$k]['id_material']= $newarr['arrmaterialid'][$k];
                $arrvalue[$k]['id_po_ref']= $newarr['idhead'];
                $arrvalue[$k]['list']= $newarr['arrlist'][$k];
                $arrvalue[$k]['amount']= $newarr['arramount'][$k];
                $arrvalue[$k]['quantity_get']= $newarr['quantity_get'][$k];
                $arrvalue[$k]['quantity_get_now']= $newarr['quantity_get'][$k]-$qtylast[0]->quantity_get;
                $arrvalue[$k]['quantity_get_dif']= $newarr['quantity_get'][$k]-$qtylast[0]->quantity_get;
                $arrvalue[$k]['quantity_loss']= $newarr['quantity_loss'][$k];
                $arrvalue[$k]['code_emp']= $emp_code;
            }
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_bill_run_stock_log')->insert($arrvalue);
            return end($newarr['arrbillref']);

        }




    }

    public function insertbillmoney(){

        $data= Input::all();

        $emp_code = Session::get('emp_code');
        $db = Connectdb::Databaseall();
        $idhead = $data['data']['idhead'];
        $sql ='SELECT * FROM '.$db['fsctaccount'].'.po_bill_run_money WHERE po_headid = "'.$idhead.'" ';
        $model = DB::connection('mysql')->select($sql);
        $newarr = $data['data'];

        if(!$model){
            $arrvalue = [];


            foreach ($newarr['arrbillref'] as $k =>$v){
                $arrvalue[$k]['po_headid']= $newarr['idhead'];
                $arrvalue[$k]['datebill']= $newarr['arrdatebill'][$k];
                $arrvalue[$k]['bill_no']= $v;
                $arrvalue[$k]['pay_real']= $newarr['arrpayperbill'][$k];
                $arrvalue[$k]['returnperbill']= $newarr['arrreturnperbill'][$k];
                $arrvalue[$k]['status']= 1;
                $arrvalue[$k]['code_emp']= $emp_code;
                $arrvalue[$k]['createby']= $emp_code;
                $arrvalue[$k]['createdate']= date('Y-m-d H:i:s');

            }

            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_bill_run_money')->insert($arrvalue);


        }else{

            foreach ($newarr['arrbillref'] as $k =>$v){
                $arrvalue[$k]['po_headid']= $newarr['idhead'];
                $arrvalue[$k]['datebill']= $newarr['arrdatebill'][$k];
                $arrvalue[$k]['bill_no']= $v;
                $arrvalue[$k]['pay_real']= $newarr['arrpayperbill'][$k];
                $arrvalue[$k]['returnperbill']= $newarr['arrreturnperbill'][$k];
                $arrvalue[$k]['status']= 1;
                $arrvalue[$k]['code_emp']= $emp_code;
                $arrvalue[$k]['createby']= $emp_code;
                $arrvalue[$k]['createdate']= date('Y-m-d H:i:s');

            }
            $modelupdate = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_bill_run_money')->where('po_headid',$arrvalue[0]['po_headid'])->update(['status'=>99]);

            $model2 = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_bill_run_money')->insert($arrvalue);


        }

        return 1;

    }

    public function getpobillrunmoney(){

        $data= Input::all();
        $db = Connectdb::Databaseall();
        $idhead = $data['id'];
        $sql ='SELECT * FROM '.$db['fsctaccount'].'.po_bill_run_money WHERE po_headid = "'.$data['id'].'"   AND status = "1" ';
        $model = DB::connection('mysql')->select($sql);

        return $model;
    }

    public function getdataaccounttype(){
        $data = Input::all();
        $db = Connectdb::Databaseall();
        $sql ='SELECT * FROM '.$db['fsctaccount'].'.accounttype WHERE config_group_supp_id = "'.$data['idacc'].'"   AND status = "1" ';
        $model = DB::connection('mysql')->select($sql);

        return $model;

    }

    public function transferpo(){

        return view('transferpo');
    }

    public function inserttransfertostockacc(){

        DB::beginTransaction();
        try {
            $data= Input::all();

            $emp_code = Session::get('emp_code');
            $db = Connectdb::Databaseall();
            $idhead = $data['data']['idhead'];
            $sql ='SELECT * FROM '.$db['fsctaccount'].'.po_detail WHERE po_headid = "'.$idhead.'" ';
            $model = DB::connection('mysql')->select($sql);
            $newarr = $data['data'];

            if(!$model){
                $arrvalue = [];


                foreach ($newarr['config_group_supp_id'] as $k =>$v){
                    $arrvalue[$k]['config_group_supp_id']= $v;
                    $arrvalue[$k]['accounttype_id']= $newarr['arraccounttypeid'][$k];
                    $arrvalue[$k]['materialid']= $newarr['arrmaterialid'][$k];
                    $arrvalue[$k]['list']= $newarr['arrlist'][$k];
                    $arrvalue[$k]['amount']= $newarr['arramount'][$k];
                    $arrvalue[$k]['type_amount']= $newarr['arrtype_amount'][$k];
                    $arrvalue[$k]['price']= $newarr['arrprice'][$k];
                    $arrvalue[$k]['vat']= $newarr['arrvat'][$k];
                    $arrvalue[$k]['total']= $newarr['arrtotal'][$k];
                    $arrvalue[$k]['quantity_get']= $newarr['quantity_get'][$k];
                    $arrvalue[$k]['quantity_loss']= $newarr['quantity_loss'][$k];
                    $arrvalue[$k]['status']= 1;
                    $arrvalue[$k]['statususe']= 1;
                    $arrvalue[$k]['po_headid']= $newarr['idhead'];
                }

                $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_detail')->insert($arrvalue);


            }else{

                foreach ($newarr['config_group_supp_id'] as $k =>$v){
                    $arrvalue[$k]['config_group_supp_id']= $v;
                    $arrvalue[$k]['accounttype_id']= $newarr['arraccounttypeid'][$k];
                    $arrvalue[$k]['materialid']= $newarr['arrmaterialid'][$k];
                    $arrvalue[$k]['list']= $newarr['arrlist'][$k];
                    $arrvalue[$k]['amount']= $newarr['arramount'][$k];
                    $arrvalue[$k]['type_amount']= $newarr['arrtype_amount'][$k];
                    $arrvalue[$k]['price']= $newarr['arrprice'][$k];
                    $arrvalue[$k]['vat']= $newarr['arrvat'][$k];
                    $arrvalue[$k]['total']= $newarr['arrtotal'][$k];
                    $arrvalue[$k]['quantity_get']= $newarr['quantity_get'][$k];
                    $arrvalue[$k]['quantity_loss']= $newarr['quantity_loss'][$k];
                    $statusresult =1;
                    if($newarr['arramount'][$k] != $newarr['quantity_get'][$k]){
                        $arrvalue[$k]['status']= $statusresult;
                    }else if($newarr['arramount'][$k] == $newarr['quantity_get'][$k]){
                        $arrvalue[$k]['status']= 2;
                    }

                    $arrvalue[$k]['statususe']= 1;
                    $arrvalue[$k]['po_headid']= $newarr['idhead'];
                }

                $modelupdate = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_detail')->where('po_headid',$arrvalue[0]['po_headid'])->update(['statususe'=>99]);

                $model2 = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_detail')->insert($arrvalue);

            }




//            print_r($newarr);

            $arrmaterial = [];
            $arrasset =[];


            foreach ($newarr['config_group_supp_id'] as $key => $value){
                if($newarr['arrmaterialid'][$key]!=0){//สินค้า
                    $arrmaterial[$key]['material_id']=$newarr['arrmaterialid'][$key];
                    $arrmaterial[$key]['company_id']=$newarr['company_id_transfer'];
                    $arrmaterial[$key]['branch_id']=$newarr['branch_id_transfer'];
                    $arrmaterial[$key]['time']=date('Y-m-d H:i:s');
                    $arrmaterial[$key]['type']=8;
                    $arrmaterial[$key]['amount']=$newarr['arramount'][$key];
                    $arrmaterial[$key]['ref']=$newarr['idhead'];

                    $model = DB::connection('mysql')->table($db['fsctmain'].'.'.'stock')->insert($arrmaterial);

                }else{//ไม่ใช่สินค้า

                    $arrasset[$key]['po_ref']=$newarr['idhead'];
                    $arrasset[$key]['config_group_supp_id']=$value;
                    $arrasset[$key]['account_id']=$newarr['arraccounttypeid'][$key];
                    $arrasset[$key]['company_id']=$newarr['company_id_transfer'];
                    $arrasset[$key]['branch_id']=$newarr['branch_id_transfer'];
                    $arrasset[$key]['type']=8;
                    $arrasset[$key]['list']=$newarr['arrlist'][$key];
                    $arrasset[$key]['price_unit']=$newarr['arrprice'][$key];
                    $arrasset[$key]['amount']=$newarr['arramount'][$key];
                    $arrasset[$key]['vat']=$newarr['arrvat'][$key];
                    $arrasset[$key]['withhold']=$newarr['arrwithhold'][$key];
                    $arrasset[$key]['total']=$newarr['arrtotal'][$key];
                    $arrasset[$key]['time']=date('Y-m-d H:i:s');
                    $arrasset[$key]['createby']=$emp_code;

                    $model = DB::connection('mysql')->table($db['fsctmain'].'.'.'asset')->insert($arrasset);


                }
            }



            DB::commit();
            return 1;
        } catch (\Exception $e) {
            DB::rollback();
        }

    }

    public function updatevendorstatus(){
        $data= Input::all();
        //print_r($data);
        $id = $data['data']['id'];
        $status = $data['data']['status'];

        $db = Connectdb::Databaseall();
        $modelupdate = DB::connection('mysql')->table($db['fsctaccount'].'.'.'supplier')->where('id',$id)->update(['status'=>$status]);

        return 1;

    }

    public function getprrefmaterialid(){
      $data= Input::all();
      // print_r($data);
      // exit;
      $db = Connectdb::Databaseall();
      $sqlhead = "SELECT id FROM $db[fsctaccount].ppr_head
              WHERE week ='$data[week]'
              AND id_company  ='$data[idcompany]'";
      $modelhead = DB::connection('mysql')->select($sqlhead);

      $arridhead = [];
      foreach ($modelhead as $value) {
            $arridhead[] = $value->id;
      }
      $arrid = implode(",",$arridhead);

      $countitem = count($modelhead);

      $sqldetail =  "SELECT  branch_id,
                  name_branch,
                  number_ppr,
                  list,
                  amount ,
                  price,
                  vat,
                  withhold,
                  materialid,
                  list,
                  type_amount,
                  config_group_supp_id,
                  total
              FROM $db[fsctaccount].ppr_detail
              INNER JOIN $db[fsctaccount].ppr_head
                ON $db[fsctaccount].ppr_detail.ppr_headid = $db[fsctaccount].ppr_head.id
              INNER JOIN $db[hr_base].branch
                ON $db[fsctaccount].ppr_head.branch_id = $db[hr_base].branch.code_branch
              WHERE ppr_headid IN ($arrid)
              AND $db[fsctaccount].ppr_detail.status  ='1'
              AND $db[fsctaccount].ppr_head.status != '99'
              AND materialid= $data[materialid]
              AND id_supplier = '$data[supplier]'";

      $modeldetail = DB::connection('mysql')->select($sqldetail);

      return $modeldetail;
    }

    public function insertporefpr(){

        $data= Input::all();

        // print_r($data);
        // exit;

        $emp_code = Session::get('emp_code');
        $db = Connectdb::Databaseall();
        $sqlhead = "SELECT id FROM $db[fsctaccount].ppr_head
                WHERE week ='$data[week]'
                AND id_company  ='$data[idcompany]'";
        $modelhead = DB::connection('mysql')->select($sqlhead);

        $arridhead = [];
        foreach ($modelhead as $value) {
              $arridhead[] = $value->id;
        }
        $arrid = implode(",",$arridhead);

        $countitem = count($modelhead);

        $sqldetail =  "SELECT $db[fsctaccount].ppr_head.number_ppr,
                    ppr_headid,
                    amount
                    price,
                    vat,
                    withhold,
                    materialid,
                    list,
                    type_amount,
                    config_group_supp_id
                FROM $db[fsctaccount].ppr_detail
                INNER JOIN $db[fsctaccount].ppr_head
                    ON $db[fsctaccount].ppr_head.id = $db[fsctaccount].ppr_detail.ppr_headid
                WHERE ppr_headid IN ($arrid)
                AND $db[fsctaccount].ppr_detail.status  ='1'
                AND id_supplier = '$data[supplier]'";

        $modeldetail = DB::connection('mysql')->select($sqldetail);

        // print_r($modeldetail);

        $i = 0;
        $arrdata = [];
        foreach ($modeldetail as $value) {
            $arrdata[$i]['po_id']=$data['idpo'];
            $arrdata[$i]['ponumber']=$data['po_number'];
            $arrdata[$i]['pr_id']=$value->ppr_headid;;
            $arrdata[$i]['pr_number']=$value->number_ppr;;
            $arrdata[$i]['config_group_supp_id']=$value->config_group_supp_id;;
            $arrdata[$i]['material_id']=$value->materialid;
            $arrdata[$i]['createby']=$emp_code;
            $arrdata[$i]['createdate']=date('Y-m-d H:i:s');
            $arrdata[$i]['status']='1';
      $i++;
        }

        $updateupdate = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_ref_pr')->where('po_id',$data['idpo'])->update(['status'=>99]);
        $updateupdate = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_ref_pr')->insert($arrdata);

         return 1;
    }



    public function printpaymentvoucher(){
        $data= Input::all();
        $id=$data['po_num'];
        $db = Connectdb::Databaseall();
        $sql = "SELECT * FROM $db[fsctaccount].po_head  WHERE id ='$id' ";
        $datahead = DB::connection('mysql')->select($sql);
        // echo "<pre>";
        // print_r($datahead);
    //    print($datahead[0]->status_pv);


        if($datahead[0]->status_pv==1){
            $data = [
                'id'=>$id
            ];
            $pdf = PDF::loadView('printpv', $data);
            return @$pdf->stream();
        }else{
            $brcode = Session::get('brcode');
            $countid = DB::table('po_head')->where('status_pv', 1)->where('branch_id', $brcode)->count();
            $countidone=$countid+1;
            $arrInsert['status_pv']=1;
            $arrInsert['id_pv']=date("Ym")."/"."$brcode"."-"."$countidone";
            $arrInsert['pv_date']= date("Y-m-d");
            //  print($arrInsert['id_pv']);
            //  exit;


            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'po_head')->where('id',$id)->update($arrInsert);
            return redirect()->back()->with('alert', 'สร้างใบสำคัญจ่ายสำเร็จ!');
        }



    }


    public function saveatmcash(){
        $data= Input::all();

        $atmmoney = $data['atmmoney'];
        $brcode = Session::get('brcode');
        $emp_code = Session::get('emp_code');
        $db = Connectdb::Databaseall();

        $arrInsert = ['id'=>'',
                      'cash'=>$atmmoney,
                      'branch'=>$brcode,
                      'code_emp'=>$emp_code,
                      'datetime'=>date('Y-m-d H:i:s'),
                      'status'=>1];

       $tranfermodel = DB::connection('mysql')->table($db['fsctaccount'].'.atmcash')->insertGetId($arrInsert);


       $arrCash = ['id'=>'',
                   'datetimeinsert'=>date('Y-m-d H:i:s'),
                   'money'=>$atmmoney,
                   'typedoc'=>11,
                   'emp_code'=>$emp_code,
                   'branch'=>$brcode,
                   'status'=>1,
                   'typetranfer'=>1,
                   'log'=>'กด ATM สาขา'.$brcode.' โดย '.$emp_code,
                   'ref'=>$tranfermodel,
                   'typereftax'=>$tranfermodel
                 ];

        $tranfermodel = DB::connection('mysql')->table($db['fsctaccount'].'.insertcashrent')->insertGetId($arrCash);


          return view('withdraw_money');



    }


    public function insertpopremony(){
          $data= Input::all();

          echo "<pre>";
          print_r($data);
          exit;


    }

    public function getcheckprpoduplicate(){
          $data= Input::all();

          return 0;
          exit;

          $db = Connectdb::Databaseall();
          $sqlhead = "SELECT id FROM $db[fsctaccount].po_head
                  WHERE week ='$data[week]'
                  AND id_company  ='$data[idcompany]'
                  AND supplier_id ='$data[supplier]'
                  AND vat = '$data[vat]'
                  AND status <> '99' ";
          $modelhead = DB::connection('mysql')->select($sqlhead);

          if(!empty($modelhead)){

                return 1;

          }else{
                return 0;

          }

    }


}
