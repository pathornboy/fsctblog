<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Illuminate\Support\Facades\Input;
use phpDocumentor\Reflection\Types\Null_;
use Session;

use App\bankcash;

class BankcashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(){
       $data =  bankcash::get();

        return view('bankcash',['data'=>$data]);
    }

    public function get()
    {
        $input = Input::all();
        //  $data =  bankcash::where('id','=', $input['id'])->get();
         $data =  bankcash::find($input['id']);
        return $data;
    }

    public function create()
    {
        $input = Input::all();
        bankcash::insert([
            'bank'=>$input['bank'],
            'status'=>1
        ]);
        return redirect()->back()->with('alert', 'บันทึกสำเร็จ!');
    }

    public function edit()
    {
        $input = Input::all();
        $data =  bankcash::find($input['id']);
        $data->accounttypeno = $input['accounttypeno'];
        $data->save();
        return redirect()->back()->with('alert', 'แก้ไขสำเร็จ!');
    }


    public function delete()
    {
        $input = Input::all();
        $data =  bankcash::find($input['data']['id']);
        $data->delete();
        return 1;
    }



}
