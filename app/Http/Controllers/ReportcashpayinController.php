<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Input;
use Excel;
use Session;
use App\Api\Connectdb;
use App\Api\Datetime;


class ReportcashpayinController extends Controller {

      public function reportpopayin(){

        return view('reportpopayin');
      }

      public function serachreportpopayin(){
          $data = Input::all();

          $branch = $data['branch'];
          $datepicker = $data['datepicker'];
          $dateranger = Datetime::FormatDateFromCalendarRange($datepicker);
          $start_date = $dateranger['start_date'];
          $end_date = $dateranger['end_date'];

          $db = Connectdb::Databaseall();
          if($branch=='*'){
            $sql = "SELECT $db[fsctaccount].po_head.*
                  FROM $db[fsctaccount].po_head
                  WHERE $db[fsctaccount].po_head.date BETWEEN '$start_date' AND '$end_date'
                  AND $db[fsctaccount].po_head.status_head IN (0,1,2,3,4,5)";
          }else{
            $sql = "SELECT $db[fsctaccount].po_head.*
                  FROM $db[fsctaccount].po_head
                  WHERE $db[fsctaccount].po_head.date BETWEEN '$start_date' AND '$end_date'
                  AND $db[fsctaccount].po_head.status_head IN (0,1,2,3,4,5)
                  AND $db[fsctaccount].po_head.branch_id = '$branch' ";

          }


          $dataresulte = DB::connection('mysql')->select($sql);
          // echo "<pre>";
          // print_r($dataresulte);
          // exit;

          return view('reportpopayin',['data'=>true,'branch'=>$branch,'datepicker'=>$datepicker,'dataresulte'=>$dataresulte]);




      }

}
