<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Api\Connectdb;

use App\Pprdetail;
use DB;
use Illuminate\Support\Facades\Input;
use phpDocumentor\Reflection\Types\Null_;
use Session;

class TaxwithholdsupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('taxwithholdsupplier');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function findpohead(Request $request)
    {

        // echo "<pre>";
        $po_number = $request->po_number;

        $db = Connectdb::Databaseall();
        $sql = "SELECT pre, name_supplier, address_send, tax_id
                FROM $db[fsctaccount].supplier as supplier INNER JOIN $db[fsctaccount].po_head as po_head ON(supplier.id = po_head.supplier_id)
                WHERE po_head.status_head = '1' AND po_number = '$po_number'";
        $result_vendor = DB::connection('mysql')->select($sql);
        // print_r($result_vendor);
        if($result_vendor){
          return response()->json([
              'result' => $result_vendor[0]
          ]);
        }else{
          return response()->json([
              'result' => NULL
          ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
