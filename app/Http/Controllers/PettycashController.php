<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Illuminate\Support\Facades\Input;
use phpDocumentor\Reflection\Types\Null_;
use Session;

use App\pettycash;

class PettycashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(){
       $data =  pettycash::get();

        return view('pettycash',['data'=>$data]);
    }

    public function get()
    {   
        $input = Input::all();
        //  $data =  pettycash::where('id','=', $input['id'])->get();
         $data =  pettycash::find($input['id']);
        return $data;
    }

    public function create()
    {   
        $input = Input::all();
        pettycash::insert([
            'listname'=>$input['listname'],
            'status'=>1
        ]);
        return redirect()->back()->with('alert', 'บันทึกสำเร็จ!');
    }

    public function edit()
    {   
        $input = Input::all();
        $data =  pettycash::find($input['id']);
        $data->listname = $input['listname'];
        $data->save();
        return redirect()->back()->with('alert', 'แก้ไขสำเร็จ!');
    }


    public function delete()
    {   
        $input = Input::all();
        $data =  pettycash::find($input['data']['id']);
        $data->delete();
        return 1;
    }

        

}
