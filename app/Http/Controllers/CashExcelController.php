<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;
use App\Api\Datetime;

use Session;

class CashExcelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function excelreportcashdailynew() { //รายงานเงินสดย่อยรายวัน



  				$data = Input::all();
  				$db = Connectdb::Databaseall();
          // echo "<pre>";
          // print_r($data);
          // exit;

          // $datepicker = explode("/",trim(($data['datepicker'])));
          // if(count($datepicker) > 0) {
          //     $datetime = $datepicker[2] . '-' . $datepicker[1] . '-' . $datepicker[0]; //วัน - เดือน - ปี
          // }
          $datetime = $data['datepicker'];
          $branch_id = $data['brcode'];

          $sql = 'SELECT '.$db['fsctaccount'].'.insertcashrent.*
                  FROM '.$db['fsctaccount'].'.insertcashrent

                  WHERE '.$db['fsctaccount'].'.insertcashrent.datetimeinsert LIKE "%'.$datetime.'%"
                    AND '.$db['fsctaccount'].'.insertcashrent.branch = '.$branch_id.'
                    AND '.$db['fsctaccount'].'.insertcashrent.typedoc NOT IN (99 , 10 , 11 , 12 , 14 , 15, 16)
                    AND '.$db['fsctaccount'].'.insertcashrent.status NOT IN (99)
                    ';
          $datatresult = DB::connection('mysql')->select($sql);

          $sqlpo = 'SELECT '.$db['fsctaccount'].'.po_head.*,
                           '.$db['fsctaccount'].'.insertcashrent.*
                    FROM '.$db['fsctaccount'].'.po_head
                    INNER JOIN  '.$db['fsctaccount'].'.insertcashrent
                       ON '.$db['fsctaccount'].'.po_head.id = '.$db['fsctaccount'].'.insertcashrent.ref

                    WHERE '.$db['fsctaccount'].'.insertcashrent.datetimeinsert LIKE "%'.$datetime.'%"
                      AND '.$db['fsctaccount'].'.insertcashrent.branch = '.$branch_id.'
                      AND '.$db['fsctaccount'].'.insertcashrent.typedoc NOT IN (0 , 99 , 1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 , 11 , 13 , 14 , 15 , 16)
                      AND '.$db['fsctaccount'].'.insertcashrent.status NOT IN (99)
                      ';
          $datatresultpo = DB::connection('mysql')->select($sqlpo);

          $sqlmoneycash = 'SELECT '.$db['fsctaccount'].'.po_head.*,
                           '.$db['fsctaccount'].'.insertcashrent.*
                    FROM '.$db['fsctaccount'].'.po_head
                    INNER JOIN  '.$db['fsctaccount'].'.insertcashrent
                       ON '.$db['fsctaccount'].'.po_head.id = '.$db['fsctaccount'].'.insertcashrent.ref

                    WHERE '.$db['fsctaccount'].'.insertcashrent.datetimeinsert LIKE "%'.$datetime.'%"
                      AND '.$db['fsctaccount'].'.insertcashrent.branch = '.$branch_id.'
                      AND '.$db['fsctaccount'].'.insertcashrent.typedoc IN (16)
                      AND '.$db['fsctaccount'].'.insertcashrent.status NOT IN (99)
                      ';

          $datatresultmoneycash = DB::connection('mysql')->select($sqlmoneycash);

          $sqlmoney = 'SELECT '.$db['fsctaccount'].'.reservemoney.*,
                              '.$db['fsctaccount'].'.insertcashrent.*
                       FROM '.$db['fsctaccount'].'.reservemoney
                       INNER JOIN  '.$db['fsctaccount'].'.insertcashrent
                         ON '.$db['fsctaccount'].'.reservemoney.id = '.$db['fsctaccount'].'.insertcashrent.ref

                       WHERE '.$db['fsctaccount'].'.insertcashrent.datetimeinsert LIKE "%'.$datetime.'%"
                         AND '.$db['fsctaccount'].'.insertcashrent.branch = '.$branch_id.'
                         AND '.$db['fsctaccount'].'.insertcashrent.typedoc IN (14)
                         AND '.$db['fsctaccount'].'.insertcashrent.status NOT IN (99)
                         ';
          $datatresultmoney = DB::connection('mysql')->select($sqlmoney);

          $sqlatm = 'SELECT '.$db['fsctaccount'].'.insertcashrent.*
                     FROM '.$db['fsctaccount'].'.insertcashrent
                     INNER JOIN  '.$db['fsctaccount'].'.atmcash
                        ON '.$db['fsctaccount'].'.insertcashrent.ref = '.$db['fsctaccount'].'.atmcash.id

                     WHERE '.$db['fsctaccount'].'.insertcashrent.datetimeinsert LIKE "%'.$datetime.'%"
                       AND '.$db['fsctaccount'].'.insertcashrent.branch = '.$branch_id.'
                       AND '.$db['fsctaccount'].'.insertcashrent.typedoc IN (11)
                       AND '.$db['fsctaccount'].'.insertcashrent.status NOT IN (99)
                       ';
          $datatresultatm = DB::connection('mysql')->select($sqlatm);

          $sqlexpenses = 'SELECT '.$db['fsctaccount'].'.insertcashrent.*
                     FROM '.$db['fsctaccount'].'.insertcashrent
                     INNER JOIN  '.$db['fsctaccount'].'.not_expenses
                        ON '.$db['fsctaccount'].'.insertcashrent.ref = '.$db['fsctaccount'].'.not_expenses.id

                     WHERE '.$db['fsctaccount'].'.insertcashrent.datetimeinsert LIKE "%'.$datetime.'%"
                       AND '.$db['fsctaccount'].'.insertcashrent.branch = '.$branch_id.'
                       AND '.$db['fsctaccount'].'.insertcashrent.typedoc IN (15)
                       AND '.$db['fsctaccount'].'.insertcashrent.status NOT IN (99)
                       ';
          $dataexpenses = DB::connection('mysql')->select($sqlexpenses);

          $sqlcheck = 'SELECT '.$db['fsctaccount'].'.checkbank.*
                  FROM '.$db['fsctaccount'].'.checkbank

                  WHERE '.$db['fsctaccount'].'.checkbank.time LIKE "%'.$datetime.'%"
                    AND '.$db['fsctaccount'].'.checkbank.branch_id = '.$branch_id.'
                    AND '.$db['fsctaccount'].'.checkbank.status NOT IN (99)
                    ';
          $datatresultcheck = DB::connection('mysql')->select($sqlcheck);

          // echo "<pre>";
          // print_r($datatresult);
          // print_r($datatresultpo);
          // print_r($datatresultmoneycash);
          // print_r($datatresultmoney);
          // print_r($datatresultatm);
          // print_r($dataexpenses);
          // print_r($datatresultcheck);
          // exit;

          $arrNewdatacash = [];
  				$arrNewdata = [];
          $arrNewdataatm = [];
          $arrNewdatamoney = [];
          $arrNewdatamoneycash = [];
          $arrNewdataexpenses = [];
          $arrNewdatacheck = [];
          $arrNewdataresultpo = [];

          $arrNewdataall = [];  //รวมเงินสด
          $all = 0;

          $arrNewdataall2 = [];  //รวมเงินสด + รวมเงินโอน
          $all2 = 0;

          $arrNewdataall3 = [];  //รวมสินค้าตัดหาย
          $all3 = 0;

          $arrNewdataall4 = [];  //รวมรายรับ
          $all4 = 0;

          $arrNewdataall5 = [];  //รวมรายจ่าย
          $all5 = 0;

          $arrNewdataall6 = [];  //รวมรับประกัน
          $all6 = 0;

          $arrNewdataall7 = [];  //รวมคืนประกัน
          $all7 = 0;

          $arrNewdataall8 = [];  //ยอดคงเหลือ
          $all8 = 0;

          $arrNewdataday = [];  //ยอดเงินสดที่ต้องนำฝากในแต่ละวัน
          $day = 0;

          $arrNewdataday2 = [];  //ยอดคงเหลือ
          $day2 = 0;

          $arrNewdataday3 = [];  //ยอดยกไป
          $day3 = 0;

          $arrNewdataday4 = [];  //ยอดที่ต้องนำฝาก
          $day4 = 0;


  				$i =1;
  				$j=0;
          $c=0;

          //--------------------- ยอดยกมา ---------------------
          $sqlcash = 'SELECT * FROM '.$db['fsctaccount'].'.cash
                  WHERE '.$db['fsctaccount'].'.cash.branch_id = "'.$branch_id.'"
                  AND '.$db['fsctaccount'].'.cash.time = "'.$datetime.'"
                 ';

          $dataresultcash = DB::connection('mysql')->select($sqlcash);
          // print_r($dataresultcash);
          // exit;

          $sumall =0;

          $arrNewdatacash[$j]['วันที่'] = " ";
          $arrNewdatacash[$j]['ชื่อลูกค้า'] = " ";
          $arrNewdatacash[$j]['ประเภท'] = 'ยกยอดมา';

          if($dataresultcash){
            // $grandtotal = $dataresultcash[0]->grandtotal;
            $arrNewdatacash[$j]['เงินทอน'] = $dataresultcash[0]->grandtotal;
          }

          $arrNewdatacash[$j]['สินค้าตัดหาย'] = " ";
          $arrNewdatacash[$j]['รายรับ'] = " ";
          $arrNewdatacash[$j]['รายจ่าย'] = " ";
          $arrNewdatacash[$j]['รับประกัน'] = " ";
          $arrNewdatacash[$j]['คืนประกัน'] = " ";

          if($dataresultcash){
            $sumall = $sumall + $dataresultcash[0]->grandtotal;
            $arrNewdatacash[$j]['คงเหลือ'] = $dataresultcash[0]->grandtotal;
          }


          $sumtotalloss = 0; //รวมค่าสินค้าตัดหาย
          $sumtotalinsurance = 0; //รวมรับประกัน
          $sumtotalinsurancepay = 0; //รวมคืนประกัน
          $sumtotalall = 0; //รวมรายรับ
          $sumcreditnoteall = 0; //รวมลดหนี้
          $sumtotalpay = 0; //รวมรายจ่าย
          $summdtotalall = 0; //รวม MD โอนมา
          $summdtotalatm = 0; //รวมเงินถอน
          $sumtotalmoney = 0; //เงินสำรองจ่าย

          $sumtotallossbank = 0; //รวมค่าสินค้าตัดหาย (โอน)
          $sumtotalallbank = 0; //รวมรายรับ (โอน)
          $sumcreditnoteallbank = 0; //รวมลดหนี้ (โอน)
          $sumtotalinsurancebank = 0; //รวมรับประกัน (โอน)
          $sumtotalinsurancepaybank = 0; //รวมคืนประกัน (โอน)
          $summdtotalallbank = 0; //รวม MD โอนมา (โอน)
          $sumtotalpaybank = 0; //รวมรายจ่าย (โอน)
          $summdtotalatmbank = 0; //รวมเงินถอน (โอน)

          $sumtotalexpenses = 0; //ค่าใช้จ่าย(ที่ไม่ถือเป็นรายรับ)
          $sumtotalmoneycash = 0; //คืนเงินสำรองจ่าย

          $sumall1 = 0; // รวมเงินสด
          $sumallmoney = 0; // รวมเงินสด + รวมเงินสำรองจ่าย
          $sumall2 = 0; // รวมเงินสด + จัดซื้อเงินสด
          $sumall3 = 0; // รวมเงินโอน
          $sumall4 = 0; // รวมเงินโอน + จัดซื้อเงินโอน
          $sumall5 = 0; // รวมเงิน ATM
          $sumallatm = 0; //เงินถอน

          $grandtotal = 0; //รวมยอดยกไป
          $moneytotal = 0; //รวมนำฝาก

          $sumtotalwht = 0;
          $sumtotalwhtbank = 0;

  				foreach ($datatresult as $key => $value) {

            $arrNewdata[$c]['วันที่'] = $value->datetimeinsert;

            $customerid = $value->log;
            $test = explode('=',$customerid);

            $modelcus = Maincenter::getdatacustomercash($test[1]);
            if($modelcus){
              $arrNewdata[$c]['ชื่อลูกค้า'] = $modelcus[0]->name." ".$modelcus[0]->lastname;
            }


            if($value->typedoc == 0){ //รับค่าเช่า
              $arrNewdata[$c]['ประเภท'] = 'รับค่าเช่า';
            }
            if($value->typedoc == 1){ //รับค่าเช่าเพิ่มเติม
              $arrNewdata[$c]['ประเภท'] = 'รับค่าเช่าเพิ่มเติม';
            }
            if($value->typedoc == 2){ //รับค่าเช่าของหาย
              $arrNewdata[$c]['ประเภท'] = 'รับค่าเช่าของหาย';
            }
            if($value->typedoc == 3){ //ลดหนี้
              $arrNewdata[$c]['ประเภท'] = 'ลดหนี้';
            }
            if($value->typedoc == 4){ //รับเงินประกัน
              $arrNewdata[$c]['ประเภท'] = 'รับเงินประกัน';
            }
            if($value->typedoc == 5){ //คืนเงินประกัน
              $arrNewdata[$c]['ประเภท'] = 'คืนเงินประกัน';
            }
            if($value->typedoc == 6){ //รับค่าขนส่ง
              $arrNewdata[$c]['ประเภท'] = 'รับค่าขนส่ง';
            }
            if($value->typedoc == 7){ //ลดหนี้กรณีพิเศษ
              $arrNewdata[$c]['ประเภท'] = 'ลดหนี้กรณีพิเศษ';
            }
            if($value->typedoc == 8){ //หัก ณ ที่จ่าย
              $arrNewdata[$c]['ประเภท'] = 'หัก ณ ที่จ่าย';
            }
            if($value->typedoc == 9){ //รับค่าเช่าแบบย่อย
              $arrNewdata[$c]['ประเภท'] = 'รับค่าเช่าแบบย่อย';
            }
            if($value->typedoc == 13){ //รับค่าขายของ
              $arrNewdata[$c]['ประเภท'] = 'รับค่าขายของ';
            }

            $arrNewdata[$c]['เงินทอน'] = " ";


            if($value->typedoc == 2){  //สินค้าตัดหาย
              if($value->typetranfer == '1'){ //รับเงินสด
                // $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                $sumtotalloss = $sumtotalloss +  $value->money;
                $arrNewdata[$c]['สินค้าตัดหาย'] = $value->money;
              }
              elseif ($value->typetranfer == '2'){ //รับเงินโอน
                // echo "[";
                $sumtotallossbank = $sumtotallossbank +  $value->money;
                $arrNewdata[$c]['สินค้าตัดหาย'] = "[".$value->money."]";
                // echo "]";
              }
              else{
                // $sumsubtotalinput = $sumsubtotalinput + 0;
                $sumtotalloss = $sumtotalloss + 0;
                $arrNewdata[$c]['สินค้าตัดหาย'] = '0';
              }
            }else {
                $arrNewdata[$c]['สินค้าตัดหาย'] = " ";
            }


            if($value->typedoc == 0 || $value->typedoc == 1 || $value->typedoc == 6 || $value->typedoc == 9 || $value->typedoc == 13){
              if($value->typetranfer == '1'){ //รับเงินสด
                // $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                $sumtotalall = $sumtotalall +  $value->money;
                // echo '<span style="color: green;" />';
                $arrNewdata[$c]['รายรับ'] = $value->money;
                // echo '</span>';
              }
              elseif ($value->typetranfer == '2'){ //รับเงินโอน
                // echo '<span style="color: green;" />';
                // echo "[";
                $sumtotalallbank = $sumtotalallbank +  $value->money;
                $arrNewdata[$c]['รายรับ'] = "[".$value->money."]";
                // echo "]";
                // echo '</span>';
              }
              else{
                // $sumsubtotalinput = $sumsubtotalinput + 0;
                $sumtotalall = $sumtotalall + 0;
                // echo '<span style="color: green;" />';
                $arrNewdata[$c]['รายรับ'] = '0';
                // echo '</span>';
              }
            }else {
              $arrNewdata[$c]['รายรับ'] = " ";
            }

            // ลดหนี้
            if($value->typedoc == 3 || $value->typedoc == 7){
              if($value->typetranfer == '1'){ //รับเงินสด
                // $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                $sumcreditnoteall = $sumcreditnoteall +  $value->money;
                // echo '<span style="color: red;" />';
                $arrNewdata[$c]['รายรับ'] = -($value->money);
                // echo '</span>';
              }
              elseif ($value->typetranfer == '2'){ //รับเงินโอน
                // echo '<span style="color: red;" />';
                // echo "[";
                $sumcreditnoteallbank = $sumcreditnoteallbank +  $value->money;
                $arrNewdata[$c]['รายรับ'] = "[".-($value->money)."]";
                // echo "]";
                // echo '</span>';
              }
              else{
                // $sumsubtotalinput = $sumsubtotalinput + 0;
                $sumcreditnoteall = $sumcreditnoteall + 0;
                // echo '<span style="color: red;" />';
                $arrNewdata[$c]['รายรับ'] = '0';
                // echo '</span>';
              }
            }


            if($value->typedoc == 8){  //หัก ณ ที่จ่าย
              if($value->typetranfer == '2') //หัก ณ ที่จ่าย (โอน)
              {
                // echo '<span style="color: red;" />';
                // echo "[";
                $sumtotalwhtbank = $sumtotalwhbank +  $value->money;
                $arrNewdata[$c]['รายจ่าย'] = "[".$value->money."]";
                // echo "]";
                // echo '</span>';
              }
              else //หัก ณ ที่จ่าย (เงินสด)
              {
                $sumtotalwht = $sumtotalwht +  $value->money;
                // echo '<span style="color: red;" />';
                $arrNewdata[$c]['รายจ่าย'] = $value->money;
                // echo '</span>';
              }
            }else {
                $arrNewdata[$c]['รายจ่าย'] = " ";
            }



            if($value->typedoc == 4){  //รับประกัน
              if($value->typetranfer == '2') //รับประกัน (โอน)
              {
                // echo '<span style="color: green;" />';
                // echo "[";
                $sumtotalinsurancebank = $sumtotalinsurancebank +  $value->money;
                $arrNewdata[$c]['รับประกัน'] = "[".$value->money."]";
                // echo "]";
                // echo '</span>';
              }
              else //รับประกัน (เงินสด)
              {
                $sumtotalinsurance = $sumtotalinsurance +  $value->money;
                // echo '<span style="color: green;" />';
                $arrNewdata[$c]['รับประกัน'] = $value->money;
                // echo '</span>';
              }
            }else {
                $arrNewdata[$c]['รับประกัน'] = " ";
            }


            if($value->typedoc == 5){  //คืนประกัน
              if($value->typetranfer == '2') //คืนประกัน (โอน)
              {
                // echo '<span style="color: red;" />';
                // echo "[";
                $sumtotalinsurancepaybank = $sumtotalinsurancepaybank +  $value->money;
                $arrNewdata[$c]['คืนประกัน'] = "[".$value->money."]";
                // echo "]";
                // echo '</span>';
              }
              else //คืนประกัน (เงินสด)
              {
                $sumtotalinsurancepay = $sumtotalinsurancepay +  $value->money;
                // echo '<span style="color: red;" />';
                $arrNewdata[$c]['คืนประกัน'] = $value->money;
                // echo '</span>';
              }
            }else {
                $arrNewdata[$c]['คืนประกัน'] = " ";
            }


            $sumall1 = ((((($sumtotalloss + $sumtotalall)- ($sumcreditnoteall)) + $sumtotalinsurance) - $sumtotalwht) - $sumtotalinsurancepay ) + $sumall; //เงินสด
            $arrNewdata[$c]['คงเหลือ'] = $sumall1;

  							$i++;
  							$c++;
  				}

  				foreach ($datatresultatm as $key3 => $value3) {

            $arrNewdataatm[$key3]['วันที่'] = $value3->datetimeinsert;
            $arrNewdataatm[$key3]['ชื่อลูกค้า'] = $value3->log;

            if($value3->typedoc == 11){ //เงินถอน
              $arrNewdataatm[$key3]['ประเภท'] = 'เงินถอน';
            }


            if($value3->typedoc == 11){
              if($value3->typetranfer == '1'){ //รับเงินสด
                // $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                $summdtotalatm = $summdtotalatm +  $value3->money;
                $arrNewdataatm[$key3]['เงินทอน'] = $value3->money;
              }
              elseif ($value3->typetranfer == '2'){ //รับเงินโอน
                // echo "[";
                $summdtotalatmbank = $summdtotalatmbank +  $value3->money;
                $arrNewdataatm[$key3]['เงินทอน'] = "[".$value3->money."]";
                // echo "]";
              }
              else{
                // $sumsubtotalinput = $sumsubtotalinput + 0;
                $summdtotalatm = $summdtotalatm + 0;
                $arrNewdataatm[$key3]['เงินทอน'] = '0';
              }
            }

            $arrNewdataatm[$key3]['สินค้าตัดหาย'] = " ";
            $arrNewdataatm[$key3]['รายรับ'] = " ";
            $arrNewdataatm[$key3]['รายจ่าย'] = " ";
            $arrNewdataatm[$key3]['รับประกัน'] = " ";
            $arrNewdataatm[$key3]['คืนประกัน'] = " ";

            if($sumall1 != 0){
              $sumall1 = $sumall1 + $value3->money;
            }else if ($sumall1 == 0) {
              $sumall1 = $sumall1 + $value3->money + $sumall;
            }
            // $sumall1 = $sumall1 + $value3->money;
            $arrNewdataatm[$key3]['คงเหลือ'] = $sumall1;

  							$i++;
  							$key3++;
  				}

          foreach ($datatresultmoney as $key5 => $value5) {

            $arrNewdatamoney[$key5]['วันที่'] = $value5->datetimeinsert;
            $arrNewdatamoney[$key5]['ชื่อลูกค้า'] = $value5->log;

            if($value5->typedoc == 14){ //เงินสำรองจ่าย
              $arrNewdatamoney[$key5]['ประเภท'] = 'เงินสำรองจ่าย';
            }

            $arrNewdatamoney[$key5]['เงินทอน'] = " ";
            $arrNewdatamoney[$key5]['สินค้าตัดหาย'] = " ";
            $arrNewdatamoney[$key5]['รายรับ'] = " ";


            if($value5->typedoc == 14){
              if($value5->typetranfer == '1'){ //รับเงินสด
                // $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                $sumtotalmoney = $sumtotalmoney +  $value5->money;
                $arrNewdatamoney[$key5]['รายจ่าย'] = $value5->money;
              }
              elseif ($value5->typetranfer == '2'){ //รับเงินโอน
                // echo "[";
                // $sumtotalmoneybank = $sumtotalmoneybank +  $value5->money;
                $arrNewdatamoney[$key5]['รายจ่าย'] = "[".$value5->money."]";
                // echo "]";
              }
              else{
                // $sumsubtotalinput = $sumsubtotalinput + 0;
                $sumtotalmoney = $sumtotalmoney + 0;
                $arrNewdatamoney[$key5]['รายจ่าย'] = '0';
              }
            }


            $arrNewdatamoney[$key5]['รับประกัน'] = " ";
            $arrNewdatamoney[$key5]['คืนประกัน'] = " ";

            if($sumall1 != 0){
              if($value5->typetranfer == '1'){
                $sumall1 = $sumall1 - $value5->money;
              }
            }else if ($sumall1 == 0) {
              if($value5->typetranfer == '1'){
              $sumall1 = $sumall1 - $value5->money + $sumall;
              }
            }
            $arrNewdatamoney[$key5]['คงเหลือ'] = $sumall1;

  							$i++;
  							$key5++;
  				}

          foreach ($dataexpenses as $key6 => $value6) {

            $arrNewdataexpenses [$key6]['วันที่'] = $value6->datetimeinsert;
            $arrNewdataexpenses [$key6]['ชื่อลูกค้า'] = $value6->log;

            if($value6->typedoc == 15){ //เงินสำรองจ่าย
              $arrNewdataexpenses[$key6]['ประเภท'] = 'เงินสำรองจ่าย';
            }

            $arrNewdataexpenses[$key6]['เงินทอน'] = " ";
            $arrNewdataexpenses[$key6]['สินค้าตัดหาย'] = " ";
            $arrNewdataexpenses[$key6]['รายรับ'] = " ";


            $arrNewdataexpenses[$key6]['รายจ่าย'] = $value6->money;
            if($value6->typedoc == 15){
              if($value6->typetranfer == '1'){ //รับเงินสด
                // $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                $sumtotalexpenses = $sumtotalexpenses +  $value6->money;
                $arrNewdataexpenses[$key6]['รายจ่าย'] = $value6->money;
              }
              elseif ($value6->typetranfer == '2'){ //รับเงินโอน
                // echo "[";
                // $sumtotalmoneybank = $sumtotalmoneybank +  $value6->money;
                $arrNewdataexpenses[$key6]['รายจ่าย'] = "[".$value6->money."]";
                // echo "]";
              }
              else{
                // $sumsubtotalinput = $sumsubtotalinput + 0;
                $sumtotalexpenses = $sumtotalexpenses + 0;
                $arrNewdataexpenses[$key6]['รายจ่าย'] = '0';
              }
            }


            $arrNewdataexpenses[$key6]['รับประกัน'] = " ";
            $arrNewdataexpenses[$key6]['คืนประกัน'] = " ";

            if($sumall1 != 0){
              $sumall1 = $sumall1 + $value6->money;
            }else if ($sumall1 == 0) {
              $sumall1 = $sumall1 + $value6->money + $sumall;
            }
            $arrNewdataexpenses[$key6]['คงเหลือ'] = $sumall1;

                $i++;
                $key6++;
          }

          foreach ($datatresultmoneycash as $key7 => $value7) {

            $arrNewdatamoneycash[$key7]['วันที่'] = $value7->datetimeinsert;
            $arrNewdatamoneycash[$key7]['ชื่อลูกค้า'] = $value7->log;

            if($value7->typedoc == 16){ //คืนเงินสำรองจ่าย
              $arrNewdatamoneycash[$key7]['ประเภท'] = 'คืนเงินสำรองจ่าย';
            }


            if($value7->typedoc == 16){
              if($value7->typetranfer == '1'){ //รับเงินสด
                // $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                $sumtotalmoneycash = $sumtotalmoneycash +  $value7->money;
                $arrNewdatamoneycash[$key7]['เงินทอน'] = $value7->money;
              }
              elseif ($value7->typetranfer == '2'){ //รับเงินโอน
                // echo "[";
                // $sumtotalmoneybank = $sumtotalmoneybank +  $value7->money;
                $arrNewdatamoneycash[$key7]['เงินทอน'] = "[".$value7->money."]";
                // echo "]";
              }
              else{
                // $sumsubtotalinput = $sumsubtotalinput + 0;
                $sumtotalmoneycash = $sumtotalmoneycash + 0;
                $arrNewdatamoneycash[$key7]['เงินทอน'] = '0';
              }
            }


            $arrNewdatamoneycash[$key7]['สินค้าตัดหาย'] = " ";
            $arrNewdatamoneycash[$key7]['รายรับ'] = " ";
            $arrNewdatamoneycash[$key7]['รายจ่าย'] = " ";
            $arrNewdatamoneycash[$key7]['รับประกัน'] = " ";
            $arrNewdatamoneycash[$key7]['คืนประกัน'] = " ";

            if($sumall1 != 0){
              $sumall1 = $sumall1 + $value7->money;
            }else if ($sumall1 == 0) {
              $sumall1 = $sumall1 + $value7->money + $sumall;
            }
            $arrNewdatamoneycash[$key7]['คงเหลือ'] = $sumall1;

                $i++;
                $key7++;
          }

          foreach ($datatresultpo as $key2 => $value2) {

            // echo "<pre>";
            // echo $value2->po_number;
            // exit;

            $arrNewdataresultpo[$key2]['วันที่'] = $value2->datetimeinsert;

            $poid = $value2->log;
            $test = explode('ใบ',$poid);
            $modelpoid = Maincenter::getdatapodetail($value2->ref);
            // echo "<pre>";
            // print $modelpoid[0];
            // exit;
            // $arrNewdataresultpo[$key2]['ชื่อลูกค้า'] = $test[1];
            // $arrNewdataresultpo[$key2]['ชื่อลูกค้า'] = $value2->po_number;

            foreach ($modelpoid as $key4 => $value4) {
              $po = $value4->list;
              $testpo = explode('__',$po);
              // echo $testpo[1];

              // echo "<br>";
              // echo "- ";
              $arrNewdataresultpo[$key2]['ชื่อลูกค้า'] =  ($value2->po_number)." ".$testpo[1]." = ".($value4->total)." บาท";
            }


            if($value2->typedoc == 10){ //จัดซื้อ PO
              $arrNewdataresultpo[$key2]['ประเภท'] = "จัดซื้อ PO";
            }
            if($value2->typedoc == 12){ //MD โอนมา
              $arrNewdataresultpo[$key2]['ประเภท'] = "MD โอนมา";
            }


            if($value2->typedoc == 12){
              if($value2->typetranfer == '1'){ //รับเงินสด
                // $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                $summdtotalall = $summdtotalall +  $value2->money;
                $arrNewdataresultpo[$key2]['เงินทอน'] = $value2->money;
              }
              elseif ($value2->typetranfer == '2'){ //รับเงินโอน
                // echo "[";
                $summdtotalallbank = $summdtotalallbank +  $value2->money;
                $arrNewdataresultpo[$key2]['เงินทอน'] = "[".$value2->money."]";
                // echo "]";
              }
              else{
                // $sumsubtotalinput = $sumsubtotalinput + 0;
                $summdtotalall = $summdtotalall + 0;
                $arrNewdataresultpo[$key2]['เงินทอน'] = '0';
              }
            }


            $arrNewdataresultpo[$key2]['สินค้าตัดหาย'] = " ";
            $arrNewdataresultpo[$key2]['รายรับ'] = " ";


            if($value2->typedoc == 10){
              if($value2->typetranfer == '1'){ //รับเงินสด
                // $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                $sumtotalpay = $sumtotalpay +  $value2->money;
                $arrNewdataresultpo[$key2]['รายจ่าย'] = $value2->money;
              }
              elseif ($value2->typetranfer == '2'){ //รับเงินโอน
                // echo "[";
                $sumtotalpaybank = $sumtotalpaybank +  $value2->money;
                $arrNewdataresultpo[$key2]['รายจ่าย'] = "[".$value2->money."]";
                // echo "]";
              }
              else{
                // $sumsubtotalinput = $sumsubtotalinput + 0;
                $sumtotalpay = $sumtotalpay + 0;
                $arrNewdataresultpo[$key2]['รายจ่าย'] = '0';
              }
            }


            $arrNewdataresultpo[$key2]['รับประกัน'] = " ";
            $arrNewdataresultpo[$key2]['คืนประกัน'] = " ";

            if($value2->typedoc == 10){
              if($value2->typetranfer == '1'){
                 $sumall1 = $sumall1 - $value2->money;
                 // if($sumall1 != 0){
                 //   $sumall1 = $sumall1 + $value2->money;
                 // }else if ($sumall1 == 0) {
                 //   $sumall1 = $sumall1 + $value2->money + $sumall;
                 // }
              }
              // echo number_format($sumtotalpay,2);
              // echo number_format($sumall1,2);
            }else {
              $sumall1 = $sumall1 - $summdtotalall;
              // echo number_format($sumall1,2);
            }
            $arrNewdataresultpo[$key2]['คงเหลือ'] = $sumall1;

                $i++;
                $key2++;
          }

          //รวมเงินสด
          $arrNewdataall[$all]['วันที่'] = " ";
          $arrNewdataall[$all]['ชื่อลูกค้า'] = " ";
          $arrNewdataall[$all]['ประเภท'] = " ";
          $arrNewdataall[$all]['เงินทอน'] = "รวมเงินสด";
          $arrNewdataall[$all]['สินค้าตัดหาย'] = $sumtotalloss;
          $arrNewdataall[$all]['รายรับ'] = $sumtotalall;
          $arrNewdataall[$all]['รายจ่าย'] = $sumtotalpay+$sumtotalmoney+$sumtotalwht;
          $arrNewdataall[$all]['รับประกัน'] = $sumtotalinsurance;
          $arrNewdataall[$all]['คืนประกัน'] = $sumtotalinsurancepay;
          $arrNewdataall[$all]['คงเหลือ'] = $sumall1;


          //--------------------- Start รวมเงินสด + วมเงินโอน	---------------------
          //รวมเงินสด + รวมเงินโอน
          $arrNewdataall2[$all2]['วันที่'] = " ";
          $arrNewdataall2[$all2]['ชื่อลูกค้า'] = "รวมเงินสด";
          $arrNewdataall2[$all2]['ประเภท'] = " ";
          $arrNewdataall2[$all2]['เงินทอน'] = " ";
          $arrNewdataall2[$all2]['สินค้าตัดหาย'] = " ";
          $arrNewdataall2[$all2]['รายรับ'] = " ";
          $arrNewdataall2[$all2]['รายจ่าย'] = " ";
          $arrNewdataall2[$all2]['รับประกัน'] = "รวมเงินโอน";
          $arrNewdataall2[$all2]['คืนประกัน'] = " ";
          $arrNewdataall2[$all2]['คงเหลือ'] = " ";

          //รวมสินค้าตัดหาย
          $arrNewdataall3[$all3]['วันที่'] = "รวมสินค้าตัดหาย";
          $arrNewdataall3[$all3]['ชื่อลูกค้า'] = $sumtotalloss;
          $arrNewdataall3[$all3]['ประเภท'] = "บาท";
          $arrNewdataall3[$all3]['เงินทอน'] = " ";
          $arrNewdataall3[$all3]['สินค้าตัดหาย'] = " ";
          $arrNewdataall3[$all3]['รายรับ'] = " ";
          $arrNewdataall3[$all3]['รายจ่าย'] = "รวมสินค้าตัดหาย";
          $arrNewdataall3[$all3]['รับประกัน'] = $sumtotallossbank;
          $arrNewdataall3[$all3]['คืนประกัน'] = "บาท";
          $arrNewdataall3[$all3]['คงเหลือ'] = " ";

          //รวมรายรับ
          $arrNewdataall4[$all4]['วันที่'] = "รวมรายรับ";
          $arrNewdataall4[$all4]['ชื่อลูกค้า'] = $sumtotalall;
          $arrNewdataall4[$all4]['ประเภท'] = "บาท";
          $arrNewdataall4[$all4]['เงินทอน'] = " ";
          $arrNewdataall4[$all4]['สินค้าตัดหาย'] = " ";
          $arrNewdataall4[$all4]['รายรับ'] = " ";
          $arrNewdataall4[$all4]['รายจ่าย'] = "รวมรายรับ";
          $arrNewdataall4[$all4]['รับประกัน'] = $sumtotalallbank;
          $arrNewdataall4[$all4]['คืนประกัน'] = "บาท";
          $arrNewdataall4[$all4]['คงเหลือ'] = " ";

          //รวมรายจ่าย
          $arrNewdataall5[$all5]['วันที่'] = "รวมรายจ่าย";
          $arrNewdataall5[$all5]['ชื่อลูกค้า'] = $sumtotalpay+$sumtotalmoney+$sumtotalwht;
          $arrNewdataall5[$all5]['ประเภท'] = "บาท";
          $arrNewdataall5[$all5]['เงินทอน'] = " ";
          $arrNewdataall5[$all5]['สินค้าตัดหาย'] = " ";
          $arrNewdataall5[$all5]['รายรับ'] = " ";
          $arrNewdataall5[$all5]['รายจ่าย'] = "รวมรายจ่าย";
          $arrNewdataall5[$all5]['รับประกัน'] = $sumtotalpaybank;
          $arrNewdataall5[$all5]['คืนประกัน'] = "บาท";
          $arrNewdataall5[$all5]['คงเหลือ'] = " ";

          //รวมรับประกัน
          $arrNewdataall6[$all6]['วันที่'] = "รวมรับประกัน";
          $arrNewdataall6[$all6]['ชื่อลูกค้า'] = $sumtotalinsurance;
          $arrNewdataall6[$all6]['ประเภท'] = "บาท";
          $arrNewdataall6[$all6]['เงินทอน'] = " ";
          $arrNewdataall6[$all6]['สินค้าตัดหาย'] = " ";
          $arrNewdataall6[$all6]['รายรับ'] = " ";
          $arrNewdataall6[$all6]['รายจ่าย'] = "รวมรับประกัน";
          $arrNewdataall6[$all6]['รับประกัน'] = $sumtotalinsurancebank;
          $arrNewdataall6[$all6]['คืนประกัน'] = "บาท";
          $arrNewdataall6[$all6]['คงเหลือ'] = " ";

          //รวมคืนประกัน
          $arrNewdataall7[$all7]['วันที่'] = "รวมคืนประกัน";
          $arrNewdataall7[$all7]['ชื่อลูกค้า'] = $sumtotalinsurancepay;
          $arrNewdataall7[$all7]['ประเภท'] = "บาท";
          $arrNewdataall7[$all7]['เงินทอน'] = " ";
          $arrNewdataall7[$all7]['สินค้าตัดหาย'] = " ";
          $arrNewdataall7[$all7]['รายรับ'] = " ";
          $arrNewdataall7[$all7]['รายจ่าย'] = "รวมคืนประกัน";
          $arrNewdataall7[$all7]['รับประกัน'] = $sumtotalinsurancepaybank;
          $arrNewdataall7[$all7]['คืนประกัน'] = "บาท";
          $arrNewdataall7[$all7]['คงเหลือ'] = " ";

          //ยอดคงเหลือ
          $arrNewdataall8[$all8]['วันที่'] = "ยอดคงเหลือ";
          $arrNewdataall8[$all8]['ชื่อลูกค้า'] = $sumall1;
          $arrNewdataall8[$all8]['ประเภท'] = "บาท";
          $arrNewdataall8[$all8]['เงินทอน'] = " ";
          $arrNewdataall8[$all8]['สินค้าตัดหาย'] = " ";
          $arrNewdataall8[$all8]['รายรับ'] = " ";
          $arrNewdataall8[$all8]['รายจ่าย'] = " ";
          $arrNewdataall8[$all8]['รับประกัน'] = " ";
          $arrNewdataall8[$all8]['คืนประกัน'] = " ";
          $arrNewdataall8[$all8]['คงเหลือ'] = " ";
          //-----------------------End รวมเงินสด + วมเงินโอน	---------------------


          //---------------- Start ยอดเงินสดที่ต้องนำฝากในแต่ละวัน  -----------------
          //ยอดเงินสดที่ต้องนำฝากในแต่ละวัน
          $arrNewdataday[$day]['วันที่'] = " ";
          $arrNewdataday[$day]['ชื่อลูกค้า'] = "ยอดเงินสดที่ต้องนำฝากในแต่ละวัน";
          $arrNewdataday[$day]['ประเภท'] = " ";

          //ยอดคงเหลือ
          $arrNewdataday2[$day2]['วันที่'] = "ยอดคงเหลือ";
          $arrNewdataday2[$day2]['ชื่อลูกค้า'] = $sumall1;
          $arrNewdataday2[$day2]['ประเภท'] = "บาท";

          //ยอดยกไป
          $arrNewdataday3[$day3]['วันที่'] = "ยอดยกไป";

          if($dataresultcash){
            if($datatresultmoney){
              $grandtotal = ($dataresultcash[0]->grandtotal - $sumtotalmoney);
              // echo ($dataresultcash[0]->grandtotal - $sumtotalmoney);
            }else {
              $grandtotal = $dataresultcash[0]->grandtotal;
              // echo ($dataresultcash[0]->grandtotal);
            }

            if($datatresultmoneycash){
              $grandtotal = $grandtotal + $sumtotalmoneycash;
            }
            // echo number_format ($grandtotal,2);
            $arrNewdataday3[$day3]['ชื่อลูกค้า'] = $grandtotal;

          }

          $arrNewdataday3[$day3]['ประเภท'] = "บาท";

          //ยอดที่ต้องนำฝาก
          $arrNewdataday4[$day4]['วันที่'] = "ยอดที่ต้องนำฝาก";

          if($dataresultcash){
            if($datatresultmoney){

             $moneytotal = $sumall1 - $dataresultcash[0]->grandtotal + $sumtotalmoney;
             // echo number_format ($moneytotal,2);
             // echo number_format ($sumall1 - $dataresultcash[0]->grandtotal + $sumtotalmoney,2);
           }else {

             $moneytotal = $sumall1 - $dataresultcash[0]->grandtotal;
             // echo number_format ($moneytotal,2);
             // echo number_format ($sumall1 - $dataresultcash[0]->grandtotal,2);
           }

           if($datatresultmoneycash){
             // echo number_format ($sumall1,2);
             // echo number_format ($moneytotal,2);
             // echo number_format ($sumtotalmoneycash,2);
             // $moneytotal = $sumall1 - $grandtotal;
             $moneytotal = $moneytotal - $sumtotalmoneycash;
           }
           // echo number_format ($moneytotal,2);
           $arrNewdataday4[$day4]['ชื่อลูกค้า'] = $moneytotal;
          }

          $arrNewdataday4[$day4]['ประเภท'] = "บาท";
          //------------------ End ยอดเงินสดที่ต้องนำฝากในแต่ละวัน  -----------------

          // echo "<pre>";
          // print_r($arrNewdatacash);
          // print_r($arrNewdata);
          // print_r($arrNewdataatm);
          // print_r($arrNewdatamoney);
          // print_r($arrNewdataexpenses);
          // print_r($arrNewdatamoneycash);
          // print_r($arrNewdataresultpo);
          // print_r($arrNewdataall);
    			// exit;

        if($arrNewdatacash || $arrNewdata || $arrNewdataatm || $arrNewdatamoney || $arrNewdataexpenses || $arrNewdatamoneycash || $arrNewdataresultpo){
        $result = array_merge($arrNewdatacash,$arrNewdata,$arrNewdataatm,$arrNewdatamoney,
                              $arrNewdataexpenses,$arrNewdatamoneycash,$arrNewdataresultpo,
                              $arrNewdataall,$arrNewdataall2,$arrNewdataall3,$arrNewdataall4,
                              $arrNewdataall5,$arrNewdataall6,$arrNewdataall7,$arrNewdataall8,
                              $arrNewdataday,$arrNewdataday2,$arrNewdataday3,$arrNewdataday4
                              );
        }
        // echo "<pre>";
  			// print_r($result);
  			// exit;

  			// $arr = [
  			// 					['data2', 'data2'],
  			// 					['data2', 'data2']
  			// 			 ];

  				return Excel::create('รายงานเงินสดย่อยรายวัน', function($excel) use ($result) {

  							$excel->sheet('Sheetname', function($sheet) use ($result) {

  								// $arr = [ ['data2', 'data2'],
  								// 				 ['data2', 'data2']
  								// 			 ];

  									$sheet->fromArray($result);

  							});

  					})->export('csv');
  	}





































}
?>
