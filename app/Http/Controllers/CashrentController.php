<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Api\Connectdb;

use App\Pprdetail;
use DB;
use Illuminate\Support\Facades\Input;
use phpDocumentor\Reflection\Types\Null_;
use Session;

class CashrentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cashrent');
    }

    public function getcashrent(Request $request)
    {

        // return view('cashrent');
        $db = Connectdb::Databaseall();
        $sql = "SELECT *
                FROM $db[fsctaccount].insertcashrent WHERE id = '".$request->id."'";
        $result_vendor = DB::connection('mysql')->select($sql);
        // echo "<pre>";
        // print_r($result_vendor[0]);
        // exit();

        return response()->json($result_vendor[0]);
    }

    public function savecashrent(Request $request){
        $db = Connectdb::Databaseall();
        $data = $request->data;
        $id = $data['ID'];
        $money = $data['money'];
        $emp_code = $data['emp_code'];
        $typetranfer = $data['typetranfer'];

        $sql = "SELECT *
                FROM $db[fsctaccount].insertcashrent WHERE id = '".$id."'";
        $result_old = DB::connection('mysql')->select($sql);

        $sql = "UPDATE $db[fsctaccount].insertcashrent SET status = '99' WHERE id = '".$id."'";
        $result = DB::connection('mysql')->select($sql);

        // $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($arrInsert);
        $result_old[0]->money = $money;
        $result_old[0]->typetranfer = $typetranfer;
        $result_old[0]->emp_code = $emp_code;

        unset($result_old[0]->id);

        $result_old = (array) $result_old[0];

        // echo "<pre>";
        // print_r($result_old);
        // exit('');

        $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($result_old);

        if($model) return "success";
        else return "false";


        if($result_vendor) return 1;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function insertbillcash(){

      $data = Input::all();
      $db = Connectdb::Databaseall();
      // echo "<pre>";
      // print_r($data);
      // exit;

      $brcode = Session::get('brcode');
      $emp_code = Session::get('emp_code');

      $billno = $data['data']['billno'];

      if($data['data']['typedoc'] == "0"){  //RA

        $sql = 'SELECT '.$db['fsctaccount'].'.taxinvoice_abb.*
                FROM '.$db['fsctaccount'].'.taxinvoice_abb

                WHERE '.$db['fsctaccount'].'.taxinvoice_abb.number_taxinvoice LIKE "%'.$billno.'%"
                  ';

        $datatresult = DB::connection('mysql')->select($sql);
        // echo "<pre>";
        // print_r($datatresult);
        // exit;

        $array = ['id'=>'',
                'datetimeinsert'=>$datatresult[0]->time,
                'money'=>$data['data']['moneycash'],
                'typedoc'=>0,
                'emp_code'=>$datatresult[0]->codeemp,
                'branch'=>$datatresult[0]->branch_id,
                'status'=>1,
                'typetranfer'=>$data['data']['typetranfercash'],
                'log'=>"รับเงินจากลูกค้า customer id =".$datatresult[0]->customerid,
                'ref'=>$datatresult[0]->bill_rent,
                'typereftax'=>$datatresult[0]->id
                ];

        $insertmodel = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($array);
        // echo "<pre>";
        // print_r($insertmodel);
        // exit;

      }else if($data['data']['typedoc'] == "1"){  //RN

        $sql = 'SELECT '.$db['fsctaccount'].'.taxinvoice_more_abb.*
                FROM '.$db['fsctaccount'].'.taxinvoice_more_abb

                WHERE '.$db['fsctaccount'].'.taxinvoice_more_abb.number_taxinvoice LIKE "%'.$billno.'%"
                  ';

        $datatresult = DB::connection('mysql')->select($sql);

        $array = ['id'=>'',
                'datetimeinsert'=>$datatresult[0]->time,
                'money'=>$data['data']['moneycash'],
                'typedoc'=>1,
                'emp_code'=>$datatresult[0]->codeemp,
                'branch'=>$datatresult[0]->branch_id,
                'status'=>1,
                'typetranfer'=>$data['data']['typetranfercash'],
                'log'=>"รับเงินจากลูกค้า customer id =".$datatresult[0]->customerid,
                'ref'=>$datatresult[0]->bill_rent,
                'typereftax'=>$datatresult[0]->id
                ];

        $insertmodel = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($array);

      }else if($data['data']['typedoc'] == "2"){  //RL

        $sql = 'SELECT '.$db['fsctaccount'].'.taxinvoice_loss_abb.*
                FROM '.$db['fsctaccount'].'.taxinvoice_loss_abb

                WHERE '.$db['fsctaccount'].'.taxinvoice_loss_abb.number_taxinvoice LIKE "%'.$billno.'%"
                  ';

        $datatresult = DB::connection('mysql')->select($sql);

        $array = ['id'=>'',
                'datetimeinsert'=>$datatresult[0]->time,
                'money'=>$data['data']['moneycash'],
                'typedoc'=>2,
                'emp_code'=>$datatresult[0]->codeemp,
                'branch'=>$datatresult[0]->branch_id,
                'status'=>1,
                'typetranfer'=>$data['data']['typetranfercash'],
                'log'=>"รับเงินจากลูกค้า customer id =".$datatresult[0]->customerid,
                'ref'=>$datatresult[0]->bill_rent,
                'typereftax'=>$datatresult[0]->id
                ];

        $insertmodel = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($array);

      }else if($data['data']['typedoc'] == "3"){  //CN

        $sql = 'SELECT '.$db['fsctaccount'].'.taxinvoice_creditnote.*
                FROM '.$db['fsctaccount'].'.taxinvoice_creditnote

                WHERE '.$db['fsctaccount'].'.taxinvoice_creditnote.number_taxinvoice LIKE "%'.$billno.'%"
                  ';

        $datatresult = DB::connection('mysql')->select($sql);

        $array = ['id'=>'',
                'datetimeinsert'=>$datatresult[0]->time,
                'money'=>$data['data']['moneycash'],
                'typedoc'=>3,
                'emp_code'=>$datatresult[0]->codeemp,
                'branch'=>$datatresult[0]->branch_id,
                'status'=>1,
                'typetranfer'=>$data['data']['typetranfercash'],
                'log'=>"รับเงินจากลูกค้า customer id =".$datatresult[0]->customerid,
                'ref'=>$datatresult[0]->bill_rent,
                'typereftax'=>$datatresult[0]->id
                ];

        $insertmodel = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($array);

      }else if($data['data']['typedoc'] == "4"){  //TI

        $sql = 'SELECT '.$db['fsctaccount'].'.taxinvoice_insurance.*
                FROM '.$db['fsctaccount'].'.taxinvoice_insurance

                WHERE '.$db['fsctaccount'].'.taxinvoice_insurance.number_taxinvoice LIKE "%'.$billno.'%"
                  ';

        $datatresult = DB::connection('mysql')->select($sql);

        $array = ['id'=>'',
                'datetimeinsert'=>$datatresult[0]->time,
                'money'=>$data['data']['moneycash'],
                'typedoc'=>4,
                'emp_code'=>$datatresult[0]->codeemp,
                'branch'=>$datatresult[0]->branch_id,
                'status'=>1,
                'typetranfer'=>$data['data']['typetranfercash'],
                'log'=>"รับเงินจากลูกค้า customer id =".$datatresult[0]->customerid,
                'ref'=>$datatresult[0]->bill_rent,
                'typereftax'=>$datatresult[0]->id
                ];

        $insertmodel = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($array);

      }else if($data['data']['typedoc'] == "5"){  //CI

        $sql = 'SELECT '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.*
                FROM '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote

                WHERE '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.number_taxinvoice LIKE "%'.$billno.'%"
                  ';

        $datatresult = DB::connection('mysql')->select($sql);

        $array = ['id'=>'',
                'datetimeinsert'=>$datatresult[0]->time,
                'money'=>$data['data']['moneycash'],
                'typedoc'=>5,
                'emp_code'=>$datatresult[0]->codeemp,
                'branch'=>$datatresult[0]->branch_id,
                'status'=>1,
                'typetranfer'=>$data['data']['typetranfercash'],
                'log'=>"รับเงินจากลูกค้า customer id =".$datatresult[0]->customerid,
                'ref'=>$datatresult[0]->bill_rent,
                'typereftax'=>$datatresult[0]->id
                ];

        $insertmodel = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($array);

      }else if($data['data']['typedoc'] == "6"){  //RS

        $sql = 'SELECT '.$db['fsctaccount'].'.taxinvoice_special_abb.*
                FROM '.$db['fsctaccount'].'.taxinvoice_special_abb

                WHERE '.$db['fsctaccount'].'.taxinvoice_special_abb.number_taxinvoice LIKE "%'.$billno.'%"
                  ';

        $datatresult = DB::connection('mysql')->select($sql);

        $array = ['id'=>'',
                'datetimeinsert'=>$datatresult[0]->date_approved,
                'money'=>$data['data']['moneycash'],
                'typedoc'=>6,
                'emp_code'=>$datatresult[0]->codeemp,
                'branch'=>$datatresult[0]->branch_id,
                'status'=>1,
                'typetranfer'=>$data['data']['typetranfercash'],
                'log'=>"รับเงินจากลูกค้า customer id =".$datatresult[0]->customerid,
                'ref'=>$datatresult[0]->bill_rent_id,
                'typereftax'=>$datatresult[0]->id
                ];

        $insertmodel = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($array);

      }else if($data['data']['typedoc'] == "7"){  //CS

        $sql = 'SELECT '.$db['fsctaccount'].'.taxinvoice_creditnote_special_abb.*
                FROM '.$db['fsctaccount'].'.taxinvoice_creditnote_special_abb

                WHERE '.$db['fsctaccount'].'.taxinvoice_creditnote_special_abb.number_taxinvoice LIKE "%'.$billno.'%"
                  ';

        $datatresult = DB::connection('mysql')->select($sql);

        $array = ['id'=>'',
                'datetimeinsert'=>$datatresult[0]->date_approved,
                'money'=>$data['data']['moneycash'],
                'typedoc'=>7,
                'emp_code'=>$datatresult[0]->codeemp,
                'branch'=>$datatresult[0]->branch_id,
                'status'=>1,
                'typetranfer'=>$data['data']['typetranfercash'],
                'log'=>"รับเงินจากลูกค้า customer id =".$datatresult[0]->customerid,
                'ref'=>$datatresult[0]->bill_rent_id,
                'typereftax'=>$datatresult[0]->id
                ];

        $insertmodel = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($array);

      }else if($data['data']['typedoc'] == "8"){  //WTH

        $billnum = $data['data']['billno'][0].$data['data']['billno'][1];

        if($billnum == "TF" || "TK" || "TM" || "TP"){

          if($billnum == "TF"){
            $sql = 'SELECT '.$db['fsctaccount'].'.taxinvoice.*
                    FROM '.$db['fsctaccount'].'.taxinvoice

                    WHERE '.$db['fsctaccount'].'.taxinvoice.number_taxinvoice LIKE "%'.$billno.'%"
                      ';

            $datatresult = DB::connection('mysql')->select($sql);
          }else if($billnum == "TK"){
            $sql = 'SELECT '.$db['fsctaccount'].'.taxinvoice_loss.*
                    FROM '.$db['fsctaccount'].'.taxinvoice_loss

                    WHERE '.$db['fsctaccount'].'.taxinvoice_loss.number_taxinvoice LIKE "%'.$billno.'%"
                      ';

            $datatresult = DB::connection('mysql')->select($sql);
          }else if($billnum == "TM"){
            $sql = 'SELECT '.$db['fsctaccount'].'.taxinvoice_more.*
                    FROM '.$db['fsctaccount'].'.taxinvoice_more

                    WHERE '.$db['fsctaccount'].'.taxinvoice_more.number_taxinvoice LIKE "%'.$billno.'%"
                      ';

            $datatresult = DB::connection('mysql')->select($sql);
          }else if($billnum == "TP"){
            $sql = 'SELECT '.$db['fsctaccount'].'.taxinvoice_partial.*
                    FROM '.$db['fsctaccount'].'.taxinvoice_partial

                    WHERE '.$db['fsctaccount'].'.taxinvoice_partial.number_taxinvoice LIKE "%'.$billno.'%"
                      ';

            $datatresult = DB::connection('mysql')->select($sql);
          }

          $array = ['id'=>'',
                  'datetimeinsert'=>$datatresult[0]->timereal,
                  'money'=>$data['data']['moneycash'],
                  'typedoc'=>8,
                  'emp_code'=>$datatresult[0]->codeemp,
                  'branch'=>$datatresult[0]->branch_id,
                  'status'=>1,
                  'typetranfer'=>$data['data']['typetranfercash'],
                  'log'=>"รับเงินจากลูกค้า customer id =".$datatresult[0]->customerid,
                  'ref'=>$datatresult[0]->bill_rent,
                  'typereftax'=>$datatresult[0]->id
                  ];

          $insertmodel = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($array);

        }else if($billnum == "RA" || "RL" || "RN" || "RO"){

          if($billnum == "RA"){
            $sql = 'SELECT '.$db['fsctaccount'].'.taxinvoice_abb.*
                    FROM '.$db['fsctaccount'].'.taxinvoice_abb

                    WHERE '.$db['fsctaccount'].'.taxinvoice_abb.number_taxinvoice LIKE "%'.$billno.'%"
                      ';

            $datatresult = DB::connection('mysql')->select($sql);
          }else if($billnum == "RL"){
            $sql = 'SELECT '.$db['fsctaccount'].'.taxinvoice_loss_abb.*
                    FROM '.$db['fsctaccount'].'.taxinvoice_loss_abb

                    WHERE '.$db['fsctaccount'].'.taxinvoice_loss_abb.number_taxinvoice LIKE "%'.$billno.'%"
                      ';

            $datatresult = DB::connection('mysql')->select($sql);
          }else if($billnum == "RN"){
            $sql = 'SELECT '.$db['fsctaccount'].'.taxinvoice_more_abb.*
                    FROM '.$db['fsctaccount'].'.taxinvoice_more_abb

                    WHERE '.$db['fsctaccount'].'.taxinvoice_more_abb.number_taxinvoice LIKE "%'.$billno.'%"
                      ';

            $datatresult = DB::connection('mysql')->select($sql);
          }else if($billnum == "RO"){
            $sql = 'SELECT '.$db['fsctaccount'].'.taxinvoice_partial_abb.*
                    FROM '.$db['fsctaccount'].'.taxinvoice_partial_abb

                    WHERE '.$db['fsctaccount'].'.taxinvoice_partial_abb.number_taxinvoice LIKE "%'.$billno.'%"
                      ';

            $datatresult = DB::connection('mysql')->select($sql);
          }

          $array = ['id'=>'',
                  'datetimeinsert'=>$datatresult[0]->time,
                  'money'=>$data['data']['moneycash'],
                  'typedoc'=>8,
                  'emp_code'=>$datatresult[0]->codeemp,
                  'branch'=>$datatresult[0]->branch_id,
                  'status'=>1,
                  'typetranfer'=>$data['data']['typetranfercash'],
                  'log'=>"รับเงินจากลูกค้า customer id =".$datatresult[0]->customerid,
                  'ref'=>$datatresult[0]->bill_rent,
                  'typereftax'=>$datatresult[0]->id
                  ];

          $insertmodel = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($array);

        }else if($billnum == "RS"){

          $sql = 'SELECT '.$db['fsctaccount'].'.taxinvoice_special_abb.*
                  FROM '.$db['fsctaccount'].'.taxinvoice_special_abb

                  WHERE '.$db['fsctaccount'].'.taxinvoice_special_abb.number_taxinvoice LIKE "%'.$billno.'%"
                    ';

          $datatresult = DB::connection('mysql')->select($sql);

          $array = ['id'=>'',
                  'datetimeinsert'=>$datatresult[0]->date_approved,
                  'money'=>$data['data']['moneycash'],
                  'typedoc'=>8,
                  'emp_code'=>$datatresult[0]->codeemp,
                  'branch'=>$datatresult[0]->branch_id,
                  'status'=>1,
                  'typetranfer'=>$data['data']['typetranfercash'],
                  'log'=>"รับเงินจากลูกค้า customer id =".$datatresult[0]->customer_id,
                  'ref'=>$datatresult[0]->id,
                  'typereftax'=>$datatresult[0]->id
                  ];

          $insertmodel = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($array);

        }

      }else if($data['data']['typedoc'] == "9"){  //RO

        $sql = 'SELECT '.$db['fsctaccount'].'.taxinvoice_partial_abb.*
                FROM '.$db['fsctaccount'].'.taxinvoice_partial_abb

                WHERE '.$db['fsctaccount'].'.taxinvoice_partial_abb.number_taxinvoice LIKE "%'.$billno.'%"
                  ';

        $datatresult = DB::connection('mysql')->select($sql);

        $array = ['id'=>'',
                'datetimeinsert'=>$datatresult[0]->date_approved,
                'money'=>$data['data']['moneycash'],
                'typedoc'=>9,
                'emp_code'=>$datatresult[0]->codeemp,
                'branch'=>$datatresult[0]->branch_id,
                'status'=>1,
                'typetranfer'=>$data['data']['typetranfercash'],
                'log'=>"รับเงินจากลูกค้า customer id =".$datatresult[0]->customerid,
                'ref'=>$datatresult[0]->bill_rent_id,
                'typereftax'=>$datatresult[0]->id
                ];

        $insertmodel = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($array);

      }else if($data['data']['typedoc'] == "10"){  //PO

        $sql = 'SELECT '.$db['fsctaccount'].'.po_head.*
                FROM '.$db['fsctaccount'].'.po_head

                WHERE '.$db['fsctaccount'].'.po_head.po_number LIKE "%'.$billno.'%"
                  ';

        $datatresult = DB::connection('mysql')->select($sql);

        $array = ['id'=>'',
                'datetimeinsert'=>$datatresult[0]->po_date_ap,
                'money'=>$data['data']['moneycash'],
                'typedoc'=>10,
                'emp_code'=>$datatresult[0]->emp_code_po,
                'branch'=>$datatresult[0]->branch_id,
                'status'=>1,
                'typetranfer'=>$data['data']['typetranfercash'],
                'log'=>"จ่ายเงินให้กับใบ  ".$datatresult[0]->po_number,
                'ref'=>$datatresult[0]->id,
                'typereftax'=>0
                ];

        $insertmodel = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($array);

      }
      // else if($data['data']['typedoc'] == "11"){  //เงินถอน
      //
      //   // $sql = 'SELECT '.$db['fsctaccount'].'.atmcash.*
      //   //         FROM '.$db['fsctaccount'].'.atmcash
      //   //
      //   //         WHERE '.$db['fsctaccount'].'.atmcash.po_number LIKE "%'.$billno.'%"
      //   //           ';
      //   //
      //   // $datatresult = DB::connection('mysql')->select($sql);
      //   //
      //   // $array = ['id'=>'',
      //   //         'datetimeinsert'=>$datatresult[0]->po_date_ap,
      //   //         'money'=>$data['data']['moneycash'],
      //   //         'typedoc'=>11,
      //   //         'emp_code'=>$datatresult[0]->emp_code_po,
      //   //         'branch'=>$datatresult[0]->branch_id,
      //   //         'status'=>1,
      //   //         'typetranfer'=>$data['data']['typetranfercash'],
      //   //         'log'=>"จ่ายเงินให้กับใบ  ".$datatresult[0]->po_number,
      //   //         'ref'=>$datatresult[0]->id,
      //   //         'typereftax'=>0
      //   //         ];
      //   //
      //   // $insertmodel = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($array);
      //
      // }
      // else if($data['data']['typedoc'] == "12"){  //MD โอนเงิน
      //
      //   $sql = 'SELECT '.$db['fsctaccount'].'.po_head.*
      //           FROM '.$db['fsctaccount'].'.po_head
      //
      //           WHERE '.$db['fsctaccount'].'.po_head.po_number LIKE "%'.$billno.'%"
      //             ';
      //
      //   $datatresult = DB::connection('mysql')->select($sql);
      //
      //   $array = ['id'=>'',
      //           'datetimeinsert'=>$datatresult[0]->po_date_ap,
      //           'money'=>$data['data']['moneycash'],
      //           'typedoc'=>12,
      //           'emp_code'=>$datatresult[0]->emp_code_po,
      //           'branch'=>$datatresult[0]->branch_id,
      //           'status'=>1,
      //           'typetranfer'=>$data['data']['typetranfercash'],
      //           'log'=>"MD/BOSS โอนเงินจากใบ ".$datatresult[0]->po_number,
      //           'ref'=>$datatresult[0]->id,
      //           'typereftax'=>$datatresult[0]->id
      //           ];
      //
      //   $insertmodel = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($array);
      //
      // }
        else if($data['data']['typedoc'] == "13"){  //SS

        $sql = 'SELECT '.$db['fsctaccount'].'.stock_sell_head.*
                FROM '.$db['fsctaccount'].'.stock_sell_head

                WHERE '.$db['fsctaccount'].'.stock_sell_head.bill_no LIKE "%'.$billno.'%"
                  ';

        $datatresult = DB::connection('mysql')->select($sql);

        $array = ['id'=>'',
                'datetimeinsert'=>$datatresult[0]->date_approved,
                'money'=>$data['data']['moneycash'],
                'typedoc'=>13,
                'emp_code'=>$datatresult[0]->codeemp,
                'branch'=>$datatresult[0]->branch_id,
                'status'=>1,
                'typetranfer'=>$data['data']['typetranfercash'],
                'log'=>"รับเงินจากลูกค้า customer id =".$datatresult[0]->customer_id,
                'ref'=>$datatresult[0]->id,
                'typereftax'=>$datatresult[0]->id
                ];

        $insertmodel = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($array);

      }
      // else if($data['data']['typedoc'] == "14"){  //จ่ายก่อน
      //
      //   // $sql = 'SELECT '.$db['fsctaccount'].'.stock_sell_head.*
      //   //         FROM '.$db['fsctaccount'].'.stock_sell_head
      //   //
      //   //         WHERE '.$db['fsctaccount'].'.stock_sell_head.bill_no LIKE "%'.$billno.'%"
      //   //           ';
      //   //
      //   // $datatresult = DB::connection('mysql')->select($sql);
      //   //
      //   // $array = ['id'=>'',
      //   //         'datetimeinsert'=>$datatresult[0]->date_approved,
      //   //         'money'=>$data['data']['moneycash'],
      //   //         'typedoc'=>14,
      //   //         'emp_code'=>$datatresult[0]->codeemp,
      //   //         'branch'=>$datatresult[0]->branch_id,
      //   //         'status'=>1,
      //   //         'typetranfer'=>$data['data']['typetranfercash'],
      //   //         'log'=>"รับเงินจากลูกค้า customer id =".$datatresult[0]->customer_id,
      //   //         'ref'=>$datatresult[0]->id,
      //   //         'typereftax'=>$datatresult[0]->id
      //   //         ];
      //   //
      //   // $insertmodel = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($array);
      //
      // }
      // else if($data['data']['typedoc'] == "15"){  //ค่าใช้จ่ายที่ไม่ถือเป็นรายรับ
      //
      //   // $sql = 'SELECT '.$db['fsctaccount'].'.stock_sell_head.*
      //   //         FROM '.$db['fsctaccount'].'.stock_sell_head
      //   //
      //   //         WHERE '.$db['fsctaccount'].'.stock_sell_head.bill_no LIKE "%'.$billno.'%"
      //   //           ';
      //   //
      //   // $datatresult = DB::connection('mysql')->select($sql);
      //   //
      //   // $array = ['id'=>'',
      //   //         'datetimeinsert'=>$datatresult[0]->date_approved,
      //   //         'money'=>$data['data']['moneycash'],
      //   //         'typedoc'=>15,
      //   //         'emp_code'=>$datatresult[0]->codeemp,
      //   //         'branch'=>$datatresult[0]->branch_id,
      //   //         'status'=>1,
      //   //         'typetranfer'=>$data['data']['typetranfercash'],
      //   //         'log'=>"รับเงินจากลูกค้า customer id =".$datatresult[0]->customer_id,
      //   //         'ref'=>$datatresult[0]->id,
      //   //         'typereftax'=>$datatresult[0]->id
      //   //         ];
      //   //
      //   // $insertmodel = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($array);
      //
      // }else if($data['data']['typedoc'] == "16"){  //PO เงินถอน
      //
      //   // $sql = 'SELECT '.$db['fsctaccount'].'.stock_sell_head.*
      //   //         FROM '.$db['fsctaccount'].'.stock_sell_head
      //   //
      //   //         WHERE '.$db['fsctaccount'].'.stock_sell_head.bill_no LIKE "%'.$billno.'%"
      //   //           ';
      //   //
      //   // $datatresult = DB::connection('mysql')->select($sql);
      //   //
      //   // $array = ['id'=>'',
      //   //         'datetimeinsert'=>$datatresult[0]->date_approved,
      //   //         'money'=>$data['data']['moneycash'],
      //   //         'typedoc'=>16,
      //   //         'emp_code'=>$datatresult[0]->codeemp,
      //   //         'branch'=>$datatresult[0]->branch_id,
      //   //         'status'=>1,
      //   //         'typetranfer'=>$data['data']['typetranfercash'],
      //   //         'log'=>"รับเงินจากลูกค้า customer id =".$datatresult[0]->customer_id,
      //   //         'ref'=>$datatresult[0]->id,
      //   //         'typereftax'=>$datatresult[0]->id
      //   //         ];
      //   //
      //   // $insertmodel = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($array);
      //
      // }

      return view('cashrent');
      // return $model;

    }





























}
