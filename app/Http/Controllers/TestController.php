<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Input;
use Excel;


class TestController extends Controller
    {

        public function form(){

            $a=[10,20,40,80,160,320,640,1280];

            return view('test',['a'=>$a]);

        }

        public function content(){

            $a=[10,20,40,80,160,320,640,1280];

            return view('content',['a'=>$a]);

        }


        public function formadd(){

            return view('formadd');

        }


        public function formadddo(){

            $data = Input::all();
            //print_r($data);
            $json = json_decode($data['meow']);
            //print_r($json);
            $arrInsert = [];
                foreach ($json as $v){
                    $arrInsert [$v->name] = $v->value;
                }
                print_r($arrInsert);


            $model = DB::connection('mysql')->table('cat')->insert($arrInsert);


        }

        public function exportexcel(){

            Excel::create('Filename', function($excel) {

                $excel->sheet('Sheetname', function($sheet) {

                    $sheet->fromArray(array(
                        array('data1', 'data2'),
                        array('data3', 'data4')
                    ));

                });

            })->export('xls');

        }


    }


