<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Input;
use Excel;
use Session;
use App\Api\Connectdb;
use App\Api\Datetime;


class PrintTextController extends Controller {

      public function printwithholdtaxpo(){

        return view('printwithholdtaxpo');
      }

      public function serachprintwithholdtaxpo(){

            $data = Input::all();
            $db = Connectdb::Databaseall();
            $datetime = DateTime::FormatDateFromCalendarRange($data['datepicker']);
            $branch_id = $data['branch_id'];

            $start_date = $datetime['start_date']." 00:00:00";
            $end_date = $datetime['end_date']." 23:59:59";
            // print_r($datetime);
            // exit;

            $sql = 'SELECT '.$db['fsctaccount'].'.withholdtaxpo.*
                    FROM '.$db['fsctaccount'].'.withholdtaxpo
                    WHERE '.$db['fsctaccount'].'.withholdtaxpo.branch = '.$branch_id.'
                      AND '.$db['fsctaccount'].'.withholdtaxpo.date  BETWEEN "'.$start_date.'" AND  "'.$end_date.'"
                      AND '.$db['fsctaccount'].'.withholdtaxpo.status != 99 ';

            $datatresult = DB::connection('mysql')->select($sql);

            return view('printwithholdtaxpo',['data'=>$datatresult,'query'=>true,'datepicker'=>$data['datepicker'],'branch_id'=>$branch_id,'datepicker2'=>$datetime]);
        }















}
