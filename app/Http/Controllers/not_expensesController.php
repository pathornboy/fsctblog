<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Illuminate\Support\Facades\Input;
use Session;
use App\Api\Connectdb;

class not_expensesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('not_expenses');
    }

    public function insert_expenses(){
        $data = Input::all();
        $db = Connectdb::Databaseall();
        // echo "<pre>";
        // print_r($data);
        $brcode = Session::get('brcode');
        $emp_code = Session::get('emp_code');
        $arrInsert = ['id'=>'',
                      'datetime'=>date('Y-m-d H:i:s'),
                      'list'=>$data['data']['list'],
                      'amount'=>$data['data']['amount'],
                      // 'vat'=>$data['data']['vat'],
                      // 'vat_money'=>$data['data']['vat_money'],
                      // 'total'=>$data['data']['total'],
                      // 'po_ref'=>$data['data']['po_ref'],
                      'note'=>$data['data']['note'],
                      'id_compay'=>$data['data']['id_compay'],
                      'branch'=>$brcode,
                      'code_emp'=>$emp_code
                    ];


      $model = DB::connection('mysql')->table($db['fsctaccount'].'.not_expenses')->insertGetId($arrInsert);

      $array = ['id'=>'',
              'datetimeinsert'=>date('Y-m-d H:i:s'),
              'money'=>$data['data']['amount'],
              'typedoc'=>15,
              'emp_code'=>$emp_code,
              'branch'=>$brcode,
              'status'=>1,
              'typetranfer'=>1,
              'log'=>$data['data']['note'],
              'ref'=>$model,
              'typereftax'=>$model
              ];

    $insertmodel = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($array);

      return $model;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
