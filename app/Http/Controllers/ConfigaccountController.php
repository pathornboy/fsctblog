<?php
/**
 * Created by PhpStorm.
 * User: pacharapol
 * Date: 1/13/2018 AD
 * Time: 9:22 AM
 */

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;

use  App\Api\Connectdb;
use App\working_company;
use DB;
use Illuminate\Support\Facades\Input;



class ConfigaccountController extends Controller
{
    public function taxfig(){
        return view('configtax');
    }

    public function configtaxinsertandupdate(){

        $datainsert = Input::all();
        $db = Connectdb::Databaseall();
        $data = json_decode($datainsert['data']);
        $arrInsert = [];
        foreach ($data as $v){
            $arrInsert [$v->name] = $v->value;
        }
          if($arrInsert['id'] == ""){
              $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'tax_config')->insert($arrInsert);
          }else{
              $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'tax_config')->where('id',$arrInsert['id'])->update($arrInsert);
          }

          return 1;
    }

    public function getdataconfigtax(){
        $data = Input::all();

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].tax_config.* 
                     FROM $db[fsctaccount].tax_config
                     WHERE $db[fsctaccount].tax_config.id = '$data[id]'";


        $dataquery = DB::connection('mysql2')->select($sql);

        return $dataquery;

    }


    public function configcodeaccount(){
        return view('configcodeaccount');
    }


    public function configaccoutcodeinsertandupdate(){
        $datainsert = Input::all();
        $db = Connectdb::Databaseall();
        $data = json_decode($datainsert['data']);

        $arrInsert = [];
        foreach ($data as $v){
            $arrInsert [$v->name] = $v->value;
        }
        if($arrInsert['id'] == ""){
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'accounttype')->insert($arrInsert);
        }else{
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'accounttype')->where('id',$arrInsert['id'])->update($arrInsert);
        }

        return 1;
    }

    public function getdataconfigaccountcode(){
        $data = Input::all();

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].accounttype.* 
                     FROM $db[fsctaccount].accounttype
                     WHERE $db[fsctaccount].accounttype.id = '$data[id]'";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;

    }

    public function configtypepay(){

        return view('configtypepay');
    }

    public function configtypepayinsertandupdate(){
        $datainsert = Input::all();
        $db = Connectdb::Databaseall();
        $data = json_decode($datainsert['data']);

        $arrInsert = [];
        foreach ($data as $v){
            $arrInsert [$v->name] = $v->value;
        }
        if($arrInsert['id'] == ""){
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'type_pay')->insert($arrInsert);
        }else{
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'type_pay')->where('id',$arrInsert['id'])->update($arrInsert);
        }

        return 1;
    }

    public function getdataconfigtypepay(){
        $data = Input::all();

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].type_pay.* 
                     FROM $db[fsctaccount].type_pay
                     WHERE $db[fsctaccount].type_pay.id = '$data[id]'";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;

    }

    public function configtypebuy(){

        return view('configtypebuy');
    }


    public function configtypebuyinsertandupdate(){
        $datainsert = Input::all();
        $db = Connectdb::Databaseall();
        $data = json_decode($datainsert['data']);

        $arrInsert = [];
        foreach ($data as $v){
            $arrInsert [$v->name] = $v->value;
        }
        if($arrInsert['id'] == ""){
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'type_buy')->insert($arrInsert);
        }else{
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'type_buy')->where('id',$arrInsert['id'])->update($arrInsert);
        }

        return 1;
    }

    public function getdataconfigtypebuy(){
        $data = Input::all();

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].type_buy.* 
                     FROM $db[fsctaccount].type_buy
                     WHERE $db[fsctaccount].type_buy.id = '$data[id]'";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;

    }

    public function configterms(){

        return view('configterms');
    }

    public function configtermsinsertandupdate(){
        $datainsert = Input::all();
        $db = Connectdb::Databaseall();
        $data = json_decode($datainsert['data']);

        $arrInsert = [];
        foreach ($data as $v){
            $arrInsert [$v->name] = $v->value;
        }
        if($arrInsert['id'] == ""){
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'supplier_terms')->insert($arrInsert);
        }else{
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'supplier_terms')->where('id',$arrInsert['id'])->update($arrInsert);
        }

        return 1;
    }

    public function getdataconfigterms(){
        $data = Input::all();

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].supplier_terms.* 
                     FROM $db[fsctaccount].supplier_terms
                     WHERE $db[fsctaccount].supplier_terms.id = '$data[id]'";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;

    }

    public function configconfiggroupsupp(){

        return view('configconfiggroupsupp');
    }

    public function configconfiggroupsuppinsertandupdate(){
        $datainsert = Input::all();
        $db = Connectdb::Databaseall();
        $data = json_decode($datainsert['data']);

        $arrInsert = [];
        foreach ($data as $v){
            $arrInsert [$v->name] = $v->value;
        }
        if($arrInsert['id'] == ""){
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'config_group_supp')->insert($arrInsert);
        }else{
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'config_group_supp')->where('id',$arrInsert['id'])->update($arrInsert);
        }
		

        return 1;
    }

    public function getdataconfigconfiggroupsupp(){
        $data = Input::all();

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].config_group_supp.* 
                     FROM $db[fsctaccount].config_group_supp
                     WHERE $db[fsctaccount].config_group_supp.id = '$data[id]'";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;

    }
    public function configinitial(){

        return view('configinitial');
    }

    public function configinitialinsertandupdate(){
        $datainsert = Input::all();
        $db = Connectdb::Databaseall();
        $data = json_decode($datainsert['data']);

        $arrInsert = [];
        foreach ($data as $v){
            $arrInsert [$v->name] = $v->value;
        }
        if($arrInsert['id'] == ""){
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'initial')->insert($arrInsert);
        }else{
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'initial')->where('id',$arrInsert['id'])->update($arrInsert);
        }

        return 1;
    }
	
    public function getdataconfiginitial(){
        $data = Input::all();

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].initial.* 
                     FROM $db[fsctaccount].initial
                     WHERE $db[fsctaccount].initial.id = '$data[id]'";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;

    }

    public function configstatusaccounts(){
        return view('configstatusaccounts');

    }

    public function configstatusaccinsertandupdate(){
        $datainsert = Input::all();
        $db = Connectdb::Databaseall();
        $data = json_decode($datainsert['data']);

        $arrInsert = [];
        foreach ($data as $v){
            $arrInsert [$v->name] = $v->value;
        }
        if($arrInsert['id'] == ""){
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'accountstatusforprogram')->insert($arrInsert);
        }else{
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'accountstatusforprogram')->where('id',$arrInsert['id'])->update($arrInsert);
        }

        return 1;

    }


    public function getdataconfigstatusacc(){
        $data = Input::all();
        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].accountstatusforprogram.* 
                     FROM $db[fsctaccount].accountstatusforprogram
                     WHERE $db[fsctaccount].accountstatusforprogram.id = '$data[id]'";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;
    }


    public function configwithhold(){
        return view('configwithhold');

    }

    public function configwithholdinsertandupdate(){
        $datainsert = Input::all();
        $db = Connectdb::Databaseall();
        $data = json_decode($datainsert['data']);

        $arrInsert = [];
        foreach ($data as $v){
            $arrInsert [$v->name] = $v->value;
        }
        if($arrInsert['id'] == ""){
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'withhold')->insert($arrInsert);
        }else{
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'withhold')->where('id',$arrInsert['id'])->update($arrInsert);
        }

        return 1;

    }


    public function getdataconfigwithhold(){
        $data = Input::all();
        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].withhold.* 
                     FROM $db[fsctaccount].withhold
                     WHERE $db[fsctaccount].withhold.id = '$data[id]'";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;
    }





}