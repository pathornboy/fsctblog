<?php
/**
 * Created by PhpStorm.
 * User: pacharapol
 * Date: 12/23/2017 AD
 * Time: 9:38 AM
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Api\Connectdb;
use App\working_company;
use DB;
use Illuminate\Support\Facades\Input;



class FsctmainController extends Controller
{
    public function __construct()
    {

    }

    public function getmainmaterialall(){

        $db = Connectdb::Databaseall();

        $sql ='SELECT CONCAT('.$db['fsctmain'].'.type_material.alphabet,'.$db['fsctmain'].'.group_material.alphabet,'.$db['fsctmain'].'.material.id,"__",'.$db['fsctmain'].'.material.name) as label,
                      CONCAT('.$db['fsctmain'].'.type_material.alphabet,'.$db['fsctmain'].'.group_material.alphabet,'.$db['fsctmain'].'.material.id,"__",'.$db['fsctmain'].'.material.name) as value,
                      '.$db['fsctmain'].'.material.id as id
        FROM '.$db['fsctmain'].'.material
        INNER JOIN '.$db['fsctmain'].'.type_material ON '.$db['fsctmain'].'.material.type_material = '.$db['fsctmain'].'.type_material.id
        INNER JOIN '.$db['fsctmain'].'.group_material ON '.$db['fsctmain'].'.group_material.id = type_material.group_id';

        $model = DB::connection('mysql')->select($sql);

        return $model;
    }


    public function getgoodassetall(){

        $db = Connectdb::Databaseall();

        $sql ='SELECT CONCAT('.$db['fsctaccount'].'.type_good.alphabet,'.$db['fsctaccount'].'.group_good.alphabet,'.$db['fsctaccount'].'.good.id,"__",'.$db['fsctaccount'].'.good.name) as label,
                      CONCAT('.$db['fsctaccount'].'.type_good.alphabet,'.$db['fsctaccount'].'.group_good.alphabet,'.$db['fsctaccount'].'.good.id,"__",'.$db['fsctaccount'].'.good.name) as value,
                      '.$db['fsctaccount'].'.good.id as id,
                      '.$db['fsctaccount'].'.good.unit as unit
        FROM '.$db['fsctaccount'].'.good
        INNER JOIN '.$db['fsctaccount'].'.type_good ON '.$db['fsctaccount'].'.type_good.id = '.$db['fsctaccount'].'.good.type_id
        INNER JOIN '.$db['fsctaccount'].'.group_good ON '.$db['fsctaccount'].'.group_good.id = '.$db['fsctaccount'].'.type_good.group_id
        WHERE group_supplier = "1" ';

        $model = DB::connection('mysql')->select($sql);

        return $model;
    }

    public function getotherall(){

        $db = Connectdb::Databaseall();

        $sql ='SELECT CONCAT('.$db['fsctaccount'].'.type_good.alphabet,'.$db['fsctaccount'].'.group_good.alphabet,'.$db['fsctaccount'].'.good.id,"__",'.$db['fsctaccount'].'.good.name) as label,
                      CONCAT('.$db['fsctaccount'].'.type_good.alphabet,'.$db['fsctaccount'].'.group_good.alphabet,'.$db['fsctaccount'].'.good.id,"__",'.$db['fsctaccount'].'.good.name) as value,
                      '.$db['fsctaccount'].'.good.id as id,
                      '.$db['fsctaccount'].'.good.unit as unit
        FROM '.$db['fsctaccount'].'.good
        INNER JOIN '.$db['fsctaccount'].'.type_good ON '.$db['fsctaccount'].'.type_good.id = '.$db['fsctaccount'].'.good.type_id
        INNER JOIN '.$db['fsctaccount'].'.group_good ON '.$db['fsctaccount'].'.group_good.id = '.$db['fsctaccount'].'.type_good.group_id
        WHERE group_supplier = "3" ';

        $model = DB::connection('mysql')->select($sql);

        return $model;
    }


}
