<?php

namespace App\Http\Controllers;


use App\Item;
use App\Api\Connectdb;
use DB;
use Excel;
use Illuminate\Support\Facades\Input;
class PRExcelController extends Controller
{
	public function importExport()
	{
		return view('importExport');
	}
	public function downloadExcelPR()
	{
		$datapost = Input::all();
		// print_r($datapost);
		
		$type = 'xls';
	
		// exit();
		 
		$db = Connectdb::Databaseall();
		//$data = Item::get()->toArray();
		$date = date('Y-m-d');
		$week = $datapost['week'];
		$month = $datapost['month'];
		$year = $datapost['year'];
		$status = $datapost['status'];
		$branch = $datapost['branch'];
		$other = "  AND H.branch_id = '$branch'";
			
		 $sql = "SELECT *
				FROM $db[fsctaccount].ppr_head AS H
				INNER JOIN $db[fsctaccount].ppr_detail AS D on H.id = D.ppr_headid
				INNER JOIN $db[fsctaccount].type_pay AS P on H.type_pay = P.id
				INNER JOIN $db[fsctaccount].type_buy AS B on H.type_buy = B.id
				WHERE D.status = 1 
				AND (H.week = '$week' OR (H.status = '$status') OR (H.date like '____-$month-__' AND H.date like '$year%'))".$other; 
				
		$model = DB::connection('mysql')->select($sql);
		
		$ss = "SELECT count(*) as num
					FROM $db[fsctaccount].ppr_head AS H
					INNER JOIN $db[fsctaccount].ppr_detail AS D on H.id = D.ppr_headid
					INNER JOIN $db[fsctaccount].type_pay AS P on H.type_pay = P.id
					INNER JOIN $db[fsctaccount].type_buy AS B on H.type_buy = B.id
					WHERE D.status = 1 
					AND (H.week = '$week' OR (H.status = '$status') OR (H.date like '____-$month-__' AND H.date like '$year%'))".$other;
		$mod = DB::connection('mysql')->select($ss);
		
			// $count = 9;
			foreach($mod as $d => $v){
				$count = $v->num;
			}
			
		
		$data = [];
		$i = 1;
		foreach($model as $k => $val){
			$data[$k]['#']=$i;
			$data[$k]['เลขใบ PO']=$val->number_ppr;
			$data[$k]['วันเดือนปี']=$val->date;
			$data[$k]['ราคา']=number_format($val->price,2);
			$data[$k]['จำนวน']=$val->amount;
			$data[$k]['vat (บาท)']=number_format((($val->vat * ($val->price * $val->amount))/100),2);
			$data[$k]['ภาษีหัก ณ ที่จ่าย (บาท)']=number_format((((($val->vat * ($val->price * $val->amount))/100) * $val->withhold)/100),2);
			$data[$k]['ประเภทการซื้อ']= $val->name_buy;		
			$data[$k]['ประเภทการจ่าย']= $val->name_pay;
			$data[$k]['รวม']=number_format($val->total,2);
			
			$i++;
				
		}
			
		
		return Excel::create('Report_PR_Excel_'.$date, function($excel) use ($data,$count) {
	
			$excel->sheet('mySheet', function($sheet) use ($data,$count)
	        {
								
				$sheet->fromArray(array(array('')));
				$sheet->fromArray($data);
				// Set top, right, bottom, left
				$sheet->setPageMargin(array(
				0.25, 0.30, 0.25, 0.30
				));
				$sheet->setAutoSize(true);
				
				$sheet->cell('A1', function($cell) {
					$cell->setValue('รายงานใบสั่งซื้อ  (PR)');
					 // Set font
                    $cell->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '18',
                        'bold'       =>  true
					));	
				});

				// $sheet->cell('C2', function($cell) {
					// $cell->setValue('สาขา  :  ');
					 // Set font
                    // $cell->setFont(array(
                        // 'family'     => 'Calibri',
                        // 'size'       => '14',
                        // 'bold'       =>  false
					// ));	
				// });
								
				// Sets borders
				
					$styleArray = array(
						'borders' => array(
							'allborders' => array(
								'style' => 'thin'
							)
						)
					);
					
					$sheet->fromArray(array(array('','','','')),'','','',''); // empty rows
					
					$sheet->fromArray(array(
						array('รวมทั้งหมด  :  ','','',"=SUM(D4:D".(($count+4)-1).")","=SUM(E4:E".(($count+4)-1).")","=SUM(F4:F".(($count+4)-1).")","=SUM(G4:G".(($count+4)-1).")",
						'','',"=SUM(J4:J".(($count+4)-1).")")),'','','',''); //sum rows
					
					
					// Apply the style
					$sheet->getDefaultStyle()
						->applyFromArray($styleArray);
				
				$sheet->mergeCells('A1:I1');
				$sheet->mergeCells("A".(($count+4)+1).":B".(($count+4)+1));
				$sheet->getStyle('A1:I1')->getAlignment()->applyFromArray(
					array('horizontal' => 'center')
				);
				$sheet->getStyle("A".(($count+4)+1).":B".(($count+4)+1))->getAlignment()->applyFromArray(
					array('horizontal' => 'center')
				);
				$sheet->fromArray();
				
	        });
			
		})
		->download($type);
		
		// ->store('xls', storage_path('excel/exports'));
	}
	public function importExcel()
	{
		if(Input::hasFile('import_file')){
			$path = Input::file('import_file')->getRealPath();
			$data = Excel::load($path, function($reader) {
			})->get();
			if(!empty($data) && $data->count()){
				foreach ($data as $key => $value) {
					$insert[] = ['title' => $value->title, 'description' => $value->description];
				}
				if(!empty($insert)){
					DB::table('items')->insert($insert);
					dd('Insert Record successfully.');
				}
			}
		}
		return back();
	}
}
?>