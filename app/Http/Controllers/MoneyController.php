<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Session;
use App\Api\Connectdb;

class MoneyController extends Controller
{
        public function withdraw_money(){
            return view('withdraw_money');
        }

        public function insertpaypre(){
            $data = Input::all();
            $db = Connectdb::Databaseall();

            $brcode = Session::get('brcode');
            $emp_code = Session::get('emp_code');
            $arrInsert = ['id'=>'',
                          'datetime'=>date('Y-m-d H:i:s'),
                          'list'=>$data['data']['list'],
                          'amount'=>$data['data']['amount'],
                          'vat'=>$data['data']['vat'],
                          'vat_money'=>$data['data']['vat_money'],
                          'total'=>$data['data']['total'],
                          'po_ref'=>$data['data']['po_ref'],
                          'note'=>$data['data']['note'],
                          'id_compay'=>$data['data']['id_compay'],
                          'branch'=>$brcode,
                          'code_emp'=>$emp_code
                        ];


          $model = DB::connection('mysql')->table($db['fsctaccount'].'.reservemoney')->insertGetId($arrInsert);

          $array = ['id'=>'',
                  'datetimeinsert'=>date('Y-m-d H:i:s'),
                  'money'=>$data['data']['total'],
                  'typedoc'=>14,
                  'emp_code'=>$emp_code,
                  'branch'=>$brcode,
                  'status'=>1,
                  'typetranfer'=>1,
                  'log'=>$data['data']['note'],
                  'ref'=>$model,
                  'typereftax'=>$model
                  ];

        $insertmodel = DB::connection('mysql')->table($db['fsctaccount'].'.'.'insertcashrent')->insert($array);

          return $model;

        }


      public function insertporef(){
          $data = Input::all();


          $idreservemoney = $data['id'];
          $id = $data['ponumber'];
          // print_r($_FILES['fileref']['tmp_name']);
          move_uploaded_file($_FILES['fileref']['tmp_name'], 'uploads_po/' .$id);
          // exit;
          $db = Connectdb::Databaseall();
           $sql = "SELECT $db[fsctaccount].po_head.*
                  FROM $db[fsctaccount].po_head
                  WHERE $db[fsctaccount].po_head.po_number = '$id'
                  AND status_head = '2' ";



          $dataquery = DB::connection('mysql')->select($sql);


          if(!empty($dataquery)){
            // echo "string";
            // exit;
            $idporef = $dataquery[0]->id;
            $totolsumall = $dataquery[0]->totolsumall;

            $brcode = Session::get('brcode');
            $emp_code = Session::get('emp_code');

            $arrCash = ['id'=>'',
                        'datetimeinsert'=>date('Y-m-d H:i:s'),
                        'money'=>$totolsumall,
                        'typedoc'=>16,
                        'emp_code'=>$emp_code,
                        'branch'=>$brcode,
                        'status'=>1,
                        'typetranfer'=>1,
                        'log'=>'กด atm โอนเงินสำรองจ่ายคืน สาขา'.$brcode.' โดย '.$emp_code,
                        'ref'=>$idporef,
                        'typereftax'=>$idporef
                      ];

             $tranfermodel = DB::connection('mysql')->table($db['fsctaccount'].'.insertcashrent')->insertGetId($arrCash);



            $model = DB::connection('mysql')->table($db['fsctaccount'].'.reservemoney')->where('id',$idreservemoney)->update(['po_ref'=>$idporef]);


            echo $sqlreservemoney = " SELECT $db[fsctaccount].reservemoney.*
                                FROM $db[fsctaccount].reservemoney
                                WHERE $db[fsctaccount].reservemoney.id = '$idreservemoney' ";

            $datareservemoneyquery = DB::connection('mysql')->select($sqlreservemoney);


            echo "<pre>";
            print_r($datareservemoneyquery);
            exit;








                      DB::table('inform_po')->insert(
                      ['po_ref' => $id,
                       'id_po' => $idporef,
                       'bill_no'=>$data['bill_no'],
                       'vat_percent' => $data['vat_percent'],
                       'vat_price' => $data['vat_price'],
                       'wht_percent'=>0,
                       'wht'=>0,
                       'type_pay' => 1,
                       'payout' => $data['payout'],
                       'change_pay' => $data['change_pay'],
                       'inform_po_picture' =>  $data['po_ref'],
                       'datebill'=>$data['datebill']
                      ]);


                $emp_code = Session::get('emp_code');
                $brcode = Session::get('brcode');


                      $arrvalue['id']= '';
                      $arrvalue['po_headid']= $data['id_po'];
                      $arrvalue['datebill']= date('Y-m-d');
                      $arrvalue['bill_no']= $data['bill_no'];
                      $arrvalue['pay_real']= $data['payout'];
                      $arrvalue['returnperbill']= $data['change_pay'];
                      $arrvalue['status']= 1;
                      $arrvalue['code_emp']= $emp_code;
                      $arrvalue['createby']= $emp_code;
                      $arrvalue['createdate']= date('Y-m-d H:i:s');


                      DB::table('po_bill_run_money')->insert(
                      ['id' => '',
                        'po_headid' => $data['id_po'],
                        'datebill' =>date('Y-m-d'),
                        'bill_no'=>$data['bill_no'],
                        'pay_real'=>$data['payout'],
                        'returnperbill' => $data['change_pay'],
                        'status' => '1',
                        'code_emp' => $emp_code,
                        'createby' => $emp_code,
                        'createdate' => date('Y-m-d H:i:s')
                      ]);



                  // echo "string";
                  // exit;

                if($data['totolsumall']==$data['payout']){
                    //// จ่ายครบ
                      $modelupdate = DB::table('po_head')->where('id',$data['id_po'])->update(['status_head'=>3]);

                }else if($data['totolsumall'] > $data['payout']){
                    //// มีถอน
                      $modelupdate = DB::table('po_head')->where('id',$data['id_po'])->update(['status_head'=>4]);


                }else if($data['totolsumall'] < $data['payout']){
                    //// ขาด
                      $modelupdate = DB::table('po_head')->where('id',$data['id_po'])->update(['status_head'=>5]);


                }



              return 1;
          }else{

              return 0;
          }





      }
}
