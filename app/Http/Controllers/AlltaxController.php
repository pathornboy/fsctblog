<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Illuminate\Support\Facades\Input;
use Session;
use App\Api\Connectdb;

class AlltaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $db = Connectdb::Databaseall();

        // $sql = "SELECT * FROM $db[fsctaccount].po_head INNER JOIN $db[fsctaccount].withholdtaxpo ON ( $db[fsctaccount].po_head.id = $db[fsctaccount].withholdtaxpo.id_po ) INNER JOIN $db[fsctaccount].supplier ON ($db[fsctaccount].supplier.id = $db[fsctaccount].po_head.supplier_id)  WHERE withholdtaxpo.status = '1' ";

        // $result_sql = DB::connection('mysql')->select($sql);
        return view('tax3');
    }

    public function gen_tax3()
    {
        $db = Connectdb::Databaseall();

        $sql = "SELECT id_po, beforevat, total FROM $db[fsctaccount].withholdtaxpo WHERE status = '1'";

        $result_sql = DB::connection('mysql')->select($sql);

        function tax3($po_id, $key, $beforevat, $total){

            $db = Connectdb::Databaseall();

            $tmp = "";

            $tmp .= ++$key."||";

            $s = "SELECT inv_number, supplier_id FROM $db[fsctaccount].po_head INNER JOIN $db[hr_base].working_company ON ( $db[fsctaccount].po_head.id_company = $db[hr_base].working_company.id ) WHERE status = '1' AND po_head.id = '$po_id' ";

            $r_s = DB::connection('mysql')->select($s);
            $in_no = $r_s[0]->inv_number;
            $supplier_id = $r_s[0]->supplier_id;

            $s = "SELECT * FROM $db[fsctaccount].supplier WHERE status = '1' AND id = '$supplier_id' ";

            $r_s = DB::connection('mysql')->select($s);
            $supplier_tax_id = $r_s[0]->tax_id;
            $pre = $r_s[0]->pre;
            $name_supplier = $r_s[0]->name_supplier;
            $name_supplier = explode(' ', $name_supplier);
            $address = $r_s[0]->address;
            $district = $r_s[0]->district;
            $amphur = $r_s[0]->amphur;
            $province = $r_s[0]->province;


            // echo "<pre>";
            // print_r($r_s);
            // var_dump($s);

            $date = date("Y-m-d");
            // return $date;
            $date_explode = explode('-', $date);



            $tmp .= "$in_no|0000000000|00000|1||1|0|0|$date_explode[0]|$date_explode[1]|$supplier_tax_id||0|$pre|$name_supplier[0]|$name_supplier[1]|||||$address|||$district|$amphur|$province|||$date_explode[2]$date_explode[1]$date_explode[0]|";
            $tmp .= "ค่าบริการ, รับจ้างทำของ|3.00|$beforevat|$total|1|";
            return $tmp;
        }

        $temp_return = '';
        foreach ($result_sql as $key => $value) {
            # code...
            $po_id = $result_sql[0]->id_po;
            $beforevat = $result_sql[0]->id_po;
            $total = $result_sql[0]->id_po;
            // echo $value->id_po." ";
            $temp_return .= tax3($po_id, $key, $beforevat, $total)."\r\n";
        }

        $content = $temp_return;

        return response($content)
                ->withHeaders([
                    'Content-Type' => 'text/plain',
                    'Cache-Control' => 'no-store, no-cache',
                    'Content-Disposition' => 'attachment; filename="logs.txt',
                ]);

        // return $temp_return;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
