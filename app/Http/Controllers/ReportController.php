<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Input;
use Excel;
use Session;
use App\Api\Connectdb;
use App\Api\Datetime;


class ReportController extends Controller {

      public function reportcashdaily(){

        return view('reportcashdaily');
      }

      public function reportcashdailynew(){

        return view('reportcashdailynew');
      }

      public function reporttotalcashdaily(){

        return view('reporttotalcashdaily');
      }

      public function getdatacashedit(){

        return view('getdatacashedit');

      }

      public function cashcheckbank(){

        return view('cashcheckbank');
      }

      public function reporttaxbuy(){ //รายงานภาษีซื้อ

            return view('reporttaxbuy');
      }

      public function reportexpenses(){ //รายงานค่าใช้จ่าย

            return view('reportexpenses');
      }

      public function reporttaxsell(){ //รายงานภาษีขาย

            return view('reporttaxsell');
      }

      public function reporttaxsellinsurance(){ //รายงานภาษีขาย (เงินประกัน)

            return view('reporttaxsellinsurance');
      }

      public function reporttaxwht(){ //รายงานภาษีหัก ณ ที่จ่าย

            return view('reporttaxwht');
      }

      public function searchreportcashdaily(){  //รายงานเงิดสดย่อยรายวัน (อันเก่า)

          $data = Input::all();
          $db = Connectdb::Databaseall();
          // echo "<pre>";
          // print_r($data);
          // exit;
          // $datetime = DateTime::DateToMysqlDB($data['datepicker']);
          // $datetime = DateTime::FormatDateFromCalendarRange($data['datepicker']);

          $datepicker = explode("/",trim(($data['datepicker'])));
          if(count($datepicker) > 0) {
              $datetime = $datepicker[2] . '-' . $datepicker[1] . '-' . $datepicker[0]; //วัน - เดือน - ปี
          }

          $branch_id = $data['branch_id'];

          $sql = 'SELECT '.$db['fsctmain'].'.bill_rent.*,
                         '.$db['fsctmain'].'.bill_rent.id as idbillrent

                  FROM '.$db['fsctmain'].'.bill_rent
                  INNER JOIN  '.$db['fsctmain'].'.quotation_head
                     ON '.$db['fsctmain'].'.bill_rent.qt_id = '.$db['fsctmain'].'.quotation_head.id

                  WHERE '.$db['fsctmain'].'.bill_rent.startdate = "'.$datetime.'"
                    AND '.$db['fsctmain'].'.bill_rent.branch_id = '.$branch_id.'
                    AND '.$db['fsctmain'].'.quotation_head.status NOT IN ( 0 , 99)';

          $datatresult = DB::connection('mysql')->select($sql);
          // echo "<pre>";
          // print_r($datatresult);
          // exit;
          $sqlloss = 'SELECT '.$db['fsctmain'].'.quotation_head.*,
                         '.$db['fsctmain'].'.bill_rent.*,
                         '.$db['fsctaccount'].'.taxinvoice_loss_abb.*

                  FROM '. $db['fsctaccount'].'.taxinvoice_loss_abb
                  INNER JOIN  '.$db['fsctmain'].'.bill_rent
                     ON '. $db['fsctaccount'].'.taxinvoice_loss_abb.bill_rent = '. $db['fsctmain'].'.bill_rent.id
                  INNER JOIN  '.$db['fsctmain'].'.quotation_head
                     ON '. $db['fsctmain'].'.bill_rent.qt_id = '. $db['fsctmain'].'.quotation_head.id

                  WHERE '.$db['fsctaccount'].'.taxinvoice_loss_abb.time LIKE "%'.$datetime.'%"
                    AND '.$db['fsctaccount'].'.taxinvoice_loss_abb.branch_id = '.$branch_id.'
                    AND '.$db['fsctmain'].'.quotation_head.status NOT IN ( 0 , 99)
                    AND '.$db['fsctaccount'].'.taxinvoice_loss_abb.status IN (1,98)
                    AND '.$db['fsctaccount'].'.taxinvoice_loss_abb.type NOT IN (2)
                    ';

          $datatresultloss = DB::connection('mysql')->select($sqlloss);

          $sqlmore = 'SELECT '.$db['fsctmain'].'.quotation_head.*,
                         '.$db['fsctmain'].'.bill_rent.*,
                         '.$db['fsctaccount'].'.taxinvoice_more_abb.*

                  FROM '. $db['fsctaccount'].'.taxinvoice_more_abb
                  INNER JOIN  '.$db['fsctmain'].'.bill_rent
                     ON '. $db['fsctaccount'].'.taxinvoice_more_abb.bill_rent = '. $db['fsctmain'].'.bill_rent.id
                  INNER JOIN  '.$db['fsctmain'].'.quotation_head
                     ON '. $db['fsctmain'].'.bill_rent.qt_id = '. $db['fsctmain'].'.quotation_head.id

                  WHERE '.$db['fsctaccount'].'.taxinvoice_more_abb.time LIKE "%'.$datetime.'%"
                    AND '.$db['fsctaccount'].'.taxinvoice_more_abb.branch_id = '.$branch_id.'
                    AND '.$db['fsctmain'].'.quotation_head.status NOT IN ( 0 , 99)
                    AND '.$db['fsctaccount'].'.taxinvoice_more_abb.status IN (1,98)
                    AND '.$db['fsctaccount'].'.taxinvoice_more_abb.type NOT IN (2)
                    ';

          $datatresultmore = DB::connection('mysql')->select($sqlmore);

          $sqlpa = 'SELECT '.$db['fsctmain'].'.quotation_head.*,
                           '.$db['fsctmain'].'.bill_rent.*,
                           '.$db['fsctaccount'].'.taxinvoice_partial_abb.*

                    FROM '. $db['fsctaccount'].'.taxinvoice_partial_abb
                    INNER JOIN  '.$db['fsctmain'].'.bill_rent
                       ON '. $db['fsctaccount'].'.taxinvoice_partial_abb.bill_rent = '. $db['fsctmain'].'.bill_rent.id
                    INNER JOIN  '.$db['fsctmain'].'.quotation_head
                       ON '. $db['fsctmain'].'.bill_rent.qt_id = '. $db['fsctmain'].'.quotation_head.id

                    WHERE '.$db['fsctaccount'].'.taxinvoice_partial_abb.time LIKE "%'.$datetime.'%"
                      AND '.$db['fsctaccount'].'.taxinvoice_partial_abb.branch_id = '.$branch_id.'
                      AND '.$db['fsctmain'].'.quotation_head.status NOT IN ( 0 , 99)
                      AND '.$db['fsctaccount'].'.taxinvoice_partial_abb.status IN (1,98)
                      AND '.$db['fsctaccount'].'.taxinvoice_partial_abb.type NOT IN (2)
                      ';

          $datatresultpa = DB::connection('mysql')->select($sqlpa);

          $sqlspa = 'SELECT '.$db['fsctmain'].'.quotation_head.*,
                            '.$db['fsctmain'].'.bill_rent.*,
                            '.$db['fsctaccount'].'.taxinvoice_special_abb.*

                    FROM '. $db['fsctaccount'].'.taxinvoice_special_abb
                    INNER JOIN  '.$db['fsctmain'].'.bill_rent
                       ON '. $db['fsctaccount'].'.taxinvoice_special_abb.bill_rent_id = '. $db['fsctmain'].'.bill_rent.id
                    INNER JOIN  '.$db['fsctmain'].'.quotation_head
                       ON '. $db['fsctmain'].'.bill_rent.qt_id = '. $db['fsctmain'].'.quotation_head.id

                    WHERE '.$db['fsctaccount'].'.taxinvoice_special_abb.date_approved LIKE "%'.$datetime.'%"
                      AND '.$db['fsctaccount'].'.taxinvoice_special_abb.branch_id = '.$branch_id.'
                      AND '.$db['fsctmain'].'.quotation_head.status NOT IN ( 0 , 99)
                      AND '.$db['fsctaccount'].'.taxinvoice_special_abb.status IN (1,98)
                      AND '.$db['fsctaccount'].'.taxinvoice_special_abb.type NOT IN (2)
                      ';

          $datatresultspa = DB::connection('mysql')->select($sqlspa);

          $sqlreturn = 'SELECT '.$db['fsctmain'].'.bill_return_head.*,
                               '.$db['fsctmain'].'.bill_rent.*,
                               '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.*

                        FROM '.$db['fsctmain'].'.bill_rent
                        INNER JOIN '.$db['fsctmain'].'.bill_return_head
                           ON '.$db['fsctmain'].'.bill_rent.id = '.$db['fsctmain'].'.bill_return_head.bill_rent

                        INNER JOIN '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote
                           ON '.$db['fsctmain'].'.bill_rent.id = '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.bill_rent

                        WHERE '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.time = "'.$datetime.'"
                          AND '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.branch_id = '.$branch_id.'
                        ';

          $datatresultreturn = DB::connection('mysql')->select($sqlreturn);

          $sqlengine = 'SELECT '.$db['fsctmain'].'.billengine_rent.*,
                               '.$db['fsctmain'].'.billengine_rent.id as idbillrent,
                               '.$db['fsctmain'].'.quotationengine_head.*

                        FROM '.$db['fsctmain'].'.billengine_rent
                        INNER JOIN  '.$db['fsctmain'].'.quotationengine_head
                           ON '.$db['fsctmain'].'.billengine_rent.qt_id = '.$db['fsctmain'].'.quotationengine_head.id

                        WHERE '.$db['fsctmain'].'.billengine_rent.startdate = "'.$datetime.'"
                          AND '.$db['fsctmain'].'.billengine_rent.branch_id = '.$branch_id.'
                          AND '.$db['fsctmain'].'.quotationengine_head.status NOT IN ( 0 , 99)
                          ';

          $datatresultengine = DB::connection('mysql')->select($sqlengine);

          $sqlengineloss = 'SELECT '.$db['fsctmain'].'.quotationengine_head.*,
                               '.$db['fsctmain'].'.billengine_rent.*,
                               '.$db['fsctaccount'].'.taxinvoice_loss_abb.*

                            FROM '. $db['fsctaccount'].'.taxinvoice_loss_abb
                            INNER JOIN  '.$db['fsctmain'].'.billengine_rent
                               ON '. $db['fsctaccount'].'.taxinvoice_loss_abb.bill_rent = '. $db['fsctmain'].'.billengine_rent.id
                            INNER JOIN  '.$db['fsctmain'].'.quotationengine_head
                               ON '. $db['fsctmain'].'.billengine_rent.qt_id = '. $db['fsctmain'].'.quotationengine_head.id

                            WHERE '.$db['fsctaccount'].'.taxinvoice_loss_abb.time LIKE "%'.$datetime.'%"
                              AND '.$db['fsctaccount'].'.taxinvoice_loss_abb.branch_id = '.$branch_id.'
                              AND '.$db['fsctmain'].'.quotationengine_head.status NOT IN ( 0 , 99)
                              AND '.$db['fsctaccount'].'.taxinvoice_loss_abb.status IN (1,98)
                              AND '.$db['fsctaccount'].'.taxinvoice_loss_abb.type IN (2)
                              ';

          $datatresultengineloss = DB::connection('mysql')->select($sqlengineloss);

          $sqlenginemore = 'SELECT '.$db['fsctmain'].'.quotationengine_head.*,
                               '.$db['fsctmain'].'.billengine_rent.*,
                               '.$db['fsctaccount'].'.taxinvoice_more_abb.*

                            FROM '. $db['fsctaccount'].'.taxinvoice_more_abb
                            INNER JOIN  '.$db['fsctmain'].'.billengine_rent
                               ON '. $db['fsctaccount'].'.taxinvoice_more_abb.bill_rent = '. $db['fsctmain'].'.billengine_rent.id
                            INNER JOIN  '.$db['fsctmain'].'.quotationengine_head
                               ON '. $db['fsctmain'].'.billengine_rent.qt_id = '. $db['fsctmain'].'.quotationengine_head.id

                            WHERE '.$db['fsctaccount'].'.taxinvoice_more_abb.time LIKE "%'.$datetime.'%"
                              AND '.$db['fsctaccount'].'.taxinvoice_more_abb.branch_id = '.$branch_id.'
                              AND '.$db['fsctmain'].'.quotationengine_head.status NOT IN ( 0 , 99)
                              AND '.$db['fsctaccount'].'.taxinvoice_more_abb.status IN (1,98)
                              AND '.$db['fsctaccount'].'.taxinvoice_more_abb.type IN (2)
                              ';

          $datatresultenginemore = DB::connection('mysql')->select($sqlenginemore);

          $sqlenginepa = 'SELECT '.$db['fsctmain'].'.quotationengine_head.*,
                                 '.$db['fsctmain'].'.billengine_rent.*,
                                 '.$db['fsctaccount'].'.taxinvoice_partial_abb.*

                          FROM '. $db['fsctaccount'].'.taxinvoice_partial_abb
                          INNER JOIN  '.$db['fsctmain'].'.billengine_rent
                             ON '. $db['fsctaccount'].'.taxinvoice_partial_abb.bill_rent = '. $db['fsctmain'].'.billengine_rent.id
                          INNER JOIN  '.$db['fsctmain'].'.quotationengine_head
                             ON '. $db['fsctmain'].'.billengine_rent.qt_id = '. $db['fsctmain'].'.quotationengine_head.id

                          WHERE '.$db['fsctaccount'].'.taxinvoice_partial_abb.time LIKE "%'.$datetime.'%"
                            AND '.$db['fsctaccount'].'.taxinvoice_partial_abb.branch_id = '.$branch_id.'
                            AND '.$db['fsctmain'].'.quotationengine_head.status NOT IN ( 0 , 99)
                            AND '.$db['fsctaccount'].'.taxinvoice_partial_abb.status IN (1,98)
                            AND '.$db['fsctaccount'].'.taxinvoice_partial_abb.type IN (2)
                            ';

          $datatresultenginepa = DB::connection('mysql')->select($sqlenginepa);

          $sqlenginespa = 'SELECT '.$db['fsctmain'].'.quotationengine_head.*,
                                 '.$db['fsctmain'].'.billengine_rent.*,
                                 '.$db['fsctaccount'].'.taxinvoice_special_abb.*

                          FROM '. $db['fsctaccount'].'.taxinvoice_special_abb
                          INNER JOIN  '.$db['fsctmain'].'.billengine_rent
                             ON '. $db['fsctaccount'].'.taxinvoice_special_abb.bill_rent_id = '. $db['fsctmain'].'.billengine_rent.id
                          INNER JOIN  '.$db['fsctmain'].'.quotationengine_head
                             ON '. $db['fsctmain'].'.billengine_rent.qt_id = '. $db['fsctmain'].'.quotationengine_head.id

                          WHERE '.$db['fsctaccount'].'.taxinvoice_special_abb.date_approved LIKE "%'.$datetime.'%"
                            AND '.$db['fsctaccount'].'.taxinvoice_special_abb.branch_id = '.$branch_id.'
                            AND '.$db['fsctmain'].'.quotationengine_head.status NOT IN ( 0 , 99)
                            AND '.$db['fsctaccount'].'.taxinvoice_special_abb.status IN (1,98)
                            AND '.$db['fsctaccount'].'.taxinvoice_special_abb.type IN (2)
                            ';

          $datatresultenginespa = DB::connection('mysql')->select($sqlenginespa);

          $sqlreturnengine = 'SELECT '.$db['fsctmain'].'.billengine_return_head.*,
                                     '.$db['fsctmain'].'.billengine_rent.*,
                                     '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.*

                              FROM '.$db['fsctmain'].'.billengine_rent
                              INNER JOIN '.$db['fsctmain'].'.billengine_return_head
                                 ON '.$db['fsctmain'].'.billengine_rent.id = '.$db['fsctmain'].'.billengine_return_head.bill_rent

                              INNER JOIN '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote
                                 ON '.$db['fsctmain'].'.billengine_rent.id = '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.bill_rent

                              WHERE '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.time LIKE "%'.$datetime.'%"
                                AND '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.branch_id = '.$branch_id.'
                              ';

          $datatresultreturnengine = DB::connection('mysql')->select($sqlreturnengine);
          // echo "<pre>";
          // print_r($datatresultreturnengine);
          // exit;
          return view('reportcashdaily',[
            'data'=>$datatresult,
            'dataloss'=>$datatresultloss,
            'datamore'=>$datatresultmore,
            'datapa'=>$datatresultpa,
            'dataspa'=>$datatresultspa,
            'datareturn'=>$datatresultreturn,
            'dataengine'=>$datatresultengine,
            'dataengineloss'=>$datatresultengineloss,
            'dataenginemore'=>$datatresultenginemore,
            'dataenginepa'=>$datatresultenginepa,
            'dataenginespa'=>$datatresultenginespa,
            'datareturnengine'=>$datatresultreturnengine,
            'query'=>true,
            'datepicker'=>$data['datepicker'],
            'branch_id'=>$branch_id,
            'datepicker2'=>$datetime]);
      }

      public function searchreporttotalcashdaily(){  //รายงานภาพรวมเงินสดย่อย

          $data = Input::all();
          $db = Connectdb::Databaseall();
          // echo "<pre>";
          // print_r($data);
          // exit;
          // $datetime = DateTime::DateToMysqlDB($data['datepicker']);
          // $datetime = DateTime::FormatDateFromCalendarRange($data['datepicker']);

          $datepicker = explode("/",trim(($data['datepicker'])));
          if(count($datepicker) > 0) {
              $datetime = $datepicker[2] . '-' . $datepicker[1] . '-' . $datepicker[0]; //วัน - เดือน - ปี
          }

          $branch_id = $data['branch_id'];

          $sql = 'SELECT '.$db['fsctmain'].'.bill_rent.*,
                         '.$db['fsctmain'].'.quotation_head.*

                  FROM '.$db['fsctmain'].'.bill_rent
                  INNER JOIN  '.$db['fsctmain'].'.quotation_head
                     ON '.$db['fsctmain'].'.bill_rent.qt_id = '.$db['fsctmain'].'.quotation_head.id

                  WHERE '.$db['fsctmain'].'.bill_rent.startdate = "'.$datetime.'"
                    AND '.$db['fsctmain'].'.bill_rent.branch_id = '.$branch_id.'
                    AND '.$db['fsctmain'].'.quotation_head.status NOT IN ( 0 , 99)';

          $datatresult = DB::connection('mysql')->select($sql);

          $sqlreturn = 'SELECT '.$db['fsctmain'].'.bill_return_head.*,
                               '.$db['fsctmain'].'.bill_rent.*

                        FROM '.$db['fsctmain'].'.bill_rent
                        INNER JOIN '.$db['fsctmain'].'.bill_return_head
                           ON '.$db['fsctmain'].'.bill_rent.id = '.$db['fsctmain'].'.bill_return_head.bill_rent

                        WHERE '.$db['fsctmain'].'.bill_return_head.time = "'.$datetime.'"
                          AND '.$db['fsctmain'].'.bill_return_head.branch_id = '.$branch_id.'
                        ';

          $datatresultreturn = DB::connection('mysql')->select($sqlreturn);

          $sqlengine = 'SELECT '.$db['fsctmain'].'.billengine_rent.*,
                               '.$db['fsctmain'].'.quotationengine_head.*

                        FROM '.$db['fsctmain'].'.billengine_rent
                        INNER JOIN  '.$db['fsctmain'].'.quotationengine_head
                           ON '.$db['fsctmain'].'.billengine_rent.qt_id = '.$db['fsctmain'].'.quotationengine_head.id

                        WHERE '.$db['fsctmain'].'.billengine_rent.startdate = "'.$datetime.'"
                          AND '.$db['fsctmain'].'.billengine_rent.branch_id = '.$branch_id.'
                          AND '.$db['fsctmain'].'.quotationengine_head.status NOT IN ( 0 , 99)';

          $datatresultengine = DB::connection('mysql')->select($sqlengine);

          $sqlreturnengine = 'SELECT '.$db['fsctmain'].'.billengine_return_head.*,
                                     '.$db['fsctmain'].'.billengine_rent.*,
                                     '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.*

                              FROM '.$db['fsctmain'].'.billengine_rent
                              INNER JOIN '.$db['fsctmain'].'.billengine_return_head
                                 ON '.$db['fsctmain'].'.billengine_rent.id = '.$db['fsctmain'].'.billengine_return_head.bill_rent

                             INNER JOIN '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote
                                ON '.$db['fsctmain'].'.billengine_rent.id = '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.bill_rent

                              WHERE '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.time LIKE "%'.$datetime.'%"
                                AND '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.branch_id = '.$branch_id.'
                              ';

          $datatresultreturnengine = DB::connection('mysql')->select($sqlreturnengine);
          // echo "<pre>";
          // print_r($datatresult);
          // exit;
          return view('reporttotalcashdaily',[
            'data'=>$datatresult,
            'datareturn'=>$datatresultreturn,
            'dataengine'=>$datatresultengine,
            'datareturnengine'=>$datatresultreturnengine,
            'query'=>true,
            'datepicker'=>$data['datepicker'],
            'branch_id'=>$branch_id,
            'datepicker2'=>$datetime]);
      }

      //กรณีเมื่อคลิก นำฝาก (อันเก่า)
      public function configreportcashdaily(){

            $datainsert = Input::all();
            $db = Connectdb::Databaseall();
            // echo "<pre>";
            // print_r($datainsert);
            // exit;
            //$data = json_decode($datainsert['datainsert']);

            $emp_id = Session::get('emp_code');
            // $range_time = Session::get('range_time');
            // $type = Session::get('type');
            // $allmonth = Session::get('allmonth');

            $arrInsert = [];
            foreach ($datainsert['time'] as $key => $value){
                $arrInsert[$key]['id'] = '';
                $arrInsert[$key]['grandtotal'] = $datainsert['sumall1'];
                $arrInsert[$key]['branch_id'] = $datainsert['branch_id'];
                $arrInsert[$key]['codeemp'] = $emp_id;
                $arrInsert[$key]['time'] = $value;
                $arrInsert[$key]['note'] = $datainsert['grandtotal'];
                $arrInsert[$key]['status'] = 1;
            }
            // echo "<pre>";
            // print_r($arrInsert);
            // exit;
            if($arrInsert[$key]['id'] == ""){
                $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'cash')->insert($arrInsert);

            }

            return view('reportcashdaily');

          }

          public function searchreportcashdailynow(){  //รายงานเงิดสดย่อยรายวัน (ปัจจุบัน)

              $data = Input::all();
              $db = Connectdb::Databaseall();
              // echo "<pre>";
              // print_r($data);
              // exit;

              $datepicker = explode("/",trim(($data['datepicker'])));
              if(count($datepicker) > 0) {
                  $datetime = $datepicker[2] . '-' . $datepicker[1] . '-' . $datepicker[0]; //วัน - เดือน - ปี
              }

              $branch_id = $data['branch_id'];

              $sql = 'SELECT '.$db['fsctaccount'].'.insertcashrent.*
                      FROM '.$db['fsctaccount'].'.insertcashrent

                      WHERE '.$db['fsctaccount'].'.insertcashrent.datetimeinsert LIKE "%'.$datetime.'%"
                        AND '.$db['fsctaccount'].'.insertcashrent.branch = '.$branch_id.'
                        AND '.$db['fsctaccount'].'.insertcashrent.typedoc NOT IN (99 , 10 , 11 , 12 , 14 , 15, 16)
                        AND '.$db['fsctaccount'].'.insertcashrent.status NOT IN (99)
                        ';
              $datatresult = DB::connection('mysql')->select($sql);

              $sqlpo = 'SELECT '.$db['fsctaccount'].'.po_head.*,
                               '.$db['fsctaccount'].'.insertcashrent.*
                        FROM '.$db['fsctaccount'].'.po_head
                        INNER JOIN  '.$db['fsctaccount'].'.insertcashrent
                           ON '.$db['fsctaccount'].'.po_head.id = '.$db['fsctaccount'].'.insertcashrent.ref

                        WHERE '.$db['fsctaccount'].'.insertcashrent.datetimeinsert LIKE "%'.$datetime.'%"
                          AND '.$db['fsctaccount'].'.insertcashrent.branch = '.$branch_id.'
                          AND '.$db['fsctaccount'].'.insertcashrent.typedoc NOT IN (0 , 99 , 1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 , 11 , 13 , 14 , 15 , 16)
                          AND '.$db['fsctaccount'].'.insertcashrent.status NOT IN (99)
                          ';
              $datatresultpo = DB::connection('mysql')->select($sqlpo);

              $sqlmoneycash = 'SELECT '.$db['fsctaccount'].'.po_head.*,
                               '.$db['fsctaccount'].'.insertcashrent.*
                        FROM '.$db['fsctaccount'].'.po_head
                        INNER JOIN  '.$db['fsctaccount'].'.insertcashrent
                           ON '.$db['fsctaccount'].'.po_head.id = '.$db['fsctaccount'].'.insertcashrent.ref

                        WHERE '.$db['fsctaccount'].'.insertcashrent.datetimeinsert LIKE "%'.$datetime.'%"
                          AND '.$db['fsctaccount'].'.insertcashrent.branch = '.$branch_id.'
                          AND '.$db['fsctaccount'].'.insertcashrent.typedoc IN (16)
                          AND '.$db['fsctaccount'].'.insertcashrent.status NOT IN (99)
                          ';

              $datatresultmoneycash = DB::connection('mysql')->select($sqlmoneycash);

              $sqlmoney = 'SELECT '.$db['fsctaccount'].'.reservemoney.*,
                                  '.$db['fsctaccount'].'.insertcashrent.*
                           FROM '.$db['fsctaccount'].'.reservemoney
                           INNER JOIN  '.$db['fsctaccount'].'.insertcashrent
                             ON '.$db['fsctaccount'].'.reservemoney.id = '.$db['fsctaccount'].'.insertcashrent.ref

                           WHERE '.$db['fsctaccount'].'.insertcashrent.datetimeinsert LIKE "%'.$datetime.'%"
                             AND '.$db['fsctaccount'].'.insertcashrent.branch = '.$branch_id.'
                             AND '.$db['fsctaccount'].'.insertcashrent.typedoc IN (14)
                             AND '.$db['fsctaccount'].'.insertcashrent.status NOT IN (99)
                             ';
              $datatresultmoney = DB::connection('mysql')->select($sqlmoney);

              $sqlatm = 'SELECT '.$db['fsctaccount'].'.insertcashrent.*
                         FROM '.$db['fsctaccount'].'.insertcashrent
                         INNER JOIN  '.$db['fsctaccount'].'.atmcash
                            ON '.$db['fsctaccount'].'.insertcashrent.ref = '.$db['fsctaccount'].'.atmcash.id

                         WHERE '.$db['fsctaccount'].'.insertcashrent.datetimeinsert LIKE "%'.$datetime.'%"
                           AND '.$db['fsctaccount'].'.insertcashrent.branch = '.$branch_id.'
                           AND '.$db['fsctaccount'].'.insertcashrent.typedoc IN (11)
                           AND '.$db['fsctaccount'].'.insertcashrent.status NOT IN (99)
                           ';
              $datatresultatm = DB::connection('mysql')->select($sqlatm);

              $sqlexpenses = 'SELECT '.$db['fsctaccount'].'.insertcashrent.*
                         FROM '.$db['fsctaccount'].'.insertcashrent
                         INNER JOIN  '.$db['fsctaccount'].'.not_expenses
                            ON '.$db['fsctaccount'].'.insertcashrent.ref = '.$db['fsctaccount'].'.not_expenses.id

                         WHERE '.$db['fsctaccount'].'.insertcashrent.datetimeinsert LIKE "%'.$datetime.'%"
                           AND '.$db['fsctaccount'].'.insertcashrent.branch = '.$branch_id.'
                           AND '.$db['fsctaccount'].'.insertcashrent.typedoc IN (15)
                           AND '.$db['fsctaccount'].'.insertcashrent.status NOT IN (99)
                           ';
              $dataexpenses = DB::connection('mysql')->select($sqlexpenses);

              $sqlcheck = 'SELECT '.$db['fsctaccount'].'.checkbank.*
                      FROM '.$db['fsctaccount'].'.checkbank

                      WHERE '.$db['fsctaccount'].'.checkbank.time LIKE "%'.$datetime.'%"
                        AND '.$db['fsctaccount'].'.checkbank.branch_id = '.$branch_id.'
                        AND '.$db['fsctaccount'].'.checkbank.status NOT IN (99)
                        ';
              $datatresultcheck = DB::connection('mysql')->select($sqlcheck);

              // echo "<pre>";
              // print_r($datatresult);
              // print_r($datatresultpo);
              // print_r($datatresultmoney);
              // print_r($datatresultatm);
              // print_r($datatresultcheck);
              // exit;

              return view('reportcashdailynew',[
                'data'=>$datatresult,
                'dataenpo'=>$datatresultpo,
                'dataenmoney'=>$datatresultmoney,
                'dataenmoneycash'=>$datatresultmoneycash,
                'dataatm'=>$datatresultatm,
                'dataexpenses'=>$dataexpenses,
                'datacheck'=>$datatresultcheck,
                'query'=>true,
                'datepicker'=>$data['datepicker'],
                'branch_id'=>$branch_id,
                'datepicker2'=>$datetime]);
          }

          //กรณีเมื่อคลิก นำฝาก (ปัจจุบัน)
          public function configreportcashdailynow(){

                $datainsert = Input::all();
                $db = Connectdb::Databaseall();

                $checkmoney = $datainsert['grandtotal'][0];
                // echo "<pre>";
                // print_r($datainsert);


                //$data = json_decode($datainsert['datainsert']);

                $emp_id = Session::get('emp_code');

                // $range_time = Session::get('range_time');
                // $type = Session::get('type');
                // $allmonth = Session::get('allmonth');


                $date = date('Y-m-d H:i:s');
                $day = date("D", strtotime($date));

                $arrInsert = [];

                if($day=='Sat'){
                  //เบิ้ล 2 วัน

                        $arrInsert[0]['id'] = '';
                        $arrInsert[0]['grandtotal'] = $datainsert['grandtotal'][0];
                        $arrInsert[0]['branch_id'] = $datainsert['branch_id'][0];
                        $arrInsert[0]['codeemp'] = $emp_id;
                        $arrInsert[0]['time'] = date('Y-m-d',strtotime("+1 day"));
                        $arrInsert[0]['note'] = "ยอดที่นำฝาก = " .$datainsert['all'][0];
                        $arrInsert[0]['timeold'] = $datainsert['startdate'][0];
                        $arrInsert[0]['status'] = 1;

                        $arrInsert[1]['id'] = '';
                        $arrInsert[1]['grandtotal'] = $datainsert['grandtotal'][0];
                        $arrInsert[1]['branch_id'] = $datainsert['branch_id'][0];
                        $arrInsert[1]['codeemp'] = $emp_id;
                        $arrInsert[1]['time'] = date('Y-m-d',strtotime("+2 day"));
                        $arrInsert[1]['note'] = "ยอดที่นำฝาก = " .$datainsert['all'][0];
                        $arrInsert[1]['timeold'] = $datainsert['startdate'][0];
                        $arrInsert[1]['status'] = 1;

                     $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'cash')->insert($arrInsert);

                }else{

                    foreach ($datainsert['time2'] as $key => $value){
                        $arrInsert[$key]['id'] = '';
                        $arrInsert[$key]['grandtotal'] = $datainsert['grandtotal'][0];
                        $arrInsert[$key]['branch_id'] = $datainsert['branch_id'][0];
                        $arrInsert[$key]['codeemp'] = $emp_id;
                        $arrInsert[$key]['time'] = $value;
                        $arrInsert[$key]['note'] = "ยอดที่นำฝาก = " .$datainsert['all'][0];
                        $arrInsert[$key]['timeold'] = $datainsert['startdate'][0];
                        $arrInsert[$key]['status'] = 1;
                    }

                    if($arrInsert[$key]['id'] == ""){
                        $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'cash')->insert($arrInsert);

                    }

                }

                // echo "<pre>";
                // print_r($arrInsert);
                // exit;

                // foreach ($variable as $key => $value) {
                //   // code...
                // }
                // echo "<pre>";
                // print_r($arrInsert);
                // exit;

                echo "<script>";
                echo "alert('บันทึกสำเร็จกรุณาเช็คยอดยกไปของวันถัดไปด้วยนะครับ');";
                echo "</script>";

            return view('reportcashdailynew');

          }

          //กรณีเมื่อคลิก ฝากสลิป
          public function configgetcashchechbank(){

                $datainsert = Input::all();
                $db = Connectdb::Databaseall();
                // echo "<pre>";
                // print_r($datainsert);
                // exit;
                $branch_id = ($datainsert['brcode']);
                $datepicker = explode("/",trim(($datainsert['datepicker'])));
                if(count($datepicker) > 0) {
                    $datetime = $datepicker[2] . '-' . $datepicker[1] . '-' . $datepicker[0]; //วัน - เดือน - ปี
                }

                 if($_FILES['image']['error']==4){
                   $datainsert['image']='';
                 }
                 // if($_FILES['image2']['error']==4){
                 //   $datainsert['image2']='';
                 // }
                 // if($_FILES['image3']['error']==4){
                 //   $datainsert['image3']='';
                 // }
                 // if($_FILES['image4']['error']==4){
                 //   $datainsert['image4']='';
                 // }

                 // exit;
                 // DB::table($db['fsctaccount'].'.'.'checkbank')->insert(
                 // ['id' => '',
                 //  'grandtotal' => $datainsert['grandtotal'],
                 //  'grandtotal1' => $datainsert['grandtotal1'],
                 //  'grandtotal2' => $datainsert['grandtotal2'],
                 //  'grandtotal3' => $datainsert['grandtotal3'],
                 //  'image' => $datainsert['image1'],
                 //  'image1' => $datainsert['image2'],
                 //  'image2' =>  $datainsert['image3'],
                 //  'image3' => $datainsert['image4'],
                 //  'total' => $datainsert['total'],
                 //  'branch_id' => $datainsert['brcode'],
                 //  'codeemp'=>$datainsert['empcode'],
                 //  'time'=>$datetime,
                 //  'status' => 1,
                 // ]);

                try{
                  if(Input::hasFile('image')){

                    // echo Input::file('image4'); exit;

                    // echo "<pre>";
                    // print_r($datainsert);
                    // exit;
                    // echo var_dump(Input::file('image'));
                    // exit;

                    // $file = Input::file('image');
                    // $file = Input::file('image1');
                    // $file = Input::file('image2');
                    // $file = Input::file('image3');
                    //
                    for($i=0;$i<count($_FILES['image']['name']);$i++){
                      if($_FILES['image']['name'][$i] != ''){
                          move_uploaded_file($_FILES['image']['tmp_name'][$i],"checkcash/".$_FILES['image']['name'][$i]);
                      }
                    }
                    // $name2 = Input::file('image2')->getClientOriginalName();
                    // $name3 = Input::file('image3')->getClientOriginalName();
                    // $name4 = Input::file('image4')->getClientOriginalName();

                    // $fullname = array($name1,$name2,$name3,$name4);
                    // print_r($fullname); exit;

                    // $file->move('checkcash', $name);
                    // $file->move('checkcash', $name1);
                    // $file->move('checkcash', $name2);
                    // $file->move('checkcash', $name3);

                    // echo "<pre>";
                    // print_r($image);
                    // exit;

                        DB::table($db['fsctaccount'].'.'.'checkbank')->insert(
                        ['id' => '',
                         'grandtotal' => $datainsert['grandtotal'],
                         'grandtotal1' => $datainsert['grandtotal1'],
                         'grandtotal2' => $datainsert['grandtotal2'],
                         'grandtotal3' => $datainsert['grandtotal3'],
                         'image' => $_FILES['image']['name'][0],
                         'image1' => $_FILES['image']['name'][1],
                         'image2' => $_FILES['image']['name'][2],
                         'image3' => $_FILES['image']['name'][3],
                         'total' => $datainsert['total'],
                         'branch_id' => $datainsert['brcode'],
                         'codeemp'=>$datainsert['empcode'],
                         'bill_no'=>$datainsert['bill_no'],
                         'bill_no2'=>$datainsert['bill_no1'],
                         'bill_no3'=>$datainsert['bill_no2'],
                         'bill_no4'=>$datainsert['bill_no3'],
                         'time'=>$datetime,
                         'status' => 1,
                        ]);

                  }

                  }catch (\Exception $e) {
                      DB::rollback();
                      throw new \Exception($e);
                  }

            return view('cashcheckbank',[
              'query'=>true,
              'branch_id'=>$branch_id,
              'datepicker2'=>$datetime,
              'datepicker'=>$datetime]);

            }

            //กรณีเมื่อ ออดิททำการตรวจเช็ค
            public function configgetcashchech(){

              $datainsert = Input::all();
              $db = Connectdb::Databaseall();
              // echo "<pre>";
              // print_r($datainsert);
              // exit;

              $emp_id = Session::get('emp_code');
              // $branch_id = ($datainsert['brcode']);

              // $datepicker = explode("/",trim(($datainsert['datepicker'])));

              // if(count($datepicker) > 0) {
              //     $datetime = $datepicker[2] . '-' . $datepicker[1] . '-' . $datepicker[0]; //วัน - เดือน - ปี
              // }
              $arrInsert = [];
              // foreach ($datainsert['brcode'] as $key => $value){
                $arrInsert['id'] = '';
                $arrInsert['totalbank'] = $datainsert['sumgrand'];
                $arrInsert['totalbook'] = $datainsert['grandtotal'];
                $arrInsert['point'] = $datainsert['point'];
                $arrInsert['branch_id'] = $datainsert['brcode'];
                $arrInsert['time'] = $datainsert['datepicker'];
                $arrInsert['codeemp'] = $emp_id;
                $arrInsert['status'] = 1;
              // }
              // echo "<pre>";
              // print_r($arrInsert);
              // exit();

              if($arrInsert['id'] == ""){
                  $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'checkcash')->insert($arrInsert);
              }

              return view('cashcheckbank');

          }

          public function searchcashcheckbank(){  //ตรวจเช็คเงินสดย่อยรายวัน

              $datainsert = Input::all();
              $db = Connectdb::Databaseall();
              // echo "<pre>";
              // print_r($datainsert);
              // exit;

              $datepicker = explode("/",trim(($datainsert['datepicker'])));
              if(count($datepicker) > 0) {
                  $datetime = $datepicker[2] . '-' . $datepicker[1] . '-' . $datepicker[0]; //วัน - เดือน - ปี
              }

              $branch_id = $datainsert['brcode'];

              $sql = 'SELECT '.$db['fsctaccount'].'.cash.*
                  FROM '.$db['fsctaccount'].'.cash

                  WHERE '.$db['fsctaccount'].'.cash.timeold LIKE "%'.$datetime.'%"
                    AND '.$db['fsctaccount'].'.cash.branch_id = '.$branch_id.'
                    AND '.$db['fsctaccount'].'.cash.status NOT IN (99)
                    ';
              $datatresult = DB::connection('mysql')->select($sql);

              $sqlcheck = 'SELECT '.$db['fsctaccount'].'.checkbank.*
                  FROM '.$db['fsctaccount'].'.checkbank

                  WHERE '.$db['fsctaccount'].'.checkbank.time LIKE "%'.$datetime.'%"
                    AND '.$db['fsctaccount'].'.checkbank.branch_id = '.$branch_id.'
                    AND '.$db['fsctaccount'].'.checkbank.status NOT IN (99)
                    ';
              $datatresultcheck = DB::connection('mysql')->select($sqlcheck);
              // echo "<pre>";
              // print_r($datatresult);
              // exit;

              return view('cashcheckbank',[
                'data'=>$datatresult,
                'datacheck'=>$datatresultcheck,
                'query'=>true,
                'datepicker'=>$datetime,
                'branch_id'=>$branch_id,
                'datepicker2'=>$datetime]);

        }

        public function reportpayinandsalary(){

        }


        public function searchgetdatacashedit(){ //แก้ไขเงินตั้งต้นรายวัน

          $data= Input::all();
          $db = Connectdb::Databaseall();
          // echo "<pre>";
          // print_r($data);
          // exit;

          $date = $data['datepicker'];

          $datepicker = explode("/",trim(($data['datepicker'])));
          if(count($datepicker) > 0) {
              $datetime = $datepicker[2] . '-' . $datepicker[1] . '-' . $datepicker[0]; //วัน - เดือน - ปี
          }

          $branch_id = $data['branch_id'];

          // $bill_rent = $data['billedit'];
          $sql ='SELECT '.$db['fsctaccount'].'.cash.*
                 FROM '.$db['fsctaccount'].'.cash
                 WHERE '.$db['fsctaccount'].'.cash.branch_id = "'.$branch_id.'"
                 AND '.$db['fsctaccount'].'.cash.time = "'.$datetime.'"
                 ';

          $datadetail = DB::connection('mysql')->select($sql);
          // echo "<pre>";
          // print_r($datadetail);
          // exit;

          return view('getdatacashedit',[
            'query'=>true,
            'data'=>$datadetail,
            'branch_id'=>$branch_id,
            'datepicker'=>$date
          ]);

        }


        public function saveeditgetdatacashedit(){ //แก้ไขเงินตั้งต้นรายวัน

          $data= Input::all();
          $db = Connectdb::Databaseall();
          // echo "<pre>";
          // print_r($data);
          // exit;

          $dbac = ($db['fsctaccount']);

          $id = $data['id'][0];
          $branch_id = $data['branch_id'][0];
          $grandtotal = $data['grandtotal'][0];
          $time = $data['time'][0];
          $timeold = $data['timeold'][0];
          $note =  $data['note'][0];

          $sqlupdater = 'UPDATE '.$dbac.'.cash SET grandtotal = '.$grandtotal.',
                                                         note = "'.$note.'"
                                               WHERE id = '.$id;

          $model = DB::connection('mysql')->update($sqlupdater);
          // echo "<pre>";
          // print_r($model);
          // exit;

          $sql ='SELECT '.$db['fsctaccount'].'.cash.*
                 FROM '.$db['fsctaccount'].'.cash
                 WHERE '.$db['fsctaccount'].'.cash.branch_id = "'.$branch_id.'"
                 AND '.$db['fsctaccount'].'.cash.time LIKE "%'.$time.'%"
                 ';

          $datadetail = DB::connection('mysql')->select($sql);

          return view('getdatacashedit',['query'=>true,'data'=>$datadetail,'branch_id'=>$branch_id,'datepicker'=>$time]);

        }

        public function insertdatacash(){

            $data = Input::all();
            $db = Connectdb::Databaseall();
            // echo "<pre>";
            // print_r($data);
            // exit;

            $brcode = Session::get('brcode');
            $emp_code = Session::get('emp_code');
            $arrInsert = ['id'=>'',
                          'datetime'=>date('Y-m-d H:i:s'),
                          'list'=>$data['data']['list'],
                          'amount'=>$data['data']['amount'],
                          'vat'=>$data['data']['vat'],
                          'vat_money'=>$data['data']['vat_money'],
                          'total'=>$data['data']['total'],
                          'po_ref'=>$data['data']['po_ref'],
                          'note'=>$data['data']['note'],
                          'id_compay'=>$data['data']['id_compay'],
                          'branch'=>$brcode,
                          'code_emp'=>$emp_code
                        ];

          $model = DB::connection('mysql')->table($db['fsctaccount'].'.reservemoney')->insertGetId($arrInsert);

          return $model;

        }

        public function savecash(){ //เงินตั้งต้นรายวัน

            $data= Input::all();
            // echo "<pre>";
            // print_r($data);
            // exit;

            $grandtotal = $data['grandtotal'];
            $branch_id = $data['branch_id'];
            $note = $data['note'];

            $datepicker = explode("/",trim(($data['datepicker'])));
            if(count($datepicker) > 0) {
                $datetimenow = $datepicker[2] . '-' . $datepicker[1] . '-' . $datepicker[0]; //วัน - เดือน - ปี
            }

            $datetime = explode("/",trim(($data['timeold'])));
            if(count($datetime) > 0) {
                $datetimeold = $datetime[2] . '-' . $datetime[1] . '-' . $datetime[0]; //วัน - เดือน - ปี
            }

            $emp_code = Session::get('emp_code');
            $db = Connectdb::Databaseall();

            $arrInsert = ['id'=>'',
                          'grandtotal'=>$grandtotal,
                          'branch_id'=>$branch_id,
                          'codeemp'=>$emp_code,
                          'time'=>$datetimenow,
                          'note'=>"ยอดที่นำฝาก = ".$note,
                          'timeold'=>$datetimeold,
                          'status'=>1];
           // echo "<pre>";
           // print_r($arrInsert);
           // exit;

           $tranfermodel = DB::connection('mysql')->table($db['fsctaccount'].'.cash')->insertGetId($arrInsert);
           // echo "<pre>";
           // print_r($tranfermodel);
           // exit;

        return view('getdatacashedit');

        }

        public function boxcover(){
            return view('boxcover');
        }


        public function getboxcover(){

            $data= Input::all();
            // echo "<pre>";
            // print_r($data);
            // exit;
            $datepicker = $data['datepicker'];
            $dateranger = Datetime::FormatDateFromCalendarRange($datepicker);
            $start_date = $dateranger['start_date'];
            $end_date = $dateranger['end_date'];
            $brcode = Session::get('brcode');
            Session::put('datepickerboxcover',$datepicker);

            $db = Connectdb::Databaseall();
            // echo "<pre>";
            // print_r($data);
            // exit;
            $dbac = $db['fsctaccount'];
            $sql ='SELECT '.$db['fsctaccount'].'.po_head.*
                   FROM '.$db['fsctaccount'].'.po_head
                   WHERE '.$db['fsctaccount'].'.po_head.date BETWEEN  "'.$start_date.'" AND "'.$end_date.'"
                   AND '.$db['fsctaccount'].'.po_head.status_head IN (2,3,4,5)
                   AND '.$db['fsctaccount'].'.po_head.branch_id =  "'.$brcode.'"';

            $dataquery = DB::connection('mysql')->select($sql);

            return view('boxcover',['query'=>true,'data'=>$dataquery]);
        }


        public function serachreporttaxbuy(){ //รายงานภาษีซื้อ

          $data = Input::all();
          $db = Connectdb::Databaseall();
          // echo "<pre>";
          // print_r($data);
          // exit;

          $datepicker = explode("-",trim(($data['datepicker'])));

          // $start_date = $datepicker[0];
          $e1 = explode("/",trim(($datepicker[0])));
                  if(count($e1) > 0) {
                      $start_date = $e1[2] . '-' . $e1[0] . '-' . $e1[1]; //ปี - เดือน - วัน
                      $start_date2 = $start_date." 00:00:00";
                  }

          // $end_date = $datepicker[1];
          $e2 = explode("/",trim(($datepicker[1])));
                  if(count($e2) > 0) {
                      $end_date = $e2[2] . '-' . $e2[0] . '-' . $e2[1]; //ปี - เดือน - วัน
                      $end_date2 = $end_date." 23:59:59";
                  }

          $branch_id = $data['branch'];

          // echo "<pre>";
          // print_r($start_date);
          // print_r($end_date);
          // exit;

          $sql = 'SELECT '.$db['fsctaccount'].'.inform_po.*,
                         '.$db['fsctaccount'].'.po_head.branch_id

                 FROM '.$db['fsctaccount'].'.inform_po
                 INNER JOIN  '.$db['fsctaccount'].'.po_head
                    ON '.$db['fsctaccount'].'.po_head.id = '.$db['fsctaccount'].'.inform_po.id_po

                  WHERE '.$db['fsctaccount'].'.po_head.branch_id = "'.$branch_id.'"
                    AND '.$db['fsctaccount'].'.inform_po.datetime  BETWEEN "'.$start_date.'" AND  "'.$end_date.'"
                    AND '.$db['fsctaccount'].'.inform_po.status NOT IN (99)
                    AND '.$db['fsctaccount'].'.inform_po.vat_percent IN (7)
                    ORDER BY '.$db['fsctaccount'].'.inform_po.datebillreceipt
                 ';

          $datatresult = DB::connection('mysql')->select($sql);
          // echo "<pre>";
          // print_r($datatresult);
          // exit;

          $sqlreserve = 'SELECT '.$db['fsctaccount'].'.reservemoney.*
                         FROM '.$db['fsctaccount'].'.reservemoney

                         WHERE '.$db['fsctaccount'].'.reservemoney.branch = "'.$branch_id.'"
                          AND '.$db['fsctaccount'].'.reservemoney.dateporef  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                          AND '.$db['fsctaccount'].'.reservemoney.status IN (0 , 1 , 2)
                          AND '.$db['fsctaccount'].'.reservemoney.vat >= 1
                          AND '.$db['fsctaccount'].'.reservemoney.po_ref != 0
                          ORDER BY '.$db['fsctaccount'].'.reservemoney.date_bill_no
                         ';

          $datatresultreserve = DB::connection('mysql')->select($sqlreserve);
          // echo "<pre>";
          // print_r($datatresultreserve);
          // exit;

          return view('reporttaxbuy',[
            'data'=>true,
            'datareserve'=>$datatresultreserve,
            'datatresult'=>$datatresult,
            'datepicker'=>$data['datepicker'],
            'branch'=>$branch_id
            // 'datepicker2'=>$datetime
          ]);

      }

      public function serachreportexpenses(){ //รายงานค่าใช้จ่าย

        $data = Input::all();
        $db = Connectdb::Databaseall();
        // echo "<pre>";
        // print_r($data);
        // exit;

        $datepicker = explode("-",trim(($data['datepicker'])));

        // $start_date = $datepicker[0];
        $e1 = explode("/",trim(($datepicker[0])));
                if(count($e1) > 0) {
                    $start_date = $e1[2] . '-' . $e1[0] . '-' . $e1[1]; //ปี - เดือน - วัน
                    $start_date2 = $start_date." 00:00:00";
                }

        // $end_date = $datepicker[1];
        $e2 = explode("/",trim(($datepicker[1])));
                if(count($e2) > 0) {
                    $end_date = $e2[2] . '-' . $e2[0] . '-' . $e2[1]; //ปี - เดือน - วัน
                    $end_date2 = $end_date." 23:59:59";
                }

        $branch_id = $data['branch'];

        // echo "<pre>";
        // print_r($start_date);
        // print_r($end_date);
        // exit;

        $sql = 'SELECT '.$db['fsctaccount'].'.inform_po.*,
                       '.$db['fsctaccount'].'.po_head.branch_id

               FROM '.$db['fsctaccount'].'.inform_po
               INNER JOIN  '.$db['fsctaccount'].'.po_head
                  ON '.$db['fsctaccount'].'.po_head.id = '.$db['fsctaccount'].'.inform_po.id_po

                WHERE '.$db['fsctaccount'].'.po_head.branch_id = "'.$branch_id.'"
                  AND '.$db['fsctaccount'].'.inform_po.datetime  BETWEEN "'.$start_date.'" AND  "'.$end_date.'"
                  AND '.$db['fsctaccount'].'.inform_po.status NOT IN (99)
                  ORDER BY '.$db['fsctaccount'].'.inform_po.datebill
               ';

        $datatresult = DB::connection('mysql')->select($sql);
        // echo "<pre>";
        // print_r($datatresult);
        // exit;

        $sqlreserve = 'SELECT '.$db['fsctaccount'].'.reservemoney.*
                       FROM '.$db['fsctaccount'].'.reservemoney

                       WHERE '.$db['fsctaccount'].'.reservemoney.branch = "'.$branch_id.'"
                        AND '.$db['fsctaccount'].'.reservemoney.dateporef BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                        AND '.$db['fsctaccount'].'.reservemoney.status IN (0 , 1 , 2)
                        AND '.$db['fsctaccount'].'.reservemoney.po_ref != 0
                        ORDER BY '.$db['fsctaccount'].'.reservemoney.date_bill_no
                       ';

        $datatresultreserve = DB::connection('mysql')->select($sqlreserve);
        // echo "<pre>";
        // print_r($datatresultreserve);
        // exit;

        return view('reportexpenses',[
          'data'=>true,
          'datareserve'=>$datatresultreserve,
          'datatresult'=>$datatresult,
          'datepicker'=>$data['datepicker'],
          'branch'=>$branch_id
          // 'datepicker2'=>$datetime
        ]);

    }

    public function serachreporttaxsell(){ //รายงานภาษีขาย

      $data = Input::all();
      $db = Connectdb::Databaseall();
      // echo "<pre>";
      // print_r($data);
      // exit;

      $datepicker = explode("-",trim(($data['datepicker'])));

      // $start_date = $datepicker[0];
      $e1 = explode("/",trim(($datepicker[0])));
              if(count($e1) > 0) {
                  $start_date = $e1[2] . '-' . $e1[0] . '-' . $e1[1]; //ปี - เดือน - วัน
                  $start_date2 = $start_date." 00:00:00";
              }

      // $end_date = $datepicker[1];
      $e2 = explode("/",trim(($datepicker[1])));
              if(count($e2) > 0) {
                  $end_date = $e2[2] . '-' . $e2[0] . '-' . $e2[1]; //ปี - เดือน - วัน
                  $end_date2 = $end_date." 23:59:59";
              }

      $branch_id = $data['branch'];

      // echo "<pre>";
      // print_r($start_date);
      // print_r($end_date);
      // exit;

      $sqlra = 'SELECT '.$db['fsctaccount'].'.taxinvoice_abb.*

             FROM '.$db['fsctaccount'].'.taxinvoice_abb

              WHERE '.$db['fsctaccount'].'.taxinvoice_abb.branch_id = "'.$branch_id.'"
                AND '.$db['fsctaccount'].'.taxinvoice_abb.time BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                AND '.$db['fsctaccount'].'.taxinvoice_abb.status IN (1,98)
                ORDER BY '.$db['fsctaccount'].'.taxinvoice_abb.number_taxinvoice ASC
             ';

      $datataxra = DB::connection('mysql')->select($sqlra);


      $sqltf = 'SELECT '.$db['fsctaccount'].'.taxinvoice.*

             FROM '.$db['fsctaccount'].'.taxinvoice

              WHERE '.$db['fsctaccount'].'.taxinvoice.branch_id = "'.$branch_id.'"
                AND '.$db['fsctaccount'].'.taxinvoice.time  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                AND '.$db['fsctaccount'].'.taxinvoice.status = 1
                ORDER BY '.$db['fsctaccount'].'.taxinvoice.number_taxinvoice ASC
             ';

      $datataxtf = DB::connection('mysql')->select($sqltf);


      $sqlcn = 'SELECT '.$db['fsctaccount'].'.taxinvoice_creditnote.*

             FROM '.$db['fsctaccount'].'.taxinvoice_creditnote

              WHERE '.$db['fsctaccount'].'.taxinvoice_creditnote.branch_id = "'.$branch_id.'"
                AND '.$db['fsctaccount'].'.taxinvoice_creditnote.time  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                AND '.$db['fsctaccount'].'.taxinvoice_creditnote.status = 1
                ORDER BY '.$db['fsctaccount'].'.taxinvoice_creditnote.number_taxinvoice ASC
             ';

      $datataxcn = DB::connection('mysql')->select($sqlcn);


      $sqlcs = 'SELECT '.$db['fsctaccount'].'.taxinvoice_creditnote_special_abb.*

             FROM '.$db['fsctaccount'].'.taxinvoice_creditnote_special_abb

              WHERE '.$db['fsctaccount'].'.taxinvoice_creditnote_special_abb.branch_id = "'.$branch_id.'"
                AND '.$db['fsctaccount'].'.taxinvoice_creditnote_special_abb.date_approved  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                AND '.$db['fsctaccount'].'.taxinvoice_creditnote_special_abb.status = 1
                AND '.$db['fsctaccount'].'.taxinvoice_creditnote_special_abb.status = 1
                ORDER BY '.$db['fsctaccount'].'.taxinvoice_creditnote_special_abb.number_taxinvoice ASC
             ';

      $datataxcs = DB::connection('mysql')->select($sqlcs);


      $sqltk = 'SELECT '.$db['fsctaccount'].'.taxinvoice_loss.*

             FROM '.$db['fsctaccount'].'.taxinvoice_loss

              WHERE '.$db['fsctaccount'].'.taxinvoice_loss.branch_id = "'.$branch_id.'"
                AND '.$db['fsctaccount'].'.taxinvoice_loss.time  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                AND '.$db['fsctaccount'].'.taxinvoice_loss.status = 1
                ORDER BY '.$db['fsctaccount'].'.taxinvoice_loss.number_taxinvoice ASC
             ';

      $datataxtk = DB::connection('mysql')->select($sqltk);


      $sqlrl = 'SELECT '.$db['fsctaccount'].'.taxinvoice_loss_abb.*

             FROM '.$db['fsctaccount'].'.taxinvoice_loss_abb

              WHERE '.$db['fsctaccount'].'.taxinvoice_loss_abb.branch_id = "'.$branch_id.'"
                AND '.$db['fsctaccount'].'.taxinvoice_loss_abb.time  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                AND '.$db['fsctaccount'].'.taxinvoice_loss_abb.status IN (1,98)
                ORDER BY '.$db['fsctaccount'].'.taxinvoice_loss_abb.number_taxinvoice ASC
             ';

      $datataxrl = DB::connection('mysql')->select($sqlrl);


      $sqltm = 'SELECT '.$db['fsctaccount'].'.taxinvoice_more.*

             FROM '.$db['fsctaccount'].'.taxinvoice_more

              WHERE '.$db['fsctaccount'].'.taxinvoice_more.branch_id = "'.$branch_id.'"
                AND '.$db['fsctaccount'].'.taxinvoice_more.time  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                AND '.$db['fsctaccount'].'.taxinvoice_more.status = 1
                ORDER BY '.$db['fsctaccount'].'.taxinvoice_more.number_taxinvoice ASC
             ';

      $datataxtm = DB::connection('mysql')->select($sqltm);


      $sqlrn = 'SELECT '.$db['fsctaccount'].'.taxinvoice_more_abb.*

             FROM '.$db['fsctaccount'].'.taxinvoice_more_abb

              WHERE '.$db['fsctaccount'].'.taxinvoice_more_abb.branch_id = "'.$branch_id.'"
                AND '.$db['fsctaccount'].'.taxinvoice_more_abb.time  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                AND '.$db['fsctaccount'].'.taxinvoice_more_abb.status IN (1,98)
                ORDER BY '.$db['fsctaccount'].'.taxinvoice_more_abb.number_taxinvoice ASC
             ';

      $datataxrn = DB::connection('mysql')->select($sqlrn);


      $sqltp = 'SELECT '.$db['fsctaccount'].'.taxinvoice_partial.*

             FROM '.$db['fsctaccount'].'.taxinvoice_partial

              WHERE '.$db['fsctaccount'].'.taxinvoice_partial.branch_id = "'.$branch_id.'"
                AND '.$db['fsctaccount'].'.taxinvoice_partial.time  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                AND '.$db['fsctaccount'].'.taxinvoice_partial.status = 1
                ORDER BY '.$db['fsctaccount'].'.taxinvoice_partial.number_taxinvoice ASC
             ';

      $datataxtp = DB::connection('mysql')->select($sqltp);


      $sqlro = 'SELECT '.$db['fsctaccount'].'.taxinvoice_partial_abb.*

             FROM '.$db['fsctaccount'].'.taxinvoice_partial_abb

              WHERE '.$db['fsctaccount'].'.taxinvoice_partial_abb.branch_id = "'.$branch_id.'"
                AND '.$db['fsctaccount'].'.taxinvoice_partial_abb.time  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                AND '.$db['fsctaccount'].'.taxinvoice_partial_abb.status IN (1,98)
                ORDER BY '.$db['fsctaccount'].'.taxinvoice_partial_abb.number_taxinvoice ASC
             ';

      $datataxro = DB::connection('mysql')->select($sqlro);


      $sqlrs = 'SELECT '.$db['fsctaccount'].'.taxinvoice_special_abb.*

             FROM '.$db['fsctaccount'].'.taxinvoice_special_abb

              WHERE '.$db['fsctaccount'].'.taxinvoice_special_abb.branch_id = "'.$branch_id.'"
                AND '.$db['fsctaccount'].'.taxinvoice_special_abb.date_approved  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                AND '.$db['fsctaccount'].'.taxinvoice_special_abb.status = 1
                ORDER BY '.$db['fsctaccount'].'.taxinvoice_special_abb.number_taxinvoice ASC
             ';

      $datataxrs = DB::connection('mysql')->select($sqlrs);


      $sqlss = 'SELECT '.$db['fsctmain'].'.stock_sell_head.*

             FROM '.$db['fsctmain'].'.stock_sell_head

              WHERE '.$db['fsctmain'].'.stock_sell_head.branch_id = "'.$branch_id.'"
                AND '.$db['fsctmain'].'.stock_sell_head.date_approved  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                AND '.$db['fsctmain'].'.stock_sell_head.status = 1
                ORDER BY '.$db['fsctmain'].'.stock_sell_head.bill_no ASC
             ';

      $datataxss = DB::connection('mysql')->select($sqlss);


      // echo "<pre>";
      // print_r($datataxra);
      // exit;

      return view('reporttaxsell',[
        'data'=>true,
        'datataxra'=>$datataxra,
        'datataxtf'=>$datataxtf,
        'datataxcn'=>$datataxcn,
        'datataxcs'=>$datataxcs,
        'datataxtk'=>$datataxtk,
        'datataxrl'=>$datataxrl,
        'datataxtm'=>$datataxtm,
        'datataxrn'=>$datataxrn,
        'datataxtp'=>$datataxtp,
        'datataxro'=>$datataxro,
        'datataxrs'=>$datataxrs,
        'datataxss'=>$datataxss,
        'datepicker'=>$data['datepicker'],
        'branch'=>$branch_id
        // 'datepicker2'=>$datetime
      ]);

      }


      public function serachreporttaxwht(){ //รายงานภาษีหัก ณ ที่จ่าย

        $data = Input::all();
        $db = Connectdb::Databaseall();
        // echo "<pre>";
        // print_r($data);
        // exit;

        $datepicker = explode("-",trim(($data['datepicker'])));

        // $start_date = $datepicker[0];
        $e1 = explode("/",trim(($datepicker[0])));
                if(count($e1) > 0) {
                    $start_date = $e1[2] . '-' . $e1[0] . '-' . $e1[1]; //ปี - เดือน - วัน
                    $start_date2 = $start_date." 00:00:00";
                }

        // $end_date = $datepicker[1];
        $e2 = explode("/",trim(($datepicker[1])));
                if(count($e2) > 0) {
                    $end_date = $e2[2] . '-' . $e2[0] . '-' . $e2[1]; //ปี - เดือน - วัน
                    $end_date2 = $end_date." 23:59:59";
                }


        // echo "<pre>";
        // print_r($start_date);
        // print_r($end_date);
        // exit;

        $branch_id = $data['branch'];

        $sql = 'SELECT '.$db['fsctaccount'].'.withholdtaxpo.*,
                       '.$db['fsctaccount'].'.po_head.supplier_id,
                       '.$db['fsctaccount'].'.supplier.pre,name_supplier

               FROM '.$db['fsctaccount'].'.withholdtaxpo
               INNER JOIN  '.$db['fsctaccount'].'.po_head
                  ON '.$db['fsctaccount'].'.po_head.id = '.$db['fsctaccount'].'.withholdtaxpo.id_po

               INNER JOIN  '.$db['fsctaccount'].'.supplier
                  ON '.$db['fsctaccount'].'.supplier.id = '.$db['fsctaccount'].'.po_head.supplier_id

                WHERE '.$db['fsctaccount'].'.withholdtaxpo.date  BETWEEN "'.$start_date.'" AND  "'.$end_date.'"
                  AND '.$db['fsctaccount'].'.withholdtaxpo.branch = "'.$branch_id.'"
                  AND '.$db['fsctaccount'].'.withholdtaxpo.status != 99
                  ORDER BY '.$db['fsctaccount'].'.withholdtaxpo.date
               ';

        $datatresult = DB::connection('mysql')->select($sql);
        // echo "<pre>";
        // print_r($datatresult);
        // exit;

        return view('reporttaxwht',[
          'data'=>true,
          'datatresult'=>$datatresult,
          'datepicker'=>$data['datepicker'],
          'branch'=>$branch_id
          // 'datepicker2'=>$datetime
        ]);

      }

      public function serachreporttaxsellinsurance(){ //รายงานภาษีขาย (เงินประกัน)

        $data = Input::all();
        $db = Connectdb::Databaseall();
        // echo "<pre>";
        // print_r($data);
        // exit;

        $datepicker = explode("-",trim(($data['datepicker'])));

        // $start_date = $datepicker[0];
        $e1 = explode("/",trim(($datepicker[0])));
                if(count($e1) > 0) {
                    $start_date = $e1[2] . '-' . $e1[0] . '-' . $e1[1]; //ปี - เดือน - วัน
                    $start_date2 = $start_date." 00:00:00";
                }

        // $end_date = $datepicker[1];
        $e2 = explode("/",trim(($datepicker[1])));
                if(count($e2) > 0) {
                    $end_date = $e2[2] . '-' . $e2[0] . '-' . $e2[1]; //ปี - เดือน - วัน
                    $end_date2 = $end_date." 23:59:59";
                }

        $branch_id = $data['branch'];

        // echo "<pre>";
        // print_r($start_date);
        // print_r($end_date);
        // exit;

        $sqlinsurance = 'SELECT '.$db['fsctaccount'].'.taxinvoice_insurance.*

               FROM '.$db['fsctaccount'].'.taxinvoice_insurance

                WHERE '.$db['fsctaccount'].'.taxinvoice_insurance.branch_id = "'.$branch_id.'"
                  AND '.$db['fsctaccount'].'.taxinvoice_insurance.time BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                  AND '.$db['fsctaccount'].'.taxinvoice_insurance.status = 1
                  ORDER BY '.$db['fsctaccount'].'.taxinvoice_insurance.number_taxinvoice ASC
               ';

        $datataxinsurance = DB::connection('mysql')->select($sqlinsurance);


        $sqlinsurancecn = 'SELECT '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.*

               FROM '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote

                WHERE '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.branch_id = "'.$branch_id.'"
                  AND '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.time  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                  AND '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.status = 1
                  ORDER BY '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.number_taxinvoice ASC
               ';

        $datataxinsurancecn = DB::connection('mysql')->select($sqlinsurancecn);
        // echo "<pre>";
        // print_r($datataxra);
        // exit;

        return view('reporttaxsellinsurance',[
          'data'=>true,
          'datataxinsurance'=>$datataxinsurance,
          'datataxinsurancecn'=>$datataxinsurancecn,
          'datepicker'=>$data['datepicker'],
          'branch'=>$branch_id
          // 'datepicker2'=>$datetime
        ]);

        }









































}
