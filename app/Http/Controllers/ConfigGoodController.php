<?php
/**
 * Created by Notepad++.
 * User: Phathorn
 * Date: 01/27/2018 AD
 * Time: 1.30 PM
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Session;
use App\Api\Connectdb;
use App\working_company;
use DB;
use Illuminate\Support\Facades\Input;



class ConfigGoodController extends Controller
{
	
	
	
	
    public function __construct()
    {

    }

	
	
	//Good GROUP
	
	    public function configgoodgroup(){

        return view('configgoodgroup');
    }
	
	    public function getdataconfiggoodgroup(){
        $data = Input::all();

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].group_good.* 
                     FROM $db[fsctaccount].group_good
                     WHERE $db[fsctaccount].group_good.id = '$data[id]'";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;

    }
	
	    public function configgoodgroupinsertandupdate(){
        $datainsert = Input::all();
        $db = Connectdb::Databaseall();
        $data = json_decode($datainsert['data']);
		$empcode = Session::get('emp_code');
		$brcode = Session::get('brcode');

        $arrInsert = [];
        foreach ($data as $v){
            $arrInsert [$v->name] = $v->value;
        }
        if($arrInsert['id'] == ""){
            $id = DB::connection('mysql')->table($db['fsctaccount'].'.'.'group_good')->insertGetId($arrInsert);

			$model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'log_group_good')->insert(
			[
			'id' => $id,
			'alphabet' => $arrInsert['alphabet'],
			'name_group' => $arrInsert['name_group'],
			'status' => $arrInsert['status'],
			'change_type' =>1,
			'emp_id' => $empcode
			]
			);
        }else{
					$idd= $arrInsert['id'];
			        $sql = "SELECT $db[fsctaccount].group_good.* 
					FROM $db[fsctaccount].group_good 
					WHERE $db[fsctaccount].group_good.id = '$idd'";
					$oldarray = DB::connection('mysql')->select($sql);
			$model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'log_group_good')->insert(
			[
			'id' => $arrInsert['id'],
			'alphabet' => $arrInsert['alphabet'],
			'name_group' => $arrInsert['name_group'],
			'status' => $arrInsert['status'],
			'old_alphabet' => $oldarray[0]->alphabet,
			'old_name' => $oldarray[0]->name_group,
			'old_status' => $oldarray[0]->status,
			'change_type' =>2,
			'emp_id' => $empcode
			]
			);
			
			
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'group_good')->where('id',$arrInsert['id'])->update($arrInsert);
			
			
			
			
			
        }

        return 1;
    }
	
	
		//Good type
	
	    public function configgoodtype(){

        return view('configgoodtype');
    }
	
	    public function getdataconfiggoodtype(){
        $data = Input::all();

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].type_good.* 
                     FROM $db[fsctaccount].type_good
                     WHERE $db[fsctaccount].type_good.id = '$data[id]'";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;

    }
	
	    public function configgoodtypeinsertandupdate(){
        $datainsert = Input::all();
        $db = Connectdb::Databaseall();
        $data = json_decode($datainsert['data']);
				$empcode = Session::get('emp_code');
		$brcode = Session::get('brcode');

        $arrInsert = [];
        foreach ($data as $v){
            $arrInsert [$v->name] = $v->value;			
        }
        if($arrInsert['id'] == ""){
            $id = DB::connection('mysql')->table($db['fsctaccount'].'.'.'type_good')->insertGetId($arrInsert);

			
				$model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'log_type_good')->insert(
			[
			'id' => $id,
			'alphabet' => $arrInsert['alphabet'],
			'name_type' => $arrInsert['name_type'],
			'group_id' => $arrInsert['group_id'],
			'status' => $arrInsert['status'],
			'change_type' =>1,
			'emp_id' => $empcode
			]
			);
			
			
        }else{
			
							$idd= $arrInsert['id'];
			        $sql = "SELECT $db[fsctaccount].type_good.* 
					FROM $db[fsctaccount].type_good 
					WHERE $db[fsctaccount].type_good.id = '$idd'";
					$oldarray = DB::connection('mysql')->select($sql);
			
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'type_good')->where('id',$arrInsert['id'])->update($arrInsert);
        

			$model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'log_type_good')->insert(
			[
			'id' => $arrInsert['id'],
			'alphabet' => $arrInsert['alphabet'],
			'name_type' => $arrInsert['name_type'],
			'group_id' => $arrInsert['group_id'],
			'status' => $arrInsert['status'],
			'old_alphabet' => $oldarray[0]->alphabet,
			'old_name' => $oldarray[0]->name_type,
			'old_group_id' =>  $oldarray[0]->group_id,
			'old_status' => $oldarray[0]->status,
			'change_type' =>2,
			'emp_id' => $empcode
			]
			);
		
		
		
		}

        return 1;
    }
	
			//Good
	
	    public function configgood(){

        return view('configgood');
    }
	
	    public function getdataconfiggood(){
        $data = Input::all();

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].good.* 
                     FROM $db[fsctaccount].good
                     WHERE $db[fsctaccount].good.id = '$data[id]'";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;

    }
	
	    public function configgoodinsertandupdate(){
        $datainsert = Input::all();
        $db = Connectdb::Databaseall();
        $data = json_decode($datainsert['data']);
		$empcode = Session::get('emp_code');
		$brcode = Session::get('brcode');
        $arrInsert = [];
        foreach ($data as $v){
            $arrInsert [$v->name] = $v->value;
        }
        if($arrInsert['id'] == ""){
			$id = DB::connection('mysql')->table($db['fsctaccount'].'.'.'good')->insertGetId($arrInsert);
				$model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'log_good')->insert(
			[
			'id' => $id,
			'name' => $arrInsert['name'],
			'type_id' => $arrInsert['type_id'],
			'group_supplier' => $arrInsert['group_supplier'],
			'unit' => $arrInsert['unit'],
			'accounttype' => $arrInsert['accounttype'],
			'status' => $arrInsert['status'],
			'change_type' =>1,
			'emp_id' => $empcode
			]
			);
			
			
        }else{
			

					$idd= $arrInsert['id'];
			        $sql = "SELECT $db[fsctaccount].good.* 
					FROM $db[fsctaccount].good 
					WHERE $db[fsctaccount].good.id = '$idd'";
					$oldarray = DB::connection('mysql')->select($sql);
			$model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'good')->where('id',$arrInsert['id'])->update($arrInsert);
			
			$model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'log_good')->insert(
			[
			'id' => $arrInsert['id'],
			'name' => $arrInsert['name'],
			'type_id' => $arrInsert['type_id'],
			'unit' => $arrInsert['unit'],
			'group_supplier' => $arrInsert['group_supplier'],
			'accounttype' => $arrInsert['accounttype'],
			'status' => $arrInsert['status'],
			'old_name' => $oldarray[0]->name,
			'old_type_id' => $oldarray[0]->type_id,
			'old_group_supplier' => $oldarray[0]->group_supplier,
			'old_unit' => $oldarray[0]->unit,
			'old_accounttype' => $oldarray[0]->accounttype,
			'old_status' => $oldarray[0]->status,
			'change_type' =>2,
			'emp_id' => $empcode
			]
			);
			
			
			
			
        }

        return 1;
    }
	

	
	
	
	
	
	
	
	
	
	
	


}