<?php

namespace App\Http\Controllers;


use App\Item;
use App\Api\Connectdb;
use DB;
use Excel;
use Illuminate\Support\Facades\Input;
class POExcelController extends Controller
{
	public function importExport()
	{
		return view('importExport');
	}
	public function downloadExcelPO()
	{
		$datapost = Input::all();
		// print_r($datapost);
		
		$type = 'xls';
	
		// exit();
		 
		$db = Connectdb::Databaseall();
		//$data = Item::get()->toArray();
		$date = date('Y-m-d');
		$week = $datapost['week'];
		$month = $datapost['month'];
		$year = $datapost['year'];
		$branch = $datapost['branch'];
		$other = "  AND H.branch_id = '$branch'";
			
		$sql = "SELECT *
				FROM $db[fsctaccount].po_head AS H
				INNER JOIN $db[fsctaccount].po_detail AS D on H.id = D.po_headid
				WHERE D.statususe = 1 
				AND (H.week = '$week' OR (date like '____-$month-__' AND date like '$year%'))".$other; 
						
		$model = DB::connection('mysql')->select($sql);	
		
		$ss = "SELECT count(*) as num
				FROM $db[fsctaccount].po_head AS H
				INNER JOIN $db[fsctaccount].po_detail AS D on H.id = D.po_headid
				WHERE D.statususe = 1 
				AND (H.week = '$week' OR (date like '____-$month-__' AND date like '$year%'))".$other; 
		$mod = DB::connection('mysql')->select($ss);

		foreach($mod as $d => $v){
				$count = $v->num;
			}
		
		$data = [];
		foreach($model as $k => $val){
			// $data[$k]['#']=$val->id;
			$data[$k]['เลขใบ PO']=$val->po_number;
			$data[$k]['วันเดือนปี']=$val->date; $day = $val->date;
			$data[$k]['ราคา']=$val->price;
			$data[$k]['จำนวน']=$val->amount;
			$data[$k]['vat (บาท)']=(($val->vat * ($val->price * $val->amount))/100);
			$data[$k]['ภาษีหัก ณ ที่จ่าย (บาท)']=(((($val->vat * ($val->price * $val->amount))/100) * $val->withhold)/100);
			$data[$k]['ส่วนต่าง']=($val->payreal - $val->total);
			$data[$k]['ราคารวม']=$val->total;
			$data[$k]['จ่ายจริง']=$val->payreal;
		}
		
		return Excel::create('Report_PO_Excel_'.$date, function($excel) use ($data,$count,$day) {
	
			$excel->sheet('mySheet', function($sheet) use ($data,$count,$day)
	        {				
						
				$sheet->fromArray(array(array(''),array('')));
				$sheet->fromArray($data);
				// Set top, right, bottom, left
				$sheet->setPageMargin(array(
				0.25, 0.30, 0.25, 0.30
				));
				
				$sheet->cell('A1', function($cell) {
					$cell->setValue('รายงานใบสั่งซื้อ  (PO)');
					 // Set font
                    $cell->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '18',
                        'bold'       =>  true
					));	
				});
				
				$sheet->cell('B2', function($cell) use ($day) {
					$cell->setValue('วันที่ :  '.$day);
					 // Set font
                    $cell->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '10',
                        'bold'       =>  false
					));	
				});
								
				// Sets borders
				
					$styleArray = array(
						'borders' => array(
							'allborders' => array(
								'style' => 'thin'
							)
						)
					);
				
				$sheet->fromArray(array(array('','','','')),'','','',''); // empty rows
					
				$sheet->fromArray(array(
						array('รวมทั้งหมด  :  ','',"=SUM(C5:C".(($count+5)-1).")",'',"=SUM(E5:E".(($count+5)-1).")","=SUM(F5:F".(($count+5)-1).")","=SUM(G5:G".(($count+5)-1).")",
						"=SUM(H5:H".(($count+5)-1).")","=SUM(I5:I".(($count+5)-1).")")),'','','',''); //sum rows
				
					// Apply the style
					$sheet->getDefaultStyle()
						->applyFromArray($styleArray);
				
				$sheet->mergeCells('A1:J1');
				$sheet->mergeCells('B2:C2');
				$sheet->mergeCells("A".(($count+5)+1).":B".(($count+5)+1));
				$sheet->getStyle('A1:J1')->getAlignment()->applyFromArray(
					array('horizontal' => 'center')
				);
				$sheet->getStyle("A".(($count+5)+1).":B".(($count+5)+1))->getAlignment()->applyFromArray(
					array('horizontal' => 'center')
				);
				$sheet->fromArray();
				
	        });
			
		})
		->download($type);
		
		// ->store('xls', storage_path('excel/exports'));
	}
	public function importExcel()
	{
		if(Input::hasFile('import_file')){
			$path = Input::file('import_file')->getRealPath();
			$data = Excel::load($path, function($reader) {
			})->get();
			if(!empty($data) && $data->count()){
				foreach ($data as $key => $value) {
					$insert[] = ['title' => $value->title, 'description' => $value->description];
				}
				if(!empty($insert)){
					DB::table('items')->insert($insert);
					dd('Insert Record successfully.');
				}
			}
		}
		return back();
	}
}
?>