<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;

?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }

        body {
            font-family: "THSarabunNew";
        }
        h4 {
            font-family: "THSarabunNew";
        }
        h4 {
            font-family: "THSarabunNew";
        }


        .container table {
        border-collapse: collapse;
        border: solid 1px #000;
        }
        .container table td {
        border: solid 1px #000;
        }
        .no-border-left-and-bottom {
        border-left: solid 1px #FFF!important;
        border-bottom: solid 1px #FFF!important;
        }
        .no-border-left-and-bottom-and-top {
        border-left: solid 1px #FFF!important;
        border-bottom: solid 1px #FFF!important;
        border-top: solid 1px #FFF!important;
        }
        .no-border-bottom-and-top {
        border-bottom: solid 1px #FFF!important;
        border-top: solid 1px #FFF!important;
        }
        .no-top{
        border-top: solid 1px #FFF!important;
        }
    </style>
</head>
<body>
    <?php
        $db = Connectdb::Databaseall();
        $sql = "SELECT * FROM $db[fsctaccount].po_head  WHERE id ='$id' ";
        $datahead = DB::connection('mysql')->select($sql);

        $brcode = Session::get('brcode');
        $brcodedetail = Maincenter::databranchbycode($brcode);


        $type_pay = $datahead[0]->type_pay;

    ?>

    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="10%">
                @if($datahead[0]->id_company==1)
                    <img src="images/company/1.png" width="275px" >
                @elseif($datahead[0]->id_company==2)
                    <img src="images/company/2.png" width="275px" >
                @endif
            </td>
            <td width="90%" valign="top" style="padding-top: -30px">
                <table width="100%">
                    <tr>
                        <td>
                            <?php $datacompany = Maincenter::databranchbycode($brcode);?>
                            <h3 style="color: white;"> FSCT<?php //print_r($datacompany[0]->name_eng);?></h3>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: -55px">
                            <?php $datacompany = Maincenter::datacompany($datahead[0]->id_company);?>
                          <h4><?php print_r($datacompany[0]->name)?></h4>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: -35px">
                            <?php $datacompany = Maincenter::datacompany($datahead[0]->id_company);?>
                            <?php print_r($brcodedetail[0]->addresstax)?>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: -15px">
                           โทรศัพท์  <?php print_r($datacompany[0]->Tel)?>
                           เลขประจำตัวผู้เสียภาษี <?php print_r($datacompany[0]->business_number)?>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: -15px">
                            โทรสาร  <?php print_r($datacompany[0]->Fax)?>
                        </td>
                    </tr>
                </table>


            </td>
        </tr>
    </table>

    <center><h3>ใบสำคัญจ่าย/Payment Voucher </h3></center>
    <?php
    // print_r($datahead[0]->pr_id);
    // exit;
    //$datadetail = Vendorcenter::getdatadetailpr($datahead[0]->pr_id);

//    print_r($datadetail);
//    exit;

    $i= 0;

   // print_r($datadetail);
    ?>
    <table width="100%" border="0">
        <tr>
            <td width="50%" colspan="2"><font style="font-weight: bold">Date/วันที่  :...........................</font></td>
            <td width="50%" colspan="2" align="right"><font style="font-weight: bold">PV NO./เลขที่ใบสำคัญจ่าย  :</font>{{$datahead[0]->id_pv}}</td>
        </tr>
        <tr>
            <td width="50%" colspan="2"><font style="font-weight: bold">Receiver/ผู้รับเงิน :
                   <?php
                    $data = Vendorcenter::getdatavendorcenter($datahead[0]->supplier_id);
                    print_r($data[0]->pre.'  '.$data[0]->name_supplier);

                   ?>
                </font></td>
            <td width="50%" colspan="2" align="right"><font style="font-weight: bold">

                    <?php
                        if($data[0]->tax_id!=0){
                              echo "เลขที่ผู้เสียภาษี".'      '.$data[0]->tax_id;
                        }else{
                              echo "";
                        }
                    ?>
            </td>
        </tr>
        <tr>
            <td width="100%" colspan="4"><font style="font-weight: bold">Address/ที่อยู่ :
                <?php
                    print_r($data[0]->address .'  ตำบล  '.$data[0]->district.'  อำเภอ  '.$data[0]->amphur.'  จังหวัด  '.$data[0]->province);

                    print_r('รหัสไปรษณีย์  '.$data[0]->zipcode .' โทรศัพท์ '.$data[0]->phone);

                ?>
                </font>
            </td>
        </tr>
    </table>
    <br>
    <?php
    $datadetail = Vendorcenter::getdatapodetail($datahead[0]->id);
    $i = 0;

     $arrwhd = [];
     $arrTotal = [];

     $sqlinform_po = "SELECT * FROM $db[fsctaccount].inform_po  WHERE id_po ='$id' ";
     $datainfo = DB::connection('mysql')->select($sqlinform_po);
     $discountmoney = 0 ;
     $payout = 0;

     if(!empty($datainfo)){
            $payout = $datainfo[0]->payout;
            $discountmoney = $payout;
     }else{
            $discountmoney = 0;
            $payout = 0;
     }
     // exit;
     // echo "<pre>";
     // print_r($datainfo);

     ?>

    <table width="100%" border="1"  cellspacing="0" cellpadding="0">
        <tr valign="top">
            <td width="3%" align="center" bgcolor="#dcdcdc">#</td>
            <td width="25%" align="center"  bgcolor="#dcdcdc">รายการ</td>
            <td width="12%" align="center"  bgcolor="#dcdcdc">ราคาต่อหน่วย</td>
            <td width="10%" align="center"  bgcolor="#dcdcdc">จำนวน</td>
            <td width="10%" align="center"  bgcolor="#dcdcdc">รวม</td>
        </tr>
        <?php foreach ($datadetail as $key => $value){?>
        <tr>
          <td align="center">  <?php echo $i+1; ?></td>
          <td>&nbsp;&nbsp;<?php echo $value->list.'&nbsp;&nbsp;&nbsp;'.$value->type_amount;?></td>
          <td align="center"><?php echo number_format($value->price,2);?></td>
          <td align="center"><?php echo $value->amount;?></td>
          <td align="center"><?php
          $arrTotal[] = $value->total;
          echo number_format($value->total,2);?>
          </td>
        </tr>
      <?php $i++; } ?>
        <tr>
          <td colspan="3" class="no-border-bottom-and-top"></td>
          <td align="center" bgcolor="#dcdcdc"> รวม</td>
          <td align="center"><?php
                  echo array_sum($arrTotal)
              ?>
          </td>
        </tr>
        <tr>
          <td colspan="3" class="no-border-bottom-and-top"></td>
          <td align="center" bgcolor="#dcdcdc"> ส่วนลด (บาท)</td>
          <td align="center">
            0
          </td>
        </tr>
        <tr>
          <td colspan="3" class="no-border-bottom-and-top"></td>
          <td align="center" bgcolor="#dcdcdc"> รวมทั้งสิ้น (บาท)</td>
          <td align="center"><?php

                  echo array_sum($arrTotal) ;
              ?>
          </td>
        </tr>
        <tr>
          <td colspan="3" class="no-border-bottom-and-top">
              &nbsp;&nbsp;ประเภทการจ่ายเงิน
              <b>
                <?php
                $sqltype_pay = "SELECT * FROM $db[fsctaccount].type_pay  WHERE id ='$type_pay' ";
                $datatypepayname = DB::connection('mysql')->select($sqltype_pay);

                echo $datatypepayname[0]->name_pay;
                ?>
              </b>
          </td>
          <td align="center"  bgcolor="#dcdcdc">รวม ภาษีหัก ณ ที่จ่าย</td>
          <td align="center"><?php print_r(array_sum($arrwhd));?></td>
        </tr>
        <tr>
          <td colspan="3" class="no-border-bottom-and-top"></td>
          <td align="center" bgcolor="#dcdcdc"> ภาษี <?php print_r($datahead[0]->vat) ?> % (บาท)</td>
          <td align="center"><?php
                $totalvat = (array_sum($arrTotal)*($datahead[0]->vat/100));
            echo number_format($totalvat,2);?>
          </td>
        </tr>
        <tr>
          <td class="no-top"  colspan="3" align="center">
            <?php
            $totalbeforecal = array_sum($arrTotal)+$totalvat-array_sum($arrwhd);

              echo "<b>( ".(Accountcenter::converttobath(number_format($totalbeforecal,2)))." )</b>";
             ?>
          </td>
          <td align="center"  bgcolor="#dcdcdc"> รวม สุทธิ (บาท)</td>
          <td align="center"><?php
              echo number_format($totalbeforecal,2);
          ?></td>
        </tr>
    </table>
    <br>
    <br>

    <!-- <table width="100%" border="0">
        <tr>
            <td width="10%" valign="top"><b>หมายเหตุ</b></td>
            <td width="90%">Price are included VAT / ราคาที่สั่งซื้อ เป็นราคาที่ได้รวมภาษีมูลค่าเพิ่มไว้แล้ว <br>
                Please refer the Purchase Voucher No. at each Delivery Note/Invoice. กรุณาระบุหมายเลขใบสำคัญจ่ายทุกครั้งในใบส่งสินค้า/ใบแจ้งหนี้<br>
                Please issue the receipt in the name of <b><?php //print_r($datacompany[0]->name_eng)?></b><br>
                กรุณาออกใบเสร็จรับเงินในนาม <b><?php //print_r($datacompany[0]->name)?></b>


            </td>
        </tr>
    </table> -->

    <br>
    <br>
    <table width="100%" border="0">
        <tr>
            <td width="50%" align="center">
                ....................................................................................
            </td>
            <td width="50%" align="center">
                ....................................................................................
            </td>
        </tr>
        <tr>
            <td width="50%" align="center">

                (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
            </td>
            <td width="50%" align="center">
                (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
            </td>
        </tr>
        <tr>
            <td width="50%" align="center">
                ผู้จ่ายเงิน
            </td>
            <td width="50%" align="center">
                ผู้รับเงิน
            </td>
        </tr>
    </table>
    <br><br><br>
    <table width="100%" border="0">
        <tr>
            <td width="50%" align="center">
                ....................................................................................
            </td>
            <td width="50%" align="center">

            </td>
        </tr>
        <tr>
            <td width="50%" align="center">

                (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
            </td>
            <td width="50%" align="center">

            </td>
        </tr>
        <tr>
            <td width="50%" align="center">
                ผู้ตรวจสอบ/อนุมัติ
            </td>
            <td width="50%" align="center">

            </td>
        </tr>
    </table>
</body>
</html>
