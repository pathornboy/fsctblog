<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;
use  App\Api\Datetime;
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }

        body {
            font-family: "THSarabunNew";
        }
        h3 {
            font-family: "THSarabunNew";
        }
        h4 {
            font-family: "THSarabunNew";
        }



    </style>
</head>
<body>

  <?php
        $datepicker = Session::get('datepickerboxcover');
        $dateranger = Datetime::FormatDateFromCalendarRange($datepicker);
        $start_date = $dateranger['start_date'];
        $s = explode('-',$start_date);
        // print_r($s);

        $end_date = $dateranger['end_date'];
        $e = explode('-',$end_date);

        $brcode = Session::get('brcode');

        $db = Connectdb::Databaseall();
        // echo "<pre>";
        // print_r($data);
        // exit;
        $dbac = $db['fsctaccount'];
        $sql ='SELECT '.$db['fsctaccount'].'.po_head.*
               FROM '.$db['fsctaccount'].'.po_head
               WHERE '.$db['fsctaccount'].'.po_head.date BETWEEN  "'.$start_date.'" AND "'.$end_date.'"
               AND '.$db['fsctaccount'].'.po_head.status_head IN (2,3,4,5)
               AND '.$db['fsctaccount'].'.po_head.branch_id =  "'.$brcode.'"';

        $data = DB::connection('mysql')->select($sql);

  ?>
    <center><h3 style="margin-top:-15px">รายการค่าใช่จ่ายตั้งแต่วันที่ <?php echo $s[2].'-'.$s[1].'-'.$s[0]?> ถึง  <?php echo $e[2].'-'.$e[1].'-'.$e[0]?></h3></center>
    <center><h4 style="margin-top:-15px">สาขา
                <?php $brdata = Maincenter::databranchbycode($brcode);
                      echo $brdata[0]->name_branch;
                ?>
                   (<?php  echo $brcode; ?>)
            </h4>
    </center>
    <table border="1" cellspacing="0" cellpadding="0"  width="100%">
        <thead>
          <tr>
            <td colspan="7"></td>
            <td colspan="4" align="center">ลักษณะบิล</td>
            <td></td>
            <td></td>
          </tr>
          <tr>

            <td align="center"><font size="12px">ลำดับ</font></td>
            <td align="center"><font size="12px">เลขที่ PO</font></td>
            <td align="center"><font size="12px">เจ้าหนี้</font></td>
            <td align="center"><font size="12px">รายการ</font></td>
            <td align="center"><font size="12px">จำนวนเงิน</font></td>
            <td align="center"><font size="12px">ภาษีมูลค่าเพิ่ม</font></td>
            <td align="center"><font size="12px">จำนวนเงินรวม</font></td>
            <td align="center"><font size="12px">ใบกำกับภาษี</font></td>
            <td align="center"><font size="12px">สำเนาใบกำกับภาษี</font></td>
            <td align="center"><font size="12px">บิลเงินสด</font></td>
            <td align="center"><font size="12px">ค่าใช่จ่ายที่ไม่มีบิล</font></td>

            <td align="center"><font size="12px">ใบPO/สำคัญจ่าย</font></td>
            <td align="center"><font size="12px">หมายเหต</font>ุ</td>

          </tr>
        </thead>
        <tbody>
          <?php
          $i = 1;
          foreach ($data as $key => $value) {
          ?>
            <tr>
              <td><font size="12px"><?php echo $i;?></font></td>
              <td><font size="12px">
                <?php
                      echo ($value->po_number);
                ?>
                  </font>
              </td>
              <td><font size="12px">
                <?php
                      $supplier = Vendorcenter::getdatavendorcenter($value->supplier_id);
                      echo $supplier[0]->pre.'  '.$supplier[0]->name_supplier;
                ?></font>
              </td>
              <td><font size="12px">
                <?php
                      $datadetailpo = getdetailpo($value->id);
                      // echo "<pre>";
                      foreach ($datadetailpo as $k => $v) {
                            echo $v->list."<br>";
                      }
                ?></font>
              </td>
              <td align="right"><font size="12px">
                <?php
                        if($value->status_head==2){
                            $datadetailpo = getdetailpo($value->id);
                            $total = 0;
                            foreach ($datadetailpo as $r => $l) {
                                  $total= $total + $l->total;
                            }
                              echo number_format($total,2);
                        }else if($value->status_head==3){
                            $dateshow = getdata($value->id);
                            if($value->vat!=0){
                                echo number_format(($dateshow[0]->payout+$dateshow[0]->wht-$dateshow[0]->vat_price),2);
                            }else{
                                echo number_format(($dateshow[0]->payout+$dateshow[0]->wht-$dateshow[0]->vat_price),2);
                            }



                        }else if($value->status_head==4){
                            $dateshow = getdata($value->id);
                            if($value->vat!=0){
                                echo number_format(($dateshow[0]->payout+$dateshow[0]->wht-$dateshow[0]->vat_price),2);
                            }else{
                                echo number_format(($dateshow[0]->payout+$dateshow[0]->wht-$dateshow[0]->vat_price),2);
                            }


                        }else if($value->status_head==5){
                            $dateshow = getdata($value->id);
                            if($value->vat!=0){
                                echo number_format(($dateshow[0]->payout+$dateshow[0]->wht-$dateshow[0]->vat_price),2);
                            }else{
                                echo number_format(($dateshow[0]->payout+$dateshow[0]->wht-$dateshow[0]->vat_price),2);
                            }
                        }
                ?></font>
              </td>
              <td  align="right"><font size="12px">
                <?php
                        if($value->status_head==2){
                            $datadetailpo = getdetailpo($value->id);
                            $total = 0;
                            foreach ($datadetailpo as $r => $l) {
                                  $total= $total + $l->total;
                            }
                                if($value->vat!=0){
                                    echo number_format(($total*($value->vat/100)),2);
                                }else{
                                    echo "0.00";
                                }
                        }else if($value->status_head==3){
                            $dateshow = getdata($value->id);
                            if($value->vat!=0){
                                echo number_format(($dateshow[0]->vat_price),2);
                            }else{
                                echo number_format(($dateshow[0]->vat_price),2);
                            }

                        }else if($value->status_head==4){
                            $dateshow = getdata($value->id);
                            if($value->vat!=0){
                                echo number_format(($dateshow[0]->vat_price),2);
                            }else{
                                echo number_format(($dateshow[0]->vat_price),2);
                            }


                        }else if($value->status_head==5){
                            $dateshow = getdata($value->id);
                            if($value->vat!=0){
                                echo number_format(($dateshow[0]->vat_price),2);
                            }else{
                                echo number_format(($dateshow[0]->vat_price),2);
                            }

                        }
                ?></font>
              </td>
              <td  align="right"><font size="12px">
                <?php
                        if($value->status_head==2){
                            $datadetailpo = getdetailpo($value->id);
                            $total = 0;
                            foreach ($datadetailpo as $r => $l) {
                                  $total= $total + $l->total;
                            }
                                if($value->vat!=0){
                                    echo number_format($total+ ($total*($value->vat/100)),2);
                                }else{
                                    echo number_format($total+ ($total*($value->vat/100)),2);
                                }
                        }else if($value->status_head==3){
                            $dateshow = getdata($value->id);
                            echo number_format($dateshow[0]->payout,2);

                        }else if($value->status_head==4){
                            $dateshow = getdata($value->id);
                            echo number_format($dateshow[0]->payout,2);
                        }else if($value->status_head==5){
                            $dateshow = getdata($value->id);
                            echo number_format($dateshow[0]->payout,2);
                        }
                ?></font>
              </td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          <?php $i++;} ?>
        </tbody>
    </table>
    <br>
    <br>
    <table border="0" width="100%">
      <tr>
          <td width="16.66%"></td>
          <td width="16.66%"><hr></td>
          <td width="16.66%"></td>
          <td width="16.66%"></td>
          <td width="16.66%"><hr></td>
          <td width="16.66%"></td>
      </tr>
      <tr>
          <td></td>
          <td align="center">ผุ้จัดทำ</td>
          <td></td>
          <td></td>
          <td align="center">ผู้ครวจสอบ</td>
          <td></td>
      </tr>
    </table>
  </font>
    <?php
        function getdata($id){
            $db = Connectdb::Databaseall();
            $dbac = $db['fsctaccount'];
            $sqlinform = "SELECT $dbac.inform_po.*
                          FROM $dbac.inform_po
                          WHERE  $dbac.inform_po.id_po = '$id'
                          AND status = '1' ";

            $dataquery = DB::connection('mysql')->select($sqlinform);
            return $dataquery;
        }

        function getdetailpo($id){
            $db = Connectdb::Databaseall();
            $dbac = $db['fsctaccount'];
            $sqlinform = "SELECT $dbac.po_detail.*
                          FROM $dbac.po_detail
                          WHERE  $dbac.po_detail.po_headid = '$id'
                          AND statususe = '1' ";

            $dataquery = DB::connection('mysql')->select($sqlinform);
            return $dataquery;
        }

    ?>


</body>
</html>
