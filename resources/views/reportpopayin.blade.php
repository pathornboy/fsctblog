<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Vendorcenter;
use  App\Api\Maincenter;



$db = Connectdb::Databaseall();
?>
@include('headmenu')
<link>

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>

<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">
<script type="text/javascript" src = 'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js'></script>
<script type="text/javascript" src = 'https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js'></script>
<script type="text/javascript" src = 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js'></script>
<script type="text/javascript" src = 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js'></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">




<script type="text/javascript" src = 'js/vendor/reportpopayin.js'></script>


<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<style>
    .modal-ku {
        width: 90%;
        margin: auto;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">งานจัดซื้อ</a>
                    </li>

                </ul>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        รายงานใบ PO ที่จ่ายแล้ว
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body" style="overflow-x:auto;">
                              <form action="serachreportpopayin" method="post">
                                  <input type="hidden" name="_token" value="{{ csrf_token() }}">

                              <div class="row">
                                <div class="col-md-4">

                                </div>
                                <div class="col-md-4">
                                    <?php
                                        $db = Connectdb::Databaseall();
                                        $sql = 'SELECT '.$db['hr_base'].'.branch.*
                                           FROM '.$db['hr_base'].'.branch
                                           WHERE '.$db['hr_base'].'.branch.status = "1"';

                                        $brcode = DB::connection('mysql')->select($sql);

                                    ?>
                                    <select name="branch" id="branch" class="form-control" >
                                        <option value="*">เลือกทั้งหมด</option>
                                        <?php
                                          foreach ($brcode as $key => $value) {
                                        ?>
                                          <option value="<?php echo $value->code_branch;?>" <?php if(!empty($data)&&($value->code_branch==$branch)){ echo "selected";}?> > <?php echo $value->name_branch;?></option>

                                        <?php }?>
                                    </select>
                                </div>
                                <div class="col-md-4">

                                </div>
                              </div>
                              <div class="row">
                                  <br>
                              </div>
                              <div class="row">
                                <div class="col-md-4">

                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="datepicker" id="datepicker" <?php if(!empty($data)){ echo $datepicker;}?>  class="form-control datepicker" readonly>
                                </div>
                                <div class="col-md-4">

                                </div>
                              </div>
                              <div class="row">
                                  <br>
                              </div>
                              <div class="row">
                                <div class="col-md-3">

                                </div>
                                <div class="col-md-3">
                                    <input type="submit" value="ค้นหา" class="btn btn-primary pull-right">
                                </div>
                                <div class="col-md-3">
                                    <input type="reset" class="btn btn-danger">
                                </div>
                                <div class="col-md-3">

                                </div>
                              </div>
                            </form>
                              <div class="row">
                                <?php if(!empty($data)){

                                      $db = Connectdb::Databaseall();

                                  ?>
                                    <table id="example" class="display nowrap" style="width:100% ">
                                        <thead>
                                            <tr>
                                                <th>ลำดับที่</th>
                                                <th>รหัสสาขา</th>
                                                <th>ชื่อสาขา</th>
                                                <th>วันที่</th>
                                                <th>เลขที่เอกสาร</th>
                                                <th>ชื่อเจ้าหนี้</th>
                                                <th>เลขผู้เสียภาษี</th>
                                                <th>รายละเอียด</th>
                                                <th>จำนวนเงิน</th>
                                                <th>ภาษีมูลค่าเพิ่ม</th>
                                                <th>ภาษีซื้อไม่ขอคืน</th>
                                                <th>จำนวนเงินรวมทั้งสิ้น</th>
                                                <th>WHT %</th>
                                                <th>WHT</th>
                                                <th>Account Code</th>
                                                <th>Account Descriton</th>
                                                <th>หมายเหตุ</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                              $i=1;
                                              $before = 0;
                                              $vatmoney = 0;
                                              $totolsumall = 0;
                                              foreach ($dataresulte as $k => $v) { ?>
                                            <tr>
                                                <td><?php echo $i;?></td>
                                                <td><?php echo $v->branch_id;?></td>
                                                <td><?php
                                                  $namebtanch = Maincenter::databranchbycode($v->branch_id);
                                                  echo $namebtanch[0]->name_branch;

                                                 ?>
                                                </td>
                                                <td><?php echo $v->date; ?></td>
                                                <td>
                                                  <?php   $id = $v->id; //เลขที่เอกสาร

                                                        $sqlrun_money = "SELECT po_bill_run_money.*
                                                                FROM $db[fsctaccount].po_bill_run_money
                                                                WHERE po_headid = '$id' AND status = '1' ";
                                                        $run_money = DB::connection('mysql')->select($sqlrun_money);
                                                        if(!empty($run_money)){
                                                          echo $run_money[0]->bill_no;
                                                        }

                                                      if(!empty($run_money)){
                                                        if($v->vat==0){
                                                            $before =  $run_money[0]->pay_real;
                                                            $vatmoney = 0;
                                                            $totolsumall = $run_money[0]->pay_real;
                                                        }else{
                                                            $before = ((($v->totolsumall)*100)/(100+$v->vat));
                                                            $vatmoney = $before*($v->vat/100);
                                                            $totolsumall = $before + $vatmoney;

                                                          }
                                                      }else{

                                                            // $before = 0;
                                                            // $vatmoney = 0;
                                                            // $totolsumall = 0;
                                                            $before = ($v->totolsumall*100)/107;
                                                            $vatmoney = $before*0.07;
                                                            $totolsumall = $v->totolsumall;
                                                      }





                                                  ?>
                                                </td>
                                                <td>
                                                  <?php
                                                        $supplier_id =  $v->supplier_id;
                                                        $sqlsupplier = "SELECT * FROM $db[fsctaccount].supplier
                                                        WHERE id='$supplier_id' ";
                                                        $suppliername = DB::connection('mysql')->select($sqlsupplier);
                                                        echo $suppliername[0]->pre.'  '.$suppliername[0]->name_supplier;

                                                  ?>
                                                </td>
                                                <td>
                                                      <?php echo $suppliername[0]->tax_id; ?>
                                                </td>
                                                <td>
                                                  <?php
                                                        $sqldetail = "SELECT * FROM $db[fsctaccount].po_detail
                                                        WHERE po_headid='$id'
                                                        AND status IN(1,2)
                                                        AND statususe = '1' ";
                                                        $detail = DB::connection('mysql')->select($sqldetail);
                                                        // print_r($detail);

                                                        foreach ($detail as $x => $y) {
                                                            echo $y->list;
                                                            echo "<br>";
                                                        }
                                                   ?>
                                                </td>
                                                <td>
                                                  <?php

                                                        echo number_format($before,2);
                                                   ?>
                                                </td>
                                                <td>
                                                  <?php

                                                  echo number_format($vatmoney,2);

                                                  ?>
                                                </td>
                                                <td>
                                                    -
                                                </td>
                                                <td>
                                                  <?php echo number_format($totolsumall,2); ?>
                                                </td>
                                                <td>
                                                    -
                                                </td>
                                                <td>
                                                    -
                                                </td>
                                                <td>
                                                  <?php   foreach ($detail as $x => $y) {

                                                        $idm =  $y->materialid;
                                                        $sqlacccode = "SELECT $db[fsctaccount].good.*,
                                                                              $db[fsctaccount].accounttype.accounttypeno,
                                                                              $db[fsctaccount].accounttype.accounttypefull
                                                                        FROM $db[fsctaccount].good
                                                                        INNER JOIN  $db[fsctaccount].accounttype
                                                                        ON $db[fsctaccount].good.accounttype = $db[fsctaccount].accounttype.id
                                                                        WHERE good.id='$idm'";
                                                        $acccode = DB::connection('mysql')->select($sqlacccode);

                                                        if(!empty($acccode)){
                                                          echo $acccode[0]->accounttypeno;
                                                          echo "<br>";
                                                        }

                                                    } ?>
                                                </td>
                                                <td>
                                                  <?php   foreach ($detail as $x => $y) {

                                                    $idm =  $y->materialid;
                                                    $sqlacccode = "SELECT $db[fsctaccount].good.*,
                                                                          $db[fsctaccount].accounttype.accounttypeno,
                                                                          $db[fsctaccount].accounttype.accounttypefull
                                                                    FROM $db[fsctaccount].good
                                                                    INNER JOIN  $db[fsctaccount].accounttype
                                                                    ON $db[fsctaccount].good.accounttype = $db[fsctaccount].accounttype.id
                                                                    WHERE good.id='$idm' ";
                                                    $acccode = DB::connection('mysql')->select($sqlacccode);
                                                    if(!empty($acccode)){
                                                      echo $acccode[0]->accounttypefull;
                                                      echo "<br>";
                                                    }


                                                    } ?>
                                                </td>
                                                <td>

                                                </td>
                                            </tr>
                                            <?php $i++; } ?>
                                        </tbody>
                                    </table>
                                <?php } ?>
                              </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')
