<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;

?>
@include('headmenu')
<link>

{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}
<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>

<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>

<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">

<script type="text/javascript" src = 'js/vendor/vendorcenter.js'></script>

<link rel="stylesheet" type="text/css" href="bower_components/select2/dist/css/select2.min.css">
<script type="text/javascript" src = 'bower_components/select2/dist/js/select2.full.min.js'></script>


<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	
	$(document).ready(function(){
		// console.log("TEST");
		$(".week").hide();
		$(".month").hide();
		
		$('input[name="choice"]').change(function () {
			if (this.value == 1) {
				$(".week").show();
				$(".month").hide();
				$("#month").val('');
				$("#year").val('');
				
				$("#week").attr('required',true);
				$("#month").attr('required',false);
				$("#year").attr('required',false);
				// console.log("TEST");
			}else if(this.value == 2){
				$(".week").hide();
				$(".month").show();
				$("#week").val('');
				
				$("#month").attr('required',true);
				$("#year").attr('required',true);
				$("#week").attr('required',false);
				// console.log("YYYY");
			}
		});
	});
	
</script>
<style>
    .modal-ku {
        width: 90%;
        margin: auto;
    }
</style>
<style type="text/css" media="print">
input{
    display:none;
}
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">งานจัดซื้อ</a>
                    </li>
                    <li class="active">รายงานใบสั่งซื้อ (PO)</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        รายงานใบสั่งซื้อ (PO)
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body">
								<div class="row">
									<div class="col-md-2 text-right">
										<b>เลือกรูปแบบการค้นหา : </b>
									</div>
									<div class="col-md-4">
										<form method="GET" action="<?php echo url("/pocenter"); ?>">
											<input type="radio" name="choice" id="choice" value="1" /> &nbsp;ค้นหาจากงวด &nbsp;&nbsp;&nbsp;&nbsp;
											<input type="radio" name="choice" id="choice" value="2" /> &nbsp;ค้นหาจากเดือนปี &nbsp;&nbsp;&nbsp;&nbsp;
										
										</br></br>
									</div>
								</div>
								<div class="row ">
									<div class="col-md-2 text-right week">
										<b>เลือก งวดการจ่ายเงิน : </b>
									</div>
									<div class="col-md-2 week">
										
										  <select name="week" id="week" class="form-inline select2" >
											  <option value="" selected>เลือกงวดการจ่ายเงิน</option>
											  <?php
												$db = Connectdb::Databaseall();
												$dataweek = DB::connection('mysql')->select('SELECT week FROM '.$db['fsctaccount'].'.ppr_head GROUP BY week ORDER BY ppr_head.week DESC');
											  ?>
											  @foreach($dataweek as $week)
												<option value="{{$week->week}}">{{$week->week}}</option>
											  @endforeach
										  </select>
										 
									</div>
									<div class="col-md-2 text-right month">
										<b>เลือก เดือน : &nbsp;&nbsp;&nbsp;</b>
									</div>
									<div class="col-md-2 text-left month">
										<select name="month" id="month" class="form-inline" >
											<option value="" selected>--เลือกเดือน--</option>
											<option value="01">มกราคม</option>
											<option value="02">กุมภาพันธ์</option>
											<option value="03">มีนาคม</option>
											<option value="04">เมษายน</option>
											<option value="05">พฤษภาคม</option>
											<option value="06">มิถุนายน</option>
											<option value="07">กรกฎาคม</option>
											<option value="08">สิงหาคม</option>
											<option value="09">กันยายน</option>
											<option value="10">ตุลาคม</option>
											<option value="11">พฤศจิกายน</option>
											<option value="12">ธันวาคม</option>
										</select>
									</div>
					<?php
						$year = date('Y');
						$yearTH = $year+543;
					?>			
									<div class="col-md-1 text-left month">
										<b>เลือก ปี : &nbsp;&nbsp;&nbsp;</b>
									</div>
									<div class="col-md-2 text-left month">
										<select name="year" id="year" class="form-inline" >
											<option value="" selected>--เลือกปี--</option>
									<?php
										for($x=2;$x>0;$x--){
											echo "<option value='".($year-$x)."'>".($yearTH-$x)."</option>";
										}
											echo "<option value='".$year."'>".$yearTH."</option>";
										for($y=1;$y<3;$y++){
											echo "<option value='".($year+$y)."'>".($yearTH+$y)."</option>";
											
										}
									?>
										</select>
									</div>
									<div class="col-md-2 text-right">
										<b>เลือก สาขา : </b>
									</div>
									<div class="col-md-2 ">
										  <select name="branch" id="branch" class="form-inline select2" required>
											  <option value="" selected>เลือกสาขา</option>
											  <option value="99">ทุกสาขา</option>
											  <?php
												$db = Connectdb::Databaseall();
												$databranch = DB::connection('mysql')->select('SELECT code_branch,name_branch FROM '.$db['hr_base'].'.branch GROUP BY code_branch ORDER BY branch.code_branch ASC');
											  ?>
											  @foreach($databranch as $branch)
												<option value="{{$branch->code_branch}}">{{$branch->name_branch}}</option>
											  @endforeach
										  </select> &nbsp;&nbsp;&nbsp;
										  
									</div>
									
									<div class="col-md-1">
										 <input type="submit" class="form-inline btn-success" id="search" value="Search" />
										</form> 
									</div>
								</div>
								<div class="row">
								
								</div>
                                <div class="row">
                                    <!--<div class="col-md-10">
                                        <div class="pull-right">
                                            <a href="<?php //echo url("/vendoraddbill");?>"><img src="images/global/invoice.png">เพิ่มรายการจัดซื้อ</a>
                                        </div>
                                    </div>-->
								
                                    <div class="col-md-5">
                                        <input type="hidden" name="settime" id="settime" value="<?php echo date('Y-m-d')?>">
                                        <input type="hidden" name="setlogin" id="setlogin" value="<?php echo $emp_code = Session::get('emp_code')?>">
                                        <!--<a href="#" title="เพิ่มข้อมูล" data-toggle="modal" data-target="#myModal" onclick="insertnew()" ><img src="images/global/add.png">เพิ่มข้อมูล</a>-->
                                    </div>
                                </div>								
                                <table id="example" class="table table-striped table-bordered">
                                    <thead class="thead-inverse">
                                    <tr>
                                        <td>#</td>
                                        <td>เลขใบ PO</td>
                                        <td align="center">วันเดือนปี</td>
                                        <td align="center">ราคา</td>
                                        <td align="center">จำนวน</td>
                                        <td align="center">vat (บาท)</td>
                                        <td align="center">หัก ณ ที่จ่าย (บาท)</td>
                                        <td align="center">ส่วนต่าง</td>
                                        <td align="center">ราคารวม</td>
                                        <td align="center">จ่ายจริง</td>
                                        
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
										$db = Connectdb::Databaseall();
										
										if((isset($_GET['branch']))){
											$week = $_GET['week'];
											$month = $_GET['month'];
											$year = $_GET['year'];
											$branch = $_GET['branch'];
										}else{
											$week = '';
											$month = '';
											$year = '';
											$branch = '';
										}
										
										// $week = '2018-02-20 10:00:00';
										
									if((isset($_GET['branch'])=='') ){
										
										$data = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.po_head as H
										inner join '.$db['fsctaccount'].'.po_detail as D on H.id = D.po_headid where D.statususe = 1');
										
										$sum = DB::table('po_detail')
													->where('statususe', '=', 1)
													->sum('total');
										
										$sumpayreal = DB::table('po_head')
													->where('status_head', '=', 2)
													->sum('payreal');
										
									}else{
																		
										if($_GET['branch'] == '99'){	
											$other = "";
										}else{
											$other = " AND H.branch_id = '$branch'";
										}
										
										$sql = "SELECT *
												FROM $db[fsctaccount].po_head AS H
												INNER JOIN $db[fsctaccount].po_detail AS D on H.id = D.po_headid
												WHERE D.statususe = 1 
												AND (H.week = '$week' OR (date like '____-$month-__' AND date like '$year%'))".$other; 
											
										
										
										// echo $sql;
										// exit();
										
										$data = DB::connection('mysql')->select($sql);		
																				
										$sum = DB::table('po_detail')
													->where('statususe', '=', 1)
													->sum('total');
										
										$sumpayreal = DB::table('po_head')
													->where('status_head', '=', 2)
													->where('week', '=', '$week')
													->where('branch_id', '=', '$branch')
													->sum('payreal');
									}
										$i = 1;
										
                                    ?>
                                    @foreach ($data as $value)
                                        <tr>
                                            <td scope="row">{{ $i }}</td>
                                            <td align="left">{{ $value->po_number }}</td>
                                            <td align="center">{{ $value->date }}</td>
                                            <td align="center">{{ number_format($value->price,2) }}</td>
                                            <td align="center">{{ $value->amount }}</td>
                                            <td align="center">{{ number_format(($value->vat * ($value->price * $value->amount))/100,2) }}</td>
                                            <td align="center">{{ number_format(((($value->vat * ($value->price * $value->amount))/100) * $value->withhold)/100,2) }}</td>
                                            <td align="center">{{ number_format($value->payreal - $value->totolsumall,2) }}</td>                                           
                                            <td align="right">{{ number_format($value->total,2) }}</td>
											<td align="right">{{ number_format($value->payreal,2) }}</td>
											
                                        </tr>
										
                                        <?php $i++; ?>
                                    @endforeach
                                    </tbody>
										<tr>
											<td colspan="8" align="right"><U><B>รวมจ่ายจริงทั้งหมด : </B></U></td>
											<td colspan="2" align="right"><U><B>{{ number_format($sumpayreal,2) }}</B></U></td>
										</tr>
                                </table>
								<?php //print_r($data);?>
								<p align="center">
									<?php $path = 'week='.$week.'&month='.$month.'&year='.$year.'&branch='.$branch?>
									<a href="<?php echo url("/poprint?$path");?>" target="_blank">
									<button class="btn btn-success">PRINT REPORT</button>
									</a>
									
									<a href="<?php echo url("/downloadExcelPO?$path"); ?>">
									<button class="btn btn-success">Download Excel xls</button>
									</a>
								</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')
