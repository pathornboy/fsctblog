<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Vendorcenter;

$db = Connectdb::Databaseall();
?>
@include('headmenu')
<link>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}
<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>

<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">

<script type="text/javascript" src = 'js/vendor/polist.js'></script>


<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<style>
    .modal-ku {
        width: 90%;
        margin: auto;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">งานจัดซื้อ</a>
                    </li>

                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body" style="overflow-x:auto;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        รายการอนุมติเสนอใบ PO
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body">
                              <form action="searchbc" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-2">
                                    <input type="text" name="datepicker" id="datepicker" class="form-control" value="<?php if(isset($querybr)){ echo $datepicker;}?>" readonly>
                                </div>
                                <div class="col-md-2">
                                  <?php

                                      $sqlbr ='SELECT '.$db['hr_base'].'.branch.*
                                      FROM '.$db['hr_base'].'.branch
                                      WHERE status != "99" ';
                                      $modelbr = DB::connection('mysql')->select($sqlbr);
                                  ?>
                                    <select name="branchselect" id="branchselect" class="form-control" >
                                        <option value=''>== เลือก สาขา ==</option>
                                        <?php foreach ($modelbr as $k => $v) {?>
                                        <option value='<?php echo $v->code_branch;?>' <?php
                                        if(isset($querybr)){
                                            if($branchselect==$v->code_branch){ echo "selected";}
                                        }
                                      ?>
                                        ><?php echo $v->name_branch;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                  <?php $arris = ['0'=>'รออนุมัติ',
                                                  '1'=>'อนุมัติแล้ว รอโอน',
                                                  '2'=>'โอนแล้ว',
                                                  '3'=>'จ่ายแล้ว ครบแล้ว',
                                                  '4'=>'จ่ายแล้ว มีเงินทอน',
                                                  '5'=>'จ่ายแล้ว เงินขาด'
                                                 ]

                                   ?>
                                    <select name="status_head" id="status_head" class="form-control" required>
                                        <option value=''>== เลือก สถานะ ==</option>
                                        <?php foreach ($arris as $ks => $vs) {?>
                                        <option value='<?php echo $ks;?>' <?php
                                        if(isset($querybr)){
                                            if($status_head==$ks){ echo "selected";}
                                        }
                                      ?>
                                        ><?php echo $vs;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <input type="submit" class="btn btn-success" value="ค้นหา">
                                </div>
                                <div class="col-md-2">
                                </div>
                              </form>
                            </div>
                            <div class="box-body">

                                <?php

                              $datemonth = date('Y-m-d');
                              $timeremove = strtotime('-7 day',strtotime($datemonth));
                              $timeremove = date('Y-m-d',$timeremove);


                                $sql ='SELECT '.$db['fsctaccount'].'.po_head.id,
                                              '.$db['fsctaccount'].'.po_head.po_number,
                                              '.$db['fsctaccount'].'.po_head.date,
                                              '.$db['fsctaccount'].'.po_head.supplier_id,
                                              '.$db['fsctaccount'].'.po_head.vat,
                                              '.$db['fsctaccount'].'.po_head.totolsumall,
                                              '.$db['fsctaccount'].'.po_head.status_head,
                                              '.$db['fsctaccount'].'.po_head.po_date_ap
                                      FROM '.$db['fsctaccount'].'.po_head ';


                                if(isset($querybr)){

                                    if(isset($status_head)){

                                        $sql .= " WHERE status_head = '$status_head' ";

                                    }else{
                                        $sql .= " WHERE status_head != '99' ";

                                    }
                                }else {
                                        $sql .= " WHERE status_head != '99' ";
                                }


                                if(isset($querybr)){
                                    if($branchselect){
                                      $sql .= " AND   branch_id = '$branchselect' ";
                                    }else{
                                      $sql .= " AND   branch_id != '9999' ";
                                    }

                                }

                                if(isset($querybr)){
                                      // echo $start_date;
                                      // echo "<br>";
                                      // echo $end_date;
                                    $sql .= " AND date BETWEEN '$start_date' AND  '$end_date' ";

                                }else{
                                    $sql .= " AND date >= '".$timeremove."' ";
                                }





                                $sql .= ' ORDER BY po_head.id DESC';
//

                                $model = DB::connection('mysql')->select($sql);

                                ?>
                                <form action="saveapprovedpo" method="post">
                                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <table class="table table-striped">
                                  <thead>
                                    <tr>
                                      <th>เลขที่เอกสาร</th>
                                      <th>วันที่ขอ</th>
                                      <th>ชื่อผู้ขาย</th>
                                      <th >รายการที่ขอ</th>
                                      <th >vat</th>
                                      <th >ราคารวม</th>
                                      <th>เปลี่ยนแปลงรายการ</th>
                                      <th>สถานะ</th>
                                      <th>รายละเอียด</th>
                                      <th>โดย</th>
                                      <th>ไม่อนุมัติ</th>
                                      <th>วันอนุมัติล่าสุด</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php
                                    $arraytranfersum = 0;
                                    $arraywaitsum = 0;
                                    $arrwaitapproved = 0;
                                    $arrpay = 0;
                                    $arrreturn = 0;
                                    foreach ($model as $key => $value) { ?>
                                    <tr>
                                      <td><?php echo $value->po_number;?></td>
                                      <td><?php echo $value->date;?></td>
                                      <td>
                                          <?php
                                              $supplier_id = $value->supplier_id;
                                              $sqlsupplier = "SELECT $db[fsctaccount].supplier.*
                                                           FROM $db[fsctaccount].supplier
                                                           WHERE $db[fsctaccount].supplier.id = '$supplier_id'";

                                              $datasup = DB::connection('mysql')->select($sqlsupplier);
                                              // echo "<pre>";
                                              echo $datasup[0]->pre.'&nbsp;&nbsp;'.$datasup[0]->name_supplier;


                                          ?>
                                      </td>

                                      <td >
                                        <input type="hidden" name="idpo<?php echo $value->id?>[]"  value="<?php echo $value->id;?>">
                                        <?php  $idpo = $value->id;
                                        $sqldetail ="SELECT $db[fsctaccount].po_detail.*
                                        FROM $db[fsctaccount].po_detail
                                        WHERE po_headid = '$idpo' AND statususe = '1' ";

                                        $modeldetail = DB::connection('mysql')->select($sqldetail);
                                            foreach ($modeldetail as $e => $l) {
                                                echo "<li>".$l->list."</li><br>";
                                                echo "<input type='hidden' name='iddetail".$idpo."[]' value='$l->id'>";
                                                echo "<input tpye='text' name='amount".$idpo."[]' id='amount,$l->id,$idpo' onblur='recalculatelast($idpo,$l->id)'  value='$l->amount' size='10'>";
                                                echo "ชิ้น";
                                                echo "   ราคา";
                                                echo "<input tpye='text' name='price".$idpo."[]'  id='price,$l->id,$idpo' onblur='recalculatelast($idpo,$l->id)' value='$l->price' size='10'>";
                                                echo " บาท";
                                                echo "<font color='red'>[".$l->note."]</font>";
                                            }

                                         ?>
                                      </td>
                                      <td>

                                        <input type="hidden" name="vatset<?php echo $idpo?>[]" id="vatset<?php echo $idpo?>" value="<?php echo $value->vat;?>">
                                        <input type="text" name="vat<?php echo $idpo?>[]" id="vat<?php echo $idpo?>"  value="<?php echo (($value->totolsumall*100)/(100+$value->vat)*($value->vat/100));?>"></td>
                                      <td>
                                        <input type="text" name="totolsumall<?php echo $idpo?>[]" id="totolsumall<?php echo $idpo ?>" value="<?php echo $value->totolsumall;?>">

                                     </td>
                                     <td>
                                         <button type="button" onclick="savechangesmoney(<?php echo $idpo?>)"  class="btn btn-warning"> เปลี่ยนแปลงข้อมูล</button>
                                     </td>
                                      <td><?php
                                        if($value->status_head==0){
                                              echo "<span>รออนุมัติ</span>";
                                                   $arrwaitapproved = $arrwaitapproved + $value->totolsumall;
                                        }else if($value->status_head==1){
                                              echo "<span class='text-info'>อนุมัติแล้ว รอโอน</span>";
                                                   $arraywaitsum = $arraywaitsum + $value->totolsumall;
                                              echo "(".$value->po_date_ap.")";
                                        }else if($value->status_head==2){
                                              echo "<span class='text-success'>โอนแล้ว</span>";
                                                   $arraytranfersum = $arraytranfersum + $value->totolsumall;
                                        }else if($value->status_head==3){
                                                $datapaybill = Vendorcenter::getpaybillemp($value->id);
                                                if($datapaybill[0]->sumpay_real != ''){
                                                    $setpayemp = $datapaybill[0]->sumpay_real;
                                                    // print_r(number_format($datapaybill[0]->sumpay_real,2));
                                                }else{
                                                    $setpayemp = 0;
                                                }
                                              $totolsumall = $value->totolsumall;
                                              $arrpay = $arrpay + $setpayemp;
                                              $arrreturn = $arrreturn + ($totolsumall-$setpayemp);
                                              $setpayemp = $setpayemp;
                                              echo "<span style='color:#4b0082;'>จ่ายแล้ว   ";
                                              echo "จำนวนเงินที่ต้องจ่าย  ".$value->totolsumall."<br>";
                                              echo "จ่ายจริง ".$setpayemp."<br>";
                                              echo "ส่วนต่าง  ".($totolsumall-$setpayemp)."<br>";
                                              echo "</span>";
                                        }else if($value->status_head==4) {
                                              $datapaybill = Vendorcenter::getpaybillemp($value->id);
                                              if($datapaybill[0]->sumpay_real != ''){
                                                  $setpayemp = $datapaybill[0]->sumpay_real;
                                                  // print_r(number_format($datapaybill[0]->sumpay_real,2));
                                              }else{
                                                  $setpayemp = 0;
                                              }
                                            $totolsumall = $value->totolsumall;
                                            $arrpay = $arrpay + $setpayemp;
                                            $arrreturn = $arrreturn + ($totolsumall-$setpayemp);
                                            $setpayemp = $setpayemp;
                                            echo "<span color='#4b0082'>มีเงินทอน</span>";
                                            echo "จำนวนเงินที่ต้องจ่าย  ".$value->totolsumall."<br>";
                                            echo "จ่ายจริง ".$setpayemp."<br>";
                                            echo "ส่วนต่าง  ".($totolsumall-$setpayemp)."<br>";
                                            echo "</span>";

                                        }else if($value->status_head==5){
                                            $datapaybill = Vendorcenter::getpaybillemp($value->id);
                                            if($datapaybill[0]->sumpay_real != ''){
                                                $setpayemp = $datapaybill[0]->sumpay_real;
                                                // print_r(number_format($datapaybill[0]->sumpay_real,2));
                                            }else{
                                                $setpayemp = 0;
                                            }
                                          $totolsumall = $value->totolsumall;
                                          $arrpay = $arrpay + $setpayemp;
                                          $arrreturn = $arrreturn + ($totolsumall-$setpayemp);
                                          $setpayemp = $setpayemp;
                                          echo "<span color='#4b0082'>ขาด</span>";
                                          echo "จำนวนเงินที่ต้องจ่าย  ".$value->totolsumall."<br>";
                                          echo "จ่ายจริง ".$setpayemp."<br>";
                                          echo "ส่วนต่าง  ".($totolsumall-$setpayemp)."<br>";
                                          echo "</span>";

                                        }else{
                                              echo "<span class='text-danger'>ยกเลิก</span>";

                                        }

                                      ?></td>
                                      <td><a href="printpo/<?php echo $value->id;?>" target="_blank"> <img src="images/global/edit-bill.png"></a></td>
                                      <td>
                                        <?php
                                        if($value->status_head==0){?>
                                           <input type="checkbox" name="approvedidpo[]"  value="<?php echo $value->id;?>">อนุมัติ
                                           <br>

                                        <?php } else if($value->status_head==1){ ?>
                                           <input type="checkbox" name="tranfermoneyidpo[]" value="<?php echo $value->id;?>">โอนเงิน
                                           <br>
                                        <?php } ?>
                                          <input type="checkbox" name="returnstatus[]" value="<?php echo $value->id;?>">แก้คืน
                                          <br>
                                      </td>
                                      <td>
                                        <input type="checkbox" name="cancelstatus[]" value="<?php echo $value->id;?>">ไม่อนุมัติ
                                        <br>
                                      </td>
                                      <td>
                                          <?php echo $value->po_date_ap;?>
                                      </td>
                                    </tr>
                                    <?php } ?>
                                  </tbody>
                                </table>

                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-3">
                                           <font color="red"> รออนุมัติ <?php echo number_format($arrwaitapproved,2);?></font>
                                    </div>
                                    <div class="col-md-3">
                                           <font color="green"> อนุมัติแล้ว รอโอน <?php echo number_format($arraywaitsum,2);?></font>
                                    </div>
                                    <div class="col-md-3">
                                           <font color="blue"> โอนแล้ว <?php echo number_format($arraytranfersum,2);?></font>
                                    </div>
                                    <div class="col-md-3">
                                           <font > จ่ายจริง <?php echo number_format($arrpay,2);?>
                                                  <br>
                                                  ส่วนต่าง <?php echo number_format($arrreturn,2);?>
                                           </font>
                                    </div>
                                </div>
                                <input type="submit" class="btn btn-primary pull-right" value="บันทึก">
                              </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')
