<?php

use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;
use  App\Api\DateTime;


?>
@include('headmenu')




<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>
<link rel="stylesheet" type="text/css" href="css/ui/jquery-ui.css">

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>

<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap2.min.js'></script>

<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">

<script type="text/javascript" src = 'js/report/reportstock.js'></script>
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet"/>  datepicker year bootstrap-->

<script src="js/code/highcharts.js"></script>
<script src="js/code/modules/exporting.js"></script>
<script src="js/code/modules/export-data.js"></script>


<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


<style>
    .ui-autocomplete-input {
        border: none;
        font-size: 14px;
        width: 225px;
        height: 24px;
        margin-bottom: 5px;
        padding-top: 2px;
        border: 1px solid #DDD !important;
        padding-top: 0px !important;
        z-index: 1511;
        position: relative;
    }
    .ui-menu .ui-menu-item a {
        font-size: 12px;
    }
    .ui-autocomplete {
        position: absolute;
        top: 0;
        left: 0;
        z-index: 1510 !important;
        float: left;
        display: none;
        min-width: 160px;
        width: 160px;
        padding: 4px 0;
        margin: 2px 0 0 0;
        list-style: none;
        background-color: #ffffff;
        border-color: #ccc;
        border-color: rgba(0, 0, 0, 0.2);
        border-style: solid;
        border-width: 1px;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding;
        background-clip: padding-box;
        *border-right-width: 2px;
        *border-bottom-width: 2px;
    }
    .ui-menu-item > a.ui-corner-all {
        display: block;
        padding: 3px 15px;
        clear: both;
        font-weight: normal;
        line-height: 18px;
        color: #555555;
        white-space: nowrap;
        text-decoration: none;
    }
    .ui-state-hover, .ui-state-active {
        color: #ffffff;
        text-decoration: none;
        background-color: #0088cc;
        border-radius: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        background-image: none;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-cog home-icon"></i>
                        <a href="#">รายงาน</a>
                    </li>
                    <li class="active">รายงานเงินสดย่อยรายวัน</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>

            <div class="box-body" style="overflow-x:auto;">
              <form action="searchreporttotalcashdaily" method="post">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-2">

                    </div>

                    <div class="col-md-3">
                        <p class="text-right">
                          ค้นหาสาขา
                        </p>
                    </div>
                    <div class="col-md-2">
                      <?php
                        $db = Connectdb::Databaseall();
                        $sql = 'SELECT '.$db['hr_base'].'.branch.*
                           FROM '.$db['hr_base'].'.branch
                           WHERE '.$db['hr_base'].'.branch.status = "1"';

                        $brcode = DB::connection('mysql')->select($sql);
                      ?>
                        <select name="branch_id" id="branch_id" class="form-control" required>
                          <option value="">เลือกสาขา</option>
                          <?php foreach ($brcode as $key => $value) { ?>
                              <option value="<?php echo $value->code_branch?>" <?php if(isset($query)){ if($branch_id==$value->code_branch){ echo "selected";} }?>><?php echo $value->name_branch?></option>
                          <?php } ?>
                        </select>
                    </div>

                    <div class="col-md-5">

                    </div>
                </div>
                <div class="row">
                    <br>
                </div>
                <div class="row">
                    <div class="col-md-2">

                    </div>
                    <div class="col-md-3">
                        <p class="text-right">
                         วันที่
                        </p>
                    </div>
                    <div class="col-md-2">
                          <input type="text" name="datepicker" id="datepicker" value="<?php if(isset($query)){ print_r($datepicker); }?>" class="form-control datepicker" required readonly>
                    </div>

                    <div class="col-md-2">

                    </div>
                    <div class="col-md-3">

                    </div>

                </div>



                <div class="row">
                    <br>
                </div>
                <!-- <div class="row">
                    <div class="col-md-2">

                    </div>
                    <div class="col-md-3">
                        <p class="text-right">
                          เลือกรายการ
                        </p>
                    </div>
                    <div class="col-md-2">
                        <select type="typeselect" name="typeselect" required class="form-control">
                            <option value="1">รายการลงเช่า</option>
                            <option value="2">ใบกำกับภาษี ย่อ/เต็ม</option>

                        </select>
                    </div>

                    <div class="col-md-2">

                    </div>
                    <div class="col-md-3">

                    </div>

                </div>
                <div class="row">
                    <br>
                </div>-->
                <div class="row">
                  <div class="col-md-5">

                  </div>
                  <div class="col-md-3">
                    <input type="submit" class="btn btn-primary" value="ค้นหา">
                    <input type="reset" class="btn btn-danger">
                  </div>


                  <div class="col-md-4">

                  </div>
                </form>

                  </div>
                  <div class="row">
                      <br>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <?php
                      if(isset($query)){

                          // echo "<pre>";
                          // print_r($data);
                          // exit;

                          // $year = $datepicker;
                          // $yearto = $datepicker2;
                          // echo "<pre>";
                          // print_r($datepicker2);

                        ?>
                        <table class="table table-striped">
                           <thead>
                             <tr>
                               <th>วันที่</th>
                               <th>ชื่อลูกค้า</th>
                               <th>ประเภท</th>
                               <th>เงินทอน</th>
                               <th>สินค้าตัดหาย</th>
                               <th>รับเงินสด</th>
                               <th>รับเงินโอน</th>
                               <th>จ่ายเงินสด</th>
                               <th>จ่ายเงินโอน</th>
                               <th>รับประกัน</th>
                               <th>จ่ายประกัน</th>
                               <th>คงเหลือ</th>
                             </tr>
                           </thead>
                           <tbody>

                           <?php

                           foreach ($data as $key => $value) {

                           ?>
                           <tr>
                              <td><?php echo ($value->startdate); ?></td>
                              <td><!--ชื่อ - นามสกุล-->
                                 <?php
                                      $modelcus = Maincenter::getdatacustomer($value->customer_id);
                                      echo ($modelcus[0]->name);
                                  ?>&nbsp
                                  <?php
                                      echo ($modelcus[0]->lastname);
                                  ?>
                              </td>
                              <td>
                                <?php
                                echo ($value->bill_rent);

                                if($value->bill_rent){
                                  echo "<br>";
                                  echo '<span style="color: green;" /><center>รับค่าเช่า</center></span>';
                                  //echo "รับค่าเช่า";
                                }
                                ?>
                              </td>
                              <td><?php  ?></td>
                              <td><?php  ?></td>
                              <td>
                                <font color="green"><?php echo ($value->total); ?></font>
                              </td>
                              <td><?php  ?></td>
                              <td><?php  ?></td>
                              <td><?php  ?></td>
                              <td><?php echo ($value->insurance_money); ?></td>
                              <td><?php  ?></td>
                              <td><?php  ?></td>

                           </tr>
                           <?php  }  ?>

                           <?php

                           foreach ($datareturn as $key1 => $value1) {

                           ?>
                           <tr>
                              <td><?php echo ($value1->startdate); ?></td>
                              <td><!--ชื่อ - นามสกุล-->
                                 <?php
                                      $modelcus = Maincenter::getdatacustomer($value1->customer_id);
                                      echo ($modelcus[0]->name);
                                  ?>&nbsp
                                  <?php
                                      echo ($modelcus[0]->lastname);
                                  ?>
                              </td>
                              <td>
                                <?php
                                echo ($value1->numberrun);

                                if($value1->numberrun){
                                  echo "<br>";
                                  echo '<span style="color: red;" /><center>คืนประกัน</center></span>';
                                  //echo "รับค่าเช่า";
                                }
                                ?>
                              </td>
                              <td><?php  ?></td>
                              <td><?php  ?></td>
                              <td>
                                <!-- <font color="red"><?php //echo ($value1->total); ?></font> -->
                              </td>
                              <td><?php  ?></td>
                              <td><?php  ?></td>
                              <td><?php  ?></td>
                              <td><?php  ?></td>
                              <td>
                                <font color="red"><?php echo ($value1->insurance_money); ?></font>
                              </td>
                              <td><?php  ?></td>

                           </tr>
                           <?php  }  ?>

                           <?php

                           foreach ($dataengine as $key2 => $value2) {

                           ?>
                           <tr>
                              <td><?php echo ($value2->startdate); ?></td>
                              <td><!--ชื่อ - นามสกุล-->
                                 <?php
                                      $modelcus = Maincenter::getdatacustomer($value2->customer_id);
                                      if($modelcus){
                                      echo ($modelcus[0]->name);
                                  ?>&nbsp
                                  <?php
                                      echo ($modelcus[0]->lastname);
                                      }
                                  ?>
                              </td>
                              <td>
                                <?php
                                echo ($value2->billengine_rent);

                                if($value2->billengine_rent){
                                  echo "<br>";
                                  echo '<span style="color: green;" /><center>รับค่าเช่า</center></span>';
                                  //echo "รับค่าเช่า";
                                }
                                ?>
                              </td>
                              <td><?php  ?></td>
                              <td><?php  ?></td>
                              <td>
                                <font color="green"><?php echo ($value2->total); ?></font>
                              </td>
                              <td><?php  ?></td>
                              <td><?php  ?></td>
                              <td><?php  ?></td>
                              <td><?php echo ($value2->insurance_money); ?></td>
                              <td><?php  ?></td>
                              <td><?php  ?></td>

                           </tr>
                           <?php  }  ?>

                           <?php

                           foreach ($datareturnengine as $key3 => $value3) {

                           ?>
                           <tr>
                              <td><?php echo ($value3->startdate); ?></td>
                              <td><!--ชื่อ - นามสกุล-->
                                 <?php
                                      $modelcus = Maincenter::getdatacustomer($value3->customer_id);
                                      echo ($modelcus[0]->name);
                                  ?>&nbsp
                                  <?php
                                      echo ($modelcus[0]->lastname);
                                  ?>
                              </td>
                              <td>
                                <?php
                                echo ($value3->numberrun);

                                if($value3->numberrun){
                                  echo "<br>";
                                  echo '<span style="color: red;" /><center>คืนประกัน</center></span>';
                                  //echo "รับค่าเช่า";
                                }
                                ?>
                              </td>
                              <td><?php  ?></td>
                              <td><?php  ?></td>
                              <td>
                                <!-- <font color="red"><?php //echo ($value3->total); ?></font> -->
                              </td>
                              <td><?php  ?></td>
                              <td><?php  ?></td>
                              <td><?php  ?></td>
                              <td><?php  ?></td>
                              <td>
                                <font color="red"><?php echo ($value3->insurance_money); ?></font>
                              </td>
                              <td><?php  ?></td>

                           </tr>
                           <?php  }  ?>


                           <div class="row">
                               <br>
                           </div>

                           <tr>
                               <td colspan="4" align="right"><b>รวม</b></td>
                               <td><?php //echo number_format($sumtotal1,2); ?></td>
                               <td><?php //echo number_format($sumtotalpay1,2); ?></td>
                               <td><?php //echo number_format($sumtotalinsurance1,2); ?></td>
                               <td><?php //echo number_format($sumtotalinsurancepay,2); ;?></td>
                               <td><?php //echo number_format($sumtotalall,2);  ?></td>
                               <td><?php //echo number_format($sumtotalpayall,2);  ?></td>
                               <td><?php //echo $countbillrentsum ;?></td>
                               <td><?php //echo $countbillreturnsum ;?></td>
                           </tr>
                           </tbody>

                        </table>

                    </div>
                  </div >


                  <div class="row">
                      <br>
                  </div>



               <?php } ?>

            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')
