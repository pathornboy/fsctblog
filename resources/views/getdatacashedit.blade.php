<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;

  $db = Connectdb::Databaseall();
  $emp_code = Session::get('emp_code');
  // $emp_code = Session::get('emp_code');

?>

@include('headmenu')

<link>

<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>
<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">

<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript" src = 'js/report/cash.js'></script>
<meta name="csrf-token" content="{{ csrf_token() }}" />

<style>
    .ui-autocomplete-input {
        border: none;
        font-size: 14px;
        width: 225px;
        height: 24px;
        margin-bottom: 5px;
        padding-top: 2px;
        border: 1px solid #DDD !important;
        padding-top: 0px !important;
        z-index: 1511;
        position: relative;
    }
    .ui-menu .ui-menu-item a {
        font-size: 12px;
    }
    .ui-autocomplete {
        position: absolute;
        top: 0;
        left: 0;
        z-index: 1510 !important;
        float: left;
        display: none;
        min-width: 160px;
        width: 160px;
        padding: 4px 0;
        margin: 2px 0 0 0;
        list-style: none;
        background-color: #ffffff;
        border-color: #ccc;
        border-color: rgba(0, 0, 0, 0.2);
        border-style: solid;
        border-width: 1px;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding;
        background-clip: padding-box;
        *border-right-width: 2px;
        *border-bottom-width: 2px;
    }
    .ui-menu-item > a.ui-corner-all {
        display: block;
        padding: 3px 15px;
        clear: both;
        font-weight: normal;
        line-height: 18px;
        color: #555555;
        white-space: nowrap;
        text-decoration: none;
    }
    .ui-state-hover, .ui-state-active {
        color: #ffffff;
        text-decoration: none;
        background-color: #0088cc;
        border-radius: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        background-image: none;
    }
    .table {

    }

</style>


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->

    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-cog home-icon"></i>
                        <a href="#">ข้อมูลเงินสด</a>
                    </li>
                    <li class="active">จัดการข้อมูลเงินตั้งต้นรายวัน</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>

            <div class="box-body" style="overflow-x:auto;">
              <form action="searchgetdatacashedit" method="post">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-2">

                    </div>

                    <div class="col-md-3">
                        <p class="text-right">
                          ค้นหาสาขา
                        </p>
                    </div>
                    <div class="col-md-2">
                      <?php
                        $db = Connectdb::Databaseall();
                        $sql = 'SELECT '.$db['hr_base'].'.branch.*
                                FROM '.$db['hr_base'].'.branch
                                WHERE '.$db['hr_base'].'.branch.status = "1"';

                        $brcode = DB::connection('mysql')->select($sql);
                      ?>
                        <select name="branch_id" id="branch_id" class="form-control" required>
                          <option value="">เลือกสาขา</option>
                          <?php foreach ($brcode as $key => $value) { ?>
                              <option value="<?php echo $value->code_branch?>" <?php if(isset($query)){ if($branch_id==$value->code_branch){ echo "selected";} }?>><?php echo $value->name_branch?></option>
                          <?php } ?>
                        </select>
                    </div>

                    <div class="col-md-5">

                    </div>
                </div>

                <div class="row">
                    <br>
                </div>

                <div class="row">
                    <div class="col-md-2">

                    </div>
                    <div class="col-md-3">
                        <p class="text-right">
                         วันที่
                        </p>
                    </div>
                    <div class="col-md-2">
                          <input type="text" name="datepicker" id="datepicker" value="<?php if(isset($query)){ print_r($datepicker); }else{ echo date('d/m/Y');}?>" class="form-control datepicker" required readonly>
                    </div>

                    <div class="col-md-2">

                    </div>
                    <div class="col-md-3">

                    </div>
                </div>

                <div class="row">
                    <br>
                </div>

                <div class="row">
                  <div class="col-md-5">

                  </div>
                  <div class="col-md-3">
                    <input type="submit" class="btn btn-primary" value="ค้นหา">
                    <input type="reset" class="btn btn-danger">
                  </div>

                  <div class="col-md-12" align="right">

                  </div>

                  <div class="col-md-4">

                  </div>

                </form>

              </div>

                  <div class="row">
                      <br>
                  </div>

                      <?php
                      if(isset($query)){
                        // echo "<pre>";
                        // print_r($data);
                        // exit;
                        ?>

                      <div class="col-md-2">
                          <a href="#" title="เพิ่มข้อมูล" data-toggle="modal" data-target="#myModal"  ><img src="images/global/add.png">เพิ่มข้อมูล</a>
                      </div>

                      <div class="row">
                          <br>
                      </div>
                      <div class="row">
                        <form action="saveeditgetdatacashedit" method="post" onSubmit="if(!confirm('ยืนยันการทำรายการ?')){return false;}">
                          <!-- <input type="hidden" name="id" value="<?php //echo $data[0]->id; ?>"> -->
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-10">

                                </div>

                                <!-- <div class="col-md-2">
                                    <a href="#" title="เพิ่มข้อมูล" data-toggle="modal" data-target="#myModal"  ><img src="images/global/add.png">เพิ่มข้อมูล</a>
                                </div> -->
                            </div>
                            <div class="row">
                                <br>
                            </div>
                          <?php
                          if($data){
                          ?>
                              <table class="table table-bordered">
                                   <tr>
                                     <td>
                                       <B>สาขา</B>
                                     </td>
                                     <td><input type="text" name="branch_id[]" value="<?php echo $data[0]->branch_id; ?>" required readonly></td>
                                     <td>
                                       <B>เงินตั้งต้น</B>
                                      </td>
                                     <td><input type="text" name="grandtotal[]" value="<?php echo $data[0]->grandtotal; ?>"></td>
                                     <td>
                                       <B>วันที่ปัจจุบัน</B>
                                    </td>
                                     <td>
                                       <input type="text" name="time[]" value="<?php echo $data[0]->time; ?>" required readonly>
                                     </td>


                                   <tr>
                                     <td colspan="10">
                                        <br>
                                     </td>
                                   </tr>
                                   <tr>
                                     <td>
                                       <B>วันที่นำเงินฝาก</B>
                                     </td>
                                     <td><input type="text"  name="timeold[]" value="<?php echo $data[0]->timeold; ?>" required readonly></td>
                                     <!-- <td>

                                     </td> -->
                                     <td colspan="2"><input type="text" name="note[]"  value="<?php echo $data[0]->note; ?>"></td>
                                     <input type="hidden" name="id[]"  value="<?php echo $data[0]->id; ?>">
                                     <td>

                                     </td>
                                     <td></td>

                                   </tr>
                                   <tr>
                                     <td colspan="10">
                                        <br>
                                     </td>


                              </table>

                          </div>
                          </div>

                          <div class="row">
                              <div class="col-md-4">

                              </div>
                              <div class="col-md-2">

                              </div>
                              <div class="col-md-2">

                              </div>
                              <div class="col-md-1"  style="float: right;">
                                <input type="submit" class="btn btn-success" value="บันทึก">
                              </div>
                          </div>
                          <?php
                            }
                          ?>

                          <!-- <div class="col-md-12">
                              <div class="row" style="float: right;">
                                <input type="submit" class="btn btn-success" value="บันทึก">
                              </div>
                          </div> -->
                        </form>
                    <?php } ?>
              </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>

@include('footer')


<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">เพิ่มเงินตั้งต้นรายวัน</h4>
        </div>
        <div class="modal-body">
        <form action="savecash" method="post" >
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

            <div class="form-group row">
              <label for="timeold" class="col-sm-2">วันที่ปัจจุบัน</label>
              <div class="col-sm-10">
                <input type="text" name="datepicker" id="datepicker" value="<?php if(isset($query)){ print_r($datepicker); }else{ echo date('d/m/Y');}?>" class="form-control datepicker" required readonly>
                <!-- <input class="form-control col-sm-10" type="text" id="timeold" name="timeold" value="" > -->
              </div>
            </div>

            <div class="form-group row">
              <label for="grandtotal" class="col-sm-2">เงินตั้งต้น</label>
              <div class="col-sm-10">
                <input class="form-control col-sm-10" type="text" id="grandtotal" name="grandtotal" value="" >
              </div>
            </div>

            <div class="row">
                <br>
            </div>

            <div class="form-group row">
              <label for="timeold" class="col-sm-2">วันที่นำเงินฝาก</label>
              <div class="col-sm-10">
                <input type="text" name="timeold" id="timeold" value="<?php if(isset($query)){ print_r($datepicker); }else{ echo date('d/m/Y');}?>" class="form-control datepicker" required readonly>
                <!-- <input class="form-control col-sm-10" type="text" id="timeold" name="timeold" value="" > -->
              </div>
            </div>

            <div class="form-group row">
              <label for="note" class="col-sm-2">ยอดที่นำฝาก</label>
              <div class="col-sm-10">
                <input class="form-control col-sm-10" type="text" id="note" name="note" value="" >
              </div>
            </div>
          </div>
          <input class="form-control col-sm-10" type="hidden" id="branch_id" name="branch_id" value="<?php if(isset($query)){echo $branch_id; }?>" >
          <input class="form-control col-sm-10" type="hidden" id="status" name="status" value="1" >

          <div class="row">
              <br>
          </div>



        <div class="modal-footer">
            <button type="submit"  class="btn btn-success">ยืนยัน</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </form>
        </div>
        </div>
      </div>

    </div>
  </div>
