<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;

 $emp_code = Session::get('emp_code');


?>
@include('headmenu')
<link>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>


<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">


<link rel="stylesheet" type="text/css" href="bower_components/select2/dist/css/select2.min.css">
<script type="text/javascript" src = 'bower_components/select2/dist/js/select2.full.min.js'></script>

<script type="text/javascript" src = 'js/vendor/inform_po.js'></script>



<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="breadcrumbs" id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="ace-icon fa fa-home home-icon"></i>
                            <a href="#">งานจัดซื้อ</a>
                        </li>
                        <li class="active">แจ้งการจ่ายเงิน</li>
                    </ul>
                    <ul class="breadcrumb">
                        <li>
                            แจ้งการจ่ายเงิน
                        </li>
                    </ul>
                            @if (session('alert'))
                                <div class="alert alert-success">
                                    {{ session('alert') }}
                                </div>
                            @endif

                    <div class="box-body">
                                <div class="row">
                                    <div class="col-md-10">

                                    </div>

                                    <div class="col-md-2">
                                        <a href="#" title="เพิ่มข้อมูล" data-toggle="modal" data-target="#myModal"><img src="images/global/add.png">เพิ่มข้อมูล</a>
                                    </div>
                    </div>

                    <table id="example" class="table table-striped table-bordered">
                    <thead class="thead-inverse">
                        <tr>
                            <td>#</td>
                            <td>เลขPo</td>
                            <td>วันที่บิล</td>
                            <td>จ่ายเงิน</td>
                            <td>เงินทอน</td>
                            <td>WHT(%)</td>
                            <td>WHT(บาท)</td>
                            <td>VAT(%)</td>
                            <td>VAT(บาท)</td>
                            <td>วิธีการจ่าย</td>
                            <td>img</td>
                            <td>แก้ไข</td>
                            <td>ลบ</td>

                        </tr>
                    </thead>
                      <?php $i=1; ?>
                    @foreach ($data_po as $value)

                    <tbody>
                        <tr>
                            <td> {{$i}} </td>
                            <td> {{$value->po_ref}} </td>
                            <td> {{$value->datebill}} </td>
                            <td> {{$value->payout}} </td>
                            <td> {{$value->change_pay}} </td>
                            <td> {{$value->wht_percent}} </td>
                            <td> {{$value->wht}} </td>
                            <td> {{$value->vat_percent}} </td>
                            <td> {{$value->vat_price}} </td>
                            <td> @if($value->type_pay==1) เงินสด  @elseif($value->type_pay==2) เงินโอน  @else error  @endif </td>
                            <td> {{$value->inform_po_picture}} </td>
                            <td>
                              <?php
                              if($emp_code=='1001' OR
                                 $emp_code=='1002' OR
                                 $emp_code=='1383' OR
                                 $emp_code=='1474' OR
                                 $emp_code=='1606' OR
                                 $emp_code=='1611' OR
                                 $emp_code=='1612'){?>

                                <button class="btn btn-primary" onclick="editmodelinform({{$value->id}})">แก้ไข</button>

                               <?php }?>
                            </td>

                            <td>
                              <?php
                               $datenow = date('Y-m-d');
                              if(($emp_code=='1001' OR
                                 $emp_code=='1002' OR
                                 $emp_code=='1383' OR
                                 $emp_code=='1474' OR
                                 $emp_code=='1606' OR
                                 $emp_code=='1611' OR
                                 $emp_code=='1612') && ($value->datebill == $datenow)){?>
                                 <a href="deleteinform/{{$value->id}}"><button class="btn btn-danger">ลบ</button></a>
                            <?php }else{ echo ""; } ?>
                            </td>

                        </tr>
                    </tbody>
                    <?php $i++; ?>
                    @endforeach

                    </div>
        </div>
                    </table>

    </section>
    <!-- /.content -->
</div>
@include('footer')


<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">เพิ่มข้อมูลการแจ้งการจ่ายเงิน</h4>
        </div>
        <div class="modal-body">
            <form action="inform_po_insert" method="post"  enctype="multipart/form-data">
            <input type="hidden" name="id_po" id="id_po" >
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

            <label><b>เลขที่ PO </b></label> <span style="color:red;">กรุณากรอกเลขที่ใบ PO</span>
            <input type="text" value="PO" id="po_ref" name="po_ref" class="form-control" required>
            <br>
            <center>
            <button type="button" class="btn btn-primary" id="serachpo" onclick="serachpothis()" name="serachpo">ค้นหาใบ PO</button>
            </center>
            <br>
            <br>

            <label class="showdetail" style="display:none;"><b>จำนวนเงินที่ขอ </b></label>
            <input type="text" readonly  id="totolsumall" name="totolsumall" style="display:none;" class="form-control showdetail" required>

            <br>
            <br>
            <label class="showdetail" style="display:none;"><b>วันที่บิล </b></label>
            <input type="text"  name="datebill" id="datebill" style="display:none;" class="form-control showdetail" required>

            <label class="showdetail" style="display:none;"><b>เลขที่บิล </b></label>
            <input type="text"  name="bill_no" style="display:none;" class="form-control showdetail" required>


            <label class="showdetail" style="display:none;"><b>VAT(%) </b></label>

            <select class="form-control  showdetail" style="display:none;" name="vat_percent" id="vat_percent" required >
                <option value="0">ไม่มี vat </option>
            @foreach ($vat_po as $value)
                <option value="{{$value->tax}}">{{$value->tax}}</option>
            @endforeach

            </select>

            <label class="showdetail" style="display:none;"><b>ยอดVATตามบิล (บาท) </b></label>
            <input type="text" placeholder="กรุณากรอกจำนวนเงิน VAT " style="display:none;" id="vat_price" name="vat_price" class="form-control showdetail" required>

            <label class="showdetail" style="display:none;"><b>หัก ณ ที่จ่าย(%) </b></label>
            <select class="form-control  showdetail" style="display:none;" name="wht_percent" id="wht_percent" required >
                <option value="0">ไม่มี หัก ณ ทีจ่าย </option>
                @foreach ($wht_po as $value)
                <option value="{{$value->withhold}}">{{$value->withhold}}</option>
                @endforeach

            </select>
            <label class="showdetail" style="display:none;">ยอดออกหนังสือ หัก ณ ที่จ่าย (บาท) <b> </b></label>
            <input type="text" placeholder="กรุณากรอก จำนวนเงินจ่าย" name="wht" id="wht" style="display:none;" class="form-control showdetail" required>

            <label class="showdetail" style="display:none;"><b>จำนวนเงินที่จ่าย </b></label>
            <input type="text" placeholder="กรุณากรอก จำนวนเงินจ่าย" name="payout" style="display:none;" class="form-control showdetail" required>

            <label class="showdetail" style="display:none;"><b>จำนวนเงินทอน </b></label>
            <input type="text" placeholder="กรุณากรอก จำนวนเงินทอน" style="display:none;" name="change_pay" class="form-control showdetail" required>

            <label class="showdetail" style="display:none;"><b>วิธีการจ่าย </b></label>
            <select class="form-control showdetail" name="type_pay" style="display:none;" required >
                <option value="1">เงินสด</option>
                <option value="2">เงินโอน</option>
            </select>

            <label class="showdetail" style="display:none;"><b>หลักฐาน </b></label>
            <input type="file"  class="form-control-file showdetail" style="display:none;" name="inform_po_picture" id="inform_po_picture" required>

        <br>
        <br>
        <table width="100%" class="table">
          <thead>
            <th>รายการ</th>
            <th>จำนวนซื้อ</th>
            <th>จำนวนรับ</th>
          </thead>
          <tbody id="showdetaillist">

          </tbody>
        </table>

        </div>
        <div class="modal-footer">
            <button type="submit" style="display:none;"  class="btn btn-success showdetail">ยืนยัน</button>
          <button type="button" style="display:none;" class="btn btn-danger showdetail" data-dismiss="modal">Close</button>
          </form>
        </div>
      </div>

    </div>
  </div>



  <!-- ModalEdit -->
  <div class="modal fade" id="myModalEdit" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">เพิ่มข้อมูลการแจ้งการจ่ายเงิน</h4>
          </div>
          <div class="modal-body">
              <form action="inform_po_edit" method="post"  enctype="multipart/form-data">
              <input type="hidden" name="id_poedit" id="id_poedit" >
              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

              <label><b>เลขที่ PO </b></label> <span style="color:red;">กรุณากรอกเลขที่ใบ PO</span>
              <input type="text" value="PO" id="po_ref_edit" name="po_ref_edit" class="form-control" required>
              <br>
              <label class="showdetail" style="display:none;"><b>จำนวนเงินที่ขอ </b></label>
              <input type="text" readonly  id="totolsumalledit" name="totolsumalledit" style="display:none;" class="form-control showdetail" required>

              <br>
              <br>
              <label class="showdetail" style="display:none;"><b>วันที่บิล </b></label>
              <input type="text"  name="datebilledit" id="datebilledit" style="display:none;" class="form-control showdetail" required>

              <label class="showdetail" style="display:none;"><b>เลขที่บิล </b></label>
              <input type="text" id="bill_noedit"  name="bill_noedit" style="display:none;" class="form-control showdetail" required>


              <label class="showdetail" style="display:none;"><b>VAT(%) </b></label>

              <select class="form-control  showdetail" style="display:none;" name="vat_percentedit" id="vat_percentedit" required >
                  <option value="0">ไม่มี vat </option>
              @foreach ($vat_po as $value)
                  <option value="{{$value->tax}}">{{$value->tax}}</option>
              @endforeach

              </select>

              <label class="showdetail" style="display:none;"><b>ยอดVATตามบิล (บาท) </b></label>
              <input type="text" placeholder="กรุณากรอกจำนวนเงิน VAT " style="display:none;" id="vat_price_edit" name="vat_price_edit" class="form-control showdetail" required>

              <label class="showdetail" style="display:none;"><b>หัก ณ ที่จ่าย(%) </b></label>
              <select class="form-control  showdetail" style="display:none;" name="wht_percent_edit" id="wht_percent_edit" required >
                  <option value="0">ไม่มี หัก ณ ทีจ่าย </option>
                  @foreach ($wht_po as $value)
                  <option value="{{$value->withhold}}">{{$value->withhold}}</option>
                  @endforeach

              </select>
              <label class="showdetail" style="display:none;">ยอดออกหนังสือ หัก ณ ที่จ่าย (บาท) <b> </b></label>
              <input type="text" placeholder="กรุณากรอก จำนวนเงินจ่าย" name="whtedit" id="whtedit" style="display:none;" class="form-control showdetail" required>

              <label class="showdetail" style="display:none;"><b>จำนวนเงินที่จ่าย </b></label>
              <input type="text" placeholder="กรุณากรอก จำนวนเงินจ่าย" name="payoutedit" id="payoutedit" style="display:none;" class="form-control showdetail" required>

              <label class="showdetail" style="display:none;"><b>จำนวนเงินทอน </b></label>
              <input type="text" placeholder="กรุณากรอก จำนวนเงินทอน" style="display:none;" id="change_payedit" name="change_payedit" class="form-control showdetail" required>

              <label class="showdetail" style="display:none;"><b>วิธีการจ่าย </b></label>
              <select class="form-control showdetail" name="type_payedit" id="type_payedit" style="display:none;" required >
                  <option value="1">เงินสด</option>
                  <option value="2">เงินโอน</option>
              </select>

              <label class="showdetail" style="display:none;"><b>หลักฐาน </b></label>
              <input type="file"  class="form-control-file showdetail" style="display:none;" name="inform_po_pictureedit" id="inform_po_pictureedit" required>

          <br>
          <br>
          <table width="100%" class="table">
            <thead>
              <th>รายการ</th>
              <th>จำนวนซื้อ</th>
              <th>จำนวนรับ</th>
            </thead>
            <tbody id="showdetaillistedit">

            </tbody>
          </table>

          </div>
          <div class="modal-footer">
              <button type="submit" style="display:none;"  class="btn btn-success showdetail">ยืนยัน</button>
            <button type="button" style="display:none;" class="btn btn-danger showdetail" data-dismiss="modal">Close</button>
            </form>
          </div>
        </div>

      </div>
    </div>
