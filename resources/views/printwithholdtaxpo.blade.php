<?php

use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;

?>
@include('headmenu')




<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>
<link rel="stylesheet" type="text/css" href="css/ui/jquery-ui.css">

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>

<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap2.min.js'></script>


<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">


<script type="text/javascript" src = 'js/report/printPO.js'></script>
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>


<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


<style>
    .ui-autocomplete-input {
        border: none;
        font-size: 14px;
        width: 225px;
        height: 24px;
        margin-bottom: 5px;
        padding-top: 2px;
        border: 1px solid #DDD !important;
        padding-top: 0px !important;
        z-index: 1511;
        position: relative;
    }
    .ui-menu .ui-menu-item a {
        font-size: 12px;
    }
    .ui-autocomplete {
        position: absolute;
        top: 0;
        left: 0;
        z-index: 1510 !important;
        float: left;
        display: none;
        min-width: 160px;
        width: 160px;
        padding: 4px 0;
        margin: 2px 0 0 0;
        list-style: none;
        background-color: #ffffff;
        border-color: #ccc;
        border-color: rgba(0, 0, 0, 0.2);
        border-style: solid;
        border-width: 1px;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding;
        background-clip: padding-box;
        *border-right-width: 2px;
        *border-bottom-width: 2px;
    }
    .ui-menu-item > a.ui-corner-all {
        display: block;
        padding: 3px 15px;
        clear: both;
        font-weight: normal;
        line-height: 18px;
        color: #555555;
        white-space: nowrap;
        text-decoration: none;
    }
    .ui-state-hover, .ui-state-active {
        color: #ffffff;
        text-decoration: none;
        background-color: #0088cc;
        border-radius: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        background-image: none;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-cog home-icon"></i>
                        <a href="#">รายงาน</a>
                    </li>
                    <li class="active">พิมพ์เอกสาร ภพ 30</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>

            <div class="box-body" style="overflow-x:auto;">
              <form action="serachprintwithholdtaxpo" method="post">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-2">

                    </div>

                    <div class="col-md-3">
                        <p class="text-right">
                          ค้นหาสาขา
                        </p>
                    </div>
                    <div class="col-md-2">
                      <?php
                        $db = Connectdb::Databaseall();
                        $sql = 'SELECT '.$db['hr_base'].'.branch.*
                           FROM '.$db['hr_base'].'.branch
                           WHERE '.$db['hr_base'].'.branch.status = "1"';

                        $brcode = DB::connection('mysql')->select($sql);
                      ?>
                        <select name="branch_id" id="branch_id" class="form-control" required>
                          <option value="">เลือกสาขา</option>
                          <?php foreach ($brcode as $key => $value) { ?>
                              <option value="<?php echo $value->code_branch?>" <?php if(isset($query)){ if($branch_id==$value->code_branch){ echo "selected";} }?>><?php echo $value->name_branch?></option>
                          <?php } ?>
                        </select>
                    </div>

                    <div class="col-md-5">

                    </div>
                </div>
                <div class="row">
                    <br>
                </div>
                <div class="row">
                    <div class="col-md-2">

                    </div>
                    <div class="col-md-3">
                        <p class="text-right">
                          วันที่
                        </p>
                    </div>
                    <div class="col-md-2">
                        <input type="text" name="datepicker" id="reservation" value="<?php if(isset($query)){ print_r($datepicker); }?>" class="form-control " required readonly>
                    </div>

                    <div class="col-md-2">

                    </div>
                    <div class="col-md-3">

                    </div>

                </div>
                <div class="row">
                    <br>
                </div>

                  <div class="col-md-5">

                  </div>
                  <div class="col-md-3">
                    <input type="submit" class="btn btn-primary" value="ค้นหา">
                    <input type="reset" class="btn btn-danger">
                  </div>


                  <div class="col-md-4">

                  </div>
                </form>

                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <?php
                      if(isset($query)){
                          echo "<pre>";
                          print_r($data);
                          exit;
                        ?>
                        <table class="table table-striped">
                           <thead>
                             <tr>
                               <th>ลำดับ</th>
                             </tr>
                           </thead>
                           <tbody>
                           <?php
                                $i = 1;

                                foreach ($data as $key => $value) {
                           ?>
                             <tr>
                               <td><?php echo $i; ?></td><!--ลำดับ-->
                             </tr>
                           <?php $i++; }  ?>
                          </tbody>
                        </table>


                    </div>
                  </div >

                  <div class="row">
                      <br>
                  </div>
               <?php } ?>

            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')
