<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }

        body {
            font-family: "THSarabunNew";
        }
        h1 {
            font-family: "THSarabunNew";
        }
    </style>
</head>
<body>
ใบแจ้งหนี้สำหรับ {{ $name }}
ขอขอบคุณในการสั่งซื้อ

</body>
</html>