<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;
use  App\Api\DateTime;

?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        @font-face {
            font-family: 'TH Niramit AS';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/TH Niramit AS.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'TH Niramit AS';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/TH Niramit AS Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'TH Niramit AS';
            font-style: italic;
            font-weight: normal;
            src: url("{{ public_path('fonts/TH Niramit AS Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'TH Niramit AS';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/TH Niramit AS Bold Italic.ttf') }}") format('truetype');
        }

        body {
            font-family: "TH Niramit AS";
            font-size: 14.90pt;

        }
        h4 {
            font-family: "TH Niramit AS";
        }
        h4 {
            font-family: "TH Niramit AS";
        }


        .container table {
        border-collapse: collapse;
        border: solid 1px #000;
        }
        .container table td {
        border: solid 1px #000;
        }
        .no-border-left-and-bottom {
        border-left: solid 1px #FFF!important;
        border-bottom: solid 1px #FFF!important;
        }
        .no-border-left-and-bottom-and-top {
        border-left: solid 1px #FFF!important;
        border-bottom: solid 1px #FFF!important;
        border-top: solid 1px #FFF!important;
        }
        .no-border-bottom-and-top {
        border-bottom: solid 1px #FFF!important;
        border-top: solid 1px #FFF!important;
        }
        .no-top{
        border-top: solid 1px #FFF!important;
        }
        .text-right{
          text-align: right;
        }
        .text-left{
          text-align: left;
        }
        .text-center{
          text-align: center;
        }
        .text-bold{
          font-weight: bold;
        }
        .text-default{
          font-weight: normal;
        }
        .border-1px{
          border: 1px solid black;
        }
        .border-dot{
          border-bottom: 1px dotted black;
        }
        .border-solid{
          border-bottom: 1px solid black;
        }
        .text-top{
          vertical-align: top;
        }
        .table{
          width: 100%;
        }
    </style>
</head>
<body style="margin: 0;">
    <?php

        $db = Connectdb::Databaseall();
        $sql = "SELECT * FROM $db[fsctaccount].withholdtaxpo  WHERE withholdtaxpo.id ='$id' ";
        $datahead = DB::connection('mysql')->select($sql);

        $idpo = ($datahead[0]->id_po);

        $sqlpo = "SELECT * FROM $db[fsctaccount].po_head  WHERE po_head.id ='$idpo' ";
        $datapo = DB::connection('mysql')->select($sqlpo);


        $id_company = $datapo[0]->id_company;

        $sqlcompany = "SELECT * FROM $db[hr_base].working_company  WHERE working_company.id ='$id_company' ";
        $datacompany = DB::connection('mysql')->select($sqlcompany);

        $branch_id = $datapo[0]->branch_id;

        $sqlbranch = "SELECT * FROM $db[hr_base].branch  WHERE branch.code_branch ='$branch_id' ";
        $databranch = DB::connection('mysql')->select($sqlbranch);


        $id_supplier = ($datapo[0]->supplier_id);
        $sqlsupplier = "SELECT * FROM $db[fsctaccount].supplier  WHERE supplier.id ='$id_supplier' ";
        $datasupplier = DB::connection('mysql')->select($sqlsupplier);


        $presupplier = $datasupplier[0]->pre;

        $sqlpre = "SELECT * FROM $db[fsctaccount].initial  WHERE initial.per ='$presupplier' ";
        $datapresupplier = DB::connection('mysql')->select($sqlpre);

        // echo "<pre>";
        // print_r($datapresupplier);
        $statusperson = $datapresupplier[0]->statusperson;

    ?>
<!-- margin-top: -1.5em; -->
    <p style="margin-top: -2.38em; margin-left: 2.50em;">
        ฉบับที่ 1 (สำหรับผู้ถูกหักภาษี ณ ที่จ่าย ใช้แนบพร้อมกับแบบแสดงรายการภาษี)
    </p>
    <p style="margin-top: -1.5em;  margin-left: 2.50em;">
        ฉบับที่ 2 (สำหรับผู้ถูกหักภาษี ณ ที่จ่าย เก็บไว้เป็นหลักฐาน)
    </p>

    <table style="border: 1px solid black; width: 90%; text-align: center; margin-top: -12px ;" align="center" border="0">
        <!-- <thead>


        </thead> -->
        <tbody>
          <tr>
              <td colspan="4">
                  <p style="font-weight: bold; font-size: 20pt; margin-top: -1.2em; margin-bottom: -3em;">หนังสือรับรองการหักภาษี ณ ที่จ่าย</p>
                  <p style="margin-top: -5em; margin-bottom: -1.8em; font-size: 14pt;">ตามมาตรา 50 ทวิ แห่งประมวลรัษฎากร</p>
              </td>
              <td style="width: 20%;">
                  <table width="100%" border="0" style="margin-top: -0.8em;">
                      <thead>
                        <tbody>
                          <tr>
                            <td width="40%" class="text-right"><span style="font-size: 12pt; margin-top: 4em;">เล่มที่</span></td>
                            <td width="40%" class="text-center" style="border-bottom: 1px dotted black; margin-top: -2em;"><?php echo $datahead[0]->idhead;?></td>
                            <td width="20%"></td>
                          </tr>

                          <tr>
                            <td width="40%" class="text-right"><span style="font-size: 12pt;">เลขที่</span></td>
                            <td width="40%" class="text-center" style="border-bottom: 1px dotted black;"><?php echo str_pad($datahead[0]->idhead, 3, "0", STR_PAD_LEFT);?></td>
                            <td width="20%"></td>
                          </tr>
                        </tbody>
                      </thead>
                  </table>
              </td>
          </tr>
          <tr>
            <td colspan="5">
              <table width="100%" style="border: 1px solid black;" border="0">
                <tbody>
                  <tr style="margin-top: -1.5em;">
                    <td colspan="4" class="text-bold" ><span style="font-size: 15pt;:">ผู้มีหน้าที่หักภาษี ณ ที่จ่าย :</span></td>
                    <td width="20%" class="text-bold text-center">เลขประจำตัวผู้เสียภาษีอากร</td>
                  </tr>

                  <tr>
                    <td colspan="4">
                      <table width="100%" style="margin-top: -0.5em;">
                        <tbody>
                          <tr>
                            <td class="text-center text-bold" width="5%">ชี่อ </td>
                            <td width="75%" class="border-solid text-bold"><span style="font-size: 15pt;"><?php echo $datacompany[0]->name;?></span></td>
                            <td width="20%"></td>
                          </tr>
                          <tr>
                            <td colspan="3" class="text-default">&nbsp;&nbsp;&nbsp;<span style="font-size:12pt;">(ให้ระบุว่าเป็นบุคคล นิติบุคคล บริษัท สมาคม หรือคณะนิติบุคคล)</span></td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                    <td class="text-center text-top">
                      <div class="border-1px text-center text-top" >
                          <span style="font-size: 15pt;"><?php echo $datacompany[0]->inv_number;?></span>
                      </div>
                    </td>
                  </tr>

                  <tr>
                    <td colspan="4" class="text-bold">
                      <table width="100%" style="margin-top: -1em;">
                        <tbody>
                          <tr>
                            <td class="text-center text-bold" width="5%">ที่อยู่ </td>
                            <td width="75%" class="border-solid"> <?php echo $databranch[0]->addresstax;?></td>
                            <td width="20%"></td>
                          </tr>
                          <tr>
                            <td colspan="3" class="text-default">&nbsp;&nbsp;&nbsp;<span style="font-size:12pt;">(ให้ระบุชื่ออาคารหมู่บ้าน ห้องเลขที่ ชั้นที่ เลขที่ ตรอก/ซอย หมู่ที่ ถนน ตำบล/แขวง อำเภอ/เขต จังหวัด)</span></td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>

          <tr>
            <td colspan="5">
              <table width="100%" style="border: 1px solid black;" border="0">
                <tbody>
                  <tr>
                    <td colspan="4" class="text-bold">ผู้ถูกหักภาษี ณ ที่จ่าย :</td>
                    <td width="20%" class="text-bold text-center">เลขประจำตัวผู้เสียภาษีอากร</td>
                  </tr>

                  <tr>
                    <td colspan="4">
                      <table width="100%" style="margin-top: -0.5em;">
                        <tbody>
                          <tr>
                            <td class="text-center text-bold" width="5%">ชี่อ </td>
                            <td width="75%" class="border-solid text-bold"> <span style="font-size: 15pt;"><?php echo $datasupplier[0]->pre.'  '.$datasupplier[0]->name_supplier;?></span></td>
                            <td width="20%"></td>
                          </tr>
                          <tr>
                            <td colspan="3" class="text-default">&nbsp;&nbsp;&nbsp;<span style="font-size:12pt;">(ให้ระบุว่าเป็นบุคคล นิติบุคคล บริษัท สมาคม หรือคณะนิติบุคคล)</span></td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                    <td class="text-center text-top">
                      <div class="border-1px text-center text-top" >
                          <span style="font-size: 15pt;"><?php echo $datasupplier[0]->tax_id;?></span>
                      </div>
                    </td>
                  </tr>

                  <tr>
                    <td colspan="4" class="text-bold">
                      <table width="100%" style="margin-top: -1em;">
                        <tbody>
                          <tr>
                            <td class="text-center text-bold" width="5%">ที่อยู่ </td>
                            <td width="75%" class="border-solid">   <span style="font-size: 15pt;"><?php echo $datasupplier[0]->address.'   ตำบล '.$datasupplier[0]->district.'  อำเภอ '.$datasupplier[0]->amphur.'  จังหวัด '.$datasupplier[0]->province.'   '.$datasupplier[0]->zipcode;;?></span></td>
                            <td width="20%"></td>
                          </tr>
                          <tr>
                            <td colspan="3" class="text-default">&nbsp;&nbsp;&nbsp;<span style="font-size:12pt;"> (ให้ระบุชื่ออาคารหมู่บ้าน ห้องเลขที่ ชั้นที่ เลขที่ ตรอก/ซอย หมู่ที่ ถนน ตำบล/แขวง อำเภอ/เขต จังหวัด)</td>
                          </tr>
                        </tbody>
                        <tfoot>
                          <tr>
                            <td style="height: 4.5em;"></td>
                          </tr>
                        </tfoot>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>

          <tr>
            <td colspan="5">
              <table width="100%" border="0" style="float: left; margin-top: -5.5em;">
                <tr>
                  <td width="20%">
                    <table width="100%">
                      <tbody>
                        <tr>
                          <td class="text-center">ลำดับที่ *</td>
                          <td width="35%" class="text-center text-top border-1px"> 1 </td>
                          <td class="text-center">ในแบบ</td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                  <td><input style="margin-top: 8px; margin-right: 3px; margin-bottom: -17px;" type="checkbox" />(1) ภ.ง.ด.1ก</td>
                  <td><input style="margin-top: 8px; margin-right: 3px; margin-bottom: -17px;" type="checkbox" />(2) ภ.ง.ด.1ก พิเศษ</td>
                  <td><input style="margin-top: 8px; margin-right: 3px; margin-bottom: -17px;" type="checkbox" />(3) ภ.ง.ด.2</td>
                  <td><input style="margin-top: 8px; margin-right: 3px; margin-bottom: -17px;" type="checkbox" <?php if($statusperson==0){ echo 'checked';} ?> />(4) ภ.ง.ด.3</td>
                  <td></td>
                </tr>

                <tr>
                  <td width="20%">
                    <table width="100%">
                      <tbody>
                        <tr>
                          <td colspan="3"></td>

                        </tr>
                      </tbody>
                    </table>
                  </td>
                  <td><input style="margin-top: 8px; margin-right: 3px; margin-bottom: -17px;" type="checkbox" />(5) ภ.ง.ด.2ก</td>
                  <td><input style="margin-top: 8px; margin-right: 3px; margin-bottom: -17px;" type="checkbox" />(6) ภ.ง.ด.3ก</td>
                  <td><input style="margin-top: 8px; margin-right: 3px; margin-bottom: -17px;" type="checkbox" <?php if($statusperson==1){ echo 'checked';} ?> />(7) ภ.ง.ด.53</td>
                  <td></td>
                  <td></td>
                </tr>
              </table>
            </td>
          </tr>

          <tr>
            <td colspan="5">
              <table width="100%" border="0" class="text-center border-1px" style="border-collapse: collapse;">
                <thead>
                  <tr>
                    <td class="border-1px">ประเภทเงินได้ที่จ่าย</td>
                    <td class="border-1px">วัน เดือน ปี <br />ที่จ่ายเงิน</td>
                    <td class="border-1px" colspan="2">จำนวนเงิน <br />ที่จ่าย</td>

                    <td class="border-1px" colspan="2">ภาษี <br />หัก ณ ที่จ่าย</td>

                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td width="55%" class="text-left" style="border-right: 1px solid black;">1. เงินเดือน ค่าจ้าง เบี้ยเลี้ยง โบนัส ฯลฯ ตามมาตรา 40 (1)</td>
                    <td width="13%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                  </tr>

                  <tr>
                    <td width="55%" class="text-left" style="border-right: 1px solid black;">2. ค่าธรรมเนียม ค่านายหน้า ฯลฯ ตามมาตรา 40 (2)</td>
                    <td width="13%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                  </tr>

                  <tr>
                    <td width="55%" class="text-left" style="border-right: 1px solid black;">3. ค่าแห่งลิขสิทธิ์ ฯลฯ ตามมาตรา 40 (3)</td>
                    <td width="13%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                  </tr>

                  <tr>
                    <td width="55%" class="text-left" style="border-right: 1px solid black;">4. (ก) ค่าดอกเบี้ย ฯลฯ ตามมาตรา 40(4) (ก)</td>
                    <td width="13%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                  </tr>

                  <tr>
                    <td width="55%" class="text-left" style="border-right: 1px solid black;">&nbsp;&nbsp;&nbsp; (ข) เงินปันผล เงินส่วนแบ่งกำไร ฯลฯ ตามมาตรา 40 (4) (ข) </td>
                    <td width="13%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                  </tr>

                  <tr>
                    <td width="55%" class="text-left" style="border-right: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (1) กิจการที่ต้องเสียภาษีเงินได้นิติบุคคลในอัตราต่อไปนี้ </td>
                    <td width="13%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                  </tr>

                  <tr>
                    <td width="55%" class="text-left" style="border-right: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" style="margin-bottom: -15px; margin-top: 7px;"> (1.1) อัตราร้อยละ 30 ของกำไรสุทธิ </td>
                    <td width="13%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                  </tr>

                  <tr>
                    <td width="55%" class="text-left" style="border-right: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" style="margin-bottom: -15px; margin-top: 7px;"> (1.2) อัตราร้อยละ 25 ของกำไรสุทธิ </td>
                    <td width="13%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                  </tr>

                  <tr>
                    <td width="55%" class="text-left" style="border-right: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" style="margin-bottom: -15px; margin-top: 7px;"> (1.3) อัตราร้อยละ 20 ของกำไรสุทธิ </td>
                    <td width="13%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                  </tr>

                  <tr>
                    <td width="55%" class="text-left" style="border-right: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" style="margin-bottom: -15px; margin-top: 7px;"> (1.4) อัตราอื่น ๆ ระบุ _____________ ของกำไรสุทธิ </td>
                    <td width="13%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                  </tr>

                  <tr>
                    <td width="55%" class="text-left" style="border-right: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (2) กิจการที่ได้รับยกเว้นภาษีเงินได้นิติบุคคล ซึ่งผู้รับเงินปันผลไม่ได้รับเครดิตภาษี</td>
                    <td width="13%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                  </tr>

                  <tr>
                    <td width="55%" class="text-left" style="border-right: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (3) กำไรเฉพาะส่วนที่ได้รับยกเว้นไม่ต้องนำมารวมคำนวณภาษีเงินได้นิติบุคคล</td>
                    <td width="13%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                  </tr>

                  <tr>
                    <td width="55%" class="text-left" style="border-right: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (3) กำไรเฉพาะส่วนที่ได้รับยกเว้นไม่ต้องนำมารวมคำนวณภาษีเงินได้นิติบุคคล</td>
                    <td width="13%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                  </tr>

                  <tr>
                    <td width="55%" class="text-left" style="border-right: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ซึ่งผู้รับเงินปันผลไม่ได้รับเครดิตภาษี</td>
                    <td width="13%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                  </tr>

                  <tr>
                    <td width="55%" class="text-left" style="border-right: 1px solid black;">5. การชำระเงินได้ที่ต้องหักภาษี ณ ที่จ่าย ตามคำสั่งกรมสรรพากรที่ออกตามมาตรา</td>
                    <td width="13%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                  </tr>

                  <tr>
                    <td width="55%" class="text-left" style="border-right: 1px solid black;">&nbsp;&nbsp; 3 เตรส เช่น รางวัล ส่วนลดหรือประโยชน์ใด ๆ เนื่องจากการส่งเสริมการขาย</td>
                    <td width="13%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                  </tr>

                  <tr>
                    <td width="55%" class="text-left" style="border-right: 1px solid black;">&nbsp;&nbsp; รางวัลในการประกวด การแข่งขัน การชิงโชค คำแสดงของนักแสดงสาธารณะ </td>
                    <td width="13%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                  </tr>

                  <tr>
                    <td width="55%" class="text-left" style="border-right: 1px solid black;">&nbsp;&nbsp; ค่าจ้างทำของ ค่าโฆษณา ค่าเช่า ค่าขนส่ง ค่าบริการ ค่าเบี้ยประกันวินาศภัย ฯลฯ</td>
                    <td width="13%" class="border-dot" style="border-right: 1px solid black;">
                        <?php
                        $dateen = explode('-',$datahead[0]->date);
                        // print_r($dateen);

                        $intMonth=(int)$dateen[1];
                        $arrMonth = ['1'=>'ม.ค.',
                                     '2'=>'ก.พ.',
                                     '3'=>'มี.ค.',
                                     '4'=>'เม.ย.',
                                     '5'=>'พ.ค.',
                                     '6'=>'มิ.ย.',
                                     '7'=>'ก.ค.',
                                     '8'=>'ส.ค.',
                                     '9'=>'ก.ย.',
                                     '10'=>'ต.ค.',
                                     '11'=>'พ.ย.',
                                     '12'=>'ธ.ค.'];



                        $dd = $dateen[2];
                        $mm = $arrMonth[$intMonth];
                        $yy = $dateen[0]+543;


                                print_r($dd.' '.$mm.'  '.$yy);
                        ?>
                    </td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"><?php echo number_format($datahead[0]->beforevat,2); ?></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"><?php  echo number_format($datahead[0]->total,2); ?></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                  </tr>

                  <tr>
                    <td width="55%" class="text-left" style="border-bottom: 1px solid black; border-right: 1px solid black;">6. อื่น ๆระบุ ____________________________________________</td>
                    <td width="13%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-dot" style="border-right: 1px solid black;"></td>
                    <td width="3%" class="border-dot" style="border-right: 1px solid black;"></td>
                  </tr>

                  <tr>
                    <td width="55%" class="text-right">รวมเงินที่จ่ายและภาษีที่หักนำส่ง</td>
                    <td width="13%" class="" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-solid" style="border-right: 1px solid black;"><?php echo number_format($datahead[0]->beforevat,2);  ?></td>
                    <td width="3%" class="border-solid" style="border-right: 1px solid black;"></td>
                    <td width="12%" class="border-solid" style="border-right: 1px solid black;"><?php echo number_format($datahead[0]->total,2); ?></td>
                    <td width="3%" class="border-solid" style="border-right: 1px solid black;"></td>
                  </tr>

                  <tr>
                    <td colspan="6" class="text-left">รวมเงินนำส่ง (ตัวอักษร)  (<?php echo Accountcenter::converttobath(number_format($datahead[0]->total,2)); ?>) </td>
                  </tr>


                </tbody>
              </table>
            </td>
          </tr>

          <tr>
            <td colspan="5">
              <table width="100%" border="0">
                <thead>
                  <tr>
                    <td width="10%">ค่าประกันสังคม &nbsp;&nbsp;ปี</td>
                    <td width="10%" class="border-dot"></td>
                    <td width="10%">จำนวนเงิน</td>
                    <td width="10%"></td>
                    <td width="10%">บาท</td>
                    <td width="30%"></td>

                  </tr>
                </thead>
              </table>
            </td>
          </tr>

          <tr>
            <td colspan="5">
              <table width="100%" class="table border-1px">
                <thead>
                  <tr>
                    <td width="20%" class="text-center">ผู้จ่ายเงิน </td>
                    <td width="20%" class="text-left">
                      <input style="margin-top: 8.5px; margin-bottom: -5px;" type="checkbox">
                       (1) ออกภาษีให้ครั้งเดียว
                    </td>
                    <td width="20%" class="text-left">
                      <input style="margin-top: 8.5px; margin-bottom: -5px;" type="checkbox">
                       (2) ออกภาษีให้ตลอดไป
                    </td>
                    <td width="20%" class="text-left">
                      <input style="margin-top: 8.5px; margin-bottom: -5px;" type="checkbox">
                       (3) หักภาษี ณ ที่จ่าย
                    </td>
                    <td width="20%" class="text-left">
                      <input style="margin-top: 8.5px; margin-bottom: -5px;" type="checkbox">
                       (4) อื่น ๆ (ระบุ)…….
                    </td>
                  </tr>
                </thead>
              </table>
            </td>
          </tr>

          <tr>
            <td colspan="5">
              <table width="100%" class="table border-1px">
                <thead>
                  <tr>
                    <td width="80%" class="text-center">
                      <center>
                      ขอรับรองว่าข้อความและตัวเลขดังกล่าวข้างต้นถูกต้องตรงกับความจริงทุกประการ <br>
                      ลงชื่อ ……………………………….…………………..     ผู้มีหน้าที่หักภาษี ณ ที่จ่าย <br>
                      <span style="margin-left:-6em"  class="border-dot" ><?php echo $dd; ?> /  <?php echo $mm; ?>  /  <?php echo $yy; ?></center></span> <br>
                    </td>
                    <td width="20%" class="text-left">
                      <img src="images/company/niti.png" >
                    </td>
                  </tr>
                </thead>
              </table>
            </td>
          </tr>
        </tbody>

        <tfoot>
          <tr>
            <td colspan="5" class="text-left">
              หมายเหตุ * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ให้สามารถอ้างอิงหรือสอบยันกันได้ระหว่างลำดับที่ตามหนังสือรับรองฯ ถ้าแบบยื่นรายการภาษีหัก ณ ที่จ่าย <br>
              คำเตือน    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ผู้มีหน้าที่ออกหนังสือรับรองการหักภาษี ณ ที่จ่าย ฝ่าฝืนไม่ปฏิบัติตามมาตรา 50 ทวิ แห่งประมวลรัษฎากร ต้องรับโทษทาง <br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;อาญา ตามมาตรา 35 แห่งประมวลรัษฎากร
            </td>
          </tr>
        </tfoot>
    </table>




</body>
</html>
<?php //exit; ?>
