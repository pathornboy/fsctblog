<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;
use  App\Api\Rent;

  $level_emp = Session::get('level_emp');
  $emp_code = Session::get('emp_code');
  $brcode = Session::get('brcode');


  // exit('5000');
?>
@include('headmenu')

<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>



<link rel="stylesheet" type="text/css" href="css/ui/jquery-ui.css">

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>


<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">


<link rel="stylesheet" type="text/css" href="bower_components/select2/dist/css/select2.min.css">
<script type="text/javascript" src = 'bower_components/select2/dist/js/select2.full.min.js'></script>

<link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
<script type="text/javascript" src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src = 'js/jquery.mask.js'></script>

<script type="text/javascript" src = 'js/quotation/quotation.js'></script>
<script type="text/javascript" src = 'js/quotation/quotationoff.js'></script>

<script type="text/javascript" src = 'https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js'></script>


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">


<style>
    .ui-autocomplete-input {
        border: none;
        font-size: 14px;
        width: 225px;
        height: 24px;
        margin-bottom: 5px;
        padding-top: 2px;
        border: 1px solid #DDD !important;
        padding-top: 0px !important;
        z-index: 1511;
        position: relative;
    }
    .ui-menu .ui-menu-item a {
        font-size: 12px;
    }
    .ui-autocomplete {
        position: absolute;
        top: 0;
        left: 0;
        z-index: 1510 !important;
        float: left;
        display: none;
        min-width: 160px;
        width: 160px;
        padding: 4px 0;
        margin: 2px 0 0 0;
        list-style: none;
        background-color: #ffffff;
        border-color: #ccc;
        border-color: rgba(0, 0, 0, 0.2);
        border-style: solid;
        border-width: 1px;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding;
        background-clip: padding-box;
        *border-right-width: 2px;
        *border-bottom-width: 2px;
    }
    .ui-menu-item > a.ui-corner-all {
        display: block;
        padding: 3px 15px;
        clear: both;
        font-weight: normal;
        line-height: 18px;
        color: #555555;
        white-space: nowrap;
        text-decoration: none;
    }
    .ui-state-hover, .ui-state-active {
        color: #ffffff;
        text-decoration: none;
        background-color: #0088cc;
        border-radius: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        background-image: none;
    }
     .ps3{
      padding-top: 20px; border-style: solid; border-color: coral;
    }


</style>



<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <input type="hidden" id="dateset" value="<?php echo date('Y-m-d H:i:s')?>">
    <input type="hidden" id="datea" value="<?php echo date('Y-m-d')?>">
    <input type="hidden" id="dateb" value="<?php echo date('d-m-Y')?>">
    <input type="hidden"  class="form-control"  id="yearset" value="<?php echo date('Y')?>"  />
    <input type="hidden"  class="form-control" id="year_thset" value="<?php echo date('Y',strtotime("+543 year"))?>"  />
    <input type="hidden"  class="form-control" id="dateshowset" value="<?php $yearnow = date('Y')+543 ;echo date('d-m').'-'.$yearnow;?>"  disabled/>


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">งานการตลาด/เช่าสินค้า</a>
                    </li>
                    <li class="active">ข้อมูลการใบเสนอราคา</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">
              <!--   ข้อมูลการใบเสนอราคา   -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        ข้อมูลการใบเสนอราคา
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body">
                              <form action="searchqt" method="post">
                                <div class="row">
                                    <div class="col-md-1">
                                        <center>ตั้งแต่วันที่</center>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" name="dateqtstart" id="dateqtstart" class="form-control" value="<?php if(isset($query)){ echo $datestart;}?>" readonly required>
                                    </div>
                                    <div class="col-md-1">
                                        <center>ถึงวันที่</center>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" name="dateqtend" id="dateqtend" class="form-control"  value="<?php if(isset($query)){ echo $dateend;}?>"  readonly required>
                                    </div>
                                    <div class="col-md-2">
                                        <center>สถานะ</center>
                                    </div>
                                    <div class="col-md-2">
                                      <?php $arrqtstatus = [0=>'รออนุมัติใบเสนอราคา',
                                                            1=>'อนุมัติแล้ว',
                                                            2=>'ลงเช่าแล้ว',
                                                            3=>'อยู่ระหว่างการเช่า',
                                                            4=>'คืนของแล้ว',
                                                            99=>'ยกเลิก'];
                                      ?>
                                        <select name="statusqt"  id="statusqt" class="form-control" required>
                                            <option value="">สถานะใบ QT</option>
                                            <?php foreach ($arrqtstatus as $k => $vs ) {?>
                                                <option value="<?php echo $k;?>" <?php if(isset($query) && $statuqt==$k){ echo "selected" ;}?> > <?php echo $vs;?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                    <div class="col-md-1">
                                        <input type="submit" value="ค้นหา" class="btn btn-primary">
                                    </div>

                                    <div class="col-md-1">
                                        <a href="#" title="เพิ่มข้อมูล" data-toggle="modal" data-target="#myModal" onclick="insertnew()" ><img src="images/global/add.png">เพิ่มข้อมูล</a>
                                    </div>
                                </div>
                              </form>

                                <div class="row">
                                    <br>
                                </div>
                                    <?php
                                    $brcode = Session::get('brcode');
                                    $db = Connectdb::Databaseall();



                                    if(isset($query)){

                                      $sql = 'SELECT * FROM '.$db['fsctmain'].'.quotation_head';
                                      if($level_emp == '1' || $level_emp== '2'){
                                            $sql.=' WHERE '.$db['fsctmain'].'.quotation_head.status = "'.$statuqt.'"
                                                    AND '.$db['fsctmain'].'.quotation_head.date_req BETWEEN "'.$datestartset.'" AND "'.$dateendset.'" ';
                                      }else{
                                          // echo $datestartset;
                                          // echo "<br>";
                                          // echo $dateendset;

                                            $sql.=' WHERE '.$db['fsctmain'].'.quotation_head.branch_id = "'.$brcode.'"
                                                    AND '.$db['fsctmain'].'.quotation_head.status  = "'.$statuqt.'"
                                                    AND '.$db['fsctmain'].'.quotation_head.date_req BETWEEN "'.$datestartset.'" AND "'.$dateendset.'" ';

                                      }
                                            $sql.='  ORDER BY id DESC  ';

                                        $data = DB::connection('mysql')->select($sql);

                                    }else{

                                      $sql = 'SELECT * FROM '.$db['fsctmain'].'.quotation_head';
                                      if($level_emp == '1' || $level_emp== '2'){
                                            $sql.=' WHERE '.$db['fsctmain'].'.quotation_head.status IN (0) ';
                                      }else{
                                            $datemonth = date('Y-m-d');
                                            $timeremove = strtotime('-15 day',strtotime($datemonth));
                                            $timeremove = date('Y-m-d 23:59:59',$timeremove);

                                            $sql.=' WHERE '.$db['fsctmain'].'.quotation_head.branch_id = "'.$brcode.'"
                                                    AND '.$db['fsctmain'].'.quotation_head.status IN (0,1)
                                                    AND '.$db['fsctmain'].'.quotation_head.date_req > "'.$timeremove.'" ';

                                      }
                                            $sql.='  ORDER BY id DESC  ';


                                        $data = DB::connection('mysql')->select($sql);
                                    }


                                    $i = 1;
                                    ?>
                                <div class="row" style="overflow-x:auto;">
                                  <table id="example" class="table table-striped table-bordered" style="width:100%">
                                      <thead class="thead-inverse">
                                      <tr>
                                          <td>#</td>
                                          <td>วันที่</td>

                                          <td>สาขา</td>
                                          <td>QT No.</td>
                                          <td align="center">รายชื่อลูกค้า</td>

                                          <td align="center">Tax ID</td>
                                          <td>แก้ไข</td>
                                          <td>สถานะ</td>
                                          <td>ส่วนลดค่าเช่า</td>
                                          <td>ส่วนลดเงินประกัน</td>
                                          <td>ราคาสินค้า</td>
                                          <td>ยกเลิก</td>
                                          <td>พิมพ์ใบเสนอราคา( กระดาษธรรมดา )</td>
                                          <td>พิมพ์ใบเสนอราคา( กระดาษต่อเนื่อง )</td>
                                      </tr>
                                      </thead>
                                      <tbody>

                                      @foreach ($data as $value)
                                          <tr>
                                              <td scope="row">{{ $i }}</td>
                                              <td align="center">
                                                  <?php echo $value->date_req ;?>
                                              </td>
                                              <td align="center">
                                                  {{ $value->branch_id }}
                                              </td>
                                              <td align="center">
                                                  {{ $value->number_qt }}
                                              </td>
                                              <td align="center">
                                                 <?php
                                                 $datacustomer = Maincenter::getdatacustomer($value->customer_id);
                                                 if($datacustomer){
                                                   print_r($datacustomer[0]->name);
                                                   echo "&nbsp;&nbsp;&nbsp;";
                                                   print_r($datacustomer[0]->lastname);
                                                 }else {
                                                   echo $value->name_customer;
                                                 }

                                                 ?>
                                              </td>
                                              <td align="center">
                                                  {{ $value->customer_id }}
                                              </td>
                                              <td align="center">
                                                <?php if($value->status==0 || $value->status==1 || $value->status==3 ){ ?>
                                                    <a href="#" title="แก้ไข" data-toggle="modal" data-target="#myModal" onclick="getdata(2,{{$value->id}})"><img src="images/global/edit-icon.png" ></a>
                                                <?php } ?>
                                              </td>
                                              <td align="center">
                                                  <?php if($value->status==0){
                                                      echo "<font color='#00bfff'>รออนุมัติใบเสนอราคา</font>";
                                                  }else if($value->status==1){
                                                      echo "<font color='#2eb82e'>อนุมัติแล้ว</font>";
                                                  }else if($value->status==2){
                                                      echo "<font color='#ff9933'>ลงเช่าแล้ว</font>";
                                                  }else if($value->status==3){
                                                      echo "<font color='#0000ff'>อยู่ระหว่างการเช่า</font>";
                                                  }else if($value->status==4){
                                                      echo "<font color='#008000'>คืนของแล้ว</font>";
                                                  }else if($value->status==99){
                                                      echo "<font color='red'>ยกเลิก</font>";
                                                  }
                                                  ?>
                                              </td>
                                              <td align="center">
                                                <?php //ส่วนลด

                                                  echo "( ";
                                                  echo $value->discount;
                                                  echo "% )";
                                                ?>
                                              </td>
                                              <td align="center">
                                                <?php //ส่วนเงินประกัน

                                                $resultdiscountinsurance = Rent::calculatepersentinsurance($value->id);
                                                //print_r($resultdiscountinsurance);
                                                echo number_format($resultdiscountinsurance,2);
                                                // echo " % ";
                                                ?>
                                              </td>
                                              <td><?php //ราคาสินค้า

                                                  $resultsumloss = Rent::calculatetotalloss($value->id);
                                                  // print_r($resultsumloss);
                                                  echo number_format($resultsumloss[0]->sumtotalloss,2);
                                                  echo " บาท";

                                              ?>
                                             </td>
                                              <td align="center">
                                                <?php if($value->status==0){ ?>
                                                    <a href="#" title="ยกเลิก" onclick="getidqtcancel('<?php echo $value->number_qt ?>',{{$value->id}})" data-toggle="modal" data-target="#modalcalcel"><img src="images/global/close.png"></a>
                                                <?php } ?>
                                              </td>
                                              <td align="center">
                                                <?php if($value->status==1){ ?>
                                                  <a href="<?php echo url("/printqt/$value->id");?>" target="_blank"><img src="images/global/qt.png" ></a>
                                                <?php } ?>
                                              </td>
                                              <td align="center">
                                                <?php if($value->status==1){ ?>
                                                  <a href="<?php echo url("/printqtdotmatrix/$value->id");?>" target="_blank"><img src="images/global/printqtdotmatrix.png" ></a>
                                                <?php } ?>
                                              </td>
                                          </tr>
                                          <?php $i++; ?>
                                      @endforeach
                                      </tbody>
                                  </table>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                  <br>
                  <br>
                </div>
                  <!--   ข้อมูลการใบเสนอราคา fsct.co.th   -->

                  <div class="row">
                      <div class="col-md-12">
                          <div class="box box-primary">
                              <div class="breadcrumbs" id="breadcrumbs">
                                  <ul class="breadcrumb">
                                      <li>
                                          ข้อมูลการใบเสนอราคา fsct.co.th
                                      </li>
                                  </ul><!-- /.breadcrumb -->
                                  <!-- /section:basics/content.searchbox -->
                              </div>
                              <div class="box-body">
                                  <div class="row">
                                      <br>
                                  </div>
                                      <?php
                                      $brcode = Session::get('brcode');
                                      $db = Connectdb::Databaseall();

                                      $sqloff = 'SELECT * FROM '.$db['fsctweb'].'.quotation_heads';
                                      if($level_emp == '1' || $level_emp== '2'){
                                            $sqloff.=' WHERE '.$db['fsctweb'].'.quotation_heads.status IN (0) ';
                                      }else{
                                            $datemonth = date('Y-m-d');
                                            $timeremove = strtotime('-15 day',strtotime($datemonth));
                                            $timeremove = date('Y-m-d 23:59:59',$timeremove);

                                            $sqloff.=' WHERE '.$db['fsctweb'].'.quotation_heads.branch_id = "'.$brcode.'"
                                                    AND '.$db['fsctweb'].'.quotation_heads.status IN (0)
                                                    AND '.$db['fsctweb'].'.quotation_heads.date_req > "'.$timeremove.'" ';

                                      }
                                            $sqloff.='  ORDER BY id DESC  ';


                                        $dataoff = DB::connection('mysql')->select($sqloff);

                                      $i = 1;
                                      ?>
                                  <div class="row" style="overflow-x:auto;">
                                    <table id="example2" class="table table-striped table-bordered" style="width:100%">
                                        <thead class="thead-inverse">
                                        <tr>
                                            <td>#</td>
                                            <td>วันที่</td>

                                            <td>สาขา</td>
                                            <td>QT No.</td>
                                            <td align="center">รายชื่อลูกค้า</td>

                                            <td align="center">Tax ID</td>
                                            <td>แก้ไข</td>
                                            <td>สถานะ</td>
                                            <td>ส่วนลดค่าเช่า</td>
                                            <td>ส่วนลดเงินประกัน</td>
                                            <td>ราคาสินค้า</td>
                                            <td>ยกเลิก</td>
                                            <td>พิมพ์ใบเสนอราคา( กระดาษธรรมดา )</td>
                                            <td>พิมพ์ใบเสนอราคา( กระดาษต่อเนื่อง )</td>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach ($dataoff as $valueoff)
                                            <tr>
                                                <td scope="row">{{ $i }}</td>
                                                <td align="center">
                                                    <?php echo $valueoff->date_req ;?>
                                                </td>
                                                <td align="center">
                                                    {{ $valueoff->branch_id }}
                                                </td>
                                                <td align="center">
                                                    {{ $valueoff->number_qt }}
                                                </td>
                                                <td align="center">
                                                   <?php
                                                   $datacustomer = Maincenter::getdatacustomer($valueoff->customer_id);
                                                   if($datacustomer){
                                                     print_r($datacustomer[0]->name);
                                                     echo "&nbsp;&nbsp;&nbsp;";
                                                     print_r($datacustomer[0]->lastname);
                                                   }

                                                   ?>
                                                </td>
                                                <td align="center">
                                                    {{ $valueoff->customer_id }}
                                                </td>
                                                <td align="center">
                                                  <?php if($valueoff->status==0 || $valueoff->status==1 || $valueoff->status==3 ){ ?>
                                                      <a href="#" title="แก้ไข" data-toggle="modal" data-target="#myModal" onclick="getdataoff(2,{{$valueoff->id}})"><img src="images/global/edit-icon.png" ></a>
                                                  <?php } ?>
                                                </td>
                                                <td align="center">
                                                    <?php if($valueoff->status==0){
                                                            echo "<font color='#00bfff'>รออนุมัติใบเสนอราคา</font>";
                                                          }
                                                    ?>
                                                </td>
                                                <td align="center">
                                                  <?php //ส่วนลด

                                                    echo "( ";
                                                    echo $valueoff->discount;
                                                    echo "% )";
                                                  ?>
                                                </td>
                                                <td align="center">
                                                  <?php //ส่วนเงินประกัน

                                                  $resultdiscountinsurance = Rent::calculatepersentinsurance($valueoff->id);
                                                  //print_r($resultdiscountinsurance);
                                                  echo number_format($resultdiscountinsurance,2);
                                                  // echo " % ";
                                                  ?>
                                                </td>
                                                <td><?php //ราคาสินค้า

                                                    $resultsumloss = Rent::calculatetotalloss($valueoff->id);
                                                    // print_r($resultsumloss);
                                                    echo number_format($resultsumloss[0]->sumtotalloss,2);
                                                    echo " บาท";

                                                ?>
                                               </td>
                                                <td align="center">
                                                  <?php if($valueoff->status==0){ ?>
                                                      <a href="#" title="ยกเลิก" onclick="getidqtcancel('<?php echo $valueoff->number_qt ?>',{{$valueoff->id}})" data-toggle="modal" data-target="#modalcalcel"><img src="images/global/close.png"></a>
                                                  <?php } ?>
                                                </td>
                                                <td align="center">
                                                  <?php if($valueoff->status==1){ ?>
                                                    <a href="<?php echo url("/printqt/$valueoff->id");?>" target="_blank"><img src="images/global/qt.png" ></a>
                                                  <?php } ?>
                                                </td>
                                                <td align="center">
                                                  <?php if($valueoff->status==1){ ?>
                                                    <a href="<?php echo url("/printqtdotmatrix/$valueoff->id");?>" target="_blank"><img src="images/global/printqtdotmatrix.png" ></a>
                                                  <?php } ?>
                                                </td>
                                            </tr>
                                            <?php $i++; ?>
                                        @endforeach
                                        </tbody>
                                    </table>
                                  </div>


                              </div>
                          </div>
                      </div>
                  </div>



            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')


<!-- Modal -->



<div class="modal fade" id="myModal"  role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog"  style="width:80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">ข้อมูลใบเสนอราคา</h5>
            </div>

            <div class="modal-body">

                <form id="configFormvendors" onsubmit="return getdatesubmit();" data-toggle="validator" method="post" class="form-horizontal">
                    <input value="{{ null }}" type="hidden" id="id" name="id" />
                    {{--<input type="hidden" name="status" id="status" value="0">--}}
                    <input type="hidden" id="statusprocess" value="0">
                    <div class="row">
                      <div class="col-md-7">

                        </div>
                        <div class="col-md-5">
                            <div class="pull-right">รหัสสาขา <?php  $brcode = Session::get('brcode');?>
                                <input type="text" readonly  class="form-control" name="branch_id" id="branch_id" value="<?php  $brcode = Session::get('brcode'); echo $brcode ;?>"  />

                                <?php
                                //$databr = Maincenter::databranchbycode($brcode);
                                //print_r($databr[0]->name_branch);
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-6 control-label">วันที่ออกใบเสนอราคา<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-5">
                                    <input type="text"  class="form-control" id="dateshow" value="<?php echo date('d-m-Y',strtotime("+543 year"))?>"  disabled/>
                                    <input type="hidden"  class="form-control" name="date" id="date" value="<?php echo date('Y-m-d H:i:s')?>"  />
                                    <input type="hidden"  class="form-control" name="year" id="year" value="<?php echo date('Y')?>"  />
                                    <input type="hidden"  class="form-control" name="year_th" id="year_th" value="<?php echo date('Y',strtotime("+543 year"))?>"  />
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">เลือกบริษัท<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <select name="id_company" id="id_company" class="form-control" onchange="selectnoqt(this)">
                                        <?php
                                        $db = Connectdb::Databaseall();
                                        $data = DB::connection('mysql')->select('SELECT * FROM '.$db['hr_base'].'.working_company WHERE status ="1"');
                                        ?>
                                        @foreach ($data as $value)
                                            <option value="{{$value->id}}">{{$value->name}}</option>

                                        @endforeach

                                    </select>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="pull-right">
                                <div class="form-group">
                                    <label class="col-xs-6 control-label">เลขที่เอกสาร<i><span style="color: red">*</span></i></label>
                                    <div class="col-xs-6">
                                        <input type="text"  class="form-control" name="number_qt" id="number_qt" value=""  readonly/>
                                    </div>
                                    <div class="col-xs-4">

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">ชือลูกค้า<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" name="name_customer" id="name_customer" class="form-control" required onkeyup="autocompletecusdata()">
                                    <input type="hidden" name="cusid" id="cusid">
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">ประเภท การเช่า<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <select class="form-control" id="type_price" name="type_price" onchange="selecttypeprice(this)" required>
                                        <option value="">เลือก การเช่า</option>
                                        <?php
                                        $db = Connectdb::Databaseall();
                                        $data = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctmain'].'.type_price WHERE status = "1" ');
                                        ?>
                                        @foreach ($data as $value)
                                            <option value="{{$value->id}}">{{$value->name}}</option>

                                        @endforeach
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">ประเภท จ่ายเงิน<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <select class="form-control" id="type_pay" name="type_pay" onchange="selecttypeprice(this)" required>
                                        <option value="">เลือก จ่ายเงิน</option>
                                        <option value="1">เงินสด</option>
                                        <option value="2">เงินเชื่อ</option>
                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-xs-1 control-label">ที่อยู่<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-10">
                                    <span id="address"></span>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="col-xs-7 control-label">เบอร์โทรลูกค้า<i><span style="color: red">*</span></i></label>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <input type="text" name="tel_contactcustomer" id="tel_contactcustomer"  required class="form-control" >
                        </div>
                        <div class="col-md-8">

                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                                <label class="col-xs-2 control-label">เลือก site<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                  <select class="form-control" id="site_id" name="site_id" disabled>
                                      <option value="0">เลือก site</option>
                                  </select>
                                </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-5 control-label">ช่วงเวลาเริ่มเช่า<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-7">
                                    <input type="text" id="startdate" name="startdate" class="form-control datepicker" onchange="pickdate(this)" value="<?php echo date('d-m-Y');?>" readonly >
                                    <input type="hidden" id="startdatehidden" name="startdatehidden" value="<?php echo date('Y-m-d')?>">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-6 control-label">จำนวนวันที่เช่า<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" name="countrent" id="countrent"  class="form-control" onblur="calcountrent(this)" placeholder="วัน">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-5 control-label">ช่วงเวลาสิ้นสุดการเช่า<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-7">
                                    <input type="text" id="duedate" name="duedate" class="form-control" readonly >
                                    <input type="hidden" id="duedatehidden" name="duedatehidden">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row" id="hideselecthead">
                       <div class="col-md-12" >
                           <center><h4><font color="red">กรุณาเลือกประเภทการเช่าก่อนทำรายการ</font></h4></center>
                       </div>
                    </div>

                    <div class="row showdetail" >
                        <div class="col-md-12">
                            <div class="pull-right">
                                <a href="#" title="เพิ่มรายการ" id="addrow" ><img src="images/global/add.png">เพิ่มรายการ</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>

                    <div class="row showdetail" style="display: none">
                        <div class="col-md-12">
                            <table class="table" id="thdetail">
                                <thead>
                                <tr>
                                    <td align="center">รายการ</td>
                                    <td align="center" >ราคา</td>
                                    <td align="center">จำนวน</td>
                                    <td align="center">รวม</td>
                                    <td align="center">ลบ</td>
                                </tr>
                                </thead>
                                <tbody id="tdbody">
                                    <td align="center">
                                        <input type="hidden" name="material_id[]" id="materialid0" value="0">
                                        <input type="text" name="list[]" id="list0"  onfocus="autocompletedata(0)" class="form-control" >
                                    </td>
                                    <td align="center">
                                        <input type="text" name="price[]" id="price0"   class="form-control" disabled >
                                        <input type="hidden" name="insurance[]" id="insurance0">
                                        <input type="hidden" name="loss[]" id="loss0">
                                        <input type="hidden" name="weight[]" id="weight0">
                                    </td>
                                    <td align="center">
                                        <input type="text" name="amount[]" id="amount0" onblur="getamount(this,0)" class="form-control"  placeholder="จำนวน">
                                    </td>
                                    <td align="center">
                                        <input type="hidden" name="totalinsurance[]" id="totalinsurance0">
                                        <input type="hidden" name="totaloss[]" id="totaloss0">
                                        <input type="hidden" name="totalweight[]" id="totalweight0">
                                        <input type="text" name="total[]"  id="total0" class="form-control"  >
                                    </td>
                                    <td align="center"></td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row showdetail "  style="display: none ;">
                        <div class="col-md-4">

                            <table width="100%" border="3" style="border-color:coral;">
                                <tr>
                                    <td>
                                      <table width="100%">
                                        <tr>
                                            <td>
                                              <div class="form-group">
                                                  <label class="col-xs-5 control-label">ส่วนลด<i><span style="color: red">*</span></i></label>
                                                  <div class="col-xs-4">
                                                      <input type="text" name="discount" id="discount" onblur="calcullatelast()"  class="form-control" placeholder="%" value="0">
                                                  </div>
                                                  <label class="col-xs-1 control-label">%</label>

                                              </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                              <div class="form-group">
                                                  <label class="col-xs-5 control-label">จำนวน<i><span style="color: red">*</span></i></label>
                                                  <div class="col-xs-4">
                                                      <span id="showdiscount"></span>
                                                  </div>
                                                  <label class="col-xs-1 control-label">บาท</label>

                                              </div>
                                            </td>
                                        </tr>
                                      </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-4">
                            <table width="100%" border="3" style="border-color:coral;">
                                <tr>
                                    <td>
                                      <table width="100%">
                                        <tr>
                                            <td>
                                              <div class="form-group">
                                                  <label class="col-xs-5 control-label">หัก ณ ที่จ่าย<i><span style="color: red">*</span></i></label>
                                                  <div class="col-xs-4">
                                                      <select name="withhold" id="withhold" class="form-control" onchange="calcullatelast()">
                                                          <option value="0" >ไม่คิดภาษี</option>
                                                          <?php
                                                          $db = Connectdb::Databaseall();
                                                          $datawht = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.withhold WHERE status ="1"');
                                                          ?>
                                                          @foreach ($datawht as $valuewth)
                                                              <option value="{{$valuewth->withhold}}">{{$valuewth->withhold}}</option>
                                                          @endforeach
                                                      </select>
                                                  </div>
                                                  <label class="col-xs-1 control-label">%</label>

                                              </div>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                              <div class="form-group">
                                                  <label class="col-xs-5 control-label">จำนวน<i><span style="color: red">*</span></i></label>
                                                  <div class="col-xs-4">
                                                      <span id="showwithhold"></span>
                                                  </div>
                                                  <label class="col-xs-1 control-label">บาท</label>

                                              </div>
                                            </td>
                                        </tr>
                                      </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-4">
                          <table width="100%" border="3" style="border-color:coral;">
                              <tr>
                                  <td>
                                    <table width="100%">
                                      <tr>
                                          <td>
                                            <div class="form-group">
                                                <label class="col-xs-5 control-label">ภาษี<i><span style="color: red">*</span></i></label>
                                                <div class="col-xs-4">

                                                    <select name="vat" id="vat" class="form-control" onchange="calcullatelast()">
                                                        <option value="0" >ไม่คิดภาษี</option>
                                                        <?php
                                                        $db = Connectdb::Databaseall();
                                                        $datavat = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.tax_config WHERE status ="1"');
                                                        ?>
                                                        @foreach ($datavat as $valuevat)
                                                            <option value="{{$valuevat->tax}}" <?php if($valuevat->tax=='7'){ echo "selected=selected";}?>>{{$valuevat->tax}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <label class="col-xs-1 control-label">%</label>

                                            </div>

                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                            <div class="form-group">
                                                <label class="col-xs-5 control-label">จำนวน<i><span style="color: red">*</span></i></label>
                                                <div class="col-xs-4">
                                                    <span id="showvat"></span>
                                                </div>
                                                <label class="col-xs-1 control-label">บาท</label>

                                            </div>
                                          </td>
                                      </tr>
                                    </table>
                                  </td>
                              </tr>
                          </table>

                        </div>
                    </div>

                    <div class="row">
                        <br>
                    </div>
                    <div class="row showdetail ps3" style="display: none">
                        <div class="col-md-4">
                          <div class="form-group">
                              <label class="col-xs-5 control-label">เงินประกัน<i><span style="color: red">*</span></i></label>
                              <div class="col-xs-7">
                                    <?php
                                    $db = Connectdb::Databaseall();
                                    $datainsurance= DB::connection('mysql')->select('SELECT * FROM '.$db['fsctmain'].'.type_insurance WHERE status = "1" ');
                                    ?>
                                    <select class="form-control" id="type_insurance_id" name="type_insurance_id" onchange="typeinsuranceid(this)">
                                      <?php foreach ($datainsurance as $key => $value): ?>

                                          <option value="{{ $value->id}}" <?php if($value->id == 2){ echo "selected=selected";} ?>>{{ $value->type_description }}</option>
                                      <?php endforeach; ?>
                                    </select>
                              </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                              <label class="col-xs-5 control-label">เงินประกัน<i><span style="color: red">*</span></i></label>
                              <div class="col-xs-4">
                                  <input type="text" name="insurance_money" id="insurance_money"  class="form-control" placeholder="%" value="0">
                              </div>
                              <label class="col-xs-1 control-label">บาท</label>

                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                              <label class="col-xs-5 control-label">เงินประกันออนไลน์<i><span style="color: red">*</span></i></label>
                              <div class="col-xs-4">
                                  <input type="text" name="insurance_money_online" id="insurance_money_online"  class="form-control" placeholder="%" disabled value="0">
                              </div>
                              <label class="col-xs-1 control-label">บาท</label>

                          </div>
                                <input type="hidden" name="sumtotalloss[]" id="sumtotalloss"  class="form-control" placeholder="%" value="0">
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row showdetail ps3" style="display: none">
                        <div class="col-md-4">
                          <div class="form-group">

                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">

                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <div class="pull-right">
                              ลดกี่เปอร์เซ็นต์&nbsp;&nbsp;
                            </div>
                          </div>
                        </div>
                        <div class="col-md-1">
                          <div class="form-group">
                            <input type="text" class="form-control" id="present_insurance" name="present_insurance" size="4" readonly>
                          </div>

                        </div>
                        <div class="col-md-1">
                          <div class="form-group">
                            %
                          </div>

                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>

                    <div class="row showdetail ps3" style="display: none">
                        <div class="col-md-6">
                          <div class="form-group">
                              <label class="col-xs-5 control-label"><U>น้ำหนักรวมทั้งหมด</U><i><span style="color: red">*</span></i></label>
                              <div class="col-xs-5">
                                  <input type="text" name="sumtotalweight" id="sumtotalweight"  class="form-control"  value="0" readonly>
                              </div>
                              <label class="col-xs-1 control-label">กิโลกรัม</label>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                              <label class="col-xs-5 control-label"><U>ยอดรวมค่าเช่า</U><i><span style="color: red">*</span></i></label>
                              <div class="col-xs-5">
                                  <input type="text" name="total" id="total"  class="form-control" placeholder="%" value="0" disabled>
                              </div>
                              <label class="col-xs-1 control-label">บาท</label>
                          </div>

                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row ">
                      <div class="col-md-4">
                        <div class="pull-right">
                          ประกัน+วงเงิน
                            <input type="text" class="form-control" name="credit" id="credit"  readonly>
                        </div>

                      </div>
                      <div class="col-md-4">
                        <div class="pull-right">
                          ประกันรวมทุกบิล+ค่าเช่าค้างจ่าย
                            <input type="text" class="form-control" name="credit_require" id="credit_require" readonly>
                        </div>

                      </div>
                        <div class="col-md-4">


                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="pull-right">
                            วงเงินที่ให้
                            <input type="text" class="form-control" id="creditcustomer" readonly>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="pull-right">
                            วงเงินที่เหลือ
                            <input type="text" class="form-control" name="creditreal" id="creditreal" readonly>
                        </div>

                      </div>
                        <div class="col-md-4">
                            <div class="pull-right">
                                รหัสพนักงาน:
                                <input type="text" class="form-control" name="code_emp" id="code_emp" value="<?php echo $emp_code = Session::get('emp_code')?>" readonly>

                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="pull-right">
                                รวมราคาสินค้า
                                <input type="text"  class="form-control"  id="sumlossreal"  readonly >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="pull-right">
                                <input type="text" class="form-control" name="code_sup" id="code_sup" placeholder="รหัสซุปประจำสาขา">
                                <input type="hidden" id="amountinsurancetype0" name="amountinsurancetype" value="0">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-right">

                              <input type="text" class="form-control" name="salecode" id="salecode" placeholder="รหัสพนักงานขาย">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row" style="display: none;" id="updatestatus" >
                        <div class="col-md-4">

                        </div>
                        <div class="col-md-4">
                            สถานะปัจจุบัน
                            <?php
                            $id_position = Session::get('id_position');
                            $level_emp = Session::get('level_emp');
                            // echo $emp_code ;//
                            // echo $brcode ;

                            $db = Connectdb::Databaseall();

                             $sql = 'SELECT * FROM '.$db['fsctmain'].'.config_auth_qt
                                    WHERE level_emp = "'.$level_emp.'"
                                    AND code_emp = "'.$emp_code.'"
                                    AND branch_id = "'.$brcode.'" ';


                            $datainsurance= DB::connection('mysql')->select($sql);
                            // print_r($datainsurance);
                            // exit;
                            $setpersent = 0;
                            $setpresentinsurance = 0;
                            $minmoney = 0;
                            $minmoneyloss = 0;

                            if($datainsurance){
                              $setpersent = $datainsurance[0]->precentdiscount;
                              $minmoneyloss = $datainsurance[0]->minmoneyloss;
                              $setpresentinsurance = $datainsurance[0]->presentdiscountinsure;
                            }else{
                               $sql2 = 'SELECT * FROM '.$db['fsctmain'].'.config_auth_qt
                                    WHERE level_emp = "'.$level_emp.'"
                                    AND code_emp = "all"
                                    AND branch_id = "all" ';
                               $datainsurancedefault= DB::connection('mysql')->select($sql2);
                               // print_r($datainsurancedefault);
                                  if($datainsurancedefault){
                                      $setpersent = $datainsurancedefault[0]->precentdiscount;
                                      $minmoneyloss = $datainsurancedefault[0]->minmoneyloss;
                                      $setpresentinsurance = $datainsurancedefault[0]->presentdiscountinsure;
                                  }

                            }


                            ?>
                            <input type="hidden" name="level_emp" id="level_emp" value="<?php echo $level_emp;?>">
                            <input type="hidden" name="percentdiscount" id="percentdiscount" value="<?php echo $setpersent; ?>">
                            <input type="hidden" name="presentinsurance" id="presentinsurance" value="<?php echo $setpresentinsurance; ?>">
                            <input type="hidden" name="minmoneyloss" id="minmoneyloss" value="<?php echo $minmoneyloss; ?>">
                            <select name="status" id="status" class="form-control" onfocus="selectapproved()">
                                <option value="0" selected>รออนุมัติ</option>
                                <option value="1" id="approveddisabled"  <?php  if(!$datainsurance){ echo "disabled"; } ?> >อนุมัติ</option>
                                <option value="99" >ยกเลิก</option>
                            </select>


                        </div>
                        <div class="col-md-4">

                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>

                    <!-- add cal transport -->
                    <div class="row">
                          <br>
                    </div>

                    <div class="row-fluid text-center">
                        <button id="btn-cal-transportไม่มีอะไร" class="btn btn-primary col-md-offset-2 col-md-8" type="button" >ประมาณราคาค่าขนส่ง</button>
                    </div>

                    <div class="row">
                        <br>
                    </div>

                    <!-- start map cal transport -->
                    <div class="row cal_transport">
                      <div id="map_canvas" style="width: 100%; height: 28em; margin:auto; margin-top:10px;"></div>
                      <div class="row-fluid" id="showDD" style="margin:auto;padding-top:5px;width:550px;"> 

                        <div class="form-group">
                          <label for="namePlaceGet">ต้นทางจาก บจกฯ ฟ้าใสฯ<span class="text-danger">(ไม่ควรเปลี่ยน)</span></label>
                          <input class="form-control" name="namePlaceGet" type="text" id="namePlaceGet" readonly>
                        </div>

                        <div class="form-group">
                          <label for="toPlaceGet">กรุณาย้ายหมุด B ไปยังจุดหมายที่ต้องการ</label>
                          <input class="form-control" name="toPlaceGet" type="text" id="toPlaceGet" readonly/>
                        </div>

                        <div class="row form-inline">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="distance_value">ระยะทาง x 2 (ไปกลับ) &nbsp;</label>
                              <input class="form-control text-center" readonly name="distance_value" type="text" id="distance_value" value="0" />
                              <label>&nbsp; กิโลเมตร</label>
                            </div>
                          </div>

<!--                            <div class="col-md-6">
                            <div class="form-group">
                              <label for="duration_text">ระยะเวลาข้อความ</label>
                              <input class="form-control" name="duration_text" type="text" id="duration_text"/>
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="duration_value">ระยะเวลาตัวเลข</label>
                              <input class="form-control" name="duration_value" type="text" id="duration_value" value="0"/>
                              <label>&nbsp; วินาที</label>
                            </div>
                          </div> -->
                        </div>

                      </div>


                    </div>

                    <input type="hidden" name="start_location_lat" id="start_location_lat" value="">
                    <input type="hidden" name="start_location_lng" id="start_location_lng" value="">
                    <input type="hidden" name="end_location_lat" id="end_location_lat" value="">
                    <input type="hidden" name="end_location_lng" id="end_location_lng" value="">

                    <!-- end map cal transport -->

                    <div class="row">
                        <br>
                    </div>

                    <div class="row showdetail ps3 cal_transport" style="">
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="col-xs-5 control-label">ค่าขนส่ง<i><span style="color: red">*</span></i></label>
                              <div class="col-xs-4">
                                  <input type="text" name="transport" id="transport" onkeyup="calcullatelast()"   class="form-control" placeholder="บาท" value="1" readonly>
                              </div>
                              <label class="col-xs-1 control-label">บาท</label>

                          </div>
                      </div>
                      <div class="col-md-4 cal_transport">
                          <div class="form-group">
                              <label class="col-xs-5 control-label">ค่าขนส่ง หัก ณ ที่จ่าย<i><span style="color: red">*</span></i></label>
                              <div class="col-xs-4">
                                  <input type="text" name="withhold_transport" id="withhold_transport" onkeyup="calcullatelast()"   class="form-control" placeholder="บาท" value="0">


                              </div>
                                <label class="col-xs-1 control-label">%</label>
                          </div>
                      </div>
                      <div class="col-md-4 cal_transport">
                        <div class="form-group">
                            <label class="col-xs-5 control-label">จำนวนเงิน<i><span style="color: red">*</span></i></label>
                            <div class="col-xs-4">
                                <input type="text" name="showwithholdtransport" id="showwithholdtransport" value="" class="form-control text-danger" readonly>
                            </div>
                            <label class="col-xs-1 control-label">บาท</label>

                        </div>
                      </div>

                    </div>

                    <div class="row cal_transport">
                      <br>
                    </div>

                    <div class="row show_method_cal">
                      <table class="table">
                        <thead id="thead_show_method_cal">

                        </thead>
                        <tbody id="tbody_show_method_cal">

                        </tbody>
                      </table>
                    </div>

                    <div class="row cal_transport">
                      <br>
                    </div>

                    {{-- <div class="row cal_transport">
                        <div class="col-md-4">
                          <label id="label_total_price" >รวมราคาสินค้า</label>
                        </div>
                        <div class="col-md-7">
                          <input type="text"  class="form-control text-center"  id="sumlossreal"  readonly >
                        </div>
                        <label class="col-xs-1 control-label">บาท</label>

                    </div> --}}

                    <div class="row cal_transport">
                        <br>
                    </div>

                    <div class="row cal_transport">

                      <label class="col-xs-5 control-label col-xs-offset-1">เลือกขนาดของรถบรรทุก</label>

                      <div class="col-xs-5">
                        <select class="form-control" id="select_transport_id" >

                        </select>
                      </div>
                    </div>

                    <div class="row cal_transport">
                        <br>
                    </div>

                    {{-- <div class="row cal_transport">
                        <table class="table table-striped text-center" id="table_cal_transport">
                          <thead id="thead_cal_transport">
                            <tr>
                              <th >ชนิดรถ</th>
                              <th >จำนวน(คัน)</th>
                            </tr>
                          </thead>
                          <tbody id="tbody_cal_transport">

                          </tbody>
                        </table>
                    </div> --}}

                    <div class="row cal_transport">
                        <br>
                    </div>

                    <div class="row-fluid text-center cal_transport">
                        <label class="label text-warning">*** ค่าขนส่งขึ้นอยู่กับลักษณะของสินค้า เป็นเพียงการประมาณการค่าขนส่งสินค้า <br> เฉพาะสาขาบางปะอินต้องติดต่อกับขนส่งภายนอก ***</label>
                    </div>

                    <div class="row cal_transport">
                        <br>
                    </div>
                    <!--end cal transport -->

                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-xs-5 col-xs-offset-3">
                                    <button type="submit" id="Btn_save"  class="btn btn-primary">Save</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">

                    </div>



                </form>
            </div>
        </div>
    </div>
</div>



<!---   myModal approved--->

<div class="modal fade" id="modalcalcel" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">ยกเลิกใบเสนอราคา</h4>
        </div>
        <div class="modal-body">
          <div class="row">
              <div class="col-md-3">
              </div>
              <div class="col-md-3">
                <div  style="text-align:right">
                  ใบเลขที่
                </div>
              </div>
              <div class="col-md-3">
                  <input type="hidden" id="idqtcancel" name="idqtcancel">
                  <span id="qtnumber">
                  </span>
              </div>
              <div class="col-md-3">
              </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="savecancelqt()" class="btn btn-warning">ยืนยัน</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
