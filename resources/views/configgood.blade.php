<?php
use  App\Api\Connectdb;

use  App\Api\Vendorcenter;

?>
@include('headmenu')

<link>
<!-- <script type="text/javascript" src = 'https://code.jquery.com/jquery-3.3.1.js'></script> -->
<!-- <script type="text/javascript" src = 'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js'></script>
<script type="text/javascript" src = 'https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js'></script>
<script type="text/javascript" src = 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js'></script>
<script type="text/javascript" src = 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js'></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"> -->

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>
<script type="text/javascript" src = 'js/config/configgood.js'></script>
<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">

<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

</script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-cog home-icon"></i>
                        <a href="#">ตั้งค่า</a>
                    </li>
                    <li class="active">ตั้งค่าสินค้า</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        ตั้งค่าสินค้า
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-10">

                                    </div>
                                    <div class="col-md-1">

                                    </div>
                                    <div class="col-md-1">
                                        <a href="#" title="เพิ่มข้อมูล" data-toggle="modal" data-target="#myModal" onclick="insertnew()" ><img src="images/global/add.png"></a>
                                    </div>
                                </div>
                                <div class="row">
                                    <br>
                                </div>
                                <div class="row">
                                    <?php


                                    ?>
                                        <table id="example" class="table table-striped table-bordered">
                                            <thead class="thead-inverse">
                                            <tr>
                                                <td>#</td>
                                                <td>ชื่อ</td>
                        												<td>ชื่อหน่วย</td>
                        												<td>ชื่อประเภท</td>
                        												<td>ชื่อกลุ่มการซื้อ</td>
                        												<td>รหัสบัญชี</td>
                                                <td>สถานะ</td>
                                                <td>การจัดการ</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $db = Connectdb::Databaseall();
                                            $config = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.good');
                                            $i = 1;
                                            ?>
                                            @foreach ($config as $detail)
                                                <tr>
                                                    <td scope="row">{{ $i }}</td>
                                                    <td>{{ $detail->name }}</td>
                  													        <td>{{ $detail->unit }}</td>

                  													        <td><?php
                            													$datagroupname = Vendorcenter::getdatagoodtype($detail->type_id);
                            													foreach($datagroupname as $r){
                                                                                      echo $r->name_type;
                            													}
                            													?>
                                                    </td>
													                          <td>
                                                     <?php
                                                       $dataconfigsupp = Vendorcenter::getdatagroupsupp($detail->group_supplier);
                                                     foreach($dataconfigsupp as $r){
                                                         echo $r->name;
                                                        }

                                                         ?>
                                                    </td>
													                          <td>
                                                     <?php
                                                       $dataconfigsupp = Vendorcenter::getdataaccounttype($detail->accounttype);
                                                     foreach($dataconfigsupp as $r){
                                                         echo $r->accounttypeno;
                                                        }
                                                         ?>
                                                    </td>

                                                    <td><?php if($detail->status==1){ echo "<font color='green'>ใช้งาน</font>";} else {  echo "<font color='red'>ยกเลิก</font>";} ?></td>
                                                    <td><a href="#" title="แก้ไขข้อมูล" data-toggle="modal" data-target="#myModal" onclick="getdata({{ $detail->id }})"><img src="images/global/edit-icon.png"></a>
                                                    </td>

                                                </tr>
                                                <?php $i++; ?>
                                            @endforeach
                                            </tbody>
                                        </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')


<!-- Modal -->

<!-- Modal -->


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">ตั้งค่าสินค้า</h5>
            </div>

            <div class="modal-body">
                <!-- The form is placed inside the body of modal -->
                <form id="configgood" onsubmit="return getdatesubmit();" data-toggle="validator" method="post" class="form-horizontal">
                    <input value="{{ null }}" type="hidden" id="id" name="id" />

                    <div class="form-group">
                        <label class="col-xs-3 control-label">ชื่อ<i><span style="color: red">*</span></i></label>
                        <div class="col-xs-5">
                            <input type="text" id="name" class="form-control" name="name" size="1" required/>
                        </div>
                        <div class="col-xs-4">

                        </div>
                    </div>


										 <div class="form-group">
                        <label class="col-xs-3 control-label">ชื่อหน่วย<i><span style="color: red">*</span></i></label>
                        <div class="col-xs-5">
                            <input type="text" id="unit" class="form-control" name="unit" size="1" required/>
                        </div>
                        <div class="col-xs-4">

                        </div>
                    </div>


                    {{--<input value="0" type="hidden" id="check" name="check"/>--}}


                    <div class="form-group">
                        <label class="col-xs-3 control-label">ชื่อประเภทสินค้า<i><span style="color: red">*</span></i></label>
                        <div class="col-xs-5">
                                    <select name="type_id" id="type_id" class="form-control">
                                        <?php
                                        $db = Connectdb::Databaseall();
                                        $data = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.type_good WHERE status ="1"');
                                        ?>
                                        @foreach ($data as $value)
                                            <option value="{{$value->id}}">{{$value->name_type}}</option>

                                        @endforeach

                                    </select>
                        </div>
                    </div>





					    <div class="form-group">
                        <label class="col-xs-3 control-label">ชื่อประเภททางการซื้อ<i><span style="color: red">*</span></i></label>
                        <div class="col-xs-5">
                                    <select name="group_supplier" id="group_supplier" class="form-control">
                                        <?php
                                        $db = Connectdb::Databaseall();
                                        $data = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.config_group_supp WHERE status ="1"');
                                        ?>
                                        @foreach ($data as $value)
                                            <option value="{{$value->id}}">{{$value->name}}</option>

                                        @endforeach

                                    </select>
                        </div>
                    </div>



						<div class="form-group">
                        <label class="col-xs-3 control-label">ประเภททางการบัญชี<i><span style="color: red">*</span></i></label>
                        <div class="col-xs-5">
                                    <select name="accounttype" id="accounttype" class="form-control">
                                        <?php
                                        $db = Connectdb::Databaseall();
                                        $data = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.accounttype WHERE status ="1"');
                                        ?>
                                        @foreach ($data as $value)
                                            <option value="{{$value->id}}"> ({{$value->accounttypeno}}) {{$value->accounttypefull}}</option>

                                        @endforeach

                                    </select>
                        </div>
                    </div>







                    <div class="form-group">
                        <label class="col-xs-3 control-label">สถานะ<i><span style="color: red">*</span></i></label>
                        <div class="col-xs-5">
                           <select class="form-control" name="status" id="status">
                               <option value="1">ใช้งาน</option>
                               <option value="99">ยกเลิก</option>
                           </select>
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-xs-5 col-xs-offset-3">
                            <button type="submit" id="Btn_save" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
