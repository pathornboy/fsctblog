<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;

use App\Api\Datetime;
use App\working_company;
use Illuminate\Support\Facades\Input;

?>

<link>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}
<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>

<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">

<script type="text/javascript" src = 'js/vendor/vendorcenter.js'></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

    <style>
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }

        body {
            font-family: "THSarabunNew";
            font-size: 10px;
        }
        h4 {
            font-family: "THSarabunNew";
        }
        h4 {
            font-family: "THSarabunNew";
        }

    </style>

<style>
    .modal-ku {
        width: 100%;
        margin: auto;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="box-body">

            <?php

              $data = Input::all();
              $db = Connectdb::Databaseall();
              // echo "<pre>";
              // print_r($data);
              // exit;

              $datepicker = explode("-",trim(($data['datepicker'])));

              $datepickerstart = explode("/",trim(($datepicker[0])));
              if(count($datepickerstart) > 0) {
                  $datetime = $datepickerstart[1] . '-' . $datepickerstart[0]; //วัน - เดือน
              }

              $datepickerend = explode("/",trim(($datepicker[1])));
              if(count($datepickerend) > 0) {
                  $datetime2 = $datepickerend[1] . '-' . $datepickerend[0]; //วัน - เดือน
              }

              if($datepickerstart[0] == "01"){$monthTH = "มกราคม";
                }else if($datepickerstart[0] == "02"){$monthTH = "กุมภาพันธ์";
                }else if($datepickerstart[0] == "03"){$monthTH = "มีนาคม";
                }else if($datepickerstart[0] == "04"){$monthTH = "เมษายน";
                }else if($datepickerstart[0] == "05"){$monthTH = "พฤษภาคม";
                }else if($datepickerstart[0] == "06"){$monthTH = "มิถุนายน";
                }else if($datepickerstart[0] == "07"){$monthTH = "กรกฎาคม";
                }else if($datepickerstart[0] == "08"){$monthTH = "สิงหาคม";
                }else if($datepickerstart[0] == "09"){$monthTH = "กันยายน";
                }else if($datepickerstart[0] == "10"){$monthTH = "ตุลาคม";
                }else if($datepickerstart[0] == "11"){$monthTH = "พฤศจิกายน";
                }else if($datepickerstart[0] == "12"){$monthTH = "ธันวาคม";
                }

                $modelname = Maincenter::databranchbycode($data['branch']);

                $compid = $modelname[0]->company_id;
                $sqlcompany = "SELECT * FROM $db[hr_base].working_company  WHERE id ='$compid' ";
                $datacomp = DB::connection('mysql')->select($sqlcompany);

                $branch_id = $data['branch'];
                $sql = "SELECT * FROM $db[hr_base].branch  WHERE code_branch ='$branch_id' ";
                $databranch = DB::connection('mysql')->select($sql);

            ?>

          <div class="row">
              <div class="col-md-12">
                  <div class="box box-primary">
                      <div class="breadcrumbs" id="breadcrumbs">
                          <ul class="breadcrumb">
                              <div align="center">



                              <table width="100%">
                                <tr>
                                  <td align="center" ><b>รายงานภาษีขาย</b></td>
                                </tr>

                                <tr>
                                  <td align="center" ><b>เดือนภาษี {{$monthTH}} ปี {{$datepickerstart[2]}}</b></td>
                                </tr>

                                <tr>
                                  <td align="center" ><b>ชื่อผู้ประกอบการ : บริษัท ฟ้าใสคอนสตรัคชั่นทูลส์ จำกัด ({{$modelname[0]->name_branch}})</b></td>
                                </tr>

                                <tr>
                                  <td align="right" ><b>เลขประจำตัวผู้เสียภาษีอากร {{$datacomp[0]->inv_number}}</b></td>
                                </tr>

                                <?php if($modelname[0]->branch_tax == "สำนักงานใหญ่"){?>
                                <tr>
                                  <td align="right" ><b>X สำนักงานใหญ่    สาขา</b></td>
                                </tr>
                                <?php } ?>

                                <?php if($modelname[0]->branch_tax != "สำนักงานใหญ่"){?>
                                <tr>
                                  <td align="right" ><b>  สำนักงานใหญ่  X สาขา{{$modelname[0]->branch_tax}}</b></td>
                                </tr>
                                <?php } ?>

                                <tr>
                                  <td align="left" ><b>ที่อยู่สถานประกอบการ : {{$modelname[0]->addresstax}}</b></td>
                                </tr>

                              </table>
              </div>
              </div>
              </ul><!-- /.breadcrumb -->
              <!-- /section:basics/content.searchbox -->
              </div>

							<div align="center">
							<?php

              $data = Input::all();
              $db = Connectdb::Databaseall();
              // echo "<pre>";
              // print_r($data);
              // exit;

              // $branch_id = $data['branch_id'];
              // $start_date = $data['datepickerstart']." 00:00:00";
              // $end_date = $data['datepickerend']." 23:59:59";

              $datepicker = explode("-",trim(($data['datepicker'])));

              // $start_date = $datepicker[0];
              $e1 = explode("/",trim(($datepicker[0])));
                      if(count($e1) > 0) {
                          $start_date = $e1[2] . '-' . $e1[0] . '-' . $e1[1]; //ปี - เดือน - วัน
                          $start_date2 = $start_date." 00:00:00";
                      }

              // $end_date = $datepicker[1];
              $e2 = explode("/",trim(($datepicker[1])));
                      if(count($e2) > 0) {
                          $end_date = $e2[2] . '-' . $e2[0] . '-' . $e2[1]; //ปี - เดือน - วัน
                          $end_date2 = $end_date." 23:59:59";
                      }

              $branch_id = $data['branch'];

              // echo "<pre>";
              // print_r($start_date);
              // print_r($end_date);
              // exit;

              $sqlra = 'SELECT '.$db['fsctaccount'].'.taxinvoice_abb.*

                     FROM '.$db['fsctaccount'].'.taxinvoice_abb

                      WHERE '.$db['fsctaccount'].'.taxinvoice_abb.branch_id = "'.$branch_id.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_abb.time  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_abb.status IN (1,98)
                        ORDER BY '.$db['fsctaccount'].'.taxinvoice_abb.number_taxinvoice ASC
                     ';

              $datataxra = DB::connection('mysql')->select($sqlra);


              $sqltf = 'SELECT '.$db['fsctaccount'].'.taxinvoice.*

                     FROM '.$db['fsctaccount'].'.taxinvoice

                      WHERE '.$db['fsctaccount'].'.taxinvoice.branch_id = "'.$branch_id.'"
                        AND '.$db['fsctaccount'].'.taxinvoice.timereal  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                        AND '.$db['fsctaccount'].'.taxinvoice.status = 1
                        ORDER BY '.$db['fsctaccount'].'.taxinvoice.number_taxinvoice ASC
                     ';

              $datataxtf = DB::connection('mysql')->select($sqltf);


              $sqlcn = 'SELECT '.$db['fsctaccount'].'.taxinvoice_creditnote.*

                     FROM '.$db['fsctaccount'].'.taxinvoice_creditnote

                      WHERE '.$db['fsctaccount'].'.taxinvoice_creditnote.branch_id = "'.$branch_id.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_creditnote.time  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_creditnote.status = 1
                        ORDER BY '.$db['fsctaccount'].'.taxinvoice_creditnote.number_taxinvoice ASC
                     ';

              $datataxcn = DB::connection('mysql')->select($sqlcn);


              $sqlcs = 'SELECT '.$db['fsctaccount'].'.taxinvoice_creditnote_special_abb.*

                     FROM '.$db['fsctaccount'].'.taxinvoice_creditnote_special_abb

                      WHERE '.$db['fsctaccount'].'.taxinvoice_creditnote_special_abb.branch_id = "'.$branch_id.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_creditnote_special_abb.date_approved  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_creditnote_special_abb.status = 1
                        AND '.$db['fsctaccount'].'.taxinvoice_creditnote_special_abb.status = 1
                        ORDER BY '.$db['fsctaccount'].'.taxinvoice_creditnote_special_abb.number_taxinvoice ASC
                     ';

              $datataxcs = DB::connection('mysql')->select($sqlcs);


              $sqltk = 'SELECT '.$db['fsctaccount'].'.taxinvoice_loss.*

                     FROM '.$db['fsctaccount'].'.taxinvoice_loss

                      WHERE '.$db['fsctaccount'].'.taxinvoice_loss.branch_id = "'.$branch_id.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_loss.timereal  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_loss.status = 1
                        ORDER BY '.$db['fsctaccount'].'.taxinvoice_loss.number_taxinvoice ASC
                     ';

              $datataxtk = DB::connection('mysql')->select($sqltk);


              $sqlrl = 'SELECT '.$db['fsctaccount'].'.taxinvoice_loss_abb.*

                     FROM '.$db['fsctaccount'].'.taxinvoice_loss_abb

                      WHERE '.$db['fsctaccount'].'.taxinvoice_loss_abb.branch_id = "'.$branch_id.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_loss_abb.time  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_loss_abb.status IN (1,98)
                        ORDER BY '.$db['fsctaccount'].'.taxinvoice_loss_abb.number_taxinvoice ASC
                     ';

              $datataxrl = DB::connection('mysql')->select($sqlrl);


              $sqltm = 'SELECT '.$db['fsctaccount'].'.taxinvoice_more.*

                     FROM '.$db['fsctaccount'].'.taxinvoice_more

                      WHERE '.$db['fsctaccount'].'.taxinvoice_more.branch_id = "'.$branch_id.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_more.timereal  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_more.status = 1
                        ORDER BY '.$db['fsctaccount'].'.taxinvoice_more.number_taxinvoice ASC
                     ';

              $datataxtm = DB::connection('mysql')->select($sqltm);


              $sqlrn = 'SELECT '.$db['fsctaccount'].'.taxinvoice_more_abb.*

                     FROM '.$db['fsctaccount'].'.taxinvoice_more_abb

                      WHERE '.$db['fsctaccount'].'.taxinvoice_more_abb.branch_id = "'.$branch_id.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_more_abb.time  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_more_abb.status IN (1,98)
                        ORDER BY '.$db['fsctaccount'].'.taxinvoice_more_abb.number_taxinvoice ASC
                     ';

              $datataxrn = DB::connection('mysql')->select($sqlrn);


              $sqltp = 'SELECT '.$db['fsctaccount'].'.taxinvoice_partial.*

                     FROM '.$db['fsctaccount'].'.taxinvoice_partial

                      WHERE '.$db['fsctaccount'].'.taxinvoice_partial.branch_id = "'.$branch_id.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_partial.timereal  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_partial.status = 1
                        ORDER BY '.$db['fsctaccount'].'.taxinvoice_partial.number_taxinvoice ASC
                     ';

              $datataxtp = DB::connection('mysql')->select($sqltp);


              $sqlro = 'SELECT '.$db['fsctaccount'].'.taxinvoice_partial_abb.*

                     FROM '.$db['fsctaccount'].'.taxinvoice_partial_abb

                      WHERE '.$db['fsctaccount'].'.taxinvoice_partial_abb.branch_id = "'.$branch_id.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_partial_abb.time  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_partial_abb.status IN (1,98)
                        ORDER BY '.$db['fsctaccount'].'.taxinvoice_partial_abb.number_taxinvoice ASC
                     ';

              $datataxro = DB::connection('mysql')->select($sqlro);


              $sqlrs = 'SELECT '.$db['fsctaccount'].'.taxinvoice_special_abb.*

                     FROM '.$db['fsctaccount'].'.taxinvoice_special_abb

                      WHERE '.$db['fsctaccount'].'.taxinvoice_special_abb.branch_id = "'.$branch_id.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_special_abb.date_approved  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_special_abb.status = 1
                        ORDER BY '.$db['fsctaccount'].'.taxinvoice_special_abb.number_taxinvoice ASC
                     ';

              $datataxrs = DB::connection('mysql')->select($sqlrs);


              $sqlss = 'SELECT '.$db['fsctmain'].'.stock_sell_head.*

                     FROM '.$db['fsctmain'].'.stock_sell_head

                      WHERE '.$db['fsctmain'].'.stock_sell_head.branch_id = "'.$branch_id.'"
                        AND '.$db['fsctmain'].'.stock_sell_head.date_approved  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                        AND '.$db['fsctmain'].'.stock_sell_head.status = 1
                        ORDER BY '.$db['fsctmain'].'.stock_sell_head.bill_no ASC
                     ';

              $datataxss = DB::connection('mysql')->select($sqlss);
              // echo "<pre>";
              // print_r($datatresult);
              // exit;

              ?>


              <div align="center" >

							<?php //echo $week;?>
              <!-- <div class="row">
                  <div class="col-md-5">
                      <input type="hidden" name="settime" id="settime" value="<?php //echo date('Y-m-d')?>">
                      <input type="hidden" name="setlogin" id="setlogin" value="<?php //echo $emp_code = Session::get('emp_code')?>">
                  </div>
              </div> -->

              <!-- <table class="table table-striped table-bordered" width="100%" border="1" cellpadding="0"> -->
              <!-- <font size="1" > -->
              <table class="table table-bordered" width="100%" border="1" cellspacing="0">
                  <thead class="thead-inverse" >
                  <tr>
                    <th align="center">ลำดับ</th>
                    <th align="center">ปี/เดือน/วัน</th>
                    <th align="center">เลขที่</th>
                    <th align="center">ชื่อผู้ซื้อสินค้า/ผู้รับบริการ</th>
                    <th align="center">เลขประจำตัวผู้เสียภาษี</th>
                    <th align="center">สถานประกอบการ</th>
                    <th align="center">มูลค่าสินค้าหรือบริการ</th>
                    <th align="center">จำนวนเงินภาษีมูลค่าเพิ่ม</th>
                    <th align="center">จำนวนเงินรวมทั้งหมด</th>
                    <th align="center">หมายเหตุ</th>
                    <th align="center"></th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php

                  $sumtotalloss = 0;
                  $vat = 0;
                  $i = 1;
                  $sumsubtotal = 0;
                  $sumvat = 0;
                  $sumgrandtotal = 0;

                  $sumsubtotalcn = 0;
                  $sumvatcn = 0;
                  $sumgrandtotalcn = 0;

                  $subtotal = 0;

                  $j = 1;

                  ?>

                  <? foreach ($datataxra as $key => $value) { ?>
                    <tr>
                       <td><?php echo $i;?></td><!--ลำดับ-->
                       <td><?php echo ($value->time);?></td><!--ปี/เดือน/วัน-->
                       <td><?php echo ($value->number_taxinvoice);?></td><!--เลขที่-->
                       <td>
                         <?php
                         $modelcustomerra = Maincenter::getdatacustomer($value->customerid);
                               if($modelcustomerra){
                                 echo ($modelcustomerra[0]->per);
                                 echo " ";
                                 echo ($modelcustomerra[0]->name);
                                 echo " ";
                                 echo ($modelcustomerra[0]->lastname);
                               }

                         ?>
                       </td>
                       <td align="center" ><?php echo ($value->customerid);?></td>
                       <td><?php //echo ($modelsupplier[0]->type_branch);?></td>

                       <td align="right" >
                         <?php
                           if($value->status == 1){
                             if($value->discountmoney >= "0"){
                                echo number_format($value->subtotal - $value->discountmoney, 2);
                                $sumsubtotal = $sumsubtotal + ($value->subtotal - $value->discountmoney);
                             }else if(($value->discountmoney == "0")){
                                echo number_format($value->subtotal, 2);
                                $sumsubtotal = $sumsubtotal + ($value->subtotal);
                             }
                           }elseif ($value->status == 98){
                                echo "0.00";
                           }
                           // echo number_format($value->subtotal, 2);
                           // $sumsubtotal = $sumsubtotal + ($value->subtotal);
                         ?>
                       </td>

                       <td align="right" >
                         <?php
                           if($value->status == 1){
                             echo number_format ($value->vatmoney,2);
                             $sumvat = $sumvat + $value->vatmoney;
                           }
                           elseif ($value->status == 98){
                             echo "0.00";
                           }
                         ?>
                       </td>

                       <td align="right" >
                         <?php
                           if($value->status == 1){
                             echo number_format ($value->grandtotal + $value->withholdmoney,2);
                             $sumgrandtotal = $sumgrandtotal + $value->grandtotal + $value->withholdmoney;
                           }
                           elseif ($value->status == 98){
                             echo "0.00";
                           }
                         ?>
                       </td>
                       <td align="center" >
                         <?php
                           // echo ($value->note);
                             echo "-";
                         ?>
                       </td>
                       <td>
                         <?php

                          $modeldatatf = Maincenter::getdatatf($value->bill_rent);

                          if($value->status == 98){
                            echo "ยกเลิกไปออกอย่างเต็ม ";

                            if($modeldatatf){
                              echo ($modeldatatf[0]->number_taxinvoice);
                            }

                          }
                         ?>
                       </td>
                    </tr>

                  <?php $i++; } ?>

                  <?
                  foreach ($datataxtf as $key2 => $value2) { ?>

                  <tr>
                     <td><?php echo $i;?></td><!--ลำดับ-->
                     <td><?php echo ($value2->timereal);?></td><!--ปี/เดือน/วัน-->
                     <td><?php echo ($value2->number_taxinvoice);?></td><!--เลขที่-->
                     <td>
                       <?php
                       // $modelcustomertf = Maincenter::getdatacustomer($value2->customerid);
                       //       if($modelcustomertf){
                       //         echo ($modelcustomertf[0]->name);
                       //         echo " ";
                       //         echo ($modelcustomertf[0]->lastname);
                       //       }

                       echo ($value2->initial);
                       echo " ";
                       echo ($value2->name);

                       ?>
                     </td>
                     <td align="center" ><?php echo ($value2->tax_no);?></td>
                     <td align="center" ><?php echo ($value2->branch_no);?></td>

                     <td align="right" >
                       <?php

                       if($value2->discountmoney >= "0"){
                          echo number_format($value2->subtotal - $value2->discountmoney, 2);
                          $sumsubtotal = $sumsubtotal + ($value2->subtotal - $value2->discountmoney);
                       }else if(($value2->discountmoney == "0")){
                          echo number_format($value2->subtotal, 2);
                          $sumsubtotal = $sumsubtotal + ($value2->subtotal);
                       }

                         // echo number_format($value2->subtotal, 2);
                         // $sumsubtotal = $sumsubtotal + $value2->subtotal;
                       ?>
                     </td>

                     <td align="right" >
                       <?php
                         echo number_format ($value2->vatmoney,2);
                         $sumvat = $sumvat + $value2->vatmoney;
                       ?>
                     </td>

                     <td align="right" >
                       <?php
                         echo number_format ($value2->grandtotal + $value2->withholdmoney,2);
                         $sumgrandtotal = $sumgrandtotal + $value2->grandtotal + $value2->withholdmoney;
                       ?>
                     </td>
                     <td align="center" >
                       <?php
                         // echo ($value2->note);
                           echo "-";
                       ?>
                     </td>
                     <td></td>
                  </tr>

                  <?php $i++; } ?>


                  <?
                  foreach ($datataxcn as $key3 => $value3) { ?>

                  <tr>
                     <td><?php echo $i;?></td><!--ลำดับ-->
                     <td><?php echo ($value3->time);?></td><!--ปี/เดือน/วัน-->
                     <td><?php echo ($value3->number_taxinvoice);?></td><!--เลขที่-->
                     <td>
                       <?php
                         $modelcustomercn = Maincenter::getdata_cn($value3->bill_rent,$value3->type,$value3->ref_tax_type);
                         // echo "<pre>";
                         // print_r($modelcustomercn);
                         // exit;
                              if($modelcustomercn){

                                 if(!empty($modelcustomercn[0]->name)){
                                     echo ($modelcustomercn[0]->initial);
                                     echo " ";
                                     echo ($modelcustomercn[0]->name);
                                 }
                                 else{

                                 $modelcustomer_cn = Maincenter::getdatacustomer($value3->customerid);
                                    if($modelcustomer_cn){
                                       echo ($modelcustomer_cn[0]->per);
                                       echo " ";
                                       echo ($modelcustomer_cn[0]->name);
                                       echo " ";
                                       echo ($modelcustomer_cn[0]->lastname);
                                   }
                                 }

                              }

                       ?>
                     </td>
                     <td align="center" >
                       <?php
                       if($modelcustomercn){

                          if(!empty($modelcustomercn[0]->name)){
                              echo ($modelcustomercn[0]->tax_no);
                          }
                          else{
                              echo ($value3->customerid);
                          }

                       }
                       // echo ($value3->customerid);
                       ?>
                     </td>
                     <td align="center" ><?php echo ($value3->branchcustomer);?></td>

                     <td align="right" >
                       <?php

                         if($value3->discountmoney >= "0"){
                            echo number_format(-($value3->subtotal - $value3->discountmoney), 2);
                            $sumsubtotalcn = $sumsubtotalcn + ($value3->subtotal - $value3->discountmoney);
                         }else if(($value3->discountmoney == "0")){
                            echo number_format(-$value3->subtotal, 2);
                            $sumsubtotalcn = $sumsubtotalcn + ($value3->subtotal);
                         }

                         // echo number_format($value3->subtotal, 2);
                         // $sumsubtotalcn = $sumsubtotalcn + $value3->subtotal;
                       ?>
                     </td>

                     <td align="right" >
                       <?php
                         echo number_format (-$value3->vatmoney,2);
                         $sumvatcn = $sumvatcn + $value3->vatmoney;
                       ?>
                     </td>

                     <td align="right" >
                       <?php
                         echo number_format (-($value3->grandtotal + $value3->withholdmoney),2);
                         $sumgrandtotalcn = $sumgrandtotalcn + $value3->grandtotal + $value3->withholdmoney;
                       ?>
                     </td>
                     <td>
                       <?php
                         echo ($value3->note);
                           // echo "-";
                       ?>
                     </td>
                     <td>
                       <?php
                       $modelcnoftax = Maincenter::gettaxinvoice_all($value3->bill_rent,$value3->type,$value3->ref_tax_type);
                             if($modelcnoftax){
                               echo ($modelcnoftax[0]->number_taxinvoice);
                             }

                       ?>
                     </td>
                  </tr>

                  <?php $i++; } ?>


                  <?php
                  foreach ($datataxtk as $key5 => $value5) { ?>
                    <tr>
                       <td><?php echo $i;?></td><!--ลำดับ-->
                       <td><?php echo ($value5->timereal);?></td><!--ปี/เดือน/วัน-->
                       <td><?php echo ($value5->number_taxinvoice);?></td><!--เลขที่-->
                       <td>
                         <?php
                         // $modelcustomertk = Maincenter::getdatacustomer($value5->customerid);
                         //       if($modelcustomertk){
                         //         echo ($modelcustomertk[0]->name);
                         //         echo " ";
                         //         echo ($modelcustomertk[0]->lastname);
                         //       }

                         echo ($value5->initial);
                         echo " ";
                         echo ($value5->name);

                         ?>
                       </td>
                       <td align="center"><?php echo ($value5->tax_no);?></td>
                       <td align="center"><?php echo $value5->branch_no;?></td>

                       <td align="right" >
                         <?php

                           if($value5->discount > "0"){
                              $subtotal = ($value5->subtotal*$value5->discount)/100;
                              echo number_format($value5->subtotal - $subtotal, 2);
                              $sumsubtotal = $sumsubtotal + ($value5->subtotal - $subtotal);
                           }else if(($value5->discount == "0")){
                              echo number_format($value5->subtotal, 2);
                              $sumsubtotal = $sumsubtotal + ($value5->subtotal);
                           }

                           // echo number_format($value5->subtotal, 2);
                           // $sumsubtotal = $sumsubtotal + ($value5->subtotal);
                         ?>
                       </td>

                       <td align="right" >
                         <?php
                           echo number_format ($value5->vatmoney,2);
                           $sumvat = $sumvat + $value5->vatmoney;
                         ?>
                       </td>

                       <td align="right" >
                         <?php
                           echo number_format ($value5->grandtotal,2);
                           $sumgrandtotal = $sumgrandtotal + $value5->grandtotal;
                         ?>
                       </td>
                       <td align="center">
                         <?php
                           // echo ($value5->note);
                             echo "-";
                         ?>
                       </td>
                       <td></td>
                    </tr>

                  <?php $i++; } ?>

                  <?php
                  foreach ($datataxrl as $key6 => $value6) { ?>
                    <tr>
                       <td><?php echo $i;?></td><!--ลำดับ-->
                       <td><?php echo ($value6->time);?></td><!--ปี/เดือน/วัน-->
                       <td><?php echo ($value6->number_taxinvoice);?></td><!--เลขที่-->
                       <td>
                         <?php
                         $modelcustomerrl = Maincenter::getdatacustomer($value6->customerid);
                               if($modelcustomerrl){
                                 echo ($modelcustomerrl[0]->per);
                                 echo " ";
                                 echo ($modelcustomerrl[0]->name);
                                 echo " ";
                                 echo ($modelcustomerrl[0]->lastname);
                               }

                         ?>
                       </td>
                       <td align="center"><?php echo ($value6->customerid);?></td>
                       <td align="center"><?php //echo ($modelsupplier[0]->type_branch);?></td>

                       <td align="right" >
                         <?php

                           // if($value6->discountmoney >= "0"){
                           //    echo number_format(-$value6->subtotal - $value6->discountmoney, 2);
                           //    $sumsubtotal = $sumsubtotal + ($value6->subtotal - $value6->discountmoney);
                           // }else if(($value6->discountmoney == "0")){
                           //    echo number_format($value6->subtotal, 2);
                           //    $sumsubtotal = $sumsubtotal + ($value6->subtotal);
                           // }

                           if($value6->status == 1){
                             echo number_format($value6->subtotal, 2);
                             $sumsubtotal = $sumsubtotal + ($value6->subtotal);
                           }
                           elseif ($value6->status == 98){
                             echo "0.00";
                           }
                         ?>
                       </td>

                       <td align="right" >
                         <?php
                           if($value6->status == 1){
                             echo number_format ($value6->vatmoney,2);
                             $sumvat = $sumvat + $value6->vatmoney;
                           }
                           elseif ($value6->status == 98){
                             echo "0.00";
                           }
                         ?>
                       </td>

                       <td align="right" >
                         <?php
                           if($value6->status == 1){
                             echo number_format ($value6->grandtotal,2);
                             $sumgrandtotal = $sumgrandtotal + $value6->grandtotal;
                           }elseif ($value6->status == 98){
                             echo "0.00";
                           }
                         ?>
                       </td>
                       <td align="center">
                         <?php
                           // echo ($value6->note);
                             echo "-";
                         ?>
                       </td>
                       <td>
                         <?php
                         $modeldatatk = Maincenter::getdatatk($value6->bill_rent);

                         if($value6->status == 98){
                           echo "ยกเลิกไปออกอย่างเต็ม ";

                           if($modeldatatk){
                             echo ($modeldatatk[0]->number_taxinvoice);
                           }

                         }
                         ?>
                       </td>
                    </tr>

                  <?php $i++; } ?>

                  <?php
                  foreach ($datataxtm as $key7 => $value7) { ?>
                    <tr>
                       <td><?php echo $i;?></td><!--ลำดับ-->
                       <td><?php echo ($value7->timereal);?></td><!--ปี/เดือน/วัน-->
                       <td><?php echo ($value7->number_taxinvoice);?></td><!--เลขที่-->
                       <td>
                         <?php
                         // $modelcustomertm = Maincenter::getdatacustomer($value7->customerid);
                         //       if($modelcustomertm){
                         //         echo ($modelcustomertm[0]->name);
                         //         echo " ";
                         //         echo ($modelcustomertm[0]->lastname);
                         //       }

                         echo ($value7->initial);
                         echo " ";
                         echo ($value7->name);

                         ?>
                       </td>
                       <td align="center"><?php echo ($value7->tax_no);?></td>
                       <td align="center"><?php echo $value7->branch_no;?></td>

                       <td align="right" >
                         <?php

                           if($value7->discountmoney >= "0"){
                              echo number_format($value7->subtotal - $value7->discountmoney, 2);
                              $sumsubtotal = $sumsubtotal + ($value7->subtotal - $value7->discountmoney);
                           }else if(($value7->discountmoney == "0")){
                              echo number_format($value7->subtotal, 2);
                              $sumsubtotal = $sumsubtotal + ($value7->subtotal);
                           }

                           // echo number_format($value7->subtotal, 2);
                           // $sumsubtotal = $sumsubtotal + ($value7->subtotal);
                         ?>
                       </td>

                       <td align="right" >
                         <?php
                           echo number_format ($value7->vatmoney,2);
                           $sumvat = $sumvat + $value7->vatmoney;
                         ?>
                       </td>

                       <td align="right" >
                         <?php
                           echo number_format ($value7->grandtotal + $value7->withholdmoney,2);
                           $sumgrandtotal = $sumgrandtotal + $value7->grandtotal + $value7->withholdmoney;
                         ?>
                       </td>
                       <td align="center">
                         <?php
                           // echo ($value7->note);
                             echo "-";
                         ?>
                       </td>
                       <td></td>
                    </tr>

                  <?php $i++; } ?>

                  <?php
                  foreach ($datataxrn as $key8 => $value8) { ?>
                    <tr>
                       <td><?php echo $i;?></td><!--ลำดับ-->
                       <td><?php echo ($value8->time);?></td><!--ปี/เดือน/วัน-->
                       <td><?php echo ($value8->number_taxinvoice);?></td><!--เลขที่-->
                       <td>
                         <?php
                         $modelcustomerrn = Maincenter::getdatacustomer($value8->customerid);
                               if($modelcustomerrn){
                                 echo ($modelcustomerrn[0]->per);
                                 echo " ";
                                 echo ($modelcustomerrn[0]->name);
                                 echo " ";
                                 echo ($modelcustomerrn[0]->lastname);
                               }

                         ?>
                       </td>
                       <td align="center"><?php echo ($value8->customerid);?></td>
                       <td align="center"><?php //echo $value8->branch_no;?></td>

                       <td align="right" >
                         <?php

                           if($value8->status == 1){
                              if($value8->discountmoney >= "0"){
                                 echo number_format($value8->subtotal - $value8->discountmoney, 2);
                                 $sumsubtotal = $sumsubtotal + ($value8->subtotal - $value8->discountmoney);
                              }else if(($value8->discountmoney == "0")){
                                 echo number_format($value8->subtotal, 2);
                                 $sumsubtotal = $sumsubtotal + ($value8->subtotal);
                              }
                            }elseif ($value8->status == 98) {
                                 echo "0.00";
                            }

                           // echo number_format($value8->subtotal, 2);
                           // $sumsubtotal = $sumsubtotal + ($value8->subtotal);
                         ?>
                       </td>

                       <td align="right" >
                         <?php
                           if($value8->status == 1){
                             echo number_format ($value8->vatmoney,2);
                             $sumvat = $sumvat + $value8->vatmoney;
                           }
                           elseif ($value8->status == 98){
                                echo "0.00";
                           }
                         ?>
                       </td>

                       <td align="right" >
                         <?php
                           if($value8->status == 1){
                             echo number_format ($value8->grandtotal + $value8->withholdmoney,2);
                             $sumgrandtotal = $sumgrandtotal + $value8->grandtotal + $value8->withholdmoney;
                           }
                           elseif ($value8->status == 98){
                                echo "0.00";
                           }
                         ?>
                       </td>
                       <td>
                         <?php
                           echo ($value8->note);
                             // echo "-";
                         ?>
                       </td>
                       <td>
                         <?php
                         $modeldatatm = Maincenter::getdatatm($value8->bill_rent);

                         if($value8->status == 98){
                           echo "ยกเลิกไปออกอย่างเต็ม ";

                           if($modeldatatm){
                             echo ($modeldatatm[0]->number_taxinvoice);
                           }

                         }
                         ?>
                       </td>
                    </tr>

                  <?php $i++; } ?>

                  <?php
                  foreach ($datataxtp as $key9 => $value9) { ?>
                    <tr>
                       <td><?php echo $i;?></td><!--ลำดับ-->
                       <td><?php echo ($value9->timereal);?></td><!--ปี/เดือน/วัน-->
                       <td><?php echo ($value9->number_taxinvoice);?></td><!--เลขที่-->
                       <td>
                         <?php
                         // $modelcustomertp = Maincenter::getdatacustomer($value9->customerid);
                         //       if($modelcustomertp){
                         //         echo ($modelcustomertp[0]->name);
                         //         echo " ";
                         //         echo ($modelcustomertp[0]->lastname);
                         //       }

                         echo ($value9->initial);
                         echo " ";
                         echo ($value9->name);

                         ?>
                       </td>
                       <td align="center"><?php echo ($value9->tax_no);?></td>
                       <td align="center"><?php echo $value9->branch_no;?></td>

                       <td align="right">
                         <?php

                           // if($value9->discountmoney >= "0"){
                           //    echo number_format(-$value9->subtotal - $value9->discountmoney, 2);
                           //    $sumsubtotal = $sumsubtotal + ($value9->subtotal - $value9->discountmoney);
                           // }else if(($value9->discountmoney == "0")){
                           //    echo number_format($value9->subtotal, 2);
                           //    $sumsubtotal = $sumsubtotal + ($value9->subtotal);
                           // }

                           echo number_format($value9->subtotal, 2);
                           $sumsubtotal = $sumsubtotal + ($value9->subtotal);
                         ?>
                       </td>

                       <td align="right">
                         <?php
                           echo number_format ($value9->vatmoney,2);
                           $sumvat = $sumvat + $value9->vatmoney;
                         ?>
                       </td>

                       <td align="right">
                         <?php
                           echo number_format ($value9->grandtotal + $value9->withholdmoney,2);
                           $sumgrandtotal = $sumgrandtotal + $value9->grandtotal + $value9->withholdmoney;
                         ?>
                       </td>
                       <td align="center">
                         <?php
                           // echo ($value9->note);
                             echo "-";
                         ?>
                       </td>
                       <td></td>
                    </tr>

                  <?php $i++; } ?>

                  <?php
                  foreach ($datataxro as $key10 => $value10) { ?>
                    <tr>
                       <td><?php echo $i;?></td><!--ลำดับ-->
                       <td><?php echo ($value10->time);?></td><!--ปี/เดือน/วัน-->
                       <td><?php echo ($value10->number_taxinvoice);?></td><!--เลขที่-->
                       <td>
                         <?php
                         $modelcustomerro = Maincenter::getdatacustomer($value10->customerid);
                               if($modelcustomerro){
                                 echo ($modelcustomerro[0]->per);
                                 echo " ";
                                 echo ($modelcustomerro[0]->name);
                                 echo " ";
                                 echo ($modelcustomerro[0]->lastname);
                               }

                         ?>
                       </td>
                       <td align="center"><?php echo ($value10->customerid);?></td>
                       <td align="center"><?php //echo $value10->branch_no;?></td>

                       <td align="right">
                         <?php

                           // if($value10->discountmoney >= "0"){
                           //    echo number_format(-$value10->subtotal - $value10->discountmoney, 2);
                           //    $sumsubtotal = $sumsubtotal + ($value10->subtotal - $value10->discountmoney);
                           // }else if(($value10->discountmoney == "0")){
                           //    echo number_format($value10->subtotal, 2);
                           //    $sumsubtotal = $sumsubtotal + ($value10->subtotal);
                           // }

                           if($value10->status == 1){
                             echo number_format($value10->subtotal, 2);
                             $sumsubtotal = $sumsubtotal + ($value10->subtotal);
                           }
                           elseif ($value10->status == 98){
                                echo "0.00";
                           }
                         ?>
                       </td>

                       <td align="right">
                         <?php
                           if($value10->status == 1){
                             echo number_format ($value10->vatmoney,2);
                             $sumvat = $sumvat + $value10->vatmoney;
                           }
                           elseif ($value10->status == 98){
                                echo "0.00";
                           }
                         ?>
                       </td>

                       <td align="right">
                         <?php
                           if($value10->status == 1){
                             echo number_format ($value10->grandtotal + $value10->withholdmoney,2);
                             $sumgrandtotal = $sumgrandtotal + $value10->grandtotal + $value10->withholdmoney;
                           }
                           elseif ($value10->status == 98) {
                             echo "0.00";
                           }
                         ?>
                       </td>
                       <td align="center">
                         <?php
                           // echo ($value10->note);
                             echo "-";
                         ?>
                       </td>
                       <td>
                         <?php
                         $modeldatatp = Maincenter::getdatatp($value10->bill_rent);

                         if($value10->status == 98){
                           echo "ยกเลิกไปออกอย่างเต็ม ";

                           if($modeldatatp){
                             echo ($modeldatatp[0]->number_taxinvoice);
                           }

                         }
                         ?>
                       </td>
                    </tr>

                  <?php $i++; } ?>

                  <?php
                  foreach ($datataxrs as $key11 => $value11) { ?>
                    <tr>
                       <td><?php echo $i;?></td><!--ลำดับ-->
                       <td><?php echo ($value11->date_approved);?></td><!--ปี/เดือน/วัน-->
                       <td><?php echo ($value11->number_taxinvoice);?></td><!--เลขที่-->
                       <td>
                         <?php
                         $modelcustomerrs = Maincenter::getdatacustomer($value11->customer_id);
                               if($modelcustomerrs){
                                 echo ($modelcustomerrs[0]->per);
                                 echo " ";
                                 echo ($modelcustomerrs[0]->name);
                                 echo " ";
                                 echo ($modelcustomerrs[0]->lastname);
                               }

                         ?>
                       </td>
                       <td align="center"><?php echo ($value11->customer_id);?></td>
                       <td align="center"><?php //echo $value11->branch_no;?></td>

                       <td align="right" >
                         <?php

                           // if($value11->discountmoney >= "0"){
                           //    echo number_format(-$value11->subtotal - $value11->discountmoney, 2);
                           //    $sumsubtotal = $sumsubtotal + ($value11->subtotal - $value11->discountmoney);
                           // }else if(($value11->discountmoney == "0")){
                           //    echo number_format($value11->subtotal, 2);
                           //    $sumsubtotal = $sumsubtotal + ($value11->subtotal);
                           // }

                           echo number_format($value11->subtotal, 2);
                           $sumsubtotal = $sumsubtotal + ($value11->subtotal);
                         ?>
                       </td>

                       <td align="right" >
                         <?php
                           echo number_format ($value11->vatmoney,2);
                           $sumvat = $sumvat + $value11->vatmoney;
                         ?>
                       </td>

                       <td align="right" >
                         <?php
                           echo number_format ($value11->grandtotal + $value11->withholdmoney,2);
                           $sumgrandtotal = $sumgrandtotal + $value11->grandtotal + $value11->withholdmoney;
                         ?>
                       </td>
                       <td>
                         <?php
                           echo ($value11->note);
                           // echo "-";
                         ?>
                       </td>
                       <td></td>
                    </tr>

                  <?php $i++; } ?>

                  <?php
                  foreach ($datataxss as $key12 => $value12) { ?>
                    <tr>
                       <td><?php echo $i;?></td><!--ลำดับ-->
                       <td><?php echo ($value12->date_approved);?></td><!--ปี/เดือน/วัน-->
                       <td><?php echo ($value12->bill_no);?></td><!--เลขที่-->
                       <td>
                         <?php
                         $modelcustomerss = Maincenter::getdatacustomer($value12->customer_id);
                               if($modelcustomerss){
                                 echo ($modelcustomerss[0]->per);
                                 echo " ";
                                 echo ($modelcustomerss[0]->name);
                                 echo " ";
                                 echo ($modelcustomerss[0]->lastname);
                               }

                         ?>
                       </td>
                       <td align="center"><?php echo ($value12->customer_id);?></td>
                       <td align="center"><?php //echo $value12->branch_no;?></td>

                       <td align="right" >
                         <?php

                           // if($value12->discountmoney >= "0"){
                           //    echo number_format($value12->grandtotal - $value12->discountmoney - $value12->vatmoney, 2);
                           //    $sumsubtotal = $sumsubtotal + ($value12->grandtotal - $value12->discountmoney - $value12->vatmoney);
                           // }else if(($value12->discountmoney == "0")){
                           //    echo number_format($value12->grandtotal - $value12->vatmoney, 2);
                           //    $sumsubtotal = $sumsubtotal + ($value12->grandtotal - $value12->vatmoney);
                           // }

                           echo number_format(($value12->grandtotal - $value12->vatmoney), 2);
                           $sumsubtotal = $sumsubtotal + ($value12->grandtotal - $value12->vatmoney);
                         ?>
                       </td>

                       <td align="right" >
                         <?php
                           echo number_format ($value12->vatmoney,2);
                           $sumvat = $sumvat + $value12->vatmoney;
                         ?>
                       </td>

                       <td align="right" >
                         <?php
                           echo number_format ($value12->grandtotal,2);
                           $sumgrandtotal = $sumgrandtotal + $value12->grandtotal;
                         ?>
                       </td>
                       <td>
                         <?php
                             echo ($value12->note);
                             // echo "-";
                         ?>
                       </td>
                       <td></td>
                    </tr>

                  <?php $i++; } ?>

                  <tr>
                    <td colspan="5" align="right"></td>
                    <td><center><b>รวมรายรับ</b></center></td>
                    <td align="center"><b><?php echo number_format($sumsubtotal,2);  ?></b></td>
                    <td align="center"><b><?php echo number_format($sumvat,2);  ?></b></td>
                    <td align="center"><b><?php echo number_format($sumgrandtotal,2); ?></b></td>
                    <td></td>
                    <td></td>
                  </tr>

                  <tr>
                    <td colspan="5" align="right"></td>
                    <td><center><b>รวมลดหนี้</b></center></td>
                    <td align="center"><b><?php echo number_format(-$sumsubtotalcn,2);  ?></b></td>
                    <td align="center"><b><?php echo number_format(-$sumvatcn,2);  ?></b></td>
                    <td align="center"><b><?php echo number_format(-$sumgrandtotalcn,2); ?></b></td>
                    <td></td>
                    <td></td>
                  </tr>

                  <tr>
                    <td colspan="5" align="right"></td>
                    <td><center><b>รวมทั้งสิ้น</b></center></td>
                    <td align="center"><b><?php echo number_format($sumsubtotal - $sumsubtotalcn,2);  ?></b></td>
                    <td align="center"><b><?php echo number_format($sumvat - $sumvatcn,2);  ?></b></td>
                    <td align="center"><b><?php echo number_format($sumgrandtotal - $sumgrandtotalcn,2); ?></b></td>
                    <td></td>
                    <td></td>
                  </tr>

                      </tbody>
                   </table>
                   <!-- </font> -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
