<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;
$db = Connectdb::Databaseall();
$emp_code = Session::get('emp_code');

?>
@include('headmenu')
<link>
<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}



<link rel="stylesheet" type="text/css" href="css/ui/jquery-ui.css">
{{--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>


<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">


<link rel="stylesheet" type="text/css" href="bower_components/select2/dist/css/select2.min.css">
<script type="text/javascript" src = 'bower_components/select2/dist/js/select2.full.min.js'></script>

<script type="text/javascript" src = 'js/vendor/not_expenses.js'></script>


<style>
    .ui-autocomplete-input {
        border: none;
        font-size: 14px;
        width: 150px;
        height: 24px;
        margin-bottom: 5px;
        padding-top: 2px;
        border: 1px solid #DDD !important;
        padding-top: 0px !important;
        z-index: 1511;
        position: relative;
    }
    .ui-menu .ui-menu-item a {
        font-size: 12px;
    }
    .ui-autocomplete {
        position: absolute;
        top: 0;
        left: 0;
        z-index: 1510 !important;
        float: left;
        display: none;
        min-width: 160px;
        width: 160px;
        padding: 4px 0;
        margin: 2px 0 0 0;
        list-style: none;
        background-color: #ffffff;
        border-color: #ccc;
        border-color: rgba(0, 0, 0, 0.2);
        border-style: solid;
        border-width: 1px;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding;
        background-clip: padding-box;
        *border-right-width: 2px;
        *border-bottom-width: 2px;
    }
    .ui-menu-item > a.ui-corner-all {
        display: block;
        padding: 3px 15px;
        clear: both;
        font-weight: normal;
        line-height: 18px;
        color: #555555;
        white-space: nowrap;
        text-decoration: none;
    }
    .ui-state-hover, .ui-state-active {
        color: #ffffff;
        text-decoration: none;
        background-color: #0088cc;
        border-radius: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        background-image: none;
    }

</style>



<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">งานจัดซื้อ</a>
                    </li>
                    <li class="active">ค่าใช้จ่ายที่ไม่ถือเป็นรายรับ</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        ค่าใช้จ่ายที่ไม่ถือเป็นรายรับ
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body table-responsive">
                                <div class="row">
                                    <div class="col-md-10">

                                    </div>

                                    <div class="col-md-2">
                                        <a href="#" title="เพิ่มข้อมูล" data-toggle="modal" data-target="#myModal"  ><img src="images/global/add.png">เพิ่มข้อมูล</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <br>
                                </div>
                                <?php
                                $brcode = Session::get('brcode');
                                $sqlreservemoney = "SELECT not_expenses.datetime, note, amount FROM $db[fsctaccount].not_expenses WHERE status = '1'";

                                $datareservemoney = DB::connection('mysql')->select($sqlreservemoney);



                                ?>

                                        <table id="example" class="table table-striped table-bordered">
                                            <thead class="thead-inverse">
                                              <tr>
                                                  <td>วันที่</td>
                                                  <td>รายการ</td>
                                                  <td>จำนวนเงิน</td>

                                              </tr>
                                            </thead>

                                            <tbody>
                                            <?php foreach ($datareservemoney as $k => $v) {?>
                                              <tr>
                                                <td>{{$v->datetime}}</td>
                                                <td>{{$v->note}}</td>
                                                <td>{{$v->amount}}</td>
                                              </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')


<!-- Modal -->

<div class="modal fade" id="myModal"  role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog" style="width:60%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">จ่ายเงินสำรอง</h5>
            </div>

            <div class="modal-body">

                <form id="configFormvendors" onsubmit="return getdatesubmit();" data-toggle="validator" method="post" class="form-horizontal">
                    <input value="{{ null }}" type="hidden" id="id" name="id" />
                    <div class="container-fluid">
                      <div class="form-group row">
                        <label for="list" class="col-sm-2">รายการ</label>
                        <div class="col-sm-10">
                          <?php

                              $sqlcom = "SELECT * FROM `listpaypre` WHERE status =1 ";
                              $data = DB::connection('mysql')->select($sqlcom);
                              // print_r($data);
                          ?>
                          <select class="form-control col-sm-10" name="id_compay" id="id_compay" required>
                              <option value="1"> บริษัท ฟ้าใสคอนสตรัคชั่นทูลส์ จำกัด </option>
                              <option value="2"> บริษัท ฟ้าใส แมนูแฟคเจอริ่ง จำกัด </option>

                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="list" class="col-sm-2">รายการ</label>
                        <div class="col-sm-10">
                          <?php
                              $sql = "SELECT * FROM `listexpenses` WHERE status =1 ";
                              $data = DB::connection('mysql')->select($sql);
                              // print_r($data);
                          ?>
                          <select class="form-control col-sm-10" name="list" id="list" required>
                              <option value=""> เลือกรายการ </option>
                            <?php foreach ($data as $key => $value) { ?>
                              <option value="<?php echo $value->id; ?>"> <?php echo $value->listname; ?> </option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="amount" class="col-sm-2">จำนวนเงิน</label>
                        <div class="col-sm-10">
                          <input class="form-control col-sm-10" type="text" id="amount" name="amount" value="" required>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="total" class="col-sm-2">Note</label>
                        <div class="col-sm-10">
                          <input class="form-control col-sm-10" type="text" name="note" id="note" value="" required>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-xs-5 col-xs-offset-3">
                                    <button type="submit" id="Btn_save" class="btn btn-primary">Save</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>




<div class="modal fade" id="myPoref"  role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog" style="width:60%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">PO. ref</h5>
            </div>

            <div class="modal-body">

                <form id="configPo" onsubmit="return getponumbersubmit();" data-toggle="validator" method="post" class="form-horizontal">
                    <input value="{{ null }}" type="hidden" id="reservemoneyid" name="reservemoneyid" />
                    <div class="container-fluid">
                      <div class="form-group row">
                        <label for="amount" class="col-sm-2">PO.</label>
                        <div class="col-sm-10">
                          <input class="form-control col-sm-10" type="text" id="ponumber" name="ponumber" value="" required>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-xs-5 col-xs-offset-3">
                                    <button type="submit" id="Btn_save" class="btn btn-primary">Save</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>






<!---   myModal approved--->
