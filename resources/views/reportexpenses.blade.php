<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Vendorcenter;
use  App\Api\Maincenter;

$db = Connectdb::Databaseall();
?>
@include('headmenu')
<link>

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>

<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">
<script type="text/javascript" src = 'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js'></script>
<script type="text/javascript" src = 'https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js'></script>
<script type="text/javascript" src = 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js'></script>
<script type="text/javascript" src = 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js'></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">

<script type="text/javascript" src = 'js/vendor/reportpopayin.js'></script>

<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<style>
    .modal-ku {
        width: 90%;
        margin: auto;
    }
</style>

<div class="content-wrapper">

<!-- Content Header (Page header) -->
<!-- Main content -->

    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">รายงาน</a>
                    </li>

                </ul>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        รายงานค่าใช้จ่าย
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                    <div class="box-body" style="overflow-x:auto;">
                      <form action="serachreportexpenses" method="post">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">

                      <div class="row">
                        <div class="col-md-4">

                        </div>
                        <div class="col-md-4">
                            <?php
                                $db = Connectdb::Databaseall();
                                $sql = 'SELECT '.$db['hr_base'].'.branch.*
                                   FROM '.$db['hr_base'].'.branch
                                   WHERE '.$db['hr_base'].'.branch.status = "1"';

                                $brcode = DB::connection('mysql')->select($sql);

                            ?>
                            <select name="branch" id="branch" class="form-control" >
                                <option value="*">เลือกทั้งหมด</option>
                                <?php
                                  foreach ($brcode as $key => $value) {
                                ?>
                                  <option value="<?php echo $value->code_branch;?>" <?php if(!empty($data)&&($value->code_branch==$branch)){ echo "selected";}?> > <?php echo $value->name_branch;?></option>

                                <?php }?>
                            </select>
                        </div>
                        <div class="col-md-4">

                        </div>
                      </div>
                      <div class="row">
                          <br>
                      </div>
                      <div class="row">
                        <div class="col-md-4">

                        </div>
                        <div class="col-md-4">
                            <input type="text" name="datepicker" id="datepicker" <?php if(!empty($data)){ echo $datepicker;}?>  class="form-control datepicker" readonly>
                        </div>
                        <div class="col-md-4">

                        </div>
                      </div>
                      <div class="row">
                          <br>
                      </div>
                      <div class="row">
                        <div class="col-md-3">

                        </div>
                        <div class="col-md-3">
                            <input type="submit" value="ค้นหา" class="btn btn-primary pull-right">
                        </div>
                        <div class="col-md-3">
                            <input type="reset" class="btn btn-danger">
                        </div>
                        <div class="col-md-3">

                        </div>
                        <div class="col-md-12" align="right">
                        <?php   if(isset($data)){ ////echo $branch_id; ?>

                                <?php  //if(isset($group_branch_acc_select) && $group_branch_acc_select != ''){?>
                                        <!-- <a href="printcovertaxabb?group_branch_acc_select=<?php //echo $group_branch_acc_select ;?>&&datepickerstart=<?php //echo $datepicker2['start_date'];?>&&datepickerend=<?php  //echo $datepicker2['end_date'];?>"><img src="images/global/printall.png"></a> -->
                                <?php //}else { ?>
                                <?php $path = '&datepicker='.$datepicker.'&branch='.$branch?>
                                        <a href="<?php echo url("/printreportexpenses?$path");?>" target="_blank"><img src="images/global/printall.png"></a>
                                        <!-- <a href="printcovertaxabb?branch_id=<?php //echo $branch_id ;?>&&datepickerstart=<?php //echo $datepicker2['start_date'];?>&&datepickerend=<?php  //echo $datepicker2['end_date'];?> target="_blank" "><img src="images/global/printall.png"></a> -->
                                <?php //} ?>

                        <?php } ?>
                      </div>
                      </div>
                    </form>

                    <div class="row">
                        <br>
                    </div>

                    <div class="row">
                      <?php if(!empty($data)){
                            // echo "<pre>";
                            // print_r($datatresult);
                            // print_r($datareserve);
                            // exit;

                            $db = Connectdb::Databaseall();

                        ?>
                        <table id="example" class="display nowrap" style="width:100% ">
                            <thead>
                                <tr>
                                  <th>ลำดับ</th>
                                  <th>วันที่จ่าย</th>
                                  <th>ประเภทการจ่ายเงิน</th>
                                  <!-- <th>ใบสำคัญจ่าย</th> -->
                                  <th>วันที่บิล</th>
                                  <th>เลขที่บิล</th>
                                  <th>จ่ายให้</th>
                                  <th>เลขประจำตัวผู้เสียภาษี</th>
                                  <th>จำนวนเงิน</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php

                                  $sumtotalloss = 0;
                                  $vat = 0;
                                  $i = 1;
                                  $sumsubtotal = 0;
                                  $sumvat = 0;
                                  $sumgrandtotal = 0;

                                  $sumtransfer = 0;
                                  $sumcash = 0;

                                  $j = 1;

                                  // $sumtotalloss2 = 0;
                                  // $sumsubtotal2 = 0;
                                  // $sumgrandtotal2 = 0;

                                  foreach ($datatresult as $key => $value) { ?>
                                    <tr>
                                       <td><?php echo $i;?></td><!--ลำดับ-->
                                       <td><?php echo ($value->datetime);?></td><!--วันที่จ่าย-->
                                       <td>
                                         <?php
                                                 if($value->type_pay == "1"){
                                                   echo "เงินสด";
                                                 }elseif ($value->type_pay == "2") {
                                                   echo "เงินโอน";
                                                 }

                                                 if($value->wht_percent != "0"){
                                                   echo "<br>";
                                                   echo "ภาษีหัก ณ ที่จ่าย";
                                                 }

                                         ?>
                                       </td>
                                       <!-- <td><?php //echo $i;?></td> -->
                                       <td><?php echo ($value->datebillreceipt);?></td><!--วันที่บิล-->
                                       <td><?php echo ($value->receipt_no);?></td><!--เลขที่-->
                                       <td>
                                         <?php
                                         $modelsupplier = Maincenter::getdatasupplierpo($value->id_po);
                                               if($modelsupplier){
                                                 echo ($modelsupplier[0]->pre);
                                                 echo ($modelsupplier[0]->name_supplier);

                                         ?>
                                       </td>
                                       <td><?php echo ($modelsupplier[0]->tax_id); } ?></td><!--เลขประจำตัวผู้เสียภาษี-->
                                       <td>
                                         <?php
                                           echo number_format($value->payout,2);
                                           $sumgrandtotal = $sumgrandtotal + $value->payout;

                                           if($value->type_pay == "1"){
                                             $sumcash = $sumcash + $value->payout;
                                           }elseif ($value->type_pay == "2") {
                                             $sumtransfer = $sumtransfer + $value->payout;
                                           }

                                           if($value->wht_percent != "0"){
                                             echo "<br>";
                                             echo number_format($value->wht,2);
                                           }
                                         ?>
                                       </td>

                                    </tr>

                                  <?php $i++; } ?>

                                  <?
                                  foreach ($datareserve as $key2 => $value2) { ?>

                                  <tr>
                                     <td><?php echo $i;?></td><!--ลำดับ-->
                                     <td><?php echo ($value2->datetime);?></td><!--วันที่จ่าย-->
                                     <td>
                                       <?php
                                       $modelpo = Maincenter::getdatapo($value2->po_ref);
                                       // echo "<pre>";
                                       // print_r($modelpo);
                                       // exit;
                                       if($modelpo){
                                               if($modelpo[0]->type_buy == "1"){
                                                 echo "เงินสด";
                                               }elseif ($modelpo[0]->type_buy == "2") {
                                                 echo "เงินโอน";
                                               }

                                               if($modelpo[0]->whd != "0.00"){
                                                 echo "<br>";
                                                 echo "ภาษีหัก ณ ที่จ่าย";
                                               }
                                       }

                                       ?>
                                     </td>
                                     <!-- <td><?php //echo $i;?></td> -->
                                     <td><?php echo ($value2->date_bill_no);?></td><!--วันที่บิล-->
                                     <td><?php echo ($value2->bill_no);?></td><!--เลขที่-->
                                     <td>
                                       <?php
                                       $modelsuppliername = Maincenter::getdatasupplierpo($value2->po_ref);
                                             if($modelsuppliername){
                                               echo ($modelsuppliername[0]->pre);
                                               echo ($modelsuppliername[0]->name_supplier);

                                       ?>
                                     </td>
                                     <td><?php echo ($modelsuppliername[0]->tax_id); }?></td>

                                     <?php  //$vat = (($value2->vat_money * 7 )/ 107); ?>

                                     <td>
                                       <?php
                                         echo ($value2->total);
                                         $sumgrandtotal = $sumgrandtotal + $value2->total;

                                         if($modelpo){
                                             // if($modelpo[0]->whd != "0.00"){
                                             //   echo $modelpo[0]->whd;
                                             // }

                                             if($modelpo[0]->type_buy == "1"){
                                               $sumcash = $sumcash + $value2->total;
                                             }elseif ($modelpo[0]->type_buy == "2") {
                                               $sumtransfer = $sumtransfer + $value2->total;
                                             }

                                             $modelwth = Maincenter::getdatawth($value2->po_ref);

                                             if ($modelwth) {
                                                 echo "<br>";
                                                 // $withthis = ($sumtotalthis)*(0.05);// ((100*7)/100)
                                                 echo number_format (($modelwth[0]->total),2);
                                             }

                                             // if ($modelpo[0]->whd == 10) {
                                             //     echo "<br>";
                                             //     // $withthis = ($sumtotalthis)*(0.05);// ((100*7)/100)
                                             //     echo number_format (($modelpo[0]->totolsumall)*(0.10),2);
                                             // }
                                             //
                                             // if ($modelpo[0]->whd == 5) {
                                             //     echo "<br>";
                                             //     // $withthis = ($sumtotalthis)*(0.05);// ((100*7)/100)
                                             //     echo number_format (($modelpo[0]->totolsumall)*(0.05),2);
                                             // }
                                             //
                                             // if ($modelpo[0]->whd == 3) {
                                             //     echo "<br>";
                                             //     // $withthis = ($sumtotalthis)*(0.05);// ((100*7)/100)
                                             //     echo number_format (($modelpo[0]->totolsumall)*(0.03),2);
                                             // }
                                             //
                                             // if ($modelpo[0]->whd == 1) {
                                             //     echo "<br>";
                                             //     // $withthis = ($sumtotalthis)*(0.05);// ((100*7)/100)
                                             //     echo number_format (($sumtotalthis)*(0.01),2);
                                             // }
                                         }
                                       ?>
                                     </td>

                                  </tr>

                                  <?php $i++; } ?>

                                     <!-- <tr>
                                       <td colspan="7" align="right"></td>
                                       <td><center><b>รวม</b></center></td>
                                       <?php //echo number_format($sumsubtotal,2);  ?>
                                       <?php //echo number_format($sumvat,2);  ?>
                                       <td><b><?php //echo number_format($sumgrandtotal,2); ?></b></td>
                                     </tr> -->

                            </tbody>
                        </table>

                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')
