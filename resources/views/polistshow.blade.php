<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Vendorcenter;

$db = Connectdb::Databaseall();
?>
@include('headmenu')
<link>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}
<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>

<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">

<script type="text/javascript" src = 'js/vendor/polist.js'></script>


<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<style>
    .modal-ku {
        width: 90%;
        margin: auto;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">งานจัดซื้อ</a>
                    </li>

                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body" style="overflow-x:auto;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        รายการอนุมติเสนอใบ PO
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body">
                              <form action="searchbcshow" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-3">
                                  <?php

                                      $sqlbr ='SELECT '.$db['hr_base'].'.branch.*
                                      FROM '.$db['hr_base'].'.branch
                                      WHERE status != "99" ';
                                      $modelbr = DB::connection('mysql')->select($sqlbr);
                                  ?>
                                    <select name="branchselect" id="branchselect" class="form-control" required>
                                        <option value=''>== เลือก สาขา ==</option>
                                        <?php foreach ($modelbr as $k => $v) {?>
                                        <option value='<?php echo $v->code_branch;?>' <?php
                                        if(isset($querybr)){
                                            if($branchselect==$v->code_branch){ echo "selected";}
                                        }
                                      ?>
                                        ><?php echo $v->name_branch;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <input type="submit" class="btn btn-success" value="ค้นหา">
                                </div>
                                <div class="col-md-3">
                                </div>
                              </form>
                            </div>
                            <div class="box-body">

                                <?php

                                $sql ='SELECT '.$db['fsctaccount'].'.po_head.*
                                      FROM '.$db['fsctaccount'].'.po_head  WHERE status_head != "99" ';

                                if(isset($querybr)){
                                    if($branchselect==$v->code_branch){ echo "selected";}
                                      $sql .= " AND   branch_id = '$branchselect' ";
                                }
                                $sql .= ' ORDER BY po_head.id DESC';



                                $model = DB::connection('mysql')->select($sql);

                                ?>
                                <form action="saveapprovedpo" method="post">
                                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <table class="table table-striped">
                                  <thead>
                                    <tr>
                                      <th>เลขที่เอกสาร</th>
                                      <th>วันที่ขอ</th>
                                      <th>ชื่อผู้ขาย</th>
                                      <th>รายการที่ขอ</th>
                                      <th>ราคารวม</th>
                                      <th>สถานะ</th>
                                      <th>รหัสพนักงานที่ขอ</th>
                                      <th>สถานะการขอ</th>
                                      <th>รายละเอียด</th>

                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php
                                    $arraytranfersum = 0;
                                    $arraywaitsum = 0;
                                    $arrwaitapproved = 0;
                                    $arrpay = 0;
                                    $arrreturn = 0;
                                    foreach ($model as $key => $value) { ?>
                                    <tr>
                                      <td><?php echo $value->po_number;?></td>
                                      <td><?php echo $value->date;?></td>
                                      <td>
                                          <?php
                                              $supplier_id = $value->supplier_id;
                                              $sqlsupplier = "SELECT $db[fsctaccount].supplier.*
                                                           FROM $db[fsctaccount].supplier
                                                           WHERE $db[fsctaccount].supplier.id = '$supplier_id'";

                                              $datasup = DB::connection('mysql')->select($sqlsupplier);
                                              // echo "<pre>";
                                              echo $datasup[0]->pre.'&nbsp;&nbsp;'.$datasup[0]->name_supplier;


                                          ?>
                                      </td>
                                      <td>
                                        <?php  $idpo = $value->id;
                                        $sqldetail ="SELECT $db[fsctaccount].po_detail.*
                                        FROM $db[fsctaccount].po_detail
                                        WHERE po_headid = '$idpo' AND statususe = '1' ";

                                        $modeldetail = DB::connection('mysql')->select($sqldetail);
                                            foreach ($modeldetail as $e => $l) {
                                                echo "<li>".$l->list."<font color='red'>[".$l->note."]</font>"."</li><br>";
                                            }

                                         ?>
                                      </td>
                                      <td>
                                        <?php echo $value->totolsumall;

                                        ?>
                                     </td>
                                      <td><?php
                                        if($value->status_head==0){
                                              echo "<span>รออนุมัติ</span>";
                                                   $arrwaitapproved = $arrwaitapproved + $value->totolsumall;
                                        }else if($value->status_head==1){
                                              echo "<span class='text-info'>อนุมัติแล้ว รอโอน</span>";
                                                   $arraywaitsum = $arraywaitsum + $value->totolsumall;
                                        }else if($value->status_head==2){
                                              echo "<span class='text-success'>โอนแล้ว</span>";
                                                   $arraytranfersum = $arraytranfersum + $value->totolsumall;
                                        }else if($value->status_head==3){
                                                $datapaybill = Vendorcenter::getpaybillemp($value->id);
                                                if($datapaybill[0]->sumpay_real != ''){
                                                    $setpayemp = $datapaybill[0]->sumpay_real;
                                                    // print_r(number_format($datapaybill[0]->sumpay_real,2));
                                                }else{
                                                    $setpayemp = 0;
                                                }
                                              $totolsumall = $value->totolsumall;
                                              $arrpay = $arrpay + $setpayemp;
                                              $arrreturn = $arrreturn + ($totolsumall-$setpayemp);
                                              $setpayemp = $setpayemp;
                                              echo "<span style='color:#4b0082;'>จ่ายแล้ว   ";
                                              echo "จำนวนเงินที่ต้องจ่าย  ".$value->totolsumall."<br>";
                                              echo "จ่ายจริง ".$setpayemp."<br>";
                                              echo "ส่วนต่าง  ".($totolsumall-$setpayemp)."<br>";
                                              echo "</span>";
                                        }else if($value->status_head==4) {
                                              $datapaybill = Vendorcenter::getpaybillemp($value->id);
                                              if($datapaybill[0]->sumpay_real != ''){
                                                  $setpayemp = $datapaybill[0]->sumpay_real;
                                                  // print_r(number_format($datapaybill[0]->sumpay_real,2));
                                              }else{
                                                  $setpayemp = 0;
                                              }
                                            $totolsumall = $value->totolsumall;
                                            $arrpay = $arrpay + $setpayemp;
                                            $arrreturn = $arrreturn + ($totolsumall-$setpayemp);
                                            $setpayemp = $setpayemp;
                                            echo "<span color='#4b0082'>มีเงินทอน</span>";
                                            echo "จำนวนเงินที่ต้องจ่าย  ".$value->totolsumall."<br>";
                                            echo "จ่ายจริง ".$setpayemp."<br>";
                                            echo "ส่วนต่าง  ".($totolsumall-$setpayemp)."<br>";
                                            echo "</span>";

                                        }else if($value->status_head==5){
                                            $datapaybill = Vendorcenter::getpaybillemp($value->id);
                                            if($datapaybill[0]->sumpay_real != ''){
                                                $setpayemp = $datapaybill[0]->sumpay_real;
                                                // print_r(number_format($datapaybill[0]->sumpay_real,2));
                                            }else{
                                                $setpayemp = 0;
                                            }
                                          $totolsumall = $value->totolsumall;
                                          $arrpay = $arrpay + $setpayemp;
                                          $arrreturn = $arrreturn + ($totolsumall-$setpayemp);
                                          $setpayemp = $setpayemp;
                                          echo "<span color='#4b0082'>ขาด</span>";
                                          echo "จำนวนเงินที่ต้องจ่าย  ".$value->totolsumall."<br>";
                                          echo "จ่ายจริง ".$setpayemp."<br>";
                                          echo "ส่วนต่าง  ".($totolsumall-$setpayemp)."<br>";
                                          echo "</span>";

                                        }else{
                                              echo "<span class='text-danger'>ยกเลิก</span>";

                                        }

                                      ?></td>
                                      <td><?php echo $value->emp_code_po;?></td>
                                      <td><?php if($value->id==1){ echo "<font color='red'>เร่งด่วน</font>"; }else{ echo "ปกติ"; }?></td>
                                      <td><a href="printpo/<?php echo $value->id;?>" target="_blank"> <img src="images/global/edit-bill.png"></a></td>

                                    </tr>
                                    <?php } ?>
                                  </tbody>
                                </table>

                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-3">
                                           <font color="red"> รออนุมัติ <?php echo number_format($arrwaitapproved,2);?></font>
                                    </div>
                                    <div class="col-md-3">
                                           <font color="green"> อนุมัติแล้ว รอโอน <?php echo number_format($arraywaitsum,2);?></font>
                                    </div>
                                    <div class="col-md-3">
                                           <font color="blue"> โอนแล้ว <?php echo number_format($arraytranfersum,2);?></font>
                                    </div>
                                    <div class="col-md-3">
                                           <font > จ่ายจริง <?php echo number_format($arrpay,2);?>
                                                  <br>
                                                  ส่วนต่าง <?php echo number_format($arrreturn,2);?>
                                           </font>
                                    </div>
                                </div>

                              </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')
