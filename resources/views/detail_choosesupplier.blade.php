<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;


?>
@include('headmenu')
<link>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>


<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">


<link rel="stylesheet" type="text/css" href="bower_components/select2/dist/css/select2.min.css">
<script type="text/javascript" src = 'bower_components/select2/dist/js/select2.full.min.js'></script>

<script type="text/javascript" src = 'js/vendor/detail_choosesupplier.js'></script>


<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
{{--<style>--}}
    {{--.modal-ku {--}}
        {{--width: 1024px;--}}
        {{--margin: auto;--}}
    {{--}--}}
{{--</style>--}}
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">งานจัดซื้อ</a>
                    </li>
                    <li class="active">ข้อมูลออกใบ PO</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">
                <input type="hidden" id="dateset" value="<?php echo date('Y-m-d')?>">
                <input type="hidden"  class="form-control"  id="yearset" value="<?php echo date('Y')?>"  />
                <input type="hidden"  class="form-control" id="year_thset" value="<?php echo date('Y',strtotime("+543 year"))?>"  />

                <input type="hidden"  class="form-control" id="dateshowset" value="<?php echo date('d-m-Y',strtotime("+543 year"))?>"  disabled/>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        ข้อมูลการจัดซื้อ
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body table-responsive">
                                <div class="row">
                                    <div class="col-md-10">

                                    </div>

                          <!--          <div class="col-md-2">
                                        <a href="#" title="เพิ่มข้อมูล" data-toggle="modal" data-target="#myModal" onclick="insertnew()" ><img src="images/global/add.png">เพิ่มข้อมูล</a>
                                    </div>-->
                                </div>
                                <div class="row">
                                    <br>
                                </div>
                                        <table id="example" class="table table-striped table-bordered">
                                            <thead class="thead-inverse">
                                            <tr>
                                                <td class="text-center">#</td>
                                                <td class="text-center">วันที่</td>
                                                <td class="text-center">No.</td>
                                                <td class="text-center">ชื่อ vendor</td>
                                                <td class="text-center">Terms</td>
                                                <td class="text-center">พิมพ์ใบคัดเลือก vendor</td>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $db = Connectdb::Databaseall();

                                            $data = DB::connection('mysql')->select('SELECT supplier.id as supplier_id , supplier.createdate, supplier_terms.day as terms, supplier.ps_number as ps_number  FROM '.$db['fsctaccount'].'.supplier INNER JOIN '.$db['fsctaccount'].'.supplier_terms ON (supplier.terms_id = supplier_terms.id) ORDER BY supplier.id DESC');
                                            //dd('SELECT supplier.id FROM '.$db['fsctaccount'].'.supplier INNER JOIN '.$db['fsctaccount'].'.supplier_terms ON (supplier.terms_id = supplier_terms.id) ORDER BY id DESC');
                                            $i = 1;
                                            ?>
                                            @foreach ($data as $value)
                                                <tr>
                                                    <td scope="row">{{ $i }}</td>
                                                    <td align="center">{{ $value->createdate}}</td>

                                                    <td align="center">{{ $value->ps_number }}</td>
                                                    <td align="center"><?php $datasupp = Vendorcenter::getdatavendorcenter($value->supplier_id);
                                                    //print_r($datasupp);
                                                     print_r($datasupp[0]->pre."     ".$datasupp[0]->name_supplier)
                                                    ?></td>

                                                    <td align="center">{{ $value->terms }}</td>
                                                    <td align="center"><a href="<?php echo url("/printchoosesupplier/$value->supplier_id");?>"  target="_blank"><img src="images/global/po.png"></a></td>
                                                </tr>
                                                <?php $i++; ?>
                                            @endforeach
                                            </tbody>
                                        </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')


<!-- Modal -->

<div class="modal fade" id="myModal"  role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog"  style="width:1330px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">ข้อมูลใบ PO </h5>
            </div>

            <div class="modal-body">


                    <input value="{{ null }}" type="hidden" id="id" name="id" />
                    <input type="hidden" id="po_no_branch" name="po_no_branch" >
                    <input type="hidden" name="statusupdate" id="statusupdate" value="0">
                    <div class="row">
                        <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-xs-3 control-label">วันที่ขอใบ PO<i><span style="color: red">*</span></i></label>
                            <div class="col-xs-5">
                                <input type="text"  class="form-control" id="dateshow" value="<?php echo date('d-m-Y',strtotime("+543 year"))?>"  disabled/>
                                <input type="hidden"  class="form-control" name="date" id="date" value="<?php echo date('Y-m-d')?>"  />
                                <input type="hidden"  class="form-control" name="year" id="year" value="<?php echo date('Y')?>"  />
                                <input type="hidden"  class="form-control" name="year_th" id="year_th" value="<?php echo date('Y',strtotime("+543 year"))?>"  />
                            </div>
                            <div class="col-xs-4">

                            </div>
                        </div>
                        </div>
                        <div class="col-md-1">

                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-1">

                        </div>
                        <div class="col-md-4">
                            <div class="pull-right">รหัสสาขา <?php echo $brcode = Session::get('brcode');?>
                                <input type="hidden"  class="form-control" name="branch_id" id="branch_id" value="<?php echo $brcode ;?>"  />
                                (
                                <?php
                                $databr = Maincenter::databranchbycode($brcode);
                                print_r($databr[0]->name_branch);
                                ?>)
                            </div>
                        </div>
                    </div>
                    <div>
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">เลือกบริษัท<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-8">
                                    <select name="id_company" id="id_company" class="form-control" onchange="selecttranfer(this)">
                                        <?php
                                          $db = Connectdb::Databaseall();
                                          $data = DB::connection('mysql')->select('SELECT name, id FROM '.$db['hr_base'].'.working_company WHERE status ="1"');
                                        ?>
                                        @foreach ($data as $value)
                                            <option value="{{$value->id}}">{{$value->name}}</option>

                                        @endforeach

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">
                                <b>เลือก งวดการจ่ายเงิน.</b>
                        </div>
                        <div class="col-md-3">
                              <select name="week" id="week" class="form-control select2" required>
                                  <option value="" select>เลือกงวดการจ่ายเงิน</option>
                                  <?php
                                    $db = Connectdb::Databaseall();
                                    $dataweek = DB::connection('mysql')->select('SELECT week FROM '.$db['fsctaccount'].'.ppr_head GROUP BY week ORDER BY ppr_head.week DESC');
                                  ?>
                                  @foreach($dataweek as $week)
                                    <option value="{{$week->week}}">{{$week->week}}</option>
                                  @endforeach
                              </select>
                        </div>
                        <div class="col-md-1">
                            <div class="pull-right">
                                <b> เลือก Seller/ผู้ขาย.</b>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <select name="supplier_id" id="supplier_id" class="form-control select2" required>
                                <option value="" select>เลือก supplier</option>
                                <?php
                                  $db = Connectdb::Databaseall();
                                  $datasupp = DB::connection('mysql')->select('SELECT pre, id, name_supplier  FROM '.$db['fsctaccount'].'.supplier  WHERE status = "1"');
                                ?>
                                @foreach($datasupp as $supp)
                                  <option value="{{$supp->id}}">{{$supp->pre}}   {{$supp->name_supplier}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-1">
                            <button class="btn btn-primary" id="serachpridref" onclick="serachprid()">ค้นหา</button>
                        </div>
                    </div>
                    <div>
                        <br>
                    </div>

                    <div class="row detailshow" style="display: none;">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label"> เลขที่ใบ PO.<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" id="po_number" name="po_number" disabled>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">ประเภทการจ่าย<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                  <select class="form-control" id="type_pay" name="type_pay" required>
                                      <option value="">เลือก ประเภทการจ่าย</option>
                                      <?php
                                      $db = Connectdb::Databaseall();
                                      $data = DB::connection('mysql')->select('SELECT name_pay, id FROM '.$db['fsctaccount'].'.type_pay WHERE status ="1"');
                                      ?>
                                      @foreach ($data as $value)
                                          <option value="{{$value->id}}">{{$value->name_pay}}</option>

                                      @endforeach
                                  </select>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">ประเภทการซื้อ<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <select class="form-control" id="type_buy" name="type_buy" required>
                                        <option value="">เลือก ประเภทการซื้อ</option>
                                        <?php
                                        $db = Connectdb::Databaseall();
                                        $data = DB::connection('mysql')->select('SELECT name_buy, id FROM '.$db['fsctaccount'].'.type_buy  WHERE status ="1"');
                                        ?>
                                        @foreach ($data as $value)
                                            <option value="{{$value->id}}">{{$value->name_buy}}</option>

                                        @endforeach


                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row detailshow" style="display: none;">
                        <br>
                    </div>
                    <div class="row detailshow" style="display: none;">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">การจัดซื้อ(ใน/นอก ประเทศ)<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input  type="radio" name="in_house" id="in_house1" value="1" checked>  ภายในประเทศ
                                    <br>
                                    <input  type="radio" name="in_house" id="in_house2" value="2" >  ต่างประเทศ
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">งบประมาณ<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                <input  type="radio" name="in_budget" id="in_budget1" value="1" checked>  ภายในงบประมาณ
                                <br>
                                <input  type="radio" name="in_budget" id="in_budget2" value="2" >  นอกงบประมาณ
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">การจัดซื้อ(เร่งด่วน/ปกติ)<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                <input  type="radio" name="urgent_status" id="urgent_status1" value="1" >  เร่งด่วน
                                <br>
                                <input  type="radio" name="urgent_status" id="urgent_status2" value="2" checked>  ปกติ
                                </div>
                            </div>
                        </div>
                    </div>
                    <div  class="row detailshow" style="display: none;">
                        <br>
                    </div>


                    <div class="row detailshow" style="display: none;">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">เงื่อนไขในการส่ง<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">

                                    <select name="type_transfer" id="type_transfer" class="form-control" >
                                        <option value="">เลือกเงื่อนไขในการส่ง</option>
                                        <?php
                                        $db = Connectdb::Databaseall();
                                        $datatransfer = DB::connection('mysql')->select('SELECT name, id FROM '.$db['fsctaccount'].'.transfer_config WHERE status ="1"');
                                        ?>
                                        @foreach ($datatransfer as $valuetransfer)
                                            <option value="{{$valuetransfer->id}}">{{$valuetransfer->name}}</option>

                                        @endforeach

                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">เครดิตการชำระเงิน<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" id="terms_show" name="terms_show" disabled>
                                    <input type="hidden" class="form-control" id="terms" name="terms" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">

                            </div>
                        </div>
                    </div>
                    <div class="row detailshow" style="display: none;">
                        <br>
                    </div>

                    <div class="row detailshow" style="display: none;" >
                        <div class="col-md-2">
                            <div class="pull-right">
                            <b>ส่งที่สำนักงานใหญ่</b>
                                <input type="checkbox" id="ckaddbranch" onclick="ckaddress()" checked>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <input type="text" name="address_send" id="address_send" class="form-control" readonly>

                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                    <div class="row detailshow" style="display: none;">
                        <br>
                    </div>

                    <div class="row detailshow" style="display: none;">
                        <div class="col-md-12" >
                            <table class="table" id="thdetail">
                                <thead>
                                <tr>
                                    <td align="center">ประเภทงานจัดซื้อ</td>

                                    <td align="center">รายการ</td>
                                    <td align="center" colspan="2">จำนวนนับ</td>
                                    <td align="center">ราคาต่อหน่วย</td>
                                    <td align="center">หัก ณ ที่จ่าย</td>
                                    <td align="center">ราคาหัก ณ ที่จ่าย</td>
                                    <td align="center">รวม</td>
                                    <td align="center">ยอดที่รับมา</td>
                                    <td align="center">ขาด</td>
                                    <td align="center">สถานะ</td>
                                    <td align="center">PR. ref</td>
                                </tr>
                                </thead>
                                <tbody id="tdbody">
                                 <tr>
                                     <td align="center">
                                         <select name="config_group_supp_id[]" id="config_group_supp_id0" class="form-control" disabled>
                                             <option value="">เลือกประเภทการจัดซื้อ</option>
                                             <?php
                                             $db = Connectdb::Databaseall();
                                             $data = DB::connection('mysql')->select('SELECT name, id FROM '.$db['fsctaccount'].'.config_group_supp WHERE status = "1" ');
                                             ?>
                                             @foreach ($data as $value)
                                                 <option value="{{$value->id}}">{{$value->name}}</option>
                                             @endforeach
                                         </select>

                                     </td>
                                     <td align="center">
                                         <input type="hidden" name="material_id[]" id="materialid0" value="0">
                                         <input type="text" name="list[]" id="list0"  class="form-control" readonly>
                                     </td>
                                     <td align="center">
                                         <input type="text" name="amount[]" id="amount0" onblur="getamount(this,0)" class="form-control" style="width: 90px;" placeholder="จำนวน" readonly>
                                     </td>
                                     <td align="center">
                                         <input type="text" name="type_amount[]" id="type_amount0"  class="form-control" style="width: 80px;" placeholder="หน่วยนับ" readonly>
                                     </td>
                                     <td align="center">
                                         <input type="text" name="price[]" id="price0" onblur="getprice(this,0)"    class="form-control"  >
                                     </td>

                                     <td align="center">
                                         <select name="withhold[]" id="withhold0" class="form-control" onchange="calculatewithhold(this,0)" style="width: 90px;" >
                                             <option value="0" selected>ไม่มีหัก</option>
                                             <?php
                                             $db = Connectdb::Databaseall();
                                             $datawithhold = DB::connection('mysql')->select('SELECT Note, withhold FROM '.$db['fsctaccount'].'.withhold WHERE status = "1"');
                                             ?>
                                             @foreach ($datawithhold as $value)
                                                 <option value="{{$value->withhold}}">{{$value->Note}}</option>
                                             @endforeach
                                         </select>
                                     </td>
                                     <td align="center">
                                         <input type="text" name="resultwithhold[]"  id="resultwithhold0" class="form-control"  readonly>
                                     </td>
                                     <td align="center">
                                         <input type="text" name="total[]"  id="total0" class="form-control"  readonly>
                                     </td>
                                     <td align="center">
                                         <input type="text" class="form-control quantity"  name="quantity_get[]" id="quantity0" onblur="caldifamount(0)" value="0" readonly>
                                     </td>
                                     <td align="center">
                                         <input type="text" class="form-control"  name="quantity_loss[]" id="loss0" readonly>
                                     </td>
                                     <td align="center">
                                         <span id="statuspodetai0">
                                             <font color="red">ยังไม่ครบ</font>
                                         </span>
                                     </td>
                                     <td align="center">
                                          <span id="modalprref0"> </span>
                                     </td>
                                 </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row detailshow" style="display: none;">
                        <br>
                    </div>
                    <div class="row" >
                      <div class="col-md-1">
                        ภาษี
                      </div>
                      <div class="col-md-2">
                        <select name="vat" id="vat" class="form-control" onchange="calculatevat(this,0)">
                            <option value="0">ไม่มี vat</option>
                            <?php
                            $db = Connectdb::Databaseall();
                            $data = DB::connection('mysql')->select('SELECT tax FROM '.$db['fsctaccount'].'.tax_config WHERE status = "1"');
                            ?>
                            @foreach ($data as $value)
                                <option value="{{$value->tax}}">{{$value->tax}}</option>
                            @endforeach

                        </select>
                      </div>
                      <div class="col-md-9">

                      </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row detailshow" style="display: none;">
                        <div class="col-md-1">
                            ราคารวม
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control" id="totolsumall" name="totolsumall" readonly>
                        </div>
                        <div class="col-md-1">

                        </div>
                        <div class="col-md-1">
                            MD/BOSS โอน
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control" id="payreal" name="payreal" onblur="caldiffpay()">
                        </div>
                        <div class="col-md-1">

                        </div>
                        <div class="col-md-1">
                            ถอน/ขาด
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control" id="diffpay" name="diffpay" readonly>
                        </div>
                        <div class="col-md-1">

                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row detailshow" style="display: none;">
                        <div class="col-md-4">

                        </div>
                        <div class="col-md-4">
                          ผู้อนุมัติใบ PO
                          <?php
                          $db = Connectdb::Databaseall();
                          $dataemp_approved_po = DB::connection('mysql')->select('SELECT code_emp FROM '.$db['fsctaccount'].'.emp_approved_po WHERE status = "1"');
                           ?>
                          <select name="code_emp_approve" id="code_emp_approve" class="form-control">
                                  <option value="0">เลือกผู้อนุมัติ</option>
                                  <?php foreach ($dataemp_approved_po as $K1 => $V1) { ?>
                                      <option value="<?php echo $V1->code_emp;?>"><?php echo $V1->code_emp;?></option>
                                  <?php }?>
                          </select>
                        </div>
                        <div class="col-md-4">

                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row detailshow" style="display: none;">
                        <div class="col-md-6">
                            <div class="pull-right">
                            รหัสพนักงานที่ขอใบ po:
                                <input type="hidden" name="code_emp_pr" id="code_emp_pr">
                                <input type="text" class="form-control" name="code_emp_po" id="code_emp_po" value="<?php echo $brcode = Session::get('emp_code')?>" readonly>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="pull-left">
                            รหัสซุปประจำสาขา:
                            <input type="text" class="form-control" name="code_sup" id="code_sup" placeholder="รหัสซุปประจำสาขา">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>

                    <div class="row">
                        <br>
                    </div>
                    <div class="row" style="display: none;" id="showbill">
                        <div class="col-md-3">

                        </div>
                        <div class="col-md-6">
                            <table width="100%">
                                <thead>
                                    <tr>
                                        <td width="16.6%">วันที่</td>
                                        <td width="16.6%">เลขที่บิล</td>
                                        <td width="16.6%">จ่ายจริงตามบิล</td>
                                        <td width="16.6%">คืนเงิน</td>
                                        <td width="16.6%">พิมพ์ใบรับของ</td>
                                        <td width="16.6%"></td>
                                    </tr>
                                </thead>
                                <tbody id="addrowbilltb">
                                    <tr>
                                        <td width="16.6%">
                                            <input type="hidden" class="form-control" id="datebill0" name="datebill[]" readonly value="<?php echo date('Y-m-d') ?>">
                                            <input type="text" class="form-control" id="datebillshow0" name="datebillshow[]" readonly value="<?php echo date('d-m-Y',strtotime("+543 year"))?>">
                                        </td>
                                        <td width="16.6%">
                                            <input type="text" class="form-control" id="billref0" name="billref[]" placeholder="บิล No.">
                                        </td>
                                        <td width="16.6%">
                                            <input type="text" class="form-control" id="payperbill0" name="payperbill[]" placeholder="จ่ายจริง">
                                        </td>
                                        <td width="16.6%">
                                            <input type="text" class="form-control" id="returnperbill0" name="returnperbill[]" placeholder="จ่ายคืน">
                                        </td>
                                        <td width="16.6%" align="center"><span id="imgprintrecip0"></td>
                                        <td width="16.6%"></td>
                                    </tr>
                                </tbody>

                            </table>
                        </div>
                        <div class="col-md-3">
                            <a href="#" title="เพิ่มบิล" onclick="addnewrow()"><img src="images/global/add.png">เพิ่มบิล</a>
                        </div>

                    </div>
                <div class="row">
                    <br>
                </div>
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-xs-5 col-xs-offset-3">
                                <button type="submit" id="Btn_save" class="btn btn-primary" onclick="savepo()" >Save</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<!---   myModal approved --->


<div class="modal fade" id="prdoc" role="dialog">
    <div class="modal-dialog modal-lg">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">ใบ PR ที่เกี่ยงข้อง</h4>
        </div>
        <div class="modal-body">
            <div class="row" id="showprref">
              <table width="100%" class="table table-bordered">
                 <thead>
                  <tr>
                      <td>ใบ PR. </td>
                      <td>สาขา</td>
                      <td>ชื่อสาขา</td>
                      <td>รายการ</td>
                      <td>ราคา</td>
                      <td>จำนวน</td>
                      <td>หัก ณ ที่จ่าย %</td>
                      <td>ภาษี %</td>
                      <td>รวม</td>
                  </tr>
                </thead>
                <tbody id="tbodyshowprref">
                </tbody>
              </table>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
