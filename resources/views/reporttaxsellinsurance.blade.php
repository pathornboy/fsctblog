<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Vendorcenter;
use  App\Api\Maincenter;



$db = Connectdb::Databaseall();
?>
@include('headmenu')
<link>

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>

<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">
<script type="text/javascript" src = 'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js'></script>
<script type="text/javascript" src = 'https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js'></script>
<script type="text/javascript" src = 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js'></script>
<script type="text/javascript" src = 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js'></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">

<script type="text/javascript" src = 'js/vendor/reportpopayin.js'></script>

<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<style>
    .modal-ku {
        width: 90%;
        margin: auto;
    }
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">รายงาน</a>
                    </li>

                </ul>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        รายงานภาษีขาย (เงินประกัน)
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body" style="overflow-x:auto;">
                              <form action="serachreporttaxsellinsurance" method="post">
                                  <input type="hidden" name="_token" value="{{ csrf_token() }}">

                              <div class="row">
                                <div class="col-md-4">

                                </div>
                                <div class="col-md-4">
                                    <?php
                                        $db = Connectdb::Databaseall();
                                        $sql = 'SELECT '.$db['hr_base'].'.branch.*
                                           FROM '.$db['hr_base'].'.branch
                                           WHERE '.$db['hr_base'].'.branch.status = "1"';

                                        $brcode = DB::connection('mysql')->select($sql);

                                    ?>
                                    <select name="branch" id="branch" class="form-control" >
                                        <option value="*">เลือกทั้งหมด</option>
                                        <?php
                                          foreach ($brcode as $key => $value) {
                                        ?>
                                          <option value="<?php echo $value->code_branch;?>" <?php if(!empty($data)&&($value->code_branch==$branch)){ echo "selected";}?> > <?php echo $value->name_branch;?></option>

                                        <?php }?>
                                    </select>
                                </div>
                                <div class="col-md-4">

                                </div>
                              </div>
                              <div class="row">
                                  <br>
                              </div>
                              <div class="row">
                                <div class="col-md-4">

                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="datepicker" id="datepicker" <?php if(!empty($data)){ echo $datepicker;}?>  class="form-control datepicker" readonly>
                                </div>
                                <div class="col-md-4">

                                </div>
                              </div>
                              <div class="row">
                                  <br>
                              </div>
                              <div class="row">
                                <div class="col-md-3">

                                </div>
                                <div class="col-md-3">
                                    <input type="submit" value="ค้นหา" class="btn btn-primary pull-right">
                                </div>
                                <div class="col-md-3">
                                    <input type="reset" class="btn btn-danger">
                                </div>
                                <div class="col-md-3">

                                </div>

                                <div class="col-md-12" align="right">
                                <?php   if(isset($data)){ ////echo $branch_id; ?>

                                        <?php  //if(isset($group_branch_acc_select) && $group_branch_acc_select != ''){?>
                                                <!-- <a href="printcovertaxabb?group_branch_acc_select=<?php //echo $group_branch_acc_select ;?>&&datepickerstart=<?php //echo $datepicker2['start_date'];?>&&datepickerend=<?php  //echo $datepicker2['end_date'];?>"><img src="images/global/printall.png"></a> -->
                                        <?php //}else { ?>
                                        <?php $path = '&datepicker='.$datepicker.'&branch='.$branch?>
                                                <a href="<?php echo url("/printreporttaxsellinsurance?$path");?>" target="_blank"><img src="images/global/printall.png"></a>
                                                <!-- <a href="printcovertaxabb?branch_id=<?php //echo $branch_id ;?>&&datepickerstart=<?php //echo $datepicker2['start_date'];?>&&datepickerend=<?php  //echo $datepicker2['end_date'];?> target="_blank" "><img src="images/global/printall.png"></a> -->
                                        <?php //} ?>

                                <?php } ?>
                              </div>

                              </div>
                              </form>

                              <div class="row">
                                  <br>
                              </div>

                              <div class="row">
                                <?php if(!empty($data)){
                                      // echo "<pre>";
                                      // print_r($datataxra);
                                      // print_r($datataxtf);
                                      // exit;

                                      $db = Connectdb::Databaseall();

                                  ?>
                                    <table id="example" class="display nowrap" style="width:100% ">
                                        <thead>
                                            <tr>
                                              <th>ลำดับ</th>
                                              <th>ปี/เดือน/วัน</th>
                                              <th>เลขที่</th>
                                              <th>ชื่อผู้ซื้อสินค้า/ผู้รับบริการ</th>
                                              <th>เลขประจำตัวผู้เสียภาษี</th>
                                              <th>สถานประกอบการ</th>
                                              <th>มูลค่าสินค้าหรือบริการ</th>
                                              <th>จำนวนเงินภาษีมูลค่าเพิ่ม</th>
                                              <th>จำนวนเงินรวมทั้งหมด</th>
                                              <th>หมายเหตุ</th>
                                              <!-- <th>เลขที่</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php

                                              $sumtotalloss = 0;
                                              $vat = 0;
                                              $i = 1;
                                              $sumsubtotal = 0;
                                              $sumvat = 0;
                                              $sumgrandtotal = 0;

                                              $j = 1;

                                              foreach ($datataxinsurance as $key => $value) { ?>
                                                <tr>
                                                   <td><?php echo $i;?></td><!--ลำดับ-->
                                                   <td><?php echo ($value->time);?></td><!--ปี/เดือน/วัน-->
                                                   <td><?php echo ($value->number_taxinvoice);?></td><!--เลขที่-->
                                                   <td>
                                                     <?php
                                                     $modelcustomerinsurance = Maincenter::getdatacustomer($value->customerid);
                                                           if($modelcustomerinsurance){
                                                             echo ($modelcustomerinsurance[0]->per);
                                                             echo " ";
                                                             echo ($modelcustomerinsurance[0]->name);
                                                             echo " ";
                                                             echo ($modelcustomerinsurance[0]->lastname);
                                                           }

                                                     ?>
                                                   </td>
                                                   <td><?php echo ($value->customerid);?></td>
                                                   <td><?php //echo ($modelsupplier[0]->type_branch);?></td>

                                                   <td>
                                                     <?php

                                                       // if($value->discountmoney){
                                                       //    echo number_format($value->subtotal - $value->discountmoney, 2);
                                                       //    $sumsubtotal = $sumsubtotal + ($value->subtotal - $value->discountmoney);
                                                       // }else {
                                                       //    echo number_format($value->subtotal, 2);
                                                       //    $sumsubtotal = $sumsubtotal + ($value->subtotal);
                                                       // }

                                                       echo number_format($value->subtotal, 2);
                                                       $sumsubtotal = $sumsubtotal + ($value->subtotal);
                                                     ?>
                                                   </td>

                                                   <td>
                                                     <?php
                                                       echo number_format ($value->vatmoney,2);
                                                       $sumvat = $sumvat + $value->vatmoney;
                                                     ?>
                                                   </td>

                                                   <td>
                                                     <?php
                                                       echo number_format ($value->grandtotal,2);
                                                       $sumgrandtotal = $sumgrandtotal + $value->grandtotal;
                                                     ?>
                                                   </td>
                                                   <td>
                                                     <?php
                                                       // echo ($value->note);
                                                         echo "-";
                                                     ?>
                                                   </td>
                                                   <!-- <td></td> -->
                                                </tr>

                                              <?php $i++; } ?>

                                              <?
                                              foreach ($datataxinsurancecn as $key2 => $value2) { ?>

                                              <tr>
                                                 <td><?php echo $i;?></td><!--ลำดับ-->
                                                 <td><?php echo ($value2->time);?></td><!--ปี/เดือน/วัน-->
                                                 <td><?php echo ($value2->number_taxinvoice);?></td><!--เลขที่-->
                                                 <td>
                                                   <?php
                                                   $modelcustomerinsurancecn = Maincenter::getdatacustomer($value2->customerid);
                                                         if($modelcustomerinsurancecn){
                                                           echo ($modelcustomerinsurancecn[0]->per);
                                                           echo " ";
                                                           echo ($modelcustomerinsurancecn[0]->name);
                                                           echo " ";
                                                           echo ($modelcustomerinsurancecn[0]->lastname);
                                                         }

                                                   ?>
                                                 </td>
                                                 <td><?php echo ($value2->customerid);?></td>
                                                 <td><?php //echo ($value2->branch_id);?></td>

                                                 <td>
                                                   <?php

                                                     // if($value2->discountmoney){
                                                     //    echo number_format(-($value2->subtotal - $value2->discountmoney), 2);
                                                     //    $sumsubtotal = $sumsubtotal + ($value2->subtotal - $value->discountmoney);
                                                     // }else {
                                                     //    echo number_format($value2->subtotal, 2);
                                                     //    $sumsubtotal = $sumsubtotal + ($value2->subtotal);
                                                     // }

                                                     echo number_format(-$value2->subtotal, 2);
                                                     $sumsubtotal = $sumsubtotal + $value2->subtotal;
                                                   ?>
                                                 </td>

                                                 <td>
                                                   <?php
                                                     echo number_format (-$value2->vatmoney,2);
                                                     $sumvat = $sumvat + $value2->vatmoney;
                                                   ?>
                                                 </td>

                                                 <td>
                                                   <?php
                                                     echo number_format (-$value2->grandtotal,2);
                                                     $sumgrandtotal = $sumgrandtotal + $value2->grandtotal;
                                                   ?>
                                                 </td>
                                                 <td>
                                                   <?php
                                                     // echo ($value2->note);
                                                       echo "-";
                                                   ?>
                                                 </td>
                                                 <!-- <td></td> -->
                                              </tr>

                                              <?php $i++; } ?>

                                                 <!-- <tr>
                                                   <td colspan="5" align="right"></td>
                                                   <td><center><b>รวม</b></center></td>
                                                   <td><b><?php //echo number_format($sumsubtotal,2);  ?></b></td>
                                                   <td><b><?php //echo number_format($sumvat,2);  ?></b></td>
                                                   <td><b><?php //echo number_format($sumgrandtotal,2); ?></b></td>
                                                   <td></td>
                                                 </tr> -->

                                        </tbody>
                                    </table>

                                <?php } ?>
                              </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')
