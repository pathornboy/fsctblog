<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;

$db = Connectdb::Databaseall();
?>
@include('headmenu')
<link>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>


<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">


<link rel="stylesheet" type="text/css" href="bower_components/select2/dist/css/select2.min.css">
<script type="text/javascript" src = 'bower_components/select2/dist/js/select2.full.min.js'></script>

<script type="text/javascript" src = 'js/vendor/boxcover.js'></script>


<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">งานจัดซื้อ</a>
                    </li>
                    <li class="active">ใบปะหน้า</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs text-center" id="breadcrumbs">
                                <h3>ใบปะหน้า</h3>
                            </div>
                            <div class="box-body table-responsive">
                                <div class="row">
                                    <br>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                      <center><h4>สาขา {{Session::get('brcode')}}</h4></center>
                                  </div>
                                </div>
                                <div class="row">
                                    <br>
                                </div>

                                <div class="row">
                                    <br>
                                </div>
                                <form action="getboxcover" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="row">
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-1">
                                    </div>
                                    <div class="col-md-2">
                                         <input type="text" class="form-control datepicker" id="datepicker" name="datepicker" readonly>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="submit" class="btn btn-success" value="ค้นหา">
                                    </div>
                                    <div class="col-md-1">

                                    </div>
                                    <div class="col-md-3">

                                    </div>
                                </div>
                                </form>
                                <div class="row">
                                    <br>
                                </div>
                                <?php if(!empty($query)){?>
                                <div class="row">
                                    <a href="printboxcover" target="_blank" ><button class="btn btn-primary pull-right">ดาวน์โหลด PDF</button></a>
                                </div>
                                <div class="row">

                                    <table class="table table-bordered"  >
                                        <thead>
                                          <tr>
                                            <td colspan="7"></td>
                                            <td colspan="4" align="center">ลักษณะบิล</td>
                                            <td></td>
                                          </tr>
                                          <tr>
                                            <td align="center">ลำดับ</td>
                                            <td align="center">เลขที่ PO</td>
                                            <td align="center">เจ้าหนี้</td>
                                            <td align="center">รายการ</td>
                                            <td align="center">จำนวนเงิน</td>
                                            <td align="center">ภาษีมูลค่าเพิ่ม</td>
                                            <td align="center">จำนวนเงินรวม</td>
                                            <td align="center">ใบกำกับภาษี</td>
                                            <td align="center">สำเนาใบกำกับภาษี</td>
                                            <td align="center">บิลเงินสด</td>
                                            <td align="center">ค่าใช่จ่ายที่ไม่มีบิล</td>
                                            <td align="center">หมายเหตุ</td>
                                            <td align="center">ใบPO/สำคัญจ่าย</td>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <?php
                                          $i = 1;
                                          foreach ($data as $key => $value) {
                                          ?>
                                            <tr>
                                              <td><?php echo $i;?></td>
                                              <td>
                                                <?php
                                                        if($value->status_head==2){
                                                          echo $value->date;
                                                        }else if($value->status_head==3){
                                                           $dateshow = getdata($value->id);
                                                           echo ($dateshow[0]->datebill);

                                                        }else if($value->status_head==4){
                                                            $dateshow = getdata($value->id);
                                                            echo ($dateshow[0]->datebill);

                                                        }else if($value->status_head==5){
                                                            $dateshow = getdata($value->id);
                                                            echo ($dateshow[0]->datebill);
                                                        }
                                                ?>
                                              </td>
                                              <td>
                                                <?php
                                                      echo ($value->po_number);
                                                ?>
                                              </td>
                                              <td>
                                                <?php
                                                      $supplier = Vendorcenter::getdatavendorcenter($value->supplier_id);
                                                      echo $supplier[0]->pre.'  '.$supplier[0]->name_supplier;
                                                ?>
                                              </td>
                                              <td>
                                                <?php
                                                      $datadetailpo = getdetailpo($value->id);
                                                      // echo "<pre>";
                                                      foreach ($datadetailpo as $k => $v) {
                                                            echo "<li>".$v->list."</li><br>";
                                                      }
                                                ?>
                                              </td>
                                              <td>
                                                <?php
                                                        if($value->status_head==2){
                                                            $datadetailpo = getdetailpo($value->id);
                                                            $total = 0;
                                                            foreach ($datadetailpo as $r => $l) {
                                                                  $total= $total + $l->total;
                                                            }
                                                              echo number_format($total,2);
                                                        }else if($value->status_head==3){
                                                            $dateshow = getdata($value->id);
                                                            if($value->vat!=0){
                                                                echo number_format(($dateshow[0]->payout+$dateshow[0]->wht-$dateshow[0]->vat_price),2);
                                                            }else{
                                                                echo number_format(($dateshow[0]->payout+$dateshow[0]->wht-$dateshow[0]->vat_price),2);
                                                            }



                                                        }else if($value->status_head==4){
                                                            $dateshow = getdata($value->id);
                                                            if($value->vat!=0){
                                                                echo number_format(($dateshow[0]->payout+$dateshow[0]->wht-$dateshow[0]->vat_price),2);
                                                            }else{
                                                                echo number_format(($dateshow[0]->payout+$dateshow[0]->wht-$dateshow[0]->vat_price),2);
                                                            }


                                                        }else if($value->status_head==5){
                                                            $dateshow = getdata($value->id);
                                                            if($value->vat!=0){
                                                                echo number_format(($dateshow[0]->payout+$dateshow[0]->wht-$dateshow[0]->vat_price),2);
                                                            }else{
                                                                echo number_format(($dateshow[0]->payout+$dateshow[0]->wht-$dateshow[0]->vat_price),2);
                                                            }
                                                        }
                                                ?>
                                              </td>
                                              <td>
                                                <?php
                                                        if($value->status_head==2){
                                                            $datadetailpo = getdetailpo($value->id);
                                                            $total = 0;
                                                            foreach ($datadetailpo as $r => $l) {
                                                                  $total= $total + $l->total;
                                                            }
                                                                if($value->vat!=0){
                                                                    echo number_format(($total*($value->vat/100)),2);
                                                                }else{
                                                                    echo "0.00";
                                                                }
                                                        }else if($value->status_head==3){
                                                            $dateshow = getdata($value->id);
                                                            if($value->vat!=0){
                                                                echo number_format(($dateshow[0]->vat_price),2);
                                                            }else{
                                                                echo number_format(($dateshow[0]->vat_price),2);
                                                            }

                                                        }else if($value->status_head==4){
                                                            $dateshow = getdata($value->id);
                                                            if($value->vat!=0){
                                                                echo number_format(($dateshow[0]->vat_price),2);
                                                            }else{
                                                                echo number_format(($dateshow[0]->vat_price),2);
                                                            }


                                                        }else if($value->status_head==5){
                                                            $dateshow = getdata($value->id);
                                                            if($value->vat!=0){
                                                                echo number_format(($dateshow[0]->vat_price),2);
                                                            }else{
                                                                echo number_format(($dateshow[0]->vat_price),2);
                                                            }

                                                        }
                                                ?>
                                              </td>
                                              <td>
                                                <?php
                                                        if($value->status_head==2){
                                                            $datadetailpo = getdetailpo($value->id);
                                                            $total = 0;
                                                            foreach ($datadetailpo as $r => $l) {
                                                                  $total= $total + $l->total;
                                                            }
                                                                if($value->vat!=0){
                                                                    echo number_format($total+ ($total*($value->vat/100)),2);
                                                                }else{
                                                                    echo number_format($total+ ($total*($value->vat/100)),2);
                                                                }
                                                        }else if($value->status_head==3){
                                                            $dateshow = getdata($value->id);
                                                            echo number_format($dateshow[0]->payout,2);

                                                        }else if($value->status_head==4){
                                                            $dateshow = getdata($value->id);
                                                            echo number_format($dateshow[0]->payout,2);
                                                        }else if($value->status_head==5){
                                                            $dateshow = getdata($value->id);
                                                            echo number_format($dateshow[0]->payout,2);
                                                        }
                                                ?>
                                              </td>
                                              <td></td>
                                              <td></td>
                                              <td></td>
                                              <td></td>
                                              <td></td>
                                              <td></td>
                                            </tr>
                                          <?php $i++;} ?>
                                        </tbody>
                                    </table>
                                </div>
                              <?php } ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<?php
    function getdata($id){
        $db = Connectdb::Databaseall();
        $dbac = $db['fsctaccount'];
        $sqlinform = "SELECT $dbac.inform_po.*
                      FROM $dbac.inform_po
                      WHERE  $dbac.inform_po.id_po = '$id'
                      AND status = '1' ";

        $dataquery = DB::connection('mysql')->select($sqlinform);
        return $dataquery;
    }

    function getdetailpo($id){
        $db = Connectdb::Databaseall();
        $dbac = $db['fsctaccount'];
        $sqlinform = "SELECT $dbac.po_detail.*
                      FROM $dbac.po_detail
                      WHERE  $dbac.po_detail.po_headid = '$id'
                      AND statususe = '1' ";

        $dataquery = DB::connection('mysql')->select($sqlinform);
        return $dataquery;
    }

?>
@include('footer')
