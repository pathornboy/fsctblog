<?php
use  App\Api\Connectdb;

use  App\Api\Vendorcenter;

?>
@include('headmenu')

<link>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}
<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>
<script type="text/javascript" src = 'js/config/configcash.js'></script>
<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">

<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-cog home-icon"></i>
                        <a href="#">ข้อมูลเงินสด</a>
                    </li>
                    <li class="active">จัดการข้อมูลเงินสด</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">
              <!-- <form action="searchgetcashedit" method="post"> -->
              <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <?php
                  $brcode = Session::get('brcode');
                  $empcode = Session::get('emp_code');
                ?>

                <form action="configgetcash" onSubmit="if(!confirm('ยืนยันการทำรายการ?')){return false;}" method="post" class="form-horizontal">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="row">
                    <div class="col-md-3">

                    </div>

                    <div class="col-md-2">
                      <center>เงินสดปัจจุบัน</center>
                    </div>

                    <div class="col-md-2">
                      <input type="text" name="grandtotal[]" value="" class="form-control">
                      <!-- <input type="submit" value="Submit"> -->
                    </div>

                    <div class="col-md-1">
                      บาท
                    </div>

                    <div class="col-md-1">
                      <input type="hidden" name="brcode[]" value="<?php echo $brcode; ?>" class="form-control">
                      <input type="hidden" name="empcode[]" value="<?php echo $empcode; ?>" class="form-control">
                      <input type="hidden" name="status[]" value="1" class="form-control">
                      <input type="hidden" name="date[]" value="<?php echo date("Y-m-d");?>" class="form-control">
                    </div>

                    <div class="col-md-3">

                    </div>

                </div>

                <div class="row">
                    <br>
                </div>

                <div class="row">
                    <div class="col-md-3">

                    </div>
                    <div class="col-md-2">

                    </div>
                    <div class="col-md-2">
                        <input type="submit" name="transfer" class="btn btn-info" value="นำฝาก">
                    </div>
                    <div class="col-md-4" >

                    </div>
                    <div class="col-md-1" >

                    </div>
                </div>

              </form>


        </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')


<!-- Modal -->

<!-- Modal -->
