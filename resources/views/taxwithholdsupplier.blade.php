<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;
$emp_code = Session::get('emp_code');

?>
@include('headmenu')
<link>
<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}



<link rel="stylesheet" type="text/css" href="css/ui/jquery-ui.css">
{{--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>


<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">


<link rel="stylesheet" type="text/css" href="bower_components/select2/dist/css/select2.min.css">
<script type="text/javascript" src = 'bower_components/select2/dist/js/select2.full.min.js'></script>

<script type="text/javascript" src = 'js/account/taxwithholdsupplier.js'></script>


<style>
    .ui-autocomplete-input {
        border: none;
        font-size: 14px;
        width: 150px;
        height: 24px;
        margin-bottom: 5px;
        padding-top: 2px;
        border: 1px solid #DDD !important;
        padding-top: 0px !important;
        z-index: 1511;
        position: relative;
    }
    .ui-menu .ui-menu-item a {
        font-size: 12px;
    }
    .ui-autocomplete {
        position: absolute;
        top: 0;
        left: 0;
        z-index: 1510 !important;
        float: left;
        display: none;
        min-width: 160px;
        width: 160px;
        padding: 4px 0;
        margin: 2px 0 0 0;
        list-style: none;
        background-color: #ffffff;
        border-color: #ccc;
        border-color: rgba(0, 0, 0, 0.2);
        border-style: solid;
        border-width: 1px;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding;
        background-clip: padding-box;
        *border-right-width: 2px;
        *border-bottom-width: 2px;
    }
    .ui-menu-item > a.ui-corner-all {
        display: block;
        padding: 3px 15px;
        clear: both;
        font-weight: normal;
        line-height: 18px;
        color: #555555;
        white-space: nowrap;
        text-decoration: none;
    }
    .ui-state-hover, .ui-state-active {
        color: #ffffff;
        text-decoration: none;
        background-color: #0088cc;
        border-radius: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        background-image: none;
    }

</style>



<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">งานบัญชี</a>
                    </li>
                    <li class="active">หนังสือรับรองการหักภาษี ณ ที่จ่าย</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">
                <input type="hidden" id="number_pprset" value="<?php $brcode = Session::get('brcode');
                $db = Connectdb::Databaseall();
                $sql = "SELECT COUNT(id) as idgen FROM $db[fsctaccount].ppr_head WHERE branch_id = '$brcode'";
                $bbr_no_branch = DB::connection('mysql')->select($sql);
                $numberrun = 0;
                //                                        print_r($bbr_no_branch);
                $onset =  $bbr_no_branch[0]->idgen;

                if($onset == 0){
                    $numberrun = 1;
                }else{
                    $numberrun = (int)($onset)+1;
                }

                echo 'PR'.substr(date('Y',strtotime("+543 year")),2,2).date('m').str_pad($numberrun, 3, "0", STR_PAD_LEFT);
                $yearth = date('Y')+543;
                ?>">
                <input type="hidden" id="level_emp" name="level_emp" value="<?php  echo $lavelemp = Session::get('level_emp');?>">
                <input type="hidden" id="dateset" value="<?php echo date('Y-m-d')?>">
                <input type="hidden"  class="form-control"  id="yearset" value="<?php echo date('Y')?>"  />
                <input type="hidden"  class="form-control" id="year_thset" value="<?php echo $yearth?>"  />
                <input type="hidden"  class="form-control"  id="bbr_no_branchset" value="<?php
                echo $numberrun;
                ?>"  />
                <input type="hidden"  class="form-control" id="dateshowset" value="<?php echo date('d-m').'-'.$yearth;?>"  disabled/>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        หนังสือรับรองการหักภาษี ณ ที่จ่าย
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body table-responsive">
                                <div class="row">
                                    <div class="col-md-10">
                                        {{--<label >Tags: </label>--}}
                                        {{--<input id="tags" class="form-control">--}}
                                    </div>

                                    <div class="col-md-2">
                                        <a href="#" title="เพิ่มข้อมูล" data-toggle="modal" data-target="#myModal" onclick="insertnew()" ><img src="images/global/add.png">เพิ่มข้อมูล</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <br>
                                </div>
                                <?php
                                $db = Connectdb::Databaseall();
                                $brcode = Session::get('brcode');
                                $levelemp = Session::get('level_emp');
                                      $sqlqthead = 'SELECT id, ppr_head.date, week, number_ppr, branch_id, ppr_head.status, id_company, urgent_status  FROM '.$db['fsctaccount'].'.ppr_head ';
                                      // print_r($sqlqthead);
                                      // exit();
                                if($levelemp == 1 || $levelemp == 2 || $levelemp == 3 || $emp_code == 1337 || $emp_code == 1471 || $emp_code == 2027 || $emp_code == 1475){
                                      $sqlqthead .= 'WHERE 1';
                                  }else{
                                      $sqlqthead .= 'WHERE branch_id = "'.$brcode.'"';
                                  }

                                $sqlqthead .= ' ORDER BY id DESC';
                                //echo "$sqlqthead";
                                $data = DB::connection('mysql')->select($sqlqthead);
                                $i = 1;
                                ?>
                                        <table id="example" class="table table-striped table-bordered">
                                            <thead class="thead-inverse">
                                            <tr>
                                                <td>#</td>
                                                <td>วันที่</td>
                                                <td>รอบที่ขอ</td>
                                                <td>บริษัท</td>
                                                <td>PR No.</td>
                                                <td>สาขา</td>
                                                <td>เร่งด่วน</td>
                                                <td>เงินที่ขออนุมัติ</td>
                                                <td>เงินที่โอนไป</td>
                                                <td>แก้ไข/เพิ่มเติม</td>
                                                <td>สถานะ</td>
                                                <td>ยกเลิก</td>
                                                <td>พิมพ์ใบ PR</td>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach ($data as $value)
                                                <tr>
                                                    <td scope="row">{{ $i }}</td>
                                                    <td align="left">{{ $value->date}}</td>
                                                    <td align="left">{{ $value->week}}</td>
                                                    <td align="left">
                                                        <?php $dataworking = Maincenter::datacompany($value->id_company);
                                                        print_r($dataworking[0]->name);

                                                        ?>
                                                    </td>
                                                    <td align="center">{{ $value->number_ppr }}</td>
                                                    <td align="center">{{ $value->branch_id }}</td>
                                                    <td align="center"><?php
                                                      if($value->urgent_status==1){
                                                          echo "<font color='#008ae6'>เร่งด่วน</font>";
                                                      }else if ($value->urgent_status==2) {
                                                          echo "<font color='#009933'>ปกติ</font>";
                                                      }else if ($value->urgent_status==3) {
                                                          echo "<font color='#ff3300'>เร่งด่วน(บอสอนุมัติแล้ว)</font>";
                                                      }
                                                     ?>
                                                    </td>
                                                    <td align="center"></td>
                                                    <td align="center"></td>
                                                    <td align="center">
                                                        @if($value->status=="0")
                                                        <a href="#" title="แก้ไขข้อมูล" data-toggle="modal" data-target="#myModal" onclick="getdata(1,{{ $value->id }})"><img src="images/global/edit-icon.png"></a>
                                                        @endif
                                                    </td>
                                                    <td align="center">
                                                        <?php $datastatus = Vendorcenter::getstatusvendor($value->status);
                                                                if($datastatus){
                                                                    $positionset = (explode(",",$datastatus[0]->position_id));

                                                                     $id_position = Session::get('id_position');
                                                                     $levelemp = Session::get('level_emp');
                                                                    if (in_array($id_position, $positionset) OR $levelemp <=2 ){

                                                                        echo  $statusresute = "<a href='#' data-toggle='modal' data-target='#myModal' onclick='getdata(3,$value->id)' ><font color='".$datastatus[0]->color."'>".$datastatus[0]->notstatus."</font></a>";
                                                                    }else{
                                                                        if($levelemp <=2){
                                                                            echo  $statusresute = "<a href='#' data-toggle='modal' data-target='#myModal' onclick='getdata(3,$value->id)' ><font color='".$datastatus[0]->color."'>".$datastatus[0]->notstatus."</font></a>";
                                                                        }else{
                                                                            echo  $statusresute = "<font color='".$datastatus[0]->color."'>".$datastatus[0]->notstatus."</font>";
                                                                        }

                                                                    }

                                                                }
                                                        ?>
                                                    </td>
                                                    <td align="center">
                                                        @if($value->status=="0")
                                                         <a href="#" title="ยกเลิกใบ PR" data-toggle="modal" data-target="#myModal" onclick="cancel(1,{{ $value->id }})"><img src="images/global/delete-icon.png"></a>
                                                        @endif
                                                    </td>
                                                    <td align="center">
                                                        <?php
                                                                $ckpo = Vendorcenter::ckstatus($value->status);
                                                                if($ckpo[0]->po==1){
                                                                    echo "<a href='printpr/$value->id' target='_blank'> <img src='images/global/pr.png' ></a>";
                                                                }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php $i++; ?>
                                            @endforeach
                                            </tbody>
                                        </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')


<!-- Modal -->

<div class="modal fade" id="myModal"  role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog" style="width:90%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">หนังสือรับรองการหักภาษี ณ ที่จ่าย</h5>
            </div>

            <div class="modal-body">

                <form id="configFormvendors" onsubmit="return getdatesubmit();" data-toggle="validator" method="post" class="form-horizontal">
                    <!-- <input value="{{ null }}" type="hidden" id="id" name="id" />

                    <input type="hidden" id="statusprocess" value="0"> -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-right">รหัสสาขา <?php echo $brcode = Session::get('brcode');?>
                                <input type="hidden"  class="form-control" name="branch_id" id="branch_id" value="<?php echo $brcode ;?>"  />
                                (
                                <?php
                               $databr = Maincenter::databranchbycode($brcode);
                               print_r($databr[0]->name_branch);
                                ?>)
                            </div>
                        </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        ผู้มีหน้าที่หักภาษี ณ ที่จ่าย :
                      </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-3 control-label">ชี่อ<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-9">
                                    <input type="text"  class="form-control" id="name" value="" name="name" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">ที่อยู่<i><span style="color: red">*</span></i></label>
                                    <div class="col-xs-8">
                                        <input class="form-control" id="address" type="text" name="address" value="">
                                    </div>
                                    <!-- <div class="col-xs-4">

                                    </div> -->
                                </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-3 control-label">เลขประจำตัวผู้เสียภาษีอากร<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-9">
                                    <input type="text"  class="form-control" id="my_tax_id" value="" name="my_tax_id" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-3 control-label">PO NUMBER<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-9">
                                    <input type="text"  class="form-control" id="po_number" value="" name="po_number" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-default" id="F_PO_NO" type="button" name="button">REF.</button>
                        </div>
                    </div>

                    <div class="row" id="SHOW_PO_REF" style="display: none;">
                      <div class="col-12">
                        <div class="row">
                          <div class="col-md-12">
                            ผู้ถูกหักภาษี ณ ที่จ่าย :
                          </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">ชี่อ<i><span style="color: red">*</span></i></label>
                                    <div class="col-xs-9">
                                        <input type="text" disabled  class="form-control" id="name_passive" value="" name="name_passive" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">ที่อยู่<i><span style="color: red">*</span></i></label>
                                    <div class="col-xs-8">
                                        <input disabled class="form-control" type="text" id="address_passive" name="address_passive" value="">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">เลขประจำตัวผู้เสียภาษีอากร<i><span style="color: red">*</span></i></label>
                                    <div class="col-xs-9">
                                        <input type="text" disabled  class="form-control" id="tax_id_passive" value="" name="tax_id_passive" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                          <div class="col-12 text-center">
                            <button type="submit" class="btn btn-primary" name="button">Save</button>
                            <button type="reset" class="btn btn-basic" name="button" data-dismiss="modal" >Cancel</button>
                          </div>
                        </div>
                      </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>



<!---   myModal approved--->
