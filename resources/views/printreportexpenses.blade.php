<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;

use App\Api\Datetime;
use App\working_company;
use Illuminate\Support\Facades\Input;

?>

<link>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}
<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>

<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">

<script type="text/javascript" src = 'js/vendor/vendorcenter.js'></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

    <style>
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }

        body {
            font-family: "THSarabunNew";
            font-size: 12px;
        }
        h4 {
            font-family: "THSarabunNew";
        }
        h4 {
            font-family: "THSarabunNew";
        }

    </style>

<style>
    .modal-ku {
        width: 90%;
        margin: auto;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="box-body">

            <?php

              $data = Input::all();
              $db = Connectdb::Databaseall();
              // echo "<pre>";
              // print_r($data);
              // exit;

              $datepicker = explode("-",trim(($data['datepicker'])));

              $datepickerstart = explode("/",trim(($datepicker[0])));
              if(count($datepickerstart) > 0) {
                  $datetime = $datepickerstart[1] . '-' . $datepickerstart[0]; //วัน - เดือน
              }

              $datepickerend = explode("/",trim(($datepicker[1])));
              if(count($datepickerend) > 0) {
                  $datetime2 = $datepickerend[1] . '-' . $datepickerend[0]; //วัน - เดือน
              }

              if($datepickerstart[0] == "01"){$monthTH = "มกราคม";
                }else if($datepickerstart[0] == "02"){$monthTH = "กุมภาพันธ์";
                }else if($datepickerstart[0] == "03"){$monthTH = "มีนาคม";
                }else if($datepickerstart[0] == "04"){$monthTH = "เมษายน";
                }else if($datepickerstart[0] == "05"){$monthTH = "พฤษภาคม";
                }else if($datepickerstart[0] == "06"){$monthTH = "มิถุนายน";
                }else if($datepickerstart[0] == "07"){$monthTH = "กรกฎาคม";
                }else if($datepickerstart[0] == "08"){$monthTH = "สิงหาคม";
                }else if($datepickerstart[0] == "09"){$monthTH = "กันยายน";
                }else if($datepickerstart[0] == "10"){$monthTH = "ตุลาคม";
                }else if($datepickerstart[0] == "11"){$monthTH = "พฤศจิกายน";
                }else if($datepickerstart[0] == "12"){$monthTH = "ธันวาคม";
                }

                $modelname = Maincenter::databranchbycode($data['branch']);

                $compid = $modelname[0]->company_id;
                $sqlcompany = "SELECT * FROM $db[hr_base].working_company  WHERE id ='$compid' ";
                $datacomp = DB::connection('mysql')->select($sqlcompany);

                $branch_id = $data['branch'];
                $sql = "SELECT * FROM $db[hr_base].branch  WHERE code_branch ='$branch_id' ";
                $databranch = DB::connection('mysql')->select($sql);

            ?>

          <div class="row">
              <div class="col-md-12">
                  <div class="box box-primary">
                      <div class="breadcrumbs" id="breadcrumbs">
                          <ul class="breadcrumb">
                              <div align="center">



                              <table width="100%">
                                <tr>
                                  <td align="center" ><b>บริษัท ฟ้าใสคอนสตรัคชั่นทูลส์ จำกัด ({{$modelname[0]->name_branch}})</b></td>
                                </tr>

                                <tr>
                                  <td align="center" ><b>รายงานค่าใช้จ่าย (ชำระแล้ว)</b></td>
                                </tr>

                                <tr>
                                  <td align="center" ><b>ตั้งแต่วันที่ {{$datepickerstart[1]}} {{$monthTH}} {{$datepickerstart[2]}} จนถึงวันที่ {{$datepickerend[1]}} {{$monthTH}} {{$datepickerstart[2]}}</b></td>
                                </tr>
                              </table>
              </div>
              </div>
              </ul><!-- /.breadcrumb -->
              <!-- /section:basics/content.searchbox -->
              </div>

							<div align="center">
							<?php

              $data = Input::all();
              $db = Connectdb::Databaseall();
              // echo "<pre>";
              // print_r($data);
              // exit;

              // $branch_id = $data['branch_id'];
              // $start_date = $data['datepickerstart']." 00:00:00";
              // $end_date = $data['datepickerend']." 23:59:59";

              $datepicker = explode("-",trim(($data['datepicker'])));

              // $start_date = $datepicker[0];
              $e1 = explode("/",trim(($datepicker[0])));
                      if(count($e1) > 0) {
                          $start_date = $e1[2] . '-' . $e1[0] . '-' . $e1[1]; //ปี - เดือน - วัน
                          $start_date2 = $start_date." 00:00:00";
                      }

              // $end_date = $datepicker[1];
              $e2 = explode("/",trim(($datepicker[1])));
                      if(count($e2) > 0) {
                          $end_date = $e2[2] . '-' . $e2[0] . '-' . $e2[1]; //ปี - เดือน - วัน
                          $end_date2 = $end_date." 23:59:59";
                      }

              $branch_id = $data['branch'];

              // echo "<pre>";
              // print_r($start_date);
              // print_r($end_date);
              // exit;

              $sql = 'SELECT '.$db['fsctaccount'].'.inform_po.*,
                             '.$db['fsctaccount'].'.po_head.branch_id

                     FROM '.$db['fsctaccount'].'.inform_po
                     INNER JOIN  '.$db['fsctaccount'].'.po_head
                        ON '.$db['fsctaccount'].'.po_head.id = '.$db['fsctaccount'].'.inform_po.id_po

                      WHERE '.$db['fsctaccount'].'.po_head.branch_id = "'.$branch_id.'"
                        AND '.$db['fsctaccount'].'.inform_po.datetime  BETWEEN "'.$start_date.'" AND  "'.$end_date.'"
                        AND '.$db['fsctaccount'].'.inform_po.status NOT IN (99)
                        ORDER BY '.$db['fsctaccount'].'.inform_po.datebillreceipt
                     ';

              $datatresult = DB::connection('mysql')->select($sql);

              $sqlreserve = 'SELECT '.$db['fsctaccount'].'.reservemoney.*
                             FROM '.$db['fsctaccount'].'.reservemoney

                             WHERE '.$db['fsctaccount'].'.reservemoney.branch = "'.$branch_id.'"
                              AND '.$db['fsctaccount'].'.reservemoney.dateporef  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                              AND '.$db['fsctaccount'].'.reservemoney.status IN (0 , 1 , 2)
                              AND '.$db['fsctaccount'].'.reservemoney.po_ref != 0
                              ORDER BY '.$db['fsctaccount'].'.reservemoney.date_bill_no
                             ';

              $datatresultreserve = DB::connection('mysql')->select($sqlreserve);
              // echo "<pre>";
              // print_r($datatresult);
              // print_r($datatresultreserve);
              // exit;

              ?>


              <div align="center" >

							<?php //echo $week;?>
              <!-- <div class="row">
                  <div class="col-md-5">
                      <input type="hidden" name="settime" id="settime" value="<?php //echo date('Y-m-d')?>">
                      <input type="hidden" name="setlogin" id="setlogin" value="<?php //echo $emp_code = Session::get('emp_code')?>">
                  </div>
              </div> -->

              <!-- <table class="table table-striped table-bordered" width="100%" border="1" cellpadding="0"> -->
              <!-- <font size="1" > -->
              <table class="table table-bordered" width="100%" border="1" cellspacing="0">
                  <thead class="thead-inverse" >
                  <tr>
                    <th align="center">ลำดับ</th>
                    <th align="center">วันที่จ่าย</th>
                    <th align="center">ประเภทการจ่ายเงิน</th>
                    <!-- <th align="center">ใบสำคัญจ่าย</th> -->
                    <th align="center">วันที่บิล</th>
                    <th align="center">เลขที่บิล</th>
                    <th align="center">จ่ายให้</th>
                    <th align="center">เลขประจำตัวผู้เสียภาษี</th>
                    <th align="center">จำนวนเงิน</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php

                  $sumtotalloss = 0;
                  $vat = 0;
                  $i = 1;
                  $sumsubtotal = 0;
                  $sumvat = 0;
                  $sumgrandtotal = 0;
                  $sumwht = 0;

                  $sumtransfer = 0;
                  $sumcash = 0;

                  $j = 1;

                  ?>
                    @foreach ($datatresult as $value)
                        <tr>
                          <td scope="row">{{ $i }}</td><!--ลำดับ-->
                          <td align="left">{{ $value->datetime }}</td><!--วันที่จ่าย-->
                          <td align="left"><!--ประเภทการจ่ายเงิน-->
                            <?php
                                    if($value->type_pay == "1"){
                                      echo "เงินสด";
                                    }elseif ($value->type_pay == "2") {
                                      echo "เงินโอน";
                                    }

                                    if($value->wht_percent != "0"){
                                      echo "<br>";
                                      echo "ภาษีหัก ณ ที่จ่าย";
                                    }
                            ?>
                          </td>
                          <!-- <td align="left"><?php //echo $i;?></td> -->
                          <td align="left">{{ $value->datebillreceipt }}</td><!--วันที่บิล-->
                          <td align="left">{{ $value->receipt_no }}</td><!--เลขที่-->

                          <?php $modelsupplier = Maincenter::getdatasupplierpo($value->id_po);
                                if($modelsupplier){ ?>
                          <td align="center">{{ ($modelsupplier[0]->pre)." ".($modelsupplier[0]->name_supplier) }}</td>
                          <td align="center">{{ $modelsupplier[0]->tax_id }}</td>
                          <?php } ?>

                          <?php  $vat = (($value->payout * 7 )/ 107); ?>

                          <td align="center">
                            <?php
                                  echo number_format ($value->payout,2);
                                  $sumgrandtotal = $sumgrandtotal + $value->payout;

                                  if($value->type_pay == "1"){
                                    $sumcash = $sumcash + $value->payout;
                                  }elseif ($value->type_pay == "2") {
                                    $sumtransfer = $sumtransfer + $value->payout;
                                  }

                                  if($value->wht_percent != "0"){
                                    echo "<br>";
                                    echo number_format($value->wht,2);
                                    $sumwht = $sumwht + $value->wht;
                                  }
                            ?>
                          </td>

                        </tr>

                        <?php $i++; ?>

                    @endforeach

                    @foreach ($datatresultreserve as $value2)
                        <tr>
                          <td scope="row">{{ $i }}</td><!--ลำดับ-->
                          <td align="left">{{ $value2->datetime }}</td><!--วันที่จ่าย-->
                          <td align="left"><!--ประเภทการจ่ายเงิน-->
                            <?php
                            $modelpo = Maincenter::getdatapo($value2->po_ref);
                            if($modelpo){
                                    if($modelpo[0]->type_buy == "1"){
                                      echo "เงินสด";
                                    }elseif ($modelpo[0]->type_buy == "2") {
                                      echo "เงินโอน";
                                    }

                                    if($modelpo[0]->whd != "0.00"){
                                      echo "<br>";
                                      echo "ภาษีหัก ณ ที่จ่าย";
                                    }
                            }
                            ?>
                          </td>
                          <!-- <td align="left"><?php //echo $i;?></td> -->
                          <td align="left">{{ $value2->date_bill_no }}</td><!--วันที่บิล-->
                          <td align="left">{{ $value2->bill_no }}</td><!--เลขที่-->

                          <?php $modelsuppliername = Maincenter::getdatasupplierpo($value2->po_ref);
                                if($modelsuppliername){ ?>
                          <td align="center">{{ ($modelsuppliername[0]->pre)." ".($modelsuppliername[0]->name_supplier) }}</td>
                          <td align="center">{{ $modelsuppliername[0]->tax_id }}</td>
                          <?php } ?>

                          <?php  //$vat = (($value2->payout * 7 )/ 107); ?>

                          <td align="center">
                            <?php
                                  echo number_format ($value2->total,2);
                                  $sumgrandtotal = $sumgrandtotal + $value2->total;

                                  if($modelpo){

                                      if($modelpo[0]->type_buy == "1"){
                                        $sumcash = $sumcash + $value2->total;
                                      }elseif ($modelpo[0]->type_buy == "2") {
                                        $sumtransfer = $sumtransfer + $value2->total;
                                      }

                                      // if($modelpo[0]->whd != "0.00"){
                                      //   echo "<br>";
                                      //   echo $modelpo[0]->whd;
                                      //   $sumwht = $sumwht + $modelpo[0]->whd;
                                      // }

                                      $modelwth = Maincenter::getdatawth($value2->po_ref);

                                      if ($modelwth) {
                                          echo "<br>";
                                          // $withthis = ($sumtotalthis)*(0.05);// ((100*7)/100)
                                          echo number_format (($modelwth[0]->total),2);
                                          $sumwht = $sumwht + ($modelwth[0]->total);
                                      }

                                      // if ($modelpo[0]->whd == 10) {
                                      //     echo "<br>";
                                      //     // $withthis = ($sumtotalthis)*(0.05);// ((100*7)/100)
                                      //     echo number_format (($modelpo[0]->totolsumall)*(0.10),2);
                                      //     $sumwht = $sumwht + (($modelpo[0]->totolsumall)*(0.10));
                                      // }
                                      //
                                      // if ($modelpo[0]->whd == 5) {
                                      //     echo "<br>";
                                      //     // $withthis = ($sumtotalthis)*(0.05);// ((100*7)/100)
                                      //     echo number_format (($modelpo[0]->totolsumall)*(0.05),2);
                                      //     $sumwht = $sumwht + (($modelpo[0]->totolsumall)*(0.05));
                                      // }
                                      //
                                      // if ($modelpo[0]->whd == 3) {
                                      //     echo "<br>";
                                      //     // $withthis = ($sumtotalthis)*(0.05);// ((100*7)/100)
                                      //     echo number_format (($modelpo[0]->totolsumall)*(0.03),2);
                                      //     $sumwht = $sumwht + (($modelpo[0]->totolsumall)*(0.03));
                                      // }
                                      //
                                      // if ($modelpo[0]->whd == 1) {
                                      //     echo "<br>";
                                      //     // $withthis = ($sumtotalthis)*(0.05);// ((100*7)/100)
                                      //     echo number_format (($sumtotalthis)*(0.01),2);
                                      //     $sumwht = $sumwht + (($sumtotalthis)*(0.01));
                                      // }
                                  }
                            ?>
                          </td>

                        </tr>

                        <?php $i++; ?>

                    @endforeach

                    <tr>
                      <td colspan="6" align="right"></td>
                      <td><center><b>รวมเงินสด</b></center></td>
                      <td align="center"><b><?php echo number_format($sumcash,2); ?></b></td>
                    </tr>

                    <tr>
                      <td colspan="6" align="right"></td>
                      <td><center><b>รวมเงินโอน</b></center></td>
                      <td align="center"><b><?php echo number_format($sumtransfer,2); ?></b></td>
                    </tr>

                    <tr>
                      <td colspan="6" align="right"></td>
                      <td><center><b>รวมภาษีหัก ณ ที่จ่าย</b></center></td>
                      <td align="center"><b><?php echo number_format($sumwht,2); ?></b></td>
                    </tr>

                    <tr>
                      <td colspan="6" align="right"></td>
                      <td><center><b>รวมทั้งสิ้น</b></center></td>
                      <td align="center"><b><?php echo number_format($sumwht + $sumgrandtotal,2); ?></b></td>
                    </tr>

                        </tbody>
                     </table>
                     <!-- </font> -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
