<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;


?>
@include('headmenu')
<link>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>


<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">


<link rel="stylesheet" type="text/css" href="bower_components/select2/dist/css/select2.min.css">
<script type="text/javascript" src = 'bower_components/select2/dist/js/select2.full.min.js'></script>

<script type="text/javascript" src = 'js/vendor/transferpo.js'></script>


<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<!-- {{--<style>--}}
{{--.modal-ku {--}}
{{--width: 1024px;--}}
{{--margin: auto;--}}
{{--}--}}
{{--</style>--}} -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">งานจัดซื้อ</a>
                    </li>
                    <li class="active">โอนสินค้าจากใบ PO</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">
                <input type="hidden" id="dateset" value="<?php echo date('Y-m-d')?>">
                <input type="hidden"  class="form-control"  id="yearset" value="<?php echo date('Y')?>"  />
                <input type="hidden"  class="form-control" id="year_thset" value="<?php echo date('Y',strtotime("+543 year"))?>"  />

                <input type="hidden"  class="form-control" id="dateshowset" value="<?php echo date('d-m-Y',strtotime("+543 year"))?>"  disabled/>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        ข้อมูลการจัดซื้อ
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-10">

                                    </div>

                                    <div class="col-md-2">

                                    </div>
                                </div>
                                <div class="row">
                                    <br>
                                </div>
                                <table id="example" class="table table-striped table-bordered">
                                    <thead class="thead-inverse">
                                    <tr>
                                        <td>#</td>
                                        <td>วันที่</td>
                                        <td>บริษัท</td>
                                        <td>PO No.</td>
                                        <td>PR No.</td>
                                        <td>รายชื่อ Vendor</td>
                                        <td>สาขา</td>
                                        <td>โอนไปสาขา</td>
                                        <td>สถานะ</td>

                                        <td>โอนสินค้าจากใบ PO</td>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $db = Connectdb::Databaseall();


                                    $data = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.po_head WHERE status_head IN (1,2) ORDER BY id DESC');
                                    // echo "<pre>";
                                    // print_r($data);
                                    // exit();

                                    $i = 1;
                                    ?>
                                    @foreach ($data as $value)
                                        <tr>
                                            <td scope="row">{{ $i }}</td>
                                            <td align="left">{{ $value->date}}</td>
                                            <td align="left">
                                                <?php $dataworking = Maincenter::datacompany($value->id_company);
                                                print_r($dataworking[0]->name);

                                                ?>
                                            </td>
                                            <td align="center">{{ $value->po_number }}</td>
                                            <td align="center">{{ $value->number_pr_ref }}</td>
                                            <td align="center"><?php $datasupp = Vendorcenter::getdatavendorcenter($value->supplier_id);
                                                print_r($datasupp[0]->pre."     ".$datasupp[0]->name_supplier)
                                                ?></td>
                                            <td align="center">{{ $value->branch_id }}</td>
                                            <td align="center">{{ $value->terms }}</td>

                                            <td align="center">
                                                <?php $datastatus = Vendorcenter::getstatusresultpo($value->status_head);
                                                print_r($datastatus);
                                                ?>
                                            </td>
                                            <td align="center">
                                                <a href="#" title="ดูรายละเอียด" data-toggle="modal" data-target="#myModal" onclick="getdata(2,{{ $value->id }})"><img src="images/global/transfer.png"></a>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')


<!-- Modal -->

<div class="modal fade" id="myModal"  role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog"  style="width:1250px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">ข้อมูลใบ PO</h5>
            </div>

            <div class="modal-body">


                <input value="{{ null }}" type="hidden" id="id" name="id" />
                {{--<input type="hidden" name="status" id="status" value="0">--}}



                <div class="row">
                    <div class="col-md-4">

                        <div class="form-group">
                            <label class="col-xs-6 control-label">เลือกบริษัท<i><span style="color: red">*</span></i></label>
                            <div class="col-xs-5">
                                <select name="id_company" id="id_company" class="form-control" onchange="selecttranfer(this)">
                                    <?php
                                    $db = Connectdb::Databaseall();
                                    $data = DB::connection('mysql')->select('SELECT * FROM '.$db['hr_base'].'.working_company WHERE status ="1"');
                                    ?>
                                    @foreach ($data as $value)
                                        <option value="{{$value->id}}">{{$value->name}}</option>

                                    @endforeach

                                </select>
                            </div>
                            <div class="col-xs-4">

                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="pull-right">
                            <b>PR.</b>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <input value="0" type="hidden" id="statusupdate" name="statusupdate" />
                        <input value="{{ null }}" type="hidden" id="pr_id" name="pr_id" />
                        <input type="text" name="number_pr_ref" id="number_pr_ref" placeholder="PR6001001" class="form-control">
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-primary" id="serachpridref" onclick="serachpridref()">ค้นหา</button>
                    </div>
                    <div class="col-md-4">
                        <div class="pull-right">รหัสสาขา <?php echo $brcode = Session::get('brcode');?>
                            <input type="hidden"  class="form-control" name="branch_id" id="branch_id" value="<?php echo $brcode ;?>"  />
                            (
                            <?php
                            $databr = Maincenter::databranchbycode($brcode);
                            print_r($databr[0]->name_branch);
                            ?>)
                        </div>
                    </div>
                </div>
                <div>
                    <br>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-xs-3 control-label">วันที่ขอ<i><span style="color: red">*</span></i></label>
                            <div class="col-xs-5">
                                <input type="text"  class="form-control" id="dateshow" value="<?php echo date('d-m-Y',strtotime("+543 year"))?>"  disabled/>
                                <input type="hidden"  class="form-control" name="date" id="date" value="<?php echo date('Y-m-d')?>"  />
                                <input type="hidden"  class="form-control" name="year" id="year" value="<?php echo date('Y')?>"  />
                                <input type="hidden"  class="form-control" name="year_th" id="year_th" value="<?php echo date('Y',strtotime("+543 year"))?>"  />
                            </div>
                            <div class="col-xs-4">

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">

                    </div>

                    <div class="col-md-4">
                        <div class="pull-right">
                            <div class="form-group">
                                <label class="col-xs-6 control-label">PO No.<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-5">
                                    <input type="text"  class="form-control po_number" name="po_number" id="po_number"   readonly/>

                                    <input type="hidden"  class="form-control" name="po_no_branch" id="po_no_branch" value=""  readonly/>

                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-xs-4 control-label"> Seller/ผู้ขาย<i><span style="color: red">*</span></i></label>
                            <div class="col-xs-6">
                                <input type="hidden" class="form-control" id="supplier_id" name="supplier_id">
                                <input type="text" class="form-control" id="supplier_show" disabled>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-xs-4 control-label">ประเภทการจ่าย<i><span style="color: red">*</span></i></label>
                            <div class="col-xs-6">
                                <input type="hidden" class="form-control" id="type_pay" name="type_pay">
                                <input type="text" class="form-control" id="type_pay_show" disabled>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-xs-4 control-label">ประเภทการซื้อ<i><span style="color: red">*</span></i></label>
                            <div class="col-xs-6">
                                <input type="hidden" class="form-control" id="type_buy" name="type_buy">
                                <input type="text" class="form-control" id="type_buy_show" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-xs-4 control-label">การจัดซื้อ(ใน/นอก ประเทศ)<i><span style="color: red">*</span></i></label>
                            <div class="col-xs-6">
                                <input type="hidden" class="form-control" id="in_house" name="in_house">
                                <input type="text" class="form-control" id="in_house_show" disabled>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-xs-4 control-label">งบประมาณ<i><span style="color: red">*</span></i></label>
                            <div class="col-xs-6">
                                <input type="hidden" class="form-control" id="in_budget" name="in_budget">
                                <input type="text" class="form-control" id="in_budget_show" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-xs-4 control-label">การจัดซื้อ(เร่งด่วน/ปกติ)<i><span style="color: red">*</span></i></label>
                            <div class="col-xs-6">
                                <input type="hidden" class="form-control" id="urgent_status" name="urgent_status">
                                <input type="text" class="form-control" id="urgent_status_show" disabled>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-xs-4 control-label">เงื่อนไขในการส่ง<i><span style="color: red">*</span></i></label>
                            <div class="col-xs-6">

                                <select name="type_transfer" id="type_transfer" class="form-control" >
                                    <option value="">เลือกเงื่อนไขในการส่ง</option>
                                    <?php
                                    $db = Connectdb::Databaseall();
                                    $datatransfer = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.transfer_config WHERE status ="1"');
                                    ?>
                                    @foreach ($datatransfer as $valuetransfer)
                                        <option value="{{$valuetransfer->id}}">{{$valuetransfer->name}}</option>

                                    @endforeach

                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-xs-4 control-label">เงื่อนไขการชำระ<i><span style="color: red">*</span></i></label>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" id="terms" name="terms" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="pull-right">
                            <b>ส่งที่สำนักงานใหญ่</b>
                            <input type="checkbox" id="ckaddbranch" onclick="ckaddress()" checked>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <input type="text" name="address_send" id="address_send" class="form-control" readonly>

                    </div>
                    <div class="col-md-6">

                    </div>
                </div>
                <div class="row">
                    <br>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <table class="table" id="thdetail">
                            <thead>
                            <tr>
                                <td align="center">ประเภทงานจัดซื้อ</td>
                                <td align="center">เลขทางบัญชี</td>
                                <td align="center">รายการ</td>
                                <td align="center" colspan="2">จำนวนนับ</td>
                                <td align="center">ราคาต่อหน่วย</td>
                                <td align="center">vat</td>
                                <td align="center">หัก ณ ที่จ่าย</td>
                                <td align="center">รวม</td>
                                <td align="center">ยอดที่รับมา</td>
                                <td align="center">ขาด</td>
                                <td align="center">สถานะ</td>
                            </tr>
                            </thead>
                            <tbody id="tdbody">
                            <tr>
                                <td align="center">
                                    <select name="config_group_supp_id[]" id="config_group_supp_id0" class="form-control" disabled>
                                        <option value="">เลือกประเภทการจัดซื้อ</option>
                                        <?php
                                        $db = Connectdb::Databaseall();
                                        $data = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.config_group_supp WHERE status = "1" ');
                                        ?>
                                        @foreach ($data as $value)
                                            <option value="{{$value->id}}">{{$value->name}}</option>
                                        @endforeach
                                    </select>

                                </td>
                                <td align="center" >
                                    <select name="accounttype_id[]" id="accounttype_id0" class="form-control"  style="width: 120px;" >
                                        <option value="">เลือกเลขบัญชี</option>
                                    </select>
                                </td>
                                <td align="center">
                                    <input type="hidden" name="material_id[]" id="materialid0" value="0">
                                    <input type="text" name="list[]" id="list0"  class="form-control" readonly>
                                </td>
                                <td align="center">
                                    <input type="text" name="amount[]" id="amount0" onblur="getamount(this,0)" class="form-control" style="width: 90px;" placeholder="จำนวน" readonly>
                                </td>
                                <td align="center">
                                    <input type="text" name="type_amount[]" id="type_amount0"  class="form-control" style="width: 80px;" placeholder="หน่วยนับ" readonly>
                                </td>
                                <td align="center">
                                    <input type="text" name="price[]" id="price0" onblur="getprice(this,0)"  disabled  class="form-control" readonly >
                                </td>
                                <td align="center">
                                    <select name="vat[]" id="vat0" class="form-control" onchange="calculatevat(this,0)" style="width: 90px;" disabled>
                                        <option value="0">ไม่มี vat</option>
                                        <?php
                                        $db = Connectdb::Databaseall();
                                        $data = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.tax_config WHERE status = "1"');
                                        ?>
                                        @foreach ($data as $value)
                                            <option value="{{$value->tax}}">{{$value->tax}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td align="center">
                                    <select name="withhold[]" id="withhold0" class="form-control" onchange="calculatewithhold(this,0)" style="width: 90px;" disabled>
                                        <option value="0" selected>ไม่มีหัก</option>
                                        <?php
                                        $db = Connectdb::Databaseall();
                                        $datawithhold = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.withhold WHERE status = "1"');
                                        ?>
                                        @foreach ($datawithhold as $value)
                                            <option value="{{$value->withhold}}">{{$value->Note}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td align="center">
                                    <input type="text" name="total[]"  id="total0" class="form-control" readonly >
                                </td>
                                <td align="center">
                                    <input type="text" class="form-control quantity"  name="quantity_get[]" id="quantity0" onblur="caldifamount(0)" value="0" readonly>
                                </td>
                                <td align="center">
                                    <input type="text" class="form-control"  name="quantity_loss[]" id="loss0" readonly>
                                </td>
                                <td align="center">
                                         <span id="statuspodetai0">
                                             <font color="red">ยังไม่ครบ</font>
                                         </span>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <br>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        ราคารวม
                    </div>
                    <div class="col-md-2">
                        <input type="text" class="form-control" id="totolsumall" name="totolsumall" readonly>
                    </div>
                    <div class="col-md-1">

                    </div>
                    <div class="col-md-1">
                        MD/BOSS โอน
                    </div>
                    <div class="col-md-2">
                        <input type="text" class="form-control" id="payreal" name="payreal" onblur="caldiffpay()">
                    </div>
                    <div class="col-md-1">

                    </div>
                    <div class="col-md-1">
                        ถอน/ขาด
                    </div>
                    <div class="col-md-2">
                        <input type="text" class="form-control" id="diffpay" name="diffpay" readonly>
                    </div>
                    <div class="col-md-1">

                    </div>
                </div>
                <div class="row">
                    <br>
                </div>
                <div class="row" >
                    <div class="col-md-6">
                        <div class="pull-right">
                            รหัสพนักงานที่ขอใบ po:
                            <input type="hidden" name="code_emp_pr" id="code_emp_pr">
                            <input type="text" class="form-control" name="code_emp_po" id="code_emp_po" value="<?php echo $brcode = Session::get('emp_code')?>" readonly>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="pull-left">
                            รหัสซุปประจำสาขา:
                            <input type="text" class="form-control" name="code_sup" id="code_sup" placeholder="รหัสซุปประจำสาขา">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <br>
                </div>

                <div class="row">
                    <br>
                </div>
                <div class="row" style="display: none;" id="showbill">
                    <div class="col-md-4">

                    </div>
                    <div class="col-md-4">
                        <table width="100%">
                            <thead>
                            <tr>
                                <td width="33.33%"></td>
                                <td width="33.33%">เลขที่บิล</td>
                                <td width="33.33%">จ่ายจริงตามบิล</td>
                            </tr>
                            </thead>
                            <tbody id="addrowbilltb">
                            <tr>
                                <td width="33.33%">บิลที่ REF.</td>
                                <td width="33.33%"><input type="text" class="form-control" id="billref0" name="billref[]" placeholder="บิล No."></td>
                                <td width="33.33%"><input type="text" class="form-control" id="payperbill0" name="payperbill[]" placeholder="จ่ายจริง"></td>
                            </tr>
                            </tbody>

                        </table>
                    </div>
                    <div class="col-md-4">

                    </div>

                </div>
                <div class="row">
                    <br>
                </div>
                <div class="row">
                    <div class="col-md-3">

                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="col-xs-6 control-label">เลือกบริษัทโอน<i><span style="color: red">*</span></i></label>
                            <div class="col-xs-12">
                                <select name="company_id_transfer" id="company_id_transfer" class="form-control"  required>
                                    <?php
                                    $db = Connectdb::Databaseall();
                                    $data = DB::connection('mysql')->select('SELECT * FROM '.$db['hr_base'].'.working_company WHERE status ="1"');
                                    ?>
                                    @foreach ($data as $value)
                                        <option value="{{$value->id}}">{{$value->name}}</option>

                                    @endforeach

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="col-xs-6 control-label">เลือกสาขาโอน<i><span style="color: red">*</span></i></label>
                            <div class="col-xs-12">
                                <?php
                                $db = Connectdb::Databaseall();
                                $databranch = DB::connection('mysql')->select('SELECT * FROM '.$db['hr_base'].'.branch WHERE status ="1"');
                                ?>
                                <select name="branch_id_transfer" id="branch_id_transfer" class="form-control"  required>

                                    @foreach ($databranch as $valuebranch)
                                        <option value="{{$valuebranch->code_branch}}">{{$valuebranch->name_branch}}   ({{$valuebranch->code_branch}})</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">

                    </div>
                </div>
                <div class="row">
                    <br>
                </div>
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-xs-5 col-xs-offset-3">
                                <button type="submit" id="Btn_save" class="btn btn-primary" onclick="savepo()">โอน</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
</div>



<!---   myModal approved--->
