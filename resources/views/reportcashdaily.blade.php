<?php

use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;
use  App\Api\DateTime;

?>
@include('headmenu')

<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>
<link rel="stylesheet" type="text/css" href="css/ui/jquery-ui.css">

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>

<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap2.min.js'></script>

<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">

<script type="text/javascript" src = 'js/report/cash.js'></script>
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet"/>  datepicker year bootstrap-->

<!-- <script src="js/code/highcharts.js"></script>
<script src="js/code/modules/exporting.js"></script>
<script src="js/code/modules/export-data.js"></script> -->


<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


<style>
    .ui-autocomplete-input {
        border: none;
        font-size: 14px;
        width: 225px;
        height: 24px;
        margin-bottom: 5px;
        padding-top: 2px;
        border: 1px solid #DDD !important;
        padding-top: 0px !important;
        z-index: 1511;
        position: relative;
    }
    .ui-menu .ui-menu-item a {
        font-size: 12px;
    }
    .ui-autocomplete {
        position: absolute;
        top: 0;
        left: 0;
        z-index: 1510 !important;
        float: left;
        display: none;
        min-width: 160px;
        width: 160px;
        padding: 4px 0;
        margin: 2px 0 0 0;
        list-style: none;
        background-color: #ffffff;
        border-color: #ccc;
        border-color: rgba(0, 0, 0, 0.2);
        border-style: solid;
        border-width: 1px;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding;
        background-clip: padding-box;
        *border-right-width: 2px;
        *border-bottom-width: 2px;
    }
    .ui-menu-item > a.ui-corner-all {
        display: block;
        padding: 3px 15px;
        clear: both;
        font-weight: normal;
        line-height: 18px;
        color: #555555;
        white-space: nowrap;
        text-decoration: none;
    }
    .ui-state-hover, .ui-state-active {
        color: #ffffff;
        text-decoration: none;
        background-color: #0088cc;
        border-radius: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        background-image: none;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-cog home-icon"></i>
                        <a href="#">รายงาน</a>
                    </li>
                    <li class="active">รายงานเงินสดย่อยรายวัน</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>

            <div class="box-body" style="overflow-x:auto;">
              <form action="searchreportcashdaily" method="post">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-2">

                    </div>

                    <div class="col-md-3">
                        <p class="text-right">
                          ค้นหาสาขา
                        </p>
                    </div>
                    <div class="col-md-2">
                      <?php
                        $db = Connectdb::Databaseall();
                        $sql = 'SELECT '.$db['hr_base'].'.branch.*
                                FROM '.$db['hr_base'].'.branch
                                WHERE '.$db['hr_base'].'.branch.status = "1"';

                        $brcode = DB::connection('mysql')->select($sql);
                      ?>
                        <select name="branch_id" id="branch_id" class="form-control" required>
                          <option value="">เลือกสาขา</option>
                          <?php foreach ($brcode as $key => $value) { ?>
                              <option value="<?php echo $value->code_branch?>" <?php if(isset($query)){ if($branch_id==$value->code_branch){ echo "selected";} }?>><?php echo $value->name_branch?></option>
                          <?php } ?>
                        </select>
                    </div>

                    <div class="col-md-5">

                    </div>
                </div>

                <div class="row">
                    <br>
                </div>

                <div class="row">
                    <div class="col-md-2">

                    </div>
                    <div class="col-md-3">
                        <p class="text-right">
                         วันที่
                        </p>
                    </div>
                    <div class="col-md-2">
                          <input type="text" name="datepicker" id="datepicker" value="<?php if(isset($query)){ print_r($datepicker); }?>" class="form-control datepicker" required readonly>
                    </div>

                    <div class="col-md-2">

                    </div>
                    <div class="col-md-3">

                    </div>
                </div>

                <div class="row">
                    <br>
                </div>

                <div class="row">
                  <div class="col-md-5">

                  </div>
                  <div class="col-md-3">
                    <input type="submit" class="btn btn-primary" value="ค้นหา">
                    <input type="reset" class="btn btn-danger">
                  </div>


                  <div class="col-md-4">

                  </div>

                </form>

              </div>

                  <div class="row">
                      <br>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <?php
                      if(isset($query)){
                          // echo "<pre>";
                          // print_r($data);
                          // exit;
                        ?>

                        <form action="configreportcashdaily" onSubmit="if(!confirm('ยืนยันการทำรายการ?')){return false;}" method="post" class="form-horizontal">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <table class="table table-striped">
                           <thead>
                             <tr>
                               <th>วันที่</th>
                               <th>ชื่อลูกค้า</th>
                               <th>ประเภท</th>
                               <th>เงินทอน</th>
                               <th>สินค้าตัดหาย</th>
                               <th>รายรับ</th>
                               <th>รายจ่าย</th>
                               <th>รับประกัน</th>
                               <th>คืนประกัน</th>
                               <th>คงเหลือ</th>
                             </tr>
                           </thead>
                           <tbody>

                           <?php  //ยอดยกมา

                             $brcode = $branch_id;
                             $datetime = $datepicker2;

                             $db = Connectdb::Databaseall();
                             $sql = 'SELECT * FROM '.$db['fsctaccount'].'.cash
                                     WHERE '.$db['fsctaccount'].'.cash.branch_id = "'.$brcode.'"
                                     AND '.$db['fsctaccount'].'.cash.time = "'.$datetime.'"
                                    ';

                             $dataresult = DB::connection('mysql')->select($sql);

                             $sumall =0;
                           ?>
                           <tr>
                             <td></td>
                             <td></td>
                             <td ><?php echo '<span style="color: red;" />ยกยอดมา</span>'; ?></td>
                             <td><font color="red"><u>
                               <?php
                                 if($dataresult){
                                   echo $dataresult[0]->grandtotal;
                                 }
                               ?>
                             </u></font></td>
                             <td></td>
                             <td></td>
                             <!-- <td></td>
                             <td></td> -->
                             <td></td>
                             <td></td>
                             <td></td>
                             <td><font color="red"><u>
                               <?php
                                 if($dataresult){
                                   $sumall = $sumall + $dataresult[0]->grandtotal;
                                   echo $dataresult[0]->grandtotal;
                                 }
                               ?>
                             </u></font></td>
                               <?php if($dataresult){
                                 // echo "<pre>";
                                 // print_r($dataresult);
                                 // exit;
                               ?>

                             <input type="hidden" name="grandtotal[]" value="<?php echo $dataresult[0]->grandtotal ?>" class="form-control">
                             <input type="hidden" name="time[]" value="<?php echo $dataresult[0]->time ?>" class="form-control">
                             <input type="hidden" name="branch_id[]" value="<?php echo $dataresult[0]->branch_id ?>" class="form-control">

                             <?php } ?>
                           </tr>

                           <?php

                            $sumsubtotalinput = 0;
                            $sumsubtotalouput = 0;
                            $sumlastinput = 0;
                            $sumlastpay = 0;
                            $sumlastinsu = 0;
                            $sumlastinpay = 0;
                            $sumlastinputall = 0;
                            $sumsubtotalinputnew = 0;

                            $sumtotalloss = 0; //รวมค่าสินค้าตัดหาย
                            $sumtotalinsurance = 0; //รวมรับประกัน
                            $sumtotalinsurancepay = 0; //รวมคืนประกัน
                            $sumtotalall = 0; //รวมรายรับ

                            $sumall1 =0;

                           foreach ($data as $key => $value) {
                           ?>

                           <tr>
                              <td><!--วันที่-->
                                 <?php echo ($value->startdate); ?>
                                 <input type="hidden" name="startdate[]" value="<?php echo $value->startdate ?>" class="form-control">
                              </td>
                              <td><!--ชื่อ - นามสกุล-->
                                 <?php
                                      $modelcus = Maincenter::getdatacustomer($value->customer_id);
                                      if($modelcus){
                                        echo ($modelcus[0]->name);
                                  ?>&nbsp
                                  <?php
                                        echo ($modelcus[0]->lastname);
                                      }
                                  ?>
                                </td>
                              <td>
                                <?php
                                  //รับค่าเช่า
                                  if($value->bill_rent){
                                    // echo "<br>";
                                    echo '<span style="color: green;" /><center>รับค่าเช่า</center></span>';
                                    echo ($value->bill_rent);
                                  }

                                  //คืนประกัน
                                  // $modelbillrent = Maincenter::getbillrent($value->id,$datepicker2);
                                  // // echo "<pre>";
                                  // // print_r($modelbillrent);
                                  // // exit;
                                  // if($modelbillrent){
                                  //    echo "<br>";
                                  //    echo '<span style="color: green;" /><center>คืนประกัน</center></span>';
                                  //    echo $modelbillrent[0]->numberrun;
                                  // }
                                ?>
                              </td>
                              <td></td>
                              <td><font color="green">
                                <?php
                                  //สินค้าตัดหาย
                                  // $sqlloss =  'SELECT sum('.$db['fsctaccount'].'.taxinvoice_loss_abb.grandtotal)  as total
                                  //              FROM '.$db['fsctaccount'].'.taxinvoice_loss_abb
                                  //              WHERE branch_id = "'.$brcode.'"
                                  //              AND bill_rent = "'.$value->idbillrent.'"
                                  //              AND status != "99"
                                  //              AND type NOT IN (2)
                                  //              ';
                                  //
                                  // $resultloss =  DB::connection('mysql')->select($sqlloss);
                                  //
                                  // if($resultloss){
                                  //   // $sumsubtotalinput = $sumsubtotalinput + $resultloss[0]->total;
                                  //   $sumtotalloss = $sumtotalloss +  $resultloss[0]->total;
                                  //   echo number_format($resultloss[0]->total,2);
                                  // }else{
                                  //   echo "0.00";
                                  //   // $sumsubtotalinput = $sumsubtotalinput + 0;
                                  //   $sumtotalloss = $sumtotalloss + 0;
                                  // }
                                ?>
                              </td></font>
                              <td><font color="green">
                               <?php
                                  //รายรับ
                                  $sql =  'SELECT sum('.$db['fsctaccount'].'.taxinvoice_abb.grandtotal)  as total,
                                                  '.$db['fsctaccount'].'.taxinvoice_abb.*

                                           FROM '.$db['fsctaccount'].'.taxinvoice_abb
                                           WHERE bill_rent = "'.$value->idbillrent.'"
                                           AND status != "99"
                                           AND type NOT IN (2)
                                           AND gettypemoney IN (0,1,2)
                                           ';

                                  $result = DB::connection('mysql')->select($sql);
                                  // echo "<pre>";
                                  // print_r($result);
                                  // exit;

                                  if($result){
                                    if($result[0]->gettypemoney == '1'){ //รับเงินสด
                                      // $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                                      $sumtotalall = $sumtotalall +  $result[0]->total;
                                      echo number_format($result[0]->total,2);
                                    }
                                    elseif ($result[0]->gettypemoney == '2'){ //รับเงินโอน
                                      echo "[";
                                      echo number_format ($result[0]->total,2);
                                      echo "]";
                                    }
                                    else{
                                      // $sumsubtotalinput = $sumsubtotalinput + 0;
                                      $sumtotalall = $sumtotalall + 0;
                                      echo "0";
                                    }
                                  }

                                  //   $sqlmore =  'SELECT sum('.$db['fsctaccount'].'.taxinvoice_more_abb.grandtotal)  as total
                                  //                FROM '.$db['fsctaccount'].'.taxinvoice_more_abb
                                  //                WHERE bill_rent = "'.$value->id.'"
                                  //                AND status != "99"
                                  //                AND type NOT IN (2)
                                  //                ';
                                  //
                                  //   $resultmore= DB::connection('mysql')->select($sqlmore);
                                  //
                                  //   if($resultmore){
                                  //     $sumsubtotalinput = $sumsubtotalinput + $resultmore[0]->total;
                                  //     $sumtotal1 = $sumtotal1 + $resultmore[0]->total;
                                  //     //echo number_format($resultloss[0]->total,2);
                                  //   }else{
                                  //     //echo "0";
                                  //     $sumsubtotalinput = $sumsubtotalinput + 0;
                                  //     $sumtotal1 = $sumtotal1 + 0;
                                  //   }
                                  //
                                  //
                                  //   $sqlpa =  'SELECT sum('.$db['fsctaccount'].'.taxinvoice_partial_abb.grandtotal)  as total
                                  //              FROM '.$db['fsctaccount'].'.taxinvoice_partial_abb
                                  //              WHERE bill_rent = "'.$value->id.'"
                                  //              AND status != "99"
                                  //              AND type NOT IN (2)
                                  //              ';
                                  //
                                  //   $resultpa= DB::connection('mysql')->select($sqlpa);
                                  //
                                  //   if($resultpa){
                                  //     $sumsubtotalinput = $sumsubtotalinput + $resultpa[0]->total;
                                  //     $sumtotal1 = $sumtotal1 +  $resultpa[0]->total;
                                  //     //echo number_format($resultloss[0]->total,2);
                                  //   }else{
                                  //     //echo "0";
                                  //     $sumsubtotalinput = $sumsubtotalinput + 0;
                                  //     $sumtotal1 = $sumtotal1 + 0;
                                  //   }
                                  //
                                  //
                                  // $sqlsp =  'SELECT sum('.$db['fsctaccount'].'.taxinvoice_special_abb.grandtotal)  as total
                                  //            FROM '.$db['fsctaccount'].'.taxinvoice_special_abb
                                  //            WHERE bill_rent = "'.$value->id.'"
                                  //            AND status != "99"
                                  //            AND type NOT IN (2)
                                  //            ';
                                  //
                                  // $resultsp= DB::connection('mysql')->select($sqlsp);
                                  //
                                  // if($resultsp){
                                  //   $sumsubtotalinput = $sumsubtotalinput + $resultsp[0]->total;
                                  //   $sumtotal1 = $sumtotal1 + $resultsp[0]->total;
                                  //   //echo number_format($resultloss[0]->total,2);
                                  // }else{
                                  //   //echo "0";
                                  //   $sumsubtotalinput = $sumsubtotalinput + 0;
                                  //   $sumtotal1 = $sumtotal1 + 0;
                                  // }

                                  // $sumlastinput = $sumlastinput + $sumsubtotalinput;
                                  // echo number_format($sumsubtotalinput,2);
                                  // $sumsubtotalinput = 0;
                              ?>
                              </td></font>
                              <td><font color="red"> <?php //รายจ่าย ?> </td></font>
                              <td><!--รับประกัน-->
                                <?php
                                  //รับประกัน (โอน)
                                  if($value->type_insurance_id == '2')
                                  {
                                    echo '<span style="color: green;" />';
                                    echo "[";
                                    echo ($value->insurance_money);
                                    echo "]";
                                    echo '</span>';
                                  }
                                  else //รับประกัน (เงินสด)
                                  {
                                    $sumtotalinsurance = $sumtotalinsurance +  $value->insurance_money;
                                    echo '<span style="color: green;" />';
                                    echo ($value->insurance_money);
                                    echo '</span>';
                                  }

                                ?>
                              </td>
                              <td>
                                <?php //คืนประกัน
                                  // if($modelbillrent){
                                  //    echo "<br>";
                                  //    echo "<br>";
                                  //    echo '<span style="color: red;" />';
                                  //    echo '[';
                                  //    echo $modelbillrent[0]->insurance_money;
                                  //    echo ']';
                                  //    echo '</span>';
                                  // }
                                ?>
                              </td>
                              <td><!--คงเหลือ-->
                                <?php
                                  $sumall1 = ($sumtotalall + $sumtotalinsurance) + $sumall;
                                  echo number_format($sumall1,2);
                                  // echo $dataresult[0]->grandtotal;
                                ?>
                              </td>
                           </tr>
                           <?php  }  ?>

                           <?php //ของหาย
                           foreach ($dataloss as $key10 => $value10) {
                           ?>
                           <tr>
                              <td><!--วันที่-->
                                 <?php echo ($value10->startdate); ?>
                                 <input type="hidden" name="startdate[]" value="<?php echo $value10->startdate ?>" class="form-control">
                              </td>
                              <td><!--ชื่อ - นามสกุล-->
                                 <?php
                                      $modelcus = Maincenter::getdatacustomer($value10->customer_id);
                                      if($modelcus){
                                        echo ($modelcus[0]->name);
                                  ?>&nbsp
                                  <?php
                                        echo ($modelcus[0]->lastname);
                                      }
                                  ?>
                                </td>
                              <td>
                                <?php
                                  //รับค่าเช่า
                                  if($value10->number_taxinvoice){
                                    // echo "<br>";
                                    echo '<span style="color: green;" /><center>รับค่าเช่าของหาย</center></span>';
                                    echo ($value10->number_taxinvoice);
                                  }
                                ?>
                              </td>
                              <td></td>
                              <td><font color="green">
                                <?php
                                  //สินค้าตัดหาย
                                  $sumtotalloss = $sumtotalloss +  $value10->grandtotal;
                                  echo ($value10->grandtotal);
                                ?>
                              </td></font>
                              <td><font color="green"> <?php //รายรับ ?> </td></font>
                              <td><font color="red"> <?php //รายจ่าย ?> </td></font>
                              <td> <?php //รับประกัน ?> </td>
                              <td> <?php //คืนประกัน ?> </td>
                              <td><!--คงเหลือ-->
                                <?php
                                  $sumall1 = $sumall1 + $sumtotalloss;
                                  echo number_format($sumall1,2);
                                ?>
                              </td>
                           </tr>
                           <?php  }  ?>

                           <?php //เพิ่มเติม
                           foreach ($datamore as $key2 => $value2) {
                           ?>
                           <tr>
                              <td><!--วันที่-->
                                 <?php echo ($value2->startdate); ?>
                                 <input type="hidden" name="startdate[]" value="<?php echo $value2->startdate ?>" class="form-control">
                              </td>
                              <td><!--ชื่อ - นามสกุล-->
                                 <?php
                                      $modelcus = Maincenter::getdatacustomer($value2->customer_id);
                                      if($modelcus){
                                        echo ($modelcus[0]->name);
                                  ?>&nbsp
                                  <?php
                                        echo ($modelcus[0]->lastname);
                                      }
                                  ?>
                                </td>
                              <td>
                                <?php
                                  //รับค่าเช่า
                                  if($value2->number_taxinvoice){
                                    // echo "<br>";
                                    echo '<span style="color: green;" /><center>รับค่าเช่าเพิ่มเติม</center></span>';
                                    echo ($value2->number_taxinvoice);
                                  }
                                ?>
                              </td>
                              <td><?php  ?></td>
                              <td><font color="green"> <?php //สินค้าตัดหาย ?> </td></font>
                              <td><font color="green">
                               <?php
                                  //รายรับ
                                  $sumtotalall = $sumtotalall +  $value2->grandtotal;
                                  echo ($value2->grandtotal);
                               ?>
                              </td></font>
                              <td><font color="red"><?php //รายจ่าย ?> </td></font>
                              <td> <?php //รับประกัน ?> </td>
                              <td> <?php //คืนประกัน ?> </td>
                              <td><!--คงเหลือ-->
                                <?php
                                  $sumall1 = $sumall1 + $sumtotalall;
                                  echo number_format($sumall1,2);
                                ?>
                              </td>
                           </tr>
                           <?php  }  ?>

                           <?php //แบบย่อย
                           foreach ($datapa as $key3 => $value3) {
                           ?>
                           <tr>
                              <td><!--วันที่-->
                                 <?php echo ($value3->startdate); ?>
                              <input type="hidden" name="startdate[]" value="<?php echo $value3->startdate ?>" class="form-control">
                              </td>
                              <td><!--ชื่อ - นามสกุล-->
                                 <?php
                                      $modelcus = Maincenter::getdatacustomer($value3->customer_id);
                                      if($modelcus){
                                        echo ($modelcus[0]->name);
                                  ?>&nbsp
                                  <?php
                                        echo ($modelcus[0]->lastname);
                                      }
                                  ?>
                                </td>
                              <td>
                                <?php
                                //รับค่าเช่า
                                if($value3->number_taxinvoice){
                                  // echo "<br>";
                                  echo '<span style="color: green;" /><center>รับค่าเช่าแบบย่อย</center></span>';
                                  echo ($value3->number_taxinvoice);
                                }
                                ?>
                              </td>
                              <td><?php  ?></td>
                              <td><font color="green"> <?php //สินค้าตัดหาย ?> </td></font>
                              <td><font color="green">
                               <?php
                                 //รายรับ
                                 $sumtotalall = $sumtotalall +  $value3->grandtotal;
                                 echo ($value3->grandtotal);
                               ?>
                              </td></font>
                              <td><font color="red"> <?php //รายจ่าย ?> </td></font>
                              <td> <?php //รับประกัน ?> </td>
                              <td> <?php //คืนประกัน ?> </td>
                              <td><!--คงเหลือ-->
                                <?php
                                  $sumall1 = $sumall1 + $sumtotalall;
                                  echo number_format($sumall1,2);
                                ?>
                              </td>
                           </tr>
                           <?php  }  ?>

                           <?php //กรณีพิเศษ
                           foreach ($dataspa as $key4 => $value4) {
                           ?>
                           <tr>
                              <td><!--วันที่-->
                                 <?php echo ($value4->startdate); ?>
                                 <input type="hidden" name="startdate[]" value="<?php echo $value4->startdate ?>" class="form-control">
                              </td>
                              <td><!--ชื่อ - นามสกุล-->
                                 <?php
                                      $modelcus = Maincenter::getdatacustomer($value4->customer_id);
                                      if($modelcus){
                                        echo ($modelcus[0]->name);
                                  ?>&nbsp
                                  <?php
                                        echo ($modelcus[0]->lastname);
                                      }
                                  ?>
                                </td>
                              <td>
                                <?php
                                //รับค่าเช่า
                                if($value4->number_taxinvoice){
                                  // echo "<br>";
                                  echo '<span style="color: green;" /><center>รับค่าขนส่ง</center></span>';
                                  echo ($value4->number_taxinvoice);
                                }
                                ?>
                              </td>
                              <td><?php  ?></td>
                              <td><font color="green"> <?php //สินค้าตัดหาย ?> </td></font>
                              <td><font color="green">
                               <?php
                                 //รายรับ
                                 $sumtotalall = $sumtotalall +  $value4->grandtotal;
                                 echo ($value4->grandtotal);
                               ?>
                              </td></font>
                              <td><font color="red"> <?php //รายจ่าย ?> </td></font>
                              <td> <?php //รับประกัน ?> </td>
                              <td> <?php //คืนประกัน ?> </td>
                              <td><!--คงเหลือ-->
                                <?php
                                  $sumall1 = $sumall1 + $sumtotalall;
                                  echo number_format($sumall1,2);
                                ?>
                              </td>
                           </tr>
                           <?php  }  ?>

                           <?php //คืนประกัน
                           foreach ($datareturn as $key1 => $value1) {
                           ?>
                           <tr>
                              <td><!--วันที่-->
                                 <?php echo ($value1->startdate); ?>
                                 <input type="hidden" name="startdate[]" value="<?php echo $value1->startdate ?>" class="form-control">
                              </td>
                              <td><!--ชื่อ - นามสกุล-->
                                 <?php
                                      $modelcus = Maincenter::getdatacustomer($value1->customer_id);
                                      if($modelcus){
                                      echo ($modelcus[0]->name);
                                  ?>&nbsp
                                  <?php
                                      echo ($modelcus[0]->lastname);
                                      }
                                  ?>
                                </td>
                              <td>
                                <?php //คืนประกัน
                                  if($value1->numberrun){
                                    // echo "<br>";
                                    echo '<span style="color: red;" /><center>คืนประกัน</center></span>';
                                    echo ($value1->numberrun);
                                  }

                                  //คืนประกัน
                                  // $modelbillrentengine = Maincenter::getbillrentengine($value1->idbillrent,$datepicker2);
                                  // // echo "<pre>";
                                  // // print_r($modelbillrentengine);
                                  // // exit;
                                  // if($modelbillrentengine){
                                  //    echo "<br>";
                                  //    echo '<span style="color: green;" /><center>คืนประกัน</center></span>';
                                  //    echo $modelbillrentengine[0]->numberrun;
                                  // }
                                ?>
                              </td>
                              <td><?php  ?></td>
                              <td><font color="green">
                                <?php //สินค้าตัดหาย
                          //         $sqlloss =  'SELECT sum('.$db['fsctaccount'].'.taxinvoice_loss_abb.grandtotal)  as total
                          //                      FROM '.$db['fsctaccount'].'.taxinvoice_loss_abb
                          //                      WHERE branch_id = "'.$brcode.'"
                          //                      AND bill_rent = "'.$value1->id.'"
                          //                      AND status != "99"
                          //                      AND type = 2
                          //                      ';
                          //
                          //         $resultloss =  DB::connection('mysql')->select($sqlloss);
                          //
                          //         if($resultloss){
                          //           // $sumsubtotalinput = $sumsubtotalinput + $resultloss[0]->total;
                          //           // $sumtotal1 = $sumtotal1 +  $resultloss[0]->total;
                          //           echo number_format($resultloss[0]->total,2);
                          //         }else{
                          //           // echo "0.00";
                          //           // $sumsubtotalinput = $sumsubtotalinput + 0;
                          //           // $sumtotal1 = $sumtotal1 + 0;
                          //         }
                          //       ?>
                               <!-- </td></font>
                               <td><font color="green"> -->
                                <?php //รายรับ
                          //
                          //     $sql =  'SELECT sum('.$db['fsctaccount'].'.taxinvoice_abb.grandtotal)  as total
                          //              FROM '.$db['fsctaccount'].'.taxinvoice_abb
                          //              WHERE bill_rent = "'.$value1->id.'"
                          //              AND status != "99"
                          //              AND type = 2
                          //              ';
                          //
                          //     $result = DB::connection('mysql')->select($sql);
                          //
                          //     // echo "======";
                          //     if($result){
                          //       $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                          //       $sumtotal1 = $sumtotal1 +  $result[0]->total;
                          //       // echo number_format($result[0]->total,2);
                          //     }else{
                          //       //echo "0";
                          //       $sumsubtotalinput = $sumsubtotalinput + 0;
                          //       $sumtotal1 = $sumtotal1 + 0;
                          //     }
                          //
                          //
                          //   $sqlmore =  'SELECT sum('.$db['fsctaccount'].'.taxinvoice_more_abb.grandtotal)  as total
                          //                FROM '.$db['fsctaccount'].'.taxinvoice_more_abb
                          //                WHERE bill_rent = "'.$value1->id.'"
                          //                AND status != "99"
                          //                AND type = 2
                          //                ';
                          //
                          //   $resultmore= DB::connection('mysql')->select($sqlmore);
                          //
                          //   if($resultmore){
                          //     $sumsubtotalinput = $sumsubtotalinput + $resultmore[0]->total;
                          //     $sumtotal1 = $sumtotal1 + $resultmore[0]->total;
                          //     //echo number_format($resultloss[0]->total,2);
                          //   }else{
                          //     //echo "0";
                          //     $sumsubtotalinput = $sumsubtotalinput + 0;
                          //     $sumtotal1 = $sumtotal1 + 0;
                          //   }
                          //
                          //
                          //   $sqlpa =  'SELECT sum('.$db['fsctaccount'].'.taxinvoice_partial_abb.grandtotal)  as total
                          //              FROM '.$db['fsctaccount'].'.taxinvoice_partial_abb
                          //              WHERE bill_rent = "'.$value1->id.'"
                          //              AND status != "99"
                          //              AND type = 2
                          //              ';
                          //
                          //   $resultpa= DB::connection('mysql')->select($sqlpa);
                          //
                          //   if($resultpa){
                          //     $sumsubtotalinput = $sumsubtotalinput + $resultpa[0]->total;
                          //     $sumtotal1 = $sumtotal1 +  $resultpa[0]->total;
                          //     //echo number_format($resultloss[0]->total,2);
                          //   }else{
                          //     //echo "0";
                          //     $sumsubtotalinput = $sumsubtotalinput + 0;
                          //     $sumtotal1 = $sumtotal1 + 0;
                          //   }
                          //
                          //
                          // $sqlsp =  'SELECT sum('.$db['fsctaccount'].'.taxinvoice_special_abb.grandtotal)  as total
                          //            FROM '.$db['fsctaccount'].'.taxinvoice_special_abb
                          //            WHERE bill_rent = "'.$value1->id.'"
                          //            AND status != "99"
                          //            AND type = 2
                          //            ';
                          //
                          // $resultsp= DB::connection('mysql')->select($sqlsp);
                          //
                          // if($resultsp){
                          //   $sumsubtotalinput = $sumsubtotalinput + $resultsp[0]->total;
                          //   $sumtotal1 = $sumtotal1 + $resultsp[0]->total;
                          //   //echo number_format($resultloss[0]->total,2);
                          // }else{
                          //   //echo "0";
                          //   $sumsubtotalinput = $sumsubtotalinput + 0;
                          //   $sumtotal1 = $sumtotal1 + 0;
                          // }
                          //
                          //   $sumlastinput = $sumlastinput + $sumsubtotalinput;
                          //   echo number_format($sumsubtotalinput,2);
                          //   $sumsubtotalinput = 0;

                               ?>
                              </td></font>
                              <td><font color="red"> <?php //รายจ่าย ?> </td></font>
                              <td>
                                <?php //รับประกัน
                                  // echo '<span style="color: green;" />';
                                  // echo ($value1->insurance_money);
                                  // echo '</span>';
                                ?>
                              </td>
                              <td>
                                <?php //คืนประกัน
                                  // if($modelbillrentengine){
                                     // echo "<br>";
                                     // echo "<br>";
                                     $sumtotalinsurancepay = $sumtotalinsurancepay + $value1->insurance_money;
                                     echo '<span style="color: red;" />';
                                     echo '[';
                                     echo $value1->insurance_money;
                                     echo ']';
                                     echo '</span>';
                                  // }
                                ?>
                              </td>
                              <td><?php echo number_format($sumall1,2); ?></td><!--คงเหลือ-->
                           </tr>

                           <?php  }  ?>

                           <?php //บิลเช่า (เครื่องยนต์)
                           foreach ($dataengine as $key5 => $value5) {
                           // echo "<pre>";
                           // print_r($value5);
                           // exit;
                           ?>

                           <tr>
                              <td><!--วันที่-->
                                 <?php echo ($value5->startdate); ?>
                                 <input type="hidden" name="startdate[]" value="<?php echo $value5->startdate ?>" class="form-control">
                              </td>
                              <td><!--ชื่อ - นามสกุล-->
                                 <?php
                                      $modelcus = Maincenter::getdatacustomer($value5->customer_id);
                                      if($modelcus){
                                      echo ($modelcus[0]->name);
                                  ?>&nbsp
                                  <?php
                                      echo ($modelcus[0]->lastname);
                                      }
                                  ?>
                                </td>
                              <td>
                                <?php
                                  //รับค่าเช่า
                                  if($value5->billengine_rent){
                                    // echo "<br>";
                                    echo '<span style="color: green;" /><center>รับค่าเช่า</center></span>';
                                    echo ($value5->billengine_rent);
                                  }

                                  //คืนประกัน
                                  // $modelbillrentengine = Maincenter::getbillrentengine($value5->idbillrent,$datepicker2);
                                  // // echo "<pre>";
                                  // // print_r($modelbillrentengine);
                                  // // exit;
                                  // if($modelbillrentengine){
                                  //    echo "<br>";
                                  //    echo '<span style="color: green;" /><center>คืนประกัน</center></span>';
                                  //    echo $modelbillrentengine[0]->numberrun;
                                  // }
                                ?>
                              </td>
                              <td><?php  ?></td>
                              <td><font color="green">
                                <?php
                                  //สินค้าตัดหาย
                                  // $sqlloss =  'SELECT sum('.$db['fsctaccount'].'.taxinvoice_loss_abb.grandtotal)  as total
                                  //              FROM '.$db['fsctaccount'].'.taxinvoice_loss_abb
                                  //              WHERE branch_id = "'.$brcode.'"
                                  //              AND bill_rent = "'.$value5->idbillrent.'"
                                  //              AND status != "99"
                                  //              AND type = 2
                                  //              ';
                                  //
                                  // $resultloss =  DB::connection('mysql')->select($sqlloss);
                                  //
                                  // if($resultloss){
                                  //   // $sumsubtotalinput = $sumsubtotalinput + $resultloss[0]->total;
                                  //   $sumtotalloss = $sumtotalloss +  $resultloss[0]->total;
                                  //   echo number_format($resultloss[0]->total,2);
                                  // }else{
                                  //   // echo "0.00";
                                  //   // $sumsubtotalinput = $sumsubtotalinput + 0;
                                  //   $sumtotalloss = $sumtotalloss + 0;
                                  // }
                                ?>
                              </td></font>
                              <td><font color="green">
                               <?php
                                //รายรับ
                                $sql =  'SELECT sum('.$db['fsctaccount'].'.taxinvoice_abb.grandtotal)  as total,
                                                '.$db['fsctaccount'].'.taxinvoice_abb.*

                                         FROM '.$db['fsctaccount'].'.taxinvoice_abb
                                         WHERE bill_rent = "'.$value5->idbillrent.'"
                                         AND status != "99"
                                         AND type = 2
                                         AND gettypemoney IN (0,1,2)
                                         ';

                                $result = DB::connection('mysql')->select($sql);
                                // echo "<pre>";
                                // print_r($result);
                                // exit;
                                if($result){
                                  if($result[0]->gettypemoney == '1'){ //รับเงินสด
                                    // $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                                    // $sumall1 = $sumall1 + $result[0]->total;
                                    $sumtotalall = $sumtotalall + $result[0]->total;

                                    $sumall1 = $sumall1 + $result[0]->total;
                                    echo number_format($result[0]->total,2);
                                  }
                                  elseif ($result[0]->gettypemoney == '2'){ //รับเงินโอน
                                    echo "[";
                                    echo number_format ($result[0]->total,2);
                                    echo "]";
                                  }
                                  else{
                                    // $sumsubtotalinput = $sumsubtotalinput + 0;
                                    $sumall1 = $sumall1 + 0;
                                    $sumtotalall = $sumtotalall + 0;
                                    echo "0";
                                  }
                                }

                                // if($result){
                                //   // $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                                //   $sumtotalall = $sumtotalall +  $result[0]->total;
                                //   echo number_format($result[0]->total,2);
                                // }else{
                                //   echo "0";
                                //   // $sumsubtotalinput = $sumsubtotalinput + 0;
                                //   $sumtotalall = $sumtotalall + 0;
                                // }

                                // $sumlastinput = $sumlastinput + $sumsubtotalinput;
                                // echo number_format($sumsubtotalinput,2);
                                // $sumsubtotalinput = 0;
                               ?>
                              </td></font>
                              <td><font color="red"> <?php //รายจ่าย ?> </td></font>
                              <td><!--รับประกัน-->
                                <?php
                                  // echo ($value5->type_insurance_id);

                                  //รับประกัน (โอน)
                                  if($value5->type_insurance_id == '2')
                                  {
                                    echo '<span style="color: green;" />';
                                    echo "[";
                                    echo ($value5->insurance_money);
                                    echo "]";
                                    echo '</span>';
                                  }
                                  else //รับประกัน (เงินสด)
                                  {
                                    $sumtotalinsurance = $sumtotalinsurance +  $value5->insurance_money;
                                    echo '<span style="color: green;" />';
                                    echo ($value5->insurance_money);
                                    echo '</span>';
                                  }
                                ?>
                              </td>
                              <td>
                                <?php
                                  //คืนประกัน
                                  // if($modelbillrentengine){
                                  //    echo "<br>";
                                  //    echo "<br>";
                                  //    echo '<span style="color: red;" />';
                                  //    echo '[';
                                  //    echo $modelbillrentengine[0]->insurance_money;
                                  //    echo ']';
                                  //    echo '</span>';
                                  // }
                                ?>
                              </td>
                              <td><!--คงเหลือ-->
                                <?php
                                  // $sumall1 = $sumall1 +($sumtotalall + $sumtotalinsurance);
                                  echo number_format($sumall1,2);
                                ?>
                              </td>
                           </tr>

                           <?php  }  ?>

                           <?php //ของหาย (เครื่องยนต์)
                           foreach ($dataengineloss as $key11 => $value11) {
                           ?>

                           <tr>
                             <td><!--วันที่-->
                                <?php echo ($value11->startdate); ?>
                                <input type="hidden" name="startdate[]" value="<?php echo $value11->startdate ?>" class="form-control">
                             </td>
                             <td><!--ชื่อ - นามสกุล-->
                                <?php
                                     $modelcus = Maincenter::getdatacustomer($value11->customer_id);
                                     if($modelcus){
                                       echo ($modelcus[0]->name);
                                 ?>&nbsp
                                 <?php
                                       echo ($modelcus[0]->lastname);
                                     }
                                 ?>
                               </td>
                             <td>
                               <?php
                                 //รับค่าเช่า
                                 if($value11->number_taxinvoice){
                                   // echo "<br>";
                                   echo '<span style="color: green;" /><center>รับค่าเช่าของหาย</center></span>';
                                   echo ($value11->number_taxinvoice);
                                 }
                               ?>
                             </td>
                             <td><?php  ?></td>
                             <td><font color="green">
                               <?php
                                //สินค้าตัดหาย
                                $sumtotalloss = $sumtotalloss +  $value11->grandtotal;
                                echo ($value11->grandtotal);
                               ?>
                             </td></font>
                             <td><font color="green"> <?php //รายรับ ?> </td></font>
                             <td><font color="red"> <?php //รายจ่าย ?> </td></font>
                             <td> <?php //รับประกัน ?> </td>
                             <td> <?php //คืนประกัน ?> </td>
                             <td><!--คงเหลือ-->
                               <?php
                                 $sumall1 = $sumall1 + $sumtotalloss;
                                 echo number_format($sumall1,2);
                               ?>
                             </td>
                           </tr>

                           <?php  }  ?>

                          <?php //เพิ่มเติม (เครื่องยนต์)
                          foreach ($dataenginemore as $key7 => $value7) {
                          ?>

                          <tr>
                             <td><!--วันที่-->
                                <?php echo ($value7->startdate); ?>
                                <input type="hidden" name="startdate[]" value="<?php echo $value7->startdate ?>" class="form-control">
                             </td>
                             <td><!--ชื่อ - นามสกุล-->
                                <?php
                                     $modelcus = Maincenter::getdatacustomer($value7->customer_id);
                                     if($modelcus){
                                       echo ($modelcus[0]->name);
                                 ?>&nbsp
                                 <?php
                                       echo ($modelcus[0]->lastname);
                                     }
                                 ?>
                               </td>
                             <td>
                               <?php
                                 //รับค่าเช่า
                                 if($value7->number_taxinvoice){
                                   // echo "<br>";
                                   echo '<span style="color: green;" /><center>รับค่าเช่าเพิ่มเติม</center></span>';
                                   echo ($value7->number_taxinvoice);
                                 }
                               ?>
                             </td>
                             <td><?php  ?></td>
                             <td><font color="green"> <?php //สินค้าตัดหาย ?> </td></font>
                             <td><font color="green">
                              <?php
                                //รายรับ
                                $sumtotalall = $sumtotalall +  $value7->grandtotal;
                                echo ($value7->grandtotal);
                              ?>
                             </td></font>
                             <td><font color="red"> <?php //รายจ่าย ?> </td></font>
                             <td> <?php //รับประกัน ?> </td>
                             <td> <?php //คืนประกัน ?> </td>
                             <td><!--คงเหลือ-->
                               <?php
                                 $sumall1 = $sumall1 + $sumtotalall;
                                 echo number_format($sumall1,2);
                               ?>
                             </td>
                          </tr>

                          <?php  }  ?>

                          <?php //แบบย่อย (เครื่องยนต์)
                          foreach ($dataenginepa as $key8 => $value8) {
                          ?>

                          <tr>
                             <td><!--วันที่-->
                                <?php echo ($value8->startdate); ?>
                                <input type="hidden" name="startdate[]" value="<?php echo $value8->startdate ?>" class="form-control">
                             </td>
                             <td><!--ชื่อ - นามสกุล-->
                                <?php
                                     $modelcus = Maincenter::getdatacustomer($value8->customer_id);
                                     if($modelcus){
                                       echo ($modelcus[0]->name);
                                 ?>&nbsp
                                 <?php
                                       echo ($modelcus[0]->lastname);
                                     }
                                 ?>
                               </td>
                             <td>
                               <?php
                                 //รับค่าเช่า
                                 if($value8->number_taxinvoice){
                                   // echo "<br>";
                                   echo '<span style="color: green;" /><center>รับค่าเช่าแบบย่อย</center></span>';
                                   echo ($value8->number_taxinvoice);
                                 }
                               ?>
                             </td>
                             <td><?php  ?></td>
                             <td><font color="green"> <?php //สินค้าตัดหาย ?> </td></font>
                             <td><font color="green">
                              <?php
                                //รายรับ
                                $sumtotalall = $sumtotalall +  $value8->grandtotal;
                                echo ($value8->grandtotal);
                              ?>
                             </td></font>
                             <td><font color="red"> <?php //รายจ่าย ?> </td></font>
                             <td> <?php //รับประกัน ?> </td>
                             <td> <?php //คืนประกัน ?> </td>
                             <td><!--คงเหลือ-->
                               <?php
                                 $sumall1 = $sumall1 + $sumtotalall;
                                 echo number_format($sumall1,2);
                               ?>
                             </td>
                          </tr>

                          <?php  }  ?>

                          <?php //กรณีพิเศษ (เครื่องยนต์)
                          foreach ($dataenginespa as $key9 => $value9) {
                          ?>

                          <tr>
                             <td><!--วันที่-->
                                <?php echo ($value9->startdate); ?>
                                <input type="hidden" name="startdate[]" value="<?php echo $value9->startdate ?>" class="form-control">
                             </td>
                             <td><!--ชื่อ - นามสกุล-->
                                <?php
                                     $modelcus = Maincenter::getdatacustomer($value9->customer_id);
                                     if($modelcus){
                                       echo ($modelcus[0]->name);
                                 ?>&nbsp
                                 <?php
                                       echo ($modelcus[0]->lastname);
                                     }
                                 ?>
                               </td>
                             <td>
                               <?php
                                 //รับค่าเช่า
                                 if($value9->number_taxinvoice){
                                   // echo "<br>";
                                   echo '<span style="color: green;" /><center>รับค่าขนส่ง</center></span>';
                                   echo ($value9->number_taxinvoice);
                                 }
                               ?>
                             </td>
                             <td><?php  ?></td>
                             <td><font color="green"> <?php //สินค้าตัดหาย ?> </td></font>
                             <td><font color="green">
                              <?php
                                //รายรับ
                                $sumtotalall = $sumtotalall +  $value9->grandtotal;
                                echo ($value9->grandtotal);
                              ?>
                             </td></font>
                             <td><font color="red"> <?php //รายจ่าย ?> </td></font>
                             <td> <?php //รับประกัน ?> </td>
                             <td> <?php //คืนประกัน ?> </td>
                             <td><!--คงเหลือ-->
                               <?php
                                 $sumall1 = $sumall1 + $sumtotalall;
                                 echo number_format($sumall1,2);
                               ?>
                             </td>
                          </tr>

                          <?php  }  ?>

                           <?php //คืนประกัน (เครื่องยนต์)
                           foreach ($datareturnengine as $key6 => $value6) {
                           ?>

                           <tr>
                              <td><!--วันที่-->
                                 <?php echo ($value6->startdate); ?>
                                 <input type="hidden" name="startdate[]" value="<?php echo $value6->startdate ?>" class="form-control">
                              </td>
                              <td><!--ชื่อ - นามสกุล-->
                                 <?php
                                      $modelcus = Maincenter::getdatacustomer($value6->customer_id);
                                      if($modelcus){
                                      echo ($modelcus[0]->name);
                                  ?>&nbsp
                                  <?php
                                      echo ($modelcus[0]->lastname);
                                      }
                                  ?>
                                </td>
                              <td>
                                <?php
                                  //คืนประกัน
                                  if($value6->numberrun){
                                    // echo "<br>";
                                    echo '<span style="color: red;" /><center>คืนประกัน</center></span>';
                                    echo ($value6->numberrun);
                                  }

                                  //คืนประกัน
                                  // $modelbillrentengine = Maincenter::getbillrentengine($value6->idbillrent,$datepicker2);
                                  // // echo "<pre>";
                                  // // print_r($modelbillrentengine);
                                  // // exit;
                                  // if($modelbillrentengine){
                                  //    echo "<br>";
                                  //    echo '<span style="color: green;" /><center>คืนประกัน</center></span>';
                                  //    echo $modelbillrentengine[0]->numberrun;
                                  // }
                                ?>
                              </td>
                              <td><?php  ?></td>
                              <td><font color="green">
                                <?php
                                //สินค้าตัดหาย
                          //         $sqlloss =  'SELECT sum('.$db['fsctaccount'].'.taxinvoice_loss_abb.grandtotal)  as total
                          //                      FROM '.$db['fsctaccount'].'.taxinvoice_loss_abb
                          //                      WHERE branch_id = "'.$brcode.'"
                          //                      AND bill_rent = "'.$value6->id.'"
                          //                      AND status != "99"
                          //                      AND type = 2
                          //                      ';
                          //
                          //         $resultloss =  DB::connection('mysql')->select($sqlloss);
                          //
                          //         if($resultloss){
                          //           // $sumsubtotalinput = $sumsubtotalinput + $resultloss[0]->total;
                          //           // $sumtotal1 = $sumtotal1 +  $resultloss[0]->total;
                          //           echo number_format($resultloss[0]->total,2);
                          //         }else{
                          //           // echo "0.00";
                          //           // $sumsubtotalinput = $sumsubtotalinput + 0;
                          //           // $sumtotal1 = $sumtotal1 + 0;
                          //         }
                               ?>
                               </td></font>
                               <td><font color="green">
                                <?php
                                //รายรับ
                          //
                          //     $sql =  'SELECT sum('.$db['fsctaccount'].'.taxinvoice_abb.grandtotal)  as total
                          //              FROM '.$db['fsctaccount'].'.taxinvoice_abb
                          //              WHERE bill_rent = "'.$value6->id.'"
                          //              AND status != "99"
                          //              AND type = 2
                          //              ';
                          //
                          //     $result = DB::connection('mysql')->select($sql);
                          //
                          //     // echo "======";
                          //     if($result){
                          //       $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                          //       $sumtotal1 = $sumtotal1 +  $result[0]->total;
                          //       // echo number_format($result[0]->total,2);
                          //     }else{
                          //       //echo "0";
                          //       $sumsubtotalinput = $sumsubtotalinput + 0;
                          //       $sumtotal1 = $sumtotal1 + 0;
                          //     }
                          //
                          //
                          //   $sqlmore =  'SELECT sum('.$db['fsctaccount'].'.taxinvoice_more_abb.grandtotal)  as total
                          //                FROM '.$db['fsctaccount'].'.taxinvoice_more_abb
                          //                WHERE bill_rent = "'.$value6->id.'"
                          //                AND status != "99"
                          //                AND type = 2
                          //                ';
                          //
                          //   $resultmore= DB::connection('mysql')->select($sqlmore);
                          //
                          //   if($resultmore){
                          //     $sumsubtotalinput = $sumsubtotalinput + $resultmore[0]->total;
                          //     $sumtotal1 = $sumtotal1 + $resultmore[0]->total;
                          //     //echo number_format($resultloss[0]->total,2);
                          //   }else{
                          //     //echo "0";
                          //     $sumsubtotalinput = $sumsubtotalinput + 0;
                          //     $sumtotal1 = $sumtotal1 + 0;
                          //   }
                          //
                          //
                          //   $sqlpa =  'SELECT sum('.$db['fsctaccount'].'.taxinvoice_partial_abb.grandtotal)  as total
                          //              FROM '.$db['fsctaccount'].'.taxinvoice_partial_abb
                          //              WHERE bill_rent = "'.$value6->id.'"
                          //              AND status != "99"
                          //              AND type = 2
                          //              ';
                          //
                          //   $resultpa= DB::connection('mysql')->select($sqlpa);
                          //
                          //   if($resultpa){
                          //     $sumsubtotalinput = $sumsubtotalinput + $resultpa[0]->total;
                          //     $sumtotal1 = $sumtotal1 +  $resultpa[0]->total;
                          //     //echo number_format($resultloss[0]->total,2);
                          //   }else{
                          //     //echo "0";
                          //     $sumsubtotalinput = $sumsubtotalinput + 0;
                          //     $sumtotal1 = $sumtotal1 + 0;
                          //   }
                          //
                          //
                          // $sqlsp =  'SELECT sum('.$db['fsctaccount'].'.taxinvoice_special_abb.grandtotal)  as total
                          //            FROM '.$db['fsctaccount'].'.taxinvoice_special_abb
                          //            WHERE bill_rent = "'.$value6->id.'"
                          //            AND status != "99"
                          //            AND type = 2
                          //            ';
                          //
                          // $resultsp= DB::connection('mysql')->select($sqlsp);
                          //
                          // if($resultsp){
                          //   $sumsubtotalinput = $sumsubtotalinput + $resultsp[0]->total;
                          //   $sumtotal1 = $sumtotal1 + $resultsp[0]->total;
                          //   //echo number_format($resultloss[0]->total,2);
                          // }else{
                          //   //echo "0";
                          //   $sumsubtotalinput = $sumsubtotalinput + 0;
                          //   $sumtotal1 = $sumtotal1 + 0;
                          // }
                          //
                          //   $sumlastinput = $sumlastinput + $sumsubtotalinput;
                          //   echo number_format($sumsubtotalinput,2);
                          //   $sumsubtotalinput = 0;
                             ?>
                             </td></font>
                              <td><font color="red"> <?php //รายจ่าย ?> </td></font>
                              <td>
                                <?php
                                  //รับประกัน
                                  // echo '<span style="color: green;" />';
                                  // echo ($value6->insurance_money);
                                  // echo '</span>';
                                ?>
                              </td>
                              <td>
                                <?php
                                  //คืนประกัน
                                  // if($modelbillrentengine){
                                     // echo "<br>";
                                     // echo "<br>";
                                     $sumtotalinsurancepay = $sumtotalinsurancepay + $value6->insurance_money;
                                     echo '<span style="color: red;" />';
                                     echo '[';
                                     echo $value6->insurance_money;
                                     echo ']';
                                     echo '</span>';
                                  // }
                                ?>
                              </td>
                              <td><?php echo number_format($sumall1,2); ?></td><!--คงเหลือ-->
                           </tr>
                           <?php  }  ?>

                           <!-- รวม -->
                           <tr>
                             <td colspan="7" align="right"></td>
                             <td></td>
                             <td></td>
                             <td></td>
                           <tr>

                           <tr>
                             <td colspan="7" align="right"><b>รวมค่าสินค้าตัดหาย</b></td>
                             <td></td>
                             <td style="color: green;"><?php echo number_format($sumtotalloss,2); ?></td>
                             <td>บาท</td>
                           <tr>

                           <tr>
                             <td colspan="7" align="right"><b>รวมรายรับ</b></td>
                             <td></td>
                             <td style="color: green;"><?php echo number_format($sumtotalall,2); ?></td>
                             <td>บาท</td>
                            <tr>

                            <tr>
                              <td colspan="7" align="right"><b>รวมรายจ่าย</b></td>
                              <td></td>
                              <td style="color: red;">
                                <?php
                                // $sqlcommission = 'SELECT '.$db['fsctmain'].'.commission.*
                                //    FROM '.$db['fsctmain'].'.commission
                                //    WHERE  status = "1" ';
                                //
                                //    $commission = DB::connection('mysql')->select($sqlcommission);
                                //    // echo "<pre>";
                                //    // print_r($commission);
                                //
                                //    $statuscom = 0;
                                //
                                //     foreach ($commission as $key2 => $value5) {
                                //         if($value5->start_money <= $sumtotal && $sumtotal <= $value5->end_money){
                                //              $statuscom = $value5->percent;
                                //         }
                                //     }
                                //   $resultcommission = $sumtotal * ($statuscom/100);
                                //   echo number_format($resultcommission,2);
                               ?>
                              </td>
                              <td>บาท</td>
                             <tr>

                             <tr>
                               <td colspan="7" align="right"><b>รวมรับประกัน</b></td>
                               <td></td>
                               <td style="color: green;"><?php echo number_format($sumtotalinsurance,2); ?></td>
                               <td>บาท</td>
                            <tr>

                            <tr>
                              <td colspan="7" align="right"><b>รวมคืนประกัน</b></td>
                              <td></td>
                              <td style="color: red;"><?php echo number_format($sumtotalinsurancepay,2); ?></td>
                              <td>บาท</td>
                            <tr>

                            <tr>
                               <td colspan="7" align="right"><b>ยอดคงเหลือ</b></td>
                               <td></td>
                               <td><u><?php echo number_format($sumall1,2); ?></u></td>
                               <input type="hidden" name="sumall1[]" value="<?php echo $sumall1 ?>" class="form-control">
                               <td>บาท</td>
                            <tr>

                          </tbody>
                        </table>

                    </div>
                  </div >

                  <div class="row">
                      <br>
                  </div>

                  <div class="row">
                      <div class="col-md-3">

                      </div>
                      <div class="col-md-2">

                      </div>
                      <div class="col-md-2">
                          <input type="submit" name="transfer" class="btn btn-info" value="นำฝาก">
                      </div>
                      <div class="col-md-4" >

                      </div>
                      <div class="col-md-1" >

                      </div>
                  </div>

               </form>

               <?php } ?>

            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')
