<?php
use  App\Api\Connectdb;

use  App\Api\Vendorcenter;

?>
@include('headmenu')

<link>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}
<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>
<script type="text/javascript" src = 'js/config/configcash.js'></script>
<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">

<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-cog home-icon"></i>
                        <a href="#">ข้อมูลเงินสด</a>
                    </li>
                    <li class="active">จัดการข้อมูลเงินสด</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">
              <!-- <form action="searchgetcashedit" method="post"> -->
              <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <?php
                  $brcode = Session::get('brcode');

                  $db = Connectdb::Databaseall();

                  $sql = "SELECT $db[fsctaccount].cash.*
                          FROM $db[fsctaccount].cash
                          WHERE $db[fsctaccount].cash.branch_id = '$brcode'
                         ";
                  // $sql = 'SELECT * FROM '.$db['fsctaccount'].'.cash';

                  $data = DB::connection('mysql')->select($sql);
                  // echo "<pre>";
                  // print_r($data);
                  // exit;
                  foreach ($data as $key => $value) {

                ?>

                <div class="row">
                    <div class="col-md-3">

                    </div>
                    <div class="col-md-2">
                      <center>เงินสดปัจจุบัน</center>
                    </div>
                    <div class="col-md-2">
                      <input type="text" name="billedit" id="billedit" class="form-control" value="<?php echo $value->grandtotal;?>"  required>
                    </div>
                    <div class="col-md-1">
                      บาท
                    </div>
                    <div class="col-md-1">
                      <a href="#" title="ดูรายละเอียด" onclick="viewdata({{ $value->id }})"><img src="images/global/search.png"></a>
                      <a href="#" title="แก้ไขข้อมูล" data-toggle="modal" data-target="#myModal" onclick="getdata({{ $value->id }})"><img src="images/global/edit-icon.png"></a>
                    </div>
                    <div class="col-md-3">

                    </div>
                </div>

                  <?php } ?>

                <div class="row">
                    <br>
                </div>

                <div class="row">
                    <br>
                </div>

                <div class="row">
                    <div class="col-md-3">

                    </div>
                    <div class="col-md-2">

                    </div>
                    <div class="col-md-2">
                      <!-- <input type="submit" class="btn btn-success" value="บันทึก">
                      <input type="reset" class="btn btn-danger" value="ยกเลิก"> -->
                    </div>
                    <div class="col-md-4" >

                    </div>
                </div>

                <div class="row">
                    <br>
                </div>



        </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')


<!-- Modal -->

<!-- Modal -->


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">ตั้งค่าข้อมูลเงินสด</h5>
            </div>

            <div class="modal-body">
                <!-- The form is placed inside the body of modal -->
                <form id="configcash" onsubmit="return getdatesubmit();" data-toggle="validator" method="post" class="form-horizontal">
                    <input value="{{ null }}" type="hidden" id="id" name="id" />

                    <div class="form-group">
                        <label class="col-xs-3 control-label">สาขา</label>
                        <div class="col-xs-5">
                            <input type="text" id="branch_id" class="form-control" name="branch_id" readonly required/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-3 control-label">เงินสด<i><span style="color: red">*</span></i></label>
                        <div class="col-xs-5">
                            <input type="text" id="grandtotal" class="form-control" name="grandtotal" required/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-xs-3 control-label">หมายเหตุ<i><span style="color: red">*</span></i></label>
                        <div class="col-xs-5">
                            <input type="text" id="note" class="form-control" name="note" required/>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-xs-5 col-xs-offset-3">
                            <button type="submit" id="Btn_save" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
