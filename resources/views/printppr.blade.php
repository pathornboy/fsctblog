<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;

?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }

        body {
            font-family: "THSarabunNew";
        }
        h4 {
            font-family: "THSarabunNew";
        }
        h4 {
            font-family: "THSarabunNew";
        }
    </style>
</head>
<body>
    <?php
        $db = Connectdb::Databaseall();
        $sql = "SELECT *,date(week) as date1,date(week + INTERVAL 7 DAY) as date2
		FROM $db[fsctaccount].ppr_head  WHERE id ='$id' ";
        $datahead = DB::connection('mysql')->select($sql);

		$branchid=$datahead[0]->branch_id;
		$sql = "SELECT * FROM $db[hr_base].branch  WHERE code_branch ='$branchid' ";
        $databranch = DB::connection('mysql')->select($sql);

		$typepayid=$datahead[0]->type_pay;
		$sql = "SELECT * FROM $db[fsctaccount].type_pay  WHERE id ='$typepayid' ";
        $datatypepay = DB::connection('mysql')->select($sql);
        //dd($datatypepay);

		$typebuyid=$datahead[0]->type_buy;
		$sql = "SELECT * FROM $db[fsctaccount].type_buy  WHERE id ='$typebuyid' ";
        $datatypebuy = DB::connection('mysql')->select($sql);
        //dd($datatypebuy);

    ?>

    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="10%">
                @if($datahead[0]->id_company==1)
                    <img src="images/company/1.png" width="275px" >
                @elseif($datahead[0]->id_company==2)
                    <img src="images/company/2.png" width="275px" >
                @endif
            </td>
            <td width="90%" valign="top" style="padding-top: -30px">
                <table width="100%">
                    <tr>
                        <td>
                            <?php $datacompany = Maincenter::datacompany($datahead[0]->id_company);?>
                            <h3><?php print_r($datacompany[0]->name_eng)?></h3>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: -55px">
                            <?php $datacompany = Maincenter::datacompany($datahead[0]->id_company);?>
                          <h4><?php print_r($datacompany[0]->name." สาขา"); print_r($databranch[0]->name_branch);?></h4>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: -35px">
                            <?php $datacompany = Maincenter::datacompany($datahead[0]->id_company);?>
                            <?php print_r($databranch[0]->address)?>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: -15px">
                           โทรศัพท์  <?php print_r($datacompany[0]->Tel)?>
                           เลขประจำตัวผู้เสียภาษี <?php print_r($datacompany[0]->business_number)?>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: -15px">
                            โทรสาร  <?php print_r($datacompany[0]->Fax)?>
                        </td>
                    </tr>
                </table>


            </td>
			<td>
			เลขที่ {{$datahead[0]->number_ppr}}
			 <? if($datahead[0]->urgent_status != 2) {echo"<br>เร่งด่วน";} ?>
			</td>
        </tr>
    </table>

    <center><h3>Purchase Requisition/ใบขออนุมัติซื้อ</h3></center>

		<center><h4>งวดวันที่ {{Maincenter::yearCorverttoBE($datahead[0]->date1)}} ถึง {{Maincenter::yearCorverttoBE($datahead[0]->date2)}} ขอวันที่ {{Maincenter::yearCorverttoBE($datahead[0]->date)}}
		<br>ประเภทการซื้อ {{$datatypebuy[0]->name_buy}} วิธีการจ่าย {{$datatypepay[0]->name_pay}}
		</h4>

		</center>
    <?php
    // $datadetail = Vendorcenter::getdatadetailpr($datahead[0]->id);
    // $i= 0;

	// print_r($datadetail);
    ?>

	<table width="100%" align="center" cellpadding="0" cellspacing="0" border="1">

	<tr>
            <td  align="center" bgcolor="#adbce6">#</td>
			<td  align="center"  bgcolor="#adbce6">Supplier/ผุ้ขาย</td>
            <td  align="center"  bgcolor="#adbce6">List/รายการ</td>
            <td  align="center"  bgcolor="#adbce6">Quantity/จำนวน</td>
            <td  align="center"  bgcolor="#adbce6">Unit/หน่วย</td>
            <td  align="center"  bgcolor="#adbce6">Unit Price/ราคาต่อหน่วย</td>
            <td  align="center"  bgcolor="#adbce6">vat/ภาษี (บาท)</td>
            <td  align="center"  bgcolor="#adbce6">Withholding / หัก ณ ที่จ่าย </td>
            <td  align="center"  bgcolor="#adbce6">Total  / รวม </td>
    </tr>

		<?php
      // exit('10');
			$datadetail = Vendorcenter::getdatadetailpr($datahead[0]->id);
      //dd($datadetail);
			$i=1;
			$total=0;
		?>

	@foreach($datadetail as $row)
		<tr>
	<?
	 if($i%2==0)
			{
		 $bgcolor="#adbce6";

				}
			else
				{

			$bgcolor="#bdc9eb";

				}

	?>
	        <td  align="center" bgcolor="{{$bgcolor}}">{{$i}}</td>

			<?
					$supplierid=$row->id_supplier;
		$sql = "SELECT * FROM $db[fsctaccount].supplier  WHERE id ='$supplierid' ";
        $datasupplier = DB::connection('mysql')->select($sql);

			?>
			$total=0;

			<td  align="center"  bgcolor="{{$bgcolor}}">{{$datasupplier[0]->name_supplier}}</td>
            <td  align="center"  bgcolor="{{$bgcolor}}">{{$row->list}}</td>
            <td  align="center"  bgcolor="{{$bgcolor}}">{{$row->amount}}</td>
            <td  align="center"  bgcolor="{{$bgcolor}}">{{$row->type_amount}}</td>
            <td  align="center"  bgcolor="{{$bgcolor}}">{{$row->price}}</td>
            <td  align="center"  bgcolor="{{$bgcolor}}">{{$row->amount*$row->price*(($row->vat)/100)}}</td>
            <td  align="center"  bgcolor="{{$bgcolor}}">{{$row->amount*$row->price*(($row->withhold)/100)}}</td>
            <td  align="center"  bgcolor="{{$bgcolor}}">{{$row->total}} </td>









	</tr>
	<?$i++;
	$total+=$row->total;?>
	@endforeach
	<tr>
	<td align="center"  colspan="7"></td>
	<td align="center" >รวมสุทธิ</td>
	<td align="center" >{{$total}}</td>
	</tr>



	</table>


	 <?
		$codeemp=$datahead[0]->code_emp;
	 	$sql = "SELECT * FROM $db[hr_base].emp_data  WHERE code_emp_old ='$codeemp' ";
        $dataemp = DB::connection('mysql')->select($sql);


	 ?>

	   <br>
  <br>
  <br>
    <br>
  <br>
    <br>
  <br>
  <br>

  <table>


  <table align="right">

    <tr>
  <td align="center">ลงชื่อ</td>
  </tr>

  <tr>
  <td align="center">_______________________</td>
  </tr>
  <tr>
  <td align="center">{{$dataemp[0]->prefixth}} {{$dataemp[0]->nameth}} {{$dataemp[0]->surnameth}}</td>
  </tr>
  <tr>
  <td align="center">พนักงานผุ้ขอ</td>
  </tr>
  </table>










</body>
</html>
