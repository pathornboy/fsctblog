<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;
$emp_code = Session::get('emp_code');

?>
@include('headmenu')
<link>
<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}



<link rel="stylesheet" type="text/css" href="css/ui/jquery-ui.css">
{{--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>


<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">


<link rel="stylesheet" type="text/css" href="bower_components/select2/dist/css/select2.min.css">
<script type="text/javascript" src = 'bower_components/select2/dist/js/select2.full.min.js'></script>

<script type="text/javascript" src = 'js/vendor/vendoraddbillcredit.js'></script>


<style>
    .ui-autocomplete-input {
        border: none;
        font-size: 14px;
        width: 150px;
        height: 24px;
        margin-bottom: 5px;
        padding-top: 2px;
        border: 1px solid #DDD !important;
        padding-top: 0px !important;
        z-index: 1511;
        position: relative;
    }
    .ui-menu .ui-menu-item a {
        font-size: 12px;
    }
    .ui-autocomplete {
        position: absolute;
        top: 0;
        left: 0;
        z-index: 1510 !important;
        float: left;
        display: none;
        min-width: 160px;
        width: 160px;
        padding: 4px 0;
        margin: 2px 0 0 0;
        list-style: none;
        background-color: #ffffff;
        border-color: #ccc;
        border-color: rgba(0, 0, 0, 0.2);
        border-style: solid;
        border-width: 1px;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding;
        background-clip: padding-box;
        *border-right-width: 2px;
        *border-bottom-width: 2px;
    }
    .ui-menu-item > a.ui-corner-all {
        display: block;
        padding: 3px 15px;
        clear: both;
        font-weight: normal;
        line-height: 18px;
        color: #555555;
        white-space: nowrap;
        text-decoration: none;
    }
    .ui-state-hover, .ui-state-active {
        color: #ffffff;
        text-decoration: none;
        background-color: #0088cc;
        border-radius: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        background-image: none;
    }

</style>



<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">งานจัดซื้อ</a>
                    </li>
                    <li class="active">ข้อมูลการจัดซื้อ (เงินเชื่อ)</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">
                <input type="hidden" id="emp_code" name="emp_code" value="<?php echo $emp_code; ?>">
                <input type="hidden" id="number_pprset" value="<?php $brcode = Session::get('brcode');

                $db = Connectdb::Databaseall();
                $sql = "SELECT COUNT(id) as idgen FROM $db[fsctaccount].ppr_head WHERE branch_id = '$brcode'";
                $bbr_no_branch = DB::connection('mysql')->select($sql);
                $numberrun = 0;
                //                                        print_r($bbr_no_branch);
                $onset =  $bbr_no_branch[0]->idgen;

                if($onset == 0){
                    $numberrun = 1;
                }else{
                    $numberrun = (int)($onset)+1;
                }

                echo 'PR'.substr(date('Y',strtotime("+543 year")),2,2).date('m').str_pad($numberrun, 3, "0", STR_PAD_LEFT);
                $yearth = date('Y')+543;
                ?>">
                <input type="hidden" id="level_emp" name="level_emp" value="<?php  echo $lavelemp = Session::get('level_emp');?>">
                <input type="hidden" id="dateset" value="<?php echo date('Y-m-d')?>">
                <input type="hidden"  class="form-control"  id="yearset" value="<?php echo date('Y')?>"  />
                <input type="hidden"  class="form-control" id="year_thset" value="<?php echo $yearth?>"  />
                <input type="hidden"  class="form-control"  id="bbr_no_branchset" value="<?php
                echo $numberrun;
                ?>"  />
                <input type="hidden"  class="form-control" id="dateshowset" value="<?php echo date('d-m').'-'.$yearth;?>"  disabled/>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        ข้อมูลการจัดซื้อ (เงินเชื่อ)
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body table-responsive">
                                <div class="row">
                                    <div class="col-md-10">
                                        {{--<label >Tags: </label>--}}
                                        {{--<input id="tags" class="form-control">--}}
                                    </div>

                                    <div class="col-md-2">
                                        <a href="#" title="เพิ่มข้อมูล" data-toggle="modal" data-target="#myModal" onclick="insertnew()" ><img src="images/global/add.png">เพิ่มข้อมูล</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <br>
                                </div>
                                <?php
                                $db = Connectdb::Databaseall();
                                $brcode = Session::get('brcode');
                                $levelemp = Session::get('level_emp');
                                      $sqlqthead = 'SELECT ppr_head.date, week, id_company, number_ppr, branch_id, urgent_status, ppr_head.status, id FROM '.$db['fsctaccount'].'.ppr_head ';
                                if($levelemp == 1 OR $levelemp == 2 OR $levelemp == 3  OR $emp_code== 1606 OR $emp_code == 1427 OR $emp_code == 1471 OR $emp_code == 2027 OR $emp_code == 1475 OR $emp_code == 1383  OR $emp_code== 1611  OR $emp_code== 1612){
                                      $sqlqthead .= 'WHERE type_ppr = "1"';
                                  }else{
                                      $sqlqthead .= 'WHERE branch_id = "'.$brcode.'" AND type_ppr = "1"';
                                  }

                                $sqlqthead .= ' ORDER BY id DESC';
                                //echo "$sqlqthead";
                                $data = DB::connection('mysql')->select($sqlqthead);
                                $i = 1;
                                ?>
                                        <table id="example" class="table table-striped table-bordered">
                                            <thead class="thead-inverse">
                                            <tr>
                                                <td>#</td>
                                                <td>วันที่</td>
                                                <td>รอบที่ขอ</td>
                                                <td>บริษัท</td>
                                                <td>PR No.</td>
                                                <td>สาขา</td>
                                                <td>เร่งด่วน</td>
                                                <td>เงินที่ขออนุมัติ</td>
                                                <td>เงินที่โอนไป</td>
                                                <td>แก้ไข/เพิ่มเติม</td>
                                                <td>สถานะ</td>
                                                <td>ยกเลิก</td>
                                                <td>พิมพ์ใบ PR</td>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach ($data as $value)
                                                <tr>
                                                    <td scope="row">{{ $i }}</td>
                                                    <td align="left">{{ $value->date}}</td>
                                                    <td align="left">{{ $value->week}}</td>
                                                    <td align="left">
                                                        <?php $dataworking = Maincenter::datacompany($value->id_company);
                                                        print_r($dataworking[0]->name);

                                                        ?>
                                                    </td>
                                                    <td align="center">{{ $value->number_ppr }}</td>
                                                    <td align="center">{{ $value->branch_id }}</td>
                                                    <td align="center"><?php
                                                      if($value->urgent_status==1){
                                                          echo "<font color='#008ae6'>เร่งด่วน</font>";
                                                      }else if ($value->urgent_status==2) {
                                                          echo "<font color='#009933'>ปกติ</font>";
                                                      }else if ($value->urgent_status==3) {
                                                          echo "<font color='#ff3300'>เร่งด่วน(บอสอนุมัติแล้ว)</font>";
                                                      }
                                                     ?>
                                                    </td>
                                                    <td align="center"></td>
                                                    <td align="center"></td>
                                                    <td align="center">
                                                        @if($value->status=="0")
                                                        <a href="#" title="แก้ไขข้อมูล" data-toggle="modal" data-target="#myModal" onclick="getdata(1,{{ $value->id }})"><img src="images/global/edit-icon.png"></a>
                                                        @endif
                                                    </td>
                                                    <td align="center">
                                                        <?php $datastatus = Vendorcenter::getstatusvendor($value->status);
                                                                if($datastatus){
                                                                    $positionset = (explode(",",$datastatus[0]->position_id));

                                                                     $id_position = Session::get('id_position');
                                                                     $levelemp = Session::get('level_emp');
                                                                    if (in_array($id_position, $positionset) OR $levelemp <=2 ){

                                                                        echo  $statusresute = "<a href='#' data-toggle='modal' data-target='#myModal' onclick='getdata(3,$value->id)' ><font color='".$datastatus[0]->color."'>".$datastatus[0]->notstatus."</font></a>";
                                                                    }else{
                                                                        if($levelemp <=2){
                                                                            echo  $statusresute = "<a href='#' data-toggle='modal' data-target='#myModal' onclick='getdata(3,$value->id)' ><font color='".$datastatus[0]->color."'>".$datastatus[0]->notstatus."</font></a>";
                                                                        }else{
                                                                            echo  $statusresute = "<font color='".$datastatus[0]->color."'>".$datastatus[0]->notstatus."</font>";
                                                                        }

                                                                    }

                                                                }
                                                        ?>
                                                    </td>
                                                    <td align="center">
                                                        @if($value->status=="0")
                                                         <a href="#" title="ยกเลิกใบ PR" data-toggle="modal" data-target="#myModal" onclick="cancel(1,{{ $value->id }})"><img src="images/global/delete-icon.png"></a>
                                                        @endif
                                                    </td>
                                                    <td align="center">
                                                        <?php
                                                                $ckpo = Vendorcenter::ckstatus($value->status);
                                                                if($ckpo[0]->po==1){
                                                                    echo "<a href='printpr/$value->id' target='_blank'> <img src='images/global/pr.png' ></a>";
                                                                }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php $i++; ?>
                                            @endforeach
                                            </tbody>
                                        </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')


<!-- Modal -->

<div class="modal fade" id="myModal"  role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog" style="width:90%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">ข้อมูลใบ PR</h5>
            </div>

            <div class="modal-body">

                <form id="configFormvendors" onsubmit="return getdatesubmit();" data-toggle="validator" method="post" class="form-horizontal">
                    <input value="{{ null }}" type="hidden" id="id" name="id" />
                    {{--<input type="hidden" name="status" id="status" value="0">--}}
                    <input type="hidden" id="statusprocess" value="0">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-right">รหัสสาขา <?php echo $brcode = Session::get('brcode');?>
                                <input type="hidden"  class="form-control" name="branch_id" id="branch_id" value="<?php echo $brcode ;?>"  />
                                (
                                <?php
                               $databr = Maincenter::databranchbycode($brcode);
                               print_r($databr[0]->name_branch);
                                ?>)
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-3 control-label">วันที่ขอ<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-5">
                                    <input type="text"  class="form-control" id="dateshow" value="<?php echo date('d-m').'-'.$yearth;?>"  disabled/>
                                    <input type="hidden"  class="form-control" name="date" id="date" value="<?php echo date('Y-m-d')?>"  />
                                    <input type="hidden"  class="form-control" name="year" id="year" value="<?php echo date('Y')?>"  />
                                    <input type="hidden"  class="form-control" name="year_th" id="year_th" value="<?php echo $yearth;?>"  />
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">เลือกบริษัท<i><span style="color: red">*</span></i></label>
                                    <div class="col-xs-8">
                                        <select name="id_company" id="id_company" class="form-control" onchange="selectnopr(this)">
                                            <?php
                                           $db = Connectdb::Databaseall();
                                           $data = DB::select('SELECT * FROM '.$db['hr_base'].'.working_company WHERE status ="1"');
                                            ?>
                                           @foreach ($data as $value)
                                                <option value="{{$value->id}}">{{$value->name}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                    <!-- <div class="col-xs-4">

                                    </div> -->
                                </div>
                        </div>

                        <div class="col-md-3">
                            <div class="pull-right">
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">PR No.<i><span style="color: red">*</span></i></label>
                                    <div class="col-xs-8">
                                        <input type="text"  class="form-control" name="number_ppr" id="number_ppr" value=""  readonly/>

                                        <input type="hidden"  class="form-control" name="bbr_no_branch" id="bbr_no_branch" value=""  readonly/>

                                    </div>
                                    <div class="col-xs-4">

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">
                                   รอบการขอ
                                </label>
                                <div class="col-xs-6">
                                  <?php
                                   $time = date('Y-m-d H:i:s');
                                     $datasetcount = Vendorcenter::configweekpr();
                                     $weekcut = ($datasetcount[0]->date_week);
                                     $timecut = ($datasetcount[0]->time_cut);
                                      $countset =  date("Y-m-d H:i:s",strtotime("$weekcut $timecut"));
                                      $time;
                                    if($time > $countset){
                                      //echo "สัปดาห์หน้า";
                                       $countset =  date("Y-m-d H:i:s",strtotime("$weekcut next week $timecut"));

                                    }else if($time <= $countset){
                                      //echo "สัปดาห์นี้";
                                       $countset;
                                    }
                                    echo $countset;

                                    ?>
                                      <input type="hidden" id="week" name="week" value="{{$countset}}">
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">ประเภทการจ่าย<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <select class="form-control" id="type_pay" name="type_pay" required>
                                        <option value="">เลือก ประเภทการจ่าย</option>
                                        <?php
                                        $db = Connectdb::Databaseall();
                                        $data = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.type_pay WHERE status ="1"');
                                        ?>
                                        @foreach ($data as $value)
                                            <option value="{{$value->id}}">{{$value->name_pay}}</option>

                                        @endforeach
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">ประเภทการซื้อ<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <select class="form-control" id="type_buy" name="type_buy" required>
                                        <option value="">เลือก ประเภทการซื้อ</option>
                                        <?php
                                        $db = Connectdb::Databaseall();
                                        $data = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.type_buy  WHERE status ="1"');
                                        ?>
                                        @foreach ($data as $value)
                                            <option value="{{$value->id}}">{{$value->name_buy}}</option>
                                        @endforeach


                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">การจัดซื้อ(ใน/นอก ประเทศ)<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input  type="radio" name="in_house" id="in_house1" value="1" checked>  ภายในประเทศ
                                    <br>
                                    <input  type="radio" name="in_house" id="in_house2" value="2" >  ต่างประเทศ
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">งบประมาณ<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input  type="radio" name="in_budget" id="in_budget1" value="1" checked>  ภายในงบประมาณ
                                    <br>
                                    <input  type="radio" name="in_budget" id="in_budget2" value="2" >  นอกงบประมาณ
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">การจัดซื้อ(เร่งด่วน/ปกติ)<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input  type="radio" name="urgent_status" id="urgent_status2" value="2" checked>  ปกติ
                                    <br>
                                    <input  type="radio" name="urgent_status" id="urgent_status1" value="1" >  เร่งด่วน
                                    <br>
                                    <input  type="radio" name="urgent_status" id="urgent_status3" value="3" checked><font color="red">เร่งด่วน (บอสอนุมัติแล้ว)</font>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-right">
                                <a href="#" title="เพิ่มรายการ" id="addrow" ><img src="images/global/add.png">เพิ่มรายการ</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>

                    <div class="row">
                           <div class="col-md-12 table-responsive">
                            <table class="table">
                              <tr>
                                <td>MD/BOSS อนุมัติ<td>
                                <td><td>
                                <td><td>
                                <td><td>
                                <td><td>
                                <td><td>
                                <td><td>
                                <td><td>
                                <td><td>
                                <td><td>
                                <td><td>
                                <td><td>
                                <td><td>
                                <td><td>
                              </tr>
                            </table>
                            <table class="table table-bordered" id="thdetail">
                                <thead>
                                <tr>
                                      <td align="center">
                                        <input type="checkbox" title="general" id="checkapprovedall" name="checkapprovedall" value='0'
                                        <?php  $lavelemp = Session::get('level_emp');

                                            // if($lavelemp == 1 OR $lavelemp == 2){
                                            //   echo '';
                                            // }else{
                                            //   echo "disabled";
                                            // }
                                        ?>
                                          >
                                      </td>

                                      <td align="center">
                                        <input type="checkbox" title="boss / md" id="checkapprovedallmd" name="checkapprovedallmd" value='0'
                                        <?php  $lavelemp = Session::get('level_emp');
                                            if($lavelemp == 1 OR $lavelemp == 2 OR   $emp_code == 1383 OR $emp_code==1427 OR $emp_code==1471  OR $emp_code== 1606){
                                              echo '';
                                            }else{
                                              echo "disabled";
                                            }
                                        ?>
                                          >
                                      </td>

                                    <td align="center">เลือก Supplier </td>
                                    <td align="center">เลือกประเภทการจัดซื้อ </td>
                                    <td align="center">ชื่อรายการ</td>
                                    <td align="center" colspan="2">จำนวนนับ<br />จำนวณ | หน่วยนับ</td>
                                    <td align="center">ราคาต่อหน่วย</td>
                                    <td align="center">vat</td>
                                    <td align="center">หัก ณ ที่จ่าย</td>
                                    <td align="center">รวม</td>
                                    <td align="center">ซื้อล่าสุด (วันที่)</td>
                                    <td align="center">ค่าเฉลี่ย</td>
                                    <td align="center">หมายเหตุ(คงเหลือปัจจุบัน)</td>
                                    <td align="center">ลบ</td>
                                </tr>
                                </thead>
                                <tbody id="tdbody">
                                 <tr>
                                     <td>
                                       <input type="checkbox" title="general" id="checkapproved0" class="checkapproved" name="checkapproved[]" value="0"
                                       <?php  $lavelemp = Session::get('level_emp');
                                           // if($lavelemp == 1 OR $lavelemp == 2){
                                           //   echo '';
                                           // }else{
                                           //   echo "disabled";
                                           // }
                                       ?>
                                       >
                                     </td>
                                     <td>
                                       <input type="checkbox" title="boss/md" id="checkapprovedmd0" class="checkapprovedmd" name="checkapprovedmd[]" value="0"
                                       <?php  $lavelemp = Session::get('level_emp');
                                           if($lavelemp == 1 OR $lavelemp == 2 OR  $emp_code == 1383 || $emp_code==1427 || $emp_code==1471  OR $emp_code== 1606){
                                             echo '';
                                           }else{
                                             echo "disabled";
                                           }
                                       ?>
                                       >
                                     </td>
                                     <td class="text-left">
                                       <?php
                                       $db = Connectdb::Databaseall();
                                        $datasuppall= DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.supplier WHERE status = "1" ');
                                        // print_r($datasuppall);
                                        // exit;
                                       ?>
                                         <select name="id_supplier[]" id="id_supplier0" class="form-control select2 full-form" required>

                                                  <option  value="0" selected>ไม่ได้เลือกผู้ขาย</option>
                                             @foreach ($datasuppall as $suppall)
                                                 <option value="{{$suppall->id}}">{{$suppall->pre}}  {{$suppall->name_supplier}}</option>
                                             @endforeach
                                         </select>

                                     </td>
                                     <td align="center">
                                         <select name="config_group_supp_id[]" id="config_group_supp_id0" class="form-control full-form" onchange="selectaccnumber(this,0)" required>
                                             <option value="">เลือกประเภทการจัดซื้อ</option>
                                             <?php
                                             $db = Connectdb::Databaseall();
                                             $data = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.config_group_supp WHERE status = "1" ');
                                             ?>
                                             @foreach ($data as $value)
                                                 <option value="{{$value->id}}">{{$value->name}}</option>
                                             @endforeach
                                         </select>
                                     </td>
                                     <td align="center">
                                         <input type="hidden" name="material_id[]" id="materialid0" value="0">
                                         <input type="text" name="list[]" id="list0"  onfocus="autocompletedata(0)" style="width: 50rem;" class="form-control" disabled>
                                     </td>
                                     <td align="center">
                                         <input type="text" name="amount[]" id="amount0" onblur="getamount(this,0)" class="form-control full-form" style="" placeholder="จำนวน">
                                     </td>
                                     <td align="center">
                                         <input type="text" name="type_amount[]" id="type_amount0"  class="form-control full-form" style="width: 60px;" placeholder="หน่วย">
                                     </td>
                                     <td align="center">
                                         <input type="text" name="price[]" id="price0" onblur="getprice(this,0)"  disabled  class="form-control full-form" style="width: 90px;">
                                     </td>
                                     <td align="center">
                                         <select name="vat[]" id="vat0" class="form-control full-form" onchange="calculatevat(this,0)" style="width: 90px;" >
                                             <option value="0">ไม่มี vat</option>
                                             <?php
                                             $db = Connectdb::Databaseall();
                                             $data = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.tax_config WHERE status = "1"');
                                             ?>
                                             @foreach ($data as $value)
                                                 <option value="{{$value->tax}}">{{$value->tax}}</option>
                                             @endforeach
                                         </select>
                                     </td>
                                     <td align="center">
                                         <select name="withhold[]" id="withhold0" class="form-control full-form" onchange="calculatewithhold(this,0)" style="width: 90px;" >
                                             <option value="0">ไม่มีหัก</option>
                                             <?php
                                             $db = Connectdb::Databaseall();
                                             $datawithhold = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.withhold WHERE status = "1"');
                                             ?>
                                             @foreach ($datawithhold as $value)
                                                 <option value="{{$value->withhold}}">{{$value->Note}}</option>
                                             @endforeach
                                         </select>
                                     </td>
                                     <td align="center">
                                         <input type="text" name="total[]"  id="total0" class="form-control full-form"  style="width: 100px;" >
                                     </td>
                                     <td align="center">
                                         <input type="text" name="daterecentpurchases[]" readonly id="daterecentpurchases0"  class="form-control datepicker full-form"  >
                                     </td>
                                     <td align="center">
                                         <input type="text" name="avg[]" id="avg0"  class="form-control full-form"  >
                                     </td>
                                     <td align="center">
                                         <input type="text" name="note[]" id="note0"  class="form-control full-form"  >
                                     </td>
                                     <td align="center"></td>
                                 </tr>

                                </tbody>
                            </table>
                       </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <font color="red" style="font-size: 25px;"><u><b>หมายเหตุ :</b>กรณีไม่ทราบราคาสินค้ากรุณาให้กรอกเป็น 0 ในช่องราคาต่อหน่วย</u></font>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-right">
                            รหัสพนักงาน:
                                <input type="text" class="form-control" name="code_emp" id="code_emp" value="<?php echo $brcode = Session::get('emp_code')?>" readonly>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-right">
                                <input type="text" class="form-control" name="code_sup" id="code_sup" placeholder="รหัสซุปประจำสาขา">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row"  id="updatestatus" >
                        <div class="col-md-4">

                        </div>
                        <div class="col-md-4">
                            สถานะปัจจุบัน
                            <?php
                            $db = Connectdb::Databaseall();
                            $datastatus = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.accountstatusforprogram');
                            ?>
                            <select name="status" id="status" class="form-control">

                                @foreach ($datastatus as $c)
                                    @if($c->numberstatus==0)
                                        <option value="{{$c->numberstatus}}" selected="">{{$c->notstatus}}</option>
                                    @endif
                                    <?php
                                    $id_position = Session::get('id_position');
                                    $levelemp = Session::get('level_emp');
                                    $positionset = explode(",",$c->position_id);
                                    ?>

                                        @if (in_array($id_position, $positionset) AND $c->numberstatus!=0)
                                            <option value="{{ $c->numberstatus }}">{{ $c->notstatus }}</option>
                                        @endif
                                    @if($c->numberstatus==99)
                                        <option value="{{$c->numberstatus}}" >{{$c->notstatus}}</option>
                                    @endif
                                @endforeach
                            </select>


                        </div>
                        <div class="col-md-4">

                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-xs-5 col-xs-offset-3">
                                    <button type="submit" id="Btn_save" class="btn btn-primary">Save</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>



<!---   myModal approved--->
