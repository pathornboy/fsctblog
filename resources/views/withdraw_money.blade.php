<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;


?>
@include('headmenu')
<link>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>


<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">


<link rel="stylesheet" type="text/css" href="bower_components/select2/dist/css/select2.min.css">
<script type="text/javascript" src = 'bower_components/select2/dist/js/select2.full.min.js'></script>

<script type="text/javascript" src = 'js/vendor/addpo.js'></script>


<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="breadcrumbs" id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="ace-icon fa fa-home home-icon"></i>
                            <a href="#">งานจัดซื้อ</a>
                        </li>
                        <li class="active">แจ้งการถอนเงิน</li>
                    </ul>
                    <ul class="breadcrumb">
                        <li>
                            แจ้งการถอนเงิน
                        </li>
                    </ul>
                            @if (session('alert'))
                                <div class="alert alert-success">
                                    {{ session('alert') }}
                                </div>
                            @endif

                    <div class="box-body">
                                <div class="row">
                                    <div class="col-md-10">

                                    </div>

                                    <div class="col-md-2">
                                        <a href="#" title="เพิ่มข้อมูล" data-toggle="modal" data-target="#myModal"><img src="images/global/add.png">เพิ่มข้อมูล</a>
                                    </div>
                    </div>
                            <?php

                                $db = Connectdb::Databaseall();

                                $brcode = Session::get('brcode');
                                $sql = "SELECT $db[fsctaccount].atmcash.*
                                            FROM $db[fsctaccount].atmcash
                                            WHERE $db[fsctaccount].atmcash.branch = '$brcode'";

                               $dataquery = DB::connection('mysql')->select($sql);


                            ?>
                            <table id="example" class="table table-striped table-bordered">

                                <tr>
                                    <td>#</td>
                                    <td>จำนวนเงิน</td>
                                    <td>วันที่</td>
                                    <td>ผู้กด ATM</td>
                                </tr>
                                <?php $i=1; foreach ($dataquery as $key => $value) {?>
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $value->cash}}</td>
                                    <td>{{ $value->code_emp}}</td>
                                    <td>{{ $value->datetime}}</td>
                                </tr>
                              <?php $i++;} ?>
                            

                             <tbody>

                            </tbody>


                            </table>
                    </div>
        </div>


    </section>
    <!-- /.content -->
</div>
@include('footer')


<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">เพิ่มข้อมูลการแจ้งการถอนเงิน</h4>
        </div>
        <div class="modal-body">
        <form action="saveatmcash" method="post" >
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

            <div class="input_fields_wrap">
                <div class="form-group">
                    <label><b>ถอนเงินจำนวน</b></label>
                    <input type="text" placeholder="กรุณากรอกจำนวนเงิน" name="atmmoney" class="form-control" required>
                </div>
            </div>


        </div>
        <div class="modal-footer">
            <button type="submit"  class="btn btn-success">ยืนยัน</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </form>
        </div>
      </div>

    </div>
  </div>



  <script>
 $(document).ready(function() {
	var max_fields      = 10; //maximum input boxes allowed
	var wrapper   		= $(".input_fields_wrap"); //Fields wrapper

	var x = 1; //initlal text box count
	$("#add-more").click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			$(wrapper).append('<div class="form-group"> <label><b>เลขที่ PO ['+x+'] </b></label> <input type="text" placeholder="กรุณากรอกเลขที่ใบ PO" name="" class="form-control" required>   <a href="#" class="remove_field">Remove</a></div>'); //add input box
		}
	});

	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); x--;
	})
});
  </script>
