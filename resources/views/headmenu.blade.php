<?php
     $emp_code = Session::get('emp_code');
     $fullname = Session::get('fullname');
     $position = Session::get('position');
     $brcode   = Session::get('brcode');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FSCT Account</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <!-- Morris chart -->
    {{--<link rel="stylesheet" href="bower_components/morris.js">--}}
    <!-- jvectormap -->
    {{--<link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">--}}
    {{--<!-- Date Picker -->--}}
    {{--<link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">--}}
    {{--<!-- Daterange picker -->--}}
    <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
    {{--<!-- bootstrap wysihtml5 - text editor -->--}}
    {{--<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">--}}



    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

    <header class="main-header">



        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->


            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <li  class="dropdown ">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">บัญชี <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('taxwithholdsupplier') }}">หนังสือรับรองการหักภาษี ณ ที่จ่าย</a></li>
                            <li><a href="{{ url('cashrent') }}">เงินสดย่อย</a></li>
                            <li><a href="#"></a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Company<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo url("/");?>">Home Page</a></li>
                            <li><a href="#">Company Snapshot</a></li>
                            <li><a href="#">Calendar</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Documents Center</a></li>
                            <li><a href="#">Clean Up Attachment Links</a></li>
                            <li><a href="#">Repair Attached Document Links</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Lead Center</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo url("/companyinformation");?>">Company Information</a></li>
                            <li><a href="#">Advanced Service Administration</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Set Up Budgets</a></li>
                            <li><a href="#">Cash Flow Projector</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Chart of Accounts</a></li>
                            <li><a href="#">Make General Journal Entries</a></li>
                            <li><a href="#">Lean About Multicurrency</a></li>
                            <li><a href="#">Multicurrency Resource Center</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Enter Vehicle Mileage</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">งานจัดซื้อ<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo url("/vendorcenter");?>">Supplier Center</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo url("/vendoraddbill");?>">เพิ่มใบ PR</a></li>
                            <li><a href="<?php echo url("/addpo");?>">เพิ่มใบ PO</a></li>
                            <li><a href="<?php echo url("/vendoraddbillcredit");?>">เพิ่มใบ PR (เงินเชื่อ)</a></li>
                            <li><a href="<?php echo url("/addpocredit");?>">เพิ่มใบ PO (เงินเชื่อ)</a></li>
                            <!-- <li><a href="<?php echo url("/transferpo");?>">โอนสินค้าจากใบ PO</a></li> -->
                            <!-- <li><a href="<?php echo url("/oilbill");?>">ขออนุมัติบิลน้ำมัน</a></li> -->
                            <li><a href="<?php echo url("/detail_choosesupplier");?>">ข้อมูลใบคัดเลือกผู้ขาย</a></li>
                            <li><a href="<?php echo url("/approve_prlist");?>">อนุมัติ PR ทั้งหมด</a></li>
                            <?php if($emp_code==1383 OR $emp_code==1001 OR
                  									 $emp_code==1427 OR $emp_code==1427 OR
                  									 $emp_code==1002 OR $emp_code==1428 OR
                  									 $emp_code==1439 OR $emp_code==1471 OR
                  									 $emp_code==1475 OR $emp_code==1612 OR
                  									 $emp_code==1611 OR $emp_code==1606 OR
                                     $emp_code==1639 OR $emp_code==1118 OR
                                     $emp_code==1013 OR $emp_code==1138 OR
                                     $emp_code==1654 OR $emp_code==1191){?>

                            <li><a href="<?php echo url("/approve_prlist_ac");?>">อนุมัติ PR (บัญชี)</a></li>
							              <li><a href="<?php echo url("/podetailbydate");?>">PO ทั้งหมดค้นหาจากวันที่ (บัญชี)</a></li>
                            <?php } ?>
                            <?php if($emp_code==1001 OR $emp_code==1002  OR $emp_code==3002  OR $emp_code==1606 ){ ?>
                            <li><a href="<?php echo url("/polist");?>">รายการอนุมัติใบ PO (MD) ทั้งหมด</a></li>
                            <?php } ?>
                            <li><a href="<?php echo url("/polistshow");?>">PO ทั้งหมด</a></li>
                            <li><a href="<?php echo url("/addpoall");?>">สร้างใบ PO ทั้งหมด</a></li>
                            <li><a href="<?php echo url("/inform_po");?>">แจ้งการจ่ายเงิน</a></li>
                            <li><a href="<?php echo url("/withdraw_money");?>">ถอนเงิน</a></li>
                            <li><a href="<?php echo url("/reserve_money");?>">จ่ายเงินสำรอง</a></li>
                            <li><a href="<?php echo url("/!expenses");?>">ค่าใช้จ่ายที่ไม่ถือเป็นรายรับ</a></li>
                            <li><a href="<?php echo url("/ภงด3");?>">ภงด 3</a></li>
                        </ul>
                    </li>

                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">ตั้งค่า<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo url("/configcodeaccount");?>">ตั้งค่าเลขบัญชี</a></li>
                            <li><a href="<?php echo url("/configtypebuy");?>">ตั้งค่าประเภทการซื้อ</a></li>
                            <li><a href="<?php echo url("/configtypepay");?>">ตั้งค่าประเภทการจ่าย</a></li>
                            <li><a href="<?php echo url("/configaccounttax");?>">ตั้งค่าภาษี</a></li>
                            <li><a href="<?php echo url("/configterms");?>">ตั้งค่า terms</a></li>
                            <li><a href="<?php echo url("/configconfiggroupsupp");?>">ตั้งค่ากลุ่มจัดซื้อ</a></li>
							              <li><a href="<?php echo url("/configinitial");?>">ตั้งค่าคำนำหน้าชื่อ</a></li>
                            <li><a href="<?php echo url("/configwithhold");?>">ตั้งค่าหัก ณ ที่จ่าย</a></li>
              							<li><a href="<?php echo url("/configgoodtype");?>">ตั้งค่ากลุ่มสินค้า</a></li>
              							<li><a href="<?php echo url("/configgoodgroup");?>">ตั้งค่าประเภทสินค้า</a></li>
              							<li><a href="<?php echo url("/configgood");?>">ตั้งค่าสินค้า</a></li>
                            <li><a href="<?php echo url("/pettycash");?>">ตั้งรายการเงินหน้าเก๊ะ</a></li>
                            <li><a href="<?php echo url("/bankcash");?>">ตั้งค่าเลขบัญชีธนาคาร</a></li>

                        </ul>
                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">ตั้งค่าโปรแกรม<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo url("/configstatusaccount");?>">ตั้งค่าสถานะการจัดซื้อ</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">งานเงินสด<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <!-- <li><a href="<?php //echo url("/getcash");?>">จัดการข้อมูลเงินสด</a></li>
                            <li class="divider"></li> -->
                            <li><a href="<?php echo url("/getdatacashedit");?>">จัดการข้อมูลเงินตั้งต้นรายวัน</a></li>
                            <li class="divider"></li>
                            <!-- <li><a href="<?php //echo url("/configaccount");?>">ตั้งค่าข้อมูลบัญชี</a></li>
                            <li class="divider"></li> -->
                            <li><a href="<?php echo url("/reportcashdailynew");?>">รายงานเงินสดย่อยรายวัน</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo url("/cashcheckbank");?>">ตรวจเช็คเงินสดย่อยรายวัน</a></li>
                            <!-- <li class="divider"></li> -->
                            <!-- <li><a href="<?php //echo url("/reportcashdaily");?>">รายงานเงินสดย่อยรายวัน</a></li>
                            <li class="divider"></li> -->
                            <!-- <li><a href="<?php //echo url("/reporttotalcashdaily");?>">รายงานภาพรวมเงินสดย่อยรายวัน</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php //echo url("#");?>">รายงานสรุปยอดเงินสดย่อยรายวัน</a></li> -->
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">รายงาน<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo url("/reportpopayin");?>">รายงานใบ PO ที่จ่ายแล้ว</a></li>
                            <li><a href="<?php echo url("/reportpayinandsalary");?>">รายงานรายจ่ายและเงินเดือนของแต่ละเดือน</a></li>
                            <li><a href="<?php echo url("/reportpayinandsalary");?>">รายงานรายจ่ายและเงินเดือนของแต่ละเดือน</a></li>
                            <li><a href="<?php echo url("/boxcover");?>">ใบปะหน้า</a></li>
                            <li><a href="<?php echo url("/reporttaxbuy");?>">รายงานภาษีซื้อ</a></li>
                            <li><a href="<?php echo url("/reportexpenses");?>">รายงานค่าใช้จ่าย</a></li>
                            <li><a href="<?php echo url("/reporttaxsell");?>">รายงานภาษีขาย</a></li>
                            <li><a href="<?php echo url("/reporttaxsellinsurance");?>">รายงานภาษีขาย (เงินประกัน)</a></li>
                            <li><a href="<?php echo url("/reporttaxwht");?>">รายงานภาษีหัก ณ ที่จ่าย</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">พิมพ์เอกสารที่เกี่ยวข้อง<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo url("/printwithholdtaxpo");?>">ภพ 30</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo url("/");?>">ภงด 90</a></li>

                        </ul>
                    </li>
                </ul>
            </div>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown user user-menu">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                          <img src="/fscthr/upload/personal/<?php echo $emp_code.'.jpg';?>" class="user-image" alt="User Image">
                          <span class="hidden-xs"><?php echo $fullname;?></span>
                      </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="/fscthr/upload/personal/<?php echo $emp_code.'.jpg';?>" class="img-circle" alt="User Image">

                                <p>
                                    <?php echo $fullname;?> - <?php echo $position;?>
                                    <br>
                                    สาขา&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $brcode;?>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="#" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                </ul>
            </div>
        </nav>
    </header>



<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{--<script src="dist/js/pages/dashboard.js"></script>--}}
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

<script type="text/javascript" src="{{ url('js/loader/loader.js') }}"></script>
</body>
</html>
