<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Vendorcenter;
use  App\Api\Maincenter;



$db = Connectdb::Databaseall();
?>
@include('headmenu')
<link>

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>

<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">
<script type="text/javascript" src = 'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js'></script>
<script type="text/javascript" src = 'https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js'></script>
<script type="text/javascript" src = 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js'></script>
<script type="text/javascript" src = 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js'></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">

<script type="text/javascript" src = 'js/vendor/reportpopayin.js'></script>

<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<style>
    .modal-ku {
        width: 90%;
        margin: auto;
    }
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">รายงาน</a>
                    </li>

                </ul>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        รายงานภาษีหัก ณ ที่จ่าย
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body" style="overflow-x:auto;">
                              <form action="serachreporttaxwht" method="post">
                                  <input type="hidden" name="_token" value="{{ csrf_token() }}">

                              <div class="row">
                                <div class="col-md-4">

                                </div>
                                <div class="col-md-4">
                                    <?php
                                        $db = Connectdb::Databaseall();
                                        $sql = 'SELECT '.$db['hr_base'].'.branch.*
                                           FROM '.$db['hr_base'].'.branch
                                           WHERE '.$db['hr_base'].'.branch.status = "1"';

                                        $brcode = DB::connection('mysql')->select($sql);

                                    ?>
                                    <select name="branch" id="branch" class="form-control" >
                                        <option value="*">เลือกทั้งหมด</option>
                                        <?php
                                          foreach ($brcode as $key => $value) {
                                        ?>
                                          <option value="<?php echo $value->code_branch;?>" <?php if(!empty($data)&&($value->code_branch==$branch)){ echo "selected";}?> > <?php echo $value->name_branch;?></option>

                                        <?php }?>
                                    </select>
                                </div>
                                <div class="col-md-4">

                                </div>
                              </div>
                              <div class="row">
                                  <br>
                              </div>
                              <div class="row">
                                <div class="col-md-4">

                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="datepicker" id="datepicker" <?php if(!empty($data)){ echo $datepicker;}?>  class="form-control datepicker" readonly>
                                </div>
                                <div class="col-md-4">

                                </div>
                              </div>
                              <div class="row">
                                  <br>
                              </div>
                              <div class="row">
                                <div class="col-md-3">

                                </div>
                                <div class="col-md-3">
                                    <input type="submit" value="ค้นหา" class="btn btn-primary pull-right">
                                </div>
                                <div class="col-md-3">
                                    <input type="reset" class="btn btn-danger">
                                </div>
                                <div class="col-md-3">

                                </div>

                                <div class="col-md-12" align="right">
                                <?php   if(isset($data)){ ////echo $branch_id; ?>

                                        <?php  //if(isset($group_branch_acc_select) && $group_branch_acc_select != ''){?>
                                                <!-- <a href="printcovertaxabb?group_branch_acc_select=<?php //echo $group_branch_acc_select ;?>&&datepickerstart=<?php //echo $datepicker2['start_date'];?>&&datepickerend=<?php  //echo $datepicker2['end_date'];?>"><img src="images/global/printall.png"></a> -->
                                        <?php //}else { ?>
                                        <?php $path = '&datepicker='.$datepicker.'&branch='.$branch?>
                                                <a href="<?php echo url("/printreporttaxwht?$path");?>" target="_blank"><img src="images/global/printall.png"></a>
                                                <!-- <a href="printcovertaxabb?branch_id=<?php //echo $branch_id ;?>&&datepickerstart=<?php //echo $datepicker2['start_date'];?>&&datepickerend=<?php  //echo $datepicker2['end_date'];?> target="_blank" "><img src="images/global/printall.png"></a> -->
                                        <?php //} ?>

                                <?php } ?>
                              </div>

                              </div>
                              </form>

                              <div class="row">
                                  <br>
                              </div>

                              <div class="row">
                                <?php if(!empty($data)){
                                      // echo "<pre>";
                                      // print_r($datatresult);
                                      // print_r($datareserve);
                                      // exit;

                                      $db = Connectdb::Databaseall();

                                  ?>
                                    <table id="example" class="display nowrap" style="width:100% ">
                                        <thead>
                                            <tr>
                                              <th>สาขา</th>
                                              <th>ชื่อ</th>
                                              <th>วันที่</th>
                                              <th>ประเภทเงินได้</th>
                                              <th>จำนวนเงินที่จ่าย 1</th>
                                              <th>จำนวนเงินที่จ่าย 3</th>
                                              <th>จำนวนเงินที่จ่าย 53</th>
                                              <th>อัตราภาษี</th>
                                              <th>ภ.ง.ด.1</th>
                                              <th>ภ.ง.ด.3</th>
                                              <th>ภ.ง.ด.53</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                              <td><!--สาขา-->
                                                <?php
                                                  $modelbranch = Maincenter::databranchbycode($branch);

                                                        if($modelbranch){
                                                             echo $modelbranch[0]->name_branch;
                                                        }
                                                  ?>
                                                </td>
                                            </tr>
                                            <?php

                                              $sumtotalloss = 0;
                                              $vat = 0;
                                              $i = 1;
                                              $sumsubtotal = 0;
                                              $sumvat = 0;
                                              $sumgrandtotal = 0;

                                              $j = 1;

                                              // $sumtotalloss2 = 0;
                                              // $sumsubtotal2 = 0;
                                              // $sumgrandtotal2 = 0;

                                              foreach ($datatresult as $key => $value) { ?>
                                                <tr>
                                                   <td></td>
                                                   <td><?php echo ($value->pre)." ".($value->name_supplier); ?></td><!--ชื่อ-->
                                                   <td><?php echo ($value->date);?></td><!--วันที่-->

                                                   <td><!--ประเภทเงินได้-->
                                                     <?php
                                                     $modelpre = Maincenter::getdatapre($value->pre);

                                                           if($modelpre){

                                                             if($modelpre[0]->statusperson == "0"){
                                                               if($value->whd_percent == "5"){
                                                                 echo "ค่าเช่า";
                                                               }else if($value->whd_percent == "3"){
                                                                 echo "ค่าบริการ";
                                                               }elseif ($value->whd_percent == "1") {
                                                                 echo "ค่าขนส่ง";
                                                               }
                                                             }

                                                             elseif ($modelpre[0]->statusperson == "1") {
                                                               if($value->whd_percent == "5"){
                                                                 echo "ค่าเช่า";
                                                               }else if($value->whd_percent == "3"){
                                                                 echo "ค่าบริการ";
                                                               }elseif ($value->whd_percent == "1") {
                                                                 echo "ค่าขนส่ง";
                                                               }
                                                             }

                                                             elseif ($modelpre[0]->statusperson == "2") {
                                                                echo "เงินเดือน";
                                                             }

                                                           }
                                                     ?>
                                                   </td>
                                                   <td align="right"> <!--จำนวนเงินที่จ่าย 1-->
                                                     <?php
                                                       // if($modelpre[0]->statusperson == "0"){
                                                       //   echo number_format ($value->beforevat,2);
                                                       // }
                                                     ?>
                                                   </td>

                                                   <td align="right"> <!--จำนวนเงินที่จ่าย 3-->
                                                     <?php
                                                       if($modelpre[0]->statusperson == "0"){
                                                         echo number_format ($value->beforevat,2);
                                                       }
                                                     ?>
                                                   </td>

                                                   <td align="right"> <!--จำนวนเงินที่จ่าย 53-->
                                                     <?php
                                                       if($modelpre[0]->statusperson == "1"){
                                                         echo number_format ($value->beforevat,2);
                                                       }
                                                     ?>
                                                   </td>

                                                   <td align="center"><?php echo ($value->whd_percent)."%";?></td> <!--อัตราภาษี-->

                                                   <td align="right"> <!--ภ.ง.ด.1-->
                                                     <?php
                                                       // if($modelpre[0]->statusperson == "0"){
                                                       //   echo number_format ($value->total,2);
                                                       // }
                                                     ?>
                                                   </td>

                                                   <td align="right"> <!--ภ.ง.ด.3-->
                                                     <?php
                                                       if($modelpre[0]->statusperson == "0"){
                                                         echo number_format ($value->total,2);
                                                       }
                                                       // echo number_format($value->payout - $vat , 2);
                                                       // $sumsubtotal = $sumsubtotal + ($value->payout - $vat);
                                                     ?>
                                                   </td>

                                                   <td align="right"> <!--ภ.ง.ด.53-->
                                                     <?php
                                                       if($modelpre[0]->statusperson == "1"){
                                                         echo number_format ($value->total,2);
                                                       }
                                                       // echo number_format ($value->payout,2);
                                                       // $sumgrandtotal = $sumgrandtotal + $value->payout;
                                                     ?>
                                                   </td>
                                                </tr>

                                              <?php $i++; } ?>

                                        </tbody>
                                    </table>

                                <?php } ?>

                              </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')
