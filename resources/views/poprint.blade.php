<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;

?>

<link>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}
<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>

<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">

<script type="text/javascript" src = 'js/vendor/vendorcenter.js'></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

    <style>
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }

        body {
            font-family: "THSarabunNew";
        }
        h4 {
            font-family: "THSarabunNew";
        }
        h4 {
            font-family: "THSarabunNew";
        }
    </style>

<style>
    .modal-ku {
        width: 90%;
        margin: auto;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
           
            <div class="box-body">
				
				
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <div align="center">
                                        <h2>รายงานใบสั่งซื้อ (PO)</h2>
                                    </div>
									<div align="center">
							<?php
							$db = Connectdb::Databaseall();
							$databranch = DB::connection('mysql')->select('SELECT code_branch,name_branch FROM '.$db['hr_base'].'.branch WHERE branch.code_branch = '.$branch);	
							?>		
										สาขา :  
										<?php 
											if($branch == '99'){	
												$nameB = "ทุกสาขา";
											}else{
												$nameB = $databranch[0]->name_branch;
											}
											echo $nameB;
										?>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										ประจำงวด    <?php echo $week;?>
									</div>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div align="center" >
								
								<?php //echo $week;?>
                                <div class="row">
                                    <div class="col-md-5">
                                        <input type="hidden" name="settime" id="settime" value="<?php echo date('Y-m-d')?>">
                                        <input type="hidden" name="setlogin" id="setlogin" value="<?php echo $emp_code = Session::get('emp_code')?>">     
                                    </div>
                                </div>

                                <table class="table table-striped table-bordered" width="95%" border="1" cellpadding="0">
                                    <thead class="thead-inverse" >
                                    <tr>
                                        <td align="center">#</td>
                                        <td>เลขใบ PO</td>
                                        <td align="center">วันเดือนปี</td>
                                        <td align="center">ราคา</td>
                                        <td align="center">จำนวน</td>
                                        <td align="center">vat (บาท)</td>
                                        <td align="center">ภาษีหัก </br>ณ ที่จ่าย (บาท)</td>
                                        <td align="center">ส่วนต่าง</td>
                                        <td align="center">ราคารวม</td>
                                        <td align="center">จ่ายจริง</td>
                                        
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
										$db = Connectdb::Databaseall();
									
										
										// $week = '2018-02-20 10:00:00';
										
										
										if($branch == '99'){	
											$other = "";
										}else{
											$other = " AND H.branch_id = '$branch'";
										}
										
										$sql = "SELECT *
												FROM $db[fsctaccount].po_head AS H
												INNER JOIN $db[fsctaccount].po_detail AS D on H.id = D.po_headid
												WHERE D.statususe = 1 
												AND (H.week = '$week' OR (date like '____-$month-__' AND date like '$year%'))".$other; 
												
										// echo $sql;
										// exit();
										
										$data = DB::connection('mysql')->select($sql);		
																								
										
										$sumpayreal = DB::table('po_head')
													->where('status_head', '=', 2)
													->where('week', '=', '$week')
													->where('branch_id', '=', '$branch')
													->sum('payreal');
									
										$i = 1;
										$sumdiff = 0;
										$sumtotal = 0;
										$sumvat = 0;
										$sumwithhold = 0;
										
                                    ?>
                                    @foreach ($data as $value)
                                        <tr>
                                            <td scope="row">{{ $i }}</td>
                                            <td align="left">{{ $value->po_number }}</td>
                                            <td align="center">{{ $value->date }}</td>
                                            <td align="center">{{ number_format($value->price,2) }}</td>
                                            <td align="center">{{ $value->amount }}</td>
                                            <td align="right">{{ number_format(($value->vat * ($value->price * $value->amount))/100,2) }}</td>
                                            <td align="right">{{ number_format(((($value->vat * ($value->price * $value->amount))/100) * $value->withhold)/100,2) }}</td>
                                            <td align="right">{{ number_format($value->payreal - $value->total,2) }}</td>                                           
                                            <td align="right">{{ number_format($value->total,2) }}</td>
											<td align="right">{{ number_format($value->payreal,2) }}</td>
											
                                        </tr>
										<?php
										$diff = $value->payreal - $value->total;
										$sumdiff = $sumdiff + $diff;
										// echo $diff;
										
										$total = $value->total;
										$sumtotal = $sumtotal + $total;
										// echo $total;
										
										$vat = ($value->vat * ($value->price * $value->amount))/100;
										$sumvat = $sumvat + $vat;
										// echo $vat;
										
										$withhold = ((($value->vat * ($value->price * $value->amount))/100) * $value->withhold)/100;
										$sumwithhold = $sumwithhold + $withhold;
										// echo $withhold;
										?>
                                        <?php $i++; ?>
                                    @endforeach
                                    </tbody>
										<tr>
											<td colspan="5" align="center"><B>รวมทั้งหมด : </B></td>
											<td colspan="1" align="right"><B><?php echo number_format($sumvat,2); ?></B></td>
											<td colspan="1" align="right"><B><?php echo number_format($sumwithhold,2); ?></B></td>
											<td colspan="1" align="right"><B><?php echo number_format($sumdiff,2);?></B></td>
											<td colspan="1" align="right"><B><?php echo number_format($sumtotal,2) ?></B></td>
											<td colspan="1" align="right"><B>{{ number_format($sumpayreal,2) }}</B></td>
										</tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>

