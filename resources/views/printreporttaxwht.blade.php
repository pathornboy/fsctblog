<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;

use App\Api\Datetime;
use App\working_company;
use Illuminate\Support\Facades\Input;

?>

<link>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}
<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>

<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">

<script type="text/javascript" src = 'js/vendor/vendorcenter.js'></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

    <style>
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }

        body {
            font-family: "THSarabunNew";
            font-size: 12px;
        }
        h4 {
            font-family: "THSarabunNew";
        }
        h4 {
            font-family: "THSarabunNew";
        }

    </style>

<style>
    .modal-ku {
        width: 90%;
        margin: auto;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="box-body">

            <?php

              $data = Input::all();
              $db = Connectdb::Databaseall();
              // echo "<pre>";
              // print_r($data);
              // exit;

              $datepicker = explode("-",trim(($data['datepicker'])));

              $datepickerstart = explode("/",trim(($datepicker[0])));
              if(count($datepickerstart) > 0) {
                  $datetime = $datepickerstart[1] . '-' . $datepickerstart[0]; //วัน - เดือน
              }

              $datepickerend = explode("/",trim(($datepicker[1])));
              if(count($datepickerend) > 0) {
                  $datetime2 = $datepickerend[1] . '-' . $datepickerend[0]; //วัน - เดือน
              }

              if($datepickerstart[0] == "01"){$monthTH = "มกราคม";
                }else if($datepickerstart[0] == "02"){$monthTH = "กุมภาพันธ์";
                }else if($datepickerstart[0] == "03"){$monthTH = "มีนาคม";
                }else if($datepickerstart[0] == "04"){$monthTH = "เมษายน";
                }else if($datepickerstart[0] == "05"){$monthTH = "พฤษภาคม";
                }else if($datepickerstart[0] == "06"){$monthTH = "มิถุนายน";
                }else if($datepickerstart[0] == "07"){$monthTH = "กรกฎาคม";
                }else if($datepickerstart[0] == "08"){$monthTH = "สิงหาคม";
                }else if($datepickerstart[0] == "09"){$monthTH = "กันยายน";
                }else if($datepickerstart[0] == "10"){$monthTH = "ตุลาคม";
                }else if($datepickerstart[0] == "11"){$monthTH = "พฤศจิกายน";
                }else if($datepickerstart[0] == "12"){$monthTH = "ธันวาคม";
                }

                $modelname = Maincenter::databranchbycode($data['branch']);

                $compid = $modelname[0]->company_id;
                $sqlcompany = "SELECT * FROM $db[hr_base].working_company  WHERE id ='$compid' ";
                $datacomp = DB::connection('mysql')->select($sqlcompany);

                $branch_id = $data['branch'];
                $sql = "SELECT * FROM $db[hr_base].branch  WHERE code_branch ='$branch_id' ";
                $databranch = DB::connection('mysql')->select($sql);

            ?>

          <div class="row">
              <div class="col-md-12">
                  <div class="box box-primary">
                      <div class="breadcrumbs" id="breadcrumbs">
                          <ul class="breadcrumb">
                              <div align="center">



                              <table width="100%">
                                <tr>
                                  <td align="center" ><b>รายงานภาษีหัก ณ ที่จ่าย</b></td>
                                </tr>

                                <tr>
                                  <td align="center" ><b>เดือนภาษี {{$monthTH}} ปี {{$datepickerstart[2]}}</b></td>
                                </tr>

                                <tr>
                                  <td align="center" ><b>ชื่อผู้ประกอบการ : บริษัท ฟ้าใสคอนสตรัคชั่นทูลส์ จำกัด ({{$modelname[0]->name_branch}})</b></td>
                                </tr>

                              </table>
              </div>
              </div>
              </ul><!-- /.breadcrumb -->
              <!-- /section:basics/content.searchbox -->
              </div>

							<div align="center">
							<?php

              $data = Input::all();
              $db = Connectdb::Databaseall();
              // echo "<pre>";
              // print_r($data);
              // exit;

              // $branch_id = $data['branch_id'];
              // $start_date = $data['datepickerstart']." 00:00:00";
              // $end_date = $data['datepickerend']." 23:59:59";

              $datepicker = explode("-",trim(($data['datepicker'])));

              // $start_date = $datepicker[0];
              $e1 = explode("/",trim(($datepicker[0])));
                      if(count($e1) > 0) {
                          $start_date = $e1[2] . '-' . $e1[0] . '-' . $e1[1]; //ปี - เดือน - วัน
                          $start_date2 = $start_date." 00:00:00";
                      }

              // $end_date = $datepicker[1];
              $e2 = explode("/",trim(($datepicker[1])));
                      if(count($e2) > 0) {
                          $end_date = $e2[2] . '-' . $e2[0] . '-' . $e2[1]; //ปี - เดือน - วัน
                          $end_date2 = $end_date." 23:59:59";
                      }

              $branch_id = $data['branch'];

              // echo "<pre>";
              // print_r($start_date);
              // print_r($end_date);
              // exit;

              $sql = 'SELECT '.$db['fsctaccount'].'.withholdtaxpo.*,
                             '.$db['fsctaccount'].'.po_head.supplier_id,
                             '.$db['fsctaccount'].'.supplier.pre,name_supplier

                     FROM '.$db['fsctaccount'].'.withholdtaxpo
                     INNER JOIN  '.$db['fsctaccount'].'.po_head
                        ON '.$db['fsctaccount'].'.po_head.id = '.$db['fsctaccount'].'.withholdtaxpo.id_po

                     INNER JOIN  '.$db['fsctaccount'].'.supplier
                        ON '.$db['fsctaccount'].'.supplier.id = '.$db['fsctaccount'].'.po_head.supplier_id

                      WHERE '.$db['fsctaccount'].'.withholdtaxpo.date  BETWEEN "'.$start_date.'" AND  "'.$end_date.'"
                        AND '.$db['fsctaccount'].'.withholdtaxpo.branch = "'.$branch_id.'"
                        AND '.$db['fsctaccount'].'.withholdtaxpo.status != 99
                        ORDER BY '.$db['fsctaccount'].'.withholdtaxpo.date
                     ';

              $datatresult = DB::connection('mysql')->select($sql);
              // echo "<pre>";
              // print_r($datatresult);
              // exit;

              ?>


              <div align="center" >

							<?php //echo $week;?>
              <!-- <div class="row">
                  <div class="col-md-5">
                      <input type="hidden" name="settime" id="settime" value="<?php //echo date('Y-m-d')?>">
                      <input type="hidden" name="setlogin" id="setlogin" value="<?php //echo $emp_code = Session::get('emp_code')?>">
                  </div>
              </div> -->

              <!-- <table class="table table-striped table-bordered" width="100%" border="1" cellpadding="0"> -->
              <!-- <font size="1" > -->
              <table class="table table-bordered" width="100%" border="1" cellspacing="0">
                  <thead class="thead-inverse" >
                  <tr>
                    <th align="center">สาขา</th>
                    <th align="center">ชื่อ</th>
                    <th align="center">วันที่</th>
                    <th align="center">ประเภทเงินได้</th>
                    <th align="center">จำนวนเงินที่จ่าย 1</th>
                    <th align="center">จำนวนเงินที่จ่าย 3</th>
                    <th align="center">จำนวนเงินที่จ่าย 53</th>
                    <th align="center">อัตราภาษี</th>
                    <th align="center">ภ.ง.ด.1</th>
                    <th align="center">ภ.ง.ด.3</th>
                    <th align="center">ภ.ง.ด.53</th>
                  </tr>
                  </thead>
                  <tbody>

                    <tr>
                      <td><!--สาขา-->
                        <?php
                          $modelbranch = Maincenter::databranchbycode($branch);

                                if($modelbranch){
                                     echo $modelbranch[0]->name_branch;
                                }
                          ?>
                      </td>
                      <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    </tr>

                  <?php

                  $sumtotalloss = 0;
                  $vat = 0;
                  $i = 1;
                  $sumsubtotal = 0;
                  $sumvat = 0;
                  $sumgrandtotal = 0;

                  $sum1 = 0;
                  $sum2 = 0;
                  $sum3 = 0;

                  $sumtotal1 = 0;
                  $sumtotal2 = 0;
                  $sumtotal3 = 0;

                  $j = 1;

                  ?>
                  <?php
                  foreach ($datatresult as $key => $value) { ?>
                    <tr>
                       <td></td>
                       <td><?php echo ($value->pre)." ".($value->name_supplier); ?></td><!--ชื่อ-->
                       <td><?php echo ($value->date);?></td><!--วันที่-->

                       <td><!--ประเภทเงินได้-->
                         <?php
                         $modelpre = Maincenter::getdatapre($value->pre);

                               if($modelpre){

                                 if($modelpre[0]->statusperson == "0"){
                                   if($value->whd_percent == "5"){
                                     echo "ค่าเช่า";
                                   }else if($value->whd_percent == "3"){
                                     echo "ค่าบริการ";
                                   }elseif ($value->whd_percent == "1") {
                                     echo "ค่าขนส่ง";
                                   }
                                 }

                                 elseif ($modelpre[0]->statusperson == "1") {
                                   if($value->whd_percent == "5"){
                                     echo "ค่าเช่า";
                                   }else if($value->whd_percent == "3"){
                                     echo "ค่าบริการ";
                                   }elseif ($value->whd_percent == "1") {
                                     echo "ค่าขนส่ง";
                                   }
                                 }

                                 elseif ($modelpre[0]->statusperson == "2") {
                                    echo "เงินเดือน";
                                 }

                               }
                         ?>
                       </td>
                       <td align="right"> <!--จำนวนเงินที่จ่าย 1-->
                         <?php
                           // if($modelpre[0]->statusperson == "0"){
                           //   echo number_format ($value->beforevat,2);
                           //   $sum1 = $sum1 + ($value->beforevat);
                           // }
                         ?>
                       </td>

                       <td align="right"> <!--จำนวนเงินที่จ่าย 3-->
                         <?php
                           if($modelpre[0]->statusperson == "0"){
                             echo number_format ($value->beforevat,2);
                             $sum2 = $sum2 + ($value->beforevat);
                           }
                         ?>
                       </td>

                       <td align="right"> <!--จำนวนเงินที่จ่าย 53-->
                         <?php
                           if($modelpre[0]->statusperson == "1"){
                             echo number_format ($value->beforevat,2);
                             $sum3 = $sum3 + ($value->beforevat);
                           }
                         ?>
                       </td>

                       <td align="center"><?php echo ($value->whd_percent)."%";?></td> <!--อัตราภาษี-->

                       <td align="right"> <!--ภ.ง.ด.1-->
                         <?php
                           // if($modelpre[0]->statusperson == "0"){
                           //   echo number_format ($value->total,2);
                           //   $sumtotal1 = $sumtotal1 + ($value->total);
                           // }
                         ?>
                       </td>

                       <td align="right"> <!--ภ.ง.ด.3-->
                         <?php
                           if($modelpre[0]->statusperson == "0"){
                             echo number_format ($value->total,2);
                             $sumtotal2 = $sumtotal2 + ($value->total);
                           }
                           // echo number_format($value->payout - $vat , 2);
                           // $sumsubtotal = $sumsubtotal + ($value->payout - $vat);
                         ?>
                       </td>

                       <td align="right"> <!--ภ.ง.ด.53-->
                         <?php
                           if($modelpre[0]->statusperson == "1"){
                             echo number_format ($value->total,2);
                             $sumtotal3 = $sumtotal3 + ($value->total);
                           }
                           // echo number_format ($value->payout,2);
                           // $sumgrandtotal = $sumgrandtotal + $value->payout;
                         ?>
                       </td>
                    </tr>

                  <?php $i++; } ?>

                    <tr>
                      <td colspan="3" align="right"></td>
                      <td align="center">รวม</td>
                      <td align="right"><b><?php echo number_format($sum1,2);  ?></b></td>
                      <td align="right"><b><?php echo number_format($sum2,2);  ?></b></td>
                      <td align="right"><b><?php echo number_format($sum3,2); ?></b></td>
                      <td></td>
                      <td align="right"><b><?php echo number_format($sumtotal1,2);  ?></b></td>
                      <td align="right"><b><?php echo number_format($sumtotal2,2);  ?></b></td>
                      <td align="right"><b><?php echo number_format($sumtotal3,2); ?></b></td>
                    </tr>

                        </tbody>
                     </table>
                     <!-- </font> -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
