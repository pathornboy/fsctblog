<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Vendorcenter;
use  App\Api\Maincenter;



$db = Connectdb::Databaseall();
?>
@include('headmenu')
<link>

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>

<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">
<script type="text/javascript" src = 'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js'></script>
<script type="text/javascript" src = 'https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js'></script>
<script type="text/javascript" src = 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js'></script>
<script type="text/javascript" src = 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js'></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">

<script type="text/javascript" src = 'js/vendor/reportpopayin.js'></script>

<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<style>
    .modal-ku {
        width: 90%;
        margin: auto;
    }
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">รายงาน</a>
                    </li>

                </ul>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        รายงานภาษีซื้อ
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body" style="overflow-x:auto;">
                              <form action="serachreporttaxbuy" method="post">
                                  <input type="hidden" name="_token" value="{{ csrf_token() }}">

                              <div class="row">
                                <div class="col-md-4">

                                </div>
                                <div class="col-md-4">
                                    <?php
                                        $db = Connectdb::Databaseall();
                                        $sql = 'SELECT '.$db['hr_base'].'.branch.*
                                           FROM '.$db['hr_base'].'.branch
                                           WHERE '.$db['hr_base'].'.branch.status = "1"';

                                        $brcode = DB::connection('mysql')->select($sql);

                                    ?>
                                    <select name="branch" id="branch" class="form-control" >
                                        <option value="*">เลือกทั้งหมด</option>
                                        <?php
                                          foreach ($brcode as $key => $value) {
                                        ?>
                                          <option value="<?php echo $value->code_branch;?>" <?php if(!empty($data)&&($value->code_branch==$branch)){ echo "selected";}?> > <?php echo $value->name_branch;?></option>

                                        <?php }?>
                                    </select>
                                </div>
                                <div class="col-md-4">

                                </div>
                              </div>
                              <div class="row">
                                  <br>
                              </div>
                              <div class="row">
                                <div class="col-md-4">

                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="datepicker" id="datepicker" <?php if(!empty($data)){ echo $datepicker;}?>  class="form-control datepicker" readonly>
                                </div>
                                <div class="col-md-4">

                                </div>
                              </div>
                              <div class="row">
                                  <br>
                              </div>
                              <div class="row">
                                <div class="col-md-3">

                                </div>
                                <div class="col-md-3">
                                    <input type="submit" value="ค้นหา" class="btn btn-primary pull-right">
                                </div>
                                <div class="col-md-3">
                                    <input type="reset" class="btn btn-danger">
                                </div>
                                <div class="col-md-3">

                                </div>

                                <div class="col-md-12" align="right">
                                <?php   if(isset($data)){ ////echo $branch_id; ?>

                                        <?php  //if(isset($group_branch_acc_select) && $group_branch_acc_select != ''){?>
                                                <!-- <a href="printcovertaxabb?group_branch_acc_select=<?php //echo $group_branch_acc_select ;?>&&datepickerstart=<?php //echo $datepicker2['start_date'];?>&&datepickerend=<?php  //echo $datepicker2['end_date'];?>"><img src="images/global/printall.png"></a> -->
                                        <?php //}else { ?>
                                        <?php $path = '&datepicker='.$datepicker.'&branch='.$branch?>
                                                <a href="<?php echo url("/printreporttaxbuy?$path");?>" target="_blank"><img src="images/global/printall.png"></a>
                                                <!-- <a href="printcovertaxabb?branch_id=<?php //echo $branch_id ;?>&&datepickerstart=<?php //echo $datepicker2['start_date'];?>&&datepickerend=<?php  //echo $datepicker2['end_date'];?> target="_blank" "><img src="images/global/printall.png"></a> -->
                                        <?php //} ?>

                                <?php } ?>
                              </div>

                              </div>
                              </form>

                              <div class="row">
                                  <br>
                              </div>

                              <div class="row">
                                <?php if(!empty($data)){
                                      // echo "<pre>";
                                      // print_r($datatresult);
                                      // print_r($datareserve);
                                      // exit;

                                      $db = Connectdb::Databaseall();

                                  ?>
                                    <table id="example" class="display nowrap" style="width:100% ">
                                        <thead>
                                            <tr>
                                              <th>ลำดับ</th>
                                              <th>ปี/เดือน/วัน</th>
                                              <th>เลขที่</th>
                                              <th>ชื่อผู้ซื้อสินค้า/ผู้รับบริการ</th>
                                              <th>เลขประจำตัวผู้เสียภาษี</th>
                                              <th>สถานประกอบการ</th>
                                              <th>มูลค่าสินค้าหรือบริการ</th>
                                              <th>จำนวนเงินภาษีมูลค่าเพิ่ม</th>
                                              <th>จำนวนเงินรวมทั้งหมด</th>
                                              <th>หมายเหตุ</th>
                                            </tr>
                                        </thead>
                                        <!-- <tfoot>
                                      		<tr>
                                      			<td></td>
                                      			<td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                      			<td>Totals</td>
                                      			<td></td>
                                      			<td></td>
                                      		</tr>
                                      	</tfoot> -->
                                        <tbody>
                                            <?php

                                              $sumtotalloss = 0;
                                              $vat = 0;
                                              $i = 1;
                                              $sumsubtotal = 0;
                                              $sumvat = 0;
                                              $sumgrandtotal = 0;

                                              $j = 1;

                                              // $sumtotalloss2 = 0;
                                              // $sumsubtotal2 = 0;
                                              // $sumgrandtotal2 = 0;

                                              foreach ($datatresult as $key => $value) { ?>
                                                <tr>
                                                   <td><?php echo $i;?></td><!--ลำดับ-->
                                                   <td><?php echo ($value->datebill);?></td><!--ปี/เดือน/วัน-->
                                                   <td><?php echo ($value->bill_no);?></td><!--เลขที่-->
                                                   <td>
                                                     <?php
                                                     $modelsupplier = Maincenter::getdatasupplierpo($value->id_po);
                                                           if($modelsupplier){
                                                             echo ($modelsupplier[0]->pre);
                                                             echo ($modelsupplier[0]->name_supplier);

                                                     ?>
                                                   </td>
                                                   <td><?php echo ($modelsupplier[0]->tax_id);?></td>
                                                   <td><?php echo ($modelsupplier[0]->type_branch); }?></td>

                                                   <?php  $vat = $value->vat_price; ?>
                                                   <?php  //$vat = ((($value->payout + $value->wht) * 7 )/ 100); ?>

                                                   <td>
                                                     <?php
                                                       echo number_format($value->payout - $vat + $value->wht , 2);
                                                       $sumsubtotal = $sumsubtotal + ($value->payout - $vat + $value->wht);
                                                     ?>
                                                   </td>

                                                   <td>

                                                     <?php
                                                       echo number_format ($vat,2);
                                                       $sumvat = $sumvat + $vat;
                                                     ?>
                                                   </td>

                                                   <td>
                                                     <?php
                                                       echo number_format ($value->payout + $value->wht,2);
                                                       $sumgrandtotal = $sumgrandtotal + $value->payout + $value->wht;
                                                     ?>
                                                   </td>
                                                   <td>
                                                     <?php
                                                       //echo ($value->payout);
                                                         echo "-";
                                                     ?>
                                                   </td>
                                                </tr>

                                              <?php $i++; } ?>

                                              <?
                                              foreach ($datareserve as $key2 => $value2) { ?>

                                              <tr>
                                                 <td><?php echo $i;?></td><!--ลำดับ-->
                                                 <td><?php echo ($value2->date_bill_no);?></td><!--ปี/เดือน/วัน-->
                                                 <td><?php echo ($value2->bill_no);?></td><!--เลขที่-->
                                                 <td>
                                                   <?php
                                                   $modelsuppliername = Maincenter::getdatasupplierpo($value2->po_ref);
                                                         if($modelsuppliername){
                                                           echo ($modelsuppliername[0]->pre);
                                                           echo ($modelsuppliername[0]->name_supplier);

                                                   ?>
                                                 </td>
                                                 <td><?php echo ($modelsuppliername[0]->tax_id);?></td>
                                                 <td><?php echo ($modelsuppliername[0]->type_branch); }?></td>

                                                 <?php  //$vat = (($value2->vat_money * 7 )/ 107); ?>

                                                 <td>
                                                   <?php
                                                     echo number_format($value2->amount, 2);
                                                     $sumsubtotal = $sumsubtotal + $value2->amount;
                                                   ?>
                                                 </td>

                                                 <td>
                                                   <?php
                                                     echo number_format ($value2->vat_money,2);
                                                     $sumvat = $sumvat + $value2->vat_money;
                                                   ?>
                                                 </td>

                                                 <td>
                                                   <?php
                                                     echo number_format ($value2->total,2);
                                                     $sumgrandtotal = $sumgrandtotal + $value2->total;
                                                   ?>
                                                 </td>
                                                 <td>
                                                   <?php
                                                     //echo ($value2->payout);
                                                       echo "-";
                                                   ?>
                                                 </td>
                                              </tr>

                                              <?php $i++; } ?>

                                                 <!-- <tr>
                                                   <td colspan="5" align="right"></td>
                                                   <td><center><b>รวม</b></center></td>
                                                   <td><b><?php //echo number_format($sumsubtotal,2);  ?></b></td>
                                                   <td><b><?php //echo number_format($sumvat,2);  ?></b></td>
                                                   <td><b><?php //echo number_format($sumgrandtotal,2); ?></b></td>
                                                   <td></td>
                                                 </tr> -->

                                        </tbody>
                                    </table>

                                    <!-- <tfoot>
                                      <tr>
                                          <th colspan="6" style="text-align:right">รวม</th>
                                          <th></th>
                                      </tr>
                                    </tfoot> -->

                                <?php } ?>
                              </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')
