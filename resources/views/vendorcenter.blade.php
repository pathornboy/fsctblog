<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;

?>
@include('headmenu')
<link>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}
<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>

<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">

<script type="text/javascript" src = 'js/vendor/vendorcenter.js'></script>


<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<style>
    .modal-ku {
        width: 90%;
        margin: auto;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">งานจัดซื้อ</a>
                    </li>
                    <li class="active">ข้อมูล Vendors</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        รายนาม Vendors
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-2 col-md-offset-5">
                                        <a href="<?php echo url("/vendoraddbill");?>"><img src="images/global/invoice.png">เพิ่มรายการจัดซื้อ</a>
                                    </div>

                                    <div class="col-md-3">
                                        <a href="<?php echo url("/detail_choosesupplier");?>" title="ปริ้นใบคัดเลือกผู้ขาย" onclick="" ><img src="images/global/po.png">ใบคัดเลือกผู้ขาย</a>
                                    </div>

                                    <div class="col-md-2">
                                        <input type="hidden" name="settime" id="settime" value="<?php echo date('Y-m-d')?>">
                                        <input type="hidden" name="setlogin" id="setlogin" value="<?php echo $emp_code = Session::get('emp_code')?>">
                                        <a href="#" title="เพิ่มข้อมูล" data-toggle="modal" data-target="#myModal_addvendor" onclick="insertnew()" ><img src="images/global/add.png">เพิ่มข้อมูล</a>
                                    </div>
                                </div>
                                <div class="row-fluid table-responsive">
                                    <table id="example" class="table table-striped table-bordered">
                                      <thead class="thead-inverse">
                                      <tr>
                                          <td>#</td>
                                          <td>ชื่อบริษัท</td>
                                          <td>สาขา</td>
                                          <td>เบอร์โทร</td>
                                          <td>มือถือ</td>
                                          <td>สถานะ</td>
                                          <td>Credit Terms</td>
                                          <td>แก้ไข</td>

                                      </tr>
                                      </thead>
                                      <tbody>
                                      <?php
                                      $db = Connectdb::Databaseall();
                                      $data = DB::connection('mysql')->select('SELECT pre, type_branch, name_supplier, phone, mobile, status, id, terms_id FROM '.$db['fsctaccount'].'.supplier');
                                      $i = 1;
                                      ?>
                                      @foreach ($data as $value)
                                          <tr>
                                              <td scope="row">{{ $i }}</td>
                                              <td align="left">{{ $value->pre }}  {{ $value->name_supplier }}</td>
                                              <td align="center">{{ $value->type_branch }}</td>
                                              <td align="center">{{ $value->phone }}</td>
                                              <td align="center">{{ $value->mobile }}</td>
                                              <td align="center"><?php
                                                  if($value->status==0){
                                                      $level_emp = Session::get('level_emp');
                                                       $emp_code = Session::get('emp_code');

                                                      if($level_emp == 1 OR $level_emp == 2 OR $emp_code== 1639 OR $emp_code== 1427 OR $emp_code== 1606 OR $emp_code== 1611 OR $emp_code== 1612){
                                                          echo "<a href='#' data-toggle='modal' data-target='#editstatus' onclick='updatestatusvendor($value->id)'><font style='color: #003eff'>รออนุมัติ</font></a>";
                                                      }else{
                                                          echo "<font style='color: #003eff'>รออนุมัติ</font>";
                                                      }
                                                  }else if($value->status==1){
                                                      $level_emp = Session::get('level_emp');
                                                      if($level_emp == 1 OR $level_emp == 2 OR $emp_code== 1639 OR $emp_code== 1427 OR $emp_code== 1606 OR $emp_code== 1611 OR $emp_code== 1612){
                                                          echo "<a href='#' data-toggle='modal' data-target='#editstatus' onclick='updatestatusvendor($value->id)'><font style='color: green'>อนุมัติ</font></a>";
                                                      }else{
                                                          echo "<font style='color:green'>อนุมัติ</font>";
                                                      }

                                                  }else{
                                                      $level_emp = Session::get('level_emp');
                                                      if($level_emp == 1 OR $level_emp == 2 OR $emp_code== 1639 OR $emp_code== 1427 OR $emp_code== 1606 OR $emp_code== 1611 OR $emp_code== 1612){
                                                          echo "<a href='#' data-toggle='modal' data-target='#editstatus' onclick='updatestatusvendor($value->id)'><font style='color: red'>ยกเลิก</font></a>";
                                                      }else{
                                                          echo "<font style='color: red'>ยกเลิก</font>";
                                                      }

                                                  }
                                                  ?></td>
                                              <td align="center">
                                                  <?php
                                                  //  print_r($value->terms_id);
                                                  $dataterms = Accountcenter::dataterms($value->terms_id);
                                                  print_r($dataterms[0]->name);
                                                  ?>
                                              </td>

                                              <td align="center">
                                                  <a href="#" title="แก้ไขข้อมูล" data-toggle="modal" data-target="#myModal" onclick="getdata({{ $value->id }})"><img src="images/global/edit-icon.png"></a>
                                              </td>
                                          </tr>
                                          <?php $i++; ?>
                                      @endforeach
                                      </tbody>
                                  </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')


<!-- Modal_addvendor -->

<div class="modal fade" id="myModal_addvendor" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">เพิ่มข้อมูล vendor</h5>
            </div>

            <div class="modal-body">
                <form id="configFormvendors_addvendor" onsubmit="return getdatesubmit_addvendor();" data-toggle="validator" method="post" class="form-horizontal">
                    <input value="{{ null }}" type="hidden" id="id_addvendor" name="id" />


                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">คำนำหน้า<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <select name="pre" id="pre_addvendor" class="form-control" required>
                                        <?php
                                        $db = Connectdb::Databaseall();
                                        $dataper = DB::connection('mysql')->select('SELECT per FROM '.$db['fsctaccount'].'.initial');
                                        ?>
                                        <option value="">เลือกคำนำหน้า</option>
                                        @foreach ($dataper as $value)
                                            <option value="{{ $value->per}}">{{ $value->per}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">ชื่อบริษัท/ชื่อ<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="name_supplier" id="name_supplier_addvendor" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">รหัสสาขา<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="type_branch" id="type_branch_addvendor" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">บ้าน เลขที่/หมู่ที่<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="address" id="address_addvendor" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">ตำบล<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="district" id="district_addvendor" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">อำเภอ<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="amphur" id="amphur_addvendor" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">จังหวัด<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="province" id="province_addvendor" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">รหัสไปรษณีย์<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="zipcode" id="zipcode_addvendor" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">เบอร์โทร<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="phone" id="phone_addvendor" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">มือถือ<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="tel" class="form-control" name="mobile" id="mobile_addvendor" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">e-mail<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="email" class="form-control" name="email" id="email_addvendor" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">รหัสผู้เสียภาษี<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="tax_id" id="tax_id_addvendor" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Credit Terms<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">

                                    <select name="terms_id" id="terms_id_addvendor" class="form-control" required>
                                        <?php
                                        $db = Connectdb::Databaseall();
                                        $dataterms = DB::connection('mysql')->select('SELECT name, id FROM '.$db['fsctaccount'].'.supplier_terms');
                                        ?>
                                        <option value="6">ไม่มี term</option>
                                        @foreach ($dataterms as $value)

                                            <option value="{{ $value->id}}">{{ $value->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">หมายเหตุ<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="Note" id="Note_addvendor" required>
                                    <input type="hidden" name="status" id="status_addvendor_addvendor" value="0">
                                    <input type="hidden" name="createby" id="createby_add_addvendor" value="<?php echo $emp_code = Session::get('emp_code')?>" >
                                    <input type="hidden" name="createdate" id="createdate_addvendor_addvendor" value="<?php echo date('Y-m-d')?>" >
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">ประเภทของกิจการ<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input class="form-control" type="text" name="business_type" value="" required>

                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">ผลิตภัณฑ์หลัก<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input class="form-control" type="text" name="main_product" value="" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">รหัสเจ้าหนี้การค้า<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input class="form-control" type="text" name="codecreditor" value="" required>

                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">กลุ่มอุตสาหกรรมที่เป็นลูกค้า<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input class="form-control" type="text" name="main_industry" value="" required>

                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">ปีที่ก่อตั้ง<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input class="form-control" type="number" placeholder="พ.ศ." name="establish_year" value="" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">ระบบบริหารที่เกี่ยวกับคุณภาพ (เช่น ISO 9001, TS 16949, GMP, TQM)<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-3">
                                    <select class="form-control" name="iso_system" required>
                                        <option value>เลือก</option>
                                        <option value="1">มี</option>
                                        <option value="0">ไม่มี</option>
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">มาตรฐานผลิตภัณฑ์สินค้าอุตสาหกรรม  (เช่น  มอก. ,JIS, ASTM, UL, CE Mark)<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-3">
                                    <select class="form-control" name="product_standard" required>
                                        <option value="">เลือก</option>
                                        <option value="1">มี</option>
                                        <option value="0">ไม่มี</option>
                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-6 control-label">ชิ้นงานตัวอย่างเพื่อพิจารณา<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <select class="form-control" name="example_product" required>
                                        <option value="">เลือก</option>
                                        <option value="1">มี</option>
                                        <option value="0">ไม่มี</option>
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">จำนวน<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input class="form-control" type="number" name="example_product_number" value="" required>
                                </div>
                                <div class="col-xs-2">
                                  <label class="col-xs-4 control-label">ชิ้น</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="col-xs-6 control-label">สามารถส่งมอบสินค้าใด้ภายใน<i><span style="color: red">*</span></i></label>
                              <div class="col-xs-6">
                                  <input class="form-control" type="number" name="number_delivery" value="" required>
                              </div>
                              <div class="col-xs-2">
                                <label class="col-xs-4 control-label">วัน</label>
                              </div>
                          </div>
                      </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">บุคคลที่สามารถติดต่อได้<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input class="form-control" type="number" name="agent" value="" required>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                      <table class="table table-striped">
                          <thead>
                            <tr>
                              <th class="text-center">เกณฑ์และคะแนนในการคัดลือก</th>
                              <th class="text-center">คะแนนเป้าหมาย</th>
                              <th class="text-center">คะแนนที่ให้</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>1. ผลการตรวจสอบคุณภาพสินค้า</td>
                              <td class="text-center">30 คะแนน</td>
                              <td class="form-group"><input class="form-control" type="number" min="1" max="30" name="quality_point" required placeholder="คะแนน" /></td>
                            </tr>
                            <tr>
                              <td>2. ด้านราคา</td>
                              <td class="text-center">20 คะแนน</td>
                              <td class="form-group"><input class="form-control" type="number" min="1" max="20" name="price_point" required placeholder="คะแนน" /></td>
                            </tr>
                            <tr>
                              <td>3. ด้านการส่งมอบ</td>
                              <td class="text-center">20 คะแนน</td>
                              <td class="form-group"><input class="form-control" type="number" min="1" max="20" name="delivery_point" required placeholder="คะแนน" /></td>
                            </tr>
                            <tr>
                              <td>4. ระยะเวลาในการให้เครดิต</td>
                              <td class="text-center">15 คะแนน</td>
                              <td class="form-group"><input class="form-control" type="number" min="1" max="15" name="credit_point" required placeholder="คะแนน" /></td>
                            </tr>
                            <tr>
                              <td>5. ระบบคุณภาพ /ควบคุมกระบวนการ</td>
                              <td class="text-center">15 คะแนน</td>
                              <td class="form-group"><input class="form-control" type="number" min="1" max="15" name="manufacture_point" required placeholder="คะแนน" /></td>
                            </tr>

                          </tbody>
                      </table>

                    </div>

                    <div class="row">
                        <br>
                    </div>
                    <div class="row">

                        <div class="col-md-4">

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-xs-5 col-xs-offset-3">
                                    <button type="submit" id="Btn_save_addvendor" class="btn btn-primary">Save</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>


<!-- Modal_addvendor -->

<!-- Modal -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">ข้อมูล vendor</h5>
            </div>

            <div class="modal-body">
                <form id="configFormvendors" onsubmit="return getdatesubmit();" data-toggle="validator" method="post" class="form-horizontal">
                    <input value="{{ null }}" type="hidden" id="id" name="id" />


                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">คำนำหน้า<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <select name="pre" id="pre" class="form-control" required>
                                        <?php
                                        $db = Connectdb::Databaseall();
                                        $dataper = DB::connection('mysql')->select('SELECT per FROM '.$db['fsctaccount'].'.initial');
                                        ?>
                                        <option value="">เลือกคำนำหน้า</option>
                                        @foreach ($dataper as $value)
                                            <option value="{{ $value->per}}">{{ $value->per}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">ชื่อบริษัท/ชื่อ<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="name_supplier" id="name_supplier" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">รหัสสาขา<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="type_branch" id="type_branch" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">บ้าน เลขที่/หมู่ที่<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="address" id="address" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">ตำบล<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="district" id="district" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">อำเภอ<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="amphur" id="amphur" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">จังหวัด<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="province" id="province" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">รหัสไปรษณีย์<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="zipcode" id="zipcode" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">เบอร์โทร<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="phone" id="phone" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">มือถือ<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="tel" class="form-control" name="mobile" id="mobile" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">e-mail<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="email" class="form-control" name="email" id="email" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">รหัสผู้เสียภาษี<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="tax_id" id="tax_id" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Credit Terms<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">

                                    <select name="terms_id" id="terms_id" class="form-control" required>
                                        <?php
                                        $db = Connectdb::Databaseall();
                                        $dataterms = DB::connection('mysql')->select('SELECT name, id FROM '.$db['fsctaccount'].'.supplier_terms');
                                        ?>
                                        <option value="6">ไม่มี term</option>
                                        @foreach ($dataterms as $value)

                                            <option value="{{ $value->id}}">{{ $value->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">หมายเหตุ<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="Note" id="Note" required>
                                    <input type="hidden" name="status" id="status" value="0">
                                    <input type="hidden" name="createby" id="createby" >
                                    <input type="hidden" name="createdate" id="createdate" >
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">ประเภทของกิจการ<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input class="form-control" type="text" name="business_type" id="business_type" value="" required>

                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">ผลิตภัณฑ์หลัก<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input class="form-control" type="text" name="main_product" id="main_product" value="" required>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">รหัสเจ้าหนี้การค้า<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input class="form-control" type="text" id="codecreditor" name="codecreditor" value="" required>

                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">กลุ่มอุตสาหกรรมที่เป็นลูกค้า<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input class="form-control" type="text" name="main_industry" id="main_industry" value="" required>

                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                              <div class="form-group">
                                  <label class="col-xs-4 control-label">ปีที่ก่อตั้ง<i><span style="color: red">*</span></i></label>
                                  <div class="col-xs-6">
                                      <input class="form-control" type="number" placeholder="พ.ศ." name="establish_year" id="establish_year" value="" required>
                                  </div>
                                  <div class="col-xs-4">

                                  </div>
                              </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">ระบบบริหารที่เกี่ยวกับคุณภาพ (เช่น ISO 9001, TS 16949, GMP, TQM)<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-3">
                                    <select class="form-control" name="iso_system" id="iso_system" required>
                                        <option value>เลือก</option>
                                        <option value="1">มี</option>
                                        <option value="0">ไม่มี</option>
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">มาตรฐานผลิตภัณฑ์สินค้าอุตสาหกรรม  (เช่น  มอก. ,JIS, ASTM, UL, CE Mark)<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-3">
                                    <select class="form-control" name="product_standard" id="product_standard" required>
                                        <option value="">เลือก</option>
                                        <option value="1">มี</option>
                                        <option value="0">ไม่มี</option>
                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-6 control-label">ชิ้นงานตัวอย่างเพื่อพิจารณา<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <select class="form-control" name="example_product" id="example_product" required>
                                        <option value="">เลือก</option>
                                        <option value="1">มี</option>
                                        <option value="0">ไม่มี</option>
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">จำนวน<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input class="form-control" type="number" name="example_product_number" id="example_product_number" value="" required>
                                </div>
                                <div class="col-xs-2">
                                  <label class="col-xs-4 control-label">ชิ้น</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="col-xs-6 control-label">สามารถส่งมอบสินค้าใด้ภายใน<i><span style="color: red">*</span></i></label>
                              <div class="col-xs-6">
                                  <input class="form-control" type="number" name="number_delivery" id="number_delivery" value="" required>
                              </div>
                              <div class="col-xs-2">
                                <label class="col-xs-4 control-label">วัน</label>
                              </div>
                          </div>
                      </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">บุคคลที่สามารถติดต่อได้<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input class="form-control" type="number" name="agent" id="agent" value="" required>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                      <table class="table table-striped">
                          <thead>
                            <tr>
                              <th class="text-center">เกณฑ์และคะแนนในการคัดลือก</th>
                              <th class="text-center">คะแนนเป้าหมาย</th>
                              <th class="text-center">คะแนนที่ให้</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>1. ผลการตรวจสอบคุณภาพสินค้า</td>
                              <td class="text-center">30 คะแนน</td>
                              <td class="form-group"><input class="form-control" type="number" min="1" max="30" name="quality_point" id="quality_point" required placeholder="คะแนน" /></td>
                            </tr>
                            <tr>
                              <td>2. ด้านราคา</td>
                              <td class="text-center">20 คะแนน</td>
                              <td class="form-group"><input class="form-control" type="number" min="1" max="20" name="price_point" id="price_point" required placeholder="คะแนน" /></td>
                            </tr>
                            <tr>
                              <td>3. ด้านการส่งมอบ</td>
                              <td class="text-center">20 คะแนน</td>
                              <td class="form-group"><input class="form-control" type="number" min="1" max="20" name="delivery_point" id="delivery_point" required placeholder="คะแนน" /></td>
                            </tr>
                            <tr>
                              <td>4. ระยะเวลาในการให้เครดิต</td>
                              <td class="text-center">15 คะแนน</td>
                              <td class="form-group"><input class="form-control" type="number" min="1" max="15" name="credit_point" id="credit_point" required placeholder="คะแนน" /></td>
                            </tr>
                            <tr>
                              <td>5. ระบบคุณภาพ /ควบคุมกระบวนการ</td>
                              <td class="text-center">15 คะแนน</td>
                              <td class="form-group"><input class="form-control" type="number" min="1" max="15" name="manufacture_point" id="manufacture_point" required placeholder="คะแนน" /></td>
                            </tr>

                          </tbody>
                      </table>

                    </div>

                    <!-- add new -->

                  <!-- -->
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">

                        <div class="col-md-4">

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-xs-5 col-xs-offset-3">
                                    <button type="submit" id="Btn_save" class="btn btn-primary">Save</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div id="editstatus" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">สถานะ</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">

                    </div>
                    <div class="col-md-4">
                        <center>
                            <input type="hidden" id="idupdate" name="idupdate">
                            <select name="statusupdate" id="statusupdate" class="form-control">
                                <option value="0">รออนุมัติ</option>
                                <option value="1">อนุมัติ</option>
                                <option value="99">ยกเลิก</option>
                            </select>
                        </center>
                    </div>
                    <div class="col-md-4">

                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="savestatus()">บันทึก</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
