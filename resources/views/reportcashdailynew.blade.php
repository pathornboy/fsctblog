<?php

use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;
use  App\Api\DateTime;

?>
@include('headmenu')

<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>
<link rel="stylesheet" type="text/css" href="css/ui/jquery-ui.css">

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>

<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap2.min.js'></script>

<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">

<script type="text/javascript" src = 'js/report/cash.js'></script>
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet"/>  datepicker year bootstrap-->

<!-- <script src="js/code/highcharts.js"></script>
<script src="js/code/modules/exporting.js"></script>
<script src="js/code/modules/export-data.js"></script> -->



<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


<style>
    .ui-autocomplete-input {
        border: none;
        font-size: 14px;
        width: 225px;
        height: 24px;
        margin-bottom: 5px;
        padding-top: 2px;
        border: 1px solid #DDD !important;
        padding-top: 0px !important;
        z-index: 1511;
        position: relative;
    }
    .ui-menu .ui-menu-item a {
        font-size: 12px;
    }
    .ui-autocomplete {
        position: absolute;
        top: 0;
        left: 0;
        z-index: 1510 !important;
        float: left;
        display: none;
        min-width: 160px;
        width: 160px;
        padding: 4px 0;
        margin: 2px 0 0 0;
        list-style: none;
        background-color: #ffffff;
        border-color: #ccc;
        border-color: rgba(0, 0, 0, 0.2);
        border-style: solid;
        border-width: 1px;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding;
        background-clip: padding-box;
        *border-right-width: 2px;
        *border-bottom-width: 2px;
    }
    .ui-menu-item > a.ui-corner-all {
        display: block;
        padding: 3px 15px;
        clear: both;
        font-weight: normal;
        line-height: 18px;
        color: #555555;
        white-space: nowrap;
        text-decoration: none;
    }
    .ui-state-hover, .ui-state-active {
        color: #ffffff;
        text-decoration: none;
        background-color: #0088cc;
        border-radius: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        background-image: none;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-cog home-icon"></i>
                        <a href="#">รายงาน</a>
                    </li>
                    <li class="active">รายงานเงินสดย่อยรายวัน</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>

            <div class="box-body" style="overflow-x:auto;">
              <form action="searchreportcashdailynow" method="post">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-2">

                    </div>

                    <div class="col-md-3">
                        <p class="text-right">
                          ค้นหาสาขา
                        </p>
                    </div>
                    <div class="col-md-2">
                      <?php
                        $db = Connectdb::Databaseall();
                        $sql = 'SELECT '.$db['hr_base'].'.branch.*
                                FROM '.$db['hr_base'].'.branch
                                WHERE '.$db['hr_base'].'.branch.status = "1"';

                        $brcode = DB::connection('mysql')->select($sql);
                      ?>
                        <select name="branch_id" id="branch_id" class="form-control" required>
                          <option value="">เลือกสาขา</option>
                          <?php foreach ($brcode as $key => $value) { ?>
                              <option value="<?php echo $value->code_branch?>" <?php if(isset($query)){ if($branch_id==$value->code_branch){ echo "selected";} }?>><?php echo $value->name_branch?></option>
                          <?php } ?>
                        </select>
                    </div>

                    <div class="col-md-5">

                    </div>
                </div>

                <div class="row">
                    <br>
                </div>

                <div class="row">
                    <div class="col-md-2">

                    </div>
                    <div class="col-md-3">
                        <p class="text-right">
                         วันที่
                        </p>
                    </div>
                    <div class="col-md-2">
                          <input type="text" name="datepicker" id="datepicker" value="<?php if(isset($query)){ print_r($datepicker); }else{ echo date('d/m/Y');}?>" class="form-control datepicker" required readonly>
                    </div>

                    <div class="col-md-2">

                    </div>
                    <div class="col-md-3">

                    </div>
                </div>

                <div class="row">
                    <br>
                </div>

                <div class="row">
                  <div class="col-md-5">

                  </div>
                  <div class="col-md-3">
                    <input type="submit" class="btn btn-primary" value="ค้นหา">
                    <input type="reset" class="btn btn-danger">
                  </div>

                  <div class="col-md-12" align="right">
                  <?php   if(isset($query)){ ////echo $branch_id; ?>
                              <!-- <a href="excelreporttexinsurancecreditnote?branch_id=<?php //echo $branch_id ;?>&&datepickerstart=<?php //echo $datepicker2['start_date'];?>&&datepickerend=<?php  //echo $datepicker2['end_date'];?>">Download Excel xls</a> -->
                              <a href="excelreportcashdailynew?brcode=<?php echo $branch_id ;?>&&datepicker=<?php echo $datepicker2;?>" target="_blank"><img src="images/global/printall.png"></a>
                  <?php } ?>
                  </div>

                  <div class="col-md-4">

                  </div>

                </form>

              </div>

                  <div class="row">
                      <br>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <?php
                      if(isset($query)){
                          // echo "<pre>";
                          // print_r($data);
                          // print_r($dataenpo);
                          // print_r($dataatm);
                          // print_r($dataexpenses);
                          // exit;
                        ?>

                        <form action="configreportcashdailynow" onSubmit="if(!confirm('ยืนยันการทำรายการ?')){return false;}" method="post" class="form-horizontal">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <table class="table table-striped">
                           <thead>
                             <tr>
                               <th>วันที่</th>
                               <th>ชื่อลูกค้า</th>
                               <th>เลขที่บิล</th>
                               <th>ประเภท</th>
                               <th>เงินทอน</th>
                               <th>สินค้าตัดหาย</th>
                               <th>รายรับ</th>
                               <th>รายจ่าย</th>
                               <th>รับประกัน</th>
                               <th>คืนประกัน</th>
                               <th>คงเหลือ</th>
                             </tr>
                           </thead>
                           <tbody>

                           <?php  //ยอดยกมา

                             $brcode = $branch_id;
                             $datetime = $datepicker2;

                             $db = Connectdb::Databaseall();
                             $sql = 'SELECT * FROM '.$db['fsctaccount'].'.cash
                                     WHERE '.$db['fsctaccount'].'.cash.branch_id = "'.$brcode.'"
                                     AND '.$db['fsctaccount'].'.cash.time = "'.$datetime.'"
                                    ';

                             $dataresult = DB::connection('mysql')->select($sql);

                             $sumall =0;
                           ?>
                           <tr>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td ><?php echo '<span style="color: red;" />ยกยอดมา</span>'; ?></td>
                             <td><font color="red"><u>
                               <?php
                                 if($dataresult){
                                   // $grandtotal = $dataresult[0]->grandtotal;
                                   echo $dataresult[0]->grandtotal;
                                 }
                               ?>
                             </u></font></td>
                             <td></td>
                             <td></td>
                             <!-- <td></td>
                             <td></td> -->
                             <td></td>
                             <td></td>
                             <td></td>
                             <td><font color="red"><u>
                               <?php
                                 if($dataresult){
                                   $sumall = $sumall + $dataresult[0]->grandtotal;
                                   echo $dataresult[0]->grandtotal;
                                 }
                               ?>
                             </u></font></td>
                               <?php if($data){
                                 // echo "<pre>";
                                 // print_r($data);
                                 // exit;
                               ?>


                             <!-- <input type="hidden" name="branch_id[]" value="<?php //if($dataresult){echo $dataresult[0]->branch_id;} ?>" class="form-control"> -->

                             <?php } ?>

                             <input type="hidden" name="branch_id[]" value="<?php if($dataresult){echo $dataresult[0]->branch_id;} ?>" class="form-control">
                             <!-- <input type="hidden" name="time[]" value="<?php //echo $datetime ?>" class="form-control"> -->
                             <input type="hidden" name="time2[]" value="<?php echo date('Y-m-d',strtotime("+1 day")); ?>" class="form-control">

                           </tr>

                           <?php

                            $sumtotalloss = 0; //รวมค่าสินค้าตัดหาย
                            $sumtotalinsurance = 0; //รวมรับประกัน
                            $sumtotalinsurancepay = 0; //รวมคืนประกัน
                            $sumtotalall = 0; //รวมรายรับ
                            $sumcreditnoteall = 0; //รวมลดหนี้
                            $sumtotalpay = 0; //รวมรายจ่าย
                            $summdtotalall = 0; //รวม MD โอนมา
                            $summdtotalatm = 0; //รวมเงินถอน
                            $sumtotalmoney = 0; //เงินสำรองจ่าย

                            $sumtotallossbank = 0; //รวมค่าสินค้าตัดหาย (โอน)
                            $sumtotalallbank = 0; //รวมรายรับ (โอน)
                            $sumcreditnoteallbank = 0; //รวมลดหนี้ (โอน)
                            $sumtotalinsurancebank = 0; //รวมรับประกัน (โอน)
                            $sumtotalinsurancepaybank = 0; //รวมคืนประกัน (โอน)
                            $summdtotalallbank = 0; //รวม MD โอนมา (โอน)
                            $sumtotalpaybank = 0; //รวมรายจ่าย (โอน)
                            $summdtotalatmbank = 0; //รวมเงินถอน (โอน)

                            $sumtotalexpenses = 0; //ค่าใช้จ่าย(ที่ไม่ถือเป็นรายรับ)
                            $sumtotalmoneycash = 0; //คืนเงินสำรองจ่าย

                            $sumall1 = 0; // รวมเงินสด
                            $sumallmoney = 0; // รวมเงินสด + รวมเงินสำรองจ่าย
                            $sumall2 = 0; // รวมเงินสด + จัดซื้อเงินสด

                            $sumall3 = 0; // รวมเงินโอน
                            $sumall4 = 0; // รวมเงินโอน + จัดซื้อเงินโอน

                            $sumall5 = 0; // รวมเงิน ATM

                            $sumallatm = 0; //เงินถอน

                            $grandtotal = 0; //รวมยอดยกไป
                            $moneytotal = 0; //รวมนำฝาก

                            $sumtotalwht = 0;
                            $sumtotalwhtbank = 0;

                           foreach ($data as $key => $value) {
                           ?>

                           <tr>
                              <td><!--วันที่-->
                                 <?php echo ($value->datetimeinsert); ?>
                                 <!-- <input type="hidden" name="startdate[]" value="<?php //if($data){ echo $value->datetimeinsert;} ?>" class="form-control"> <!--วันที่-->
                              </td>
                              <td><!--ชื่อ - นามสกุล-->
                                 <?php
                                      $customerid = $value->log;
                                      $test = explode('=',$customerid);
                                      // echo $test[1];
                                      // exit;

                                      $modelcus = Maincenter::getdatacustomercash($test[1]);
                                      if($modelcus){
                                        echo ($modelcus[0]->name);
                                  ?>&nbsp
                                  <?php
                                        echo ($modelcus[0]->lastname);
                                      }
                                  ?>
                              </td>

                              <td><!--เลขที่บิล-->
                                <?php
                                if($value->typedoc == 0){ //รับค่าเช่า
                                  $modelra = Maincenter::getdatara($value->typereftax);
                                  if($modelra){
                                    echo ($modelra[0]->number_taxinvoice);
                                  }
                                }

                                if($value->typedoc == 1){ //รับค่าเช่าเพิ่มเติม
                                  $modelrn = Maincenter::getdatarn($value->typereftax);
                                  if($modelrn){
                                    echo ($modelrn[0]->number_taxinvoice);
                                  }
                                }

                                if($value->typedoc == 2){ //รับค่าเช่าของหาย
                                  $modelrl = Maincenter::getdatarl($value->typereftax);
                                  if($modelrl){
                                    echo ($modelrl[0]->number_taxinvoice);
                                  }
                                }

                                if($value->typedoc == 3){ //ลดหนี้
                                  $modelcn = Maincenter::getdatacn($value->typereftax);
                                  if($modelcn){
                                    echo ($modelcn[0]->number_taxinvoice);
                                  }
                                }

                                if($value->typedoc == 4){ //รับเงินประกัน
                                  $modelti = Maincenter::getdatati($value->typereftax);
                                  if($modelti){
                                    echo ($modelti[0]->number_taxinvoice);
                                  }
                                }

                                if($value->typedoc == 5){ //คืนเงินประกัน
                                  $modelci = Maincenter::getdataci($value->ref);
                                  if($modelci){
                                    echo ($modelci[0]->number_taxinvoice);
                                  }
                                }

                                if($value->typedoc == 6){ //รับค่าขนส่ง
                                  $modelrs = Maincenter::getdatars($value->typereftax);
                                  if($modelrs){
                                    echo ($modelrs[0]->number_taxinvoice);
                                  }
                                }

                                if($value->typedoc == 7){ //ลดหนี้กรณีพิเศษ
                                  $modelcs = Maincenter::getdatacs($value->typereftax);
                                  if($modelcs){
                                    echo ($modelcs[0]->number_taxinvoice);
                                  }
                                }

                                // if($value->typedoc == 8){ //หัก ณ ที่จ่าย
                                //   $modelwth = Maincenter::getdatawth($value->typereftax);
                                //   if($modelwth){
                                //     echo ($modelwth[0]->number_taxinvoice);
                                //   }
                                // }

                                if($value->typedoc == 9){ //รับค่าเช่าแบบย่อย
                                  $modelro = Maincenter::getdataro($value->typereftax);
                                  if($modelro){
                                    echo ($modelro[0]->number_taxinvoice);
                                  }
                                }

                                if($value->typedoc == 13){ //รับค่าขายของ
                                  $modelss = Maincenter::getdatass($value->typereftax);
                                  if($modelss){
                                    echo ($modelss[0]->bill_no);
                                  }
                                }

                                ?>
                              </td>

                              <td><!--ประเภท-->
                                <?php

                                  if($value->typedoc == 0){ //รับค่าเช่า
                                    // echo "<br>";
                                    echo '<span style="color: green;" />รับค่าเช่า</span>';
                                    // echo ($value->bill_rent);
                                  }
                                  if($value->typedoc == 1){ //รับค่าเช่าเพิ่มเติม
                                    // echo "<br>";
                                    echo '<span style="color: green;" />รับค่าเช่าเพิ่มเติม</span>';
                                    // echo ($value->bill_rent);
                                  }
                                  if($value->typedoc == 2){ //รับค่าเช่าของหาย
                                    // echo "<br>";
                                    echo '<span style="color: green;" />รับค่าเช่าของหาย</span>';
                                    // echo ($value->bill_rent);
                                  }
                                  if($value->typedoc == 3){ //ลดหนี้
                                    // echo "<br>";
                                    echo '<span style="color: red;" />ลดหนี้</span>';
                                    // echo ($value->bill_rent);
                                  }
                                  if($value->typedoc == 4){ //รับเงินประกัน
                                    // echo "<br>";
                                    echo '<span style="color: green;" />รับเงินประกัน</span>';
                                    // echo ($value->bill_rent);
                                  }
                                  if($value->typedoc == 5){ //คืนเงินประกัน
                                    // echo "<br>";
                                    echo '<span style="color: red;" />คืนเงินประกัน</span>';
                                    // echo ($value->bill_rent);
                                  }
                                  if($value->typedoc == 6){ //รับค่าขนส่ง
                                    // echo "<br>";
                                    echo '<span style="color: green;" />รับค่าขนส่ง</span>';
                                    // echo ($value->bill_rent);
                                  }
                                  if($value->typedoc == 7){ //ลดหนี้กรณีพิเศษ
                                    // echo "<br>";
                                    echo '<span style="color: red;" />ลดหนี้กรณีพิเศษ</span>';
                                    // echo ($value->bill_rent);
                                  }
                                  if($value->typedoc == 8){ //หัก ณ ที่จ่าย
                                    // echo "<br>";
                                    echo '<span style="color: red;" />หัก ณ ที่จ่าย</span>';
                                    // echo ($value->bill_rent);
                                  }
                                  if($value->typedoc == 9){ //รับค่าเช่าแบบย่อย
                                    // echo "<br>";
                                    echo '<span style="color: green;" />รับค่าเช่าแบบย่อย</span>';
                                    // echo ($value->bill_rent);
                                  }
                                  if($value->typedoc == 13){ //รับค่าขายของ
                                    // echo "<br>";
                                    echo '<span style="color: green;" />รับค่าขายของ</span>';
                                    // echo ($value->bill_rent);
                                  }

                                ?>
                              </td>
                              <td></td>
                              <td><font color="green"> <!--สินค้าตัดหาย-->
                                <?php

                                  if($value->typedoc == 2){  //สินค้าตัดหาย
                                    if($value->typetranfer == '1'){ //รับเงินสด
                                      // $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                                      $sumtotalloss = $sumtotalloss +  $value->money;
                                      echo number_format($value->money,2);
                                    }
                                    elseif ($value->typetranfer == '2'){ //รับเงินโอน
                                      echo "[";
                                      $sumtotallossbank = $sumtotallossbank +  $value->money;
                                      echo number_format ($value->money,2);
                                      echo "]";
                                    }
                                    else{
                                      // $sumsubtotalinput = $sumsubtotalinput + 0;
                                      $sumtotalloss = $sumtotalloss + 0;
                                      echo "0";
                                    }
                                  }

                                ?>
                              </td></font>
                              <td><!--<font color="green"> รายรับ-->
                               <?php

                                  if($value->typedoc == 0 || $value->typedoc == 1 || $value->typedoc == 6 || $value->typedoc == 9 || $value->typedoc == 13){
                                    if($value->typetranfer == '1'){ //รับเงินสด
                                      // $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                                      $sumtotalall = $sumtotalall +  $value->money;
                                      echo '<span style="color: green;" />';
                                      echo number_format($value->money,2);
                                      echo '</span>';
                                    }
                                    elseif ($value->typetranfer == '2'){ //รับเงินโอน
                                      echo '<span style="color: green;" />';
                                      echo "[";
                                      $sumtotalallbank = $sumtotalallbank +  $value->money;
                                      echo number_format ($value->money,2);
                                      echo "]";
                                      echo '</span>';
                                    }
                                    else{
                                      // $sumsubtotalinput = $sumsubtotalinput + 0;
                                      $sumtotalall = $sumtotalall + 0;
                                      echo '<span style="color: green;" />';
                                      echo "0";
                                      echo '</span>';
                                    }
                                  }

                                  // ลดหนี้
                                  if($value->typedoc == 3 || $value->typedoc == 7){
                                    if($value->typetranfer == '1'){ //รับเงินสด
                                      // $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                                      $sumcreditnoteall = $sumcreditnoteall +  $value->money;
                                      echo '<span style="color: red;" />';
                                      echo number_format (-$value->money,2);
                                      echo '</span>';
                                    }
                                    elseif ($value->typetranfer == '2'){ //รับเงินโอน
                                      echo '<span style="color: red;" />';
                                      echo "[";
                                      $sumcreditnoteallbank = $sumcreditnoteallbank +  $value->money;
                                      echo number_format (-$value->money,2);
                                      echo "]";
                                      echo '</span>';
                                    }
                                    else{
                                      // $sumsubtotalinput = $sumsubtotalinput + 0;
                                      $sumcreditnoteall = $sumcreditnoteall + 0;
                                      echo '<span style="color: red;" />';
                                      echo "0";
                                      echo '</span>';
                                    }
                                  }

                              ?>
                              </td></font>
                              <td><font color="red"><!--รายจ่าย-->
                                <?php
                                  if($value->typedoc == 8){  //หัก ณ ที่จ่าย
                                    if($value->typetranfer == '2') //หัก ณ ที่จ่าย (โอน)
                                    {
                                      echo '<span style="color: red;" />';
                                      echo "[";
                                      $sumtotalwhtbank = $sumtotalwhtbank +  $value->money;
                                      echo ($value->money);
                                      echo "]";
                                      echo '</span>';
                                    }
                                    else //หัก ณ ที่จ่าย (เงินสด)
                                    {
                                      $sumtotalwht = $sumtotalwht +  $value->money;
                                      echo '<span style="color: red;" />';
                                      echo ($value->money);
                                      echo '</span>';
                                    }
                                  }

                                ?>
                              </td></font>
                              <td><!--รับประกัน-->
                                <?php

                                  if($value->typedoc == 4){  //รับประกัน
                                    if($value->typetranfer == '2') //รับประกัน (โอน)
                                    {
                                      echo '<span style="color: green;" />';
                                      echo "[";
                                      $sumtotalinsurancebank = $sumtotalinsurancebank +  $value->money;
                                      echo ($value->money);
                                      echo "]";
                                      echo '</span>';
                                    }
                                    else //รับประกัน (เงินสด)
                                    {
                                      $sumtotalinsurance = $sumtotalinsurance +  $value->money;
                                      echo '<span style="color: green;" />';
                                      echo ($value->money);
                                      echo '</span>';
                                    }
                                  }

                                ?>
                              </td>
                              <td><!--คืนประกัน-->
                                <?php

                                if($value->typedoc == 5){  //คืนประกัน
                                  if($value->typetranfer == '2') //คืนประกัน (โอน)
                                  {
                                    echo '<span style="color: red;" />';
                                    echo "[";
                                    $sumtotalinsurancepaybank = $sumtotalinsurancepaybank +  $value->money;
                                    echo ($value->money);
                                    echo "]";
                                    echo '</span>';
                                  }
                                  else //คืนประกัน (เงินสด)
                                  {
                                    $sumtotalinsurancepay = $sumtotalinsurancepay +  $value->money;
                                    echo '<span style="color: red;" />';
                                    echo ($value->money);
                                    echo '</span>';
                                  }
                                }
                                ?>
                              </td>
                              <td><!--คงเหลือ-->
                                <?php
                                  $sumall1 = ((((($sumtotalloss + $sumtotalall)- ($sumcreditnoteall)) + $sumtotalinsurance) - $sumtotalwht) - $sumtotalinsurancepay ) + $sumall; //เงินสด
                                  echo number_format($sumall1,2);

                                  // $sumall3 = ((($sumtotallossbank + $sumtotalallbank)- ($sumcreditnoteallbank)) + $sumtotalinsurancebank); //เงินโอน
                                  // echo $dataresult[0]->grandtotal;
                                ?>
                              </td>
                           </tr>
                           <?php  }  ?>
                           <input type="hidden" name="startdate[]" value="<?php echo date('Y-m-d'); ?>" class="form-control"> <!--วันที่-->

                           <?php
                           foreach ($dataatm as $key3 => $value3) {  //เงินถอน
                           ?>

                           <tr>
                              <td><!--วันที่-->
                                 <?php echo ($value3->datetimeinsert); ?>
                                 <!-- <input type="hidden" name="startdate[]" value="<?php  //if($dataatm){ echo $value3->datetimeinsert; } ?>" class="form-control"> -->
                              </td>
                              <td><?php echo $value3->log; ?></td><!--ชื่อ - นามสกุล-->

                              <td><?php  ?></td><!--เลขที่บิล-->

                              <td><!--ประเภท-->
                                <?php
                                  if($value3->typedoc == 11){ //เงินถอน
                                    echo '<span style="color: green;" />เงินถอน</span>';
                                  }
                                ?>
                              </td>
                              <td><font color="green"> <!--เงินถอน-->
                                <?php
                                if($value3->typedoc == 11){
                                  if($value3->typetranfer == '1'){ //รับเงินสด
                                    // $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                                    $summdtotalatm = $summdtotalatm +  $value3->money;
                                    echo number_format($value3->money,2);
                                  }
                                  elseif ($value3->typetranfer == '2'){ //รับเงินโอน
                                    echo "[";
                                    $summdtotalatmbank = $summdtotalatmbank +  $value3->money;
                                    echo number_format ($value3->money,2);
                                    echo "]";
                                  }
                                  else{
                                    // $sumsubtotalinput = $sumsubtotalinput + 0;
                                    $summdtotalatm = $summdtotalatm + 0;
                                    echo "0";
                                  }
                                }
                                ?>
                              </td>
                              <td><font color="green"></td></font><!--สินค้าตัดหาย-->
                              <td><font color="green"></td></font><!--//รายรับ-->
                              <td><font color="red"></td></font><!--รายจ่าย-->
                              <td></td><!--รับประกัน-->
                              <td></td><!--คืนประกัน-->
                              <td><!--คงเหลือ-->
                                <?php
                                  // $sumallatm = $summdtotalatm + $sumall1;
                                  // echo number_format($sumallatm,2);

                                  if($sumall1 != 0){
                                    $sumall1 = $sumall1 + $value3->money;
                                  }else if ($sumall1 == 0) {
                                    $sumall1 = $sumall1 + $value3->money + $sumall;
                                  }
                                  // $sumall1 = $sumall1 + $value3->money;
                                  echo number_format($sumall1,2);
                                ?>
                              </td>
                           </tr>
                           <?php  }  ?>
                           <!-- <input type="hidden" name="startdate[]" value="<?php  //if($dataatm){ echo $value3->datetimeinsert; } ?>" class="form-control"> -->

                           <?php
                           foreach ($dataenmoney as $key5 => $value5) {  //สำรองจ่าย
                           ?>

                           <tr>
                              <td><!--วันที่-->
                                 <?php echo ($value5->datetimeinsert); ?>
                                 <!-- <input type="hidden" name="startdate[]" value="<?php  //if($dataenmoney){echo $value5->datetimeinsert ;} ?>" class="form-control"> -->
                              </td>
                              <td><!--ชื่อ - นามสกุล-->
                                 <?php echo $value5->log; ?>
                              </td>

                              <td><?php  ?></td><!--เลขที่บิล-->

                              <td><!--ประเภท-->
                                <?php

                                  if($value5->typedoc == 14){ //เงินสำรองจ่าย
                                    echo '<span style="color: red;" />เงินสำรองจ่าย</span>';
                                  }

                                ?>
                              </td>
                              <td><font color="green"> </td><!--MD โอนมา-->
                              <td><font color="green"> </td></font><!--สินค้าตัดหาย-->
                              <td><font color="green"> </td></font><!--//รายรับ-->
                              <td><font color="red"> <!--รายจ่าย-->
                                <?php
                                if($value5->typedoc == 14){
                                  if($value5->typetranfer == '1'){ //รับเงินสด
                                    // $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                                    $sumtotalmoney = $sumtotalmoney +  $value5->money;
                                    echo number_format($value5->money,2);
                                  }
                                  elseif ($value5->typetranfer == '2'){ //รับเงินโอน
                                    echo "[";
                                    // $sumtotalmoneybank = $sumtotalmoneybank +  $value5->money;
                                    echo number_format ($value5->money,2);
                                    echo "]";
                                  }
                                  else{
                                    // $sumsubtotalinput = $sumsubtotalinput + 0;
                                    $sumtotalmoney = $sumtotalmoney + 0;
                                    echo "0";
                                  }
                                }
                                ?>
                              </td></font>
                              <td></td><!--รับประกัน-->
                              <td></td><!--คืนประกัน-->
                              <td><!--คงเหลือ-->
                                <?php
                                  // if($dataatm){
                                  //   $sumallmoney = ($summdtotalall + $sumallatm) - $sumtotalmoney;
                                  // }else {
                                  //   $sumallmoney = ($summdtotalall + $sumall1) - $sumtotalmoney;
                                  // }
                                  // echo number_format($sumallmoney,2);

                                  if($sumall1 != 0){
                                    if($value5->typetranfer == '1'){
                                      $sumall1 = $sumall1 - $value5->money;
                                    }
                                  }else if ($sumall1 == 0) {
                                    if($value5->typetranfer == '1'){
                                    $sumall1 = $sumall1 - $value5->money + $sumall;
                                    }
                                  }

                                  // $sumall1 = $sumall1 - $value5->money;
                                  echo number_format($sumall1,2);
                                ?>
                              </td>
                           </tr>
                           <?php  }  ?>
                           <!-- <input type="hidden" name="startdate[]" value="<?php  //if($dataenmoney){echo $value5->datetimeinsert ;} ?>" class="form-control"> -->

                           <?php
                           foreach ($dataexpenses as $key6 => $value6) {  //สำรองจ่าย
                           ?>

                           <tr>
                              <td><!--วันที่-->
                                 <?php echo ($value6->datetimeinsert); ?>
                                 <!-- <input type="hidden" name="startdate[]" value="<?php  //if($dataexpenses){echo $value6->datetimeinsert;} ?>" class="form-control"> -->
                              </td>
                              <td><!--ชื่อ - นามสกุล-->
                                 <?php echo $value6->log; ?>
                              </td>

                              <td><?php  ?></td><!--เลขที่บิล-->

                              <td><!--ประเภท-->
                                <?php

                                  if($value6->typedoc == 15){ //เงินสำรองจ่าย
                                    echo '<span style="color: red;" />เงินสำรองจ่าย</span>';
                                  }

                                ?>
                              </td>
                              <td><font color="green"> </td><!--MD โอนมา-->
                              <td><font color="green"> </td></font><!--สินค้าตัดหาย-->
                              <td><font color="green"> </td></font><!--//รายรับ-->
                              <td><font color="green"> <!--รายจ่าย-->
                                <?php
                                if($value6->typedoc == 15){
                                  if($value6->typetranfer == '1'){ //รับเงินสด
                                    // $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                                    $sumtotalexpenses = $sumtotalexpenses +  $value6->money;
                                    echo number_format($value6->money,2);
                                  }
                                  elseif ($value6->typetranfer == '2'){ //รับเงินโอน
                                    echo "[";
                                    // $sumtotalmoneybank = $sumtotalmoneybank +  $value6->money;
                                    echo number_format ($value6->money,2);
                                    echo "]";
                                  }
                                  else{
                                    // $sumsubtotalinput = $sumsubtotalinput + 0;
                                    $sumtotalexpenses = $sumtotalexpenses + 0;
                                    echo "0";
                                  }
                                }
                                ?>
                              </td></font>
                              <td></td><!--รับประกัน-->
                              <td></td><!--คืนประกัน-->
                              <td><!--คงเหลือ-->
                                <?php
                                  // if($dataatm){
                                  //   $sumallmoney = ($summdtotalall + $sumallatm) - $sumtotalmoney;
                                  // }else {
                                  //   $sumallmoney = ($summdtotalall + $sumall1) - $sumtotalmoney;
                                  // }
                                  // echo number_format($sumallmoney,2);

                                  if($sumall1 != 0){
                                    $sumall1 = $sumall1 + $value6->money;
                                  }else if ($sumall1 == 0) {
                                    $sumall1 = $sumall1 + $value6->money + $sumall;
                                  }

                                  // $sumall1 = $sumall1 + $value6->money;
                                  echo number_format($sumall1,2);
                                ?>
                              </td>
                           </tr>
                           <?php  }  ?>
                           <!-- <input type="hidden" name="startdate[]" value="<?php  //if($dataexpenses){echo $value6->datetimeinsert;} ?>" class="form-control"> -->

                           <?php
                           foreach ($dataenmoneycash as $key7 => $value7) {  //คืนเงินสำรองจ่าย
                           ?>

                           <tr>
                              <td><!--วันที่-->
                                 <?php echo ($value7->datetimeinsert); ?>
                                 <!-- <input type="hidden" name="startdate[]" value="<?php  //if($dataexpenses){echo $value7->datetimeinsert;} ?>" class="form-control"> -->
                              </td>
                              <td><!--ชื่อ - นามสกุล-->
                                 <?php echo $value7->log; ?>
                              </td>

                              <td><?php  ?></td><!--เลขที่บิล-->

                              <td><!--ประเภท-->
                                <?php

                                  if($value7->typedoc == 16){ //คืนเงินสำรองจ่าย
                                    echo '<span style="color: green;" />คืนเงินสำรองจ่าย</span>';
                                  }

                                ?>
                              </td>
                              <td><font color="green">
                                <?php
                                if($value7->typedoc == 16){
                                  if($value7->typetranfer == '1'){ //รับเงินสด
                                    // $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                                    $sumtotalmoneycash = $sumtotalmoneycash +  $value7->money;
                                    echo number_format($value7->money,2);
                                  }
                                  elseif ($value7->typetranfer == '2'){ //รับเงินโอน
                                    echo "[";
                                    // $sumtotalmoneybank = $sumtotalmoneybank +  $value7->money;
                                    echo number_format ($value7->money,2);
                                    echo "]";
                                  }
                                  else{
                                    // $sumsubtotalinput = $sumsubtotalinput + 0;
                                    $sumtotalmoneycash = $sumtotalmoneycash + 0;
                                    echo "0";
                                  }
                                }
                                ?>
                              </td><!--MD โอนมา-->
                              <td><font color="green"> </td></font><!--สินค้าตัดหาย-->
                              <td><font color="green"> </td></font><!--//รายรับ-->
                              <td><font color="green"> <!--รายจ่าย-->
                                <?php
                                // if($value7->typedoc == 15){
                                //   if($value7->typetranfer == '1'){ //รับเงินสด
                                //     // $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                                //     $sumtotalexpenses = $sumtotalexpenses +  $value7->money;
                                //     echo number_format($value7->money,2);
                                //   }
                                //   elseif ($value7->typetranfer == '2'){ //รับเงินโอน
                                //     echo "[";
                                //     // $sumtotalmoneybank = $sumtotalmoneybank +  $value7->money;
                                //     echo number_format ($value7->money,2);
                                //     echo "]";
                                //   }
                                //   else{
                                //     // $sumsubtotalinput = $sumsubtotalinput + 0;
                                //     $sumtotalexpenses = $sumtotalexpenses + 0;
                                //     echo "0";
                                //   }
                                // }
                                ?>
                              </td></font>
                              <td></td><!--รับประกัน-->
                              <td></td><!--คืนประกัน-->
                              <td><!--คงเหลือ-->
                                <?php
                                  // if($dataatm){
                                  //   $sumallmoney = ($summdtotalall + $sumallatm) - $sumtotalmoney;
                                  // }else {
                                  //   $sumallmoney = ($summdtotalall + $sumall1) - $sumtotalmoney;
                                  // }
                                  // echo number_format($sumallmoney,2);

                                  if($sumall1 != 0){
                                    $sumall1 = $sumall1 + $value7->money;
                                  }else if ($sumall1 == 0) {
                                    $sumall1 = $sumall1 + $value7->money + $sumall;
                                  }

                                  // $sumall1 = $sumall1 + $value7->money;
                                  echo number_format($sumall1,2);
                                ?>
                              </td>
                           </tr>
                           <?php  }  ?>

                           <?php
                           foreach ($dataenpo as $key2 => $value2) {
                           ?>

                           <tr>
                              <td><!--วันที่-->
                                 <?php echo ($value2->datetimeinsert); ?>
                                 <!-- <input type="hidden" name="startdate[]" value="<?php //if($dataenpo){ echo $value2->datetimeinsert;} ?>" class="form-control"> -->
                              </td>
                              <td><!--ชื่อ - นามสกุล-->
                                 <?php
                                      $poid = $value2->log;
                                      $test = explode('ใบ',$poid);
                                      // echo $test[1];
                                      // exit;

                                      $modelpoid = Maincenter::getdatapodetail($value2->ref);
                                      // echo "<pre>";
                                      // print_r($modelpoid);
                                      // exit;
                                      ?>
                                      <b><?php echo $test[1]; ?></b>
                                  <?php
                                      foreach ($modelpoid as $key4 => $value4) {
                                        $po = $value4->list;
                                        $testpo = explode('__',$po);
                                        // echo $po[1];
                                        // echo $testpo[1];
                                        // exit;

                                        echo "<br>";
                                        echo "- ";
                                        // echo $testpo[1]." = ".($value4->total)." บาท";
                                        echo $po." = ".($value4->total)." บาท";
                                      }
                                  ?>
                                </td>

                              <td><?php echo $test[1]; ?></td><!--เลขที่บิล-->

                              <td><!--ประเภท-->
                                <?php

                                  if($value2->typedoc == 10){ //จัดซื้อ PO
                                    // echo "<br>";
                                    echo '<span style="color: red;" />จัดซื้อ PO</span>';
                                    // echo ($value2->bill_rent);
                                  }

                                  if($value2->typedoc == 12){ //MD โอนมา
                                    // echo "<br>";
                                    echo '<span style="color: green;" />MD โอนมา</span>';
                                    // echo ($value2->bill_rent);
                                  }

                                ?>
                              </td>
                              <td><font color="green"> <!--MD โอนมา-->
                                <?php
                                if($value2->typedoc == 12){
                                  if($value2->typetranfer == '1'){ //รับเงินสด
                                    // $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                                    $summdtotalall = $summdtotalall +  $value2->money;
                                    echo number_format($value2->money,2);
                                  }
                                  elseif ($value2->typetranfer == '2'){ //รับเงินโอน
                                    echo "[";
                                    $summdtotalallbank = $summdtotalallbank +  $value2->money;
                                    echo number_format ($value2->money,2);
                                    echo "]";
                                  }
                                  else{
                                    // $sumsubtotalinput = $sumsubtotalinput + 0;
                                    $summdtotalall = $summdtotalall + 0;
                                    echo "0";
                                  }
                                }
                                ?>
                              </td>
                              <td><font color="green"> </td></font><!--สินค้าตัดหาย-->
                              <td><font color="green"> </td></font><!--//รายรับ-->
                              </td></font>
                              <td><font color="red"> <!--รายจ่าย-->
                                <?php
                                if($value2->typedoc == 10){
                                  if($value2->typetranfer == '1'){ //รับเงินสด
                                    // $sumsubtotalinput = $sumsubtotalinput + $result[0]->total;
                                    $sumtotalpay = $sumtotalpay +  $value2->money;
                                    echo number_format($value2->money,2);
                                  }
                                  elseif ($value2->typetranfer == '2'){ //รับเงินโอน
                                    echo "[";
                                    $sumtotalpaybank = $sumtotalpaybank +  $value2->money;
                                    echo number_format ($value2->money,2);
                                    echo "]";
                                  }
                                  else{
                                    // $sumsubtotalinput = $sumsubtotalinput + 0;
                                    $sumtotalpay = $sumtotalpay + 0;
                                    echo "0";
                                  }
                                }
                                ?>
                              </td></font>
                              <td></td><!--รับประกัน-->
                              <td></td><!--คืนประกัน-->
                              <td><!--คงเหลือ-->
                                <?php
                                  // if($dataatm){
                                  //   $sumall5 = $sumallatm - $sumtotalpay;
                                  //   if($dataenmoney){
                                  //     $sumall2 = $sumallmoney - $sumtotalpay;
                                  //   }
                                  //   else {
                                  //     $sumall2 = $sumall5;
                                  //   }
                                  // }
                                  // else if($dataenmoney){
                                  //     $sumall2 = $sumallmoney - $sumtotalpay;
                                  //   }
                                  // else {
                                  //     $sumall2 = $sumall1 - $sumtotalpay;
                                  // }
                                  // echo number_format($sumall2,2);

                                  // if($sumall1 != 0){
                                  //   $sumall1 = $sumall1 + $value3->money;
                                  // }else if ($sumall1 == 0) {
                                  //   $sumall1 = $sumall1 + $value3->money + $sumall;
                                  // }

                                  if($value2->typedoc == 10){
                                    if($value2->typetranfer == '1'){
                                       $sumall1 = $sumall1 - $value2->money;
                                       // if($sumall1 != 0){
                                       //   $sumall1 = $sumall1 + $value2->money;
                                       // }else if ($sumall1 == 0) {
                                       //   $sumall1 = $sumall1 + $value2->money + $sumall;
                                       // }
                                    }
                                    // echo number_format($sumtotalpay,2);
                                    // echo number_format($sumall1,2);
                                  }else {
                                    $sumall1 = $sumall1 - $summdtotalall;
                                    // echo number_format($sumall1,2);
                                  }

                                  echo number_format($sumall1,2);
                                ?>
                              </td>
                           </tr>
                           <?php  }  ?>
                           <!-- <input type="hidden" name="startdate[]" value="<?php //if($dataenpo){ echo $value2->datetimeinsert;} ?>" class="form-control"> -->


                           <!-- รวม -->
                           <tr>
                             <td colspan="4" align="right"><b>รวมเงินสด</b></td>
                             <td></td> <!--รวมเงินทอน-->
                             <td style="color: green;"><?php echo number_format($sumtotalloss,2); ?></td> <!--รวมค่าสินค้าตัดหาย-->
                             <td style="color: green;"><?php echo number_format($sumtotalall,2); ?></td> <!--รวมรายรับ-->
                             <td style="color: red;"><?php echo number_format($sumtotalpay+$sumtotalmoney+$sumtotalwht,2); ?></td> <!--รวมรายจ่าย-->
                             <td style="color: green;"><?php echo number_format($sumtotalinsurance,2); ?></td> <!--รวมรับประกัน-->
                             <td style="color: red;"><?php echo number_format($sumtotalinsurancepay,2); ?></td> <!--รวมคืนประกัน-->
                             <td><u>
                             <?php
                             // if($dataenpo){
                             //   echo number_format($sumall2,2);}
                             // else if($dataenmoney) {
                             //   echo number_format($sumallmoney,2);}
                             // else if($dataatm) {
                             //   echo number_format($sumallatm,2);}
                             // else {
                             //   echo number_format($sumall1,2);}
                             // }
                             echo number_format($sumall1,2);
                               ?>
                             </u></td> <!--ยอดคงเหลือ-->
                           </tr>

                           <tr>
                             <td colspan="8" align="right"></td>
                             <td></td>
                             <td></td>
                             <td></td>
                           </tr>

                           <tr>
                             <td colspan="2" align="right"><b>รวมเงินสด</b></td>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td colspan="3" align="right"><b>รวมเงินโอน</b></td>
                             <td></td>
                             <td></td>
                             <td></td>
                           </tr>

                           <!--Test-->
                           <tr>
                             <td colspan="1" align="right"><b>รวมสินค้าตัดหาย</b></td>
                             <td style="color: green;"><center><?php echo number_format($sumtotalloss,2); ?></center></td>
                             <!-- <td style="color: green;"><?php //echo number_format($sumtotallossbank,2); ?></td> -->
                             <td>บาท</td>
                             <td colspan="3" align="right"><b>รวมสินค้าตัดหาย</b></td>
                             <td colspan="2" align="center" style="color: green;"><?php echo number_format($sumtotallossbank,2); ?></td>
                             <td>บาท</td>
                             <td></td>
                             <td></td>
                           <tr>

                           <tr>
                             <td colspan="1" align="right"><b>รวมรายรับ</b></td>
                             <td style="color: green;"><center><?php echo number_format($sumtotalall,2); ?></center></td>
                             <!-- <td style="color: green;"><?php //echo number_format($sumtotalallbank,2); ?></td> -->
                             <td>บาท</td>
                             <td colspan="3" align="right"><b>รวมรายรับ</b></td>
                             <td colspan="2" align="center" style="color: green;"><?php echo number_format($sumtotalallbank,2); ?></td>
                             <td>บาท</td>
                             <td></td>
                             <td></td>
                            <tr>

                            <tr>
                              <td colspan="1" align="right"><b>รวมรายจ่าย</b></td>
                              <td style="color: red;"><center><?php echo number_format($sumtotalpay+$sumtotalmoney+$sumtotalwht,2); ?></center></td>
                              <!-- <td></td> -->
                              <td>บาท</td>
                              <td colspan="3" align="right"><b>รวมรายจ่าย</b></td>
                              <td colspan="2" align="center" style="color: red;"><?php echo number_format($sumtotalpaybank,2); ?></td>
                              <td>บาท</td>
                              <td></td>
                              <td></td>
                             <tr>

                             <tr>
                               <td colspan="1" align="right"><b>รวมรับประกัน</b></td>
                               <td style="color: green;"><center><?php echo number_format($sumtotalinsurance,2); ?></center></td>
                               <!-- <td style="color: green;"><?php //echo number_format($sumtotalinsurancebank,2); ?></td> -->
                               <td>บาท</td>
                               <td colspan="3" align="right"><b>รวมรับประกัน</b></td>
                               <td colspan="2" align="center" style="color: green;"><?php echo number_format($sumtotalinsurancebank,2); ?></td>
                               <td>บาท</td>
                               <td></td>
                               <td></td>
                            <tr>

                            <tr>
                              <td colspan="1" align="right"><b>รวมคืนประกัน</b></td>
                              <td style="color: red;"><center><?php echo number_format($sumtotalinsurancepay,2); ?></center></td>
                              <!-- <td style="color: red;"><?php //echo number_format($sumtotalinsurancepaybank,2); ?></td> -->
                              <td>บาท</td>
                              <td colspan="3" align="right"><b>รวมคืนประกัน</b></td>
                              <td colspan="2" align="center" style="color: red;"><?php echo number_format($sumtotalinsurancepaybank,2); ?></td>
                              <td>บาท</td>
                              <td></td>
                              <td></td>
                            <tr>

                            <tr>
                               <td colspan="1" align="right"><b>ยอดคงเหลือ</b></td>
                               <td><u><center><?php //if($dataenpo){echo number_format($sumall2,2);}else {echo number_format($sumallatm,2);}
                               // if($dataenpo){
                               //   echo number_format($sumall2,2);}
                               // else if($dataenmoney) {
                               //   echo number_format($sumallmoney,2);}
                               // else if($dataatm) {
                               //   echo number_format($sumallatm,2);}
                               // else {
                               //   echo number_format($sumall1,2);}
                               echo number_format($sumall1,2);
                               ?></u></center></td>
                               <!-- <td><u><?php //if($dataenpo){echo number_format($sumall2,2);}else {echo number_format($sumtotallossbank + $sumtotalallbank,2);} ?></u></td> -->
                               <td>บาท</td>
                               <td colspan="3" align="right"><b></b></td>
                               <td colspan="2" align="center" style="color: red;"><?php ?></td>
                               <td></td>
                               <td></td>
                               <td></td>
                            </tr>

                            <tr>
                              <td colspan="8" align="right"></td>
                              <td></td>
                              <td></td>
                              <td></td>
                            </tr>

                            <!--ลองคำนวณ-->
                            <tr>
                               <td colspan="2" align="right"><b>ยอดเงินสดที่ต้องนำฝากในแต่ละวัน</b></td>
                               <td></td>
                               <td></td>
                               <td></td>
                               <td colspan="3" align="right">
                               <?php if($datacheck){ ?>
                               <b>จำนวนเงินสดที่นำฝากธนาคาร</b>
                               <?php } ?>
                               </td>
                               <td></td>
                               <td></td>
                               <td></td>
                            </tr>
                            <tr>
                               <td><b>ยอดคงเหลือ</b></td>
                               <td><center><?php //if($dataenpo){echo number_format($sumall2,2);}else {echo number_format($sumallatm,2);}
                               // if($dataenpo){
                               //   echo number_format($sumall2,2);}
                               // else if($dataenmoney) {
                               //   echo number_format($sumallmoney,2);}
                               // else if($dataatm) {
                               //   echo number_format($sumallatm,2);}
                               // else {
                               //   echo number_format($sumall1,2);}
                               echo number_format($sumall1,2);
                               ?></center></td>
                               <td>บาท</td>
                               <td></td>
                               <td colspan="2" align="right">
                                 <?php if($datacheck){ ?>
                                      <b>เงินที่นำฝากธนาคาร</b>
                                 <?php }?>
                               </td>
                               <td colspan="2" align="right"><center>
                               <?php if($datacheck){
                                   echo number_format ($datacheck[0]->total,2);
                               }
                               ?></center></td>
                               <td>
                                 <?php if($datacheck){ ?>
                                      บาท
                                 <?php } ?>
                               </td>
                               <td></td>
                               <td></td>
                            <tr>
                            <tr>
                               <td><b>ยอดยกไป</b></td>
                               <td><center><?php
                               // if($dataresult){
                               //   if($dataenmoney){
                               //     echo number_format ($dataresult[0]->grandtotal - $sumtotalmoney,2);
                               //   }else {
                               //     echo number_format ($dataresult[0]->grandtotal,2);
                               //   }
                               // }

                               if($dataresult){
                                 if($dataenmoney){
                                   $grandtotal = ($dataresult[0]->grandtotal - $sumtotalmoney);
                                   // echo ($dataresult[0]->grandtotal - $sumtotalmoney);
                                 }else {
                                   $grandtotal = $dataresult[0]->grandtotal;
                                   // echo ($dataresult[0]->grandtotal);
                                 }

                                 if($dataenmoneycash){
                                   $grandtotal = $grandtotal + $sumtotalmoneycash;
                                 }
                                 echo number_format ($grandtotal,2);

                               }
                               ?></center></td>
                               <td>บาท</td>
                               <td colspan="8" align="right">
                               <input type="hidden" name="grandtotal[]" value="<?php
                               if($dataresult){
                                 if($dataenmoney){
                                   $grandtotal = ($dataresult[0]->grandtotal - $sumtotalmoney);
                                   // echo ($dataresult[0]->grandtotal - $sumtotalmoney);
                                 }else {
                                   $grandtotal = $dataresult[0]->grandtotal;
                                   // echo ($dataresult[0]->grandtotal);
                                 }

                                 if($dataenmoneycash){
                                   $grandtotal = $grandtotal + $sumtotalmoneycash;
                                 }
                                 echo $grandtotal;
                               }
                               ?>" class="form-control"> <!--ยอดยกมา-->


                            <tr>
                            <tr>
                               <td><b>ยอดที่ต้องนำฝาก</b></td>
                               <td><center><?php //if($dataresult){ if($dataenpo){echo number_format($sumall2 - $dataresult[0]->grandtotal,2);}else {echo number_format($sumallatm - $dataresult[0]->grandtotal,2);} }
                               // if($dataresult){
                               //     if($dataenpo){
                               //       echo number_format($sumall2 - $dataresult[0]->grandtotal,2);}
                               //     else if($dataenmoney) {
                               //       echo number_format($sumallmoney - $dataresult[0]->grandtotal,2);}
                               //     else if($dataatm) {
                               //       echo number_format($sumallatm - $dataresult[0]->grandtotal,2);}
                               //     else {
                               //       echo number_format($sumall1 - $dataresult[0]->grandtotal,2);}
                               // }


                               // $moneytotal = $sumtotalloss + $sumtotalall + $sumtotalinsurance;
                               // echo number_format ($moneytotal,2);


                               if($dataresult){
                                 if($dataenmoney){

                                  $moneytotal = $sumall1 - $dataresult[0]->grandtotal + $sumtotalmoney;
                                  // echo number_format ($moneytotal,2);
                                  // echo number_format ($sumall1 - $dataresult[0]->grandtotal + $sumtotalmoney,2);
                                }else {

                                  $moneytotal = $sumall1 - $dataresult[0]->grandtotal;
                                  // echo number_format ($moneytotal,2);
                                  // echo number_format ($sumall1 - $dataresult[0]->grandtotal,2);
                                }

                                if($dataenmoneycash){
                                  // echo number_format ($sumall1,2);
                                  // echo number_format ($moneytotal,2);
                                  // echo number_format ($sumtotalmoneycash,2);
                                  // $moneytotal = $sumall1 - $grandtotal;
                                  $moneytotal = $moneytotal - $sumtotalmoneycash;
                                }
                                echo number_format ($moneytotal,2);
                               }
                               ?></center></td>
                               <input type="hidden" name="all[]" value="<?php
                               // if($dataresult){ if($dataenpo){echo number_format($sumall2 - $dataresult[0]->grandtotal,2);}else {echo number_format($sumallatm - $dataresult[0]->grandtotal,2);} }
                               // if($dataresult){
                               //   if($dataenpo){
                               //     echo number_format($sumall2 - $dataresult[0]->grandtotal,2);}
                               //   else if($dataenmoney) {
                               //     echo number_format($sumallmoney - $dataresult[0]->grandtotal - $sumtotalmoney,2);}
                               //   else if($dataatm) {
                               //     echo number_format($sumallatm - $dataresult[0]->grandtotal,2);}
                               //   else {
                               //     echo number_format($sumall1 - $dataresult[0]->grandtotal,2);}
                               // }


                               // $moneytotal = $sumtotalloss + $sumtotalall + $sumtotalinsurance;
                               // echo number_format ($moneytotal,2);


                               if($dataresult){
                                 if($dataenmoney){
                                  $moneytotal = $sumall1 - $dataresult[0]->grandtotal + $sumtotalmoney;
                                  // echo number_format ($sumall1 - $dataresult[0]->grandtotal + $sumtotalmoney,2);
                                }else {
                                  $moneytotal = $sumall1 - $dataresult[0]->grandtotal;
                                  // echo number_format ($sumall1 - $dataresult[0]->grandtotal,2);
                                }

                                if($dataenmoneycash){
                                  $moneytotal = $moneytotal - $sumtotalmoneycash - $sumtotalmoneycash;
                                }
                                echo $moneytotal;
                               }
                               ?>" class="form-control"> <!--ยอดที่ต้องนำฝาก-->
                               <td>บาท</td>
                               <td colspan="3">
                               <td colspan="3">
                               <?php if($datacheck){ ?>
                                  <!-- <img src="http://www.fsctonline.com/fsctaccounting/public/checkcash/<?php //echo $datacheck[0]->image;?>" alt="User Image" style="width: 70%;height: 50%;"> -->
                                  <!-- <img src="http://localhost/fsctaccount/public/checkcash/<?php //echo $datacheck[0]->image;?>" alt="User Image" style="width: 70%;height: 70%;"> -->
                               <?php } ?></td>
                               <td colspan="2">
                            <tr>
                            <!--ลองคำนวณ-->


                          </tbody>
                        </table>


                    </div>
                  </div >

                  <div class="row">
                      <br>
                  </div>

                  <div class="row">
                      <div class="col-md-3">

                      </div>
                      <div class="col-md-2">

                      </div>
                      <div class="col-md-2">
                        <?php
                          echo "<font color='blue'>สาขา  ".$branch_id.' วันที่กดนำฝาก  '.$datetime."</font>";
                          echo "<br>";
                          echo "<br>";
                        if($data || $dataresult){
                          // echo $datepicker;
                          $datepicker2 = explode("/",trim($datepicker));
                          if(count($datepicker2) > 0) {
                              $datetime = $datepicker2[2] . '/' . $datepicker2[1] . '/' . $datepicker2[0]; //วัน - เดือน - ปี
                          }
                          // $date = explode(" ", $datepicker);

                          // exit;

                          if($datetime == date("Y/m/d")) { ?>
                          <input type="submit" name="transfer" class="btn btn-info" value="นำฝาก">
                        <?php }else{

                        }

                      }else{
                        echo "<font color='red'>คุณไม่สามารถกดนำฝากได้เนื่องจากไม่มียอดยกมากรุณาติดต่อ MD ไม่เช่นนั้นมีโทษทางวินัยพนักงาน</font>";
                      }
                        ?>
                      </div>
                      <div class="col-md-4" >

                      </div>
                      <div class="col-md-1" >

                      </div>
                  </div>

               </form>

               <?php } ?>

            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')
