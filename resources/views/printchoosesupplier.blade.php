<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;

?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }

        body {
            font-family: "THSarabunNew";
        }
        h4 {
            font-family: "THSarabunNew";
        }
        h4 {
            font-family: "THSarabunNew";
        }


        .container table {
        border-collapse: collapse;
        border: solid 1px #000;
        }
        .container table td {
        border: solid 1px #000;
        }
        .no-border-left-and-bottom {
        border-left: solid 1px #FFF!important;
        border-bottom: solid 1px #FFF!important;
        }
        .no-border-left-and-bottom-and-top {
        border-left: solid 1px #FFF!important;
        border-bottom: solid 1px #FFF!important;
        border-top: solid 1px #FFF!important;
        }
        .no-border-bottom-and-top {
        border-bottom: solid 1px #FFF!important;
        border-top: solid 1px #FFF!important;
        }
        .no-top{
        border-top: solid 1px #FFF!important;
        }
    </style>
</head>
<body>
    <?php
        $db = Connectdb::Databaseall();
        $sql = "SELECT * FROM $db[fsctaccount].supplier INNER JOIN $db[fsctaccount].supplier_terms ON(supplier.terms_id = supplier_terms.id)  WHERE supplier.id ='$id' ";
        $datahead = DB::connection('mysql')->select($sql);

    ?>

    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="30%">

                    <img src="images/company/1.png" width="250px" >

                    <!-- <img src="images/company/2.png" width="275px" > -->

            </td>
            <td width="70%" >
                <!-- บริษัท ฟ้าใสคอนสตรัคชั่นทูลส์ จำกัด -->
            </td>
        </tr>
    </table>

    <center><h3>ใบคัดเลือกผู้ขาย</h3></center>
    <?php

    $i= 0;

    ?>
    <table width="100%" border="0">
        <tr>
            <td width="100%" colspan="2" align="left"><font style="font-weight: bold">PS NO./เลขที่ใบคัดเลือกผู้ขาย  :</font> {{ $datahead[0]->ps_number }}</td>
            <td width="100%" colspan="2"><font style="font-weight: bold">Address/ที่อยู่ : </font>
                <?php
                    print_r($datahead[0]->address .'  ตำบล  '.$datahead[0]->district.'  อำเภอ  '.$datahead[0]->amphur.'  จังหวัด  '.$datahead[0]->province);

                    print_r('รหัสไปรษณีย์  '.$datahead[0]->zipcode);

                ?>

            </td>
        </tr>
        <tr>
            <td width="50%" colspan="2"><font style="font-weight: bold">Company/บริษัท  :</font> {{ $datahead[0]->name_supplier }}</td>
            <td width="50%" colspan="2"><font style="font-weight: bold">Tel/เบอร์โทรศัพท์  :</font> {{ $datahead[0]->phone }}</td>
        </tr>

        <tr>

            <td width="50%" colspan="2"><font style="font-weight: bold">establish/ปีที่ก่อตั้ง  :</font> {{ $datahead[0]->establish_year }}</td>
            <td width="50%" colspan="2"><font style="font-weight: bold">ประเภทของกิจการ  :</font> {{ $datahead[0]->business_type }}</td>
        </tr>

        <tr>
            <td width="50%" colspan="2"><font style="font-weight: bold">ผลิตภัณฑ์หลัก  :</font> {{ $datahead[0]->main_product }}</td>
            <td width="50%" colspan="4"><font style="font-weight: bold">กลุ่มอุตสาหกรรมที่เป็นลูกค้า  :</font> {{ $datahead[0]->main_industry }}</td>
        </tr>

        <tr>
            <td width="50%" colspan="4"><font style="font-weight: bold">มาตรฐานผลิตภัณฑ์สินค้าอุตสาหกรรม  (เช่น  มอก. ,JIS, ASTM, UL, CE Mark)</font></td>
        </tr>
        <tr>
            <td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input style="margin-top: 0.35em;" type="checkbox" <?php if($datahead[0]->product_standard == 1) echo "checked"; ?> /> มี </td>
            <td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input style="margin-top: 0.35em;" type="checkbox" <?php if($datahead[0]->product_standard == 0) echo "checked"; ?> /> ไม่มี </td>
            <td colspan="2" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (ถ้า มีขอสำเนาใบ Certificate)</td>
        </tr>
        <tr>
            <td width="50%" colspan="4"><font style="font-weight: bold">ระบบบริหารที่เกี่ยวกับคุณภาพ (เช่น ISO 9001, TS 16949, GMP, TQM)</font></td>
        </tr>
        <tr>
            <td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input style="margin-top: 0.35em;" type="checkbox" <?php if($datahead[0]->iso_system == 1) echo "checked"; ?> /> มี </td>
            <td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input style="margin-top: 0.35em;" type="checkbox" <?php if($datahead[0]->iso_system == 0) echo "checked"; ?> /> ไม่มี </td>
            <td colspan="2" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (ถ้า มีขอสำเนาใบ Certificate)</td>
        </tr>
        <tr>
            <td width="50%" colspan="4"><font style="font-weight: bold">ชิ้นงานตัวอย่างเพื่อพิจารณา</font></td>
        </tr>
        <tr>
            <td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input style="margin-top: 0.35em;" type="checkbox" <?php if($datahead[0]->example_product == 1) echo "checked"; ?> /> มี </td>
            <td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input style="margin-top: 0.35em;" type="checkbox" <?php if($datahead[0]->example_product_number == 0) echo "checked"; ?> /> ไม่มี </td>
            <td colspan="2" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (ถ้า มีจำนวน {{ $datahead[0]->example_product_number }} ชิ้น)</td>
        </tr>
        <tr>
            <td width="50%" colspan="2"> ระยะเวลาในการให้เครดิต {{ $datahead[0]->day }} วัน</td>
            <td width="50%" colspan="2"> สามารถส่งมอบสินค้าใด้ภายใน {{ $datahead[0]->number_delivery }} วัน</td>
        </tr>
        <tr>
            <td width="50%" colspan="4"><font style="font-weight: bold">บุคคลที่สามารถติดต่อได้: </font>{{ $datahead[0]->agent }}</td>
        </tr>
        <tr>
            <td width="50%" colspan="2"> ผู้ให้ข้อมูล {{ $datahead[0]->createby }}</td>
            <td width="50%" colspan="2"> วันที่ {{ $datahead[0]->createdate }}</td>
        </tr>
    </table>

    <table style="width: 100%" border="1" cellspacing="0">
        <thead>
            <tr>
                <th style="text-align: center;" colspan="2">เกณฑ์และคะแนนในการคัดลือก</th>
                <th style="text-align: center;">คะแนนเป้าหมาย</th>
                <th style="text-align: center;">คะแนนที่ให้</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="2">1. ผลการตรวจสอบคุณภาพสินค้า</td>
                <td style="text-align: center;">30 คะแนน</td>
                <td style="text-align: center;">{{ $datahead[0]->quality_point }} คะแนน</td>
            </tr>
            <tr>
                <td colspan="2">2. ด้านราคา</td>
                <td style="text-align: center;">20 คะแนน</td>
                <td style="text-align: center;">{{ $datahead[0]->price_point }} คะแนน</td>
            </tr>
            <tr>
                <td colspan="2">3. ด้านการส่งมอบ</td>
                <td style="text-align: center;">20 คะแนน</td>
                <td style="text-align: center;">{{ $datahead[0]->delivery_point }} คะแนน</td>
            </tr>
            <tr>
                <td colspan="2">4. ระยะเวลาในการให้เครดิต</td>
                <td style="text-align: center;">15 คะแนน</td>
                <td style="text-align: center;">{{ $datahead[0]->credit_point }} คะแนน</td>
            </tr>
            <tr>
                <td colspan="2">5. ระบบคุณภาพ /ควบคุมกระบวนการ</td>
                <td style="text-align: center;">15 คะแนน</td>
                <td style="text-align: center;">{{ $datahead[0]->manufacture_point }} คะแนน</td>
            </tr>
        </tbody>
        <?php
            $sum = $datahead[0]->quality_point + $datahead[0]->price_point + $datahead[0]->delivery_point + $datahead[0]->credit_point + $datahead[0]->manufacture_point;
        ?>
        <tfoot>
            <tr>
                <th style="text-align: center;" colspan="2">คะแนนรวม</th>
                <th style="text-align: center;">100 คะแนน</th>
                <th style="text-align: center;">{{ $sum }} คะแนน</th>
            </tr>
            <tr>
                <td colspan="4" >
                    <table width="100%" border="0" cellspacing="-1">

                        <tbody>
                            <tr>
                                <th colspan="2" style="border-right: 1px solid black;">&nbsp;&nbsp;สรุปผลการคัดเลือก</th>
                                <th colspan="2">&nbsp;&nbsp;การอนุมัติการคัดเลือก</th>
                            </tr>

                            <tr>
                                <td colspan="2" style="border-right: 1px solid black;">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input style="margin-top: 0.3em;" type="checkbox" <?php if($sum >= 75) echo "checked"; ?> /> เลือก &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ( 75 - 100 คะแนน )<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input style="margin-top: 0.3em;" type="checkbox" <?php if($sum <= 74) echo "checked"; ?> /> ไม่เลือก &nbsp;&nbsp;&nbsp; ( 0 - 74 คะแนน )
                                </td>
                                <td colspan="2">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input style="margin-top: 0.3em;" type="checkbox" <?php if( $datahead[0]->status == 1 ) echo "checked"; ?> /> อนุมัติ <br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input style="margin-top: 0.3em;" type="checkbox" <?php if( $datahead[0]->status == 99 ) echo "checked"; ?> /> ไม่อนุมัต
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="border-right: 1px solid black;" width="50%" align="center">
                                    ....................................................................................<br />
                                    ..................../...................../........................<br / />
                                    <font style="font-weight: bold;">หัวหน้าฝ่ายบัญชีจัดซื้อ</font>
                                </td>
                                <td colspan="2" width="50%" align="center">
                                    ....................................................................................<br />
                                    ..................../...................../........................<br / />
                                    <font style="font-weight: bold;">ผู้จัดการฝ่ายสำนักงาน</font>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tfoot>
    </table>

    <?php

    $i = 0;
     // echo "<pre>";
     // print_r($dataheaddetail);
     // exit;
     $arrwhd = [];
     $arrTotal = [];
     ?>









</body>
</html>
