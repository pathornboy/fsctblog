@include('headmenu')


{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">
<script type="text/javascript" src = 'js/vendor/pettycash.js'></script>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script type="text/javascript">
   $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>


<div class="content-wrapper">
    <section class="content">
        <div class="box box-success">
            <ul class="breadcrumb">
                        <li>
                            <i class="ace-icon fa fa-home home-icon"></i>
                            <a href="#">งานจัดซื้อ</a>
                        </li>
                        <li class="active">ตั้งค่ารายการเงินหน้าเก๊ะ</li>
            </ul><!-- /.breadcrumb -->
            <div class="box-body table-responsive">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                    ตั้งค่ารายการเงินหน้าเก๊ะ
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                                     @if (session('alert'))
                                    <div class="alert alert-success">
                                        {{ session('alert') }}
                                    </div>
                                    @endif
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-10">

                                    </div>

                                    <div class="col-md-2">
                                        <a href="#" title="เพิ่มข้อมูล"  data-toggle="modal" data-target="#modalinsert" ><img src="images/global/add.png">เพิ่มข้อมูล</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <br>
                                </div>
                                        <table id="example" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <td>#</td>
                                                    <td>ชื่อรายการ</td>
                                                    <td>สถานะ</td>
                                                    <td>การจัดการ</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @php $i=1 @endphp 
                                            @foreach ($data as $value)
                                                <tr>
                                                     <td>{{$i}}</td>
                                                    <td>{{$value->listname}}</td>
                                                    <td>@if($value->listname=1)ใช้งาน @else ยกเลิก @endif</td>
                                                    <td>
                                                        <a href="#" title="แก้ไข" onclick="getdata_to_edit({{ $value->id }})">แก้ไข</a> |
                                                        <a href="#" title="ลบ" onclick="getdata_to_delete({{ $value->id }})" >ลบ</a>
                                                        <!-- <form action="{{action('PettycashController@delete', $value->id)}}" method="post">
                                                          {{csrf_field()}}
                                                          <input name="_method" type="hidden" value="DELETE">
                                                          <button class="btn btn-danger" type="submit">Delete</button>
                                                        </form> -->
                                                    </td>
                                                </tr>
                                             @php $i++ @endphp   
                                            @endforeach   
                                            </tbody>
                                          </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>


</div>



<div class="modal fade" id="modalinsert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <form action="insertpettycash" method="post" >
        <h4 class="modal-title w-100 font-weight-bold">เพิ่มข้อมูล</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
        <div class="md-form mb-5">
         <label>ชื่อรายการ</label>
         <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <input type="text" name="listname" class="form-control">
        </div>
        <br>

      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button class="btn btn-default">เพิ่มข้อมูล</button>
        </form>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="modaledit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <form action="editpettycash" method="post" >
        <h4 class="modal-title w-100 font-weight-bold">แก้ไขข้อมูล</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
        <div class="md-form mb-5">
         <label>ชื่อรายการ</label>
         <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
         <input type="text" name="id" id="id_edit"  class="form-control">
          <input type="text" name="listname" id="name_edit"  class="form-control">
        </div>
        <br>

      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button class="btn btn-default">บันทึก</button>
        </form>
      </div>
    </div>
  </div>
</div>



@include('footer')




