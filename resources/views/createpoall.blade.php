<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;


?>
@include('headmenu')
<link>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>


<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">


<link rel="stylesheet" type="text/css" href="bower_components/select2/dist/css/select2.min.css">
<script type="text/javascript" src = 'bower_components/select2/dist/js/select2.full.min.js'></script>

<script type="text/javascript" src = 'js/vendor/addpoall.js'></script>


<meta name="csrf-token" content="{{ csrf_token() }}" />

<style media="screen">
  div{
    /* border: 1px solid red; */
  }
</style>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<!-- {{--<style>--}}
    {{--.modal-ku {--}}
        {{--width: 1024px;--}}
        {{--margin: auto;--}}
    {{--}--}}
{{--</style>--}} -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">งานจัดซื้อ</a>
                    </li>
                    <li class="active">ข้อมูลออกใบ PO</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body table-responsive">

                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        ข้อมูลการจัดซื้อ
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-10">

                                    </div>

                                    <div class="col-md-2">
                                        <a href="#" title="เพิ่มข้อมูล" data-toggle="modal" data-target="#myModal" onclick="insertnew()" ><img src="images/global/add.png">เพิ่มข้อมูล</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <br>
                                </div>
                                        <table id="example" class="table table-striped table-bordered">
                                            <thead class="thead-inverse">
                                            <tr>
                                                <td>#</td>
                                                <td>วันที่</td>
                                                <td>บริษัท</td>
                                                <td>PO No.</td>
                                                <td>รายชื่อ Vendor</td>
                                                <td>สาขา</td>
                                                <td>Terms</td>
                                                <td>ยอด</td>
                                                <td>BOSS/MD โอน</td>


                                                <td>จ่ายตามบิล</td>
                                                <td>คืนบริษัท</td>
                                                <td>วางบิล</td>
                                                <td>สถานะ</td>

                                                <td>พิมพ์ใบ PO</td>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $db = Connectdb::Databaseall();

                                            $data = DB::connection('mysql')->select('SELECT id, po_head.date, po_number, branch_id, terms, totolsumall, payreal, id_company, supplier_id, status_head FROM '.$db['fsctaccount'].'.po_head WHERE status_head != "99"  ORDER BY id DESC');
                                            $i = 1;
                                            ?>
                                            @foreach ($data as $value)
                                                <tr>
                                                    <td scope="row">{{ $i }}</td>
                                                    <td align="left">{{ $value->date}}</td>
                                                    <td align="left">
                                                        <?php $dataworking = Maincenter::datacompany($value->id_company);
                                                        print_r($dataworking[0]->name);

                                                        ?>
                                                    </td>
                                                    <td align="center">{{ $value->po_number }}</td>
                                                    <td align="center"><?php $datasupp = Vendorcenter::getdatavendorcenter($value->supplier_id);
                                                    //print_r($datasupp);
                                                     print_r($datasupp[0]->pre."     ".$datasupp[0]->name_supplier)
                                                    ?></td>
                                                    <td align="center">{{ $value->branch_id }}</td>
                                                    <td align="center">{{ $value->terms }}</td>
                                                    <td align="center">{{ $value->totolsumall }}</td>
                                                    <td align="center">{{ $value->payreal }}</td>
                                                    <td align="center">
                                                        <?php  $datapaybill = Vendorcenter::getpaybillemp($value->id);
                                                            if($datapaybill[0]->sumpay_real != ''){
                                                                $setpayemp = $datapaybill[0]->sumpay_real;
                                                                print_r(number_format($datapaybill[0]->sumpay_real,2));
                                                            }else{
                                                                $setpayemp = 0;
                                                            }



                                                        ?>
                                                    </td>
                                                    <td align="center"><?php
                                                            if($value->payreal - $setpayemp > 0){
                                                                echo number_format($value->payreal - $setpayemp,2);
                                                            }else{
                                                                echo 0;
                                                            }
                                                          ?></td>
                                                    <td align="center">
                                                        <a href="#" title="ดูรายละเอียด" data-toggle="modal" data-target="#myModal" onclick="getdata(2,{{ $value->id }})"><img src="images/global/edit-bill.png"></a>
                                                    </td>
                                                    <td align="center">
                                                    <?php $datastatus = Vendorcenter::getstatusresultpo($value->status_head);
                                                    print_r($datastatus);
                                                        ?>
                                                    </td>
                                                    <td align="center"><a href="<?php echo url("/printpo/$value->id");?>"  target="_blank"><img src="images/global/po.png"></a></td>
                                                </tr>
                                                <?php $i++; ?>
                                            @endforeach
                                            </tbody>
                                        </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')


<!-- Modal -->

<div class="modal fade" id="myModal"  role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog"  style="width:1330px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">สร้าง PO ทั้งหมด</h5>
            </div>

            <div class="modal-body">

              <form id="configFormvendors" onsubmit="return getdatesubmit();"  class="form-horizontal" role="form">
                    <input value="{{ null }}" type="hidden" id="id" name="id" />
                    <!-- <input type="hidden" id="po_no_branch" name="po_no_branch" > -->
                    <input type="hidden" name="statusupdate" id="statusupdate" value="0">
                    <div class="row">
                        <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-xs-3 control-label">วันที่ขอใบ PO<i><span style="color: red">*</span></i></label>
                            <div class="col-xs-5">
                                <input type="text"  class="form-control" id="dateshow" value="<?php echo date('d-m-Y',strtotime("+543 year"))?>"  disabled/>
                                <input type="hidden"  class="form-control" name="date" id="date" value="<?php echo date('Y-m-d')?>"  />
                                <input type="hidden"  class="form-control" name="year" id="year" value="<?php echo date('Y')?>"  />
                                <input type="hidden"  class="form-control" name="year_th" id="year_th" value="<?php echo date('Y',strtotime("+543 year"))?>"  />
                            </div>
                            <div class="col-xs-4">

                            </div>
                        </div>
                        </div>
                        <div class="col-md-1">

                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-1">

                        </div>
                        <div class="col-md-4">
                            <div class="pull-right">รหัสสาขา <?php echo $brcode = Session::get('brcode');?>
                                <input type="hidden"  class="form-control" name="branch_id" id="branch_id" value="<?php echo $brcode ;?>"  />
                                (
                                <?php
                                $databr = Maincenter::databranchbycode($brcode);
                                print_r($databr[0]->name_branch);
                                ?>)
                            </div>
                        </div>
                    </div>
                    <div>
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">เลือกบริษัท<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-8">
                                    <select name="id_company" id="id_company" class="form-control" onchange="selecttranfer(this)">
                                        <?php
                                          $db = Connectdb::Databaseall();
                                          $data = DB::connection('mysql')->select('SELECT name, id FROM '.$db['hr_base'].'.working_company WHERE status ="1"');
                                        ?>
                                        @foreach ($data as $value)
                                            <option value="{{$value->id}}">{{$value->name}}</option>

                                        @endforeach

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                                <b>เลือก งวดการจ่ายเงิน.</b>
                        </div>
                        <div class="col-md-3">
                              <select name="week" id="week" class="form-control select2" required>
                                  <option value="" select>เลือกงวดการจ่ายเงิน</option>
                                  <?php
                                    $db = Connectdb::Databaseall();
                                    $dataweek = DB::connection('mysql')->select('SELECT week FROM '.$db['fsctaccount'].'.ppr_head GROUP BY week ORDER BY ppr_head.week DESC');
                                  ?>
                                  @foreach($dataweek as $week)
                                    <option value="{{$week->week}}">{{$week->week}}</option>
                                  @endforeach
                              </select>
                        </div>

                        <div class="col-md-1">
                            <button type="button" class="btn btn-primary" id="serachpridrefSSSSS" onclick="serachpridall()">ค้นหา</button>
                        </div>
                    </div>
                    <div>
                        <br>
                    </div>


                <input type="hidden" name="datenow" id="dateset" value="<?php echo date('Y-m-d')?>">
                <input type="hidden" name="year"  class="form-control"  id="yearset" value="<?php echo date('Y')?>"  />
                <input type="hidden" name="year_th"  class="form-control" id="year_thset" value="<?php echo date('Y',strtotime("+543 year"))?>"  />

                <input type="hidden"  class="form-control" id="dateshowset" value="<?php echo date('d-m-Y',strtotime("+543 year"))?>"  disabled/>
                    <div class="row" style="display: none;" >
                      <div class="col-md-1">
                        ภาษี
                      </div>
                      <div class="col-md-2">
                        <select name="vat" id="vat" class="form-control" onchange="calculatevat(this,0)">
                            <option value="0">ไม่มี vat</option>
                            <?php
                            $db = Connectdb::Databaseall();
                            $data = DB::connection('mysql')->select('SELECT tax FROM '.$db['fsctaccount'].'.tax_config WHERE status = "1"');
                            ?>
                            @foreach ($data as $value)
                                <option <?php if($value->tax == 7) echo "selected"; ?> value="{{$value->tax}}">{{$value->tax}}</option>
                            @endforeach

                        </select>
                      </div>
                      <div class="col-md-9">

                      </div>
                    </div>

                    <div class="row not-found text-center" style="display: none;">
                      <h2 class="text-danger">ไม่พบข้อมูล</h2>
                      <br>
                      <br>
                    </div>

                    <div class="row selectvendortopo" style="display: none;">
                        <div class="col-md-10 col-md-offset-1">
                          <div class="pull-right">
                              <a href="#" title="เพิ่มรายการ" id="addrow" ><img src="images/global/add.png">เพิ่มรายการ</a>
                          </div>
                          <div class="row">
                            <br><br>
                          </div>

                          <input type="hidden" name="" id="level_emp" value="<?php echo session('level_emp'); ?>">

                          <!-- <input type="text" name="" value="" required> -->

                          <div class="row table-responsive">
                            <table class="table table-bordered text-center" id="thdetail">
                                <thead>
                                    <tr>
                                        <th>Supplier</th>
                                        <td>วิธีการจัดซื้อ</td>
                                        <td>วิธีการจ่าย</td>
                                        <td>ในประเทศ</td>
                                        <td>ในงบประมาณ</td>
                                        <td>การจัดส่ง</td>
                                        <th>ลบ</th>
                                    </tr>
                                </thead>
                                <tbody class="" id="tbody-transfer-money">
                                    <tr id="test_">
                                      <td class="form-group">
                                        <select required class="form-control full-form vendorbypr" name="vendor[]" id="vendorbypr_head">

                                        </select>
                                      </td>
                                      <td class="form-group">
                                        <select required class="form-control full-form" name="type_buy[]">
                                          <option value="">เลือก ประเภทการซื้อ</option>
                                          <option value="1">ทั่วไป</option>
                                        </select>
                                      </td>
                                      <td class="form-group">
                                        <select required class="form-control full-form" name="type_pay[]">
                                          <option value="">เลือก ประเภทการจ่าย</option>
                                          <option value="2">โอนเงิน</option>
                                          <option value="3">เงินสด</option>
                                        </select>
                                      </td>
                                      <td class="form-group">
                                        <select required class="form-control full-form" name="in_house[]">
                                          <option value="">เลือก การจัดซื้อ</option>
                                          <option value="1">ภายในประเทศ</option>
                                          <option value="2">ต่างประเทศ</option>
                                        </select>
                                      </td>
                                      <td class="form-group">
                                        <select required class="form-control full-form" name="in_budget[]">
                                          <option value="">เลือก งบประมาณ</option>
                                          <option value="1">ภายในงบประมาณ</option>
                                          <option value="2">นอกงบประมาณ</option>
                                        </select>
                                      </td>
                                      <td class="form-group">
                                        <select required class="form-control full-form" name="type_transfer[]">
                                          <?php
                                          $db = Connectdb::Databaseall();
                                          $data = DB::connection('mysql')->select('SELECT name, id FROM '.$db['fsctaccount'].'.transfer_config WHERE status = "1"');
                                          ?>
                                              <option value="">เลือก การจัดส่ง</option>
                                          @foreach ($data as $value)
                                              <option value="{{$value->id}}">{{$value->name}}</option>
                                          @endforeach
                                        </select>
                                      </td>

                                      <td><button type="button" id="del" onclick="deleteMe_transfer_money(this);" class="btn btn-danger"><i class="fa fa-trash"></i> </button></td>
                                    </tr>

                                </tbody>

                            </table>
                          </div>

                        </div>
                    </div>

                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-xs-5 col-xs-offset-3">
                                <button type="submit" id="Btn_save" class="btn btn-primary" >Save</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
              </form>

            </div>
        </div>
    </div>
</div>



<!---   myModal approved --->


<div class="modal fade" id="prdoc" role="dialog">
    <div class="modal-dialog modal-lg">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">ใบ PR ที่เกี่ยงข้อง</h4>
        </div>
        <div class="modal-body">
            <div class="row" id="showprref">
              <table width="100%" class="table table-bordered">
                 <thead>
                  <tr>
                      <td>ใบ PR. </td>
                      <td>สาขา</td>
                      <td>ชื่อสาขา</td>
                      <td>รายการ</td>
                      <td>ราคา</td>
                      <td>จำนวน</td>
                      <td>หัก ณ ที่จ่าย %</td>
                      <td>ภาษี %</td>
                      <td>รวม</td>
                  </tr>
                </thead>
                <tbody id="tbodyshowprref">
                </tbody>
              </table>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
