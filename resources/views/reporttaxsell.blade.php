<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Vendorcenter;
use  App\Api\Maincenter;



$db = Connectdb::Databaseall();
?>
@include('headmenu')
<link>

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>

<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">
<script type="text/javascript" src = 'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js'></script>
<script type="text/javascript" src = 'https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js'></script>
<script type="text/javascript" src = 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js'></script>
<script type="text/javascript" src = 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js'></script>
<script type="text/javascript" src = 'https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js'></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">

<script type="text/javascript" src = 'js/vendor/reportpopayin.js'></script>

<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<style>
    .modal-ku {
        width: 90%;
        margin: auto;
    }
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">รายงาน</a>
                    </li>

                </ul>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        รายงานภาษีขาย
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body" style="overflow-x:auto;">
                              <form action="serachreporttaxsell" method="post">
                                  <input type="hidden" name="_token" value="{{ csrf_token() }}">

                              <div class="row">
                                <div class="col-md-4">

                                </div>
                                <div class="col-md-4">
                                    <?php
                                        $db = Connectdb::Databaseall();
                                        $sql = 'SELECT '.$db['hr_base'].'.branch.*
                                           FROM '.$db['hr_base'].'.branch
                                           WHERE '.$db['hr_base'].'.branch.status = "1"';

                                        $brcode = DB::connection('mysql')->select($sql);

                                    ?>
                                    <select name="branch" id="branch" class="form-control" >
                                        <option value="*">เลือกทั้งหมด</option>
                                        <?php
                                          foreach ($brcode as $key => $value) {
                                        ?>
                                          <option value="<?php echo $value->code_branch;?>" <?php if(!empty($data)&&($value->code_branch==$branch)){ echo "selected";}?> > <?php echo $value->name_branch;?></option>

                                        <?php }?>
                                    </select>
                                </div>
                                <div class="col-md-4">

                                </div>
                              </div>
                              <div class="row">
                                  <br>
                              </div>
                              <div class="row">
                                <div class="col-md-4">

                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="datepicker" id="datepicker" <?php if(!empty($data)){ echo $datepicker;}?>  class="form-control datepicker" readonly>
                                </div>
                                <div class="col-md-4">

                                </div>
                              </div>
                              <div class="row">
                                  <br>
                              </div>
                              <div class="row">
                                <div class="col-md-3">

                                </div>
                                <div class="col-md-3">
                                    <input type="submit" value="ค้นหา" class="btn btn-primary pull-right">
                                </div>
                                <div class="col-md-3">
                                    <input type="reset" class="btn btn-danger">
                                </div>
                                <div class="col-md-3">

                                </div>

                                <div class="col-md-12" align="right">
                                <?php   if(isset($data)){ ////echo $branch_id; ?>

                                        <?php  //if(isset($group_branch_acc_select) && $group_branch_acc_select != ''){?>
                                                <!-- <a href="printcovertaxabb?group_branch_acc_select=<?php //echo $group_branch_acc_select ;?>&&datepickerstart=<?php //echo $datepicker2['start_date'];?>&&datepickerend=<?php  //echo $datepicker2['end_date'];?>"><img src="images/global/printall.png"></a> -->
                                        <?php //}else { ?>
                                        <?php $path = '&datepicker='.$datepicker.'&branch='.$branch?>
                                                <a href="<?php echo url("/printreporttaxsell?$path");?>" target="_blank"><img src="images/global/printall.png"></a>
                                                <!-- <a href="printcovertaxabb?branch_id=<?php //echo $branch_id ;?>&&datepickerstart=<?php //echo $datepicker2['start_date'];?>&&datepickerend=<?php  //echo $datepicker2['end_date'];?> target="_blank" "><img src="images/global/printall.png"></a> -->
                                        <?php //} ?>

                                <?php } ?>
                              </div>

                              </div>
                              </form>

                              <div class="row">
                                  <br>
                              </div>

                              <div class="row">
                                <?php if(!empty($data)){
                                      // echo "<pre>";
                                      // print_r($datataxra);
                                      // print_r($datataxtf);
                                      // print_r($datataxcn);
                                      // print_r($datataxcs);
                                      // print_r($datataxtk);
                                      // print_r($datataxrl);
                                      // print_r($datataxtm);
                                      // print_r($datataxrn);
                                      // print_r($datataxtp);
                                      // print_r($datataxro);
                                      // print_r($datataxrs);
                                      // print_r($datataxss);
                                      // exit;

                                      $db = Connectdb::Databaseall();

                                  ?>
                                    <table id="example" class="display nowrap" style="width:100% ">
                                        <thead>
                                            <tr>
                                              <th>ลำดับ</th>
                                              <th>ปี/เดือน/วัน</th>
                                              <th>เลขที่</th>
                                              <th>ชื่อผู้ซื้อสินค้า/ผู้รับบริการ</th>
                                              <th>เลขประจำตัวผู้เสียภาษี</th>
                                              <th>สถานประกอบการ</th>
                                              <th>มูลค่าสินค้าหรือบริการ</th>
                                              <th>จำนวนเงินภาษีมูลค่าเพิ่ม</th>
                                              <th>จำนวนเงินรวมทั้งหมด</th>
                                              <th>หมายเหตุ</th>
                                              <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php

                                              $sumtotalloss = 0;
                                              $vat = 0;
                                              $i = 1;
                                              $sumsubtotal = 0;
                                              $sumvat = 0;
                                              $sumgrandtotal = 0;

                                              $sumsubtotalcn = 0;
                                              $sumvatcn = 0;
                                              $sumgrandtotalcn = 0;


                                              $subtotal = 0;

                                              $j = 1;

                                              ?>

                                              <? foreach ($datataxra as $key => $value) { ?>
                                                <tr>
                                                   <td><?php echo $i;?></td><!--ลำดับ-->
                                                   <td><?php echo ($value->time);?></td><!--ปี/เดือน/วัน-->
                                                   <td><?php echo ($value->number_taxinvoice);?></td><!--เลขที่-->
                                                   <td>
                                                     <?php
                                                     $modelcustomerra = Maincenter::getdatacustomer($value->customerid);
                                                           if($modelcustomerra){
                                                             echo ($modelcustomerra[0]->per);
                                                             echo " ";
                                                             echo ($modelcustomerra[0]->name);
                                                             echo " ";
                                                             echo ($modelcustomerra[0]->lastname);
                                                           }

                                                     ?>
                                                   </td>
                                                   <td align="center" ><?php echo ($value->customerid);?></td>
                                                   <td><?php //echo ($modelsupplier[0]->type_branch);?></td>

                                                   <td align="right" >
                                                     <?php
                                                       if($value->status == 1){
                                                         if($value->discountmoney >= "0"){
                                                            echo number_format($value->subtotal - $value->discountmoney, 2);
                                                            $sumsubtotal = $sumsubtotal + ($value->subtotal - $value->discountmoney);
                                                         }else if(($value->discountmoney == "0")){
                                                            echo number_format($value->subtotal, 2);
                                                            $sumsubtotal = $sumsubtotal + ($value->subtotal);
                                                         }
                                                       }elseif ($value->status == 98){
                                                            echo "0.00";
                                                       }
                                                       // echo number_format($value->subtotal, 2);
                                                       // $sumsubtotal = $sumsubtotal + ($value->subtotal);
                                                     ?>
                                                   </td>

                                                   <td align="right" >
                                                     <?php
                                                       if($value->status == 1){
                                                         echo number_format ($value->vatmoney,2);
                                                         $sumvat = $sumvat + $value->vatmoney;
                                                       }
                                                       elseif ($value->status == 98){
                                                         echo "0.00";
                                                       }
                                                     ?>
                                                   </td>

                                                   <td align="right" >
                                                     <?php
                                                       if($value->status == 1){
                                                         echo number_format ($value->grandtotal + $value->withholdmoney,2);
                                                         $sumgrandtotal = $sumgrandtotal + $value->grandtotal + $value->withholdmoney;
                                                       }
                                                       elseif ($value->status == 98){
                                                         echo "0.00";
                                                       }
                                                     ?>
                                                   </td>
                                                   <td align="center" >
                                                     <?php
                                                       // echo ($value->note);
                                                         echo "-";
                                                     ?>
                                                   </td>
                                                   <td>
                                                     <?php

                                                      $modeldatatf = Maincenter::getdatatf($value->bill_rent);

                                                      if($value->status == 98){
                                                        echo "ยกเลิกไปออกอย่างเต็ม ";

                                                        if($modeldatatf){
                                                          echo ($modeldatatf[0]->number_taxinvoice);
                                                        }

                                                      }
                                                     ?>
                                                   </td>
                                                </tr>

                                              <?php $i++; } ?>

                                              <?
                                              foreach ($datataxtf as $key2 => $value2) { ?>

                                              <tr>
                                                 <td><?php echo $i;?></td><!--ลำดับ-->
                                                 <td><?php echo ($value2->time);?></td><!--ปี/เดือน/วัน-->
                                                 <td><?php echo ($value2->number_taxinvoice);?></td><!--เลขที่-->
                                                 <td>
                                                   <?php
                                                   // $modelcustomertf = Maincenter::getdatacustomer($value2->customerid);
                                                   //       if($modelcustomertf){
                                                   //         echo ($modelcustomertf[0]->name);
                                                   //         echo " ";
                                                   //         echo ($modelcustomertf[0]->lastname);
                                                   //       }
                                                    echo ($value2->initial);
                                                    echo " ";
                                                    echo ($value2->name);
                                                   ?>
                                                 </td>
                                                 <td align="center" ><?php echo ($value2->tax_no);?></td>
                                                 <td align="center" ><?php echo ($value2->branch_no);?></td>

                                                 <td align="right" >
                                                   <?php

                                                   if($value2->discountmoney >= "0"){
                                                      echo number_format($value2->subtotal - $value2->discountmoney, 2);
                                                      $sumsubtotal = $sumsubtotal + ($value2->subtotal - $value2->discountmoney);
                                                   }else if(($value2->discountmoney == "0")){
                                                      echo number_format($value2->subtotal, 2);
                                                      $sumsubtotal = $sumsubtotal + ($value2->subtotal);
                                                   }

                                                     // echo number_format($value2->subtotal, 2);
                                                     // $sumsubtotal = $sumsubtotal + $value2->subtotal;
                                                   ?>
                                                 </td>

                                                 <td align="right" >
                                                   <?php
                                                     echo number_format ($value2->vatmoney,2);
                                                     $sumvat = $sumvat + $value2->vatmoney;
                                                   ?>
                                                 </td>

                                                 <td align="right" >
                                                   <?php
                                                     echo number_format ($value2->grandtotal + $value2->withholdmoney,2);
                                                     $sumgrandtotal = $sumgrandtotal + $value2->grandtotal + $value2->withholdmoney;
                                                   ?>
                                                 </td>
                                                 <td align="center" >
                                                   <?php
                                                     // echo ($value2->note);
                                                       echo "-";
                                                   ?>
                                                 </td>
                                                 <td></td>
                                              </tr>

                                              <?php $i++; } ?>


                                              <?
                                              foreach ($datataxcn as $key3 => $value3) { ?>

                                              <tr>
                                                 <td><?php echo $i;?></td><!--ลำดับ-->
                                                 <td><?php echo ($value3->time);?></td><!--ปี/เดือน/วัน-->
                                                 <td><?php echo ($value3->number_taxinvoice);?></td><!--เลขที่-->
                                                 <td>
                                                   <?php
                                                   $modelcustomercn = Maincenter::getdata_cn($value3->bill_rent,$value3->type,$value3->ref_tax_type);
                                                   // echo "<pre>";
                                                   // print_r($modelcustomercn);
                                                   // exit;
                                                        if($modelcustomercn){

                                                           if(!empty($modelcustomercn[0]->name)){
                                                               echo ($modelcustomercn[0]->initial);
                                                               echo " ";
                                                               echo ($modelcustomercn[0]->name);
                                                           }
                                                           else{

                                                           $modelcustomer_cn = Maincenter::getdatacustomer($value3->customerid);
                                                              if($modelcustomer_cn){
                                                                 echo ($modelcustomer_cn[0]->per);
                                                                 echo " ";
                                                                 echo ($modelcustomer_cn[0]->name);
                                                                 echo " ";
                                                                 echo ($modelcustomer_cn[0]->lastname);
                                                             }
                                                           }

                                                        }

                                                   ?>
                                                 </td>
                                                 <td align="center" >
                                                   <?php
                                                   if($modelcustomercn){

                                                      if(!empty($modelcustomercn[0]->name)){
                                                          echo ($modelcustomercn[0]->tax_no);
                                                      }
                                                      else{
                                                          echo ($value3->customerid);
                                                      }

                                                   }
                                                   // echo ($value3->customerid);
                                                   ?>
                                                 </td>
                                                 <td align="center" ><?php echo ($value3->branchcustomer);?></td>

                                                 <td align="right" >
                                                   <?php

                                                     if($value3->discountmoney >= "0"){
                                                        echo number_format(-($value3->subtotal - $value3->discountmoney), 2);
                                                        $sumsubtotalcn = $sumsubtotalcn + ($value3->subtotal - $value3->discountmoney);
                                                     }else if(($value3->discountmoney == "0")){
                                                        echo number_format(-$value3->subtotal, 2);
                                                        $sumsubtotalcn = $sumsubtotalcn + ($value3->subtotal);
                                                     }

                                                     // echo number_format($value3->subtotal, 2);
                                                     // $sumsubtotalcn = $sumsubtotalcn + $value3->subtotal;
                                                   ?>
                                                 </td>

                                                 <td align="right" >
                                                   <?php
                                                     echo number_format (-$value3->vatmoney,2);
                                                     $sumvatcn = $sumvatcn + $value3->vatmoney;
                                                   ?>
                                                 </td>

                                                 <td align="right" >
                                                   <?php
                                                     echo number_format (-($value3->grandtotal + $value3->withholdmoney),2);
                                                     $sumgrandtotalcn = $sumgrandtotalcn + $value3->grandtotal + $value->withholdmoney;
                                                   ?>
                                                 </td>
                                                 <td>
                                                   <?php
                                                     echo ($value3->note);
                                                       // echo "-";
                                                   ?>
                                                 </td>
                                                 <td>
                                                   <?php
                                                   $modelcnoftax = Maincenter::gettaxinvoice_all($value3->bill_rent,$value3->type,$value3->ref_tax_type);
                                                         if($modelcnoftax){
                                                           echo ($modelcnoftax[0]->number_taxinvoice);
                                                         }

                                                   ?>
                                                 </td>
                                              </tr>

                                              <?php $i++; } ?>


                                              <?php
                                              foreach ($datataxtk as $key5 => $value5) { ?>
                                                <tr>
                                                   <td><?php echo $i;?></td><!--ลำดับ-->
                                                   <td><?php echo ($value5->time);?></td><!--ปี/เดือน/วัน-->
                                                   <td><?php echo ($value5->number_taxinvoice);?></td><!--เลขที่-->
                                                   <td>
                                                     <?php
                                                     // $modelcustomertk = Maincenter::getdatacustomer($value5->customerid);
                                                     //       if($modelcustomertk){
                                                     //         echo ($modelcustomertk[0]->name);
                                                     //         echo " ";
                                                     //         echo ($modelcustomertk[0]->lastname);
                                                     //       }

                                                     echo ($value5->initial);
                                                     echo " ";
                                                     echo ($value5->name);

                                                     ?>
                                                   </td>
                                                   <td align="center"><?php echo ($value5->tax_no);?></td>
                                                   <td align="center"><?php echo $value5->branch_no;?></td>

                                                   <td align="right" >
                                                     <?php

                                                       if($value5->discount > "0"){
                                                          $subtotal = ($value5->subtotal*$value5->discount)/100;
                                                          echo number_format($value5->subtotal - $subtotal, 2);
                                                          $sumsubtotal = $sumsubtotal + ($value5->subtotal - $subtotal);
                                                       }else if(($value5->discount == "0")){
                                                          echo number_format($value5->subtotal, 2);
                                                          $sumsubtotal = $sumsubtotal + ($value5->subtotal);
                                                       }

                                                       // echo number_format($value5->subtotal, 2);
                                                       // $sumsubtotal = $sumsubtotal + ($value5->subtotal);
                                                     ?>
                                                   </td>

                                                   <td align="right" >
                                                     <?php
                                                       echo number_format ($value5->vatmoney,2);
                                                       $sumvat = $sumvat + $value5->vatmoney;
                                                     ?>
                                                   </td>

                                                   <td align="right" >
                                                     <?php
                                                       echo number_format ($value5->grandtotal,2);
                                                       $sumgrandtotal = $sumgrandtotal + $value5->grandtotal;
                                                     ?>
                                                   </td>
                                                   <td align="center">
                                                     <?php
                                                       // echo ($value5->note);
                                                         echo "-";
                                                     ?>
                                                   </td>
                                                   <td></td>
                                                </tr>

                                              <?php $i++; } ?>

                                              <?php
                                              foreach ($datataxrl as $key6 => $value6) { ?>
                                                <tr>
                                                   <td><?php echo $i;?></td><!--ลำดับ-->
                                                   <td><?php echo ($value6->time);?></td><!--ปี/เดือน/วัน-->
                                                   <td><?php echo ($value6->number_taxinvoice);?></td><!--เลขที่-->
                                                   <td>
                                                     <?php
                                                     $modelcustomerrl = Maincenter::getdatacustomer($value6->customerid);
                                                           if($modelcustomerrl){
                                                             echo ($modelcustomerrl[0]->per);
                                                             echo " ";
                                                             echo ($modelcustomerrl[0]->name);
                                                             echo " ";
                                                             echo ($modelcustomerrl[0]->lastname);
                                                           }

                                                     ?>
                                                   </td>
                                                   <td align="center"><?php echo ($value6->customerid);?></td>
                                                   <td align="center"><?php //echo ($modelsupplier[0]->type_branch);?></td>

                                                   <td align="right" >
                                                     <?php

                                                       // if($value6->discountmoney >= "0"){
                                                       //    echo number_format(-$value6->subtotal - $value6->discountmoney, 2);
                                                       //    $sumsubtotal = $sumsubtotal + ($value6->subtotal - $value6->discountmoney);
                                                       // }else if(($value6->discountmoney == "0")){
                                                       //    echo number_format($value6->subtotal, 2);
                                                       //    $sumsubtotal = $sumsubtotal + ($value6->subtotal);
                                                       // }
                                                       if($value6->status == 1){
                                                         echo number_format($value6->subtotal, 2);
                                                         $sumsubtotal = $sumsubtotal + ($value6->subtotal);
                                                       }elseif ($value6->status == 98){
                                                         echo "0.00";
                                                       }
                                                     ?>
                                                   </td>

                                                   <td align="right" >
                                                     <?php
                                                       if($value6->status == 1){
                                                         echo number_format ($value6->vatmoney,2);
                                                         $sumvat = $sumvat + $value6->vatmoney;
                                                       }elseif ($value6->status == 98){
                                                         echo "0.00";
                                                       }
                                                     ?>
                                                   </td>

                                                   <td align="right" >
                                                     <?php
                                                       if($value6->status == 1){
                                                         echo number_format ($value6->grandtotal,2);
                                                         $sumgrandtotal = $sumgrandtotal + $value6->grandtotal;
                                                       }elseif ($value6->status == 98){
                                                         echo "0.00";
                                                       }
                                                     ?>
                                                   </td>
                                                   <td align="center">
                                                     <?php
                                                       // echo ($value6->note);
                                                         echo "-";
                                                     ?>
                                                   </td>
                                                   <td>
                                                     <?php
                                                     $modeldatatk = Maincenter::getdatatk($value6->bill_rent);

                                                     if($value6->status == 98){
                                                       echo "ยกเลิกไปออกอย่างเต็ม ";

                                                       if($modeldatatk){
                                                         echo ($modeldatatk[0]->number_taxinvoice);
                                                       }

                                                     }
                                                     ?>
                                                   </td>
                                                </tr>

                                              <?php $i++; } ?>

                                              <?php
                                              foreach ($datataxtm as $key7 => $value7) { ?>
                                                <tr>
                                                   <td><?php echo $i;?></td><!--ลำดับ-->
                                                   <td><?php echo ($value7->time);?></td><!--ปี/เดือน/วัน-->
                                                   <td><?php echo ($value7->number_taxinvoice);?></td><!--เลขที่-->
                                                   <td>
                                                     <?php
                                                     // $modelcustomertm = Maincenter::getdatacustomer($value7->customerid);
                                                     //       if($modelcustomertm){
                                                     //         echo ($modelcustomertm[0]->name);
                                                     //         echo " ";
                                                     //         echo ($modelcustomertm[0]->lastname);
                                                     //       }

                                                     echo ($value7->initial);
                                                     echo " ";
                                                     echo ($value7->name);

                                                     ?>
                                                   </td>
                                                   <td align="center"><?php echo ($value7->tax_no);?></td>
                                                   <td align="center"><?php echo $value7->branch_no;?></td>

                                                   <td align="right" >
                                                     <?php

                                                       if($value7->discountmoney >= "0"){
                                                          echo number_format($value7->subtotal - $value7->discountmoney, 2);
                                                          $sumsubtotal = $sumsubtotal + ($value7->subtotal - $value7->discountmoney);
                                                       }else if(($value7->discountmoney == "0")){
                                                          echo number_format($value7->subtotal, 2);
                                                          $sumsubtotal = $sumsubtotal + ($value7->subtotal);
                                                       }

                                                       // echo number_format($value7->subtotal, 2);
                                                       // $sumsubtotal = $sumsubtotal + ($value7->subtotal);
                                                     ?>
                                                   </td>

                                                   <td align="right" >
                                                     <?php
                                                       echo number_format ($value7->vatmoney,2);
                                                       $sumvat = $sumvat + $value7->vatmoney;
                                                     ?>
                                                   </td>

                                                   <td align="right" >
                                                     <?php
                                                       echo number_format ($value7->grandtotal + $value7->withholdmoney,2);
                                                       $sumgrandtotal = $sumgrandtotal + $value7->grandtotal + $value7->withholdmoney;
                                                     ?>
                                                   </td>
                                                   <td align="center">
                                                     <?php
                                                       // echo ($value7->note);
                                                         echo "-";
                                                     ?>
                                                   </td>
                                                   <td></td>
                                                </tr>

                                              <?php $i++; } ?>

                                              <?php
                                              foreach ($datataxrn as $key8 => $value8) { ?>
                                                <tr>
                                                   <td><?php echo $i;?></td><!--ลำดับ-->
                                                   <td><?php echo ($value8->time);?></td><!--ปี/เดือน/วัน-->
                                                   <td><?php echo ($value8->number_taxinvoice);?></td><!--เลขที่-->
                                                   <td>
                                                     <?php
                                                     $modelcustomerrn = Maincenter::getdatacustomer($value8->customerid);
                                                           if($modelcustomerrn){
                                                             echo ($modelcustomerrn[0]->per);
                                                             echo " ";
                                                             echo ($modelcustomerrn[0]->name);
                                                             echo " ";
                                                             echo ($modelcustomerrn[0]->lastname);
                                                           }

                                                     ?>
                                                   </td>
                                                   <td align="center"><?php echo ($value8->customerid);?></td>
                                                   <td align="center"><?php //echo $value8->branch_no;?></td>

                                                   <td align="right" >
                                                     <?php

                                                      if($value8->status == 1){
                                                         if($value8->discountmoney >= "0"){
                                                            echo number_format($value8->subtotal - $value8->discountmoney, 2);
                                                            $sumsubtotal = $sumsubtotal + ($value8->subtotal - $value8->discountmoney);
                                                         }else if(($value8->discountmoney == "0")){
                                                            echo number_format($value8->subtotal, 2);
                                                            $sumsubtotal = $sumsubtotal + ($value8->subtotal);
                                                         }
                                                       }elseif ($value8->status == 98) {
                                                            echo "0.00";
                                                       }

                                                       // echo number_format($value8->subtotal, 2);
                                                       // $sumsubtotal = $sumsubtotal + ($value8->subtotal);
                                                     ?>
                                                   </td>

                                                   <td align="right" >
                                                     <?php
                                                       if($value8->status == 1){
                                                         echo number_format ($value8->vatmoney,2);
                                                         $sumvat = $sumvat + $value8->vatmoney;
                                                       }
                                                       elseif ($value8->status == 98){
                                                            echo "0.00";
                                                       }
                                                     ?>
                                                   </td>

                                                   <td align="right" >
                                                     <?php
                                                       if($value8->status == 1){
                                                         echo number_format ($value8->grandtotal + $value8->withholdmoney,2);
                                                         $sumgrandtotal = $sumgrandtotal + $value8->grandtotal + $value8->withholdmoney;
                                                       }
                                                       elseif ($value8->status == 98){
                                                            echo "0.00";
                                                       }
                                                     ?>
                                                   </td>
                                                   <td>
                                                     <?php
                                                       echo ($value8->note);
                                                         // echo "-";
                                                     ?>
                                                   </td>
                                                   <td>
                                                     <?php
                                                     $modeldatatm = Maincenter::getdatatm($value8->bill_rent);

                                                     if($value8->status == 98){
                                                       echo "ยกเลิกไปออกอย่างเต็ม ";

                                                       if($modeldatatm){
                                                         echo ($modeldatatm[0]->number_taxinvoice);
                                                       }

                                                     }
                                                     ?>
                                                   </td>
                                                </tr>

                                              <?php $i++; } ?>

                                              <?php
                                              foreach ($datataxtp as $key9 => $value9) { ?>
                                                <tr>
                                                   <td><?php echo $i;?></td><!--ลำดับ-->
                                                   <td><?php echo ($value9->time);?></td><!--ปี/เดือน/วัน-->
                                                   <td><?php echo ($value9->number_taxinvoice);?></td><!--เลขที่-->
                                                   <td>
                                                     <?php
                                                     // $modelcustomertp = Maincenter::getdatacustomer($value9->customerid);
                                                     //       if($modelcustomertp){
                                                     //         echo ($modelcustomertp[0]->name);
                                                     //         echo " ";
                                                     //         echo ($modelcustomertp[0]->lastname);
                                                     //       }

                                                     echo ($value9->initial);
                                                     echo " ";
                                                     echo ($value9->name);

                                                     ?>
                                                   </td>
                                                   <td align="center"><?php echo ($value9->tax_no);?></td>
                                                   <td align="center"><?php echo $value9->branch_no;?></td>

                                                   <td align="right">
                                                     <?php

                                                       // if($value9->discountmoney >= "0"){
                                                       //    echo number_format(-$value9->subtotal - $value9->discountmoney, 2);
                                                       //    $sumsubtotal = $sumsubtotal + ($value9->subtotal - $value9->discountmoney);
                                                       // }else if(($value9->discountmoney == "0")){
                                                       //    echo number_format($value9->subtotal, 2);
                                                       //    $sumsubtotal = $sumsubtotal + ($value9->subtotal);
                                                       // }

                                                       echo number_format($value9->subtotal, 2);
                                                       $sumsubtotal = $sumsubtotal + ($value9->subtotal);
                                                     ?>
                                                   </td>

                                                   <td align="right">
                                                     <?php
                                                       echo number_format ($value9->vatmoney,2);
                                                       $sumvat = $sumvat + $value9->vatmoney;
                                                     ?>
                                                   </td>

                                                   <td align="right">
                                                     <?php
                                                       echo number_format ($value9->grandtotal + $value9->withholdmoney,2);
                                                       $sumgrandtotal = $sumgrandtotal + $value9->grandtotal + $value9->withholdmoney;
                                                     ?>
                                                   </td>
                                                   <td align="center">
                                                     <?php
                                                       // echo ($value9->note);
                                                         echo "-";
                                                     ?>
                                                   </td>
                                                   <td></td>
                                                </tr>

                                              <?php $i++; } ?>

                                              <?php
                                              foreach ($datataxro as $key10 => $value10) { ?>
                                                <tr>
                                                   <td><?php echo $i;?></td><!--ลำดับ-->
                                                   <td><?php echo ($value10->time);?></td><!--ปี/เดือน/วัน-->
                                                   <td><?php echo ($value10->number_taxinvoice);?></td><!--เลขที่-->
                                                   <td>
                                                     <?php
                                                     $modelcustomerro = Maincenter::getdatacustomer($value10->customerid);
                                                           if($modelcustomerro){
                                                             echo ($modelcustomerro[0]->per);
                                                             echo " ";
                                                             echo ($modelcustomerro[0]->name);
                                                             echo " ";
                                                             echo ($modelcustomerro[0]->lastname);
                                                           }

                                                     ?>
                                                   </td>
                                                   <td align="center"><?php echo ($value10->customerid);?></td>
                                                   <td align="center"><?php //echo $value10->branch_no;?></td>

                                                   <td align="right">
                                                     <?php

                                                       // if($value10->discountmoney >= "0"){
                                                       //    echo number_format(-$value10->subtotal - $value10->discountmoney, 2);
                                                       //    $sumsubtotal = $sumsubtotal + ($value10->subtotal - $value10->discountmoney);
                                                       // }else if(($value10->discountmoney == "0")){
                                                       //    echo number_format($value10->subtotal, 2);
                                                       //    $sumsubtotal = $sumsubtotal + ($value10->subtotal);
                                                       // }
                                                       if($value10->status == 1){
                                                         echo number_format($value10->subtotal, 2);
                                                         $sumsubtotal = $sumsubtotal + ($value10->subtotal);
                                                       }
                                                       elseif ($value10->status == 98){
                                                            echo "0.00";
                                                       }
                                                     ?>
                                                   </td>

                                                   <td align="right">
                                                     <?php
                                                       if($value10->status == 1){
                                                         echo number_format ($value10->vatmoney,2);
                                                         $sumvat = $sumvat + $value10->vatmoney;
                                                       }
                                                       elseif ($value10->status == 98){
                                                            echo "0.00";
                                                       }
                                                     ?>
                                                   </td>

                                                   <td align="right">
                                                     <?php
                                                       if($value10->status == 1){
                                                         echo number_format ($value10->grandtotal + $value10->withholdmoney,2);
                                                         $sumgrandtotal = $sumgrandtotal + $value10->grandtotal + $value10->withholdmoney;
                                                       }
                                                       elseif ($value10->status == 98) {
                                                         echo "0.00";
                                                       }
                                                     ?>
                                                   </td>
                                                   <td align="center">
                                                     <?php
                                                       // echo ($value10->note);
                                                         echo "-";
                                                     ?>
                                                   </td>
                                                   <td>
                                                     <?php
                                                     $modeldatatp = Maincenter::getdatatp($value10->bill_rent);

                                                     if($value10->status == 98){
                                                       echo "ยกเลิกไปออกอย่างเต็ม ";

                                                       if($modeldatatp){
                                                         echo ($modeldatatp[0]->number_taxinvoice);
                                                       }

                                                     }
                                                     ?>
                                                   </td>
                                                </tr>

                                              <?php $i++; } ?>

                                              <?php
                                              foreach ($datataxrs as $key11 => $value11) { ?>
                                                <tr>
                                                   <td><?php echo $i;?></td><!--ลำดับ-->
                                                   <td><?php echo ($value11->date_approved);?></td><!--ปี/เดือน/วัน-->
                                                   <td><?php echo ($value11->number_taxinvoice);?></td><!--เลขที่-->
                                                   <td>
                                                     <?php
                                                     $modelcustomerrs = Maincenter::getdatacustomer($value11->customer_id);
                                                           if($modelcustomerrs){
                                                             echo ($modelcustomerrs[0]->per);
                                                             echo " ";
                                                             echo ($modelcustomerrs[0]->name);
                                                             echo " ";
                                                             echo ($modelcustomerrs[0]->lastname);
                                                           }

                                                     ?>
                                                   </td>
                                                   <td align="center"><?php echo ($value11->customer_id);?></td>
                                                   <td align="center"><?php //echo $value11->branch_no;?></td>

                                                   <td align="right" >
                                                     <?php

                                                       // if($value11->discountmoney >= "0"){
                                                       //    echo number_format(-$value11->subtotal - $value11->discountmoney, 2);
                                                       //    $sumsubtotal = $sumsubtotal + ($value11->subtotal - $value11->discountmoney);
                                                       // }else if(($value11->discountmoney == "0")){
                                                       //    echo number_format($value11->subtotal, 2);
                                                       //    $sumsubtotal = $sumsubtotal + ($value11->subtotal);
                                                       // }

                                                       echo number_format($value11->subtotal, 2);
                                                       $sumsubtotal = $sumsubtotal + ($value11->subtotal);
                                                     ?>
                                                   </td>

                                                   <td align="right" >
                                                     <?php
                                                       echo number_format ($value11->vatmoney,2);
                                                       $sumvat = $sumvat + $value11->vatmoney;
                                                     ?>
                                                   </td>

                                                   <td align="right" >
                                                     <?php
                                                       echo number_format ($value11->grandtotal + $value11->withholdmoney,2);
                                                       $sumgrandtotal = $sumgrandtotal + $value11->grandtotal + $value11->withholdmoney;
                                                     ?>
                                                   </td>
                                                   <td>
                                                     <?php
                                                       echo ($value11->note);
                                                       // echo "-";
                                                     ?>
                                                   </td>
                                                   <td></td>
                                                </tr>

                                              <?php $i++; } ?>

                                              <?php
                                              foreach ($datataxss as $key12 => $value12) { ?>
                                                <tr>
                                                   <td><?php echo $i;?></td><!--ลำดับ-->
                                                   <td><?php echo ($value12->date_approved);?></td><!--ปี/เดือน/วัน-->
                                                   <td><?php echo ($value12->bill_no);?></td><!--เลขที่-->
                                                   <td>
                                                     <?php
                                                     $modelcustomerss = Maincenter::getdatacustomer($value12->customer_id);
                                                           if($modelcustomerss){
                                                             echo ($modelcustomerss[0]->per);
                                                             echo " ";
                                                             echo ($modelcustomerss[0]->name);
                                                             echo " ";
                                                             echo ($modelcustomerss[0]->lastname);
                                                           }

                                                     ?>
                                                   </td>
                                                   <td align="center"><?php echo ($value12->customer_id);?></td>
                                                   <td align="center"><?php //echo $value12->branch_no;?></td>

                                                   <td align="right" >
                                                     <?php

                                                       // if($value12->discountmoney >= "0"){
                                                       //    echo number_format($value12->grandtotal - $value12->discountmoney - $value12->vatmoney, 2);
                                                       //    $sumsubtotal = $sumsubtotal + ($value12->grandtotal - $value12->discountmoney - $value12->vatmoney);
                                                       // }else if(($value12->discountmoney == "0")){
                                                       //    echo number_format($value12->grandtotal - $value12->vatmoney, 2);
                                                       //    $sumsubtotal = $sumsubtotal + ($value12->grandtotal - $value12->vatmoney);
                                                       // }

                                                       echo number_format(($value12->grandtotal - $value12->vatmoney), 2);
                                                       $sumsubtotal = $sumsubtotal + ($value12->grandtotal - $value12->vatmoney);
                                                     ?>
                                                   </td>

                                                   <td align="right" >
                                                     <?php
                                                       echo number_format ($value12->vatmoney,2);
                                                       $sumvat = $sumvat + $value12->vatmoney;
                                                     ?>
                                                   </td>

                                                   <td align="right" >
                                                     <?php
                                                       echo number_format ($value12->grandtotal,2);
                                                       $sumgrandtotal = $sumgrandtotal + $value12->grandtotal;
                                                     ?>
                                                   </td>
                                                   <td>
                                                     <?php
                                                         echo ($value12->note);
                                                         // echo "-";
                                                     ?>
                                                   </td>
                                                   <td></td>
                                                </tr>

                                              <?php $i++; } ?>














                                                 <!-- <tr>
                                                   <td colspan="5" align="right"></td>
                                                   <td><center><b>รวม</b></center></td>
                                                   <td><b><?php //echo number_format($sumsubtotal,2);  ?></b></td>
                                                   <td><b><?php //echo number_format($sumvat,2);  ?></b></td>
                                                   <td><b><?php //echo number_format($sumgrandtotal,2); ?></b></td>
                                                   <td></td>
                                                 </tr> -->

                                        </tbody>
                                    </table>

                                <?php } ?>
                              </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')
