<?php

use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;
use  App\Api\DateTime;

?>
@include('headmenu')

<link>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}
<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>
<script type="text/javascript" src = 'js/config/configcash.js'></script>
<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->

<script type="text/javascript" src = 'js/report/cash.js'></script>
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-cog home-icon"></i>
                        <a href="#">ข้อมูลเงินสด</a>
                    </li>
                    <li class="active">ตรวจเช็คเงินสดย่อยรายวัน</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">
              <!-- <form action="searchgetcashedit" method="post"> -->
              <input type="hidden" name="_token" value="{{ csrf_token() }}">

              <?php

                $brcode = Session::get('brcode');
                $empcode = Session::get('emp_code');

                $db = Connectdb::Databaseall();
                $sql = 'SELECT '.$db['fsctaccount'].'.bank.*
                        FROM '.$db['fsctaccount'].'.bank

                        WHERE '.$db['fsctaccount'].'.bank.branch_id = '.$brcode.'
                          AND '.$db['fsctaccount'].'.bank.status NOT IN (99)
                          ';

                $databank = DB::connection('mysql')->select($sql);

              ?>

            <form action="configgetcashchechbank" onSubmit="if(!confirm('ยืนยันการทำรายการ?')){return false;}" method="post" class="form-horizontal" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

            <?php //if($databank[0]->number_bank) {  ?>
              <div class="row">
                <div class="col-md-2"  align="right">
                    <b><?php echo $databank[0]->name_bank; ?></b>
                </div>

                <div class="col-md-1"  align="right">
                    <b><?php echo $databank[0]->name; ?></b>
                </div>

                <div class="col-md-2" align="center">
                    <?php echo $databank[0]->number_bank; ?>
                </div>

                <div class="col-md-2">
                  <input type="text" name="grandtotal" id="grandtotal" class="form-control">
                </div>

                <div class="col-md-1">
                  บาท
                </div>

                <div class="col-md-1">
                  <input type="file"  name="image[]" id="image" value="">

                  <!-- <input type="hidden" name="brcode" value="<?php //echo $brcode; ?>" class="form-control">
                  <input type="hidden" name="empcode" value="<?php //echo $empcode; ?>" class="form-control">
                  <input type="hidden" name="status" value="1" class="form-control"> -->
                </div>

                <div class="col-md-1">

                </div>

                <div class="col-md-1">
                  <input type="text" name="bill_no" id="bill_no" class="form-control">
                </div>

                <div font color ="red" class="col-md-1">
                    <b><font color="red">เลขที่สลิป*</font></b>
                </div>
              </div>
            <?php //} ?>

            <div class="row">
                <br>
            </div>

            <?php //if($databank[1]->number_bank) {  ?>
              <div class="row">
                <div class="col-md-2"  align="right">
                    <b><?php echo $databank[1]->name_bank; ?></b>
                </div>

                <div class="col-md-1"  align="right">
                    <b><?php echo $databank[1]->name; ?></b>
                </div>

                <div class="col-md-2" align="center">
                    <?php echo $databank[1]->number_bank; ?>
                </div>

                <div class="col-md-2">
                  <input type="text" name="grandtotal1" id="grandtotal1" class="form-control">
                </div>

                <div class="col-md-1">
                  บาท
                </div>

                <div class="col-md-1">
                  <input type="file"  name="image[]" id="image1" value="1">

                  <!-- <input type="hidden" name="brcode" value="<?php //echo $brcode; ?>" class="form-control">
                  <input type="hidden" name="empcode" value="<?php //echo $empcode; ?>" class="form-control">
                  <input type="hidden" name="status" value="1" class="form-control"> -->
                </div>

                <div class="col-md-1">

                </div>

                <div class="col-md-1">
                  <input type="text" name="bill_no1" id="bill_no1" class="form-control">
                </div>

                <div class="col-md-1">
                    <b><font color="red">เลขที่สลิป*</font></b>
                </div>
              </div>
            <?php //} ?>

            <div class="row">
                <br>
            </div>

            <?php //if($databank[2]->number_bank) {  ?>
              <div class="row">
                <div class="col-md-2"  align="right">
                    <b><?php echo $databank[2]->name_bank; ?></b>
                </div>

                <div class="col-md-1"  align="right">
                    <b><?php echo $databank[2]->name; ?></b>
                </div>

                <div class="col-md-2" align="center">
                    <?php echo $databank[2]->number_bank; ?>
                </div>

                <div class="col-md-2">
                  <input type="text" name="grandtotal2" id="grandtotal2" class="form-control">
                </div>

                <div class="col-md-1">
                  บาท
                </div>

                <div class="col-md-1">
                  <input type="file"  name="image[]" id="image2" value="2">

                  <!-- <input type="hidden" name="brcode" value="<?php //echo $brcode; ?>" class="form-control">
                  <input type="hidden" name="empcode" value="<?php //echo $empcode; ?>" class="form-control">
                  <input type="hidden" name="status" value="1" class="form-control"> -->
                </div>

                <div class="col-md-1">

                </div>

                <div class="col-md-1">
                  <input type="text" name="bill_no2" id="bill_no2" class="form-control">
                </div>

                <div class="col-md-1">
                    <b><font color="red">เลขที่สลิป*</font></b>
                </div>
              </div>
            <?php //} ?>

            <div class="row">
                <br>
            </div>

            <?php //if($databank[3]->number_bank) {  ?>
              <div class="row">
                <div class="col-md-2"  align="right">
                    <b><?php echo $databank[3]->name_bank; ?></b>
                </div>

                <div class="col-md-1"  align="right">
                    <b><?php echo $databank[3]->name; ?></b>
                </div>

                <div class="col-md-2" align="center">
                      <?php echo $databank[3]->number_bank; ?>
                </div>

                <div class="col-md-2">
                  <input type="text" name="grandtotal3" id="grandtotal3" class="form-control">
                </div>

                <div class="col-md-1">
                  บาท
                </div>

                <div class="col-md-1">
                  <input type="file"  name="image[]" id="image3" value="3">

                  <!-- <input type="hidden" name="brcode" value="<?php //echo $brcode; ?>" class="form-control">
                  <input type="hidden" name="empcode" value="<?php //echo $empcode; ?>" class="form-control">
                  <input type="hidden" name="status" value="1" class="form-control"> -->
                </div>

                <div class="col-md-1">

                </div>

                <div class="col-md-1">
                  <input type="text" name="bill_no3" id="bill_no3" class="form-control">
                </div>

                <div class="col-md-1">
                    <b><font color="red">เลขที่สลิป*</font></b>
                </div>
              </div>
            <?php //} ?>

            <div class="row">
                <br>
            </div>

            <?php if($databank) {  ?>
              <div class="row">
                <div class="col-md-3"  align="right">

                </div>

                <div class="col-md-2" align="center">
                    <b>รวม</b>
                </div>

                <div class="col-md-2">
                  <input type="text" name="total" id="total" value="" class="form-control" readonly>
                </div>

                <div class="col-md-1">
                  บาท
                </div>

                <div class="col-md-1">
                  <input type="hidden" name="brcode" value="<?php echo $brcode; ?>" class="form-control">
                  <input type="hidden" name="empcode" value="<?php echo $empcode; ?>" class="form-control">
                  <input type="hidden" name="status" value="1" class="form-control">
                </div>

                <div class="col-md-3">

                </div>
              </div>
            <?php } ?>

            <div class="row">
                <br>
            </div>

              <div class="row">
                <div class="col-md-3">

                </div>

                <div class="col-md-2" align="center">
                  <b>วันที่นำฝาก</b>
                </div>

                <div class="col-md-2" >
                  <input type="text" name="datepicker" id="datepicker" value="<?php echo date('d/m/Y');?>" class="form-control datepicker" required readonly>
                </div>

                <div class="col-md-3">

                </div>
              </div>

            <div class="row">
                <br>
            </div>

              <div class="row">
                <div class="col-md-3">

                </div>
                <div class="col-md-2">

                </div>
                <div class="col-md-2" align="center">
                    <input type="submit" name="transfer" class="btn btn-info" value="Submit">

                </div>
                <div class="col-md-4" >

                </div>
                <div class="col-md-1" >

                </div>
              </div>

            </form>

            <div class="row">
                <br>
            </div>


                <!-- Flow ของออดิทที่ทำการตรวจเช็ค -->
                <div class="row">
                  <div class="col-md-12">
                    <?php
                    if(isset($query)){
                        // echo "<pre>";
                        // print_r($data);
                        // print_r($dataenpo);
                        // print_r($dataatm);
                        // exit;
                      ?>
                    <form action="configgetcashchech" onSubmit="if(!confirm('ยืนยันการทำรายการ?')){return false;}" method="post" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <table class="table table-striped">
                    <thead>
                      <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                      </tr>
                      <tr>
                        <th><center>ยอดเงินที่ต้องนำฝาก</center></th>
                        <th><center>เลขที่สลิป</center></th>
                        <th><center>ยอดเงินที่นำฝากธนาคาร (แนบสลิป)</center></th>
                        <th></th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>

                     <?php  //ยอดยกมา

                       $brcode = $branch_id;
                       $datetime = $datepicker2;

                       $db = Connectdb::Databaseall();

                       $sql = 'SELECT '.$db['fsctaccount'].'.cash.*
                           FROM '.$db['fsctaccount'].'.cash

                           WHERE '.$db['fsctaccount'].'.cash.timeold LIKE "%'.$datetime.'%"
                             AND '.$db['fsctaccount'].'.cash.branch_id = '.$branch_id.'
                             AND '.$db['fsctaccount'].'.cash.status NOT IN (99)
                             ';
                       $datatresult = DB::connection('mysql')->select($sql);

                       $sqlcheck = 'SELECT '.$db['fsctaccount'].'.checkbank.*
                           FROM '.$db['fsctaccount'].'.checkbank

                           WHERE '.$db['fsctaccount'].'.checkbank.time LIKE "%'.$datetime.'%"
                             AND '.$db['fsctaccount'].'.checkbank.branch_id = '.$branch_id.'
                             AND '.$db['fsctaccount'].'.checkbank.status NOT IN (99)
                             ';
                       $datatresultcheck = DB::connection('mysql')->select($sqlcheck);

                     ?>
                     <input type="hidden" name="brcode" id="brcode" value="<?php echo $brcode;?>">
                     <input type="hidden" name="datepicker" value="<?php echo $datetime;?>">
                     <tr>
                       <td align="center">
                         <?php
                           if($datatresult){
                             // $grandtotal = $dataresult[0]->grandtotal;
                             $date = explode("=", $datatresult[0]->note);
                             echo $date[1];
                           }
                         ?>
                       </td>
                       <td>
                         <?php if($datatresultcheck){
                           echo $datatresultcheck[0]->bill_no;
                         }?>
                       </td>
                       <td align="center">
                         <?php if($datatresultcheck){ ?>
                           <img src="http://103.13.231.24/fsctaccounting/public/checkcash/<?php echo $datatresultcheck[0]->image;?>" alt="User Image" style="width: 80%;height: 40%;">
                           <!-- <img src="http://localhost/fsctaccount/public/checkcash/<?php //echo $datatresultcheck[0]->image;?>" alt="User Image" style="width: 40%;height: 60%;"> -->
                         <?php } ?>
                       </td>
                       <td align="center">
                         <?php
                           if($datatresultcheck){
                             //echo $datatresultcheck[0]->grandtotal;
                         ?>
                         <input type="text" name="sumgrand1" id="sumgrand1" value="<?php echo $datatresultcheck[0]->grandtotal; ?>" class="form-control">

                       </td>
                       <td><input type="checkbox" name="checkgrandtotal" value="">
                       <?php } ?>
                       </td>

                     <tr>

                     <tr>
                       <td></td>
                       <td>
                         <?php if($datatresultcheck){
                           echo $datatresultcheck[0]->bill_no2;
                         }?>
                       </td>
                       <td align="center">
                         <?php if($datatresultcheck){ ?>
                           <img src="http://103.13.231.24/fsctaccounting/public/checkcash/<?php echo $datatresultcheck[0]->image1;?>" alt="User Image" style="width: 80%;height: 40%;">
                           <!-- <img src="http://localhost/fsctaccount/public/checkcash/<?php //echo $datatresultcheck[0]->image;?>" alt="User Image" style="width: 40%;height: 60%;"> -->
                         <?php } ?>
                       </td>
                       <td align="center">
                         <?php
                           if($datatresultcheck){
                             // $grandtotal = $dataresult[0]->grandtotal;
                             //echo $datatresultcheck[0]->grandtotal1;
                         ?>
                         <input type="text" name="sumgrand2" id="sumgrand2" value="<?php echo $datatresultcheck[0]->grandtotal1; ?>" class="form-control">

                       </td>
                       <td><input type="checkbox" name="checkgrandtotal1" value="">
                       <?php } ?>
                       </td>

                     <tr>

                     <tr>
                       <td></td>
                       <td>
                         <?php if($datatresultcheck){
                           echo $datatresultcheck[0]->bill_no3;
                         }?>
                       </td>
                       <td align="center">
                         <?php if($datatresultcheck){ ?>
                           <img src="http://103.13.231.24/fsctaccounting/public/checkcash/<?php echo $datatresultcheck[0]->image2;?>" alt="User Image" style="width: 80%;height: 40%;">
                           <!-- <img src="http://localhost/fsctaccount/public/checkcash/<?php //echo $datatresultcheck[0]->image;?>" alt="User Image" style="width: 40%;height: 60%;"> -->
                         <?php } ?>
                       </td>
                       <td align="center">
                         <?php
                           if($datatresultcheck){
                             // $grandtotal = $dataresult[0]->grandtotal;
                             //echo $datatresultcheck[0]->grandtotal2;
                         ?>
                         <input type="text" name="sumgrand3" id="sumgrand3" value="<?php echo $datatresultcheck[0]->grandtotal2; ?>" class="form-control">

                       </td>
                       <td><input type="checkbox" name="checkgrandtotal2" value="">
                       <?php } ?>
                       </td>

                     <tr>

                     <tr>
                       <td></td>
                       <td>
                         <?php if($datatresultcheck){
                           echo $datatresultcheck[0]->bill_no4; 
                         }?>
                       </td>
                       <td align="center">
                         <?php if($datatresultcheck){ ?>
                           <img src="http://103.13.231.24/fsctaccounting/public/checkcash/<?php echo $datatresultcheck[0]->image3;?>" alt="User Image" style="width: 80%;height: 40%;">
                           <!-- <img src="http://localhost/fsctaccount/public/checkcash/<?php //echo $datatresultcheck[0]->image;?>" alt="User Image" style="width: 40%;height: 60%;"> -->
                         <?php } ?>
                       </td>
                       <td align="center">
                         <?php
                           if($datatresultcheck){
                             // $grandtotal = $dataresult[0]->grandtotal;
                             //echo $datatresultcheck[0]->grandtotal3;
                         ?>
                         <input type="text" name="sumgrand4" id="sumgrand4" value="<?php echo $datatresultcheck[0]->grandtotal3; ?>" class="form-control">

                       </td>
                       <td><input type="checkbox" name="checkgrandtotal3" value="">
                       <?php } ?>
                       </td>

                     <tr>

                     <tr>
                       <td colspan="2" align="center">
                       <td colspan="1" ><b><br>
                         <?php
                             if($datatresultcheck){
                             //echo $datatresultcheck[0]->total;
                         ?>
                         <input type="text" name="sumgrand" id="sumgrand" value="<?php echo $datatresultcheck[0]->total; ?>" class="form-control" readonly>
                        <?php } ?>
                       </b></td>
                       <td colspan="1" ><b><br>
                     </tr>

                </tbody>
              </table>

                  <div class="row">
                      <div class="col-md-3">

                      </div>
                      <div class="col-md-2">

                      </div>
                      <div class="col-md-2">
                        <?php
                        if($datatresult && $datatresultcheck){
                            $dateresult = explode("=", $datatresult[0]->note);
                            $date = explode(".", $dateresult[1]);
                            $date2 = explode(".", $datatresultcheck[0]->total);
                            $total1 = $date[0]; // ยอดเงินที่ต้องนำฝาก (ในระบบ)
                            $total2 = $date2[0]; // ยอดเงินที่นำฝากธนาคาร (แนบสลิป)
                            // exit;

                          // if($total1 == $total2){ ?>
                          <input type="text" name="grandtotal" value="<?php echo $dateresult[1]; ?>" class="form-control">
                          <br>
                          <center><input type="submit" name="transfer" class="btn btn-info" value="Submit"></center>
                          <input type="hidden" name="point" value="<?php echo $dateresult[1] - $datatresultcheck[0]->total; ?>" class="form-control">
                        </div>
                        <div class="col-md-4" >
                          บาท
                        <?php //} ?>
                        </div>

                        <?php } ?>
                        <div class="col-md-1" >

                        </div>
                    </div>

                </div>

             </form>

             <?php } ?>

            </div>
          </div>
        </div>

    </section>
    <!-- /.content -->
</div>
@include('footer')


<!-- Modal -->

<!-- Modal -->
