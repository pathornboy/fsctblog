<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;

use App\Api\Datetime;
use App\working_company;
use Illuminate\Support\Facades\Input;

?>

<link>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}
<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>

<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">

<script type="text/javascript" src = 'js/vendor/vendorcenter.js'></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

    <style>
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }

        body {
            font-family: "THSarabunNew";
            font-size: 10px;
        }
        h4 {
            font-family: "THSarabunNew";
        }
        h4 {
            font-family: "THSarabunNew";
        }

    </style>

<style>
    .modal-ku {
        width: 100%;
        margin: auto;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="box-body">

            <?php

              $data = Input::all();
              $db = Connectdb::Databaseall();
              // echo "<pre>";
              // print_r($data);
              // exit;

              $datepicker = explode("-",trim(($data['datepicker'])));

              $datepickerstart = explode("/",trim(($datepicker[0])));
              if(count($datepickerstart) > 0) {
                  $datetime = $datepickerstart[1] . '-' . $datepickerstart[0]; //วัน - เดือน
              }

              $datepickerend = explode("/",trim(($datepicker[1])));
              if(count($datepickerend) > 0) {
                  $datetime2 = $datepickerend[1] . '-' . $datepickerend[0]; //วัน - เดือน
              }

              if($datepickerstart[0] == "01"){$monthTH = "มกราคม";
                }else if($datepickerstart[0] == "02"){$monthTH = "กุมภาพันธ์";
                }else if($datepickerstart[0] == "03"){$monthTH = "มีนาคม";
                }else if($datepickerstart[0] == "04"){$monthTH = "เมษายน";
                }else if($datepickerstart[0] == "05"){$monthTH = "พฤษภาคม";
                }else if($datepickerstart[0] == "06"){$monthTH = "มิถุนายน";
                }else if($datepickerstart[0] == "07"){$monthTH = "กรกฎาคม";
                }else if($datepickerstart[0] == "08"){$monthTH = "สิงหาคม";
                }else if($datepickerstart[0] == "09"){$monthTH = "กันยายน";
                }else if($datepickerstart[0] == "10"){$monthTH = "ตุลาคม";
                }else if($datepickerstart[0] == "11"){$monthTH = "พฤศจิกายน";
                }else if($datepickerstart[0] == "12"){$monthTH = "ธันวาคม";
                }

                $modelname = Maincenter::databranchbycode($data['branch']);

                $compid = $modelname[0]->company_id;
                $sqlcompany = "SELECT * FROM $db[hr_base].working_company  WHERE id ='$compid' ";
                $datacomp = DB::connection('mysql')->select($sqlcompany);

                $branch_id = $data['branch'];
                $sql = "SELECT * FROM $db[hr_base].branch  WHERE code_branch ='$branch_id' ";
                $databranch = DB::connection('mysql')->select($sql);

            ?>

          <div class="row">
              <div class="col-md-12">
                  <div class="box box-primary">
                      <div class="breadcrumbs" id="breadcrumbs">
                          <ul class="breadcrumb">
                              <div align="center">



                              <table width="100%">
                                <tr>
                                  <td align="center" ><b>รายงานภาษีขาย (เงินประกัน)</b></td>
                                </tr>

                                <tr>
                                  <td align="center" ><b>เดือนภาษี {{$monthTH}} ปี {{$datepickerstart[2]}}</b></td>
                                </tr>

                                <tr>
                                  <td align="center" ><b>ชื่อผู้ประกอบการ : บริษัท ฟ้าใสคอนสตรัคชั่นทูลส์ จำกัด ({{$modelname[0]->name_branch}})</b></td>
                                </tr>

                                <tr>
                                  <td align="right" ><b>เลขประจำตัวผู้เสียภาษีอากร {{$datacomp[0]->inv_number}}</b></td>
                                </tr>

                                <?php if($modelname[0]->branch_tax == "สำนักงานใหญ่"){?>
                                <tr>
                                  <td align="right" ><b>X สำนักงานใหญ่    สาขา</b></td>
                                </tr>
                                <?php } ?>

                                <?php if($modelname[0]->branch_tax != "สำนักงานใหญ่"){?>
                                <tr>
                                  <td align="right" ><b>  สำนักงานใหญ่  X สาขา{{$modelname[0]->branch_tax}}</b></td>
                                </tr>
                                <?php } ?>

                                <tr>
                                  <td align="left" ><b>ที่อยู่สถานประกอบการ : {{$modelname[0]->addresstax}}</b></td>
                                </tr>

                              </table>
              </div>
              </div>
              </ul><!-- /.breadcrumb -->
              <!-- /section:basics/content.searchbox -->
              </div>

							<div align="center">
							<?php

              $data = Input::all();
              $db = Connectdb::Databaseall();
              // echo "<pre>";
              // print_r($data);
              // exit;

              // $branch_id = $data['branch_id'];
              // $start_date = $data['datepickerstart']." 00:00:00";
              // $end_date = $data['datepickerend']." 23:59:59";

              $datepicker = explode("-",trim(($data['datepicker'])));

              // $start_date = $datepicker[0];
              $e1 = explode("/",trim(($datepicker[0])));
                      if(count($e1) > 0) {
                          $start_date = $e1[2] . '-' . $e1[0] . '-' . $e1[1]; //ปี - เดือน - วัน
                          $start_date2 = $start_date." 00:00:00";
                      }

              // $end_date = $datepicker[1];
              $e2 = explode("/",trim(($datepicker[1])));
                      if(count($e2) > 0) {
                          $end_date = $e2[2] . '-' . $e2[0] . '-' . $e2[1]; //ปี - เดือน - วัน
                          $end_date2 = $end_date." 23:59:59";
                      }

              $branch_id = $data['branch'];

              // echo "<pre>";
              // print_r($start_date);
              // print_r($end_date);
              // exit;

              $sqlinsurance = 'SELECT '.$db['fsctaccount'].'.taxinvoice_insurance.*

                     FROM '.$db['fsctaccount'].'.taxinvoice_insurance

                      WHERE '.$db['fsctaccount'].'.taxinvoice_insurance.branch_id = "'.$branch_id.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_insurance.time BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_insurance.status = 1
                        ORDER BY '.$db['fsctaccount'].'.taxinvoice_insurance.number_taxinvoice ASC
                     ';

              $datataxinsurance = DB::connection('mysql')->select($sqlinsurance);


              $sqlinsurancecn = 'SELECT '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.*

                     FROM '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote

                      WHERE '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.branch_id = "'.$branch_id.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.time  BETWEEN "'.$start_date2.'" AND  "'.$end_date2.'"
                        AND '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.status = 1
                        ORDER BY '.$db['fsctaccount'].'.taxinvoice_insurance_creditnote.number_taxinvoice ASC
                     ';

              $datataxinsurancecn = DB::connection('mysql')->select($sqlinsurancecn);
              // echo "<pre>";
              // print_r($datatresult);
              // exit;

              ?>


              <div align="center" >

							<?php //echo $week;?>
              <!-- <div class="row">
                  <div class="col-md-5">
                      <input type="hidden" name="settime" id="settime" value="<?php //echo date('Y-m-d')?>">
                      <input type="hidden" name="setlogin" id="setlogin" value="<?php //echo $emp_code = Session::get('emp_code')?>">
                  </div>
              </div> -->

              <!-- <table class="table table-striped table-bordered" width="100%" border="1" cellpadding="0"> -->
              <!-- <font size="1" > -->
              <table class="table table-bordered" width="100%" border="1" cellspacing="0">
                  <thead class="thead-inverse" >
                  <tr>
                    <th align="center">ลำดับ</th>
                    <th align="center">ปี/เดือน/วัน</th>
                    <th align="center">เลขที่</th>
                    <th align="center">ชื่อผู้ซื้อสินค้า/ผู้รับบริการ</th>
                    <th align="center">เลขประจำตัวผู้เสียภาษี</th>
                    <th align="center">สถานประกอบการ</th>
                    <th align="center">มูลค่าสินค้าหรือบริการ</th>
                    <th align="center">จำนวนเงินภาษีมูลค่าเพิ่ม</th>
                    <th align="center">จำนวนเงินรวมทั้งหมด</th>
                    <th align="center">หมายเหตุ</th>
                    <!-- <th align="center">เลขที่</th> -->
                  </tr>
                  </thead>
                  <tbody>
                  <?php

                  $sumtotalloss = 0;
                  $vat = 0;
                  $i = 1;
                  $sumsubtotal = 0;
                  $sumvat = 0;
                  $sumgrandtotal = 0;

                  $sumsubtotal2 = 0;
                  $sumvat2 = 0;
                  $sumgrandtotal2 = 0;

                  $j = 1;

                  ?>

                  <? foreach ($datataxinsurance as $key => $value) { ?>
                    <tr>
                       <td><?php echo $i;?></td><!--ลำดับ-->
                       <td><?php echo ($value->time);?></td><!--ปี/เดือน/วัน-->
                       <td><?php echo ($value->number_taxinvoice);?></td><!--เลขที่-->
                       <td>
                         <?php
                         $modelcustomerra = Maincenter::getdatacustomer($value->customerid);
                               if($modelcustomerra){
                                 echo ($modelcustomerra[0]->per);
                                 echo " ";
                                 echo ($modelcustomerra[0]->name);
                                 echo " ";
                                 echo ($modelcustomerra[0]->lastname);
                               }

                         ?>
                       </td>
                       <td align="center" ><?php echo ($value->customerid);?></td>
                       <td><?php //echo ($modelsupplier[0]->type_branch);?></td>

                       <td align="right" >
                         <?php
                           echo number_format($value->subtotal, 2);
                           $sumsubtotal = $sumsubtotal + ($value->subtotal);
                         ?>
                       </td>

                       <td align="right" >
                         <?php
                           echo number_format ($value->vatmoney,2);
                           $sumvat = $sumvat + $value->vatmoney;
                         ?>
                       </td>

                       <td align="right" >
                         <?php
                           echo number_format ($value->grandtotal,2);
                           $sumgrandtotal = $sumgrandtotal + $value->grandtotal;
                         ?>
                       </td>
                       <td align="center" >
                         <?php
                           // echo ($value->note);
                             echo "-";
                         ?>
                       </td>
                       <!-- <td></td> -->
                    </tr>

                  <?php $i++; } ?>

                  <?
                  foreach ($datataxinsurancecn as $key2 => $value2) { ?>

                  <tr>
                     <td><?php echo $i;?></td><!--ลำดับ-->
                     <td><?php echo ($value2->time);?></td><!--ปี/เดือน/วัน-->
                     <td><?php echo ($value2->number_taxinvoice);?></td><!--เลขที่-->
                     <td>
                       <?php
                       $modelcustomertf = Maincenter::getdatacustomer($value2->customerid);
                             if($modelcustomertf){
                               echo ($modelcustomertf[0]->per);
                               echo " ";
                               echo ($modelcustomertf[0]->name);
                               echo " ";
                               echo ($modelcustomertf[0]->lastname);
                             }

                       ?>
                     </td>
                     <td align="center" ><?php echo ($value2->customerid);?></td>
                     <td align="center" ><?php //echo ($value2->branch_id);?></td>

                     <td align="right" >
                       <?php

                         // if($value2->discountmoney){
                         //    echo number_format(-($value2->subtotal - $value2->discountmoney), 2);
                         //    $sumsubtotal = $sumsubtotal + ($value2->subtotal - $value->discountmoney);
                         // }else {
                         //    echo number_format($value2->subtotal, 2);
                         //    $sumsubtotal = $sumsubtotal + ($value2->subtotal);
                         // }

                         echo number_format(-$value2->subtotal, 2);
                         $sumsubtotal2 = $sumsubtotal2 + $value2->subtotal;
                       ?>
                     </td>

                     <td align="right" >
                       <?php
                         echo number_format (-$value2->vatmoney,2);
                         $sumvat2 = $sumvat2 + $value2->vatmoney;
                       ?>
                     </td>

                     <td align="right" >
                       <?php
                         echo number_format (-$value2->grandtotal,2);
                         $sumgrandtotal2 = $sumgrandtotal2 + $value2->grandtotal;
                       ?>
                     </td>
                     <td align="center" >
                       <?php
                         // echo ($value2->note);
                           echo "-";
                       ?>
                     </td>
                     <!-- <td></td> -->
                  </tr>

                  <?php $i++; } ?>

                  <tr>
                    <td colspan="5" align="right"></td>
                    <td><center><b>รวม TI</b></center></td>
                    <td align="center"><b><?php echo number_format($sumsubtotal,2);  ?></b></td>
                    <td align="center"><b><?php echo number_format($sumvat,2);  ?></b></td>
                    <td align="center"><b><?php echo number_format($sumgrandtotal,2); ?></b></td>
                    <td></td>
                    <!-- <td></td> -->
                  </tr>

                  <tr>
                    <td colspan="5" align="right"></td>
                    <td><center><b>รวม CI</b></center></td>
                    <td align="center"><b><?php echo number_format(-$sumsubtotal2,2);  ?></b></td>
                    <td align="center"><b><?php echo number_format(-$sumvat2,2);  ?></b></td>
                    <td align="center"><b><?php echo number_format(-$sumgrandtotal2,2); ?></b></td>
                    <td></td>
                    <!-- <td></td> -->
                  </tr>

                  <tr>
                    <td colspan="5" align="right"></td>
                    <td><center><b>รวมทั้งสิ้น</b></center></td>
                    <td align="center"><b><?php echo number_format($sumsubtotal - $sumsubtotal2,2);  ?></b></td>
                    <td align="center"><b><?php echo number_format($sumvat - $sumvat2,2);  ?></b></td>
                    <td align="center"><b><?php echo number_format($sumgrandtotal - $sumgrandtotal2,2); ?></b></td>
                    <td></td>
                    <!-- <td></td> -->
                  </tr>

                      </tbody>
                   </table>
                   <!-- </font> -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
