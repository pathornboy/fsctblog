<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;

?>
@include('headmenu')
<link>

{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}
<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>

<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>

<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">

<script type="text/javascript" src = 'js/vendor/vendorcenter.js'></script>

<link rel="stylesheet" type="text/css" href="bower_components/select2/dist/css/select2.min.css">
<script type="text/javascript" src = 'bower_components/select2/dist/js/select2.full.min.js'></script>


<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	
	$(document).ready(function(){
		// console.log("TEST");
		$(".week").hide();
		$(".month").hide();
		$(".status").hide();
		
		$('input[name="choice"]').change(function () {
			if (this.value == 1) {
				$(".week").show();
				$(".month").hide();
				$(".status").hide();
				
				$("#month").val('');
				$("#year").val('');
				$("#status").val('');
				
				$("#week").attr('required',true);
				$("#month").attr('required',false);
				$("#year").attr('required',false);
				$("#status").attr('required',false);
				// console.log("TEST");
			}else if(this.value == 2){
				$(".week").hide();
				$(".status").hide();
				$(".month").show();
				
				$("#status").val('');
				$("#week").val('');
				
				$("#month").attr('required',true);
				$("#year").attr('required',true);
				$("#week").attr('required',false);
				$("#status").attr('required',false);
				// console.log("YYYY");
			}else if(this.value == 3){
				$(".week").hide();
				$(".month").hide();
				$(".status").show();
				
				$("#month").val('');
				$("#year").val('');
				$("#week").val('');
				
				$("#status").attr('required',true);
				$("#month").attr('required',false);
				$("#year").attr('required',false);
				$("#week").attr('required',false);
				// console.log("XXXXX");
			}
		});
	});
	
</script>
<style>
    .modal-ku {
        width: 90%;
        margin: auto;
    }
</style>
<style type="text/css" media="print">
input{
    display:none;
}
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">งานจัดซื้อ</a>
                    </li>
                    <li class="active">รายงานใบสั่งของพนักงาน (PR)</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        รายงานใบสั่งของพนักงาน (PR)
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body">
								<div class="row">
									<div class="col-md-2 text-right">
										<b>เลือกรูปแบบการค้นหา : </b>
									</div>
									<div class="col-md-6">
										<form method="GET" action="<?php echo url("/prcenter"); ?>">
											<input type="radio" name="choice" id="choice" value="1" /> &nbsp;ค้นหาจากรอบการขอ &nbsp;&nbsp;&nbsp;&nbsp;
											<input type="radio" name="choice" id="choice" value="2" /> &nbsp;ค้นหาจากเดือนปี &nbsp;&nbsp;&nbsp;&nbsp;
											<input type="radio" name="choice" id="choice" value="3" /> &nbsp;ค้นหาจากสถานะ &nbsp;&nbsp;&nbsp;&nbsp;
										
										</br></br>
									</div>
								</div>
								<div class="row ">
							<!-- เลือกรอบการขอ-->		
									<div class="col-md-2 col-xs-2 text-right week">
										<b>เลือก รอบการขอ : </b>
									</div>
									<div class="col-md-2 col-xs-2 week">
										
										  <select name="week" id="week" class="form-inline select2" >
											  <option value="" selected>เลือกรอบการขอ</option>
											  <?php
												$db = Connectdb::Databaseall();
												$dataweek = DB::connection('mysql')->select('SELECT week FROM '.$db['fsctaccount'].'.ppr_head GROUP BY week ORDER BY ppr_head.week DESC');
											  ?>
											  @foreach($dataweek as $week)
												<option value="{{$week->week}}">{{$week->week}}</option>
											  @endforeach
										  </select>
										 
									</div>
							<!-- เลือกเดือน ปี-->		
									<div class="col-md-2 col-xs-2 text-right month">
										<b>เลือก เดือน : &nbsp;&nbsp;&nbsp;</b>
									</div>
									<div class="col-md-2 col-xs-2 text-left month">
										<select name="month" id="month" class="form-inline" >
											<option value="" selected>--เลือกเดือน--</option>
											<option value="01">มกราคม</option>
											<option value="02">กุมภาพันธ์</option>
											<option value="03">มีนาคม</option>
											<option value="04">เมษายน</option>
											<option value="05">พฤษภาคม</option>
											<option value="06">มิถุนายน</option>
											<option value="07">กรกฎาคม</option>
											<option value="08">สิงหาคม</option>
											<option value="09">กันยายน</option>
											<option value="10">ตุลาคม</option>
											<option value="11">พฤศจิกายน</option>
											<option value="12">ธันวาคม</option>
										</select>
									</div>
					<?php
						$year = date('Y');
						$yearTH = $year+543;
					?>	
									<div class="col-md-1 col-xs-1 text-left month">
										<b>เลือก ปี : &nbsp;&nbsp;&nbsp;</b>
									</div>
									<div class="col-md-2 col-xs-2 text-left month">
										<select name="year" id="year" class="form-inline" >
											<option value="" selected>--เลือกปี--</option>
									<?php
										for($x=2;$x>0;$x--){
											echo "<option value='".($year-$x)."'>".($yearTH-$x)."</option>";
										}
											echo "<option value='".$year."'>".$yearTH."</option>";
										for($y=1;$y<3;$y++){
											echo "<option value='".($year+$y)."'>".($yearTH+$y)."</option>";
											
										}
									?>
										</select>
									</div>
							<!-- เลือกสถานะ-->		
									<div class="col-md-2 col-xs-2 text-right status">
										<b>เลือก รอบสถานะ : </b>
									</div>
									<div class="col-md-2 col-xs-2 status">
										
										  <select name="status" id="status" class="form-inline select2" >
											  <option value="" selected>เลือกสถานะ</option>
											  <?php
												$db = Connectdb::Databaseall();
												$datastatus = DB::connection('mysql')->select('SELECT numberstatus, notstatus FROM '.$db['fsctaccount'].'.accountstatusforprogram GROUP BY numberstatus ORDER BY numberstatus ASC');
											  ?>
											  @foreach($datastatus as $status)
												<option value="{{$status->numberstatus}}"> {{$status->notstatus}}</option>
											  @endforeach
										  </select>
										 
									</div>
									
							<!-- เลือกสาขา -->
									<div class="col-md-2 col-xs-2 text-right">
										<b>เลือก สาขา : </b>
									</div>
									<div class="col-md-2 col-xs-2 ">
										  <select name="branch" id="branch" class="form-inline select2" required>
											  <option value="" selected>เลือกสาขา</option>
											  <option value="99">ทุกสาขา</option>
											  <?php
												$db = Connectdb::Databaseall();
												$databranch = DB::connection('mysql')->select('SELECT code_branch,name_branch FROM '.$db['hr_base'].'.branch GROUP BY code_branch ORDER BY branch.code_branch ASC');
											  ?>
											  @foreach($databranch as $branch)
												<option value="{{$branch->code_branch}}">{{$branch->name_branch}}</option>
											  @endforeach
										  </select> &nbsp;&nbsp;&nbsp;
										  
									</div>
									
									<div class="col-md-1 col-xs-1">
										 <input type="submit" class="form-inline btn-success" id="search" value="Search" />
										</form> 
									</div>
								</div>
								<div class="row">
								
								</div>
                                <div class="row">
                                    <!--<div class="col-md-10">
                                        <div class="pull-right">
                                            <a href="<?php //echo url("/vendoraddbill");?>"><img src="images/global/invoice.png">เพิ่มรายการจัดซื้อ</a>
                                        </div>
                                    </div>-->
								
                                    <div class="col-md-5">
                                        <input type="hidden" name="settime" id="settime" value="<?php echo date('Y-m-d')?>">
                                        <input type="hidden" name="setlogin" id="setlogin" value="<?php echo $emp_code = Session::get('emp_code')?>">
                                        <!--<a href="#" title="เพิ่มข้อมูล" data-toggle="modal" data-target="#myModal" onclick="insertnew()" ><img src="images/global/add.png">เพิ่มข้อมูล</a>-->
                                    </div>
                                </div>								
                                <table id="example" class="table table-striped table-bordered">
                                    <thead class="thead-inverse">
                                    <tr>
                                        <td>#</td>
                                        <td>เลขใบ PR</td>
                                        <td align="center">วันเดือนปี</td>
                                        <td align="center">ราคา</td>
                                        <td align="center">จำนวน</td>
                                        <td align="center">vat (บาท)</td>
                                        <td align="center">หัก ณ ที่จ่าย (บาท)</td>
                                        <td align="center">ประเภทการซื้อ</td>
                                        <td align="center">ประเภทการจ่าย</td>
										<td align="center">รวม</td>
                                        
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
										$db = Connectdb::Databaseall();
										
										if((isset($_GET['branch']))){
											$week = $_GET['week'];
											$month = $_GET['month'];
											$year = $_GET['year'];
											$branch = $_GET['branch'];
											$status = $_GET['status'];
										}else{
											$week = '';
											$month = '';
											$year = '';
											$branch = '';
											$status = '';
										}
										
										// $week = '2018-02-20 10:00:00';
										
									if((isset($_GET['branch'])=='') ){
										
										$data = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.ppr_head as H
										INNER JOIN '.$db['fsctaccount'].'.ppr_detail as D on H.id = D.ppr_headid
										INNER JOIN '.$db['fsctaccount'].'.type_pay AS P on H.type_pay = P.id
										INNER JOIN '.$db['fsctaccount'].'.type_buy AS B on H.type_buy = B.id 
										where D.status = 1');
										
										$sum = DB::table('ppr_detail')
													->where('status', '=', 1)
													->sum('total');
										
										// $sumpayreal = DB::table('ppr_detail')
													// ->where('status', '=', 1)
													// ->sum('total');
										
									}else{
																		
										if($_GET['branch'] == '99'){	
											$other = "";
										}else{
											$other = " AND H.branch_id = '$branch'";
										}
										
										$sql = "SELECT *
												FROM $db[fsctaccount].ppr_head AS H
												INNER JOIN $db[fsctaccount].ppr_detail AS D on H.id = D.ppr_headid
												INNER JOIN $db[fsctaccount].type_pay AS P on H.type_pay = P.id
												INNER JOIN $db[fsctaccount].type_buy AS B on H.type_buy = B.id
												WHERE D.status = 1 
												AND ((H.week = '$week') OR (H.status = '$status') OR (H.date like '____-$month-__' AND H.date like '$year%'))".$other; 
																		
										
										// echo $sql;
										// exit();
										
										$data = DB::connection('mysql')->select($sql);		
																				
										$sum = DB::table('ppr_detail')
													->where('status', '=', 1)
													->sum('total');
										
										// $sumpayreal = DB::table('po_head')
													// ->where('status_head', '=', 2)
													// ->where('week', '=', '$week')
													// ->where('branch_id', '=', '$branch')
													// ->sum('payreal');
										// $count = DB::table('ppr_head')
												// ->select('ppr_head.id')
												// ->join('ppr_detail', 'ppr_head.id', '=', 'ppr_detail.ppr_headid')
												// ->where('ppr_detail.status', '=', '1')
												// ->where('ppr_head.week', '=', $week)
												// ->where('ppr_head.branch_id', '=', $branch)
												// ->orWhere('ppr_head.status', '=', $status)
												// ->orWhere('ppr_head.date', 'like', '____-'.$month.'-__')
												// ->whereYear('ppr_head.date', 'like', $year)
												// ->count();			
									}
										$i = 1;
										
                                    ?>
                                    @foreach ($data as $value)
                                        <tr>
                                            <td scope="row">{{ $i }}</td>
                                            <td align="left">{{ $value->number_ppr }}</td>
                                            <td align="center">{{ $value->date }}</td>
                                            <td align="center">{{ number_format($value->price,2) }}</td>
                                            <td align="center">{{ $value->amount }}</td>
                                            <td align="center">{{ number_format(($value->vat * ($value->price * $value->amount))/100,2) }}</td>
                                            <td align="center">{{ number_format(((($value->vat * ($value->price * $value->amount))/100) * $value->withhold)/100,2) }}</td>
                                            <td align="center">{{$value->name_buy}}</td>                                           
                                            <td align="center">{{$value->name_pay}}</td>
											<td align="right">{{ number_format($value->total,2)}}</td>
											
                                        </tr>
										
                                        <?php $i++; ?>
                                    @endforeach
                                    </tbody>
										<tr>
											<td colspan="8" align="right"><U><B><?php //echo $count;?></B></U></td>
											<td colspan="8" align="right"><U><B>รวมทั้งหมด : </B></U></td>
											<td colspan="2" align="right"><U><B>{{ number_format($sum,2)}}</B></U></td>
										</tr>
                                </table>
								<?php //print_r($data);?>
								<p align="center">
									<?php $path = 'week='.$week.'&month='.$month.'&year='.$year.'&status='.$status.'&branch='.$branch?>
									<a href="<?php echo url("/prprint?$path");?>" target="_blank">
									<button class="btn btn-success">PRINT REPORT</button>
									</a>
									
									<a href="<?php echo url("/downloadExcelPR?$path"); ?>">
									<button class="btn btn-success">Download Excel xls</button>
									</a>
								</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')
