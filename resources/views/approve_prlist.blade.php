<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;

$db = Connectdb::Databaseall();
?>
@include('headmenu')
<link>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>


<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">


<link rel="stylesheet" type="text/css" href="bower_components/select2/dist/css/select2.min.css">
<script type="text/javascript" src = 'bower_components/select2/dist/js/select2.full.min.js'></script>

<script type="text/javascript" src = 'js/vendor/detail_choosesupplier.js'></script>


<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">งานจัดซื้อ</a>
                    </li>
                    <li class="active">ข้อมูลออกใบ PO</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs text-center" id="breadcrumbs">
                                <h3>รายการ PR ทั้งหมด</h3>
                            </div>
                            <div class="box-body table-responsive">
                                <div class="row">
                                    <br>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">

                                    </div>
                                    <form action="searchprlist" method="post">
                                    <div class="col-md-3">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <?php
                                           $sqlbranch = 'SELECT '.$db['hr_base'].'.branch.*
                                                        FROM '.$db['hr_base'].'.branch
                                                        WHERE status = "1" ';
                                            $databranch  = DB::connection('mysql')->select($sqlbranch);

                                        ?>
                                        <select class="form-control" name="branch" id="branch" required>
                                          <option value="">เลือกสาขา</option>
                                          <?php foreach ($databranch as $key => $value): ?>
                                            <option value="<?php echo $value->code_branch; ?>" <?php
                                            if(isset($branch)){
                                              $branch = $branch;
                                            }else{
                                              $branch = '';
                                            }
                                            if($value->code_branch==$branch){ echo "selected"; }else{ echo "";}?>
                                            ><?php echo $value->name_branch; ?></option>
                                          <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1">
                                        <input type="submit" class="btn btn-success" value="ค้นหา">
                                    </div>
                                    </form>
                                    <div class="col-md-4">

                                    </div>
                                </div>
                                <div class="row">
                                    <br>
                                </div>
                                      <?php if(isset($data)){?>
                                      <?php
                                          $sql = 'SELECT '.$db['fsctaccount'].'.ppr_head.number_ppr,
                                                     '.$db['fsctaccount'].'.ppr_head.urgent_status,
                                                     '.$db['fsctaccount'].'.ppr_head.date,
                                                     '.$db['fsctaccount'].'.ppr_detail.*
                                             FROM '.$db['fsctaccount'].'.ppr_head
                                             INNER JOIN '.$db['fsctaccount'].'.ppr_detail ON '.$db['fsctaccount'].'.ppr_head.id = '.$db['fsctaccount'].'.ppr_detail.ppr_headid
                                             WHERE  '.$db['fsctaccount'].'.ppr_head.branch_id = '.$branch.'
                                             AND '.$db['fsctaccount'].'.ppr_detail.status = "1"
                                             AND '.$db['fsctaccount'].'.ppr_detail.approvedstatus = "1"
                                             AND '.$db['fsctaccount'].'.ppr_detail.approvedstatusmd = "1"

                                             ORDER BY ppr_detail.id DESC';


                                      $datapr = DB::connection('mysql')->select($sql);
                                        $i=1;
                                      ?>

                                        <form action="approvedtopu" method="post">
                                        <input name="branch" type="hidden" value="<?php echo $branch; ?>">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <table class="table table-stripped text-center">
                                            <thead>
                                              <tr>
                                                <th>ลำดับ</th>
                                                <th>วันที่ขออนุมติ</th>
                                                <th style="text-align: left;">ใบ PR</th>
                                                <th>supplier</th>
                                                <th>รายการ</th>
                                                <th>สถานะการขอ</th>
                                                <th>จำนวนเงิน</th>
                                                <th>สถานะ</th>

                                              </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($datapr as $key => $value) {?>
                                              <tr>
                                                <td>{{$i}}</td>
                                                <td><?php echo $value->date;?></td>
                                                <td><?php echo $value->number_ppr;?></td>
                                                <td>
                                                    <?php
                                                         $id_supplier =  $value->id_supplier;

                                                         $sqlsup =  'SELECT '.$db['fsctaccount'].'.supplier.*
                                                                  FROM '.$db['fsctaccount'].'.supplier
                                                                  WHERE   id = '.$id_supplier.' ' ;

                                                         $datasup= DB::connection('mysql')->select($sqlsup);
                                                         if(!empty($datasup)){
                                                         echo $datasup[0]->pre.'      '.$datasup[0]->name_supplier;
                                                          }

                                                    ?>
                                                </td>
                                                <td><?php echo $value->list;?></td>
                                                <td><?php if($value->urgent_status==1){
                                                          echo "<font color='red'>เร่งด่วน</font>";
                                                        }else if($value->urgent_status==2){
                                                          echo "ปกติ";
                                                        }else if($value->urgent_status==3){
                                                          echo "<font color='red'>เร่งด่วนมาก(บอสอนุมัติ)</font>";
                                                        } ;?>
                                                </td>
                                                <td><?php echo $value->total; ?></td>
                                                <td>
                                                <?php if($value->approvedstatus==0){
                                                          echo "<font color='pink'>รอ</font>";
                                                      }else if($value->approvedstatus==1){
                                                          echo "<font color='green'>อนุมัติ</font>";
                                                      }
                                                ?>
                                                </td>

                                              </tr>
                                            <?php $i++; } ?>
                                          </tbody>
                                        </table>

                                        </form>
                                      <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')
