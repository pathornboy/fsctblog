<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;
$emp_code = Session::get('emp_code');

$readonly = '';
$disable = '';

if($emp_code == '1001' OR $emp_code == '1002' OR $emp_code == '1383' ){
  $readonly = '';
  $disable = '';
}else{
  $readonly = "readonly";
  $disable = 'disabled';
}

?>
@include('headmenu')
<link>
<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}



<link rel="stylesheet" type="text/css" href="css/ui/jquery-ui.css">
{{--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>


<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">


<link rel="stylesheet" type="text/css" href="bower_components/select2/dist/css/select2.min.css">
<script type="text/javascript" src = 'bower_components/select2/dist/js/select2.full.min.js'></script>

<script type="text/javascript" src = 'js/account/cashrent.js'></script>


<style>
    .ui-autocomplete-input {
        border: none;
        font-size: 14px;
        width: 150px;
        height: 24px;
        margin-bottom: 5px;
        padding-top: 2px;
        border: 1px solid #DDD !important;
        padding-top: 0px !important;
        z-index: 1511;
        position: relative;
    }
    .ui-menu .ui-menu-item a {
        font-size: 12px;
    }
    .ui-autocomplete {
        position: absolute;
        top: 0;
        left: 0;
        z-index: 1510 !important;
        float: left;
        display: none;
        min-width: 160px;
        width: 160px;
        padding: 4px 0;
        margin: 2px 0 0 0;
        list-style: none;
        background-color: #ffffff;
        border-color: #ccc;
        border-color: rgba(0, 0, 0, 0.2);
        border-style: solid;
        border-width: 1px;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding;
        background-clip: padding-box;
        *border-right-width: 2px;
        *border-bottom-width: 2px;
    }
    .ui-menu-item > a.ui-corner-all {
        display: block;
        padding: 3px 15px;
        clear: both;
        font-weight: normal;
        line-height: 18px;
        color: #555555;
        white-space: nowrap;
        text-decoration: none;
    }
    .ui-state-hover, .ui-state-active {
        color: #ffffff;
        text-decoration: none;
        background-color: #0088cc;
        border-radius: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        background-image: none;
    }

</style>



<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">งานบัญชี</a>
                    </li>
                    <li class="active">เงินสดย่อย</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        เงินสดย่อย
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body table-responsive">
                                <div class="row">
                                    <div class="col-md-10">

                                    </div>
                                    <div class="col-md-2">
                                        <a href="#" title="เพิ่มข้อมูล" data-toggle="modal" data-target="#myInsert" ><img src="images/global/add.png">เพิ่มข้อมูล</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <br>
                                </div>
                                <?php
                                $db = Connectdb::Databaseall();
                                $datenow = date('Y-m-d 00:00:00');
                                $brcode = Session::get('brcode');
                                $levelemp = Session::get('level_emp');
                                // echo "<pre>";
                                // print_r($levelemp);
                                // exit;
                                $sqlqthead = 'SELECT * FROM '.$db['fsctaccount'].'.insertcashrent ';
                                if($levelemp == 1 || $levelemp == 2 || $emp_code == 1001 || $emp_code == 1337  || $emp_code == 1471 || $emp_code == 2027 || $emp_code == 1475 || $emp_code == 1474 || $emp_code == 1383
                                  || $emp_code == 1514 || $emp_code == 1469 || $emp_code == 1636 || $emp_code == 1036 || $emp_code == 1447 ){

                                      $sqlqthead .= 'WHERE 1';
                                  }else{
                                      $sqlqthead .= 'WHERE 0';
                                  }
                                $sqlqthead .= " AND datetimeinsert > '$datenow'";
                                $sqlqthead .= ' ORDER BY id DESC';

                                $data = DB::connection('mysql')->select($sqlqthead);
                                // echo "<pre>";
                                // print_r($data);
                                // exit;

                                $i = 1;
                                ?>
                                        <table id="example" class="table table-striped table-bordered">
                                            <thead class="thead-inverse">
                                            <tr>
                                                <td class="text-center">#</td>
                                                <td class="text-center">วันที่</td>
                                                <td class="text-center">เงิน</td>
                                                <td class="text-center">ชนิดเอกสาร</td>
                                                <td class="text-center">รหัสพนักงาน</td>
                                                <td class="text-center">สาขา</td>
                                                <td class="text-center">สถานะ</td>
                                                <td class="text-center">สถานะการโอน</td>
                                                <td class="text-center">log</td>
                                                <td class="text-center">ref</td>
                                                <td class="text-center">typereftax</td>
                                                <td class="text-center">แก้ไข</td>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <?php
                                              function typedoc($id){
                                                switch ($id) {
                                                    case '0':
                                                        return "RA";
                                                        break;
                                                    case '1':
                                                        return "RN";
                                                        break;
                                                    case '2':
                                                        return "RL";
                                                        break;
                                                    case '3':
                                                        return "CN";
                                                        break;
                                                    case '4':
                                                        return "TI";
                                                        break;
                                                    case '5':
                                                        return "CI";
                                                        break;
                                                    case '6':
                                                        return "RS";
                                                        break;
                                                    case '7':
                                                        return "CS";
                                                        break;
                                                    case '8':
                                                        return "หัก ณ ที่จ่าย";
                                                        break;
                                                    case '9':
                                                        return "RO";
                                                        break;
                                                    default:
                                                        return "ไม่ทราบ";

                                                }
                                              }

                                              function cashrent_status($id){
                                                if($id == '1') echo "<span style=\"font-weight: bold;\" class=\"text-success\">เพิ่ม</span>";
                                                elseif($id == '2') echo "<span style=\"font-weight: bold;\" class=\"text-warning\">แก้ไข</span>";
                                                elseif($id == '99') echo "<span style=\"font-weight: bold;\" class=\"text-danger\">ยกเลิก</span>";
                                              }

                                              function typetranfer($id){
                                                if($id == '1') return "สด";
                                                elseif($id == '2') return "โอน";
                                              }
                                            ?>

                                            @foreach ($data as $value)
                                                <tr>
                                                    <td scope="row">{{ $i }}</td>
                                                    <td align="left">{{ $value->datetimeinsert}}</td>
                                                    <td class="text-right">{{ $value->money}}</td>
                                                    <td align="center">{{ typedoc($value->typedoc) }}</td>
                                                    <td align="center">{{ $value->emp_code }}</td>
                                                    <td align="center">{{ $value->branch }}</td>
                                                    <td align="center">{{ cashrent_status($value->status) }}</td>
                                                    <td align="center">{{ typetranfer($value->typetranfer) }}</td>
                                                    <td align="center">{{ $value->log }}</td>
                                                    <td align="center">{{ $value->ref }}

                                                        <!-- <a href="#" title="แก้ไขข้อมูล" data-toggle="modal" data-target="#myModal" onclick="getdata(1,{{ $value->id }})"><img src="images/global/edit-icon.png"></a> -->

                                                    </td>
                                                    <td align="center">{{ $value->typereftax }}</td>
                                                    <td>   <a href="#" title="แก้ไข" data-toggle="modal" data-target="#myModal" onclick="getcashrent({{ $value->id }})" ><img src="images/global/edit-icon.png"></a> </td>
                                                </tr>
                                                <?php $i++; ?>
                                            @endforeach
                                            </tbody>
                                        </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')


<!-- Modal -->

<div class="modal fade" id="myModal"  role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog" style="width:90%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">เงินสดย่อย</h5>
            </div>

            <div class="modal-body">

                <form id="configFormvendors" onsubmit="return getdatesubmit();" data-toggle="validator" method="post" class="form-horizontal">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-right">รหัสสาขา <?php echo $brcode = Session::get('brcode');?>
                                <input type="hidden"  class="form-control" name="branch_id" id="branch_id" value="<?php echo $brcode ;?>"  />
                                (
                                <?php
                               $databr = Maincenter::databranchbycode($brcode);
                               print_r($databr[0]->name_branch);
                                ?>)
                            </div>
                        </div>
                    </div>

                    <!-- <div class="row">
                      <div class="col-md-12">
                        ผู้มีหน้าที่หักภาษี ณ ที่จ่าย :
                      </div>
                    </div> -->

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-3 control-label">เงิน<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-9">
                                    <input required type="number" step="0.01"  class="form-control" id="money" value="" name="money" <?php echo $readonly;?>  />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">สถานะการโอน<i><span style="color: red">*</span></i></label>
                                    <div class="col-xs-8">
                                        <!-- <input class="form-control" id="typetranfer" type="text" name="typetranfer" value=""> -->
                                        <select required id="typetranfer" class="form-control" name="typetranfer">
                                          <option value=""> Select </option>
                                          <option value="1">สด</option>
                                          <option value="2">โอน</option>
                                          <option value="99" <?php echo $disable;?> >ยกเลิก</option>
                                        </select>
                                    </div>
                                    <!-- <div class="col-xs-4">

                                    </div> -->
                                </div>
                        </div>

                    </div>

                    <div class="row" id="SHOW_PO_REF">
                      <div class="col-12">
                        <div class="row">
                          <div class="col-12 text-center">
                            <input type="hidden" id="ID" name="ID" value="">
                            <input type="hidden" id="emp_code" name="emp_code" value="{{ $emp_code }}">
                            <button type="submit" class="btn btn-primary" name="button">Save</button>
                            <button type="reset" class="btn btn-basic" name="button" data-dismiss="modal" >Cancel</button>
                          </div>
                        </div>
                      </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>



<!---   myModal approved--->


<!-- myInsert -->

<div class="modal fade" id="myInsert"  role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog" style="width:90%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">เงินสดย่อย</h5>
            </div>

            <div class="modal-body">

                <form id="configinsert" onsubmit="return getdatesubmitbill();" data-toggle="validator" method="post" class="form-horizontal">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-right">รหัสสาขา <?php echo $brcode = Session::get('brcode');?>
                                <input type="hidden"  class="form-control" name="branch_id" id="branch_id" value="<?php echo $brcode ;?>"  />
                                (
                                <?php
                               $databr = Maincenter::databranchbycode($brcode);
                               print_r($databr[0]->name_branch);
                                ?>)
                            </div>
                        </div>
                    </div>

                    <div class="row">
                      <!-- <div class="col-md-12">
                        ผู้มีหน้าที่หักภาษี ณ ที่จ่าย :
                      </div> -->
                      <?php echo "<br>" ?>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-3 control-label">ประเภท<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-9">
                                  <select required id="typedoc" class="form-control" name="typedoc">
                                    <option value=""> Select </option>
                                    <option value="0">RA</option>
                                    <option value="1">RN</option>
                                    <option value="2">RL</option>
                                    <option value="3">CN</option>
                                    <option value="4">TI</option>
                                    <option value="5">CI</option>
                                    <option value="6">RS</option>
                                    <option value="7">CS</option>
                                    <option value="8">หัก ณ ที่จ่าย</option>
                                    <option value="9">RO</option>
                                    <option value="10">PO</option>
                                    <!-- <option value="11">เงินถอน</option> -->
                                    <!-- <option value="12">MD โอนเงิน</option> -->
                                    <option value="13">SS</option>
                                    <!-- <option value="14">จ่ายก่อน</option> -->
                                    <!-- <option value="15">ค่าใช้จ่ายที่ไม่ถือเป็นรายรับ</option> -->
                                    <!-- <option value="16">PO ถอน</option> -->
                                  </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">เลขที่บิล<i><span style="color: red">*</span></i></label>
                                    <div class="col-xs-8">
                                        <input class="form-control" id="billno" type="text" name="billno">
                                        <!-- <select required id="typetranfer" class="form-control" name="typetranfer">
                                          <option value=""> Select </option>
                                          <option value="1">สด</option>
                                          <option value="2">โอน</option>
                                          <option value="99">ยกเลิก</option>
                                        </select> -->
                                    </div>
                                    <!-- <div class="col-xs-4">

                                    </div> -->
                                </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-3 control-label">เงิน<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-9">
                                    <input class="form-control" id="moneycash" type="text" name="moneycash">
                                    <!-- <input required type="number" step="0.01"  class="form-control" id="money" name="money" > -->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">สถานะการโอน<i><span style="color: red">*</span></i></label>
                                    <div class="col-xs-8">
                                        <!-- <input class="form-control" id="typetranfer" type="text" name="typetranfer" value=""> -->
                                        <select required id="typetranfercash" class="form-control" name="typetranfercash">
                                          <option value=""> Select </option>
                                          <option value="1">สด</option>
                                          <option value="2">โอน</option>
                                          <option value="99">ยกเลิก</option>
                                        </select>
                                    </div>
                                    <!-- <div class="col-xs-4">

                                    </div> -->
                                </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-xs-5 col-xs-offset-3">
                                    <button type="submit" id="Btn_save" class="btn btn-primary">Save</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>



<!---   myInsert approved--->
