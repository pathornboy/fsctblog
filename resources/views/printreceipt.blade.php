<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;

?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }

        body {
            font-family: "THSarabunNew";
        }
        h4 {
            font-family: "THSarabunNew";
        }
        h4 {
            font-family: "THSarabunNew";
        }
    </style>
</head>
<body>
    <?php
        $db = Connectdb::Databaseall();
        $sql = "SELECT * FROM $db[fsctaccount].po_head  WHERE id ='$poid' ";
        $datahead = DB::connection('mysql')->select($sql);



    ?>
    <?php

    $db = Connectdb::Databaseall();
    $sqlstock = "SELECT * FROM $db[fsctaccount].po_bill_run_stock_log  WHERE id_po_ref ='$poid'  AND bill_no = '$billno'";
    $datastock = DB::connection('mysql')->select($sqlstock);


    $sqlmoney = "SELECT * FROM $db[fsctaccount].po_bill_run_money  WHERE po_headid ='$poid'  AND bill_no = '$billno'";
    $datamoney = DB::connection('mysql')->select($sqlmoney);
//    print_r($datastock);
//    exit;
    $i= 0;

    // print_r($datadetail);
    ?>

    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="10%">
                @if($datahead[0]->id_company==1)
                    <img src="images/company/1.png" width="275px" >
                @elseif($datahead[0]->id_company==2)
                    <img src="images/company/2.png" width="275px" >
                @endif
            </td>
            <td width="90%" valign="top" style="padding-top: -30px">
                <table width="100%">
                    <tr>
                        <td>
                            <?php $datacompany = Maincenter::datacompany($datahead[0]->id_company);?>
                            <h3><?php print_r($datacompany[0]->name_eng);

                                ?></h3>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: -55px">
                            <?php $datacompany = Maincenter::datacompany($datahead[0]->id_company);?>
                          <h4><?php print_r($datacompany[0]->name)?></h4>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: -35px">
                            <?php $datacompany = Maincenter::datacompany($datahead[0]->id_company);?>
                            <?php print_r($datacompany[0]->address)?>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: -15px">
                           โทรศัพท์  <?php print_r($datacompany[0]->Tel)?>
                           เลขประจำตัวผู้เสียภาษี <?php print_r($datacompany[0]->business_number)?>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: -15px">
                            โทรสาร  <?php print_r($datacompany[0]->Fax)?>
                        </td>
                    </tr>
                </table>


            </td>
        </tr>
    </table>

    <center><h3>Receive/ใบรับของ </h3></center>

    <table width="100%" border="0">
        <tr>
            <td width="50%" colspan="2"><b>วันที่รับ : <?php  $datebill = explode('-',$datamoney[0]->datebill);
                    echo ($datebill[2]).'-'.($datebill[1]).'-'.($datebill[0]+543);
                    ?></b></td>
            <td width="50%" colspan="2" align="right"><b>บิลเลขที่ : {{ $billno }}</b></td>
        </tr>
        <tr>
            <td width="50%" colspan="2"><font style="font-weight: bold">Seller/ผู้ขาย  :
                   <?php
                    $data = Vendorcenter::getdatavendorcenter($datahead[0]->supplier_id);
                    print_r($data[0]->pre.'  '.$data[0]->name_supplier);

                   ?>
                </font></td>
            <td width="50%" colspan="2" align="right"><font style="font-weight: bold">PO NO./เลขที่ใบขออนุมัติซื้อ  :</font>{{$datahead[0]->po_number}}</td>
        </tr>
        <tr >
            <td width="50%" colspan="2"><font style="font-weight: bold">Address/ที่อยู่ :
                <?php
                    print_r($data[0]->address .'  ตำบล  '.$data[0]->district.'  อำเภอ  '.$data[0]->amphur.'  จังหวัด  '.$data[0]->province);
                    echo "<br>";
                    print_r('รหัสไปรษณีย์  '.$data[0]->zipcode .' โทรศัพท์ '.$data[0]->phone);

                ?>
                </font></td>
            <td width="25%"></td>
            <td width="25%" align="right" valign="top"><font style="font-weight: bold">Date/วันที่  :</font>{{$datahead[0]->date}}</td>
        </tr>
    </table>
    <br>

    <table width="100%" border="1"  cellspacing="0" cellpadding="0">
        <tr valign="top">
            <td width="5%" align="center" bgcolor="#dcdcdc">#</td>
            <td width="55%" align="center"  bgcolor="#dcdcdc">รายการ</td>
            <td width="15%" align="center"  bgcolor="#dcdcdc">จำนวน</td>
            <td width="15%" align="center"  bgcolor="#dcdcdc">จำนวนรับ</td>
        </tr>
        @foreach($datastock as $j)
        <tr valign="top">
            <td width="5%" align="center">{{ $i+1 }}</td>
            <td width="55%" align="center"  >{{ $j->list }}</td>
            <td width="15%" align="center"  >{{ $j->amount }}</td>
            <td width="15%" align="center" >{{ $j->quantity_get_now }}</td>
        </tr>
            {{ $i++ }}
         @endforeach
        <tr>
            <td colspan="3" align="center" bgcolor="dcdcdc" align="right" >เงินจ่ายตามบิล</td>
            <td colspan="1" align="center" bgcolor="dcdcdc">{{ number_format($datamoney[0]->pay_real,2)}}</td>
        </tr>
    </table>
    <br>
    <br>

    <br>
    <br>
    <table width="100%" border="0">
        <tr>
            <td width="50%" align="center">
                ....................................................................................
            </td>
            <td width="50%" align="center">
                ....................................................................................
            </td>
        </tr>
        <tr>
            <td width="50%" align="center">

              (<?php
              $dataemp = Maincenter::getdatacompemp($datahead[0]->emp_code_po);
              print_r($dataemp[0]->nameth.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$dataemp[0]->surnameth);
              ?>)
            </td>
            <td width="50%" align="center">
              (
              <?php
              $dataempapproved = Maincenter::getdatacompemp($datahead[0]->code_emp_approve);
              //print_r($datahead[0]->code_emp_approve);
              print_r($dataempapproved[0]->nameth.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$dataempapproved[0]->surnameth);
              ?>
              )
            </td>
        </tr>
        <tr>
            <td width="50%" align="center">
                ผู้ขอซื้อ
            </td>
            <td width="50%" align="center">
                ผู้อนุมัติ
            </td>
        </tr>
    </table>
</body>
</html>
