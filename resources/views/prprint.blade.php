<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;

?>

<link>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}
<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>

<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">

<script type="text/javascript" src = 'js/vendor/vendorcenter.js'></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

    <style>
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }

        body {
            font-family: "THSarabunNew";
        }
        h4 {
            font-family: "THSarabunNew";
        }
        h4 {
            font-family: "THSarabunNew";
        }
    </style>

<style>
    .modal-ku {
        width: 90%;
        margin: auto;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
           
            <div class="box-body">
				
				
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <div align="center">
                                        <h2>รายงานใบสั่งของพนักงาน (PR)</h2>
                                    </div>
									<div align="center">
							<?php
							$db = Connectdb::Databaseall();
							$databranch = DB::connection('mysql')->select('SELECT code_branch,name_branch FROM '.$db['hr_base'].'.branch WHERE branch.code_branch = '.$branch);
							
							$datastat = DB::connection('mysql')->select('SELECT numberstatus, notstatus FROM '.$db['fsctaccount'].'.accountstatusforprogram WHERE accountstatusforprogram.numberstatus = '.$status);	
							?>		
										สาขา :  
										<?php 
											if($branch == '99'){	
												$nameB = "ทุกสาขา";
											}else{
												$nameB = $databranch[0]->name_branch;
											}
											echo $nameB;
										?>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<?php
										
										if($month == "01"){$monthTH = "มกราคม";
										}else if($month == "02"){$monthTH = "กุมภาพันธ์";
										}else if($month == "03"){$monthTH = "มีนาคม";
										}else if($month == "04"){$monthTH = "เมษายน";
										}else if($month == "05"){$monthTH = "พฤษภาคม";
										}else if($month == "06"){$monthTH = "มิถุนายน";
										}else if($month == "07"){$monthTH = "กรกฎาคม";
										}else if($month == "08"){$monthTH = "สิงหาคม";
										}else if($month == "09"){$monthTH = "กันยายน";
										}else if($month == "10"){$monthTH = "ตุลาคม";
										}else if($month == "11"){$monthTH = "พฤศจิกายน";
										}else if($month == "12"){$monthTH = "ธันวาคม";
										}
								// echo $monthTH;
											if(($week == "") && ($status == "")){
												echo "ประจำเดือน  ".$monthTH."  ปี  ".($year+543);
											}else if(($month == "") && ($status == "")){
												echo "ประจำรอบการขอ   ".$week;
											}else{
												echo "สถานะการขอ  :  ".$datastat[0]->notstatus;
											}
										?>
										
										
									</div>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div align="center" >
								
								<?php //echo $week;?>
                                <div class="row">
                                    <div class="col-md-5">
                                        <input type="hidden" name="settime" id="settime" value="<?php echo date('Y-m-d')?>">
                                        <input type="hidden" name="setlogin" id="setlogin" value="<?php echo $emp_code = Session::get('emp_code')?>">     
                                    </div>
                                </div>

                                <table class="table table-striped table-bordered" width="95%" border="1" cellpadding="0">
                                    <thead class="thead-inverse" >
                                    <tr>
                                        <td align="center">#</td>
                                        <td>เลขใบ PR</td>
                                        <td align="center">วันเดือนปี</td>
                                        <td align="center">ราคา</td>
                                        <td align="center">จำนวน</td>
                                        <td align="center">vat (บาท)</td>
                                        <td align="center">หัก ณ ที่จ่าย (บาท)</td>
                                        <td align="center">ประเภทการซื้อ</td>
                                        <td align="center">ประเภทการจ่าย</td>
										<td align="center">รวม</td>
                                        
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
										$db = Connectdb::Databaseall();
									
										
										// $week = '2018-02-20 10:00:00';
										
										
										if($branch == '99'){	
											$other = "";
										}else{
											$other = " AND H.branch_id = '$branch'";
										}
										
										$sql = "SELECT *
												FROM $db[fsctaccount].ppr_head AS H
												INNER JOIN $db[fsctaccount].ppr_detail AS D on H.id = D.ppr_headid
												INNER JOIN $db[fsctaccount].type_pay AS P on H.type_pay = P.id
												INNER JOIN $db[fsctaccount].type_buy AS B on H.type_buy = B.id
												WHERE D.status = 1 
												AND (H.week = '$week' OR (H.status = '$status') OR (H.date like '____-$month-__' AND H.date like '$year%'))".$other;  
												
										// echo $sql;
										// exit();
										
										$data = DB::connection('mysql')->select($sql);		
																								
										
										$sumpayreal = DB::table('po_head')
													->where('status_head', '=', 2)
													->where('week', '=', '$week')
													->where('branch_id', '=', '$branch')
													->sum('payreal');
									
										$i = 1;
										$sumdiff = 0;
										$sumtotal = 0;
										$sumvat = 0;
										$sumwithhold = 0;
										
                                    ?>
                                    @foreach ($data as $value)
                                        <tr>
                                            <td scope="row">{{ $i }}</td>
                                            <td align="left">{{ $value->number_ppr }}</td>
                                            <td align="center">{{ $value->date }}</td>
                                            <td align="center">{{ number_format($value->price,2) }}</td>
                                            <td align="center">{{ $value->amount }}</td>
                                            <td align="center">{{ number_format(($value->vat * ($value->price * $value->amount))/100,2) }}</td>
                                            <td align="center">{{ number_format(((($value->vat * ($value->price * $value->amount))/100) * $value->withhold)/100,2) }}</td>
                                            <td align="center">{{$value->name_buy}}</td>                                           
                                            <td align="center">{{$value->name_pay}}</td>
											<td align="right">{{ number_format($value->total,2)}}</td>
											
                                        </tr>
										<?php
																				
										$total = $value->total;
										$sumtotal = $sumtotal + $total;
										// echo $total;
										
										$vat = ($value->vat * ($value->price * $value->amount))/100;
										$sumvat = $sumvat + $vat;
										// echo $vat;
										
										$withhold = ((($value->vat * ($value->price * $value->amount))/100) * $value->withhold)/100;
										$sumwithhold = $sumwithhold + $withhold;
										// echo $withhold;
										?>
                                        <?php $i++; ?>
                                    @endforeach
                                    </tbody>
										<tr>
											<td colspan="3" align="center"><B>รวมทั้งหมด : </B></td>
											<td colspan="1" align="right"><B><?php echo number_format($sumvat,2); ?></B></td>
											<td></td>
											<td colspan="1" align="right"><B><?php echo number_format($sumwithhold,2); ?></B></td>
											<td colspan="1" align="right"><B><?php echo number_format($sumdiff,2);?></B></td>
											<td colspan="2">&nbsp;</td>
											<td colspan="1" align="right"><B><?php echo number_format($sumtotal,2) ?></B></td>
										</tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>

