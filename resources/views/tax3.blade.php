<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;
use  App\Api\Vendorcenter;
$db = Connectdb::Databaseall();
$emp_code = Session::get('emp_code');

?>
@include('headmenu')
<link>
<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}



<link rel="stylesheet" type="text/css" href="css/ui/jquery-ui.css">
{{--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>


<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">


<link rel="stylesheet" type="text/css" href="bower_components/select2/dist/css/select2.min.css">
<script type="text/javascript" src = 'bower_components/select2/dist/js/select2.full.min.js'></script>

<script type="text/javascript" src = 'js/vendor/not_expenses.js'></script>


<style>
    .ui-autocomplete-input {
        border: none;
        font-size: 14px;
        width: 150px;
        height: 24px;
        margin-bottom: 5px;
        padding-top: 2px;
        border: 1px solid #DDD !important;
        padding-top: 0px !important;
        z-index: 1511;
        position: relative;
    }
    .ui-menu .ui-menu-item a {
        font-size: 12px;
    }
    .ui-autocomplete {
        position: absolute;
        top: 0;
        left: 0;
        z-index: 1510 !important;
        float: left;
        display: none;
        min-width: 160px;
        width: 160px;
        padding: 4px 0;
        margin: 2px 0 0 0;
        list-style: none;
        background-color: #ffffff;
        border-color: #ccc;
        border-color: rgba(0, 0, 0, 0.2);
        border-style: solid;
        border-width: 1px;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding;
        background-clip: padding-box;
        *border-right-width: 2px;
        *border-bottom-width: 2px;
    }
    .ui-menu-item > a.ui-corner-all {
        display: block;
        padding: 3px 15px;
        clear: both;
        font-weight: normal;
        line-height: 18px;
        color: #555555;
        white-space: nowrap;
        text-decoration: none;
    }
    .ui-state-hover, .ui-state-active {
        color: #ffffff;
        text-decoration: none;
        background-color: #0088cc;
        border-radius: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        background-image: none;
    }

</style>



<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">งานจัดซื้อ</a>
                    </li>
                    <li class="active">ค่าใช้จ่ายที่ไม่ถือเป็นรายรับ</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        ค่าใช้จ่ายที่ไม่ถือเป็นรายรับ
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body table-responsive">
                                <div class="row">
                                    <div class="col-md-10">

                                    </div>

                                    <div class="col-md-2">
                                        <a href="{{ url('/gen_tax3') }}" title="เพิ่มข้อมูล"><img src="images/global/printall.png"> ภงด 3 .txt</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <br>
                                </div>
                                <?php
                                $brcode = Session::get('brcode');
                                $sqlreservemoney = "SELECT name_supplier, beforevat, whd, total FROM $db[fsctaccount].po_head INNER JOIN $db[fsctaccount].withholdtaxpo ON ( $db[fsctaccount].po_head.id = $db[fsctaccount].withholdtaxpo.id_po ) INNER JOIN $db[fsctaccount].supplier ON ($db[fsctaccount].supplier.id = $db[fsctaccount].po_head.supplier_id)  WHERE withholdtaxpo.status = '1' ";
                                // return print_r($sqlreservemoney);
                                $datareservemoney = DB::connection('mysql')->select($sqlreservemoney);



                                ?>

                                        <table id="example" class="table table-striped table-bordered">
                                            <thead class="thead-inverse">
                                              <tr>
                                                  <td>ชื่อ</td>
                                                  <td>ก่อน Vat</td>
                                                  <td>หัก ณ ที่จ่าย</td>
                                                  <td>ยอดรวม</td>

                                              </tr>
                                            </thead>

                                            <tbody>
                                            <?php foreach ($datareservemoney as $k => $v) {?>
                                              <tr>
                                                <td>{{$v->name_supplier}}</td>
                                                <td>{{$v->beforevat}}</td>
                                                <td>{{$v->whd}}</td>
                                                <td>{{$v->total}}</td>
                                              </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')
