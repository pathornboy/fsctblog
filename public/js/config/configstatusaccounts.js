/**
 * Created by pacharapol on 1/18/2018 AD.
 */
$(document).ready(function() {
    $('#example').DataTable();

});

function getdatesubmit() {

    var valid = $('#configForm').validator('validate').has('.has-error').length;
    if (valid == 0) {
        Save();
    }
    return false;
}

function insertnew() {
    $('#id').val('');
}

function Save() {
    var dataInpus = null;

    dataInpus = $('#configForm').serializeArray();

    var data = JSON.stringify(dataInpus);

    //
    $.post('configstatusaccinsertandupdate', {data: data}, function (res) {
        console.log(res);
        if (res == 1) {
            bootbox.alert({
                title: "แจ้งเตือน",
                message: "บันทึกข้อมูลเรียบร้อยแล้ว!!",
                callback: function () {
                    // $('#ConfirmDialog').modal('hide');
                    $('#myModal').modal('hide');
                    location.reload();

                }
            });
        }
    });

}
//


function getdata(id) {
    $('#id').val(id);
    $.get("getdataconfigstatusacc?id="+id, function( data ) {
        console.log(data);
        $('#id').val(data[0].id);
        $('#Note').val(data[0].Note);
        $('#ap').val(data[0].ap);
        $('#color').val(data[0].color);
        $('#level_emp').val(data[0].level_emp);
        $('#notstatus').val(data[0].notstatus);
        $('#numberstatus').val(data[0].numberstatus);
        $('#po').val(data[0].po);
        $('#position_id').val(data[0].position_id);
        $('#status').val(data[0].status);
        $('#auth').val(data[0].auth);

        if(data[0].po==1){
            $("#po").prop("checked", true);
        }else{
            $("#po").prop("checked", false);
        }


        if(data[0].ap==1){
            $("#ap").prop("checked", true);
        }else{
            $("#ap").prop("checked", false);

        }


    });
}
