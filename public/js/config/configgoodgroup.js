$(document).ready(function() {
    $('#example').DataTable();

});

//
function getdatesubmit() {

    var valid = $('#configgoodgroup').validator('validate').has('.has-error').length;
    if (valid == 0) {
        Save();
    }
    return false;
}

function Save() {
    var dataInpus = null;

    dataInpus = $('#configgoodgroup').serializeArray();

    var data = JSON.stringify(dataInpus);

    // console.log(data);

    $.post('configgoodgroupinsertandupdate', {data: data}, function (res) {
        console.log(res);
        if (res == 1) {
            bootbox.alert({
                title: "แจ้งเตือน",
                message: "บันทึกข้อมูลเรียบร้อยแล้ว!!",
                callback: function () {
                    // $('#ConfirmDialog').modal('hide');
                    $('#myModal').modal('hide');
                    location.reload();

                }
            });
        }
    });

}
//
function insertnew() {
    $('#id').val('');
	$('#alphabet').val('');
    $('#name_group').val('');
    $('#status').val(1);
}

function getdata(id) {
    $('#id').val(id);
    $.get("getdataconfiggoodgroup?id="+id, function( data ) {
        // console.log(data)
        $('#id').val(data[0].id);
        $('#alphabet').val(data[0].alphabet);
        $('#name_group').val(data[0].name_group);
        $('#status').val(data[0].status);
    });
}






