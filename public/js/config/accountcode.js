$(document).ready(function() {
    $('#example').DataTable();

});

//
function getdatesubmit() {

    var valid = $('#configFormaccountnumber').validator('validate').has('.has-error').length;
    if (valid == 0) {
        Save();
    }
    return false;
}

function Save() {
    var dataInpus = null;

    dataInpus = $('#configFormaccountnumber').serializeArray();

    var data = JSON.stringify(dataInpus);

    // console.log(data);

    $.post('configaccoutcodeinsertandupdate', {data: data}, function (res) {
        console.log(res);
        if (res == 1) {
            bootbox.alert({
                title: "แจ้งเตือน",
                message: "บันทึกข้อมูลเรียบร้อยแล้ว!!",
                callback: function () {
                    // $('#ConfirmDialog').modal('hide');
                    $('#myModal').modal('hide');
                    location.reload();

                }
            });
        }
    });

}
//
function insertnew() {
    $('#id').val('');
}

function getdata(id) {
    $('#id').val(id);
    $.get("getdataconfigaccountcode?id="+id, function( data ) {
        // console.log(data)
        $('#id').val(data[0].id);
        $('#accounttypeno').val(data[0].accounttypeno);
        $('#accounttypefull').val(data[0].accounttypefull);
        $('#status').val(data[0].status);
    });
}
