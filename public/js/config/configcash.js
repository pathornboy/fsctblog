$(document).ready(function() {
    $('#example').DataTable();

});

//
function getdatesubmit() {

    var valid = $('#configcash').validator('validate').has('.has-error').length;
    if (valid == 0) {
        Save();
    }
    return false;
}

function Save() {
    var dataInpus = null;

    dataInpus = $('#configcash').serializeArray();

    var data = JSON.stringify(dataInpus);

    // console.log(data);

    $.post('configcashinsertandupdate', {data: data}, function (res) {
        console.log(res);
        if (res == 1) {
            bootbox.alert({
                title: "แจ้งเตือน",
                message: "บันทึกข้อมูลเรียบร้อยแล้ว!!",
                callback: function () {
                    // $('#ConfirmDialog').modal('hide');
                    $('#myModal').modal('hide');
                    location.reload();

                }
            });
        }
    });

}
//
function insertnew() {
    $('#id').val('');
    $('#grandtotal').val('');
    $('#note').val('');
}

function getdata(id) {
    $('#id').val(id);
    $.get("getdataconfigcash?id="+id, function( data ) {
         console.log(data)
        $('#id').val(data[0].id);
        $('#grandtotal').val(data[0].grandtotal);
        $('#branch_id').val(data[0].branch_id);
        $('#note').val(data[0].note);
        $("button").show();
    });
}

function viewdata(id) {
    $('#id').val(id);
    $.get("getdataconfigcash?id="+id, function( data ) {
         console.log(data)
        $('#id').val(data[0].id);
        $('#grandtotal').val(data[0].grandtotal);
        $('#branch_id').val(data[0].branch_id);
        $('#note').val(data[0].note);

        $("button").hide();
        $('#myModal').modal('show');
    });
}
