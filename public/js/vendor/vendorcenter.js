/**
 * Created by pacharapol on 1/14/2018 AD.
 */
$(document).ready(function() {
    $('#example').DataTable();

});

function getdatesubmit() {

    var valid = $('#configFormvendors').validator('validate').has('.has-error').length;
    if (valid == 0) {
        Save();
    }
    return false;
}

function getdatesubmit_addvendor() {

    var valid = $('#configFormvendors_addvendor').validator('validate').has('.has-error').length;
    if (valid == 0) {
        Save_addvendor();
    }
    return false;
}

function Save() {
    var dataInpus = null;

    dataInpus = $('#configFormvendors').serializeArray();

    var data = JSON.stringify(dataInpus);

    // console.log(data);

    $.post('vendorcenterinsertandupdate', {data: data}, function (res) {
        console.log(res);
        if (res == 1) {
            bootbox.alert({
                title: "แจ้งเตือน",
                message: "บันทึกข้อมูลเรียบร้อยแล้ว!!",
                callback: function () {
                    // $('#ConfirmDialog').modal('hide');
                    $('#myModal').modal('hide');
                    location.reload();

                }
            });
        }
    });

}

function Save_addvendor() {
    var dataInpus = null;

    dataInpus = $('#configFormvendors_addvendor').serializeArray();

    var data = JSON.stringify(dataInpus);

     console.log(data);

    $.post('vendorcenterinsertandupdate', {data: data}, function (res) {
        console.log(res);
        if (res == 1) {
            bootbox.alert({
                title: "แจ้งเตือน",
                message: "บันทึกข้อมูลเรียบร้อยแล้ว!!",
                callback: function () {
                    // $('#ConfirmDialog').modal('hide');
                    $('#myModal_addvendor').modal('hide');
                    location.reload();

                }
            });
        }
    });

}
//
function insertnew() {
    $('#id_addvendor').val('');
    $('#status_addvendor').val(0);
    $('#createby_addvendor').val($('#setlogin').val());
    $('#createdate_addvendor').val($('#settime').val());
}

function getdata(id) {
    $('#id').val(id);
    $.get("getdatavendorcenter?id="+id, function( data ) {
        // console.log(data)
        $('#id').val(data[0].id);
        $('#pre').val(data[0].pre);
        $('#name_supplier').val(data[0].name_supplier);
        $('#type_branch').val(data[0].type_branch);
        $('#address').val(data[0].address);
        $('#district').val(data[0].district);
        $('#amphur').val(data[0].amphur);
        $('#province').val(data[0].province);
        $('#zipcode').val(data[0].zipcode);
        $('#phone').val(data[0].phone);
        $('#mobile').val(data[0].mobile);
        $('#email').val(data[0].email);
        $('#tax_id').val(data[0].tax_id);
        $('#terms_id').val(data[0].terms_id);
        $('#Note').val(data[0].Note);
        $('#codecreditor').val(data[0].codecreditor);

        $('#status').val(data[0].status);
        $('#createby').val(data[0].createby);
        $('#createdate').val(data[0].createdate);

        $('#establish_year').val(data[0].establish_year);
        $('#business_type').val(data[0].business_type);
        $('#main_product').val(data[0].main_product);
        $('#main_industry').val(data[0].main_industry);
        $('#product_standard').val(data[0].product_standard);
        $('#iso_system').val(data[0].iso_system);
        $('#example_product').val(data[0].example_product);
        $('#example_product_number').val(data[0].example_product_number);
        $('#number_delivery').val(data[0].number_delivery);
        $('#quality_point').val(data[0].quality_point);
        $('#price_point').val(data[0].price_point);
        $('#delivery_point').val(data[0].delivery_point);
        $('#credit_point').val(data[0].credit_point);
        $('#manufacture_point').val(data[0].manufacture_point);
        $('#agent').val(data[0].agent);



    });
}


function updatestatusvendor(id) {
    $.get("getdatavendorcenter?id="+id, function(data) {
        $('#idupdate').val(data[0].id);
        $('#statusupdate').val(data[0].status);
    });

}

function savestatus() {
    var datapos = {'id':$('#idupdate').val(),
        'status':$('#statusupdate').val()
    };

    $.post('updatevendorstatus', {data: datapos}, function (res) {

        if (res == 1) {
            bootbox.alert({
                title: "แจ้งเตือน",
                message: "บันทึกข้อมูลเรียบร้อยแล้ว!!",
                callback: function () {
                    // $('#ConfirmDialog').modal('hide');
                    $('#myModal').modal('hide');
                    location.reload();

                }
            });
        }
    });

}
