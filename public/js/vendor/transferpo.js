/**
 * Created by pacharapol on 1/14/2018 AD.
 */
var index = 1;
var amount = 0;
var price = 0;
var vat = 0;
$(document).ready(function() {
    $('#example').DataTable();
    $('.select2').select2();

    $('#addrow').on("click", function () {
       // console.log(index);

       CloneTableRow();
        //bindDataWithStep();
    });

    $(".datepicker").datepicker({
        format: 'dd-mm-yyyy',
    });




});


function serachpridref() {



    var idpr = $('#number_pr_ref').val();//for code;
    var id_company = $('#id_company').val();

   // console.log(idpr);
    $.get('getdataprbycodepr', {idpr: idpr,idcompany:id_company}, function (data) {
       if(data[0].statuspo == 0){
           bootbox.alert({
               message: "BOSS/MD ยังไม่ได้อนุมัติ",
               size: 'small',
               callback: function () {
                   $('#myModal').modal('hide');
                   location.reload();

               }
           });
       }else{

           //  var id_pr = data[0].id;
           $('#pr_id').val(data[0].id);
           $('#supplier_id').val(data[0].supplier_id);
           $('#supplier_show').val(data[0].Fullnameauto);
           $('#type_pay').val(data[0].type_pay);
           $('#type_pay_show').val(data[0].type_pay_name);
           $('#type_buy').val(data[0].type_buy);
           $('#type_buy_show').val(data[0].type_buy_name);
           $('#code_emp_pr').val(data[0].code_emp);



           $('#in_house').val(data[0].in_house);
           if(data[0].in_house ==1) {
               $('#in_house_show').val('ซื้อภายในประเทศ');
           }else{
               $('#in_house_show').val('ซื้อต่างประเทศ');
           }

           $('#in_budget').val(data[0].in_budget);
           if(data[0].in_budget ==1) {
               $('#in_budget_show').val('ในงบประมาณ');
           }else{
               $('#in_budget_show').val('นอกงบประมาณ');
           }

           $('#urgent_status').val(data[0].urgent_status);
           if(data[0].urgent_status ==1) {
               $('#urgent_status_show').val('การจัดซื้อเร่งด่วน');
           }else{
               $('#urgent_status_show').val('การจัดซื้อไม่เร่งด่วน');
           }

           $('#terms').val(data[0].supplier_termsname);

           loopdatadetail(data[0].id);
       }

        $.get('getidpo?id='+id_company, function (data) {
            var dataprint =  JSON.parse(data);

            $('.po_number').val(dataprint.idgen);
            $('#po_no_branch').val(dataprint.idnumber);

        });

        $.get('getdatacompany?id='+id_company, function (data) {
            //console.log(data);

            $('#address_send').val(data[0].address+'  โทรศัพท์  '+data[0].Tel +'  เลขระจำตัวผู้เสียภาษี  '+data[0].business_number);

        });



    });







}

function ckaddress() {
    var ckb = $("#ckaddbranch").is(':checked');


    if(ckb== true){
        var id_company = $('#id_company').val();
        $.get('getdatacompany?id='+id_company, function (data) {
            //console.log(data);
            $("#address_send").attr("readonly", true);
            $('#address_send').val(data[0].address+'  โทรศัพท์  '+data[0].Tel +'  เลขระจำตัวผู้เสียภาษี  '+data[0].business_number);
        });

    }else{
        $("#address_send").attr("readonly", false);
        $('#address_send').val('');
    }
}

function savepo() {
    bootbox.confirm({
        size: "small",
        message: "Are you sure?",
        callback: function(result){
            //console.log(result);
            if(result==true){
                Savehead();
            }else{
                $('#myModal').modal('hide');
            }
        }
    });




}

function Savehead() {

    var ckb = $("#ckaddbranch").is(':checked');
    var resultck = 0;


    if(ckb == true){
        resultck = 1;
    }else{
        resultck = 0;
    }

    var dataposthead = { 'id':$('#id').val(),
                        'pr_id':$('#pr_id').val(),
                        'number_pr_ref':$('#number_pr_ref').val(),
                        'date':$('#date').val(),
                        'year':$('#year').val(),
                        'year_th':$('#year_th').val(),
                        'branch_id':$('#branch_id').val(),
                        'po_number':$('#po_number').val(),
                        'po_no_branch':$('#po_no_branch').val(),
                        'supplier_id':$('#supplier_id').val(),
                        'type_pay':$('#type_pay').val(),
                        'type_buy':$('#type_buy').val(),
                        'in_house':$('#in_house').val(),
                        'in_budget':$('#in_budget').val(),
                        'urgent_status':$('#urgent_status').val(),
                        'code_emp_pr':$('#code_emp_pr').val(),
                        'code_sup':$('#code_sup').val(),
                        'id_company':$('#id_company').val(),
                        'ckaddbranch':resultck,
                        'address_send':$('#address_send').val(),
                        'transfer_config_id':$('#type_transfer').val(),
                        'emp_code_po':$('#code_emp_po').val(),
                        'terms':$('#terms').val(),
                        'totolsumall':$('#totolsumall').val(),
                        'payreal':$('#payreal').val(),
                        'diffpay':$('#diffpay').val(),
                        'status_head':2,
    };


   // console.log(dataposthead);




    $.post('addpoinsertupdate', {data: dataposthead}, function (res) {
       // console.log(res);
            savedetail(res);
    });




}
function insertnew() {

    $('#statusupdate').val(0);


    $("#serachpridref").show();
    $("#payreal").attr("readonly", false);
    $(".quantity").attr("readonly", true);

    $('#showbill').hide();
    while (index >= 1) {
        if(index == 1){
            $('#tbindex'+index).remove();
            // index++;
        }else{
            $('#tbindex'+index).remove();

        }
        index--;
    }
    index = 1 ;


    $('#Btn_save').show();
    $('#number_pr_ref').val('');
    $('#pr_id').val('');
    $('#supplier_id').val('');
    $('#supplier_show').val('');
    $('#type_pay').val('');
    $('#type_pay_show').val('');
    $('#type_buy').val('');
    $('#type_buy_show').val('');

    $('#in_house').val('');
    $('#in_house_show').val('');

    $('#in_budget').val('');
    $('#in_budget_show').val('');

    $('#urgent_status').val('');
    $('#urgent_status_show').val('');

    $('#terms').val('');

    $('#supplier_id').val('').trigger('change');
    $('#type_pay').val('');
    $('#type_buy').val('');

    $('#config_group_supp_id0').val('');
    $('#list0').val('');
    $('#amount0').val('');
    $('#type_amount0').val('');
    $('#price0').val('');
    $('#vat0').val(0);
    $('#total0').val('');
    $('#daterecentpurchases0').val('');
    $('#avg0').val('');
    $('#note0').val('');
    $('#totolsumall').val('');

    $(".datepicker").datepicker({
        format: 'dd-mm-yyyy',
    });


}

function selecttranfer(val) {
    var id_company = val.value;
    $.get('getdatacompany?id='+id_company, function (data) {
        //console.log(data);

        $('#address_send').val(data[0].address+'  โทรศัพท์  '+data[0].Tel +'  เลขระจำตัวผู้เสียภาษี  '+data[0].business_number);

    });
}


function loopdatadetail(id_pr) {


    while (index >= 1) {
        if(index == 1){
            $('#tbindex'+index).remove();
            // index++;
        }else{
            $('#tbindex'+index).remove();

        }
        index--;
    }

    $.get("getdatavendorpprdetail?id="+id_pr, function(res) {
        showtable(res);
    });


}



function showtable(data) {

    //console.log(data)

    var count = (Object.keys(data).length);
    //console.log(count);
    var html = '';
    var totalsum = 0;
    //
    var set = '';

    for (set = 1; set <= count-1; set++) {
        html += '<tr id="tbindex'+set+'">';
        html += '<td><select name="config_group_supp_id[]" id="config_group_supp_id' + set + '" class="form-control"  disabled></select></td>';
        html += '<td><select name="accounttype_id[]" id="accounttype_id' + set + '" class="form-control"  style="width: 120px;"  ><option value="">เลือกเลขบัญชี</option></select></td>';
        html += '<td><input type="hidden" name="material_id[]" id="materialid'+set+'" value="0"><input type="text" name="list[]" id="list' + set + '"  class="form-control" readonly></td>';
        html += '<td><input type="text" name="amount[]" id="amount' + set + '" onblur="getamount(this,' + set + ')"   class="form-control" style="width: 90px;" placeholder="จำนวน" readonly></td>';
        html += '<td><input type="text" name="type_amount[]" id="type_amount' + set + '"  class="form-control" style="width: 80px;" placeholder="หน่วยนับ" readonly></td>';
        html += '<td><input type="text" name="price[]" id="price' + set + '" disabled onblur="getprice(this,' + set + ')"   class="form-control" readonly ></td>';
        html += '<td><select name="vat[]" id="vat' + set + '" onchange="calculatevat(this,' + set + ')" class="form-control" style="width: 90px;" disabled></select></td>';
        html += '<td><select name="withhold[]" id="withhold'+set+'" onchange="calculatewithhold(this,'+set+')" class="form-control" style="width: 90px;" disabled></select></td>';
        html += '<td><input type="text" name="total[]" id="total' + set + '"   class="form-control"  readonly></td>';
        html += '<td><input type="text" class="form-control quantity"  name="quantity_get[]" id="quantity' + set + '" onblur="caldifamount('+set+')" value="0" readonly></td>';
        html += '<td> <input type="text" class="form-control"  name="quantity_loss[]" id="loss' + set + '" readonly></td>';
        html += '<td><span id="statuspodetai' + set + '"> <font color="red">ยังไม่ครบ</font></span></td>';
        html += '</tr>';
    }
    $('#tdbody').append(html);
    var setno ='';
    for (setno = 1; setno <= count-1; setno++) {
        $('#config_group_supp_id0').find('option').clone().appendTo('#config_group_supp_id'+setno+'');
        $('#vat0').find('option').clone().appendTo('#vat'+setno+'');
        $('#withhold0').find('option').clone().appendTo('#withhold'+setno+'');
    }




    // console.log(data);

    $.each(data, function(key, value) {
        $('#config_group_supp_id'+key).val(data[key].config_group_supp_id);

        $('#accounttype_id'+key).empty();

        var _option = "<option value=''>เลือกเลขบัญชี</option>";

        var valuetestacc = data[key].accounttype_id;

        $.get('getdataaccounttype?idacc='+data[key].config_group_supp_id, function (data) {

            $.each(data, function(key, value) {
                _option+= "<option value="+data[key].id+">"+data[key].accounttypeno+"__"+data[key].accounttypefull+"</option>";
            });

            $('#accounttype_id'+key).append(_option);
           // console.log(valuetestacc);
            $('#accounttype_id'+key).val(valuetestacc);
        });

        $('#materialid'+key).val(data[key].materialid);
        $('#list'+key).val(data[key].list);
        $('#amount'+key).val(data[key].amount);
        $('#type_amount'+key).val(data[key].type_amount);
        $('#price'+key).val(data[key].price);
        $('#vat'+key).val(data[key].vat);
        $('#withhold'+key).val(data[key].withhold);
        $('#total'+key).val(data[key].total);
        index++;
        totalsum = totalsum+data[key].total;
    });

    totalsum = totalsum.toFixed(2);

    $('#totolsumall').val(totalsum);

}


function setoption(index) {

    $('#config_group_supp_id0').find('option').clone().appendTo('#config_group_supp_id'+index+'');
    $('#vat0').find('option').clone().appendTo('#vat'+index+'');
    $('#withhold0').find('option').clone().appendTo('#withhold'+index+'');

}


function savedetail(idhead) {
    var arrconfig_group_supp_id = [];
    var arrlist = [];
    var arramount = [];
    var arrtype_amount = [];
    var arrprice = [];
    var arrvat = [];
    var arrwithhold = [];
    var arrtotal = [];
    var quantity_get = [];
    var quantity_loss = [];
    var arrmaterialid = [];
    var arraccounttypeid = [];

    var po_number = $('#po_number').val();




    $('select[name^="config_group_supp_id[]"]').each(function() {
        arrconfig_group_supp_id.push($(this).val());
    });
    $('select[name^="accounttype_id[]"]').each(function() {
        arraccounttypeid.push($(this).val());
    });
    $('input[name^="material_id[]"]').each(function() {
        arrmaterialid.push($(this).val());
    });
    $('input[name^="list[]"]').each(function() {
        arrlist.push($(this).val());
    });
    $('input[name^="amount[]"]').each(function() {
        arramount.push($(this).val());
    });
    $('input[name^="type_amount[]"]').each(function() {
        arrtype_amount.push($(this).val());
    });
    $('input[name^="price[]"]').each(function() {
        arrprice.push($(this).val());
    });
    $('select[name^="vat[]"]').each(function() {
        arrvat.push($(this).val());
    });
    $('select[name^="withhold[]"]').each(function() {
        arrwithhold.push($(this).val());
    });
    $('input[name^="total[]"]').each(function() {
        arrtotal.push($(this).val());
    });
    $('input[name^="quantity_get[]"]').each(function() {
        quantity_get.push($(this).val());
    });
    $('input[name^="quantity_loss[]"]').each(function() {
        quantity_loss.push($(this).val());
    });


    var datapostdetail = { 'config_group_supp_id':arrconfig_group_supp_id,
        'arraccounttypeid':arraccounttypeid,
        'arrmaterialid':arrmaterialid,
        'arrlist':arrlist,
        'arramount':arramount,
        'arrtype_amount':arrtype_amount,
        'arrprice':arrprice,
        'arrvat':arrvat,
        'arrwithhold':arrwithhold,
        'arrtotal':arrtotal,
        'quantity_get':quantity_get,
        'quantity_loss':quantity_loss,
        'idhead':idhead,
        'company_id_transfer':$('#company_id_transfer').val(),
        'branch_id_transfer':$('#branch_id_transfer').val(),
        'po_number':po_number,
    };

     //console.log(datapostdetail);

    var updatebill = $('#statusupdate').val();

    if(updatebill==2){
        var arrbillref = [];
        var arrpayperbill = [];

        $('input[name^="billref[]"]').each(function() {
            arrbillref.push($(this).val());
        });

        $('input[name^="payperbill[]"]').each(function() {
            arrpayperbill.push($(this).val());
        });

        var datapostrunmoney = {'arrbillref':arrbillref,
            'arrpayperbill':arrpayperbill,
            'idhead':idhead,
        };

    }



    $.post('inserttransfertostockacc', {data: datapostdetail}, function (res) {
        //console.log(res);
        if (res == 1) {
            bootbox.alert({
                title: "แจ้งเตือน",
                message: "บันทึกข้อมูลเรียบร้อยแล้ว!!",
                callback: function () {
                    // $('#ConfirmDialog').modal('hide');
                    $('#myModal').modal('hide');
                    location.reload();

                }
            });
         }
    });





}

function caldiffpay() {
    var totolsumall = $('#totolsumall').val();
    var payreal = $('#payreal').val();
    var result = payreal-totolsumall;
    result = result.toFixed(2);

    $('#diffpay').val(result);


}

function getdata(status,id) {
    $('#statusupdate').val(2);
    $("#serachpridref").hide();
    $('#showbill').show();
    while (index >= 1) {
        if(index == 1){
            $('#tbindex'+index).remove();
            // index++;
        }else{
            $('#tbindex'+index).remove();

        }
        index--;
    }
    index = 1 ;


    // $("#payreal").attr("readonly", true);





    $.get("getdatapoupdate?id="+id, function(data) {

        $('#id').val(data[0].id);
        $('#pr_id').val(data[0].pr_id);
        $('#number_pr_ref').val(data[0].number_pr_ref);
        $('#date').val(data[0].date);
        $('#year').val(data[0].year);
        $('#year_th').val(data[0].year_th);
        $('#branch_id').val(data[0].branch_id);

        $('#po_number').val(data[0].po_number);
        $('#po_no_branch').val(data[0].po_no_branch);

        $('#supplier_id').val(data[0].supplier_id);
        $('#supplier_show').val(data[0].Fullnameauto);
        $('#type_pay').val(data[0].type_pay);
        $('#type_pay_show').val(data[0].type_pay_name);
        $('#type_buy').val(data[0].type_buy);
        $('#type_buy_show').val(data[0].type_buy_name);




        $('#in_house').val(data[0].in_house);
        if(data[0].in_house ==1) {
            $('#in_house_show').val('ซื้อภายในประเทศ');
        }else{
            $('#in_house_show').val('ซื้อต่างประเทศ');
        }

        $('#in_budget').val(data[0].in_budget);
        if(data[0].in_budget ==1) {
            $('#in_budget_show').val('ในงบประมาณ');
        }else{
            $('#in_budget_show').val('นอกงบประมาณ');
        }

        $('#urgent_status').val(data[0].urgent_status);
        if(data[0].urgent_status ==1) {
            $('#urgent_status_show').val('การจัดซื้อเร่งด่วน');
        }else{
            $('#urgent_status_show').val('การจัดซื้อไม่เร่งด่วน');
        }

        $('#terms').val(data[0].supplier_termsname);

        $('#code_emp_pr').val(data[0].code_emp);
        $('#code_sup').val(data[0].code_sup);
        $('#id_company').val(data[0].id_company);
        $('#ckaddbranch').val(data[0].ckaddbranch);
        $('#address_send').val(data[0].address_send);
        $('#type_transfer').val(data[0].transfer_config_id);
        $('#emp_code_po').val(data[0].emp_code_po);
        $('#terms').val(data[0].terms);
        $('#totolsumall').val(data[0].totolsumall);
        $('#payreal').val(data[0].payreal);
        $('#diffpay').val(data[0].diffpay);


        $.get("getpobillrunmoney?id="+id, function(data) {
           // console.log(data);

            var countto = (Object.keys(data).length);
            var html2 = '';
            var set2 = '';

            for (set2 = 1; set2 <= countto-1; set2++) {
                html2 += '<tr id="tbindex'+set2+'">';
                html2 += '<td>บิลที่ REF.</td>';
                html2 += '<td><input type="text" name="billref[]" id="billref'+set2+'"  class="form-control" placeholder="บิล No." ></td>';
                html2 += '<td><input type="text" name="payperbill[]" id="payperbill'+set2+'"  class="form-control" placeholder="จ่ายจริง"></td>';
                html2 += '<td><button type="button" id="del" onclick="deleteMe(this);" class="btn btn-danger"><i class="fa fa-trash"></i> </button></td>';
                html2 += '</tr>';
            }
            $('#addrowbilltb').append(html2);

            $.each(data, function(key, value) {
                $('#billref'+key).val(data[key].bill_no);
                $('#payperbill'+key).val(data[key].pay_real);

            });




        });


        $.get("getdatapodetailupdate?id="+id, function(data) {

            var count = (Object.keys(data).length);
            // console.log(count);
            var html = '';
            var totalsum = 0;
            // index = 1;
            var set ='';
            //

            for (set = 1; set <= count-1; set++) {
                html += '<tr id="tbindex'+set+'">';
                html += '<td><select name="config_group_supp_id[]" id="config_group_supp_id' + set + '" class="form-control"  disabled></select></td>';
                html += '<td><select name="accounttype_id[]" id="accounttype_id' + set + '" class="form-control"  style="width: 120px;" ><option value="" >เลือกเลขบัญชี</option></select></td>';
                html += '<td><input type="hidden" name="material_id[]" id="materialid'+set+'" value="0"><input type="text" name="list[]" id="list' + set + '"  class="form-control" readonly></td>';
                html += '<td><input type="text" name="amount[]" id="amount' + set + '" onblur="getamount(this,' + set + ')"   class="form-control" style="width: 90px;" placeholder="จำนวน" readonly></td>';
                html += '<td><input type="text" name="type_amount[]" id="type_amount' + set + '"  class="form-control" style="width: 80px;" placeholder="หน่วยนับ" readonly></td>';
                html += '<td><input type="text" name="price[]" id="price' + set + '" disabled onblur="getprice(this,' + set + ')"   class="form-control" readonly ></td>';
                html += '<td><select name="vat[]" id="vat' + set + '" onchange="calculatevat(this,' + set + ')" class="form-control" style="width: 90px;" disabled></select></td>';
                html += '<td><select name="withhold[]" id="withhold'+set+'" onchange="calculatewithhold(this,'+set+')" class="form-control" style="width: 90px;" disabled></select></td>';
                html += '<td><input type="text" name="total[]" id="total' + set + '"   class="form-control"  readonly></td>';
                html += '<td><input type="text" class="form-control quantity"  name="quantity_get[]" id="quantity' + set + '"  onblur="caldifamount('+set+')" value="0" readonly></td>';
                html += '<td> <input type="text" class="form-control"  name="quantity_loss[]" id="loss' + set + '" readonly></td>';
                html += '<td><span id="statuspodetai' + set + '"> <font color="red">ยังไม่ครบ</font></span></td>';
                html += '</tr>';
            }
            $('#tdbody').append(html);
            var setno ='';
            for (setno = 1; setno <= count-1; setno++) {
                $('#config_group_supp_id0').find('option').clone().appendTo('#config_group_supp_id'+setno+'');
                $('#vat0').find('option').clone().appendTo('#vat'+setno+'');
                $('#withhold0').find('option').clone().appendTo('#withhold'+setno+'');
            }

           // console.log(data);

            $.each(data, function(key, value) {
                $('#config_group_supp_id'+key).val(data[key].config_group_supp_id);

                $('#accounttype_id'+key).empty();

                var _option = "<option value=''>เลือกเลขบัญชี</option>";

                var valuetestacc = data[key].accounttype_id;

                $.get('getdataaccounttype?idacc='+data[key].config_group_supp_id, function (data) {

                    $.each(data, function(key, value) {
                        _option+= "<option value="+data[key].id+">"+data[key].accounttypeno+"__"+data[key].accounttypefull+"</option>";
                    });

                    $('#accounttype_id'+key).append(_option);
                   // console.log(valuetestacc);
                    $('#accounttype_id'+key).val(valuetestacc);
                });

                $('#materialid'+key).val(data[key].materialid);
                $('#list'+key).val(data[key].list);
                $('#amount'+key).val(data[key].amount);
                $('#type_amount'+key).val(data[key].type_amount);
                $('#price'+key).val(data[key].price);
                $('#vat'+key).val(data[key].vat);
                $('#withhold'+key).val(data[key].withhold);
                $('#total'+key).val(data[key].total);
                $('#quantity'+key).val(data[key].quantity_get);
                $('#loss'+key).val(data[key].quantity_loss);
                if(data[key].quantity_loss==0){
                    $('#statuspodetai'+key).empty();
                    $('#statuspodetai'+key).append('<font color="green">ครบ</font>');
                }

                index++;
                totalsum = totalsum+data[key].total;
            });

            totalsum = totalsum.toFixed(2);

            $('#totolsumall').val(totalsum);

            $(".quantity").attr("readonly", false);

        });
    });






}

function caldifamount(thisid) {
    var quantity = 0;
    var amount = $('#amount'+thisid).val();
    quantity = $('#quantity'+thisid).val();


    $('#loss'+thisid).val(amount-quantity);

    var sumlast = amount-quantity;

    if(sumlast>0){
        $('#statuspodetai'+thisid).empty();
        $('#statuspodetai'+thisid).append('<font color="red">ยังไม่ครบ</font>');

    }else if(sumlast==0){
        $('#statuspodetai'+thisid).empty();
        $('#statuspodetai'+thisid).append('<font color="green">ครบ</font>');
    }else if(sumlast<0){
        $('#statuspodetai'+thisid).empty();
        $('#statuspodetai'+thisid).append('<font color="blue">เกินกำหนด</font>');
        $('#quantity'+thisid).val(0);
        $('#loss'+thisid).val(0);

    }

}

function addnewrow() {
    var html = '';
    html += '<tr id="tbindex'+index+'">';
    html += '<td>บิลที่ REF.</td>';
    html += '<td><input type="text" name="billref[]" id="billref'+index+'"  class="form-control" placeholder="บิล No." ></td>';
    html += '<td><input type="text" name="payperbill[]" id="payperbill'+index+'"  class="form-control" placeholder="จ่ายจริง"></td>';
    html += '<td><button type="button" id="del" onclick="deleteMe(this);" class="btn btn-danger"><i class="fa fa-trash"></i> </button></td>';
    html += '</tr>';

    $('#addrowbilltb').append(html);
}



function deleteMe(me) {
    var tr = $(me).closest("tr");
    tr.remove();
    // console.log(me);
}
