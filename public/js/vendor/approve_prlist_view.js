/**
 * Created by pacharapol on 1/14/2018 AD.
 */

$(document).ready(function() {
    $('#example').DataTable();
    $('.select2').select2({ width: '100%' });


    // $(".datepicker").datepicker({
    //     format: 'dd-mm-yyyy',
    // });
    $('.datepicker').daterangepicker();

});


function deleteMe(val){
    var id = val;
    var branch = $('#branch').val();
    bootbox.confirm({
          title: "แจ้งเตือน!!!",
          message: "ยืนยันการลบรายการ",
          buttons: {
              cancel: {
                  label: '<i class="fa fa-times"></i> Cancel'
              },
              confirm: {
                  label: '<i class="fa fa-check"></i> Confirm'
              }
          },
          callback: function(result) {
              if (result) {
                   // console.log('deleteMe');
                   $.post('deletepprdetail', { id: id , branch:branch }, function(res) {
                        console.log(res);
                          location.reload();
                    });

              }

          }
      });


}
