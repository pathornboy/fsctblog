/**
 * Created by pacharapol on 1/14/2018 AD.
 */
var index = 1;
var amount = 0;
var price = 0;
var vat = 0;
var availableTags = '';
var objasset = '';
var objohter = '';

$(document).ready(function() {
    $('#example').DataTable();
    $('.select2').select2({ width: '100%' });



    $("#datebill").datepicker({
          format: 'yyyy-mm-dd',
    });


    $('#addrow').on("click", function() {
        // console.log(index);
        CloneTableRow();
        //bindDataWithStep();
    });




    $.get("getmainmaterialall", function(res) {
        availableTags = res;
    });

    $.get("getgoodassetall", function(res) {
        // console.log(res);
        objasset = res;
    });

    $.get("getotherall", function(res) {

        objohter = res;
    });

    $('#checkapprovedall').click(function(event) {
        if (this.checked) {
            // Iterate each checkbox
            $('.checkapproved:checkbox').each(function() {//checkapprovedmd
                this.checked = true;
            });
        } else {
            $('.checkapproved:checkbox').each(function() {
                this.checked = false;
            });
        }
    });

    $('#checkapprovedallmd').click(function(event) {
        if (this.checked) {
            // Iterate each checkbox
            $('.checkapprovedmd:checkbox').each(function() {//
                this.checked = true;
            });
        } else {
            $('.checkapprovedmd:checkbox').each(function() {
                this.checked = false;
            });
        }
    });


});

function getdatesubmit() {

    var valid = $('#configFormvendors').validator('validate').has('.has-error').length;
    if (valid == 0) {

        bootbox.confirm({
            title: "ยืนยันการทำรายการ",
            message: "ยืนยันการตรวจสอบ",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function(result) {
                if (result) {
                    Savehead();
                }

            }
        });
    }
    return false;
}

function Savehead() {

    var dataposthead = {
        'id_compay': $('select[name=id_compay]').val(),
        'list': $('select[name=list]').val(),
        'amount': $('#amount').val(),
        'vat': $('#vat').val(),
        'vat_money': $('#vat_money').val(),
        'total': $('#total').val(),
        'po_ref': '',
        'note': $('#note').val()
    };

    $.post('insertpaypre', { data: dataposthead }, function(res) {
          location.reload();
    });


    //Savedetail()

}

function saveporef(val){
      $('#reservemoneyid').val('');
      $('#reservemoneyid').val(val);

}

function getponumbersubmit(){
  var valid = $('#configPo').validator('validate').has('.has-error').length;
      if (valid == 0) {

          bootbox.confirm({
              title: "ยืนยันการทำรายการ",
              message: "ยืนยันการตรวจสอบ",
              buttons: {
                  cancel: {
                      label: '<i class="fa fa-times"></i> Cancel'
                  },
                  confirm: {
                      label: '<i class="fa fa-check"></i> Confirm'
                  }
              },
              callback: function(result) {
                  if (result) {
                        var formData = new FormData();
                        var files   = $('#fileref')[0].files[0];
                        formData.append('fileref', files);
                        formData.append('id', $('#reservemoneyid').val());
                        formData.append('ponumber', $('#ponumber').val());
                        formData.append('bill_no', $('#bill_no').val());



                        // console.log(formData);

                            $.ajax({
                                  url: 'insertporef',
                                  type: "POST",
                                  data: formData,
                                  async:false,
                                  processData: false,
                                  contentType: false,
                                  success: function (res) {

                                    console.log(res);
                                  }
                              });



                  }

              }
          });
      }
      return false;


}
