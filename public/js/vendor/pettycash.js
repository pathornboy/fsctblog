function getdata_to_delete(id) {

    $.get('getpettycash?id=' + id, function(data) {
        if (confirm('ต้องการจะลบข้อมูลนี้หรือไม่ ?')) {
            deletedata(data);

        }
    });
}

function getdata_to_edit(id) {

    $.get('getpettycash?id=' + id, function(data) {
        // console.log(data['id']);
        $('#modaledit').modal('show');
        $("#id_edit").val(data['id']);
        $("#name_edit").val(data['listname']);
    });
}

function deletedata(data) {
    $.ajax({
        url: "deletepettycash",
        type: "POST",
        data: { data: data, _token: $('input[name="_token"]').val(), },
        success: function(res) {
            alert("ลบข้อมูลสำเร็จ");
            location.reload();
        },
        error: function() {
            alert("error!!!!");
        }
    }); //end of ajax
}


function test() {
    console.log(555)
}