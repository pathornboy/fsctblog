/**
 * Created by pacharapol on 1/14/2018 AD.
 */
var index = 1;
var amount = 0;
var price = 0;
var vat = 0;
$(document).ready(function() {
    $('#example').DataTable();
    $('.select2').select2({ width: '100%' });

    $('#addrow').on("click", function() {
        // console.log(index);

        CloneTableRow();
        //bindDataWithStep();
    });

    $(".datepicker").datepicker({
        format: 'dd-mm-yyyy',
    });




});


function serachprid() {
    var week = $('#week').val();
    var id_company = $('#id_company').val();
    var supplier = $('#supplier_id').val();
    var vat = $('#vat').val();
    // console.log(week);
    while (index >= 1) {
        if (index == 1) {
            $('#tbindex' + index).remove();
            $('#modalprref0').empty();
            // index++;
        } else {
            $('#tbindex' + index).remove();

        }
        index--;
    }
    index = 1;


    $('.detailshow').show();

    $.post('getdataprbycodepr', { week: week, idcompany: id_company, supplier: supplier, vat: vat }, function(data) {
        showtable(data);

    });

    $.get('getidpo?id=' + id_company, function(data) {
        var dataprint = JSON.parse(data);
        console.log(data);

        $('#po_number').val(dataprint.idgen);
        $('#po_no_branch').val(dataprint.idnumber);

    });

    $.get('getdatavendorcenter?id=' + supplier, function(data) {
        $('#terms_show').val(data[0].suppliertermsname);
        $('#terms').val(data[0].terms_id);

        //console.log(data);
    });

    $.get('getdatacompany?id=' + id_company, function(data) {
        //console.log(data);

        $('#address_send').val(data[0].address + '  โทรศัพท์  ' + data[0].Tel + '  เลขระจำตัวผู้เสียภาษี  ' + data[0].business_number);

    });







}

function ckaddress() {
    var ckb = $("#ckaddbranch").is(':checked');


    if (ckb == true) {
        var id_company = $('#id_company').val();
        $.get('getdatacompany?id=' + id_company, function(data) {
            //console.log(data);
            $("#address_send").attr("readonly", true);
            $('#address_send').val(data[0].address + '  โทรศัพท์  ' + data[0].Tel + '  เลขระจำตัวผู้เสียภาษี  ' + data[0].business_number);
        });

    } else {
        $("#address_send").attr("readonly", false);
        $('#address_send').val('');
    }
}

function savepo() {


    var type_pay = $('#type_pay').val();
    var type_buy = $('#type_buy').val();
    var type_transfer = $('#type_transfer').val();

    if(type_pay == '' || type_buy == '' || type_transfer == ''){
      //console.log('xxx');
          bootbox.alert("กรุณาเลือก ประเภทการจ่าย , ประเภทการซื้อ , เงื่อนไขในการส่ง");
    }else{
          Savehead();
    }



}

function Savehead() {

    var ckb = $("#ckaddbranch").is(':checked');
    var resultck = 0;


    if (ckb == true) {
        resultck = 1;
    } else {
        resultck = 0;
    }

    var dataposthead = {
        'id': $('#id').val(),
        'week': $('#week').val(),

        'number_pr_ref': $('#number_pr_ref').val(),
        'date': $('#date').val(),
        'year': $('#year').val(),
        'year_th': $('#year_th').val(),
        'branch_id': $('#branch_id').val(),
        'po_number': $('#po_number').val(),
        'po_no_branch': $('#po_no_branch').val(),
        'supplier_id': $('#supplier_id').val(),
        'type_pay': $('#type_pay').val(),
        'type_buy': $('#type_buy').val(),
        'in_house': $('input[name^="in_house"]:checked').val(),
        'in_budget': $('input[name^="in_budget"]:checked').val(),
        'urgent_status': $('input[name^="urgent_statu"]:checked').val(),
        'code_sup': $('#code_sup').val(),
        'id_company': $('#id_company').val(),
        'ckaddbranch': resultck,
        'address_send': $('#address_send').val(),
        'transfer_config_id': $('#type_transfer').val(),
        'emp_code_po': $('#code_emp_po').val(),
        'terms': $('#terms').val(),
        'totolsumall': $('#totolsumall').val(),
        'payreal': $('#payreal').val(),
        'diffpay': $('#diffpay').val(),
        'vat': $('#vat').val(),
        'code_emp_approve': $('#code_emp_approve').val(),
    };


    //console.log(dataposthead);




    $.post('addpoinsertupdate', { data: dataposthead }, function(res) {
        //console.log(res);
        savedetail(res);
    });




}

function insertnew() {
    // $('#serachpridref').mouseover(function(){
    //   if()
    // });

    $('#statusupdate').val(0);


    $("#serachpridref").show();
    $("#payreal").attr("readonly", false);
    $(".quantity").attr("readonly", true);

    $('#showbill').hide();
    while (index >= 1) {
        if (index == 1) {
            $('#tbindex' + index).remove();
            // index++;
        } else {
            $('#tbindex' + index).remove();

        }
        index--;
    }
    index = 1;


    $('#Btn_save').show();
    $('#number_pr_ref').val('');
    $('#pr_id').val('');
    $('#supplier_id').val('');
    $('#supplier_show').val('');
    $('#type_pay').val('');
    $('#type_pay_show').val('');
    $('#type_buy').val('');
    $('#type_buy_show').val('');

    $('#in_house').val('');
    $('#in_house_show').val('');

    $('#in_budget').val('');
    $('#in_budget_show').val('');

    $('#urgent_status').val('');
    $('#urgent_status_show').val('');

    $('#terms').val('');

    $('#supplier_id').val('').trigger('change');
    $('#type_pay').val('');
    $('#type_buy').val('');

    $('#config_group_supp_id0').val('');
    $('#list0').val('');
    $('#amount0').val('');
    $('#type_amount0').val('');
    $('#price0').val('');
    $('#vat0').val(0);
    $('#total0').val('');
    $('#daterecentpurchases0').val('');
    $('#avg0').val('');
    $('#note0').val('');
    $('#totolsumall').val('');

    $(".datepicker").datepicker({
        format: 'dd-mm-yyyy',
    });

    $('.detailshow').hide();


}

function selecttranfer(val) {
    var id_company = val.value;
    $.get('getdatacompany?id=' + id_company, function(data) {
        //console.log(data);

        $('#address_send').val(data[0].address + '  โทรศัพท์  ' + data[0].Tel + '  เลขระจำตัวผู้เสียภาษี  ' + data[0].business_number);

    });
}




function showtable(data) {

    console.log(data)


    var count = (Object.keys(data).length);
    console.log(count);

    var html = '';
    var totalsum = 0;
    //
    var set = '';

    if(count!=0){

      for (set = 1; set <= count - 1; set++) {
          html += '<tr id="tbindex' + set + '">';
          html += '<td><select name="config_group_supp_id[]" id="config_group_supp_id' + set + '" class="form-control"  disabled></select></td>';
          html += '<td><input type="hidden" name="material_id[]" id="materialid' + set + '" value="0"><input type="text" name="list[]" id="list' + set + '"  class="form-control" readonly></td>';
          html += '<td><input type="text" name="amount[]" id="amount' + set + '" onblur="getamount(this,' + set + ')"   class="form-control" style="width: 90px;" placeholder="จำนวน" readonly></td>';
          html += '<td><input type="text" name="type_amount[]" id="type_amount' + set + '"  class="form-control" style="width: 80px;" placeholder="หน่วยนับ" readonly></td>';
          html += '<td><input type="text" name="price[]" id="price' + set + '"  onblur="getprice(this,' + set + ')"   class="form-control"  ></td>';
        /*  html += '<td><select name="vat[]" id="vat' + set + '" onchange="calculatevat(this,' + set + ')" class="form-control" style="width: 90px;" ></select></td>'; */
          html += '<td><select name="withhold[]" id="withhold' + set + '" onchange="calculatewithhold(this,' + set + ')" class="form-control" style="width: 90px;" ></select></td>';
          html += '<td><input type="text" name="resultwithhold[]" id="resultwithhold' + set + '"   class="form-control"  readonly></td>';
          html += '<td><input type="text" name="total[]" id="total' + set + '"   class="form-control"  readonly></td>';
          html += '<td><input type="text" class="form-control quantity"  name="quantity_get[]" id="quantity' + set + '" onblur="caldifamount(' + set + ')" value="0" readonly></td>';
          html += '<td> <input type="text" class="form-control"  name="quantity_loss[]" id="loss' + set + '" readonly></td>';
          html += '<td><span id="statuspodetai' + set + '"> <font color="red">ยังไม่ครบ</font></span></td>';
          html += '<td><span id="modalprref' + set + '"> </span></td>';
          html += '</tr>';
      }
      $('#tdbody').append(html);

    }else{
      $('#tdbody').empty();
      html += '<tr>';
      html += '<td align="center" colspan="12">';
      html += '<font color="red">ไม่พบข้อมูล</font>';
      html += '</td>';
      html += '</tr>';

      $('#tdbody').append(html);
      $('#totolsumall').val('');

    }


    var setno = '';
    for (setno = 1; setno <= count - 1; setno++) {
        $('#config_group_supp_id0').find('option').clone().appendTo('#config_group_supp_id' + setno + '');
      //  $('#vat0').find('option').clone().appendTo('#vat' + setno + '');
        $('#withhold0').find('option').clone().appendTo('#withhold' + setno + '');
    }




    //  console.log(week);
    var total = 0;
    var totalthisrow = 0;

    $.each(data, function(key, value) {
        $('#config_group_supp_id' + key).val(data[key].config_group_supp_id);
        $('#accounttype_id' + key).empty();
        $('#materialid' + key).val(data[key].materialid);
        $('#list' + key).val(data[key].list);
        $('#amount' + key).val(data[key].amount);
        $('#type_amount' + key).val(data[key].type_amount);
        $('#price' + key).val(data[key].price);
      //  $('#vat' + key).val(data[key].vat);
      // if()
        $('#withhold' + key).val(data[key].withhold);

        totalthisrow = (data[key].amount * data[key].price)*(data[key].withhold/100);
        $('#resultwithhold' + key).val(totalthisrow);

        totalresult = ((data[key].amount) * (data[key].price)) ;
        total = totalresult.toFixed(2);
        $('#total' + key).val(total);
        $('#modalprref' + key).append('<a href="#" data-toggle="modal" onclick=showprref(' + data[key].materialid + ') data-target="#prdoc" ><img src="images/global/docs.png"></a>');
        index++;
        //totalsum = totalsum + totalresult;
    });
}


function setoption(index) {

    $('#config_group_supp_id0').find('option').clone().appendTo('#config_group_supp_id' + index + '');
  //  $('#vat0').find('option').clone().appendTo('#vat' + index + '');
    $('#withhold0').find('option').clone().appendTo('#withhold' + index + '');

}


function savedetail(idhead) {
    var arrconfig_group_supp_id = [];
    var arrlist = [];
    var arramount = [];
    var arrtype_amount = [];
    var arrprice = [];
    var arrvat = [];
    var arrtotal = [];
    var quantity_get = [];
    var quantity_loss = [];
    var arrmaterialid = [];
    var arraccounttypeid = [];
    var arrwithhold = [];




    $('select[name^="config_group_supp_id[]"]').each(function() {
        arrconfig_group_supp_id.push($(this).val());
    });
    $('input[name^="material_id[]"]').each(function() {
        arrmaterialid.push($(this).val());
    });
    $('input[name^="list[]"]').each(function() {
        arrlist.push($(this).val());
    });
    $('input[name^="amount[]"]').each(function() {
        arramount.push($(this).val());
    });
    $('input[name^="type_amount[]"]').each(function() {
        arrtype_amount.push($(this).val());
    });
    $('input[name^="price[]"]').each(function() {
        arrprice.push($(this).val());
    });
    // $('select[name^="vat[]"]').each(function() {
    //     arrvat.push($(this).val());
    // });
    $('select[name^="withhold[]"]').each(function() {
        arrwithhold.push($(this).val());
    });
    $('input[name^="total[]"]').each(function() {
        arrtotal.push($(this).val());
    });
    $('input[name^="quantity_get[]"]').each(function() {
        quantity_get.push($(this).val());
    });
    $('input[name^="quantity_loss[]"]').each(function() {
        quantity_loss.push($(this).val());
    });


    var datapostdetail = {
        'config_group_supp_id': arrconfig_group_supp_id,
        'arrmaterialid': arrmaterialid,
        'arrlist': arrlist,
        'arramount': arramount,
        'arrtype_amount': arrtype_amount,
        'arrprice': arrprice,
        //'arrvat': arrvat,
        'arrwithhold': arrwithhold,
        'arrtotal': arrtotal,
        'quantity_get': quantity_get,
        'quantity_loss': quantity_loss,
        'idhead': idhead,
    };



    var updatebill = $('#statusupdate').val();

    if (updatebill == 2) {
        var arrbillref = [];
        var arrpayperbill = [];
        var arrdatebill = [];
        var arrreturnperbill = [];

        $('input[name^="billref[]"]').each(function() {
            arrbillref.push($(this).val());
        });

        $('input[name^="payperbill[]"]').each(function() {
            arrpayperbill.push($(this).val());
        });

        $('input[name^="datebill[]"]').each(function() {
            arrdatebill.push($(this).val());
        });
        $('input[name^="returnperbill[]"]').each(function() {
            arrreturnperbill.push($(this).val());
        });

        var datapostrunmoney = {
            'arrbillref': arrbillref,
            'arrpayperbill': arrpayperbill,
            'arrdatebill': arrdatebill,
            'idhead': idhead,
            'arrreturnperbill': arrreturnperbill
        };



        $.post('insertbillmoney', { data: datapostrunmoney }, function(res) {
            console.log(res);
        });


        var datapostrunstock = {
            'config_group_supp_id': arrconfig_group_supp_id,
            'arrlist': arrlist,
            'arramount': arramount,
            'arrtype_amount': arrtype_amount,
            'arrprice': arrprice,
            //'arrvat': arrvat,
            'arrwithhold': arrwithhold,
            'arrtotal': arrtotal,
            'quantity_get': quantity_get,
            'quantity_loss': quantity_loss,
            'idhead': idhead,
            'arrbillref': arrbillref,
            'arrmaterialid': arrmaterialid,
        };

        $.post('insertlogbillrunstock', { data: datapostrunstock }, function(res) {
          //  console.log(res);
        });
    }

    var week = $('#week').val();
    var id_company = $('#id_company').val();
    var supplier = $('#supplier_id').val();
    var po_number = $('#po_number').val();

    $.post('insertporefpr', { week: week, idcompany: id_company, supplier: supplier, po_number: po_number, idpo: idhead }, function(data) {
        console.log(data);
    });


    $.post('insertdetailpo', { data: datapostdetail }, function(res) {
        if (res == 1) {
            bootbox.alert({
                title: "แจ้งเตือน",
                message: "บันทึกข้อมูลเรียบร้อยแล้ว!!",
                callback: function() {
                    // $('#ConfirmDialog').modal('hide');
                    $('#myModal').modal('hide');
                    location.reload();

                }
            });
        }
    });





}

function caldiffpay() {
    var totolsumall = $('#totolsumall').val();
    var payreal = $('#payreal').val();
    var result = payreal - totolsumall;
    result = result.toFixed(2);

    $('#diffpay').val(result);


}

function getdata(status, id) {
    $('#statusupdate').val(2);
    $("#serachpridref").hide();
    $('#showbill').show();
    while (index >= 1) {
        if (index == 1) {
            $('#tbindex' + index).remove();
            $('#modalprref0').empty();
            // index++;
        } else {
            $('#tbindex' + index).remove();

        }
        index--;
    }
    index = 1;


    // $("#payreal").attr("readonly", true);

    $('.detailshow').show();




    $.get("getdatapoupdate?id=" + id, function(data) {
      console.log(data);

        $('#week').val(data[0].week).trigger('change');
        $('#supplier_id').val(data[0].supplier_id).trigger('change');

        $('#id').val(data[0].id);
        $('#pr_id').val(data[0].pr_id);
        $('#number_pr_ref').val(data[0].number_pr_ref);
        $('#date').val(data[0].date);
        $('#year').val(data[0].year);
        $('#year_th').val(data[0].year_th);
        $('#branch_id').val(data[0].branch_id);

        $('#po_number').val(data[0].po_number);
        $('#po_no_branch').val(data[0].po_no_branch);

        $('#supplier_id').val(data[0].supplier_id);
        $('#supplier_show').val(data[0].Fullnameauto);
        $('#type_pay').val(data[0].type_pay);
        $('#type_pay_show').val(data[0].type_pay_name);
        $('#type_buy').val(data[0].type_buy);
        $('#type_buy_show').val(data[0].type_buy_name);




        $('#in_house').val(data[0].in_house);
        if (data[0].in_house == 1) {
            $('#in_house_show').val('ซื้อภายในประเทศ');
        } else {
            $('#in_house_show').val('ซื้อต่างประเทศ');
        }

        $('#in_budget').val(data[0].in_budget);
        if (data[0].in_budget == 1) {
            $('#in_budget_show').val('ในงบประมาณ');
        } else {
            $('#in_budget_show').val('นอกงบประมาณ');
        }

        $('#urgent_status').val(data[0].urgent_status);
        if (data[0].urgent_status == 1) {
            $('#urgent_status_show').val('การจัดซื้อเร่งด่วน');
        } else {
            $('#urgent_status_show').val('การจัดซื้อไม่เร่งด่วน');
        }

        $('#terms').val(data[0].supplier_termsname);

        $('#code_emp_pr').val(data[0].code_emp);
        $('#code_sup').val(data[0].code_sup);
        $('#id_company').val(data[0].id_company);
        $('#ckaddbranch').val(data[0].ckaddbranch);
        $('#address_send').val(data[0].address_send);
        $('#type_transfer').val(data[0].transfer_config_id);
        $('#emp_code_po').val(data[0].emp_code_po);
        $('#terms').val(data[0].terms);
        $('#totolsumall').val(data[0].totolsumall);
        $('#payreal').val(data[0].payreal);
        $('#diffpay').val(data[0].diffpay);
        $('#vat').val(data[0].vat);
        $('#vat').attr('readonly', true);
        $('#code_emp_approve').val(data[0].code_emp_approve);


        $.get("getpobillrunmoney?id=" + id, function(data) {
            console.log(data);

            var countto = (Object.keys(data).length);
            var html2 = '';
            var set2 = '';


            for (set2 = 1; set2 <= countto - 1; set2++) {
                html2 += '<tr id="tbindex' + set2 + '">';
                html2 += '<td><input type="hidden" class="form-control" id="datebill' + set2 + '" name="datebill[]" readonly "><input type="text" class="form-control" id="datebillshow' + set2 + '" name="datebillshow[]" readonly ></td>';
                html2 += '<td><input type="text" name="billref[]" id="billref' + set2 + '"  class="form-control" placeholder="บิล No." ></td>';
                html2 += '<td><input type="text" name="returnperbill[]" id="returnperbill' + set2 + '"  class="form-control" placeholder="จ่ายจริง"></td>';
                html2 += '<td><input type="text" name="payperbill[]" id="payperbill' + set2 + '"  class="form-control" placeholder="จ่ายจริง"></td>';
                html2 += '<td align="center"><span id="imgprintrecip' + set2 + '"></span></td>';
                html2 += '<td><button type="button" id="del" onclick="deleteMe(this);" class="btn btn-danger"><i class="fa fa-trash"></i> </button></td>';
                html2 += '</tr>';
            }
            $('#addrowbilltb').append(html2);

            $('#imgprintrecip0').empty();

            //console.log(data);
            var str = '';
            $.each(data, function(key, value) {

                str = data[key].datebill;
                var res = str.split("-");
                var year = parseInt(res[0]);

                var resultyear = ((year + 543));

                $('#imgprintrecip' + key).empty();

                $('#datebillshow' + key).val(res[2] + '-' + res[1] + '-' + resultyear);
                $('#datebill' + key).val(data[key].datebill);
                $('#billref' + key).val(data[key].bill_no);
                $('#payperbill' + key).val(data[key].pay_real);
                $('#returnperbill' + key).val(data[key].returnperbill);
                if (data[key].id) {
                    $('#imgprintrecip' + key).append('<a href="printreceipt/' + data[key].bill_no + '/' + data[key].po_headid + '" target="_blank"><img src="images/global/imagereceipt.png" ></a>');
                }

            });




        });


        $.get("getdatapodetailupdate?id=" + id, function(data) {

            var count = (Object.keys(data).length);
            // console.log(count);
            var html = '';
            var totalsum = 0;
            // index = 1;
            var set = '';
            //

            for (set = 1; set <= count - 1; set++) {
                html += '<tr id="tbindex' + set + '">';
                html += '<td><select name="config_group_supp_id[]" id="config_group_supp_id' + set + '" class="form-control"  disabled></select></td>';
                html += '<td><input type="hidden" name="material_id[]" id="materialid' + set + '" value="0"><input type="text" name="list[]" id="list' + set + '"  class="form-control" readonly></td>';
                html += '<td><input type="text" name="amount[]" id="amount' + set + '" onblur="getamount(this,' + set + ')"   class="form-control" style="width: 90px;" placeholder="จำนวน" readonly></td>';
                html += '<td><input type="text" name="type_amount[]" id="type_amount' + set + '"  class="form-control" style="width: 80px;" placeholder="หน่วยนับ" readonly></td>';
                html += '<td><input type="text" name="price[]" id="price' + set + '"  onblur="getprice(this,' + set + ')"   class="form-control"  ></td>';
              /* html += '<td><select name="vat[]" id="vat' + set + '" onchange="calculatevat(this,' + set + ')" class="form-control" style="width: 90px;" ></select></td>'; */
                html += '<td><select name="withhold[]" id="withhold' + set + '" onchange="calculatewithhold(this,' + set + ')" class="form-control" style="width: 90px;" ></select></td>';
                html += '<td><input type="text" name="resultwithhold[]" id="resultwithhold' + set + '"   class="form-control"  readonly></td>';
                html += '<td><input type="text" name="total[]" id="total' + set + '"   class="form-control"  readonly></td>';
                html += '<td><input type="text" class="form-control quantity"  name="quantity_get[]" id="quantity' + set + '" onblur="caldifamount(' + set + ')" value="0" readonly></td>';
                html += '<td> <input type="text" class="form-control"  name="quantity_loss[]" id="loss' + set + '" readonly></td>';
                html += '<td><span id="statuspodetai' + set + '"> <font color="red">ยังไม่ครบ</font></span></td>';
                html += '<td><span id="modalprref' + set + '"> </span></td>';
                html += '</tr>';
            }
            $('#tdbody').append(html);
            var setno = '';
            for (setno = 1; setno <= count - 1; setno++) {
                $('#config_group_supp_id0').find('option').clone().appendTo('#config_group_supp_id' + setno + '');
              //  $('#vat0').find('option').clone().appendTo('#vat' + setno + '');
                $('#withhold0').find('option').clone().appendTo('#withhold' + setno + '');
            }

            // console.log(data);

            $.each(data, function(key, value) {
                $('#config_group_supp_id' + key).val(data[key].config_group_supp_id);

                $('#accounttype_id' + key).empty();
                $('#materialid' + key).val(data[key].materialid);
                $('#list' + key).val(data[key].list);
                $('#amount' + key).val(data[key].amount);
                $('#type_amount' + key).val(data[key].type_amount);
                $('#price' + key).val(data[key].price);
                //$('#vat' + key).val(data[key].vat);
                $('#withhold' + key).val(data[key].withhold);
                totalthisrow = (data[key].amount * data[key].price)*(data[key].withhold/100);
                $('#resultwithhold' + key).val(totalthisrow);

                $('#total' + key).val(data[key].total);
                $('#quantity' + key).val(data[key].quantity_get);
                $('#loss' + key).val(data[key].quantity_loss);
                $('#modalprref' + key).append('<a href="#" data-toggle="modal" onclick=showprref(' + data[key].materialid + ') data-target="#prdoc" ><img src="images/global/docs.png"></a>');
                if (data[key].quantity_loss == 0) {
                    $('#statuspodetai' + key).empty();
                    $('#statuspodetai' + key).append('<font color="green">ครบ</font>');
                }

                index++;
                // console.log(data[key].total);
                // if (data[key].total) {
                //     totalsum = totalsum + data[key].total;
                // } else {
                //     totalsum = totalsum + 0;
                // }

            });

            // totalsum = totalsum.toFixed(2);
            //
            // $('#totolsumall').val(totalsum);

            $(".quantity").attr("readonly", false);

        });
    });






}

function caldifamount(thisid) {
    var quantity = 0;
    var amount = $('#amount' + thisid).val();
    quantity = $('#quantity' + thisid).val();


    $('#loss' + thisid).val(amount - quantity);

    var sumlast = amount - quantity;

    if (sumlast > 0) {
        $('#statuspodetai' + thisid).empty();
        $('#statuspodetai' + thisid).append('<font color="red">ยังไม่ครบ</font>');

    } else if (sumlast == 0) {
        $('#statuspodetai' + thisid).empty();
        $('#statuspodetai' + thisid).append('<font color="green">ครบ</font>');
    } else if (sumlast < 0) {
        $('#statuspodetai' + thisid).empty();
        $('#statuspodetai' + thisid).append('<font color="blue">เกินกำหนด</font>');
        $('#quantity' + thisid).val(0);
        $('#loss' + thisid).val(0);

    }

}

function getprice(val, index) {
    $('#total' + index).val(0);
    price = val.value;
    resulttotalindex(index);
}


function resulttotalindex(index) {
    $('#total' + index).val(0);
    var amount = $('#amount' + index).val();
    var total = (amount * price);
    $('#total' + index).val(total);
    calltotallast()

}

function calculatevat(val, index) {
    calltotallast();
}

function caltotal(price, amount, tax, withhold, index) {
    var resulttax = 0;
    var resultwithhold = 0;
    if (tax == 0) {
        resulttax = 0;
    } else {
        resulttax = ((price * amount) * (tax / 100));
        resulttax = resulttax.toFixed(2);

    }

    if (withhold == 0) {
        resultwithhold = 0;
    } else {
        resultwithhold = ((price * amount) * (withhold / 100));
        resultwithhold = resultwithhold.toFixed(2);
    }
    var result = Number((price * amount)) + Number(resulttax) - Number(resultwithhold);
    result = result.toFixed(2);

    $('#total' + index).val(result);
    calltotallast();

}

function calculatewithhold(val, index) {
    var withhold = val.value;
    //var tax = $('#vat' + index).val();
    var setprice = $('#price' + index).val();
    var setamount = $('#amount' + index).val();

    var total = setprice * setamount;

     resultwithhold = total * (withhold/100);
      //console.log(total);
      //console.log(resultwithhold);



    // if(withhold==0){
    //     $('#resultwithhold').val(0);
    //     total = total.toFixed(2);
    //     $('#total'+index).val(total);
    // }else{

        total = total.toFixed(2);
        resultwithhold = resultwithhold.toFixed(2);
        console.log(resultwithhold);
        $('#resultwithhold'+index).val(resultwithhold);
        $('#total'+index).val(total);
    //}
    //caltotal(setprice, setamount, tax, withhold, index);
    calltotallast();

}

function calltotallast() {
    var arrtotal = [];
    var arrwithhold = []
    $('input[name^="total[]"]').each(function() {
        arrtotal.push($(this).val());
    });

    $('input[name^="resultwithhold[]"]').each(function() {
        arrwithhold.push($(this).val());
    });

    var total = 0;
    var withhold = 0;
    $.each(arrtotal, function() {
        total += Number(this);
    });

    $.each(arrwithhold, function() {
        withhold += Number(this);
    });
    //console.log(total);
    console.log(withhold);

    var vat = $('#vat').val();
    vat =  total * (vat/100);
    console.log(vat);

    var lasttotal = total + vat - withhold;



    $('#totolsumall').val(lasttotal);

}


function showprref(materialid) {
    var week = $('#week').val(); //for code;
    var id_company = $('#id_company').val();
    var supplier = $('#supplier_id').val();
    var materialid = materialid;

    $('#tbodyshowprref').empty();

    $.post('getprrefmaterialid', { 'week': week, 'idcompany': id_company, 'supplier': supplier, 'materialid': materialid }, function(data) {
        console.log(data);
        var htmlpr = '';
        $.each(data, function(key, value) {
            htmlpr += "<tr>";
            htmlpr += "<td>" + data[key].number_ppr + "</td>";
            htmlpr += "<td>" + data[key].branch_id + "</td>";
            htmlpr += "<td>" + data[key].name_branch + "</td>";
            htmlpr += "<td>" + data[key].list + "</td>";
            htmlpr += "<td>" + data[key].price + "</td>";
            htmlpr += "<td>" + data[key].amount + "</td>";
            htmlpr += "<td>" + data[key].withhold + "</td>";
            htmlpr += "<td>" + data[key].vat + "</td>";
            htmlpr += "<td>" + data[key].total + "</td>";
            htmlpr += "</tr>";
        });

        $('#tbodyshowprref').append(htmlpr);

    });

}

function addnewrow() {
    var html = '';
    html += '<tr id="tbindex' + index + '">';
    html += '<td><input type="hidden" class="form-control" id="datebill' + index + '" name="datebill[]" readonly "><input type="text" class="form-control" id="datebillshow' + index + '" name="datebillshow[]" readonly ></td>';
    html += '<td><input type="text" name="billref[]" id="billref' + index + '"  class="form-control" placeholder="บิล No." ></td>';
    html += '<td><input type="text" name="payperbill[]" id="payperbill' + index + '"  class="form-control" placeholder="จ่ายจริง"></td>';
    html += '<td><input type="text" name="returnperbill[]" id="returnperbill' + index + '"  class="form-control" placeholder="จ่ายคืน"></td>';
    html += '<td></td>';
    html += '<td><button type="button" id="del" onclick="deleteMe(this);" class="btn btn-danger"><i class="fa fa-trash"></i> </button></td>';
    html += '</tr>';

    $('#addrowbilltb').append(html);
    $('#datebill' + index).val($('#dateset').val());
    $('#datebillshow' + index).val($('#dateshowset').val());
    index++;
}

function deleteMe(me) {
    var tr = $(me).closest("tr");
    tr.remove();
    // console.log(me);
}
