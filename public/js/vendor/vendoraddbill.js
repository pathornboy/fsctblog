/**
 * Created by pacharapol on 1/14/2018 AD.
 */
var index = 1;
var amount = 0;
var price = 0;
var vat = 0;
var availableTags = '';
var objasset = '';
var objohter = '';

$(document).ready(function() {
    $('#example').DataTable();
    $('.select2').select2({ width: '100%' });

    $('#addrow').on("click", function() {
        // console.log(index);

        CloneTableRow();
        //bindDataWithStep();
    });

    $(".datepicker").datepicker({
        format: 'dd-mm-yyyy',
    });



    $.get("getmainmaterialall", function(res) {
        availableTags = res;
    });

    $.get("getgoodassetall", function(res) {
        console.log(res);
        objasset = res;
    });

    $.get("getotherall", function(res) {

        objohter = res;
    });

    $('#checkapprovedall').click(function(event) {
        if (this.checked) {
            // Iterate each checkbox
            $('.checkapproved:checkbox').each(function() {//checkapprovedmd
                this.checked = true;
            });
        } else {
            $('.checkapproved:checkbox').each(function() {
                this.checked = false;
            });
        }
    });

    $('#checkapprovedallmd').click(function(event) {
        if (this.checked) {
            // Iterate each checkbox
            $('.checkapprovedmd:checkbox').each(function() {//
                this.checked = true;
            });
        } else {
            $('.checkapprovedmd:checkbox').each(function() {
                this.checked = false;
            });
        }
    });


});

function getdatesubmit() {

    var valid = $('#configFormvendors').validator('validate').has('.has-error').length;
    if (valid == 0) {

        bootbox.confirm({
            title: "ยืนยันใบ PR?",
            message: "ยืนยันการตรวจสอบ",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function(result) {
                if (result) {
                    Savehead();
                }

            }
        });
    }
    return false;
}

function Savehead() {

    var dataposthead = {
        'id': $('#id').val(),
        'week': $('#week').val(),
        'date': $('#date').val(),
        'year': $('#year').val(),
        'year_th': $('#year_th').val(),
        'bbr_no_branch': $('#bbr_no_branch').val(),
        'branch_id': $('#branch_id').val(),
        'number_ppr': $('#number_ppr').val(),
        'type_pay': $('#type_pay').val(),
        'type_buy': $('#type_buy').val(),
        'in_house': $('input[name^="in_house"]:checked').val(),
        'in_budget': $('input[name^="in_budget"]:checked').val(),
        'urgent_status': $('input[name^="urgent_statu"]:checked').val(),
        'code_emp': $('#code_emp').val(),
        'code_sup': $('#code_sup').val(),
        'status': $('#status').val(),
        'id_company': $('#id_company').val(),
    };

    var idlastinsert;

    //console.log(dataposthead);
    $.post('insertheadppr', { data: dataposthead }, function(res) {
        console.log(res);
        idlastinsert = res;
        Savedetail(idlastinsert);
    });


    //Savedetail()

}

function Savedetail(idlastinsert) {
    var arrconfig_group_supp_id = [];
    var arrlist = [];
    var arramount = [];
    var arrtype_amount = [];
    var arrprice = [];
    var arrvat = [];
    var arrtotal = [];
    var arrdaterecentpurchases = [];
    var arravg = [];
    var arrnote = [];
    var arrmaterialid = [];
    var arridsupplier = []
    var arrwithhold = [];
    var arrcheckapproved = [];
    var arrcheckapprovedmd = [];


    $('select[name^="config_group_supp_id[]"]').each(function() {
        arrconfig_group_supp_id.push($(this).val());
    });
    $('input[name^="material_id[]"]').each(function() {
        arrmaterialid.push($(this).val());
    });
    $('input[name^="list[]"]').each(function() {
        arrlist.push($(this).val());
    });
    $('input[name^="amount[]"]').each(function() {
        arramount.push($(this).val());
    });
    $('input[name^="type_amount[]"]').each(function() {
        arrtype_amount.push($(this).val());
    });
    $('input[name^="price[]"]').each(function() {
        arrprice.push($(this).val());
    });
    $('select[name^="vat[]"]').each(function() {
        arrvat.push($(this).val());
    });
    $('select[name^="withhold[]"]').each(function() {
        arrwithhold.push($(this).val());
    });
    $('input[name^="total[]"]').each(function() {
        arrtotal.push($(this).val());
    });
    $('input[name^="daterecentpurchases[]"]').each(function() {
        arrdaterecentpurchases.push($(this).val());
    });
    $('input[name^="avg[]"]').each(function() {
        arravg.push($(this).val());
    });
    $('input[name^="note[]"]').each(function() {
        arrnote.push($(this).val());
    });

    $('input[name^="checkapproved[]"]').each(function() {
        if ($(this).is(':checked')) {
            //console.log(1);
            arrcheckapproved.push(1);
        } else {
            arrcheckapproved.push(99);
        }
    });

    $('input[name^="checkapprovedmd[]"]').each(function() {
        if ($(this).is(':checked')) {
            //console.log(1);
            arrcheckapprovedmd.push(1);
        } else {
            arrcheckapprovedmd.push(99);
        }
    });

    $('select[name^="id_supplier[]"]').each(function() {
        arridsupplier.push($(this).val());
    });

    var datapostdetail = {
        'config_group_supp_id': arrconfig_group_supp_id,
        'arrmaterialid': arrmaterialid,
        'arrlist': arrlist,
        'arramount': arramount,
        'arrtype_amount': arrtype_amount,
        'arrprice': arrprice,
        'arrvat': arrvat,
        'arrwithhold': arrwithhold,
        'arrtotal': arrtotal,
        'arrdaterecentpurchases': arrdaterecentpurchases,
        'arravg': arravg,
        'arrnote': arrnote,
        'idhead': idlastinsert,
        'arrcheckapproved': arrcheckapproved,
        'arrcheckapprovedmd': arrcheckapprovedmd,
        'arridsupplier': arridsupplier,
    };

    //console.log(datapostdetail);
    $.post('insertdetailppr', { data: datapostdetail }, function(res) {
        //console.log(res);
        if (res == 1) {
            bootbox.alert({
                title: "แจ้งเตือน",
                message: "บันทึกข้อมูลเรียบร้อยแล้ว!!",
                callback: function() {
                    // $('#ConfirmDialog').modal('hide');
                    $('#myModal').modal('hide');
                    location.reload();

                }
            });
        }

    });



}
//
function insertnew() {

    while (index >= 1) {
        if (index == 1) {
            $('#tbindex' + index).remove();
            // index++;
        } else {
            $('#tbindex' + index).remove();

        }
        index--;
    }
    index = 1;
    $('#status').val(0);

    $.get('getidpr?id=' + 1, function(data) {
        var dataprint = JSON.parse(data);
        // console.log(dataprint.idgen);
        $('#number_ppr').val(dataprint.idgen);
        $('#bbr_no_branch').val(dataprint.idnumber);
    });


    $('#Btn_save').show();
    // $('#updatestatus').hide();

    $('#id').val('');
    $('#dateshow').val($('#dateshowset').val());
    $('#date').val($('#dateset').val());
    $('#year').val($('#yearset').val());
    $('#year_th').val($('#year_thset').val());
    // $('#bbr_no_branch').val($('#bbr_no_branchset').val());
    // $('#number_ppr').val($('#number_pprset').val());


    $('#supplier_id').val('0').trigger('change');
    $('#type_pay').val('');
    $('#type_buy').val('');
    $("#in_house1").prop("checked", true);
    $("#in_budget1").prop("checked", true);
    $("#urgent_status2").prop("checked", true);

    $('#config_group_supp_id0').val('');
    $('#list0').val('');
    $('#amount0').val('');
    $('#type_amount0').val('');
    $('#price0').val('');
    $('#vat0').val(0);
    $('#total0').val('');
    $('#daterecentpurchases0').val('');
    $('#avg0').val('');
    $('#note0').val('');

    $(".datepicker").datepicker({
        format: 'dd-mm-yyyy',
    });

    $('.select2').select2({ width: '100%' });



}




function CloneTableRow() {

    // console.log(index);

    var html = '';
    html += '<tr id="tbindex' + index + '">';
    html += '<td><input type="checkbox" id="checkapproved' + index + '" name="checkapproved[]" class="checkapproved" value="0"></td>';
    html += '<td><input type="checkbox" id="checkapprovedmd' + index + '" name="checkapprovedmd[]" class="checkapprovedmd" value="0"></td>';
    html += '<td><select name="id_supplier[]" id="id_supplier' + index + '" class="form-control select2"  required></select></td>';
    html += '<td><select name="config_group_supp_id[]" id="config_group_supp_id' + index + '" class="form-control" onchange="selectaccnumber(this,' + index + ')" required></select></td>';
    html += '<td> <input type="hidden" name="material_id[]" id="materialid' + index + '" value="0"><input type="text" style="width: 50rem;" name="list[]" id="list' + index + '" onfocus="autocompletedata(' + index + ')"  class="form-control" disabled></td>';
    html += '<td><input type="text" name="amount[]" id="amount' + index + '" onblur="getamount(this,' + index + ')"   class="form-control form-control full-form" style="width: 80px;" placeholder="จำนวน"></td>';
    html += '<td><input type="text" name="type_amount[]" id="type_amount' + index + '"  class="form-control form-control full-form" style="width: 60px;" placeholder="หน่วยนับ"></td>';
    html += '<td><input type="text" name="price[]" id="price' + index + '" disabled onblur="getprice(this,' + index + ')"  style="width: 90px;"  class="form-control form-control full-form" ></td>';
    html += '<td><select name="vat[]" id="vat' + index + '" onchange="calculatevat(this,' + index + ')" class="form-control form-control full-form" style="width: 90px;" ></select></td>';
    html += '<td style="display:none;"><select name="withhold[]" id="withhold' + index + '" onchange="calculatewithhold(this,' + index + ')" class="form-control form-control full-form" style="width: 90px;" ></select></td>';
    html += '<td><input type="text" name="total[]" id="total' + index + '"   class="form-control form-control full-form" style="width: 100px;"  ></td>';
    html += '<td><input type="text" name="daterecentpurchases[]" readonly id="daterecentpurchases' + index + '"   class="form-control datepicker form-control full-form"  ></td>';
    html += '<td><input type="text" name="avg[]" id="avg' + index + '"   class="form-control form-control full-form"  ></td>';
    html += '<td><input type="text" name="note[]" id="note' + index + '"   class="form-control form-control full-form"  ></td>';
    html += '<td><button type="button" id="del" onclick="deleteMe(this);" class="btn btn-danger"><i class="fa fa-trash"></i> </button></td>';
    html += '</tr>';


    $('#tdbody').append(html);


    setoption(index);

    var level_emp = $('#level_emp').val();
    var emp_code = $('#emp_code').val();

    if (level_emp == 1 || level_emp == 2 || emp_code== 1606 || emp_code == 1427  || emp_code == 1383) {
        $("#checkapproved" + index).removeAttr('disabled');
    } else {
        $("#checkapproved" + index).attr('disabled', 'disabled');
    }


    index++;
    $(".datepicker").datepicker({
        format: 'dd-mm-yyyy',
    });
    $('.select2').select2();





}


function deleteMe(me) {
    var tr = $(me).closest("tr");
    tr.remove();
    // console.log(me);
}

function setoption(index) {

    $('#id_supplier0').find('option').clone().appendTo('#id_supplier' + index + '');
    $('#config_group_supp_id0').find('option').clone().appendTo('#config_group_supp_id' + index + '');
    $('#vat0').find('option').clone().appendTo('#vat' + index + '');
    $('#withhold0').find('option').clone().appendTo('#withhold' + index + '');

}


function getamount(val, index) {
    $('#total' + index).val(0);

    amount = val.value;
    resulttotalindex(index);
    $('#total' + index).val(0);
    $('#price' + index).prop("disabled", false);

}

function getprice(val, index) {
    $('#total' + index).val(0);
    price = val.value;
    resulttotalindex(index);
}




function resulttotalindex(index) {
    $('#total' + index).val(0);
    var total = (amount * price);
    $('#total' + index).val(total);
}





function getdata(val, id) {
    //console.log(val);

    while (index >= 1) {
        if (index == 1) {
            $('#tbindex' + index).remove();
            // index++;
        } else {
            $('#tbindex' + index).remove();

        }
        index--;
    }

    // if (val == 1) {
    //     $('#Btn_save').show();
    //     $('#updatestatus').hide();
    // } else if (val == 2) {
    //     $('#Btn_save').hide();
    //     $('#updatestatus').hide();
    // } else if (val == 3) {
    //     $('#Btn_save').show();
    //     $('#updatestatus').show();
    // }





    $('#id').val(id);
    $.get("getdatavendordetail?id=" + id, function(data) {

        // console.log(data);
        $('#statusprocess').val(1);

        $('#id').val(data[0].id);
        $('#date').val(data[0].date);
        $('#year').val(data[0].year);
        $('#year_th').val(data[0].year_th);
        $('#bbr_no_branch').val(data[0].bbr_no_branch);
        $('#branch_id').val(data[0].branch_id);
        $('#number_ppr').val(data[0].number_ppr);
        $('#supplier_id').val(data[0].supplier_id).trigger('change');
        $('#type_pay').val(data[0].type_pay);
        $('#type_buy').val(data[0].type_buy);
        $('#id_company').val(data[0].id_company);
        // $('input[name^="in_house"]:checked').val();
        // $('input[name^="in_budget"]:checked').val();
        // $('input[name^="urgent_statu"]:checked').val();
        if (data[0].in_house == 1) {
            $("#in_house1").prop("checked", true);
        } else {
            $("#in_house2").prop("checked", true);

        }

        if (data[0].in_budget == 1) {
            $("#in_budget1").prop("checked", true);
        } else {
            $("#in_budget2").prop("checked", true);

        }

        if (data[0].urgent_status == 1) {
            $("#urgent_status1").prop("checked", true);
        } else if(data[0].urgent_status == 2){
            $("#urgent_status2").prop("checked", true);
        } else if(data[0].urgent_status == 3){
            $("#urgent_status3").prop("checked", true);
        }

        $('#code_emp').val(data[0].code_emp);
        $('#code_sup').val(data[0].code_sup);
        $('#status').val(data[0].status);


        showdetailppr(val, id);

        //console.log(index);

    });


}


function showdetailppr(val, idhead) {
    $.get("getdatavendorpprdetail?id=" + idhead, function(res) {
        showtable(val, res);

    });
}

function showtable(val, data) {

    var count = (Object.keys(data).length);
    // console.log(count);
    var html = '';
    // index = 1;
    //
    var set = '';

    for (set = 1; set <= count - 1; set++) {
        index++;
        html += '<tr id="tbindex' + index + '">';
        html += '<td><input type="checkbox" id="checkapproved' + index + '" name="checkapproved[]" class="checkapproved" value="0"></td>';
        html += '<td><input type="checkbox" id="checkapprovedmd' + index + '" name="checkapprovedmd[]" class="checkapprovedmd" value="0"></td>';
        html += '<td><select name="id_supplier[]" id="id_supplier' + index + '" class="form-control select2"  required></select></td>';
        html += '<td><select name="config_group_supp_id[]" id="config_group_supp_id' + index + '" class="form-control" onchange="selectaccnumber(this,' + index + ')" required></select></td>';
        html += '<td> <input type="hidden" name="material_id[]" id="materialid' + index + '" value="0"><input type="text" name="list[]" id="list' + index + '" onfocus="autocompletedata(' + index + ')"  class="form-control" disabled></td>';
        html += '<td><input type="text" name="amount[]" id="amount' + index + '" onblur="getamount(this,' + index + ')"   class="form-control" style="width: 80px;" placeholder="จำนวน"></td>';
        html += '<td><input type="text" name="type_amount[]" id="type_amount' + index + '"  class="form-control" style="width: 60px;" placeholder="หน่วยนับ"></td>';
        html += '<td><input type="text" name="price[]" id="price' + index + '" disabled onblur="getprice(this,' + index + ')"  style="width: 90px;"  class="form-control" ></td>';
        html += '<td><select name="vat[]" id="vat' + index + '" onchange="calculatevat(this,' + index + ')" class="form-control" style="width: 90px;" ></select></td>';
        html += '<td style="display: none;"><select name="withhold[]" id="withhold' + index + '" onchange="calculatewithhold(this,' + index + ')" class="form-control" style="width: 90px;" ></select></td>';
        html += '<td><input type="text" name="total[]" id="total' + index + '"   class="form-control" style="width: 100px;"  ></td>';
        html += '<td><input type="text" name="daterecentpurchases[]" readonly id="daterecentpurchases' + index + '"   class="form-control datepicker"  ></td>';
        html += '<td><input type="text" name="avg[]" id="avg' + index + '"   class="form-control"  ></td>';
        html += '<td><input type="text" name="note[]" id="note' + index + '"   class="form-control"  ></td>';
        if (val == 1) {
            html += '<td><button type="button" id="del" onclick="deleteMe(this);" class="btn btn-danger"><i class="fa fa-trash"></i> </button></td>';
        } else {
            html += '<td></td>';
        }

        html += '</tr>';



    }

    $('#tdbody').append(html);
    // setoption(index);

    var setno = '';
    for (setno = 1; setno <= count - 1; setno++) {
        $('#id_supplier0').find('option').clone().appendTo('#id_supplier' + setno + '');
        $('#config_group_supp_id0').find('option').clone().appendTo('#config_group_supp_id' + setno + '');
        $('#vat0').find('option').clone().appendTo('#vat' + setno + '');
        $('#withhold0').find('option').clone().appendTo('#withhold' + setno + '');
    }
    $(".datepicker").datepicker({
        format: 'dd-mm-yyyy',
    });




    $.each(data, function(key, value) {

        //  console.log(data[key].approvedstatus);

        // if (data[key].approvedstatus == 1) {// checkkkkkkkkkkkkkkkkkkkkk approvedstatus
        //     $('#checkapproved' + key).attr('checked', true);
        // }
        $('#id_supplier' + key).val(data[key].id_supplier).trigger('change');
        $('#config_group_supp_id' + key).val(data[key].config_group_supp_id);
        $('#accounttype_id' + key).empty();

        var _option = "<option value=''>เลือกเลขบัญชี</option>";

        var valuetestacc = data[key].accounttype_id;

        $.get('getdataaccounttype?idacc=' + data[key].config_group_supp_id, function(data) {

            $.each(data, function(key, value) {
                _option += "<option value=" + data[key].id + ">" + data[key].accounttypeno + "__" + data[key].accounttypefull + "</option>";
            });

            $('#accounttype_id' + key).append(_option);
            //  console.log(valuetestacc);
            $('#accounttype_id' + key).val(valuetestacc);
        });

        $('#materialid' + key).val(data[key].materialid);
        $('#list' + key).val(data[key].list);
        $('#amount' + key).val(data[key].amount);
        $('#type_amount' + key).val(data[key].type_amount);
        $('#price' + key).val(data[key].price);
        var vat = Number(data[key].vat);
        $('#vat' + key).val(vat);
        $('#withhold' + key).val(data[key].withhold);
        $('#total' + key).val(data[key].total);
        $('#daterecentpurchases' + key).val(data[key].daterecentpurchases);
        $('#avg' + key).val(data[key].avg);
        $('#note' + key).val(data[key].note);
        index++;
    });

    seleteacctype(data)

    $(".select2").select2({ width: '100%' });



}

function seleteacctype(data) {
    console.log(data);
}


function calculatevat(val, index) {
    var tax = val.value;
    var setamount = $('#amount' + index).val();
    var setprice = $('#price' + index).val();
    var withhold = $('#withhold' + index).val();


    caltotal(setprice, setamount, tax, withhold, index);

}


function calculatewithhold(val, index) {
    var withhold = val.value;
    var tax = $('#vat' + index).val();
    var setprice = $('#price' + index).val();
    var setamount = $('#amount' + index).val();

    // if(withhold==0){
    //     var total = (setamount*setprice)+0;
    //     total = total.toFixed(2);
    //     $('#total'+index).val(total);
    // }else{
    //     var total = (setamount*setprice)+((setamount*setprice)/tex);
    //     total = total.toFixed(2);
    //     $('#total'+index).val(total);
    // }
    caltotal(setprice, setamount, tax, withhold, index);

}


function caltotal(price, amount, tax, withhold, index) {
    var resulttax = 0;
    var resultwithhold = 0;
    if (tax == 0) {
        resulttax = 0;
    } else {
        resulttax = ((price * amount) * (tax / 100));
        resulttax = resulttax.toFixed(2);

    }

    if (withhold == 0) {
        resultwithhold = 0;
    } else {
        resultwithhold = ((price * amount) * (withhold / 100));
        resultwithhold = resultwithhold.toFixed(2);
    }
    var result = Number((price * amount)) + Number(resulttax) - Number(resultwithhold);
    result = result.toFixed(2);

    $('#total' + index).val(result);

}


function selectnopr(val) {

    var idcompany = val.value;

    $.get('getidpr?id=' + idcompany, function(data) {
        var dataprint = JSON.parse(data);
        // console.log(dataprint.idgen);
        $('#number_ppr').val(dataprint.idgen);
        $('#bbr_no_branch').val(dataprint.idnumber);
    });
}



function autocompletedata(thisval) {
    var thisconfig_group_supp_id = $('#config_group_supp_id' + thisval).val();

    // สินทรัพย์
    if (thisconfig_group_supp_id == 1) {
        $('#list' + thisval).autocomplete({
            source: objasset,
            select: function(event, ui) {
                var materialid = ui.item.id;
                var unit = ui.item.unit;
                $('#materialid' + thisval).val(materialid);
                $('#type_amount' + thisval).val(unit);
            }
        });
    }

    // สินค้า
    if (thisconfig_group_supp_id == 2) {
        $('#list' + thisval).autocomplete({
            source: availableTags,
            select: function(event, ui) {
                var materialid = ui.item.id;
                $('#materialid' + thisval).val(materialid);
            }
        });
    }
    // อื่นๆ
    if (thisconfig_group_supp_id == 3) {
        $('#list' + thisval).autocomplete({
            source: objohter,
            select: function(event, ui) {
                var materialid = ui.item.id;
                var unit = ui.item.unit;
                $('#materialid' + thisval).val(materialid);
                $('#type_amount' + thisval).val(unit);
            }
        });
    }



}

function selectaccnumber(val, thisindex) {
    var typeconfig_group_supp = val.value;
    $('#accounttype_id' + thisindex).empty();

    var _option = "<option value=''>เลือกเลขบัญชี</option>";

    $.get('getdataaccounttype?idacc=' + typeconfig_group_supp, function(data) {

        $.each(data, function(key, value) {
            _option += "<option value=" + data[key].id + ">" + data[key].accounttypeno + "__" + data[key].accounttypefull + "</option>";
        });

        $('#accounttype_id' + thisindex).append(_option);

    });

    $('#list' + thisindex).prop("disabled", false); // Element(s) are now enabled.

}
