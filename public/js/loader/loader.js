function loader(){
  var dialog_load;
  $(document).ajaxStart(function(){
    // $("#wait").css("display", "block");
    // alert('start');
    dialog_load = bootbox.dialog({
        size: "small",
        message: '<div class="text-center"><img class="loader" src="images/load/spin.png" alt="loader"></div><div class="text-center"><i class="fa fa-spin fa-spinner"></i> Loading...</div>',
        closeButton: false
    })

    dialog_load = dialog_load.find('.modal-content').css({
         'background-color': '#f6f6f6',
         'font-weight' : 'bold',
         'font-size': '1.5em',
         'border-radius': '15px',
         'display': 'block',
         'position': 'relative',
         'margin-top': function (){
             var w = $( window ).height();
             var b = $(".modal-dialog").height();
             // should not be (w-h)/2
             var h = (w-b)/2;
             return h+"px";
         }
     });
     // console.log(dialog_load);
    // dialog_load.modal('hide');
  });
  $(document).ajaxComplete(function(){
    // $("#wait").css("display", "none");
    // console.log(dialog_load);
    $('.bootbox').hide();
    $('.modal-backdrop').hide();
  });
}
