var datacus = '';

$(document).ready(function() {

      $(".datepicker").datepicker({
        format: 'dd/mm/yyyy',
        language: 'th'
      });

      $('#reservation').daterangepicker()

      $("#month").datepicker( {  //รายเดือน
        format: " yyyy",
        viewMode: "years",
        minViewMode: "years"
        });

        $("#allmonth").datepicker( {  //ทุกเดือน
          format: " mm/yyyy",
          viewMode: "months",
          minViewMode: "months"
        });

        $("#year").datepicker( {  //รายปี
          format: " yyyy",
          viewMode: "years",
          minViewMode: "years"

        });

        $("#toyear").datepicker( {  //รายปี
          format: " yyyy",
          viewMode: "years",
          minViewMode: "years"

        });

        $("#monthbr").datepicker( {  //ทุกเดือน
          format: " mm-yyyy",
          viewMode: "months",
          minViewMode: "months"
        });

        // console.log('2222222');
        // var datepicker = $('#datepicker').val();
        // console.log(datepicker);

        var grandtotal = 0;
        var grandtotal1 = 0;
        var grandtotal2 = 0;
        var grandtotal3 = 0;
        var total = 0;
        var sumgrand1 = 0;
        var sumgrand2 = 0;
        var sumgrand3 = 0;
        var sumgrand4 = 0;
        var sumgrand = 0;

        $('#grandtotal').change(function(){
            grandtotal = parseFloat($(this).val());
            if($(this).val() == '' || $(this).val() == undefined){
                $('#total').val('');
            }else{
                total = grandtotal + grandtotal1 + grandtotal2 + grandtotal3;
                $('#total').val(total);
            }
        });

        $('#grandtotal1').change(function(){
            grandtotal1 = parseFloat($(this).val());
            if($(this).val() == '' || $(this).val() == undefined){
                $('#total').val('');
            }else{
                total = grandtotal + grandtotal1 + grandtotal2 + grandtotal3;
                $('#total').val(total);
            }
        });

        $('#grandtotal2').change(function(){
            grandtotal2 = parseFloat($(this).val());
            if($(this).val() == '' || $(this).val() == undefined){
                $('#total').val('');
            }else{
                total = grandtotal + grandtotal1 + grandtotal2 + grandtotal3;
                $('#total').val(total);
            }
        });

        $('#grandtotal3').change(function(){
            grandtotal3 = parseFloat($(this).val());
            if($(this).val() == '' || $(this).val() == undefined){
                $('#total').val('');
            }else{
                total = grandtotal + grandtotal1 + grandtotal2 + grandtotal3;
                $('#total').val(total);
            }
        });

        $('#sumgrand1').change(function(){
          // console.log($(this).val());
            sumgrand1 = parseFloat($(this).val());
            if($(this).val() == '' || $(this).val() == undefined){
                $('#sumgrand').val('');
            }else{
                sumgrand = sumgrand1 + sumgrand2 + sumgrand3 + sumgrand4;
                $('#sumgrand').val(sumgrand);
            }
        });

        $('#sumgrand2').change(function(){
          // console.log($(this).val());
            sumgrand2 = parseFloat($(this).val());
            if($(this).val() == '' || $(this).val() == undefined){
                $('#sumgrand').val('');
            }else{
                sumgrand = sumgrand1 + sumgrand2 + sumgrand3 + sumgrand4;
                $('#sumgrand').val(sumgrand);
            }
        });

        $('#sumgrand3').change(function(){
          // console.log($(this).val());
            sumgrand3 = parseFloat($(this).val());
            if($(this).val() == '' || $(this).val() == undefined){
                $('#sumgrand').val('');
            }else{
                sumgrand = sumgrand1 + sumgrand2 + sumgrand3 + sumgrand4;
                $('#sumgrand').val(sumgrand);
            }
        });

        $('#sumgrand4').change(function(){
          // console.log($(this).val());
            sumgrand4 = parseFloat($(this).val());
            if($(this).val() == '' || $(this).val() == undefined){
                $('#sumgrand').val('');
            }else{
                sumgrand = sumgrand1 + sumgrand2 + sumgrand3 + sumgrand4;
                $('#sumgrand').val(sumgrand);
            }
        });

        $('#example').DataTable({
            dom: 'Bfrtip',
            buttons:[
                $.extend( true, {}, fixNewLine, {
                    extend: 'copyHtml5'
                } ),
                $.extend( true, {}, fixNewLine, {
                    extend: 'excelHtml5'
                } )

            ]

        });

});

// function getdatesubmit() {
//
//     var valid = $('#configFormvendors').validator('validate').has('.has-error').length;
//     if (valid == 0) {
//
//         bootbox.confirm({
//             title: "ยืนยันการทำรายการ",
//             message: "ยืนยันการตรวจสอบ",
//             buttons: {
//                 cancel: {
//                     label: '<i class="fa fa-times"></i> Cancel'
//                 },
//                 confirm: {
//                     label: '<i class="fa fa-check"></i> Confirm'
//                 }
//             },
//             callback: function(result) {
//                 if (result) {
//                     Savehead();
//                 }
//
//             }
//         });
//     }
//     return false;
// }

// function Savehead() {
//
//     var dataposthead = {
//         'id_compay': $('select[name=id_compay]').val(),
//         'list': $('select[name=list]').val(),
//         'amount': $('#amount').val(),
//         'vat': $('#vat').val(),
//         'vat_money': $('#vat_money').val(),
//         'total': $('#total').val(),
//         'po_ref': '',
//         'note': $('#note').val()
//     };
//
//     $.post('insertpaypre', { data: dataposthead }, function(res) {
//           location.reload();
//     });
//
//
//     //Savedetail()
//
// }


// function resulttotal(index) {
//     $('#total' + index).val(0);
//     var total = (grandtotal + grandtotal1 + grandtotal2 + grandtotal3);
//     $('#total' + index).val(total);
// }

// function nStr(){
//
//     var int1 =document.getElementById('grandtotal').value;
//     var int2=document.getElementById('grandtotal1').value;
//     var int3=document.getElementById('grandtotal2').value;
//     var int4=document.getElementById('grandtotal3').value;
//
//     var n1 = parseInt(int1);
//     var n2 = parseInt(int2);
//     var show = document.getElementById('show');
//
//      if (isNaN(n1)){
//           document.getElementById("show").setAttribute("color","red");
//           show.innerHTML=""
//          if (int2.length>0){
//              if (isNaN(int1)){
//                  document.getElementById("show").setAttribute("color","red");
//                  show.innerHTML=""
//              }
//              else if (isNaN(int2)){
//                  document.getElementById("show").setAttribute("color","red");
//                  show.innerHTML=""
//              }
//              else  if (int1.length >0){
//                    document.getElementById("show").setAttribute("color","Blue");
//                  show.innerHTML=n1+n2;
//              }
//              else if (int2.length>0){
//                  document.getElementById("show").setAttribute("color","Blue");
//                  show.innerHTML=n2;
//              }
//           }
//        }
//      else if (int1.length > 0) {
//          if (isNaN(int2)){
//                document.getElementById("show").setAttribute("color","red");
//                show.innerHTML=""
//          }
//          else if (int2.length >0){
//                document.getElementById("show").setAttribute("color","Blue");
//                show.innerHTML=n1+n2;
//          }
//          else if (int1.length > 0){
//                document.getElementById("show").setAttribute("color","Blue");
//                show.innerHTML=n1;
//        }
//      }
//    }
//    function addCommas(nStr) //ฟังชั่้นเพิ่ม คอมม่าในการแสดงเลข
//     {
//         nStr += '';
//         x = nStr.split('.');
//         show = x[0];
//         x2 = x.length > 1 ? '.' + x[1] : '';
//         var rgx = /(\d+)(\d{3})/;
//         while (rgx.test(x1)) {
//         show = show.replace(rgx, '$1' + ',' + '$2');
//         }
//         return x1 + x2;
//     }
