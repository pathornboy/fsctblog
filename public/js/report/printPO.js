var datacus = '';

$(document).ready(function() {


    $(".datepicker").datepicker({

      format: 'dd/mm/yyyy',
      language: 'th'

    });
      $('#datepicker').datepicker('setDate', new Date());
      $('#reservation').daterangepicker()

});


function autocompletecusdata() {


    $.get("getdatacustomeridandname", function(res) {
        datacus =  res;
        //console.log(datacus);

    });

    $('#namecus').autocomplete({
        source: datacus,
        select: function (event, ui) {
            var cusid = ui.item.id;
            $('#cusid').val(cusid);


        }
    });

}
