/**
 * Created by pacharapol on 1/14/2018 AD.
 */
var index = 1;
var amount = 0;
var price = 0;
var vat = 0;
var availableTags = '';
var objasset = '';
var objohter = '';

$(document).ready(function() {
    $('#example').DataTable();
    $('.select2').select2();

    $('#addrow').on("click", function() {
        // console.log(index);

        CloneTableRow();
        //bindDataWithStep();
    });

    $(".datepicker").datepicker({
        format: 'dd-mm-yyyy',
    });

    $('#F_PO_NO').click(function(){
      if($('#po_number').val() === ''){
        alert('กรุณากรอกเลข PO');
      }else{
        find_po();
      }
    });



    // $.get("getmainmaterialall", function(res) {
    //     availableTags = res;
    // });
    //
    // $.get("getgoodassetall", function(res) {
    //     console.log(res);
    //     objasset = res;
    // });

    // $.get("getotherall", function(res) {
    //
    //     objohter = res;
    // });

    // $('#checkapprovedall').click(function(event) {
    //     if (this.checked) {
    //         // Iterate each checkbox
    //         $('.checkapproved:checkbox').each(function() {//checkapprovedmd
    //             this.checked = true;
    //         });
    //     } else {
    //         $('.checkapproved:checkbox').each(function() {
    //             this.checked = false;
    //         });
    //     }
    // });

    // $('#checkapprovedallmd').click(function(event) {
    //     if (this.checked) {
    //         // Iterate each checkbox
    //         $('.checkapprovedmd:checkbox').each(function() {//
    //             this.checked = true;
    //         });
    //     } else {
    //         $('.checkapprovedmd:checkbox').each(function() {
    //             this.checked = false;
    //         });
    //     }
    // });


});

function getcashrent(id){
  $.post('getcashrent', { id: id }, function(res) {
      // console.log(res);

      $('#money').val(res.money);
      $('#typetranfer').val(res.typetranfer);
      $('#ID').val(res.id);


      // if (res) {
      //     bootbox.alert({
      //         title: "แจ้งเตือน",
      //         message: "บันทึกข้อมูลเรียบร้อยแล้ว!!",
      //         callback: function() {
      //             // console.log(res);
      //             // $('#myModal').modal('hide');
      //             // location.reload();
      //
      //
      //         }
      //     });
      // }

  });
}

// function editcashrent(){
//   $.post('insertdetailppr', { data: datapostdetail }, function(res) {
//       //console.log(res);
//       if (res == 1) {
//           bootbox.alert({
//               title: "แจ้งเตือน",
//               message: "บันทึกข้อมูลเรียบร้อยแล้ว!!",
//               callback: function() {
//                   // $('#ConfirmDialog').modal('hide');
//                   $('#myModal').modal('hide');
//                   location.reload();
//
//               }
//           });
//       }
//
//   });
// }

function getdatesubmit() {

    var valid = $('#configFormvendors').validator('validate').has('.has-error').length;
    if (valid == 0) {

        bootbox.confirm({
            title: "ยืนยันใบ PR?",
            message: "ยืนยันการตรวจสอบ",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function(result) {
                if (result) {
                    save();
                }

            }
        });
    }
    return false;
}

function save(){
  data = {
    money: $('#money').val(),
    typetranfer: $('#typetranfer').val(),
    ID: $('#ID').val(),
    emp_code: $('#emp_code').val()
  }

  $.post('savecashrent', { data: data }, function(res) {
      // console.log(res);

      // $('#money').val(res.money);
      // $('#typetranfer').val(res.typetranfer);


      if (res) {
          bootbox.alert({
              title: "แจ้งเตือน",
              message: "บันทึกข้อมูลเรียบร้อยแล้ว!!",
              callback: function() {
                  // console.log(res);
                  $('#myModal').modal('hide');
                  location.reload();


              }
          });
      }

  });
}

function getdatesubmitbill() {

    var valid = $('#configinsert').validator('validate').has('.has-error').length;
    if (valid == 0) {

        bootbox.confirm({
            title: "ยืนยันการทำรายการ",
            message: "ยืนยันการตรวจสอบ",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function(result) {
                if (result) {
                    // console.log(222);
                    Savehead();

                }

            }
        });
    }
    return false;
}

function Savehead() {

    var dataposthead = {
        'typedoc': $('select[name=typedoc]').val(),
        'billno': $('#billno').val(),
        'moneycash': $('#moneycash').val(),
        'typetranfercash': $('select[name=typetranfercash]').val(),
        // console.log(222);
    };
    console.log(typetranfer);

    $.post('insertbillcash', { data: dataposthead }, function(res) {
      // console.log(333);
          location.reload();
    });


    //Savedetail()

}
